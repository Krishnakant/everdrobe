<?php 
class NotificationQueuesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('send');
	}
	function admin_report($status=null)
	{
		$conditions=array();
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.id']="'".$search."'";
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['NotificationQueue.message LIKE']="%".$search;
			$conditions['OR']['NotificationQueue.device_token LIKE']="%".$search;
		}
		$this->paginate = array(
				'limit' => 30,
				'fields'=>array('User.id','User.first_name','User.username','User.last_name','NotificationQueue.*'),
				'conditions'=>$conditions
		);
		$data = $this->paginate('NotificationQueue');
		$this->set(compact('data'));
	}
	function send()
	{
		$current_date= date('Y-m-d H:i:s');
		$queueData=$this->NotificationQueue->find('all',array("conditions"=>array("sent"=>"no", "sent_on < "=>$current_date),"limit"=>500));
		if($queueData)
		{
			$notification_ios=array();
			$notification_android=array();
			$ids=array();
			$last_message=false;
			$message_counter=0;
			foreach($queueData as $data)
			{
				if($data["NotificationQueue"]["device_type"]=="ios")
				{
					$notification_ios[]=array(
							'queue_id'=>$data["NotificationQueue"]["id"],
							"message"=>$data["NotificationQueue"]["message"],
							"token"=>$data["NotificationQueue"]["device_token"],
							"badge"=> $data["NotificationQueue"]["badge"],
							"params"=>json_decode($data["NotificationQueue"]["params"],true)
					);
				}
				else
				{
					$message_id=md5($data["NotificationQueue"]["message"]);
					if(!isset($notification_android[$message_id]))
					{
						$notification_android[$message_id]=array(
								'message'=>array_merge(
										array("message"=>$data["NotificationQueue"]["message"]),
										json_decode($data["NotificationQueue"]["params"],true)
									),
								"tokens"=>array(),
								"queue_id"=>array()
							);
					}
					
					$notification_android[$message_id]['tokens'][]=$data["NotificationQueue"]["device_token"];
					$notification_android[$message_id]['queue_id'][]=$data["NotificationQueue"]["id"];
				}
				$ids[]=$data["NotificationQueue"]["id"];
			}
			/*
			 *send notification to ios 
			 */
			if(count($notification_ios)>0)
			{
				$response=$this->send_notification_bulk($notification_ios,true);
				
				foreach($response as $key=>$val)
				{
					$this->NotificationQueue->id=$key;
					$this->NotificationQueue->updateAll(array(
							'NotificationQueue.response'=>'"'.$val.'"',
							'NotificationQueue.attempt'=>'NotificationQueue.attempt+1',
							'NotificationQueue.sent_on'=>'"'.date('Y-m-d H:i:s').'"',
							'NotificationQueue.sent'=>'"yes"'),
							array("NotificationQueue.id"=>$key));
				}
				
				
			}
			if(count($notification_android)>0)
			{
				foreach($notification_android as $key=>$android)
				{
					$response=$this->send_notification_bulk_android($android,true);
					$queue_ids=$android['queue_id'];
					
					foreach($response['results'] as $key=>$val)
					{
						if(isset($val['message_id']))
						{
							$sent='"yes"';
							$res=$val['message_id'];
						}
						else if(isset($val['error']))
						{
							$sent='"failed"';
							$res=$val['error'];
						}
						else
						{
							$sent='"no"';
							$res="''";
						}
						
						$this->NotificationQueue->id=$key;
						$this->NotificationQueue->updateAll(array(
							'NotificationQueue.response'=>'"'.$res.'"',
							'NotificationQueue.attempt'=>'NotificationQueue.attempt+1',
							'NotificationQueue.sent_on'=>'"'.date('Y-m-d H:i:s').'"',
							'NotificationQueue.sent'=>$sent),
							array("NotificationQueue.id"=>$queue_ids[$key]));
					}
					
				}
			}
		}
	}
	
	function admin_clear_log()
	{
		if($this->NotificationQueue->deleteAll(array('NotificationQueue.sent'=>'yes')))
		{
			$this->Session->setFlash("Log clear successfully!","default",array("class"=>"success"));
		}
		else
		{
			$this->Session->setFlash("Broad Cast log not found!","default",array("class"=>"error"));
		}
		$this->redirect($this->referer());
	}
}
?>