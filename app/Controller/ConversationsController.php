<?php
class ConversationsController extends AppController
{
	var $helpers=array('Time');
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function view($conversation_id=null)
	{
		$conversations=$this->Conversation->find('all',array('fields'=>array('Conversation.*','User.first_name','User.last_name','User.username','User.unique_id','User.photo','User.star_user'),'conditions'=>array('Conversation.unique_id'=>$conversation_id),"order"=>"Conversation.created_on ASC"));
		$this->set('conversations',$conversations);
	}
	function admin_view($conversation_id=null,$uploader_id=null)
	{
		$conversations=$this->Conversation->find('all',array('fields'=>array('Conversation.*','User.id','User.first_name','User.last_name','User.username','User.unique_id','User.photo','User.star_user'),'conditions'=>array('Conversation.unique_id'=>$conversation_id),"order"=>"Conversation.created_on ASC"));
		$conversation_users=array($uploader_id);
		foreach($conversations as $conv)
		{
			if(!in_array($conv['User']['id'],$conversation_users))
			{
				$conversation_users[]=$conv['User']['id'];
			}
		}
		
		$users=$this->Conversation->User->find('all',array("fields"=>array("User.id","User.first_name","User.username","User.last_name"),"conditions"=>array("User.id"=>$conversation_users)));
		$conversation_users=array();
		foreach($users as $user)
		{
			$conversation_users[$user['User']['id']]=$user['User']['username'];
		}
		$this->set('conversation_users',$conversation_users);
		$this->set('conversations',$conversations);
		$this->set('uploader_id',$uploader_id);
	}
	function add($conversation_id=null)
	{
		$response=array();
		if(!empty($this->data) || $conversation_id==null)
		{
			if($this->Conversation->save(array('unique_id'=>$conversation_id,'user_id'=>$this->Auth->user('id'),'conversation'=>$this->data['reply'])))
			{
				$this->loadModel('Rate');
				$this->Rate->bindModel(array("belongsTo"=>array("Drobe")));
				$rateData=$this->Rate->find('first',array('fields'=>array('Rate.*','Drobe.user_id','Drobe.id'),'conditions'=>array('Rate.conversation_id'=>$conversation_id)));
				
				// inline notification must be updated
				if(true || $this->getUserSetting($rateData['Rate']['user_id'],'conversation'))
				{
					//update new reposnse or new reply in conversations between two users
					$updateFields=array();
					if($rateData['Rate']['user_id']==$this->Auth->user('id'))
					{
						$updateFields['Rate.new_reply']=$rateData['Rate']['new_reply']+1;
						
					}
					else $updateFields['Rate.new_response']=$rateData['Rate']['new_response']+1;
					$updateFields['Rate.last_conversation_time']="'".date('Y-m-d H:i:s')."'";
					$this->Rate->recursive = -1;
					$this->Rate->updateAll($updateFields,array('Rate.id'=>$rateData['Rate']['id']));
					
				}

				if($rateData['Rate']['user_id']==$this->Auth->user('id'))
				{
					$notify_to=$rateData['Drobe']['user_id'];
				}
				else
				{
					$notify_to=$rateData['Rate']['user_id'];
				}
				// send pushnotification if notification settings is on on receiver side
				if($notify_to > 0 && $this->getUserSetting($notify_to,'conversation'))
				{
					
					$params=array('action'=>"conversation",
							'drobe_id'=>$rateData['Drobe']['id'],
							'user_id'=>$notify_to,
							'conv_id'=>$conversation_id,
							'type'=> $rateData['Drobe']['user_id']==$this->Auth->user('id')?"gave":"got"
					);
					$this->send_notification_user("New conversation message recieved",$notify_to,$params);
				}
				
				
				
				$response['type']="success";
				$response['message']="Replay posted successfully on this converstion";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in post your conversation";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid data";
		}
		echo json_encode($response);
		exit();
		
	}
	function admin_add()
	{
		if(!empty($this->data))
		{
			$conversationData=$this->data['Conversation'];
			if($this->Conversation->save($conversationData))
			{
				$this->loadModel('Rate');
				$this->Rate->bindModel(array("belongsTo"=>array("Drobe")));
				$rateData=$this->Rate->find('first',array('fields'=>array('Rate.*','Drobe.user_id','Drobe.id'),'conditions'=>array('Rate.conversation_id'=>$this->data['conversation_id'])));
				if($this->getUserSetting($rateData['Rate']['user_id'],'conversation'))
				{
					//update new reposnse or new reply in conversations between two users
					$updateFields=array();
					if($rateData['Rate']['user_id']==$this->data['user_id']) $updateFields['Rate.new_reply']=$rateData['Rate']['new_reply']+1;
					else $updateFields['Rate.new_response']=$rateData['Rate']['new_response']+1;
					$this->Rate->recursive = -1;
					$this->Rate->updateAll($updateFields,array('Rate.id'=>$rateData['Rate']['id']));
				}
				$this->Session->setFlash("Reply posted successfully on this converstion","default",array("class"=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in post your conversation","default",array("class"=>"error"));
			}
		}
		else
		{
			$this->Session->setFlash("Invalid post params","default",array("class"=>"error"));
		}
		$this->redirect($this->referer());
	}
	function _conversationUsers($unique_id)
	{
		//getting list of conversation users
		$this->loadModel("Rate");
		$users=array();
		$users[]=$this->Rate->field('user_id',array("conversation_id"=>$unique_id));
		$drobe_id=$this->Rate->field('drobe_id',array("conversation_id"=>$unique_id));
		$this->loadModel("Drobe");
		$users[]=$this->Drobe->field('user_id',array("id"=>$drobe_id));
		$users=$this->Conversation->User->find('all',array("fields"=>array("User.id","User.first_name","User.username","User.last_name"),"conditions"=>array("User.id"=>$users)));
		$conversation_users=array();
		foreach($users as $user)
		{
			$conversation_users[$user['User']['id']]=$user['User']['username'];
		}
		$this->set('conversation_users',$conversation_users);
	}
	function add_conversation($conversation_id=null)
	{
		$response=array();
		if(!empty($this->data) || $conversation_id==null)
		{
			$user_id=$this->data['user_id'];
			if($this->Conversation->save(array('unique_id'=>$conversation_id,'user_id'=>$user_id,'conversation'=>$this->data['message'])))
			{
				$this->loadModel('Rate');
				$this->Rate->bindModel(array("belongsTo"=>array("Drobe")));
				$rateData=$this->Rate->find('first',array('fields'=>array('Rate.*','Drobe.user_id','Drobe.id'),'conditions'=>array('Rate.conversation_id'=>$conversation_id)));
	
				// inline notification must be updated
				if(true || $this->getUserSetting($rateData['Rate']['user_id'],'conversation'))
				{
					//update new reposnse or new reply in conversations between two users
					$updateFields=array();
					if($rateData['Rate']['user_id']==$user_id) 
					{
						$updateFields['Rate.new_reply']=$rateData['Rate']['new_reply']+1;
					}
					else $updateFields['Rate.new_response']=$rateData['Rate']['new_response']+1;
					$updateFields['Rate.last_conversation_time']="'".date('Y-m-d H:i:s')."'";
					$this->Rate->recursive = -1;
					$this->Rate->updateAll($updateFields,array('Rate.id'=>$rateData['Rate']['id']));
				}
				
				
				if($rateData['Rate']['user_id']==$user_id)
				{
					$notify_to=$rateData['Drobe']['user_id'];
					$notify_from=$rateData['Rate']['user_id'];
				}
				else
				{
					$notify_to=$rateData['Rate']['user_id'];
					$notify_from=$rateData['Drobe']['user_id'];
				}
				// send pushnotification if notification setting is on on receiver side
				if($notify_to>0 && $this->getUserSetting($notify_to,'conversation'))
				{
					$params=array('action'=>"conversation",
							'drobe_id'=>$rateData['Drobe']['id'],
							'user_id'=>$notify_from,
							'conv_id'=>$conversation_id,
							'type'=>$rateData['Drobe']['user_id']==$user_id?"gave":"got"
						);
					$this->send_notification_user("New message recieved",$notify_to,$params);
				}
				
				$response['type']="success";
				$response['message']="Replay posted successfully on this converstion";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in post your conversation";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid params";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
	}
	function admin_edit($unique_id, $conversation_id)
	{
		if(!empty($this->data))
		{
			
			if($this->Conversation->save($this->data))
			{
				$this->Session->setFlash("Conversation message updated successfully","default",array("class"=>"success"));
				
			}
			else
			{
				$this->Session->setFlash("Error occured in update message, please try again later","default",array("class"=>"error"));
				
			}
			$this->loadModel("Rate");
			$drobe_id=$this->Rate->field('drobe_id',array("conversation_id"=>$unique_id));
			$this->redirect(array("controller"=>"drobes","action"=>"conversation",$drobe_id,$unique_id,"admin"=>true));
		}
		else
		{
			$this->Conversation->id=$conversation_id;
			$this->data=$this->Conversation->read();
		}
		$this->_conversationUsers($unique_id);
	}
	function admin_remove($unique_id, $conversation_id)
	{
		if($this->Conversation->deleteAll(array('Conversation.id'=>$conversation_id)))
		{
			$this->Session->setFlash("Selected Conversation removed successfully","default",array("class"=>"success"));
			
		}
		else
		{
			$this->Session->setFlash("Error occured in remove selected conversation message, try again later","default",array("class"=>"error"));
		}
		$this->redirect($this->referer());
	}
	function admin_change_status($conversation_id=null,$new_status="close")
	{
		$this->loadModel("Rate");
		$rateData=$this->Rate->findByConversationId($conversation_id);
		if($rateData)
		{
			$this->Rate->id=$rateData['Rate']['id'];
			$this->Rate->saveField('conversation_status',$new_status);
			
		}
		$this->redirect($this->referer());
	}
}
?>