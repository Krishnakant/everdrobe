<?php 
class DrobesController extends AppController
{
	//var $components=array('FileUpload');
	var $paginate = array(
		'limit' => 25,
		'order' => array(
			'Drobe.uploaded_on' => 'desc'
		)
	);
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('do_vote','timeline','index','getlist','getlist_highest_rated','rate','drobe_list','test_from','reset_drobe_order','create_short_url','update_short_url','total_url','update_short_url1','delete_deleted_drobes');
		$this->Auth->deny('mydrobes','create');
	}
	function timeline()
	{
		$drobes=Cache::read('latest_drobes');
		if(Cache::read('latest_drobes')==null)
		{
			$this->Drobe->recursive=-1;
			$drobes=$this->Drobe->find('all',array("fields"=>array("Drobe.file_name","Drobe.comment"),"conditions"=>array("Drobe.comment !="=>"","Drobe.rate_status"=>"open"),"order"=>array("Drobe.uploaded_on DESC"),"limit"=>2));
			Cache::write('latest_drobes',$drobes);
		}
		$this->set("drobes",$drobes);
	}
	function delete_deleted_drobes()
	{
		$this->Drobe->recursive=-1;
		$drobes=$this->Drobe->find('all',array(
				"fields"=>array("id","file_name","user_id"),
				"conditions"=>array("Drobe.deleted"=>"1"),
				"limit"=>3
				));
		
		/*$file = fopen(WWW_ROOT."deleted_drobes.txt","w");
		fwrite($file,json_encode($drobes));
		fclose($file);*/
		
		$drobeIDS = array();
		foreach($drobes as $value)
		{
			
			$key = $value["Drobe"]['id'];
			$file_name = $value["Drobe"]['file_name'];
			$user_id = $value["Drobe"]['user_id'];
			
			$drobeIDS[]=$key;
			$dir=WWW_ROOT."drobe_images/";
			unlink($dir.$file_name);
			$dir=WWW_ROOT."drobe_images/thumb/";
			unlink($dir.$file_name);
			$dir=WWW_ROOT."drobe_images/iphone/";
			unlink($dir.$file_name);
			$dir=WWW_ROOT."drobe_images/iphone/thumb/";
			unlink($dir.$file_name);
			
			// update users total
			$this->_deleteDrobeRates($key);
			$this->Drobe->User->resetUserTotalFields($user_id);
			
			/*Remove badges of follower when drobe is deleted*/
			$this->_removeNotifyFollowers($key);
			
			$this->Drobe->updateRecentCache();
			
			/***********code by devang also remove from shopify****************/
			$SellDrobeData=$this->Drobe->SellDrobe->findByDrobeId($key);
			$SellDrobeData=$SellDrobeData['SellDrobe'];
			if($SellDrobeData['shopify_product_id']>0)
			{
				$response=$this->shopify("/products/".$SellDrobeData['shopify_product_id'], "","DELETE");
				if(count($response)==0)
				{
					/*
					 * Removing tracking id from everdrobe database
					*/
			
					$this->Drobe->SellDrobe->id=$SellDrobeData['id'];
					$this->Drobe->SellDrobe->saveField('shopify_product_id',0);
					$this->Drobe->SellDrobe->SellDrobeImage->updateAll(array('shopify_image_id'=>0),array('sell_drobe_id'=>$SellDrobeData['id']));
			
				}
			}
		}
		$this->Drobe->deleteAll(array('Drobe.id'=>$drobeIDS),false);
		$this->loadModel("DrobeSetting");
		$this->DrobeSetting->deleteAll(array('DrobeSetting.drobe_id'=>$drobeIDS),false);
		
		$this->loadModel("EbxTransaction");
		$this->EbxTransaction->deleteAll(array('EbxTransaction.drobe_id'=>$drobeIDS),false);
		$this->loadModel("Favourite");
		$this->Favourite->deleteAll(array('Favourite.drobe_id'=>$drobeIDS),false);
		$this->loadModel("Flag");
		$this->Flag->deleteAll(array('Flag.drobe_id'=>$drobeIDS),false);
		$this->loadModel("HighestRatedDrobe");
		$this->HighestRatedDrobe->deleteAll(array('HighestRatedDrobe.drobe_id'=>$drobeIDS),false);
		$this->loadModel("NewDrobe");
		$this->NewDrobe->deleteAll(array('NewDrobe.drobe_id'=>$drobeIDS),false);
		$this->loadModel("Rate");
		$this->Rate->deleteAll(array('Rate.drobe_id'=>$drobeIDS),false);
		$this->loadModel("SoldDrobe");
		$this->SoldDrobe->deleteAll(array('SoldDrobe.drobe_id'=>$drobeIDS),false);
		$this->loadModel("SellDrobe");
		$this->loadModel("SellDrobeImage");
		
		$this->SellDrobe->recursive=-1;
		$sel_drobes_id=$this->SellDrobe->find('list',array("conditions"=>array("SellDrobe.drobe_id"=>$drobeIDS)));
		$sell_drobes=$this->SellDrobeImage->find('list',array("fields"=>array("id","file_name"),"conditions"=>array("SellDrobeImage.sell_drobe_id"=>$sel_drobes_id)));
		
		foreach($sell_drobes as $key=>$selldrobe)
		{
			$dir=WWW_ROOT."drobe_images/additional/";
			unlink($dir.$selldrobe);
			$dir=WWW_ROOT."drobe_images/additional/thumb/";
			unlink($dir.$selldrobe);
		}
		
		$this->SellDrobe->deleteAll(array('SellDrobe.id'=>$sel_drobes_id),false);
		$this->SellDrobeImage->deleteAll(array('SellDrobeImage.sell_drobe_id'=>$sel_drobes_id),false);
		echo "Process complete";exit;
	}
	function index()
	{
		$this->loadModel('UserFilter');
		$userFilter  = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
		$this->set('userFilter',$userFilter);
		/*$this->loadModel('StreamCategory');
		$streamCategory=$this->StreamCategory->_loadUsersCategory($this->Auth->user('id'));
		$this->set('streamCategory',$streamCategory);*/
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		
		if(Cache::read('brand_list')==null)
		{
			$this->loadModel('Brand');
			$this->Brand->_updateBrandCache();
		}
		
		$this->set('title_for_layout',"Drobes");
	}
	function getlist($sort_by="recent",$latest_drobe="0",$category_id=null)
	{
		$order="";
		/*
		 * Filtering drobes for loggedin user
		 */
		$conditions=array(
			//'Drobe.user_id !='=>$this->Auth->user('id'),
			'Drobe.deleted'=>0,
			'Drobe.rate_status'=>'open',
			'User.id >'=>0
		);
		//$conditions['DrobeSetting.ask_public']=1;
		if($this->Auth->user('id')>0)
		{	
			$this->loadModel('UserFilter');
			
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
			
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}  
			
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."'  OR User.gender is null)))";
			}
			
			
			
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new") 
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
			
			
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
		}
		
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
				
				/* Old condition changed on 27/11/2013*/
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%"); 
				$conditions['AND'][] = array('OR'=>
						array('OR'=>
								array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"),
								 'Drobe.comment LIKE' => "% ".$key."%"
							)
						);
				
			}
		}
	
		/*
		 * End of filtering for logged in users
		 */
			
		
		$joins=array();
		if($sort_by=="recent") 
		{
			// fixing issue of drobe repeat on next page;
			if($latest_drobe != "0")
			{
				$latest_drobe_id=$this->Drobe->field('id',array('Drobe.unique_id'=>$latest_drobe));
				if($latest_drobe_id>0) $conditions['Drobe.id <= ']=$latest_drobe_id;				
			}
			
			if($this->Auth->user('id')>0)
			{
				$joins=array(
						array(
								'table' => 'rates',
								'alias' => 'Rate',
								'type' => 'LEFT',
								'conditions' => array(
										'Rate.drobe_id = Drobe.id',
										'Rate.user_id' => $this->Auth->user('id')
								)
						)
				);
				//$order="Drobe.uploaded_on DESC";
				$order="Rate.user_id ASC,Drobe.uploaded_on DESC";
			}
			else $order="Drobe.uploaded_on DESC";
		}
		if($sort_by=="rated") 
		{
			$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		}
		if($sort_by=="viewed") $order="Drobe.views DESC";
		
		if($sort_by=="featured")
		{
			$order="Drobe.featured DESC, Drobe.order ASC,Drobe.id DESC";
			$conditions[]=array("OR"=>array("Drobe.featured"=>1,"AND"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved")));
		}
		//$conditions=array('Drobe.user_id !='=>$this->Auth->user('id'),'Drobe.rate_status'=>'open');
		if($category_id>0)
		{
			$conditions['Drobe.category_id']=$category_id;
		}
		
		//first priority is for search keyword with #
		/*if(isset($keyword) && $keyword!=""){
			$order=" Drobe.comment = '%#".$keyword."%' , ".$order;
		}*/
		
		
		//If in search filter highest rated is on then order is following
		// if Higehest rated is 1 then drobe order is according to highest rated
		if($userFilter['highest_rated']==1)
		{
			$order = "Drobe.rate_index DESC";
		}
		
		$fields =array('Drobe.*','SellDrobe.sell_price','SellDrobe.original_sell_price','SellDrobe.size_name');
		
		if($sort_by=="recent" && strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		$this->Drobe->unbindModel(array('hasOne'=>array("DrobeSetting"),
				'hasMany'=> array("Rate","Flag","Favourite")));
		
		$this->paginate = array(
			'fields'=>$fields,
			'limit' => 24,
			'order'=>$order,
			'conditions'=>$conditions,
			'group'=>'Drobe.id',
			'joins'=>$joins
		);
		$data = $this->paginate('Drobe');
		
		$this->set(compact('data'));
		
	}
	
	/*
	 * New logic for highest rated drobe for fixing issue of duplicate listing
	 */
	
	function getlist_highest_rated($page=1,$category_id=null)
	{
		/*
		 * Filtering drobes for loggedin user
		*/
		$conditions=array(
			//	'Drobe.user_id !='=>$this->Auth->user('id'),
				'Drobe.deleted'=>0,
				'Drobe.rate_status'=>'open',
				'User.id >'=>0
		);
		//$conditions['DrobeSetting.ask_public']=1;
		if($this->Auth->user('id')>0)
		{	
			
			$this->loadModel('UserFilter');
			
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
			
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}  
			
			
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."'  OR User.gender is null)))";
			}
			
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new") 
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
			
			
			
				
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
			
		}
		
		
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
			
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
			
			/* $conditions['OR']['User.first_name LIKE ']=$keyword."%";
			$conditions['OR']['User.last_name LIKE ']=$keyword."%";
			$conditions['OR']['User.email LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.first_name," ",User.last_name) LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.last_name," ",User.first_name) LIKE ']=$keyword."%";
			$conditions['OR']['Category.category_name LIKE ']="%".$keyword."%"; */
		}
	
		/*
		 * End of filtering for logged in users
		*/
			
		$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		
		//$conditions=array('Drobe.user_id !='=>$this->Auth->user('id'),'Drobe.rate_status'=>'open');
		if($category_id>0)
		{
			$conditions['Drobe.category_id']=$category_id;
		}
		
		//first priority is for search keyword with #
		/*if(isset($keyword) && $keyword!=""){
			$order=" Drobe.comment = '%#".$keyword."%' , ".$order;
		}*/
		
		
		$fields =array('Drobe.*','SellDrobe.sell_price','SellDrobe.original_sell_price','SellDrobe.size_name');
		
		if(strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		
		
		
		
		$this->Drobe->unbindModel(array('hasOne'=>array("DrobeSetting"),
				'hasMany'=> array("Rate","Flag","Favourite")));
		
		$data=$this->Drobe->find('all', array(
				'fields'=>$fields,
				'limit' => 24*$page,
				'offset' =>0,
				'order'=>$order,
				'conditions'=>$conditions,
				'group'=>'Drobe.id',
		));
		$this->set(compact('data'));
	}
	/*
	 * Moderate Image List
	 */
	function admin_moderator_list()
	{
		$dbo = $this->Drobe->getDataSource();
		$query_1 = $dbo->buildStatement(
				array(
						'fields'=>array('Drobe.id as drobe_id','0 as image_id','Drobe.user_id as user_id','Drobe.file_name as file_name','SellDrobe.id as sell_drobe_id','Drobe.uploaded_on as uploaded_time','"drobe" as image_type'),
						'conditions'=>array('Drobe.rate_status'=>"open","Drobe.deleted"=>0),
						'joins'=>array(array(
								'table' => 'sell_drobes',
								'alias' => 'SellDrobe',
								'type' => 'LEFT',
								'conditions' => array(
										'SellDrobe.drobe_id = Drobe.id',
								)
						)
						),
						'table' => $dbo->fullTableName($this->Drobe),
						'alias' =>"Drobe",
						'order' =>"",
						'limit' => false,
						'group' =>""
				),
				$this->Drobe
		);
		$dbo = $this->Drobe->SellDrobe->SellDrobeImage->getDataSource();
		$query_2 = $dbo->buildStatement(
				array(
						'fields'=>array('Drobe.id as drobe_id','SellDrobeImage.id as image_id','Drobe.user_id as user_id','SellDrobeImage.file_name as file_name','SellDrobeImage.sell_drobe_id as sell_drobe_id','SellDrobeImage.created_on as uploaded_time','"additional_image" as image_type'),
						'table' => $dbo->fullTableName($this->Drobe->SellDrobe->SellDrobeImage),
						'joins'=>array(array(
								'table' => 'sell_drobes',
								'alias' => 'SellDrobe',
								'type' => 'LEFT',
								'conditions' => array(
										'SellDrobe.id = SellDrobeImage.sell_drobe_id',
								)
						),
								array(
										'table' => 'drobes',
										'alias' => 'Drobe',
										'type' => 'LEFT',
										'conditions' => array(
												'Drobe.id = SellDrobe.drobe_id',
										)
								)
						),
						'alias' =>"SellDrobeImage",
						'order' =>"",
						'limit' => false,
						'group' =>array(),
						'conditions' =>array()
				),
				$this->Drobe->SellDrobe
		);
		$sub_query = $query_1." UNION ". $query_2;
		
		$this->Drobe->recursive=-1;
		$this->paginate =array(
				'fields'=>array('Image.*'),
				'joins'=>array(
						array(
								'table' => "(".$sub_query.")",
								'alias' => 'Image',
								'type' => 'RIGHT',
								'conditions' => array(
										'Image.drobe_id= Drobe.id'
								))
		
				),
				'limit' => 110,
				'order'=>"Image.uploaded_time DESC"
		);
		$imageData = $this->paginate('Drobe');
		$this->set('images',$imageData);
	}

	function admin_moderator_list_old()
	{
		//$this->SellDrobe->SellDrobeImage->recursive=2;
		$drobes=$this->Drobe->find('all',array('fields'=>array('Drobe.id','Drobe.user_id','Drobe.file_name','SellDrobe.id'), 'conditions'=>array('Drobe.rate_status'=>"open","Drobe.deleted"=>0)));
		$images=array();
		foreach($drobes as $drobe)
		{
			// add drobe image into array
			$filename=WWW_ROOT . Configure::read('drobe.upload_dir') .DS . $drobe['Drobe']['file_name'];
			if (file_exists($filename)) {
				$images[filemtime($filename)]=array("file_name"=>$drobe['Drobe']['file_name'],"user_id"=>$drobe['Drobe']['user_id'],"drobe_id"=>$drobe['Drobe']['id'],"type"=>"drobe");
			}
		}
		$sell_drobes=$this->Drobe->SellDrobe->find('all');
		foreach($sell_drobes as $drobe)
		{
			if(count($drobe['SellDrobeImage'])>0)
			{
				// add additional images into array
				foreach($drobe['SellDrobeImage'] as $image)
				{
					$filename=WWW_ROOT . Configure::read('sell_drobe.upload_dir') .DS . $image['file_name'];
					if (file_exists($filename)) {
						$images[filemtime($filename)]=array_merge($image,array("user_id"=>$drobe['Drobe']['user_id'],"drobe_id"=>$drobe['Drobe']['id'],"type"=>"additional"));
					}
				}
			}
		}
		// reversing short based on image uploaded time
		krsort($images);
		$this->set('images',$images);
	}
	
	/*
	 * Webservice for drobe list
	 */
	function drobe_list($sort_by="most_recent",$user_id=null)
	{		
		$response=array();
		/*
		 * Filtering drobes for loggedin user
		*/
		
		
		$conditions=array(
				'Drobe.deleted'=>0,
				'Drobe.rate_status'=>'open',
				'User.id >'=>0,
				'OR'=>array()
		);
		
		$conditions['DrobeSetting.ask_public']=1;
		
		if($user_id==null)
		{
			//$conditions['Drobe.user_id !=']=$this->data['user_id'];
		}
		
		
		$drobe_ids_last_month = $this->Drobe->find("list",array("fields"=>array("Drobe.id"),"conditions"=>array("Drobe.uploaded_on >"=>date("Y-m-d H:i:s",strtotime("-3 month")))));
		
		if(isset($this->data['user_id']) && $this->data['user_id'])
		{
			//rates given by user on drobes
			$rated_ids=array();
			$this->Drobe->Rate->recursive=-1;
			$rated_drobes=array_values($this->Drobe->Rate->find('list',array('fields'=>array('Rate.drobe_id'),'conditions'=>array('Rate.user_id'=>$this->data['user_id']),'group'=>array('Rate.drobe_id'))));
			
			//list of my favourite
			$my_favs=array_values($this->Drobe->Favourite->find('list',array('fields'=>array('Favourite.drobe_id'),"conditions"=>array('Favourite.user_id'=>$this->data['user_id']))));
			
			// list of followings 
			$my_followings=array_values($this->Drobe->User->Follower->find('list',array('fields'=>array('Follower.user_id'),"conditions"=>array('Follower.follower_id'=>$this->data['user_id']))));
		}
		
		if(isset($this->data['only_following']) && $this->data['only_following']=="1")
		{	
			$following_ids = $this->Drobe->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$this->data['user_id'],"Follower.request"=>"accepted")));
			$conditions['Drobe.user_id']=$following_ids;
		}
		
		/*
		 * If user pass search parameter with two or more word by space then here split by space for separate each word from search text area.
		 * */
		if(isset($this->data['search']) && $this->data['search']!="")
		{
			$keyword= preg_split("/[\s,]+/", $this->data['search']);
			foreach($keyword as $key)
			{
				$key = ltrim($key,'#');
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
		}
		
		//if category ids mentioned then need to listion only selected id's drobes 
		if(isset($this->data['category_id']) && $this->data['category_id']!="" && $this->data['category_id']!="ALL")
		{
			$categories=explode(",",$this->data['category_id']);
			$conditions['Drobe.category_id']=$categories;
		}
		
		// if gender target mentioned
		//if(isset($this->data['gender']) && $this->data['gender']!="" && $this->data['gender']!="both") //old condition before post_by gender query
		if(isset($this->data['gender']) && in_array($this->data['gender'],array("male","female")))
		{
			$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$this->data['gender']."') 
			OR (Drobe.post_type='post' AND (User.gender='".$this->data['gender']."' OR User.gender is null)))";
		}
		
		
		/*
		 * Started Haresh Code for adding new filters
		 */
		$only_sell_drobe=false;
		// if drobe_for is mentioned
		if(isset($this->data['price_range']) && $this->data['price_range']!="" )
		{
			$only_sell_drobe=true;
			//$conditions['SellDrobe.sell_price <=']=intval($this->data['max_price']);
			$conditions['AND']=$this->_parse_price_range($this->data['price_range']);
		}
		// if min_price for search drobe is mentioned
		if(isset($this->data['min_price']) && intval($this->data['min_price'])>0 )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.sell_price >=']=intval($this->data['min_price']);
		}
		
		// if brand name is mentioned
		if(isset($this->data['brand_name']) && $this->data['brand_name']!="" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.sell_brand_name']=explode("||",$this->data['brand_name']);;
		}
		
		// if size name is mentioned for search filter
		if(isset($this->data['size_name']) && $this->data['size_name']!="" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.size_name']=explode("||",$this->data['size_name']);
		}
		// if only_sell_items is mentioned then search only sell drobe
		if(isset($this->data['only_sell_items']) && $this->data['only_sell_items']=="1" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.sold']="no";
		}
		
		// if only new brand is mentioned then search only new brand drobe
		if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="new" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.is_brand_new']="new";
		}
		else if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="used")  //if only new brand is mentioned as used then search only use brand
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.is_brand_new']="used";
		}
		
		//Check here only_sell_drobe is true then search only sell drobe other wise search all drobes.
		if($only_sell_drobe)
		{
			$conditions['Drobe.post_type']="sell";
		}
		/*
		 * End of Haresh for adding new filters
		 */
	
		// if listing only oter users drobes
		if($user_id!=null)
		{
			$conditions['Drobe.user_id']=$user_id;
			
			// removing notification of new drobe uploaded by followings
			if($this->data['user_id']>0)
			{
				$this->loadModel('NewDrobe');
				$this->NewDrobe->deleteAll(array('user_id'=>$user_id,'follower_id'=>$this->data['user_id']),false);
			}
		}
		else
		{
			// do not need of listing own drobe
			/*if(isset($this->data['user_id']))
			{
				$conditions['Drobe.user_id !=']=$this->data['user_id'];
			}*/
		}

		
		//setting order of drobes in listing
		$order="";
		$joins=array();
		if($sort_by=="most_recent") 
		{
			// fixing issue of drobe repeat on next page;
			if(isset($this->data['latest_drobe_id']) && $this->data['latest_drobe_id']>0)
			{
				$conditions['Drobe.id <= ']=$this->data['latest_drobe_id'];
			}
			
			/*
			 * When user is logged in the in most resent tab need to display unrated drobes first
			 */
			if(isset($this->data['user_id']) && $this->data['user_id'])
			{
				$joins=array(
					array(
						'table' => 'rates',
						'alias' => 'Rate',
						'type' => 'LEFT',
						'conditions' => array(
							'Rate.drobe_id = Drobe.id',
							'Rate.user_id' =>  $this->data['user_id']
						)
					)
				);
				$order="Rate.user_id ASC, Drobe.id DESC";
			}
			else $order="Drobe.id DESC";
		}
		if($sort_by=="most_rated") 
		{
			//$order="Drobe.total_in/Drobe.total_rate DESC, Drobe.views DESC";
			//$order="Drobe.rate_index DESC, Drobe.views DESC";
			$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		}
		if($sort_by=="most_viewed") $order="Drobe.views DESC";
		
		if($sort_by=="featured")
		{
			// added by haresh
			if(isset($this->data['highest_rated']) && $this->data['highest_rated']=="1")
			{
				$order=$this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
			}
			else
			{
				$order="Drobe.featured DESC, Drobe.order ASC,Drobe.id DESC";
			}
			// end of haresh
			$conditions[]=array("OR"=>array("Drobe.featured"=>1,"AND"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved")));
		}
		
		if(count($conditions['OR'])==0) unset($conditions['OR']);
		
		//removing unnecessory associations
		$this->Drobe->unbindModel(array('hasMany' => array("Rate","Flag","Favourite")));
		$this->Drobe->bindModel(array('hasOne' => array("SellDrobe")));
		
		//first priority is for search keyword with #
		if($sort_by!="most_recent" && isset($keyword) && $keyword!=""){
			$order=" Drobe.comment = '%#".$keyword."%' , ".$order;
		}
		
		
		/**
		 *  All fields are listed which is required in getlist.ctip file
		 * */
		$fields =array('SellDrobe.sell_price','SellDrobe.is_brand_new','SellDrobe.original_sell_price','SellDrobe.sell_description','SellDrobe.tracking_number','SellDrobe.sold','SellDrobe.size_name','SellDrobe.sell_brand_name','SellDrobe.sell_gender',
					'Drobe.id','Drobe.uploaded_on','Drobe.unique_id','Drobe.user_id','Drobe.file_name','Drobe.comment','Drobe.total_rate',
					'Drobe.views','Drobe.total_in','Drobe.total_out','Drobe.is_highest_rated',
					'Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs',
					'Category.id','Category.category_name',"User.first_name","User.last_name","User.username","User.photo");
		
		
		/*
		 * Search keyword with hash value if first word contain hash value then find with this word from database table with hash and without hash both but hash value contained word is come upper level.
		 * $keyword[0] contained first word of search input area search by user.
		 * */
		if($sort_by=="most_recent" && strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		$conditions["Drobe.id"]=$drobe_ids_last_month;
		
		
		$drobe_properties = array('fields'=>$fields,
				'limit' => 48,
				'order'=>$order,
				'conditions'=>$conditions,
				'group'=>'Drobe.id',
				'joins'=>$joins);

		if(isset($this->params->params['named']['page']))
		{
			$drobe_properties['page']=$this->params->params['named']['page'];
			
		}

		
		//$this->Drobe->bindModel(array("belongsTo"=>array("User")));
		$data = $this->Drobe->find("all",$drobe_properties);
		
		if(count($data)<48)
		{
			unset($conditions["Drobe.id"]);
			$drobe_properties = array('fields'=>$fields,
					'limit' => 48,
					'order'=>$order,
					'conditions'=>$conditions,
					'group'=>'Drobe.id',
					'joins'=>$joins);
			
			if(isset($this->params->params['named']['page']))
			{
				$drobe_properties['page']=$this->params->params['named']['page'];
					
			}
			$data = $this->Drobe->find("all",$drobe_properties);
		}
		
		if($data)
		{
			$response['drobes']=array();
				
			foreach($data as $drobe)
			{
				// counting views of drobes
				$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
					
				/*$drobe['Drobe']['total_in']= $total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0;
				$drobe['Drobe']['total_out']= $total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0;*/
				$drobe['Drobe']['total_in']= $drobe['Drobe']['total_in'];
				$drobe['Drobe']['total_out']= $drobe['Drobe']['total_out'];
				$drobe['Drobe']['username'] =  $drobe['User']['username'];
				$drobe['Drobe']['first_name'] = $drobe['User']['first_name'];
				$drobe['Drobe']['last_name'] = $drobe['User']['last_name'];
				$drobe['Drobe']['profile_photo']="";
				if($drobe['User']['photo']!="") {
					$drobe['Drobe']['profile_photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$drobe['User']['photo'],true);
				}
				
				unset($drobe['User']);
				if(isset($this->data['user_id']) && $this->data['user_id']!="")
				{
					// is this drobe is my favourite?
					$drobe['Drobe']['my_favourite']=in_array($drobe['Drobe']['id'], $my_favs)?1:0;
					
					
					// is in my follwing list?
					$drobe['Drobe']['is_my_follower']=in_array($drobe['Drobe']['user_id'], $my_followings)?1:0;
						
					// is this drobe rated by me?
					if(in_array($drobe['Drobe']['id'],$rated_drobes))
					{
						$rate=$this->Drobe->Rate->find('first',array('fields'=>array('Rate.rate','Rate.comment','Rate.rewarded'),'conditions'=>array('Rate.user_id'=>$this->data['user_id'],'Rate.drobe_id'=>$drobe['Drobe']['id'])));
						if($rate)
						{
							$drobe['Drobe']['rated_by_me']=array_merge(array("is_rated"=>1),$rate['Rate']);
							//$this->set('rate_info',$rate['Rate']);
						}
					}
					else $drobe['Drobe']['rated_by_me']=array("is_rated"=>0);
					
				}
				
				// counting favourite drobe
				$drobe['Drobe']['favourite']=$drobe['Drobe']['total_favs'];
				$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
				
				//make sure featured drobe functionality is enabled from admin
				$drobe['Drobe']['featured']= Configure::read('setting.featured_drobe')=="on" && ($drobe['Drobe']['featured']=="1" || $drobe['Drobe']['post_type']=="sell") && $drobe['Drobe']['buy_url']!="" ? "1":"0";
				if($drobe['Drobe']['buy_url']==null || Configure::read('setting.featured_drobe')!="on") 
					$drobe['Drobe']['buy_url']="";
				
				$drobe['Drobe']['Category']=$drobe['Category'];
				unset($drobe['Drobe']['file_name']);
				if($drobe['SellDrobe']['sell_price']!=null)
				{
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
					$drobe['Drobe']['original_sell_price']=($drobe['SellDrobe']['original_sell_price']!=null)?$drobe['SellDrobe']['original_sell_price']:"";
					$drobe['Drobe']['drobe_for']=$drobe['SellDrobe']['sell_gender'];
					$drobe['Drobe']['is_brand_new']=($drobe['SellDrobe']['is_brand_new']=="")?"both":$drobe['SellDrobe']['is_brand_new'];
					$drobe['Drobe']['sell_brand_name']=($drobe['SellDrobe']['sell_brand_name']!="")?$drobe['SellDrobe']['sell_brand_name']:"";
					$drobe['Drobe']['size_name']=($drobe['SellDrobe']['size_name'] != "")?$drobe['SellDrobe']['size_name']:"";
					$drobe['Drobe']['sell_description']=($drobe['SellDrobe']['sell_description']!="")?$drobe['SellDrobe']['sell_description']:"";
					$drobe['Drobe']['tracking_number']=($drobe['SellDrobe']['tracking_number'] != "")?$drobe['SellDrobe']['tracking_number']:"";
					$drobe['Drobe']['sold']=$drobe['SellDrobe']['sold'];
				}
				
				$response['type']="success";
				$response['drobes'][]=$drobe['Drobe'];
			}
			
		}
		else
		{
			$response['type']="error";
			$response['message']="drobe not found";
		}
		
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
		
		/**************log entry*******************/
		
		/* $noLogs = !isset($logs);
		if ($noLogs):
		$sources = ConnectionManager::sourceList();

		$logs = array();
		foreach ($sources as $source):
		$db = ConnectionManager::getDataSource($source);
		if (!method_exists($db, 'getLog')):
		continue;
		endif;
		$logs[$source] = $db->getLog();
		endforeach;
		endif;
		
		$log_string="";
		
		foreach($logs['default']['log'] as $log)
		{
			$log_string.="Query : ".$log['query']."\n";
			$log_string.="Time : ".$log['took']."\n";
		}
		
		$this->log("\n-----------------------------------\n".$log_string,'drobe_list_log'); */
		
		/*********************************************/
	}
	function _parse_price_range($price_range)
	{
		$price_conditions=array("OR"=>array());
		$ranges=explode("||",$price_range);	
		foreach($ranges as $range)
		{
			list($min_price,$max_price)=explode(",",$range);
			$range_condition=array();
			if($min_price>0) $range_condition['SellDrobe.sell_price >=']=$min_price;
			if($max_price>0) $range_condition['SellDrobe.sell_price <=']=$max_price;
			
			if(count($range_condition)>0)
			{
				$price_conditions['OR'][]=array('AND'=>$range_condition);
			}
		}
		return $price_conditions;
	}
	
	/*
	 * 
	 * Webservice for  Most rated drobe_list for v1.2 or above 
	 */
	function drobe_list_highest_rated($page=1,$user_id=null)
	{
		
		$response=array();
		/*
		 * Filtering drobes for loggedin user
		*/
		$conditions=array(
				'Drobe.deleted'=>0,
				'Drobe.rate_status'=>'open',
				'User.id >'=>0,
				'OR'=>array()
		);
		$conditions['DrobeSetting.ask_public']=1;
		
		if(isset($this->data['user_id']) && $this->data['user_id'])
		{
			//$conditions['Drobe.user_id !=']=$this->data['user_id'];
		}
		
		// comma saperated drobe_ids which are listed 
		if(isset($this->data['listed_drobe_ids']) && $this->data['listed_drobe_ids']!="")
		{
			$conditions['Drobe.id NOT ']=explode(',', $this->data['listed_drobe_ids']);		
		}
	
		if(isset($this->data['user_id']) && $this->data['user_id'])
		{
			//rates given by user on drobes
			$rated_ids=array();
			$this->Drobe->Rate->recursive=-1;
			$rated_drobes=array_values($this->Drobe->Rate->find('list',array('fields'=>array('Rate.drobe_id'),'conditions'=>array('Rate.user_id'=>$this->data['user_id']),'group'=>array('Rate.drobe_id'))));
				
			//list of my favourite
			$my_favs=array_values($this->Drobe->Favourite->find('list',array('fields'=>array('Favourite.drobe_id'),"conditions"=>array('Favourite.user_id'=>$this->data['user_id']))));
				
			// list of followings
			$my_followings=array_values($this->Drobe->User->Follower->find('list',array('fields'=>array('Follower.user_id'),"conditions"=>array('Follower.follower_id'=>$this->data['user_id']))));
		}
		
		// Following user for currently logged user
		if(isset($this->data['only_following']) && $this->data['only_following']=="1")
		{
		
			$following_ids = $this->Drobe->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$this->data['user_id'],"Follower.request"=>"accepted")));
			$conditions['Drobe.user_id']=$following_ids;
		}
		
		
		/*
		 * Search parameter if user given parameter in search text box that parameters are separated by space for search each word combination from comment field in database table.
		 * 
		 */
		if(isset($this->data['search']) && $this->data['search']!="")
		{
			//$keyword=$this->data['search'];
			
			$keyword= preg_split("/[\s,]+/", $this->data['search']);
			foreach($keyword as $key)
			{
				$key = ltrim($key,'#');
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
				
			//$conditions['OR']['Drobe.comment LIKE ']="%".$keyword."%";
			
			/* $conditions['OR']['User.first_name LIKE ']=$keyword;
			$conditions['OR']['User.last_name LIKE ']=$keyword;
			$conditions['OR']['User.email LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.first_name," ",User.last_name) LIKE ']=$keyword;
			$conditions['OR']['concat(User.last_name," ",User.first_name) LIKE ']=$keyword;
			$conditions['OR']['User.last_name LIKE ']=$keyword;
			$conditions['OR']['Category.category_name LIKE ']="%".$keyword."%"; */
		}
	
		//if category ids mentioned then need to listion only selected id's drobes
		if(isset($this->data['category_id']) && $this->data['category_id']!="" && $this->data['category_id']!="ALL")
		{
			$categories=explode(",",$this->data['category_id']);
			$conditions['Drobe.category_id']=$categories;
		}
	
		// if gender target mentioned
		if(isset($this->data['gender']) && in_array($this->data['gender'],array("male","female")))
		{
			//if($this->data['gender']=="female") $conditions['OR']['DrobeSetting.female']=1;
			//if($this->data['gender']=="male") $conditions['OR']['DrobeSetting.male']=1;
			$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$this->data['gender']."') 
			OR (Drobe.post_type='post' AND (User.gender='".$this->data['gender']."'  OR User.gender is null)))";
		}
		
		
		
		/*
		 * Started Haresh Code for adding new filters
		 */
		$only_sell_drobe=false;
		
		//if price range is mentioned
		if(isset($this->data['price_range']) && $this->data['price_range']!="" )
		{
			$only_sell_drobe=true;
			//$conditions['SellDrobe.sell_price <=']=intval($this->data['max_price']);
			$conditions['AND']=$this->_parse_price_range($this->data['price_range']);
		}
		
		//if min price is mentioned
		if(isset($this->data['min_price']) && intval($this->data['min_price'])>0 )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.sell_price >=']=intval($this->data['min_price']);
		}
		
		//if brand name is mentioned then search only that brand that brand name explode by pipe sign.
		if(isset($this->data['brand_name']) && $this->data['brand_name']!="" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.sell_brand_name']=explode("||",$this->data['brand_name']);;
		}
		//if size name is mentioned then seach only that size name which is explode by pipe sign.
		if(isset($this->data['size_name']) && $this->data['size_name']!="" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.size_name']=explode("||",$this->data['size_name']);
		}
		
		// if only_sell_items is mentioned then search only sell drobe 
		if(isset($this->data['only_sell_items']) && $this->data['only_sell_items']=="1" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.sold']="no";
		}
		
		//if only_new_brand is mentioned then search only new brand drobes.
		if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="new" )
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.is_brand_new']="new";
		}
		else if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="used" )  //if only_new_brand is mentioned as used then search drobe which are 
		{
			$only_sell_drobe=true;
			$conditions['SellDrobe.is_brand_new']="used";
		}
		// if only_sell_drobe is true then search only sell drobe 
		if($only_sell_drobe)
		{
			$conditions['Drobe.post_type']="sell";
		}
		/*
		 * End of Haresh for adding new filters
		 */
	
		// if listing only oter users drobes
		if($user_id!=null)
		{
			$conditions['Drobe.user_id']=$user_id;
				
			// removing notification of new drobe uploaded by followings
			if(isset($this->data['user_id']) && $this->data['user_id']>0)
			{
				$this->loadModel('NewDrobe');
				$this->NewDrobe->deleteAll(array('user_id'=>$user_id,'follower_id'=>$this->data['user_id']),false);
			}
		}
	
		//setting order of drobes in listing
		//$order="Drobe.rate_index DESC, Drobe.views DESC";
		
		$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		
		
		if(count($conditions['OR'])==0) unset($conditions['OR']);
	
		//removing unnecessory associations
		$this->Drobe->unbindModel(array('hasMany' => array("Rate","Flag","Favourite")));
		$this->Drobe->bindModel(array('hasOne' => array("SellDrobe")));
	
		//first priority is for search keyword with #
		/*if(isset($keyword) && count($keyword)){
			$order=" Drobe.comment = '%#".$keyword[0]."%' , ".$order;
		}*/
		
		
		//All required fields is listed here 
		$fields =array('SellDrobe.sell_price','SellDrobe.is_brand_new','SellDrobe.original_sell_price','SellDrobe.sell_description','SellDrobe.tracking_number','SellDrobe.sold','SellDrobe.size_name','SellDrobe.sell_brand_name','SellDrobe.sell_gender',
				'Drobe.id','Drobe.uploaded_on','Drobe.unique_id','Drobe.user_id','Drobe.file_name','Drobe.comment','Drobe.total_rate',
				'Drobe.views','Drobe.total_in','Drobe.total_out','Drobe.is_highest_rated',
				'Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs',
				'Category.id','Category.category_name',"User.first_name","User.last_name","User.username","User.photo");
		
		
		/*
		 * Search keyword with hash value if first word contain hash value then find with this word from database table with hash and without hash both but hash value contained word is come upper level.
		* $keyword[0] contained first word of search input area search by user.
		* */
		if(strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		
		$data=$this->Drobe->find('all',array(
				'fields'=>$fields,
				'limit' => 48*$page,
				'offet' => 0,
				'order'=>$order,
				'conditions'=>$conditions,
				'group'=>'Drobe.id'
		));
		if($data)
		{
			$response['drobes']=array();
	
			foreach($data as $drobe)
			{
				// counting views of drobes
				$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
					
				/*$drobe['Drobe']['total_in']= $total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0;
				$drobe['Drobe']['total_out']= $total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0;*/
				$drobe['Drobe']['total_in']= $drobe['Drobe']['total_in'];
				$drobe['Drobe']['total_out']= $drobe['Drobe']['total_out'];
				$drobe['Drobe']['username'] =  $drobe['User']['username'];
				$drobe['Drobe']['first_name'] = $drobe['User']['first_name'];
				$drobe['Drobe']['last_name'] = $drobe['User']['last_name'];
				$drobe['Drobe']['profile_photo']="";
				if($drobe['User']['photo']!="") {
					$drobe['Drobe']['profile_photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$drobe['User']['photo'],true);
				}
				
				unset($drobe['User']);
	
				if(isset($this->data['user_id']) && $this->data['user_id']!="")
				{
					// is this drobe is my favourite?
					$drobe['Drobe']['my_favourite']=in_array($drobe['Drobe']['id'], $my_favs)?1:0;
						
						
					// is in my follwing list?
					$drobe['Drobe']['is_my_follower']=in_array($drobe['Drobe']['user_id'], $my_followings)?1:0;
	
					// is this drobe rated by me?
					if(in_array($drobe['Drobe']['id'],$rated_drobes))
					{
						$rate=$this->Drobe->Rate->find('first',array('fields'=>array('Rate.rate','Rate.comment','Rate.rewarded'),'conditions'=>array('Rate.user_id'=>$this->data['user_id'],'Rate.drobe_id'=>$drobe['Drobe']['id'])));
						if($rate)
						{
							$drobe['Drobe']['rated_by_me']=array_merge(array("is_rated"=>1),$rate['Rate']);
							//$this->set('rate_info',$rate['Rate']);
						}
					}
					else $drobe['Drobe']['rated_by_me']=array("is_rated"=>0);
						
						
				}
	
				// counting favourite drobe
				$drobe['Drobe']['favourite']=$drobe['Drobe']['total_favs'];
				$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
	
				//make sure featured drobe functionality is enabled from admin
				$drobe['Drobe']['featured']= Configure::read('setting.featured_drobe')=="on" && ($drobe['Drobe']['featured']=="1" || $drobe['Drobe']['post_type']=="sell") && $drobe['Drobe']['buy_url']!="" ? "1":"0";
				if($drobe['Drobe']['buy_url']==null || Configure::read('setting.featured_drobe')!="on") $drobe['Drobe']['buy_url']="";
	
				$drobe['Drobe']['Category']=$drobe['Category'];
				unset($drobe['Drobe']['file_name']);
				
				/*if($drobe['SellDrobe']['sell_price']!=null)
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price']; */
				
				if($drobe['SellDrobe']['sell_price']!=null)
				{
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
					$drobe['Drobe']['original_sell_price']=($drobe['SellDrobe']['original_sell_price'] == null)?"":$drobe['SellDrobe']['original_sell_price'];
					$drobe['Drobe']['drobe_for']=($drobe['SellDrobe']['sell_gender']==null)?"":$drobe['SellDrobe']['sell_gender'];
					$drobe['Drobe']['is_brand_new']=($drobe['SellDrobe']['is_brand_new']=="")?"both":$drobe['SellDrobe']['is_brand_new'];
					$drobe['Drobe']['sell_brand_name']=($drobe['SellDrobe']['sell_brand_name'] == null)?"":$drobe['SellDrobe']['sell_brand_name'] ;
					$drobe['Drobe']['size_name']=($drobe['SellDrobe']['size_name'] == null)?"":$drobe['SellDrobe']['size_name'];
					$drobe['Drobe']['sell_description']=($drobe['SellDrobe']['sell_description']==null)?"":$drobe['SellDrobe']['sell_description'];
					$drobe['Drobe']['tracking_number']=($drobe['SellDrobe']['tracking_number'] == null)?"":$drobe['SellDrobe']['tracking_number'];
					$drobe['Drobe']['sold']=($drobe['SellDrobe']['sold']==null)?"":$drobe['SellDrobe']['sold'];
				}
				
				$response['type']="success";
				$response['drobes'][]=$drobe['Drobe'];
			}
				
		}
		else
		{
			$response['type']="error";
			$response['message']="drobe not found";
		}
	
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	/*
	 * 
	 * This functyion is used in webservice for getting latest uploaded drobe of perticular user followings or followers
	 */
	function get_user_drobe_detail($user_id,$logged_user_data)
	{
		$conditions=array('Drobe.rate_status'=>'open');
		$conditions['Drobe.user_id']=$user_id;
		if(count($logged_user_data)>0)
		{
			$conditions['Drobe.id NOT']=$logged_user_data['flaged_ids'];
		}
		//removing unnecessory associations
		$this->Drobe->unbindModel(array('hasMany' => array("Rate","Flag","Favourite")));
		$this->Drobe->bindModel(array('hasOne' => array("SellDrobe")));
		$order="Drobe.uploaded_on DESC";
		$data=$this->Drobe->find('all', array(
				'fields'=>array(
						'SellDrobe.sell_price','Drobe.id','Drobe.unique_id','Drobe.user_id','Drobe.file_name',
						'Drobe.comment','Drobe.uploaded_on','Drobe.total_rate',
						'Drobe.views','Drobe.total_in','Drobe.total_out','Drobe.is_highest_rated',
						'Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs',
						'Category.id','Category.category_name'
				),
				'limit' => 4,
				'order'=>$order,
				'group'=>'Drobe.id',
				'conditions'=>$conditions)
		);
		$response=array();
		if($data)
		{
			
			foreach($data as $drobe)
			{
				// counting views of drobes
				$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
			
				/*$drobe['Drobe']['total_in']= "".($total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0);
				$drobe['Drobe']['total_out']="". ($total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0);*/
				$drobe['Drobe']['total_in']= "".$drobe['Drobe']['total_in'];
				$drobe['Drobe']['total_out']="".$drobe['Drobe']['total_out'];
			
				// counting favourite drobe
				$drobe['Drobe']['favourite']=$drobe['Drobe']['total_favs'];
			
				if(count($logged_user_data)>0)
				{
					// is this drobe is my favourite?
					$drobe['Drobe']['my_favourite']="".(in_array($drobe['Drobe']['id'], $logged_user_data['my_favs'])?1:0);
			
					// is in my follwing list?
					$drobe['Drobe']['is_my_follower']=in_array($drobe['Drobe']['user_id'], $logged_user_data['my_followings'])?"1":"0";
			
						
					// is this drobe rated by me?
					if(in_array($drobe['Drobe']['id'],$logged_user_data['rated_drobes']))
					{
						$rate=$this->Drobe->Rate->find('first',array('fields'=>array('Rate.rate','Rate.comment','Rate.rewarded'),'conditions'=>array('Rate.user_id'=>$this->data['user_id'],'Rate.drobe_id'=>$drobe['Drobe']['id'])));
						if($rate)
						{
							$drobe['Drobe']['rated_by_me']=array_merge(array("is_rated"=>1),$rate['Rate']);
							
						}
					}
					else $drobe['Drobe']['rated_by_me']=array("is_rated"=>0);
				}
			
				$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['Category']=$drobe['Category'];
				
				//make sure featured drobe functionality is enabled from admin
				$drobe['Drobe']['featured']= Configure::read('setting.featured_drobe')=="on" && ($drobe['Drobe']['featured']=="1" || $drobe['Drobe']['post_type']=="sell") && $drobe['Drobe']['buy_url']!="" ? "1":"0";
				if($drobe['Drobe']['buy_url']==null || Configure::read('setting.featured_drobe')!="on") $drobe['Drobe']['buy_url']="";
				
				if($drobe['SellDrobe']['sell_price']!=null)
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
				
				unset($drobe['Drobe']['file_name']);
				$response[]=$drobe['Drobe'];
					
			}

		}
		return $response;
	}
	
	/*
	 * Webservice for favourite drobe list
	*/
	function get_favourite($sort_by="most_recent",$user_id=null)
	{
		$response=array();
		// if listing only oter users drobes
		if($user_id==null && isset($this->data['user_id']) && $this->data['user_id'])
		{
			$user_id=$this->data['user_id'];
		}
		
		// set last_faved_on variable for fixing duplicate issue 
		$last_faved_on=0;
		if($user_id!=null)
		{
			$conditions=array('Drobe.rate_status'=>'open');
			
			$fav_conditions=array('Favourite.user_id'=>$user_id);
			if($sort_by=="most_recent")
			{
				// fixing issue of drobe repeat on next page;
				if(isset($this->data['last_faved_on']) && $this->data['last_faved_on']>0)
				{
					$last_faved_on=date('Y-m-d H:i:s',$this->data['last_faved_on']);
					if($last_faved_on!=null) $fav_conditions['Favourite.created_on <= ']=$last_faved_on;
					$last_faved_on=$this->data['last_faved_on'];
				}
				else
				{
					$latest=$this->Drobe->Favourite->find('first',array('conditions'=>array('user_id'=>$user_id),'order'=>"id DESC"));
					if($latest) $last_faved_on=strtotime($latest['Favourite']['created_on']);
				}
			}
			$drobe_ids=array_values($this->Drobe->Favourite->find('list',array("fields"=>array("Favourite.drobe_id"),'conditions'=>$fav_conditions)));
			$conditions['Drobe.id']=$drobe_ids;
			
			if(isset($this->data['search']) && $this->data['search']!="")
			{
				$keyword= preg_split("/[\s,]+/", $this->data['search']);
				foreach($keyword as $key)
				{
					$key = ltrim($key,'#');
					$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
					//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				}
			}
			
			
			if(isset($this->data['user_id']) && $this->data['user_id'])
			{
				// flaged drobes by uses
				//$flaged_drobes= array_values($this->Drobe->Flag->find('list',array('fields'=>array('Flag.drobe_id'),'conditions'=>array('Flag.user_id'=>$this->data['user_id']),'group'=>array('Flag.drobe_id'))));
				//if(count($flaged_ids))	$conditions['Drobe.id NOT']=$flaged_ids;
					
				//all rates given by user on drobes
				$rated_ids=array();
				$this->Drobe->Rate->recursive=-1;
				$rated_drobes=array_values($this->Drobe->Rate->find('list',array('fields'=>array('Rate.drobe_id'),'conditions'=>array('Rate.user_id'=>$this->data['user_id']),'group'=>array('Rate.drobe_id'))));
					
				//list of all my favourite
				$my_favs=array_values($this->Drobe->Favourite->find('list',array('fields'=>array('Favourite.drobe_id'),"conditions"=>array('Favourite.user_id'=>$this->data['user_id']))));
			
				// list of all my followings
				$my_followings=array_values($this->Drobe->User->Follower->find('list',array('fields'=>array('Follower.user_id'),"conditions"=>array('Follower.follower_id'=>$this->data['user_id']))));
				
			}
			
			//if category ids mentioned then need to listion only selected id's drobes
			if(isset($this->data['category_id']) && $this->data['category_id']!="" && $this->data['category_id']!="ALL")
			{
				$categories=explode(",",$this->data['category_id']);
				$conditions['Drobe.category_id']=$categories;
			}
			
			// if gender target mentioned
			if(isset($this->data['gender']) && in_array($this->data['gender'],array("male","female")))
			{
				//if($this->data['gender']=="female") $conditions['OR']['DrobeSetting.female']=1;
				//if($this->data['gender']=="male") $conditions['OR']['DrobeSetting.male']=1;
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$this->data['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$this->data['gender']."'  OR User.gender is null)))";
			}
		
			
			
			/*
			 * Started Haresh Code for adding new filters
			 */
			$only_sell_drobe=false;
			
			if(isset($this->data['price_range']) && $this->data['price_range']!="" )
			{
				$only_sell_drobe=true;
				//$conditions['SellDrobe.sell_price <=']=intval($this->data['max_price']);
				$conditions['AND']=$this->_parse_price_range($this->data['price_range']);
			}
			if(isset($this->data['min_price']) && intval($this->data['min_price'])>0 )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_price >=']=intval($this->data['min_price']);
			}
			if(isset($this->data['brand_name']) && $this->data['brand_name']!="" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']=explode("||",$this->data['brand_name']);;
			}
			if(isset($this->data['size_name']) && $this->data['size_name']!="" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']=explode("||",$this->data['size_name']);
			}
			if(isset($this->data['only_sell_items']) && $this->data['only_sell_items']=="1" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sold']="no";
			}
			if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="new" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.is_brand_new']="new";
			}
			if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="used" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.is_brand_new']="used";
			}
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']="sell";
			}
			/*
			 * End of Haresh for adding new filters
			 */
			
			//setting order of drobes in listing
			$order="";
			$joins=array();
			if($sort_by=="most_recent") 
			{
				/*
				 * When user is logged in the in most resent tab need to display unrated drobes first
				*/
				if(isset($this->data['user_id']) && $this->data['user_id'])
				{
					$joins=array(
							array(
									'table' => 'rates',
									'alias' => 'Rate',
									'type' => 'LEFT',
									'conditions' => array(
											'Rate.drobe_id = Drobe.id',
											'Rate.user_id' =>  $this->data['user_id']
									)
							)
					);
					$order="Rate.user_id ASC, Drobe.uploaded_on DESC";
				}
				else $order="Drobe.uploaded_on DESC";
			}
			
			if($sort_by=="most_rated") 
			{
				//$order="Drobe.total_rate DESC";
				//$order="Drobe.total_in/Drobe.total_rate DESC, Drobe.views DESC";
				//$order="Drobe.rate_index DESC, Drobe.views DESC";
				
				$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
				
			}
			if($sort_by=="most_viewed") $order="Drobe.views DESC";
			
			if($sort_by=="featured")
			{
				// added by haresh
				if(isset($this->data['highest_rated']) && $this->data['highest_rated']=="1")
				{
					$order=$this->drobe_season_order_query()."Drobe.rate_index DESC, Drobe.views DESC";
				}
				else
				{
					$order="Drobe.featured DESC, Drobe.order ASC,Drobe.id DESC";
				}
				// end of haresh
				$conditions[]=array("OR"=>array("Drobe.featured"=>1,"AND"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved")));
			}
			
			//removing unnecessory associations
			$this->Drobe->unbindModel(array('hasMany' => array("Rate","Flag","Favourite")));
			$this->Drobe->bindModel(array('hasOne' => array("SellDrobe")));
			
			$fields =array('SellDrobe.sell_price','SellDrobe.is_brand_new','SellDrobe.original_sell_price','SellDrobe.sell_description','SellDrobe.tracking_number','SellDrobe.sold','SellDrobe.size_name','SellDrobe.sell_brand_name','SellDrobe.sell_gender',
					'Drobe.id','Drobe.uploaded_on','Drobe.unique_id','Drobe.user_id','Drobe.file_name','Drobe.comment','Drobe.total_rate',
					'Drobe.views','Drobe.total_in','Drobe.total_out','Drobe.is_highest_rated',
					'Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs',
					'Category.id','Category.category_name',"User.first_name","User.last_name","User.username","User.photo");
			
			if($sort_by=="most_recent" && strpos($keyword[0],'#')!==false)
			{
				$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
				$order="has_tag DESC , ".$order;
			}
			
			$this->paginate = array(
					'fields'=>$fields,
					'limit' => 48,
					'order'=>$order,
					'conditions'=>$conditions,
					'joins'=>$joins,
					'group' => "Drobe.id"
			);
			
			$data = $this->paginate('Drobe');
			if($data)
			{
				$response['drobes']=array();
				
				foreach($data as $drobe)
				{
					// counting views of drobes
					$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
						
					/*$drobe['Drobe']['total_in']= $total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0;
					$drobe['Drobe']['total_out']= $total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0;*/
					$drobe['Drobe']['total_in']= $drobe['Drobe']['total_in'];
					$drobe['Drobe']['total_out']= $drobe['Drobe']['total_out'];
					$drobe['Drobe']['username'] =  $drobe['User']['username'];
					$drobe['Drobe']['first_name'] = $drobe['User']['first_name'];
					$drobe['Drobe']['last_name'] = $drobe['User']['last_name'];
					$drobe['Drobe']['profile_photo']="";
					if($drobe['User']['photo']!="") {
						$drobe['Drobe']['profile_photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$drobe['User']['photo'],true);
					}
					
					unset($drobe['User']);
					// counting favourite drobe
					$drobe['Drobe']['favourite']=$drobe['Drobe']['total_favs'];
						
					if(isset($this->data['user_id']) && $this->data['user_id']!="")
					{
						// is this drobe is my favourite?
						$drobe['Drobe']['my_favourite']=in_array($drobe['Drobe']['id'], $my_favs)?1:0;
						
						// is in my follwing list?
						$drobe['Drobe']['is_my_follower']=in_array($drobe['Drobe']['user_id'], $my_followings)?1:0;
						
			
						// is this drobe rated by me?
						if(in_array($drobe['Drobe']['id'],$rated_drobes))
						{
							$rate=$this->Drobe->Rate->find('first',array('fields'=>array('Rate.rate','Rate.comment','Rate.rewarded'),'conditions'=>array('Rate.user_id'=>$this->data['user_id'],'Rate.drobe_id'=>$drobe['Drobe']['id'])));
							if($rate)
							{
								$drobe['Drobe']['rated_by_me']=array_merge(array("is_rated"=>1),$rate['Rate']);
							}
						}
						else $drobe['Drobe']['rated_by_me']=array("is_rated"=>0);
					}

					$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
					$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
					$drobe['Drobe']['Category']=$drobe['Category'];
					
					//make sure featured drobe functionality is enabled from admin
					$drobe['Drobe']['featured']= Configure::read('setting.featured_drobe')=="on" && ($drobe['Drobe']['featured']=="1" || $drobe['Drobe']['post_type']=="sell") && $drobe['Drobe']['buy_url']!="" && $drobe['Drobe']['buy_url']!="" ? "1":"0";
					if($drobe['Drobe']['buy_url']==null || Configure::read('setting.featured_drobe')!="on") $drobe['Drobe']['buy_url']="";
					
					
					
					unset($drobe['Drobe']['file_name']);
					
					
					if($drobe['SellDrobe']['sell_price']!=null)
					{
						$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
						$drobe['Drobe']['original_sell_price']=($drobe['SellDrobe']['original_sell_price']==null)?"":$drobe['SellDrobe']['original_sell_price'];
						$drobe['Drobe']['drobe_for']=$drobe['SellDrobe']['sell_gender'];
						$drobe['Drobe']['is_brand_new']=($drobe['SellDrobe']['is_brand_new']=="")?"both":$drobe['SellDrobe']['is_brand_new'];
						$drobe['Drobe']['sell_brand_name']=($drobe['SellDrobe']['sell_brand_name']==null)?"":$drobe['SellDrobe']['sell_brand_name'];
						$drobe['Drobe']['size_name']=($drobe['SellDrobe']['size_name'] == null)?"":$drobe['SellDrobe']['size_name'];
						$drobe['Drobe']['sell_description']=$drobe['SellDrobe']['sell_description'];
						$drobe['Drobe']['tracking_number']=($drobe['SellDrobe']['tracking_number'] == null)?"":$drobe['SellDrobe']['tracking_number'];
						$drobe['Drobe']['sold']=$drobe['SellDrobe']['sold'];
					}
					
					$response['type']="success";
					$response['drobes'][]=$drobe['Drobe'];
					
				}
				if($last_faved_on>0)
				{
					$response['last_faved_on']=$last_faved_on;
				}
			
			}
			else
			{
				$response['type']="error";
				$response['message']="drobe not found";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide user id in url";
		}
		
	
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	/*
	 * Webservice for Highest rated favourite drobe list for v1.2 or above
	 */
	
	function get_favourite_highest_rated($page=1,$user_id=null)
	{
		
		$response=array();
		// if listing only oter users drobes
		if($user_id==null && isset($this->data['user_id']) && $this->data['user_id'])
		{
			$user_id=$this->data['user_id'];
		}
	
		// set last_faved_on variable for fixing duplicate issue
		$last_faved_on=0;
		if($user_id!=null)
		{
			$conditions=array('Drobe.rate_status'=>'open');
				
			$fav_conditions=array('Favourite.user_id'=>$user_id);
			
			$drobe_ids=array_values($this->Drobe->Favourite->find('list',array("fields"=>array("Favourite.drobe_id"),'conditions'=>$fav_conditions)));
			$conditions['Drobe.id']=$drobe_ids;
				
			if(isset($this->data['user_id']) && $this->data['user_id'])
			{
				// flaged drobes by uses
				//$flaged_drobes= array_values($this->Drobe->Flag->find('list',array('fields'=>array('Flag.drobe_id'),'conditions'=>array('Flag.user_id'=>$this->data['user_id']),'group'=>array('Flag.drobe_id'))));
				//if(count($flaged_ids))	$conditions['Drobe.id NOT']=$flaged_ids;
					
				//all rates given by user on drobes
				$rated_ids=array();
				$this->Drobe->Rate->recursive=-1;
				$rated_drobes=array_values($this->Drobe->Rate->find('list',array('fields'=>array('Rate.drobe_id'),'conditions'=>array('Rate.user_id'=>$this->data['user_id']),'group'=>array('Rate.drobe_id'))));
					
				//list of all my favourite
				$my_favs=array_values($this->Drobe->Favourite->find('list',array('fields'=>array('Favourite.drobe_id'),"conditions"=>array('Favourite.user_id'=>$this->data['user_id']))));
					
				// list of all my followings
				$my_followings=array_values($this->Drobe->User->Follower->find('list',array('fields'=>array('Follower.user_id'),"conditions"=>array('Follower.follower_id'=>$this->data['user_id']))));
	
			}

			/***	This code executed when search parameter is exist in search field.	**/
			if(isset($this->data['search']) && $this->data['search']!="")
			{
				$keyword= preg_split("/[\s,]+/", $this->data['search']);
				foreach($keyword as $key)
				{
					$key = ltrim($key,'#');
					$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
					//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				}
			}
			
			
			//if category ids mentioned then need to listion only selected id's drobes
			if(isset($this->data['category_id']) && $this->data['category_id']!="" && $this->data['category_id']!="ALL")
			{
				$categories=explode(",",$this->data['category_id']);
				$conditions['Drobe.category_id']=$categories;
			}
				
			// if gender target mentioned
			if(isset($this->data['gender']) && in_array($this->data['gender'],array("male","female")))
			{
				//if($this->data['gender']=="female") $conditions['OR']['DrobeSetting.female']=1;
				//if($this->data['gender']=="male") $conditions['OR']['DrobeSetting.male']=1;
				/*
				 * 
				 * This condition work if gender is mentioned if drobe is for sell then check for sell_gender field in sell_drobes table and if drobe for normal post(not for sell) then check gender for user in users table
				 * 
				 */
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$this->data['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$this->data['gender']."'  OR User.gender is null)))";
			}
			
			
			/*
			 * Started Haresh Code for adding new filters
			 */
			$only_sell_drobe=false;
			
			if(isset($this->data['price_range']) && $this->data['price_range']!="" )
			{
				$only_sell_drobe=true;
				//$conditions['SellDrobe.sell_price <=']=intval($this->data['max_price']);
				$conditions['AND']=$this->_parse_price_range($this->data['price_range']);
			}
			if(isset($this->data['min_price']) && intval($this->data['min_price'])>0 )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_price >=']=intval($this->data['min_price']);
			}
			if(isset($this->data['brand_name']) && $this->data['brand_name']!="" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']=explode("||",$this->data['brand_name']);;
			}
			if(isset($this->data['size_name']) && $this->data['size_name']!="" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']=explode("||",$this->data['size_name']);
			}
			if(isset($this->data['only_sell_items']) && $this->data['only_sell_items']=="1" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sold']="no";
			}
			if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="new" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.is_brand_new']="new";
			}
			else if(isset($this->data['only_new_brand']) && $this->data['only_new_brand']=="used" )
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.is_brand_new']="used";
			}
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']="sell";
			}
			/*
			 * End of Haresh for adding new filters
			 */
				
				
			//setting order of drobes in listing
			//$order="";
			//$order="Drobe.rate_index DESC, Drobe.views DESC";
				
			$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
			
			//removing unnecessory associations
			$this->Drobe->unbindModel(array('hasMany' => array("Rate","Flag","Favourite")));
			$this->Drobe->bindModel(array('hasOne' => array("SellDrobe")));

			$fields =array('SellDrobe.sell_price','SellDrobe.is_brand_new','SellDrobe.original_sell_price','SellDrobe.sell_description','SellDrobe.tracking_number','SellDrobe.sold','SellDrobe.size_name','SellDrobe.sell_brand_name','SellDrobe.sell_gender',
					'Drobe.id','Drobe.uploaded_on','Drobe.unique_id','Drobe.user_id','Drobe.file_name','Drobe.comment','Drobe.total_rate',
					'Drobe.views','Drobe.total_in','Drobe.total_out','Drobe.is_highest_rated',
					'Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs',
					'Category.id','Category.category_name',"User.first_name","User.last_name","User.username","User.photo");
				
			if(strpos($keyword[0],'#')!==false)
			{
				$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
				$order="has_tag DESC , ".$order;
			}
			
			$data = $this->Drobe->find('all',array(
					'fields'=>$fields,
					'limit' => 48*$page,
					'order'=>$order,
					'conditions'=>$conditions,
					'group' => "Drobe.id"
			));
			if($data)
			{
				$response['drobes']=array();
	
				foreach($data as $drobe)
				{
					// counting views of drobes
					$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
	
					/*$drobe['Drobe']['total_in']= $total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0;
					$drobe['Drobe']['total_out']= $total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0;*/
					$drobe['Drobe']['total_in']= $drobe['Drobe']['total_in'];
					$drobe['Drobe']['total_out']= $drobe['Drobe']['total_out'];
					$drobe['Drobe']['username'] =  $drobe['User']['username'];
					$drobe['Drobe']['first_name'] = $drobe['User']['first_name'];
					$drobe['Drobe']['last_name'] = $drobe['User']['last_name'];
					$drobe['Drobe']['profile_photo']="";
					if($drobe['User']['photo']!="") {
						$drobe['Drobe']['profile_photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$drobe['User']['photo'],true);
					}
					
					unset($drobe['User']);
					// counting favourite drobe
					$drobe['Drobe']['favourite']=$drobe['Drobe']['total_favs'];
	
					if(isset($this->data['user_id']) && $this->data['user_id']!="")
					{
						// is this drobe is my favourite?
						$drobe['Drobe']['my_favourite']=in_array($drobe['Drobe']['id'], $my_favs)?1:0;
	
						// is in my follwing list?
						$drobe['Drobe']['is_my_follower']=in_array($drobe['Drobe']['user_id'], $my_followings)?1:0;
	
							
						// is this drobe rated by me?
						if(in_array($drobe['Drobe']['id'],$rated_drobes))
						{
							$rate=$this->Drobe->Rate->find('first',array('fields'=>array('Rate.rate','Rate.comment','Rate.rewarded'),'conditions'=>array('Rate.user_id'=>$this->data['user_id'],'Rate.drobe_id'=>$drobe['Drobe']['id'])));
							if($rate)
							{
								$drobe['Drobe']['rated_by_me']=array_merge(array("is_rated"=>1),$rate['Rate']);
							}
						}
						else $drobe['Drobe']['rated_by_me']=array("is_rated"=>0);
					}
	
					$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
					$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
					$drobe['Drobe']['Category']=$drobe['Category'];
						
					//make sure featured drobe functionality is enabled from admin
					$drobe['Drobe']['featured']= Configure::read('setting.featured_drobe')=="on" && ($drobe['Drobe']['featured']=="1" || $drobe['Drobe']['post_type']=="sell") && $drobe['Drobe']['buy_url']!="" && $drobe['Drobe']['buy_url']!="" ? "1":"0";
					if($drobe['Drobe']['buy_url']==null || Configure::read('setting.featured_drobe')!="on") $drobe['Drobe']['buy_url']="";
						
						
					unset($drobe['Drobe']['file_name']);
					
					if($drobe['SellDrobe']['sell_price']!=null)
					{
						$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
						$drobe['Drobe']['original_sell_price']=$drobe['SellDrobe']['original_sell_price'];
						$drobe['Drobe']['drobe_for']=$drobe['SellDrobe']['sell_gender'];
						$drobe['Drobe']['is_brand_new']=($drobe['SellDrobe']['is_brand_new']=="")?"both":$drobe['SellDrobe']['is_brand_new'];
						$drobe['Drobe']['sell_brand_name']=$drobe['SellDrobe']['sell_brand_name'];
						$drobe['Drobe']['size_name']=($drobe['SellDrobe']['size_name'] == null)?"":$drobe['SellDrobe']['size_name'];
						$drobe['Drobe']['sell_description']=$drobe['SellDrobe']['sell_description'];
						$drobe['Drobe']['tracking_number']=($drobe['SellDrobe']['tracking_number'] == null)?"":$drobe['SellDrobe']['tracking_number'];
						$drobe['Drobe']['sold']=$drobe['SellDrobe']['sold'];
					}
						
					$response['type']="success";
					$response['drobes'][]=$drobe['Drobe'];
				}
				if($last_faved_on>0)
				{
					$response['last_faved_on']=$last_faved_on;
				}
					
			}
			else
			{
				$response['type']="error";
				$response['message']="drobe not found";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide user id in url";
		}
	
	
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function userdrobe($sort_by="recent",$user_id=null,$latest_drobe="0")
	{
		$userData=$this->Drobe->User->find('first',array('order'=>$order,'conditions'=>array("User.unique_id"=>$user_id)));
		
		$conditions=array(
				'Drobe.rate_status'=>'open',
				'Drobe.user_id'=>$userData['User']['id']
		);
		
		//setting order of drobes in listing
		$order="";
		$joins=array();
		if($sort_by=="recent") 
		{
			//fixing issue of drobe repeat on next page;
			if($latest_drobe != "0")
			{
				$latest_drobe_id=$this->Drobe->field('id',array('Drobe.unique_id'=>$latest_drobe));
				if($latest_drobe_id>0) $conditions['Drobe.id <= ']=$latest_drobe_id;				
			}
			
			/*
			 * When user is logged in the in most resent tab need to display unrated drobes first
			*/
			if($this->Auth->user('id')>0)
			{
				$joins=array(
						array(
								'table' => 'rates',
								'alias' => 'Rate',
								'type' => 'LEFT',
								'conditions' => array(
										'Rate.drobe_id = Drobe.id',
										'Rate.user_id' => $this->Auth->user('id')
								)
						)
				);
				$order="Rate.user_id ASC,Drobe.uploaded_on DESC";
			}
			else $order="Drobe.uploaded_on DESC";
		}
		
		
		if($sort_by=="rated")
		{
			$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		}
		if($sort_by=="viewed") $order="Drobe.views DESC";
		
		if($sort_by=="featured")
		{
			$order="Drobe.featured DESC, Drobe.order ASC,Drobe.id DESC";
			$conditions[]=array("OR"=>array("Drobe.featured"=>1,"AND"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved")));
		}
		
		if($this->Auth->user('id')>0)
		{
		
			$this->loadModel('UserFilter');
			
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
			
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}  
			
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."'  OR User.gender is null)))";
			}
			
			
			
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new") 
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
			
			
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
		}
		
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
		
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
		}
		
		$fields =array('Drobe.*','SellDrobe.*','User.id','User.unique_id');
		
		if($sort_by=="recent" && strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		if($userData)
		{
			$this->set('is_me',$userData['User']['id']==$this->Auth->user('id'));
			$this->Drobe->unbindModel(array('hasOne'=>array("DrobeSetting"),
					'hasMany'=> array("Rate","Flag","Favourite")));
			$this->paginate = array(
					'fields'=>$fields,
					'limit' => 24,
					'order'=>$order,
					'conditions'=>$conditions,
					'joins'=>$joins
					
			);
			$data = $this->paginate('Drobe');
			$this->set(compact('data'));
		}
	}
	
	
	
	function userdrobe_highest_rated($page=1,$user_id=null)
	{
		$userData=$this->Drobe->User->find('first',array('order'=>$order,'conditions'=>array("User.unique_id"=>$user_id)));
	
		$conditions=array(
				'Drobe.rate_status'=>'open',
				'Drobe.user_id'=>$userData['User']['id']
		);
		//setting order of drobes in listing
		//$order="";
		//$order="Drobe.rate_index DESC, Drobe.views DESC";
		
		$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		
		if($this->Auth->user('id')>0)
		{
		$this->loadModel('UserFilter');
			
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
			
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}  
			
			
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."'  OR User.gender is null)))";
			}
			
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new") 
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
			
			
			
				
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
		}
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
		
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
		}
		
		
		$fields =array('Drobe.*','SellDrobe.*','User.id','User.unique_id');
		
		if(strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
	
		
		if($userData)
		{
			$this->set('is_me',$userData['User']['id']==$this->Auth->user('id'));
			$this->Drobe->unbindModel(array('hasOne'=>array("DrobeSetting"),
					'hasMany'=> array("Rate","Flag","Favourite")));
			$data =  $this->Drobe->find('all',array(
					'fields'=>$fields,
					'limit' => 24*$page,
					'offset'=>0,
					'order'=>$order,
					'conditions'=>$conditions,
			));
			$this->set(compact('data'));
		}
	}
	function admin_userdrobe($sort_by="recent",$user_id=null)
	{
		//setting order of drobes in listing
		$order="";
		if($sort_by=="most_recent") $order="Drobe.uploaded_on DESC";
		if($sort_by=="most_rated") 
		{
			//$order="Drobe.total_rate DESC";
			//$order="Drobe.total_in/Drobe.total_rate DESC, Drobe.views DESC";
			$order="Drobe.rate_index DESC, Drobe.views DESC";
		}
		if($sort_by=="most_viewed") $order="Drobe.views DESC";
	
		$userData=$this->Drobe->User->find('first',array('order'=>$order,'conditions'=>array("User.id"=>$user_id)));
		if($userData)
		{
			$this->paginate = array(
					'fields'=>array('Drobe.id','Drobe.unique_id','Drobe.file_name','Drobe.comment'),
					'limit' => 48,
					'order'=>$order,
					'conditions'=>array('Drobe.user_id'=>$userData['User']['id'],'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')
			);
			$data = $this->paginate('Drobe');
			$this->set(compact('data'));
		}
	}
	function admin_detail($drobe_id=null)
	{
		$this->helpers[]="GoogleChart";
		$date_day=array();
		for($i=0;$i<10;$i++)
		{
		$curr_date=date('Y-m-d',strtotime("-".$i." day"));
		$date_day[$curr_date][0]=array('total'=>0,'date_day'=>$curr_date);
		}
		
		$rateObj->Rate->recursive=-1;
		$dayWiseRates=$this->Drobe->Rate->find('all',array(
				"fields"=>array("count(*) as total","date_format(Rate.created_on,'%Y-%m-%d') as date_day"),
				"conditions"=>array('Rate.drobe_id'=>$drobe_id,"Rate.rate !="=>0,'Rate.created_on >= '=>date('Y-m-d',strtotime('-9 day'))),
				"order"=>"date_day DESC",
				"group"=>"date_day"));
		
		$tmp_day=$date_day;
		foreach($dayWiseRates as $drobe)
		{
			$tmp_day[$drobe[0]['date_day']]=$drobe;
		}
		
		$dayWiseRates=array_values($tmp_day);
		
		$this->set('rate_stats',$dayWiseRates);
		
		$this->Drobe->bindModel(array('belongsTo'=>array('User','Category')));
		$drobe=$this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobe_id)));
		$this->set('drobe',$drobe);
		
	}
	function admin_edit($drobe_id=null)
	{
	
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categories',Cache::read('drobe_category'));
		
		
		// loading size list from cache
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		$sizeList = Cache::read('size_list');
		$this->set('sizeList',$sizeList);
		
		
		// loading brand list from cache @sadikhasan
		if(Cache::read('brand_list')==null)
		{
			$this->loadModel('Brand');
			$this->Brand->_updateBrandCache();
		}
		$brandList = Cache::read('brand_list');
		$this->set('brandList',$brandList);
		
		if(!empty($this->data))
		{
			/*
			 * Old condition
			 * if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']=='')
			 * 
			 */
			if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']==$this->data['old_file'])
			{
				$drobeData=$this->data;
				unset($drobeData['Drobe']['file_name']);
			}
			else
			{
				if(isset($this->data['old_file']) && $this->data['old_file']!='')
				{
					if(file_exists(WWW_ROOT. Configure::read('drobe.upload_dir').DS.$this->data['old_file']))
					{
						unlink(WWW_ROOT. Configure::read('drobe.upload_dir').DS.$this->data['old_file']);
					}
					if(file_exists(WWW_ROOT. Configure::read('drobe.iphone.upload_dir').DS.$this->data['old_file']))
					{
						unlink(WWW_ROOT. Configure::read('drobe.iphone.upload_dir').DS.$this->data['old_file']);
					}
					if(file_exists(WWW_ROOT. Configure::read('drobe.thumb.upload_dir').DS.$this->data['old_file']))
					{
						unlink(WWW_ROOT. Configure::read('drobe.thumb.upload_dir').DS.$this->data['old_file']);
					}
					if(file_exists(WWW_ROOT. Configure::read('drobe.iphone.thumb.upload_dir').DS.$this->data['old_file']))
					{
						unlink(WWW_ROOT. Configure::read('drobe.iphone.thumb.upload_dir').DS.$this->data['old_file']);
					}
				}
				$this->_cropDrobe();
				$drobeData=$this->data;
			}
			
			$this->Drobe->id=$this->data['Drobe']['id'];
			
			if($drobeData['Drobe']['post_type']=="sell")
			{
				if(isset($this->data['SellDrobe']['id']) && $this->data['SellDrobe']['id']>0)
				{
					$this->Drobe->SellDrobe->id=$this->data['SellDrobe']['id'];
					$drobeData['SellDrobe']['status']='approved';
				}
				
				//@sadikhasan
				$drobeData['SellDrobe']['size_name'] = $sizeList[$this->data['SellDrobe']['size_id']];  // For getting size_name from cache depends on selected index
				$drobeData['SellDrobe']['sell_brand_name'] = $brandList[$this->data['SellDrobe']['brand_id']];  // For getting brand_name from cache depends on selected index
			} 
			else
			{
				unset($drobeData['SellDrobe']);
			}
			
			if($this->Drobe->saveAll($drobeData))
			{
				//$this->add_drobe_to_shopify($drobe_id);
				$updateMainImage="";
					
				if(isset($drobeData['Drobe']['file_name']) && $drobeData['Drobe']['file_name']!='')
				{
					$updateMainImage=$drobeData['Drobe']['file_name'];
				}
					
				$this->add_drobe_to_shopify($this->Drobe->id,$updateMainImage);
				$this->Session->setFlash("Drobe question updated successfully","default",array("class"=>"success"));
				$this->redirect(array("controller"=>"drobes","action"=>"detail","admin"=>true,$this->data['Drobe']['id']));
			}
			else
			{
				$this->Session->setFlash("Drobe question updated successfully","default",array("class"=>"error"));
			}
		}
		else
		{
			$this->Drobe->id=$drobe_id;
			$this->data=$this->Drobe->read();
		}
	}
	
	function admin_list()
	{
		$this->helpers[]='Time';
		$conditions=array("Drobe.deleted"=>0,"Drobe.rate_status"=>"open");
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['Drobe.id']=$search;
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['User.email LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
		}
		$this->paginate = array(
			'limit' => 10,
			'order' => "Drobe.uploaded_on DESC",
			'conditions'=>$conditions
		);
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	
	function admin_sell_drobes()
	{
		$conditions=array('Drobe.post_type'=>"sell",'SellDrobe.status'=>'approved',"Drobe.deleted"=>0,"Drobe.rate_status"=>"open");
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.id']=$search;
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['SellDrobe.sell_brand_name LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
		}
		$this->Drobe->bindModel(array('belongsTo'=>array('User','Category')));
		
		/* $this->request->params['named']['page'] =1; */
		$this->paginate = array(
				'fields'=>array('Drobe.id','Drobe.comment','Drobe.file_name','Drobe.uploaded_on','Drobe.post_type','SellDrobe.*','Drobe.featured','Drobe.buy_url_actual','Drobe.rate_status','User.id','User.first_name','User.username','User.last_name','Category.category_name','Drobe.total_in/(Drobe.total_in+Drobe.total_out) as percent_in','Drobe.total_out/(Drobe.total_in+Drobe.total_out) as percent_out'),
				'limit' => 10,
				'page'=>1,
				'order' => "Drobe.uploaded_on DESC",
				'conditions'=>$conditions
		);
		
		$drobes = $this->paginate('Drobe');
		
		// Remove this query because no need to pending when new drobe uploaded or remove from admin side.
		/*$pending_drobes= $this->Drobe->find('count',array('conditions'=>array('Drobe.post_type'=>"sell",'SellDrobe.status'=>'pending','Drobe.deleted'=>0,"OR"=>array(array('Drobe.rate_status'=>'open'),
				array('Drobe.rate_status'=>'close','SellDrobe.shopify_product_id >'=>0))))); */
		
		$remove_request= $this->Drobe->find('count',array('conditions'=>array('Drobe.rate_status'=>"close","SellDrobe.status"=>"pending",'Drobe.post_type'=>"sell",'Drobe.deleted'=>0)));
		$this->set(compact('drobes','pending_drobes','remove_request'));
	}
	
	function admin_sell_drobe_request()
	{
		$conditions=array('Drobe.post_type'=>"sell",'SellDrobe.status'=>'pending','Drobe.deleted'=>0,"OR"=>array(array('Drobe.rate_status'=>'open'),
				array('Drobe.rate_status'=>'close','SellDrobe.shopify_product_id >'=>0)));
		
		$this->Drobe->bindModel(array('belongsTo'=>array('User','Category')));
		$this->paginate = array(
				'fields'=>array('Drobe.id','Drobe.comment','Drobe.file_name','Drobe.uploaded_on','Drobe.post_type','SellDrobe.*','Drobe.featured','Drobe.buy_url_actual','Drobe.rate_status','User.id','User.first_name','User.username','User.last_name','Category.category_name','Drobe.total_in/(Drobe.total_in+Drobe.total_out) as percent_in','Drobe.total_out/(Drobe.total_in+Drobe.total_out) as percent_out'),
				'limit' => 10,
				'order' => "Drobe.uploaded_on DESC",
				'conditions'=>$conditions
		);
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	function admin_remove_drobe_request()
	{
		$conditions=array('Drobe.post_type'=>"sell",'Drobe.rate_status'=>'close','SellDrobe.status'=>"pending",'Drobe.deleted'=>0);
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['SellDrobe.sell_brand_name LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
			$conditions['OR']['User.first_name LIKE']=$search;
			$conditions['OR']['User.last_name LIKE']=$search;
			$conditions['OR']['User.username LIKE']="@".$search;
		}
		$this->Drobe->bindModel(array('belongsTo'=>array('User','Category')));
		$this->paginate = array(
				'fields'=>array('Drobe.id','Drobe.comment','Drobe.file_name','Drobe.uploaded_on','Drobe.post_type','SellDrobe.*','Drobe.featured','Drobe.buy_url_actual','Drobe.rate_status','User.id','User.first_name','User.last_name','User.username','Category.category_name','Drobe.total_in/(Drobe.total_in+Drobe.total_out) as percent_in','Drobe.total_out/(Drobe.total_in+Drobe.total_out) as percent_out'),
				'limit' => 10,
				'order' => "Drobe.uploaded_on DESC",
				'conditions'=>$conditions
		);
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	
	function admin_show_list()
	{
		$this->helpers[]='Time';
		$this->paginate = array(
				'fields'=>array('Drobe.id','Drobe.comment','Drobe.file_name','Drobe.uploaded_on','Drobe.views','Drobe.total_rate','Drobe.total_in','Drobe.total_out','Drobe.rate_index','User.id','User.first_name','User.last_name','User.username','Category.category_name','Drobe.total_favs','Drobe.total_in/(Drobe.total_in+Drobe.total_out) as percent_in','Drobe.total_out/(Drobe.total_in+Drobe.total_out) as percent_out'),
				'limit' => 20,
				'conditions' =>array('Drobe.deleted'=>0),
				'order' => "Drobe.uploaded_on DESC",
				'group' =>"Drobe.id"
		);
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	
	function admin_featured_drobe()
	{
		$conditions=array('Drobe.featured'=>1,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open');
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.id']=$search;
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
		}
		$this->paginate = array(
				'limit' => 10,
				'order' => "Drobe.order ASC",
				'conditions'=>$conditions
		);
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	
	
	function admin_mark_sale($drobe_id=null)
	{
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categories',Cache::read('drobe_category'));
		
		// loading size list from cache
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		$this->set('sizeList',Cache::read('size_list'));
		
		
		if(!empty($this->data))
		{
			$this->Drobe->id=$this->data['Drobe']['id'];
			$this->Drobe->SellDrobe->id=$this->data['SellDrobe']['id'];
			
			$drobeData=$this->data;
			
			//custome size name as per id
			if(isset($drobeData['SellDrobe']['size_name']))
			{
				$drobeData['SellDrobe']['size_name']=$drobeData['SellDrobe']['size_name'];
			}
			elseif(isset($drobeData['SellDrobe']['size_id']))
			{
				$sizeNames=Cache::read('size_list');
				if(empty($sizeNames))
				{
					$this->loadModel('Size');
					$this->Size->_updateSizeCache();
					$sizeNames=Cache::read('size_list');
				}
				$drobeData['SellDrobe']['size_name']=$sizeNames[$drobeData['SellDrobe']['size_id']];
			}
			unset($drobeData['SellDrobe']['size_id']);
			/************************************/
			
			if($this->Drobe->saveAll($drobeData))
			{
				$this->Drobe->SellDrobe->saveField('sold','no');
				
				$this->Drobe->recursive=-1;
				$last_order=$this->Drobe->find("first",array("fields"=>array("Drobe.order"),"conditions"=>array("Drobe.featured"=>1),"order"=>array("Drobe.order DESC")));
				$updateFields['Drobe.order']=intval($last_order['Drobe']['order'])+1;
				$updateFields['Drobe.post_type']="'sell'";
				$updateFields['Drobe.featured']="'1'";
				
				/* When admin can do mark as sell then update feature=1 post_type=sell order=order+1 */
				$this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id));
				
				$this->Session->setFlash("Drobe marked for Sale successfully.","default",array("class"=>"success"));
				$this->redirect(array("controller"=>"drobes","action"=>"sell_drobes","admin"=>true,$this->data['Drobe']['id']));
			}
			else
			{
				$this->Session->setFlash("Error occured in mark as sale. please try again later","default",array("class"=>"error"));
			}
		}
		else
		{
			$this->Drobe->id=$drobe_id;
			$this->data=$this->Drobe->read();
		}
	}
	
	function admin_flag()
	{
		$conditions=array("Drobe.deleted"=>0);
		$this->Drobe->unbindModel(array('hasMany'=>array('Rate','Favourite'),'hasOne'=>array('DrobeSetting')));
		$this->Drobe->User->unbindModel(array('hasMany'=>array("Drobe","Rate","Follower","StreamCategory")));
		$this->Drobe->recursive=3;
		$flagged_drobes=$this->Drobe->Flag->find('list',array('fields'=>array('Flag.drobe_id'),"group"=>"Flag.drobe_id"));
		$conditions=array('Drobe.id'=>$flagged_drobes,"Drobe.deleted"=>0,"Drobe.rate_status"=>'open');
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['Drobe.id']=$search;
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
			$conditions['OR']['User.first_name LIKE']=$search;
			$conditions['OR']['User.last_name LIKE']=$search;
			$conditions['OR']['User.username LIKE']="@".$search;
		}
		$this->paginate = array(
				'limit' => 10,
				'order' => "Drobe.uploaded_on DESC",
				'conditions'=>$conditions
		);
		
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	
	function admin_action($action=null,$drobe_id=null)
	{
		$this->Drobe->id=$drobe_id;
		
		//get user id from drobe table for update total of uploader
		$user_id=$this->Drobe->field('user_id',array('Drobe.id'=>$drobe_id));
			
		if($action=="remove")
		{
			if($this->Drobe->deleteAll(array('Drobe.id'=>$drobe_id),true))
			{
				// update users total
				$this->Drobe->User->updateUserTotalField($user_id,'total_drobes',-1);
				
				$this->Drobe->updateRecentCache(); 
				$this->_removeNotifyFollowers($drobe_id);
				$this->_removeDrobeRates($drobe_id);
				$this->Session->setFlash("Selected drobe removed successfully");
				return $this->redirect($this->referer());
			}
		}
		if($action=="delete")
		{
			if($this->Drobe->updateAll(array('Drobe.deleted'=>1,'Drobe.rate_status'=>"'close'"),array('Drobe.id'=>$drobe_id)))
			{	/*
				// update users total
				$this->_deleteDrobeRates($drobe_id);
				$this->Drobe->User->resetUserTotalFields($user_id);
				
				// Reset Badge Counter for follower when admin delete any drobe. 
				$this->loadModel('NewDrobe');
				$this->NewDrobe->deleteAll(array('NewDrobe.drobe_id' => $drobe_id));
				
				//Remove badges of follower when drobe is deleted
				$this->_removeNotifyFollowers($drobe_id);
				
				$this->Drobe->updateRecentCache();
				
				//code by devang also remove from shopify
				$SellDrobeData=$this->Drobe->SellDrobe->findByDrobeId($drobe_id);
				$SellDrobeData=$SellDrobeData['SellDrobe'];
				if($SellDrobeData['shopify_product_id']>0)
				{
					$response=$this->shopify("/products/".$SellDrobeData['shopify_product_id'], "","DELETE");
					if(count($response)==0)
					{
						// Removing tracking id from everdrobe database
							
						$this->Drobe->SellDrobe->id=$SellDrobeData['id'];
						$this->Drobe->SellDrobe->saveField('shopify_product_id',0);
						$this->Drobe->SellDrobe->SellDrobeImage->updateAll(array('shopify_image_id'=>0),array('sell_drobe_id'=>$SellDrobeData['id']));
							
						$this->Session->setFlash("Selected product is deleted on Everdrobe Shop","default",array("class"=>"success"));
					}
					else
					{
						$this->Session->setFlash("Error occured in delete product in everdrobe shop","default",array("class"=>"error"));
					}
				}
				
				//deleted whole deleted drobe and its images and sell drobe details + images 
				$this->Drobe->recursive=-1;
				$drobes=$this->Drobe->find('list',array("fields"=>array("id","file_name"),"conditions"=>array("Drobe.deleted"=>"1")));
						
				$drobeIDS = array();
				foreach($drobes as $key=>$value)
				{
					$drobeIDS[]=$key;
					$dir=WWW_ROOT."drobe_images/";
					unlink($dir.$value);
					$dir=WWW_ROOT."drobe_images/thumb/";
					unlink($dir.$value);
					$dir=WWW_ROOT."drobe_images/iphone/";
					unlink($dir.$value);
					$dir=WWW_ROOT."drobe_images/iphone/thumb/";
					unlink($dir.$value);
				}
				$this->Drobe->deleteAll(array('Drobe.id'=>$drobeIDS),false);
				$this->loadModel("DrobeSetting");
				$this->DrobeSetting->deleteAll(array('DrobeSetting.drobe_id'=>$drobeIDS),false);
				
				$this->loadModel("EbxTransaction");
				$this->EbxTransaction->deleteAll(array('EbxTransaction.drobe_id'=>$drobeIDS),false);
				$this->loadModel("Favourite");
				$this->Favourite->deleteAll(array('Favourite.drobe_id'=>$drobeIDS),false);
				$this->loadModel("Flag");
				$this->Flag->deleteAll(array('Flag.drobe_id'=>$drobeIDS),false);
				$this->loadModel("HighestRatedDrobe");
				$this->HighestRatedDrobe->deleteAll(array('HighestRatedDrobe.drobe_id'=>$drobeIDS),false);
				$this->loadModel("NewDrobe");
				$this->NewDrobe->deleteAll(array('NewDrobe.drobe_id'=>$drobeIDS),false);
				$this->loadModel("Rate");
				$this->Rate->deleteAll(array('Rate.drobe_id'=>$drobeIDS),false);
				$this->loadModel("SoldDrobe");
				$this->SoldDrobe->deleteAll(array('SoldDrobe.drobe_id'=>$drobeIDS),false);
				$this->loadModel("SellDrobe");
				$this->loadModel("SellDrobeImage");
				
				$this->SellDrobe->recursive=-1;
				$sel_drobes_id=$this->SellDrobe->find('list',array("conditions"=>array("SellDrobe.drobe_id"=>$drobeIDS)));
				$sell_drobes=$this->SellDrobeImage->find('list',array("fields"=>array("id","file_name"),"conditions"=>array("SellDrobeImage.sell_drobe_id"=>$sel_drobes_id)));
				
				foreach($sell_drobes as $key=>$selldrobe)
				{
					$dir=WWW_ROOT."drobe_images/additional/";
					unlink($dir.$selldrobe);
					$dir=WWW_ROOT."drobe_images/additional/thumb/";
					unlink($dir.$selldrobe);
				}
				
				$this->SellDrobe->deleteAll(array('SellDrobe.id'=>$sel_drobes_id),false);
				$this->SellDrobeImage->deleteAll(array('SellDrobeImage.sell_drobe_id'=>$sel_drobes_id),false);*/
				
				$this->Session->setFlash("Selected drobe removed successfully");
				return $this->redirect($this->referer());
			}
		}
		else if($action=="open")
		{
			if($this->Drobe->saveField('rate_status',"open"))
			{
				// update users total
				$this->Drobe->User->updateUserTotalField($user_id,'total_drobes',1);
				$this->Drobe->updateRecentCache();
				$this->Session->setFlash("Selected drobe is now opened for user rate");
				return $this->redirect($this->referer());
			}	
		}
		else if($action=="close")
		{
			$updateFields=array('Drobe.rate_status'=>"'close'","Drobe.new_response"=>0);
			$this->Drobe->recursive = -1;
			$drobeData = $this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobe_id)));
			
			/* Change post type from sell to post if drobe is for sale. */
			if($drobeData['Drobe']['post_type']=='sell')
			{
				$updateFields['Drobe.post_type']="'post'";
			}
			
			if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
			{
				// update users total
				$this->Drobe->User->updateUserTotalField($user_id,'total_drobes',-1);
				
				
				// cloading all conversations and updateing badges
				$this->loadModel('Rate');
				//$updateFields=array('Rate.conversation_id'=>"''",'Rate.conversation_status'=>"'close'",'Rate.new_reply'=>0,'Rate.new_response'=>0);
				$updateFields=array('Rate.conversation_status'=>"'close'",'Rate.new_reply'=>0,'Rate.new_response'=>0);
				$this->Rate->updateAll($updateFields,array('Rate.drobe_id'=>$drobe_id));
				
				
				$this->Drobe->updateRecentCache();
				
				$this->_removeNotifyFollowers($drobe_id);
				//$this->_removeDrobeConversations($drobe_id);
				$this->Session->setFlash("Selected drobe is now closed for user rate");
				return $this->redirect($this->referer());
			}
		}
		else if($action=="mark_featured")
		{
			$response=array();
			$updateFields=array();
			$updateFields['Drobe.featured']=1;
			
			/*If drobe is not a for sale drobe, then ask for URL (or URL can be added from Featured drobes section). */
			$actual_url=$this->Drobe->field('buy_url_actual',array('id'=>$drobe_id));
			if($actual_url!=trim($this->data['buy_url_actual']))
			{
				$updateFields['Drobe.buy_url_actual']="'".trim($this->data['buy_url_actual'])."'";
				$updateFields['Drobe.buy_url']="'".$this->makeTinyUrl(trim($this->data['buy_url_actual']))."'";
			}
			
			/*Put the drobe in "featured drobes" section to the top of the featured sort order.*/
			$this->Drobe->recursive=-1;
			$last_order=$this->Drobe->find("first",array("fields"=>array("Drobe.order"),"conditions"=>array("Drobe.featured"=>1),"order"=>array("Drobe.order ASC")));
			$updateFields['Drobe.order']=intval($last_order['Drobe']['order'])-1;
			
			if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
			{
				$this->Session->setFlash("Selected drobe is maked as featured successfully");
			}
			else
			{
				$this->Session->setFlash("Error occured in mark as featured");
			}
		}
		else if($action=="unmark_featured")
		{
			$response=array();
			$updateFields=array();
			$updateFields['Drobe.featured']=0;
			$updateFields['Drobe.order']=null;
			
			$this->Drobe->recursive =-1;
			$drobe_data = $this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobe_id)));
			
			/* If drobe is not a for sale drobe, then remove URL, remove the drobe from "featured drobes" section. */
			if($drobe_data['Drobe']['post_type']!='sell')
			{
				$updateFields['Drobe.buy_url_actual']="''";
				$updateFields['Drobe.buy_url']="''";
			}
			if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
			{
				$this->Session->setFlash("Selected drobe is unmarked as featured drobe");
			}
			else
			{
				$this->Session->setFlash("Error occured in unmarking as featured drobe");
			}
			return $this->redirect($this->referer());
		}
		else if($action=="change_link")
		{
			$response=array();
			$updateFields=array();
			$actual_url=$this->Drobe->field('buy_url_actual',array('id'=>$drobe_id));
			if($actual_url!=strtolower($this->data['buy_url_actual']))
			{
				$updateFields['Drobe.buy_url_actual']="'".strtolower($this->data['buy_url_actual'])."'";
				$updateFields['Drobe.buy_url']="'".$this->makeTinyUrl(strtolower($this->data['buy_url_actual']))."'";
			}
			if(count($updateFields))
			{
				if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
				{
					$response["type"]="success";
					$response["message"]="Buy-it link changed successfully";
				}
				else
				{
					$response["type"]="error";
					$response["message"]="Error occured in chnage buy-it link";
				}
			}
			else 
			{
				$response["type"]="error";
				$response["message"]="Update is not affected because there is no changes in link";
			}
			echo json_encode($response);
			exit();
		}
		else if($action=="unflag")
		{
			
			if($this->Drobe->Flag->deleteAll(array('Flag.drobe_id'=>$drobe_id),false))
			{
				$this->Session->setFlash("Selected drobe is unflagged successfully");
			}
			else
			{
				$this->Session->setFlash("Error occured in make unflagged drobe");
			}
			return $this->redirect($this->referer());
		}
		else if($action=="unmark_sale")
		{
			$updateFields=array();
			$updateFields['Drobe.post_type']="'post'";
			$updateFields['Drobe.featured']="'0'";
			$updateFields['Drobe.order']=NULL;
			
			if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
			{
				$this->Session->setFlash("Selected drobe is unmarked from Sale drobe");
			}
			else
			{
				$this->Session->setFlash("Error occured in unmarked from sale drobe");
			}
			return $this->redirect($this->referer());
		}
	}
	function admin_sell_drobe_action($action=null,$drobe_id=null)
	{
		$this->Drobe->id=$drobe_id;
		$this->Drobe->unbindModel(array('hasOne'=>array('Category','DrobeSetting')));
		$this->Drobe->bindModel(array('hasMany'=>array('SoldDrobe')));
		$drobeData=$this->Drobe->read();
		if($action=="approve")
		{
			$updateFields=array();
			$updateFields['SellDrobe.status']='"approved"';
			$updateFields['SellDrobe.sold']='"no"';
			if($this->Drobe->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$drobe_id)))
			{
				if($drobeData['SellDrobe']['sold']=="yes")
				{
					// when request received for relist drobe need to remove last payment record in case of approve for relist
					$this->Drobe->SoldDrobe->delete($drobeData['SoldDrobe'][0]['id']);
				}
				$max_drobe_order=$this->Drobe->field('max(Drobe.order)',array("Drobe.id != "=>$drobe_id, "Drobe.featured"=>1));
				$this->Drobe->updateAll(array('Drobe.featured'=>"1","Drobe.order"=>$max_drobe_order+1),array('Drobe.id'=>$drobe_id));
				
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$drobeData['User']['username'],
						"##image_source"=>Router::url('/drobe_images/'.$drobeData['Drobe']['file_name'],true),
						"##drobe_comment"=>$drobeData['Drobe']['comment'],
						"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
						"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
						"##sell_price"=>$drobeData['SellDrobe']['sell_price'],
						"##sell_description"=>$drobeData['SellDrobe']['sell_description']
				);
				if(isset($drobeData['User']['username']) && $drobeData['User']['username']!="")
				{
					$variables["##full_name"]=$drobeData['User']['username'];
				}
				else
				{
					$variables["##full_name"]=$drobeData['User']['first_name']." ".$drobeData['User']['last_name'];
				}
				// Sending notification mail
				$this->sendNotifyMail('approve_sell_drobe', $variables,$drobeData['User']['email'],1);
				
				$this->Session->setFlash("Selected sell drobe request is approved successfully");
			}
			else
			{
				$this->Session->setFlash("Error occured in unmarked from sale drobe");
			}
			return $this->redirect($this->referer());
		}
		else if($action=="decline")
		{
			$updateFields=array();
			$updateFields['SellDrobe.status']='"rejected"';
			if($drobeData['SellDrobe']['sold']!="yes")
			{
				// for relist drobe request rejection, no need to update drobe status
				$updateFields['SellDrobe.status']='"rejected"';
				$updateFields['SellDrobe.sold']='"no"';
				$updateFields['Drobe.post_type']='"sell"';
			}
			if($this->Drobe->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$drobe_id)))
			{
				$this->Drobe->updateAll(array('Drobe.featured'=>"0","Drobe.post_type"=>"'post'"),array('Drobe.id'=>$drobe_id));
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$drobeData['User']['username'],
						"##image_source"=>Router::url('/drobe_images/'.$drobeData['Drobe']['file_name'],true),
						"##drobe_comment"=>$drobeData['Drobe']['comment'],
						"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
						"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
						"##sell_price"=>$drobeData['SellDrobe']['sell_price'],
						"##sell_description"=>$drobeData['SellDrobe']['sell_description']
				);
				if(isset($drobeData['User']['username']) && $drobeData['User']['username']!="")
				{
					$variables["##full_name"]=$drobeData['User']['username'];
				}
				else
				{
					$variables["##full_name"]=$drobeData['User']['first_name']." ".$drobeData['User']['last_name'];
				}
				// Sending notification mail
				$this->sendNotifyMail('reject_sell_drobe', $variables,$drobeData['User']['email'],1);
				
				$this->Session->setFlash("Selected sell drobe request is declined successfully");
			}
			else
			{
				$this->Session->setFlash("Error occured in unmarked from sale drobe");
			}
			return $this->redirect($this->referer());
		}
		if($action=="unmark_approve")
		{
			$updateFields=array();
			$updateFields['SellDrobe.status']='"approved"';
			$updateFields['Drobe.post_type']='"post"';
			$updateFields['Drobe.featured']=0;
			if($this->Drobe->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$drobe_id)))
			{
				$this->Session->setFlash("Selected sell drobe unmark request is approved successfully");
			}
			else
			{
				$this->Session->setFlash("Error occured in unmarked from sale drobe");
			}
			return $this->redirect($this->referer());
		}
		else if($action=="unmark_decline")
		{
			$updateFields=array();
			$updateFields['SellDrobe.unmark_requested']=0;
			$updateFields['SellDrobe.status']='"approved"';
			if($this->Drobe->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$drobe_id)))
			{
				$this->Session->setFlash("Selected sell drobe unmark request is declined successfully");
			}
			else
			{
				$this->Session->setFlash("Error occured in unmarked from sale drobe");
			}
			return $this->redirect($this->referer());
		}
		else if($action=="unmark_sale")
		{
			$updateFields=array();
			$updateFields['Drobe.post_type']="'post'";
			$updateFields['Drobe.featured']="'0'";
			$updateFields['Drobe.order']=NULL;
				
			if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
			{
				$updateFields=array();
				$updateFields['SellDrobe.sold']='"no"';
				$this->Drobe->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$drobe_id));
						
				$this->Session->setFlash("Selected drobe is unmarked from Sale drobe");
			}
			else
			{
				$this->Session->setFlash("Error occured in unmarked from sale drobe");
			}
			return $this->redirect($this->referer());
		}
		else if($action=="shopify_add")
		{
			$drobe_data=$this->Drobe->findById($drobe_id);
			$request_data=array("product"=>array());
			$size_list=Cache::read('size_list');
			
			/*
			 * Create collection if not exist on everdrobe shop
			 */
			/*if($drobe_data['Category']['collection_id']==0)
			{
				$collection_id=$this->_createCollection($drobe_data['Category']);
			}
			else
			{
				$collection_id=$drobe_data['Category']['collection_id'];
			}*/
			
			/*
			 * End of create collection
			 */
			
			
			if($drobe_data['SellDrobe']['shopify_product_id']>0)
			{
				/*
				 * UPDATING Existing product in everdrobe shop
				 * 
				 * For Some limitations/bugs of shopify we need to use XML format for update product
				*/
				
				// getting current product detail
				$response=$this->shopify("/products/".$drobe_data['SellDrobe']['shopify_product_id'], "","GET");
				
				/*
				 * Creating product tags and product size
				*/
				$product_size_name=$drobe_data['SellDrobe']['size_name'];
				if(trim($product_size_name)=="") $product_size_name="Default";
				
				$uploader_name=$drobe_data['User']['username']. " ".$drobe_data['User']['id'];
				
				$product_tags=array();
				$product_tags[]=$drobe_data['SellDrobe']['sell_brand_name'];
				$product_tags[]=$drobe_data['Category']['category_name'];
				$product_tags[]=$product_size_name;
				$product_tags[]="For ".Inflector::humanize($drobe_data['SellDrobe']['sell_gender']);
				$product_tags[]=$uploader_name;
				$this->log("Tags:: ".print_r($product_tags,true),'shopify_tags');
				
				$request_data['product']=array(
						"id"=>$drobe_data['SellDrobe']['shopify_product_id'],
						"title"=>$drobe_data['Drobe']['comment'],
						"vendor"=>$drobe_data['SellDrobe']['sell_brand_name'],
						"body-html"=>$drobe_data['SellDrobe']['sell_description'],
						"product-type"=>$uploader_name,
						"tags"=> implode(",",$product_tags),
						"variants"=>array(
							'variant'=>array(
								// need to add variant id from current product
								"id"=>$response['product']['variants'][0]['id'],
								"price"=>$drobe_data['SellDrobe']['sell_price'],
								"title"=> $product_size_name,
								"option1"=> $product_size_name
							)
						)
				);
				
				$response=$this->shopify("/products/".$drobe_data['SellDrobe']['shopify_product_id'], json_encode($request_data),"PUT");
				if(isset($response['product']) && $response['product']['id']!="")
				{
					// adding product to collection
					/*if($collection_id>0)
					{
						$this->_updateProductCollection($response['product']['id'],$collection_id);
					}*/
					
					$this->_updateShopifyImages($drobe_data['SellDrobe']['id'],$response['product']['id']);
					$this->Session->setFlash("Selected product is updated on Everdrobe Shop","default",array("class"=>"success"));
				}
				else
				{
					if(isset($response['errors']['base']) && count($response['errors']['base'])>0)
					{
						$this->Session->setFlash("Shopify Upload Error: ". $response['errors']['base'][0],"default",array("class"=>"error"));
					}
					else $this->Session->setFlash("Error occured in upadate product in everdrobe shop","default",array("class"=>"error"));
				}
					
			}
			else
			{
				/*
				 * CREATING new product in everdrobe shop
				 */
				
				// Creating product tags and product size
				$product_size_name=$drobe_data['SellDrobe']['size_name'];
				if(trim($product_size_name)=="") $product_size_name="Default";
				
				$uploader_name=$drobe_data['User']['username']. " ".$drobe_data['User']['id'];
				
				$product_tags=array();
				$product_tags[]=$drobe_data['SellDrobe']['sell_brand_name'];
				$product_tags[]=$drobe_data['Category']['category_name'];
				$product_tags[]=$product_size_name;
				$product_tags[]="For ".Inflector::humanize($drobe_data['SellDrobe']['sell_gender']);
				$product_tags[]=$uploader_name;
				$this->log("Tags:: ".print_r($product_tags,true),'shopify_tags');
				$request_data['product']=array(
						"title"=>$drobe_data['Drobe']['comment'],
						"vendor"=>$drobe_data['SellDrobe']['sell_brand_name'],
						"body_html"=>$drobe_data['SellDrobe']['sell_description'],
						"product_type"=>$uploader_name,
						"tags"=> implode(",",$product_tags),
						"variants"=>array(array(
								"price"=>$drobe_data['SellDrobe']['sell_price'],
								"inventory_management"=> "shopify",
								//"inventory_policy"=> "continue",
								"requires_shipping"=> true,
								"taxable"=> true,
								"inventory_quantity"=>1,
								"fulfillment_service"=> "manual",
								"sku"=>sprintf("%04d",intval($drobe_data['Drobe']['id'])),
								"title"=> $product_size_name,
								"option1"=> $product_size_name
						))
				);
				
					
				/*
				 *  add main image in product
				 */
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					$image_path=WWW_ROOT . Configure::read('drobe.upload_dir') .DS . $drobe_data['Drobe']['file_name'];
					$request_data['product']['images'][]=array(
						"attachment"=>base64_encode(file_get_contents($image_path))
					);
				}
				else
				{
					$request_data['product']['images'][]=array(
						"src" => Router::url("/drobe_images/iphone/".$drobe_data['Drobe']['file_name'],true)
					);
				}
				
				$response=$this->shopify("/products", json_encode($request_data),"POST");
				if(isset($response['product']) && $response['product']['id']!="")
				{
					//updating buy URL for the product
					$buy_url=Configure::read("sell_drobe.shop_url").$response['product']['handle'];
					$buy_url="'".$buy_url."'";
					$this->Drobe->updateAll(array('Drobe.buy_url_actual'=>$buy_url,'Drobe.buy_url'=>$buy_url),array('Drobe.id'=>$drobe_id));
						
					// adding product to collection
					/*if($collection_id>0)
					{
						$this->_addProductToCollection($response['product']['id'],$collection_id);
					}*/
					
					$this->Drobe->SellDrobe->id=$drobe_data['SellDrobe']['id'];
					if($this->Drobe->SellDrobe->saveField('shopify_product_id',$response['product']['id']))
					{
						// uploading product images to shopify
						$this->_updateShopifyImages($drobe_data['SellDrobe']['id'],$response['product']['id']);
					}
					$this->Session->setFlash("Selected product is published on Everdrobe Shop","default",array("class"=>"success"));
				}
				else
				{
					if(isset($response['errors']['base']) && count($response['errors']['base'])>0)
					{
						$this->Session->setFlash("Shopify Upload Error: ". $response['errors']['base'][0],"default",array("class"=>"error"));
					}
					else $this->Session->setFlash("Error occured in publish product in everdrobe shop","default",array("class"=>"error"));
				}
			}
			return $this->redirect($this->referer());
		}
		else if($action=="shopify_remove")
		{
			/*
			 * Removeing product from Shopify 
			 */
			$SellDrobeData=$this->Drobe->SellDrobe->findByDrobeId($drobe_id);
			$SellDrobeData=$SellDrobeData['SellDrobe'];
			if($SellDrobeData['shopify_product_id']>0)
			{
				$response=$this->shopify("/products/".$SellDrobeData['shopify_product_id'], "","DELETE");
				if(count($response)==0)
				{
					/*
					 * Removing tracking id from everdrobe database
					 */
					
					$this->Drobe->SellDrobe->id=$SellDrobeData['id'];
					$this->Drobe->SellDrobe->saveField('shopify_product_id',0);
					$this->Drobe->SellDrobe->SellDrobeImage->updateAll(array('shopify_image_id'=>0),array('sell_drobe_id'=>$SellDrobeData['id']));
					
					$this->Session->setFlash("Selected product is deleted on Everdrobe Shop","default",array("class"=>"success"));
				}
				else
				{
					$this->Session->setFlash("Error occured in delete product in everdrobe shop","default",array("class"=>"error"));
				}
			}
			return $this->redirect($this->referer());
		}
	}
	function admin_remove_from_shop($drobe_id){
		$this->Drobe->id=$drobe_id;
		$this->Drobe->unbindModel(array('hasOne'=>array('Category','DrobeSetting')));
		$this->Drobe->bindModel(array('hasMany'=>array('SoldDrobe')));
		$drobeData=$this->Drobe->read();
		$SellDrobeData=$this->Drobe->SellDrobe->findByDrobeId($drobe_id);
		$SellDrobeData=$SellDrobeData['SellDrobe'];
		if($SellDrobeData['shopify_product_id']>0)
		{
			$response=$this->shopify("/products/".$SellDrobeData['shopify_product_id'], "","DELETE");
			if(count($response)==0)
			{
				/*
				 * Removing tracking id from everdrobe database
				*/
					
				$this->Drobe->SellDrobe->id=$SellDrobeData['id'];
				$this->Drobe->SellDrobe->saveField('shopify_product_id',0);
				$this->Drobe->SellDrobe->SellDrobeImage->updateAll(array('shopify_image_id'=>0),array('sell_drobe_id'=>$SellDrobeData['id']));
					
				$this->Session->setFlash("Selected product is deleted on Everdrobe Shop","default",array("class"=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in delete product in everdrobe shop","default",array("class"=>"error"));
			}
		}
		else {
			$this->Session->setFlash("Drobe is not available in shopify as a product.","default",array("class"=>"error"));
		}
		$this->Drobe->SellDrobe->id=$SellDrobeData['id'];
		$this->Drobe->SellDrobe->saveField('status','approved');
		return $this->redirect($this->referer());
	}
	function _createCollection($category_data)
	{
		$collection_info=array(
			"custom_collection"=>array(
					"title"=> $category_data["category_name"],
			)
		);
		$response=$this->shopify("/custom_collections", json_encode($collection_info),"POST");
		if(isset($response['custom_collection']) && $response['custom_collection']['id']>0)
		{
			$collection_id=$response['custom_collection']['id'];
			$this->Drobe->Category->id=$category_data['id'];
			$this->Drobe->Category->saveField('collection_id',$collection_id);
			return $collection_id;
		}
		else return null;
	}
	/*
	 * Update collection id for specific product
	 */
	function _updateProductCollection($product_id,$collection_id)
	{
		$response=$this->shopify("/custom_collections", "","GET","product_id=".$product_id);
		
		if(count($response['custom_collections'])>0)
		{
			if($response['custom_collections'][0]['id']!=$collection_id)
			{
				/*
				 *  Collection id is different then  need to remove product from existing collection and add product in new collection
				 */
				$collects=array();
					
				if(is_array($response['custom_collections'][0]['collects']))
				{
					foreach($response['custom_collections'][0]['collects'] as $product)
					{
						// removing current product from collection
						if($product_id!=$product['product_id'])
						{
							$collects[]=array("product-id"=>$product['product_id']);
						}
					}
				}
				
				$collection_info=array(
						"custom-collection"=>array(
								"collects"=>$collects
						)
				);
					
				$response=$this->shopify("/custom_collections/".$response['custom_collections'][0]['id'], json_encode($collection_info),"PUT");
				
				
				$this->_addProductToCollection($product_id,$collection_id);
			}
		}
		else
		{
			$this->_addProductToCollection($product_id,$collection_id);
		}
	}
	/*
	 * Adding product to collection
	 * 
	 */
	function _addProductToCollection($product_id,$collection_id)
	{
		$response=$this->shopify("/custom_collections/".$collection_id, "","GET");
		if(isset($response['custom_collection']) && $response['custom_collection']['id']>0)
		{
			$collects=array();
			if(is_array($response['custom_collection']['collects']))
			{
				foreach($response['custom_collection']['collects'] as $product)
				{
					$collects[]=array("product-id"=>$product['product_id']);
				} 
				
			}
			$collects[]=array("product-id"=>$product_id);
			
			$collection_info=array(
					"custom-collection"=>array(
						"collects"=>$collects
					)
			);
			
			$response=$this->shopify("/custom_collections/".$collection_id, json_encode($collection_info),"PUT");
		}
	}
	
	
					
	/*
	 * Manage product images with shopify
	 */
	function _updateShopifyImages($sell_drobe_id, $shopify_product_id,$updateMainImage="")
	{
		$additional_images=$this->Drobe->SellDrobe->SellDrobeImage->find('all',array('conditions'=>array('SellDrobeImage.sell_drobe_id'=>$sell_drobe_id)));
		$images_in_shop=$this->shopify("/products/".$shopify_product_id."/images", "","GET");
		if(isset($images_in_shop['images']) && count($images_in_shop['images'])>1)
		{
			/*
			 * Considering that first image not from additional image (it is from original drobe post)
			 */
			$first_image = array_shift($images_in_shop['images']);
			
			$images=array();
			foreach($additional_images as $image)
			{
				$images[]=$image['SellDrobeImage']['shopify_image_id'];
			}
			/*
			 * Removing image from everdrobe shop id removed image from additional product
			 */
			foreach($images_in_shop['images'] as $existing_image)
			{
				if(!in_array($existing_image['id'], $images))
				{
					$res=$this->shopify("/products/".$shopify_product_id."/images/".$existing_image['id'],"","DELETE");
				}
			}
			
			//if first image is edited then update first image
			if($updateMainImage!="")
			{
				$res=$this->shopify("/products/".$shopify_product_id."/images/".$first_image['id'],"","DELETE");
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					$image_path=WWW_ROOT . Configure::read('drobe.upload_dir') .DS .$updateMainImage;
					$product_image=array(
							"image"=>array(
									"attachment"=>base64_encode(file_get_contents($image_path)),
									"filename"=>uniqid().".gif",
									"position" => "1"
							)
					);
				}
				else
				{
					$product_image=array(
							"image"=>array(
									"src" => Router::url('/drobe_images/'.$updateMainImage,true),
									"position" => "1"
							)
					);
				}
				$this->log(print_r($product_image,true),"shopify");
				$img_response=$this->shopify("/products/".$shopify_product_id."/images", json_encode($product_image),"POST");
				$this->log(print_r($img_response,true),"shopify");
				if(isset($img_response['image']) && $img_response['image']['id']!="")
				{
						
				}
			}
			
		}
		
		foreach($additional_images as $image)
		{
				
			if($image['SellDrobeImage']['shopify_image_id']==0)
			{
				/*
				 * Uploading additional image to everdrobe shop which not have shopify image id
				 */
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					$image_path=WWW_ROOT . Configure::read('sell_drobe.upload_dir') .DS .$image['SellDrobeImage']['file_name'];
					$product_image=array(
							"image"=>array(
									"attachment"=>base64_encode(file_get_contents($image_path)),
									"filename"=>uniqid().".gif"
							)
					);
				}
				else
				{
					$product_image=array(
						"image"=>array(
							"src" => Router::url('/drobe_images/additional/'.$image['SellDrobeImage']['file_name'],true)
						)
					);
				}
				$this->log(print_r($product_image,true),"shopify");
				
				$img_response=$this->shopify("/products/".$shopify_product_id."/images", json_encode($product_image),"POST");
				$this->log(print_r($img_response,true),"shopify");
				if(isset($img_response['image']) && $img_response['image']['id']!="")
				{
					$this->Drobe->SellDrobe->SellDrobeImage->id=$image['SellDrobeImage']['id'];
					$this->Drobe->SellDrobe->SellDrobeImage->saveField('shopify_image_id',$img_response['image']['id']);
				}
			}
		}
	}
	/*
	 * Physically removes drobe rates conversions from database
	*/
	
	function _removeDrobeConversations($drobe_id)
	{
		$conversation_ids=$this->Drobe->Rate->find('list',array('fields'=>array("Rate.conversation_id"),"conditions"=>array('Rate.drobe_id'=>$drobe_id)));
		$this->loadModel('Conversation');
		$this->Conversation->deleteAll(array('Conversation.unique_id'=>$conversation_ids));
		//resetting new reply and response status of that drobe rate 
		$this->Drobe->Rate->updateAll(array("new_reply"=>0,"new_response"=>0),array('drobe_id'=>$drobe_id));
		
	}
	/*
	 * Physically removes drobe rates from database
	 */
	function _removeDrobeRates($drobe_id)
	{
		$conversation_ids=$this->Drobe->Rate->find('list',array('fields'=>array("conversation_id"),"conditions"=>array('drobe_id'=>$drobe_id)));
		$this->loadModel('Conversation');
		$this->Conversation->deleteAll(array('Conversation.unique_id'=>$conversation_ids));
		//resetting new reply and response status of that drobe rate
		$this->Drobe->Rate->deleteAll(array('Rate.drobe_id'=>$drobe_id));
	}
	/*
	 * This function updated drobe rater counters only
	 */
	function _deleteDrobeRates($drobe_id)
	{
		$rated_by=array_values($this->Drobe->Rate->find('list',array('fields'=>array("user_id"),"conditions"=>array('drobe_id'=>$drobe_id,"rate !="=>0))));
		if(count($rated_by))
		{
			$this->Drobe->User->updateUserTotalField($rated_by,'vote_given',-1);
		}
		$rated_by=array_values($this->Drobe->Rate->find('list',array('fields'=>array("user_id"),"conditions"=>array('drobe_id'=>$drobe_id,'comment NOT LIKE'=>""))));
		if(count($rated_by))
		{
			$this->Drobe->User->updateUserTotalField($rated_by,'feedback_given',-1);
		}
	}
	function admin_position($action=null,$drobe_id=null)
	{
		
		$this->Drobe->recursive=-1;
		$this->Drobe->id=$drobe_id;
		$current_order=$this->Drobe->field('order',array('Drobe.id'=>$drobe_id,'Drobe.featured'=>1));
		if($action=="up")
		{
			$previous_drobe=$this->Drobe->find('first',array("fields"=>array('Drobe.id','Drobe.order'),"conditions"=>array("Drobe.order < "=>$current_order, 'Drobe.featured'=>1,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open'),"order"=>"Drobe.order DESC"));
			if($previous_drobe)
			{
				$this->Drobe->saveField('order',$previous_drobe['Drobe']['order']);
				$this->Drobe->id=$previous_drobe['Drobe']['id'];
				$this->Drobe->saveField('order',$current_order);
				$this->Session->setFlash("Drobe moved up successfully");
			}
			else
			{
				$this->Session->setFlash("Drobe not found in up order");
			}
		}
		else if($action=="down")
		{
			$next_drobe=$this->Drobe->find('first',array("fields"=>array('Drobe.id','Drobe.order'),"conditions"=>array("Drobe.order > "=>$current_order, 'Drobe.featured'=>1,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open'),"order"=>"Drobe.order ASC"));
			if($next_drobe)
			{
				$this->Drobe->saveField('order',$next_drobe['Drobe']['order']);
				$this->Drobe->id=$next_drobe['Drobe']['id'];
				$this->Drobe->saveField('order',$current_order);
				$this->Session->setFlash("Drobe moved down successfully");
			}
			else
			{
				$this->Session->setFlash("Drobe not found in down order");
			}
		}
		else if($action=="first" || $action=="last")
		{
			/*$listing_order=$this->Drobe->find('list',array("fields"=>array("Drobe.order"),"conditions"=>array("Drobe.id != "=>$drobe_id, "Drobe.featured"=>1),"order"=>"Drobe.order ASC"));
			if($action=="first")
			{
				$this->Drobe->saveField('order',1);
				$pos=2;
				foreach ($listing_order as $id=>$order)
				{
					$this->Drobe->id=$id;
					$this->Drobe->saveField('order',$pos);
					$pos++;
				}
			}
			else
			{
				$pos=1;
				foreach ($listing_order as $id=>$order)
				{
					$this->Drobe->id=$id;
					$this->Drobe->saveField('order',$pos);
					$pos++;
				}
				$this->Drobe->id=$drobe_id;
				$this->Drobe->saveField('order',$pos);
			}*/
			
			
			if($action=="first")
			{
				$min_drobe_order=$this->Drobe->field('min(Drobe.order)',array("Drobe.id != "=>$drobe_id, "Drobe.featured"=>1));
				$this->Drobe->saveField('order',$min_drobe_order-1);
			}
			else
			{
				$max_drobe_order=$this->Drobe->field('max(Drobe.order)',array("Drobe.id != "=>$drobe_id, "Drobe.featured"=>1));
				$this->Drobe->saveField('order',$max_drobe_order+1);
			}
			
		}
		return $this->redirect($this->referer());
	}
	function rate($drobe_id=0,$sort_by="recent",$action="all_drobes",$action_id="")
	{
		/*$this->loadModel('StreamCategory');
		$streamCategory=$this->StreamCategory->_loadUsersCategory($this->Auth->user('id'));*/
		$this->loadModel('UserFilter');
		$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
		$order="";
		if(!$this->RequestHandler->isAjax())
		{
			// load stream category only one time when whole page is loading
			if($this->Auth->user('id')>0)
			{
				if(Cache::read('flag_category')==null)
				{
					$this->loadModel('FlagCategory');
					$this->FlagCategory->_updateFlagCache();
				}
				if(Cache::read('drobe_category')==null)
				{
					$this->loadModel('Category');
					$this->Category->_updateCategoryCache();
				}
				if(Cache::read('size_list')==null)
				{
					$this->loadModel('Size');
					$this->Size->_updateSizeCache();
				}
				
				if(Cache::read('brand_list')==null)
				{
					$this->loadModel('Brand');
					$this->Brand->_updateBrandCache();
				}
				
				$this->set('userFilter',$userFilter);
			}
			$this->set('is_ajax',false);
			
			
			/*
			 *  initialiding general conditions for rate drobe stream
			 */
			$conditions=array(
				'Drobe.rate_status'=>'open',
				'Drobe.deleted'=>0,
				'User.id >'=>0
				);
			if($drobe_id==0)
			{
				$conditions['Drobe.user_id !=']=$this->Auth->user('id');
			}
			//$conditions['OR']['DrobeSetting.ask_public']=1;
			
			/*
			 * In case of enter in drobe stream by clicking on drobe thumbnail
			* in this case we need to load that drobe only
			* otherwise conside that user has clicked on "Rate Drobes" navidation button 
			*/
			if($drobe_id!=null && $drobe_id!=0)
			{
				// if entered in stream by clicking on drobe thumbnail
				$conditions['Drobe.unique_id']=$drobe_id;
				
			}
			else
			{
				$order="Drobe.uploaded_on DESC";
				// if entered in stream by clicked on "Rate Drobes" navidation button
				if($this->Auth->user('id')>0)
				{
					/*
					 * loading first drobe in rate stream according to gender target filter
					 */
					$conditions['OR']=array();
					/*
					if($this->Session->Read('UserSetting.gender_filter')=="female") $conditions['OR']['User.gender']="female";
					if($this->Session->Read('UserSetting.gender_filter')=="male") $conditions['OR']['User.gender']="male"; */
					if($userFilter['gender']=="female")$conditions['OR']['User.gender']="female";
					if($userFilter['gender']=="male")$conditions['OR']['User.gender']="male";
					if(isset($conditions['OR']) && count($conditions['OR'])>1)
					{
						// if drobe is self uploaded then avoiding gender target filter
						$conditions['OR']['user_id']=$this->Auth->user('id');
					}
					
					/*
					 * loading first drobe which is not rated by user 
					 */
					$rated_ids=$this->Drobe->Rate->find('list',array('fields'=>array('Rate.drobe_id'),'conditions'=>array('Rate.user_id'=>$this->Auth->user('id')),'group'=>array('Rate.drobe_id')));
					if(count($rated_ids))	$conditions['Drobe.id NOT']=$rated_ids;
					
					/* load only user selected category stream
					if($streamCategory['categories']!="ALL" && is_array($streamCategory['categories']))
					{
						$conditions['Drobe.category_id']=$streamCategory['categories'];
					} */
					
					// load only user selected category from UserFilter table
					if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")     
					{
						$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);
					}
					$only_sell_drobe=false;
					if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
					{
						$only_sell_drobe=true;
						$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
					}
						
					if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
					{
						$only_sell_drobe=true;
						$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
					}
						
					if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
					{
						$only_sell_drobe=true;
						$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
					}
					
					
					//Check Brand New or User or both
					if($userFilter['only_new_brand']=="new")
					{
						$conditions['OR']['SellDrobe.is_brand_new']="new";
						$only_sell_drobe = true;
					}
					if($userFilter['only_new_brand']=="used")
					{
						$conditions['OR']['SellDrobe.is_brand_new']="used";
						$only_sell_drobe = true;
					}
					
					
					//if only sell drobe is true then it display only sell drobe
					if($only_sell_drobe)
					{
						$conditions['Drobe.post_type']= "sell";
					}
					
					
					if(empty($conditions['OR'])) unset($conditions['OR']);
				}
				
			}
			/*
			 * loading a drobe
			 */
			
			$this->Drobe->unbindModel(array('hasOne'=>array("DrobeSetting"),
					'hasMany'=> array("Rate","Flag","Favourite")));
			$drobe=$this->Drobe->find('first',array(
					'fields'=>array('User.*','Drobe.unique_id','Drobe.uploaded_on',"SellDrobe.size_name","SellDrobe.original_sell_price","SellDrobe.sell_price",'Drobe.id','Drobe.is_highest_rated','Drobe.file_name','Drobe.comment','Drobe.views','Drobe.total_in','Drobe.total_out','Category.category_name','Drobe.user_id','Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs'),
					'conditions'=>$conditions,
					'order'=>$order)
			);
			if(!$drobe) 
			{
				$drobe=false;
			}
		}
		else 
		{
			/*
			 * in ajax request passing post parameters for type of streaming and order ect..
			 * if those parameter has blank value then we consider default value
			 */
			$this->set('is_ajax',true);
			$action =  ($this->data['action']!="" ? $this->data['action'] : "all_drobes");
			$action_id =  ($this->data['action_id']!="" ? $this->data['action_id'] : "");
			$sort_by =  ($this->data['order']!="" ? $this->data['order'] : "");
			$direction  =  ($this->data['direction']!="" ? $this->data['direction'] : "next");
			$drobe=false;
		}
		if($action_id!="")
		{
			if(!$this->Drobe->User->field('id',array('User.unique_id'=>$action_id)))
			{
				throw new NotFoundException("Not found");
			}
		}
		if(!in_array($action,array("all_drobes","rate_stream","user","favourite","following")) || !in_array($sort_by,array("","recent","rated","viewed","featured")))
		{
			throw new NotFoundException("Not found");
		}
		
		/*
		 * In ajax request, $drobe is definately has null value or not initialized 
		 * in this case need to load next drobe based on stream type and order type from last drobe id 
		 */
		if(!$drobe)
		{
			/*
			 * initializing generaql conditions
			 * 
			 */
			
			$conditions=array('Drobe.user_id !='=>$this->Auth->user('id'),'Drobe.rate_status'=>'open','Drobe.deleted'=>0);
			
			/*
			 * satisfying condition of gender targeting filter  
			*/
			$joins_gender=array();
			//if($this->Session->Read('UserSetting.gender_filter')=="female")
			if($userFilter['gender']=="female")
			{
				$conditions['OR']['User.gender']="Female";
				$joins_gender=array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'LEFT',
						'conditions' => array(
								"User.gender" => "Female",
						)
				);
			}
			if($userFilter['gender']=="male")
			{
				$conditions['OR']['User.gender']="male";
				$joins_gender=array(
						'table' => 'users',
						'alias' => 'User',
						'type' => 'LEFT',
						'conditions' => array(
								'User.gender'=>"female",
						)
				);
			}

			
	
			
			/*
			 *  load only drobes from user selected category stream
			 */ 
			/*if($streamCategory['categories']!="ALL" && is_array($streamCategory['categories']))
			{
				$conditions['Drobe.category_id']=$streamCategory['categories'];
			}*/
			
			/*
			 *  load only drobes from user selected category from User Filter table
			*/
			if($userFilter['category_ids']!='ALL' && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);
			}
			$only_sell_drobe = false;
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
				
				
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
				
				
			//if only sell drobe is true then it display only sell drobe
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
			
			
			
			$order="";
			$joins=array();
			/*
			 * deciding order of drobe based on sort_by type 
			 */
			if($sort_by=="recent")
			{
				if($this->Auth->user('id')>0)
				{
					$joins=array(
							array(
									'table' => 'rates',
									'alias' => 'Rate',
									'type' => 'LEFT',
									'conditions' => array(
											'Rate.drobe_id = Drobe.id',
											'Rate.user_id' => $this->Auth->user('id')
									)
							)
					);
					$order="Rate.user_id ASC, Drobe.uploaded_on DESC";
				}
				else $order="Drobe.uploaded_on DESC";
			}
				
			$new_join=array();
			$new_join=$joins;
			if(!empty($joins_gender))
				$new_join[]=$joins_gender;
			if($sort_by=="rated")
			{
				//$order="Drobe.total_rate DESC";
				//$order="Drobe.total_in/Drobe.total_rate DESC, Drobe.views DESC";
				//$order="Drobe.rate_index DESC, Drobe.views DESC";
				$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
			}
			if($sort_by=="viewed") $order="Drobe.views DESC";
			
			if($sort_by=="featured")
			{
				$order="Drobe.featured DESC, Drobe.order ASC,Drobe.uploaded_on DESC";
				$conditions[]=array("OR"=>array("Drobe.featured"=>1,"AND"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved")));
			}
			
			if(($action=="rate_stream" && $drobe==false) || (isset($this->data['last_drobe']) && $this->data['last_drobe']!=""))
			{
				
				if(isset($this->data['last_drobe']))
				{
					$last_drobe_id=$this->Drobe->field('Drobe.id',array('Drobe.unique_id'=>$this->data['last_drobe']));
				}
				else
				{
					$last_drobe_id=false;
				}
				//$conditions['Drobe.id']=$rated_ids;
				
				if($action=="rate_stream")
				{
					/*
					 * considering that users come from rate drobes stream
					*/
					if($this->Auth->user('id')>0)
					{
						$rated_ids=$this->Drobe->Rate->find('list',array('fields'=>array('Rate.drobe_id'),'conditions'=>array('Rate.user_id'=>$this->Auth->user('id')),'group'=>array('Rate.drobe_id')));
						if(count($rated_ids))	$conditions['Drobe.id NOT']=$rated_ids;
					}
					$order="Drobe.uploaded_on DESC";
					$this->Drobe->recursive=0;
					$drobe=$this->Drobe->find('first',array(
							'fields'=>array("User.*",'Drobe.unique_id','Drobe.uploaded_on','Drobe.file_name','Drobe.comment','Drobe.views','Drobe.total_in','Drobe.total_out','Category.category_name','Drobe.user_id','Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs','SellDrobe.sell_price','SellDrobe.original_sell_price','SellDrobe.size_name'),
							'conditions'=>$conditions,
							'order'=>$order)
					);
					if(!$drobe) 
					{
						$drobe=false;
						if(isset($conditions['Drobe.id NOT'])) unset($conditions['Drobe.id NOT']);
					}
				}
				if(!$drobe)
				{

					/*
					 * If still drobe not found then in this situaltion  we need to follow action variable
					 */
					
					if($action=="rate_stream")
					{
						// if unrated drobe not found in rate stream then we need to run loop for all drobes 
						$action="all_drobes";
					}
					if($action=="all_drobes")
					{
						$this->Drobe->recursive=0;
						$this->Drobe->id=$last_drobe_id;
						if($sort_by=="recent")
						{
							$conditions['Drobe.id <']=$last_drobe_id;
							$neighbors = $this->Drobe->find('first', array(
		    					'order' => $order,
								'conditions'=>$conditions,
								'joins'=>$joins,
		    					'fields' => array('Drobe.id'))
							);
							
							unset($conditions['Drobe.id <']);
							
							if(isset($neighbors['Drobe']['id']))
							{
								// get next rated drobe if it is available for rate drobe
								$conditions['Drobe.id']=$neighbors['Drobe']['id'];
							}
							else if($direction=='prev' && isset($neighbors['Drobe']['id']))
							{
								$conditions['Drobe.id <']=$last_drobe_id;
							}
							else
							{
								// if next rated drobe not found then showup first drobe in ascending order
								$conditions['Drobe.id >']=$last_drobe_id;
							}
						}
						else 
						{
							
							// in this situations need to take order of all drobes because doubly filter not supported by cake php
							//$this->Drobe->recursive=0;
							$drobe_ids = $this->Drobe->find('list', array('fields' => array('Drobe.id','Drobe.unique_id'), 'conditions' => $conditions,'order'=>$order,'recursive'=>0));
							$drobe_id=array_search($this->data['last_drobe'], $drobe_ids);
							$drobe_ids=array_keys($drobe_ids);
							$drobe_id_pos=array_search($drobe_id, $drobe_ids);
							if(count($drobe_ids)> ($drobe_id_pos+1) && $direction=='next')
							{
								$conditions['Drobe.id']=$drobe_ids[$drobe_id_pos+1];
							}
							else if(count($drobe_ids)> ($drobe_id_pos-1) && ($drobe_id_pos-1)>=0 && $direction=='prev')
							{
								$conditions['Drobe.id']=$drobe_ids[$drobe_id_pos-1];
							}
							else $conditions['Drobe.id']=$drobe_ids[0];
							//$order="";
						}
						
					}
					else if($action=="user" && $action_id!=null)
					{
						$this->Drobe->recursive=0;
						$tmp_user_id=$this->Drobe->User->field('id',array('unique_id'=>$action_id));
						if(!$tmp_user_id)
						{
							throw new NotFoundException();
						}
						$specific_drobe_ids = $this->Drobe->find('list', array('fields' => array('Drobe.id','Drobe.unique_id'), 'conditions' => array_merge($conditions,array("Drobe.user_id"=>$tmp_user_id)),'order'=>$order,'joins'=>$new_join));
						$drobe_id=array_search($this->data['last_drobe'], $specific_drobe_ids);
						$specific_drobe_ids=array_keys($specific_drobe_ids);
						$drobe_id_pos=array_search($drobe_id, $specific_drobe_ids);
						if(count($specific_drobe_ids)> ($drobe_id_pos+1) &&  $direction=='next')
						{
							$conditions['Drobe.id']=$specific_drobe_ids[$drobe_id_pos+1];
						}
						else if(count($specific_drobe_ids)> ($drobe_id_pos-1) && ($drobe_id_pos-1)>=0 &&  $direction=='prev')
						{
							$conditions['Drobe.id']=$specific_drobe_ids[$drobe_id_pos-1];
						}
						else $conditions['Drobe.id']=$specific_drobe_ids[0];
						$order="";
					}
					else if($action=="favourite")
					{
						if($action_id!="")
						{
							$tmp_user_id=$this->Drobe->User->field('id',array('User.unique_id'=>$action_id));
						}
						else $tmp_user_id=$this->Auth->user('id');
						if(!$tmp_user_id)
						{
							throw new NotFoundException();
						}
						$fav_drobe_ids = $this->Drobe->Favourite->find('list', array('fields' => array('Favourite.id','Favourite.drobe_id'), 'conditions' => array("Favourite.user_id"=>$tmp_user_id),"order"=>"Favourite.created_on DESC"));
						/*if($sort_by!="recent")
						{
							$specific_drobe_ids = $this->Drobe->find('list', array('fields' => array('Drobe.id','Drobe.unique_id'), 'conditions' => array_merge($conditions,array("Drobe.id"=>$fav_drobe_ids)),'order'=>$order));
						}
						else 
						{
							$specific_drobe_ids = $this->Drobe->find('list', array('fields' => array('Drobe.id','Drobe.unique_id'), 'conditions' => array_merge($conditions,array("Drobe.id"=>$fav_drobe_ids))));
						}*/
						$specific_drobe_ids = $this->Drobe->find('list', array('fields' => array('Drobe.id','Drobe.unique_id'), 'conditions' => array_merge($conditions,array("Drobe.id"=>$fav_drobe_ids)),'order'=>$order,'joins'=>$new_join,'recursive'=>1));
						
						$drobe_id=array_search($this->data['last_drobe'], $specific_drobe_ids);
						$specific_drobe_ids=array_keys($specific_drobe_ids);
						$drobe_id_pos=array_search($drobe_id, $specific_drobe_ids);
						if(count($specific_drobe_ids)> ($drobe_id_pos+1) && $direction=='next')
						{
							$conditions['Drobe.id']=$specific_drobe_ids[$drobe_id_pos+1];
						}
						else if(count($specific_drobe_ids)> ($drobe_id_pos-1) && ($drobe_id_pos-1)>=0 && $direction=='prev')
						{
							$conditions['Drobe.id']=$specific_drobe_ids[$drobe_id_pos-1];
						}
						else $conditions['Drobe.id']=$specific_drobe_ids[0];
						$order="";
					}
					else if($action=="following")
					{
						$this->loadModel('Follower');
						$following_ids = $this->Follower->find('list',array('fields'=>array('Follower.user_id'),'conditions'=>array('Follower.follower_id'=>$this->Auth->user('id'))));
						//$following_drobes_ids = $this->Drobe->find('list',array('fields'=>array('Drobe.id','Drobe.file_name'),'conditions'=>array('Drobe.user_id'=>$following_ids)));
						$specific_drobe_ids = $this->Drobe->find('list', array('fields' => array('Drobe.id','Drobe.unique_id'), 'conditions' => array_merge($conditions,array("Drobe.user_id"=>$following_ids)),'order'=>$order,'joins'=>$new_join));
						$drobe_id=array_search($this->data['last_drobe'], $specific_drobe_ids);
						$specific_drobe_ids=array_keys($specific_drobe_ids);
						$drobe_id_pos=array_search($drobe_id, $specific_drobe_ids);
						if(count($specific_drobe_ids)> ($drobe_id_pos+1) && $direction=='next')
						{
							$conditions['Drobe.id']=$specific_drobe_ids[$drobe_id_pos+1];
						}
						else if(count($specific_drobe_ids)> ($drobe_id_pos-1)&& ($drobe_id_pos-1)>=0 && $direction=='prev')
						{
							$conditions['Drobe.id']=$specific_drobe_ids[$drobe_id_pos-1];
						}
						else $conditions['Drobe.id']=$specific_drobe_ids[0];
						$order="";
						
					}
					else
					{
						throw new NotFoundException();
					}
					
					
					$this->Drobe->recursive=0;
					$drobe=$this->Drobe->find('first',array(
							'fields'=>array("User.*",'Drobe.unique_id','Drobe.uploaded_on','Drobe.file_name','Drobe.comment','Drobe.views','Drobe.total_in','Drobe.total_out','Category.category_name','Drobe.user_id','Drobe.featured','Drobe.post_type','Drobe.buy_url','Drobe.short_url','Drobe.total_favs','SellDrobe.sell_price','SellDrobe.size_name','SellDrobe.original_sell_price'),
							'conditions'=>$conditions,
							'joins'=>$joins,
							'order'=>$order)
					);
				}
			}
		}
		if($drobe)
		{
			// counting views of drobes
			$updateFields=array();
			$updateFields["Drobe.views"]="Drobe.views + 1 ";
			$this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe['Drobe']['id']));
				
			$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
				
			/*$drobe['Drobe']['total_in']= $total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0;
			$drobe['Drobe']['total_out']= $total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0;*/
			$drobe['Drobe']['total_in']= $drobe['Drobe']['total_in'];
			$drobe['Drobe']['total_out']= $drobe['Drobe']['total_out'];
				
			// counting favourite drobe
			//$drobe['Drobe']['favs']=$this->Drobe->Favourite->find('count',array("conditions"=>array('Favourite.drobe_id'=>$drobe['Drobe']['id'])));
			$drobe['Drobe']['favs']=$this->convertToReadeable($drobe['Drobe']['total_favs']);
				
			

			if($this->Auth->user('id')>0)
			{
				// is drobe rated by loggedin user
				$rate=$this->Drobe->Rate->find('first',array('fields'=>array('Rate.rate','Rate.comment','Rate.rewarded'),'conditions'=>array('Rate.user_id'=>$this->Auth->user('id'),'Rate.drobe_id'=>$drobe['Drobe']['id'])));
				if($rate)
				{
					$this->set('is_rated',true);
					$this->set('rate_info',$rate['Rate']);
				}
				elseif($this->Auth->user('id')==$drobe['Drobe']["user_id"])
				{
					$this->set('is_rated',true);
					$this->set('rate_info',array("rate"=>1,"comment"=>"","rewarded"=>0));
				}
				else 
				{
					$this->set('is_rated',false);
				}
				
				// is in my favourite list
				$this->set('is_my_favourite',$this->Drobe->Favourite->find('count',array('conditions'=>array("Favourite.drobe_id"=>$drobe['Drobe']['id'],"Favourite.user_id"=>$this->Auth->user('id')))));
			
				// is uploader is followed by loggedin user
				$this->loadModel('Follower');
				$this->set('is_followed',$this->Follower->find('count',array('conditions'=>array("Follower.user_id"=>$drobe['Drobe']['user_id'],"Follower.follower_id"=>$this->Auth->user('id')))));
				
				$this->set('my_drobe',$this->Auth->user('id')==$drobe['Drobe']['user_id']);
				$drobe['Drobe']['following_drobe'] = $this->_removeNotifyFollowers($drobe['Drobe']['id'], $this->Auth->user('id'));
			}
			else
			{
				$this->set('is_rated',false);
				$this->set('is_followed',0);
				$this->set('my_drobe',false);
			}
		}
		if(!$drobe)
		{
			//throw new NotFoundException("Not found");
		}
		/*$this->loadModel('User');
		$user=$this->User->find('first',array('fields'=>array('User.id','User.unique_id','User.first_name','User.last_name','User.username','User.photo'),'conditions'=>array('User.id'=>$drobe['Drobe']['user_id'])));
		$this->set('user',$user);*/
		
		$this->set('drobe',$drobe);
		$this->set('title_for_layout',"My Drobe");
	}
	
	/*
	 * Do rate from commandline to unrated drobe
	 */
	function do_vote($rate=null,$user_id=null,$seconds=null)
	{
		$this->Drobe->User->recursive=-1;
		$user_data=$this->Drobe->User->findById($user_id);
		
		
		if($user_data['User']['id']>0)
		{
			$rated_ids=$this->Drobe->Rate->find('list',array('fields'=>array('Rate.drobe_id'),'conditions'=>array('Rate.user_id'=>$user_data['User']['id']),'group'=>array('Rate.drobe_id')));

			$this->Drobe->recursive=-1;
			$drobe_data=$this->Drobe->find('first',array(
					'conditions'=>array('Drobe.deleted'=>0,'Drobe.rate_status'=>"open", 'Drobe.id NOT'=> $rated_ids),
					'order'=>"Drobe.id DESC"
			));

			if($drobe_data)
			{
				if($seconds!=null && intval($seconds))
				{
					$drobeTimeSeconds=strtotime($drobe_data['Drobe']['uploaded_on']);
					$currentTimeSeconds=time();
					if($currentTimeSeconds-$drobeTimeSeconds>$seconds)
					{
						$return="Last drobe ".$drobe_data['Drobe']['id']." is not uploaded within ".$seconds." seconds.";
						$GLOBALS['vote_return']=$return;
						return $return;
					}
				}
				//view increament by 1 for selected drobe
				$this->Drobe->recursive=-1;
				$updateFields=array();
				$updateFields["Drobe.views"]="Drobe.views + 1 ";
				$this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_data['Drobe']['id']));
					
				$response=$this->do_rate($user_id,$drobe_data['Drobe']['id'],$rate);
				if($response['type']=="success")
				{
					$rate_type = ($rate==1? "Cute" : ($rate<0 ? "Hot": "Skip"));
					$return = $user_data['User']['username']." voted ".$rate_type." on drobe id ".$drobe_data['Drobe']['id'];
				}
				else $return= "OOPS, something happens wrong try again later";
			}
			else
			{
				$return = $user_data['User']['username']." voted all the drobes";
			}
		}
		else
		{
			$return= "Entered user id not exist or invalid";
		}
		$GLOBALS['vote_return']=$return;
		return $return;
	}
	/*
	 * Webservice for do rate
	 */
	function do_rate($user_id=null,$drobe_id=null,$rate=null)
	{
		/*
		 * Patched for do_rate method
		 */
		$return_value=false;
		if($drobe_id>0)
		{
			$return_value = true;
		}
		$response=array();
		if(intval($user_id)>0)
		{
			if($return_value==true || !empty($this->data))
			{
				$this->Drobe->recursive=-1;
				// patced for do_vote
				$tmp_drobe_id= $drobe_id>0 ? $drobe_id : $this->data['drobe_id'];
				$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$tmp_drobe_id)));
				$data=array();
				$data['Rate']['user_id']=$user_id;
				if($drobe_id>0)
				{
					$data['Rate']['drobe_id']=$drobe_id;
					$data['Rate']['rate']=$rate;
					$data['Rate']['comment']="";
				}
				else
				{
					$data['Rate']['drobe_id']=$this->data['drobe_id'];
					$data['Rate']['rate']=$this->data['rate'];
					$data['Rate']['comment']=$this->data['comment'];
				}
				/* if($data['Rate']['rate']==0)
				{
					$data['Rate']['comment']="";
				} */
				if($data['Rate']['comment']!="")
				{
					$data['Rate']['conversation_id']=uniqid();
					$data['Rate']['conversation_status']="open";
					$data['Rate']['new_reply']=1; // for displaying badge 1 on new comment
				}
				else $data['Rate']['conversation_status']="close";
				
				
				
				
				if($this->Drobe->Rate->find('count',array('conditions'=>array('Rate.user_id'=>$user_id,'Rate.drobe_id'=>$tmp_drobe_id)))==0)
				{	
					$data['Rate']['last_conversation_time']=date('Y-m-d H:i:s');
					
					if($this->Drobe->Rate->save($data['Rate']))
					{
						// updates total fields for drobe uploader
						$update_total=array();
						if($data['Rate']['rate']<0) $update_total[]='total_out';
						else if($data['Rate']['rate']>0) $update_total[]='total_in';
						$this->Drobe->User->updateUserTotalField($drobeData['Drobe']['user_id'],$update_total,1);
						
						// updates total fields for voter
						if($data['Rate']['rate']==0)
						{
							$update_total=array();
						}
						else
						{
							$update_total=array('vote_given');
						}
						
						if($data['Rate']['comment']!="") $update_total[]='feedback_given';
						$this->Drobe->User->updateUserTotalField($user_id,$update_total,1);
						
						// updates total fields for received comments to uploaded drobe user @13/01/2014
						$update_total=array();
						if($data['Rate']['comment']!="") $update_total[]='feedback_received';
						$this->Drobe->User->updateUserTotalField($drobeData['Drobe']['user_id'],$update_total,1);
						
						
						$updateFields=array();
						if($data['Rate']['rate']!=0)
						{
							if($data['Rate']['rate']>0)
							{
								$updateFields["Drobe.total_in"]="Drobe.total_in + 1 ";
								$drobeData['Drobe']['total_in']++;
							}
							else
							{
								$updateFields["Drobe.total_out"]="Drobe.total_out + 1 ";
								$drobeData['Drobe']['total_out']++;
							}
							$updateFields["Drobe.total_rate"]="Drobe.total_rate + 1 ";
						}
						if($data['Rate']['comment']!="")
						{
							$updateFields['Drobe.total_comments']="Drobe.total_comments + 1 ";
							
							//$updateFields["Drobe.new_response"]="Drobe.new_response + 1 ";
							// send pushnotification to uploader
							if($this->getUserSetting($drobeData['Drobe']['user_id'],'got_comment'))
							{
								$this->send_notification_user("New feedback recieved",$drobeData['Drobe']['user_id'],array('action'=>"got_feedback",'drobe_id'=>$drobeData['Drobe']['id']));
							}
						}
						if(!empty($updateFields))
						{
							$this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobeData['Drobe']['id']));
						}
						
						//updating rating_index
						$this->Drobe->updateDrobeRateIndex($drobeData['Drobe']['id']);
						
						
						$response['type']="success";
						$response['message']="Rate submitted sccessfully";
						
						$total=$drobeData['Drobe']['total_in']+$drobeData['Drobe']['total_out'];
						/*$inPercent= $total>0 ? round($drobeData['Drobe']['total_in']/$total*100) : 0 ;
						$outPercent= $total>0 ? round($drobeData['Drobe']['total_out']/$total*100) : 0 ;*/
						$inPercent= $drobeData['Drobe']['total_in'];
						$outPercent= $drobeData['Drobe']['total_out'];
						
						$response['summary']=array();
						$response['summary']['your_rate']=$data['Rate']['rate'];
						$response['summary']['rate_in']=$inPercent;
						$response['summary']['rate_out']=$outPercent;
						
						$this->loadModel("Rate");
						$ddd = $this->Rate->find("all",array("conditions"=>array("Rate.user_id"=>$user_id)));
						
					}
					else
					{
						$response['type']="error";
						$response['message']="Error occured in submit your rate, try again later";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']="This drobe already rated by you";
						
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="parameters not found";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="user id not provided in url";
		}
		if($return_value==true)
		{
			return $response;
		}
		else
		{
			$this->set('response',$response);
			$this->set('_serialize',array('response'));
		}
	}
	function increament_view($drobe_id=null,$user_id=null)
	{
		$response=array();
		if($drobe_id!=null)
		{
			if($user_id!=null)
			{
				// counting views of drobes
				$updateFields=array();
				$updateFields["Drobe.views"]="Drobe.views + 1 ";
				if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
				{
					//load drobe and respond total_in and total_out for device side calculation
					$this->Drobe->recursive=-1;
					$this->Drobe->id=$drobe_id;
					$drobeData=$this->Drobe->read();
					$response['drobe_id']=$drobe_id;
					$response['drobe_total_in']=$drobeData['Drobe']['total_in'];
					$response['drobe_total_out']=$drobeData['Drobe']['total_out'];
					
					$response['following_drobe']= $this->_removeNotifyFollowers($drobe_id, $user_id) ? "1" : "0"; 
					$response['type']="success";
					$response['message']="drobe views updated successfully";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="user id not provided in url";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="drobe id not provided in url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function admin_reset_highest_rate(){
		$this->Drobe->recursive=-1;
		
		//find highest rated drobe with new logic
		$highestRated = $this->Drobe->find('first',array(
				'conditions'=>array('uploaded_on >='=>date('Y-m-d H:i:s',strtotime('-2 months')),
									'uploaded_on <='=>date('Y-m-d H:i:s',strtotime('-1 months'))),
				'order'=>array('Drobe.rate_index DESC')
				));
		
		//updated drobe with increment by 1 highest rated
		$highestRatedUpdate=array();
		$highestRatedUpdate["Drobe.is_highest_rated"]="Drobe.is_highest_rated + 1 ";
		$this->Drobe->updateAll($highestRatedUpdate,array('Drobe.id'=>$highestRated['Drobe']['id']));
		
		//give 1000 ebx points to highest rated drobe user
		$ebxOnHighestRated=Configure::read('setting.ebx_on_highest_rated');
		$updateFieldsUserTotal=array();
		$updateFieldsUserTotal["UserTotal.current_ebx_point"]="UserTotal.current_ebx_point + ".$ebxOnHighestRated;
		$updateFieldsUserTotal["UserTotal.earned_ebx_point"]="UserTotal.earned_ebx_point + ".$ebxOnHighestRated;
		$this->loadModel('UserTotal');
		$userTotalsData=$this->UserTotal->find('first',array('conditions'=>array('UserTotal.user_id'=>$highestRated['Drobe']['user_id'])));
		$this->UserTotal->updateAll($updateFieldsUserTotal,array('UserTotal.user_id'=>$highestRated['Drobe']['user_id']));
		$this->ebx_transactions_add("highest_rated_drobe",$highestRated['Drobe']['user_id'],$ebxOnHighestRated,$userTotalsData['UserTotal']['current_ebx_point']+$ebxOnHighestRated,$highestRated['Drobe']['id'],"Selected as Highest Rated Drobe.");
		
		
		//add highest rated drobe to month wise highest rated drobe
		$this->loadModel("HighestRatedDrobe");
		$drobeData=array();
		$drobeData['HighestRatedDrobe']=array();
		$drobeData['HighestRatedDrobe']['drobe_id']=$highestRated['Drobe']['id'];
		$drobeData['HighestRatedDrobe']['in']=$highestRated['Drobe']['total_in'];
		$drobeData['HighestRatedDrobe']['out']=$highestRated['Drobe']['total_out'];
		$drobeData['HighestRatedDrobe']['rate']=$highestRated['Drobe']['total_rate'];
		$drobeData['HighestRatedDrobe']['index']=$highestRated['Drobe']['rate_index'];
		$drobeData['HighestRatedDrobe']['views']=$highestRated['Drobe']['views'];
		$this->HighestRatedDrobe->save($drobeData);
		
		//send push notification to all (broadcast)
		$this->loadModel('User');
		$selectedUser=$this->User->find('first',array('conditions'=>array('User.id'=>$highestRated['Drobe']['user_id'])));
		$selectedUser['User']['fullname']=$selectedUser['User']['username'];
		$this->log("Selected User".print_r($selectedUser,true),"Reset_Higest_Rated");
		$message="Congratulations to ".$selectedUser['User']['fullname']." for winning this month's Highest Rated post, Got $".$ebxOnHighestRated." Everbucks!";
		// fetching user ids based on sendto option
		$conditions=array();
		//$conditions['User.status']=array("active","new","inactive");
		$conditions['UserSetting.apple_device_token != ']="";
		
		// unbinding unnecessory models for fetching device token of all users
		$this->User->unbindModel(array('hasMany'=>array("Drobe","Rate","Follower","StreamCategory")));
		
		$this->User->UserSetting->recursive=-1;
		$user_data=$this->User->UserSetting->find('all',array('fields'=>array("UserSetting.user_id,UserSetting.apple_device_token","UserSetting.device_type","UserSetting.badge"),'conditions'=>$conditions));
		$this->log("UserDataForNotify".print_r($user_data,true),"Reset_Higest_Rated");
		if($user_data)
		{
			$user_ids=array();
			$notification_ios=array();
			$notification_android=array();
			foreach ($user_data as $user)
			{
				if($user["UserSetting"]["device_type"]!="android")
				{
					$notification_ios[]=array(
							"message"=>$message,
							"token"=>$user["UserSetting"]["apple_device_token"],
							"badge"=> $user["UserSetting"]['badge']+1,
							"params"=>array(
									"action"=>"awarded",
									'user_id'=>$highestRated['Drobe']['user_id']
							)
					);
					$user_ids[]=$user["UserSetting"]["user_id"];
				}
				else
				{
					$notification_android[]=$user["UserSetting"]["apple_device_token"];
				}
			}
			$this->log("Notification iOS".print_r($notification_ios,true),"Reset_Higest_Rated");
			$this->log("Notification Android".print_r($notification_android,true),"Reset_Higest_Rated");
			if(count($notification_ios)>0)
			{
				$this->send_notification_bulk($notification_ios);
				$this->User->UserSetting->updateAll(array('badge'=>'badge + 1'),array('user_id'=>$user_ids));
			}
			if(count($notification_android)>0)
			{
				$notification=array(
						'tokens'=>$notification_android,
						"message"=>array(
								"message"=>$message,
								"action"=>"awarded",
								'user_id'=>$highestRated['Drobe']['user_id']
						)
				);
				$this->send_notification_bulk_android($notification);
			}
		}
		
		$this->Session->setFlash("Highest rated drobe selected");
		$this->redirect(array('controller'=>'admins','action'=>'index','admin'=>true));
	}
	
	function admin_reset_highest_rate_old(){
		$this->Drobe->recursive=-1;
		//find highest rated drobe
		$highestRated = $this->Drobe->find('first',array(				
				'order'=>array('Drobe.rate_index DESC',
								'Drobe.total_rate DESC',
								'Drobe.total_in DESC',
								'Drobe.views DESC')));

		//updated drobe with increment by 1 highest rated
		$highestRatedUpdate=array();
		$highestRatedUpdate["Drobe.is_highest_rated"]="Drobe.is_highest_rated + 1 ";
		$this->Drobe->updateAll($highestRatedUpdate,array('Drobe.id'=>$highestRated['Drobe']['id']));
		
		//give 1000 ebx points to highest rated drobe user
		$ebxOnHighestRated=Configure::read('setting.ebx_on_highest_rated');
		$updateFieldsUserTotal=array();
		$updateFieldsUserTotal["UserTotal.current_ebx_point"]="UserTotal.current_ebx_point + ".$ebxOnHighestRated;
		$updateFieldsUserTotal["UserTotal.earned_ebx_point"]="UserTotal.earned_ebx_point + ".$ebxOnHighestRated;
		$this->loadModel('UserTotal');
		$userTotalsData=$this->UserTotal->find('first',array('conditions'=>array('UserTotal.user_id'=>$highestRated['Drobe']['user_id'])));
		$this->UserTotal->updateAll($updateFieldsUserTotal,array('UserTotal.user_id'=>$highestRated['Drobe']['user_id']));
		$this->ebx_transactions_add("highest_rated_drobe",$highestRated['Drobe']['user_id'],$ebxOnHighestRated,$userTotalsData['UserTotal']['current_ebx_point']+$ebxOnHighestRated,$highestRated['Drobe']['id'],"Selected as Highest Rated Drobe.");
		
		
		//add highest rated drobe to month wise highest rated drobe
		$this->loadModel("HighestRatedDrobe");
		$drobeData=array();
		$drobeData['HighestRatedDrobe']=array();
		$drobeData['HighestRatedDrobe']['drobe_id']=$highestRated['Drobe']['id'];
		$drobeData['HighestRatedDrobe']['in']=$highestRated['Drobe']['total_in'];
		$drobeData['HighestRatedDrobe']['out']=$highestRated['Drobe']['total_out'];
		$drobeData['HighestRatedDrobe']['rate']=$highestRated['Drobe']['total_rate'];
		$drobeData['HighestRatedDrobe']['index']=$highestRated['Drobe']['rate_index'];
		$drobeData['HighestRatedDrobe']['views']=$highestRated['Drobe']['views'];
		$this->HighestRatedDrobe->save($drobeData);
		
		//reset all drobes in, out, views, rate, rate_index
		$updateFields=array();
		$updateFields['Drobe.total_in']=0;
		$updateFields['Drobe.total_out']=0;
		$updateFields['Drobe.total_rate']=0;
		$updateFields['Drobe.rate_index']=0;
		$updateFields['Drobe.views']=0;
		$this->Drobe->updateAll($updateFields);
		
		
		//send push notification to user
		$this->send_notification_user("Your outfit was the highest rated last month. You got a bonus of $".$ebxOnHighestRated." EverBucks!", $highestRated['Drobe']['user_id'],array('action'=>'awarded'));
		
		/*$this->loadModel('UserSetting');
		$this->UserSetting->recursive=-1;
		$user=$this->UserSetting->find('first',array('conditions'=>array('UserSetting.user_id'=>$highestRated['Drobe']['user_id'])));
		if($user["Setting"]["apple_device_token"]!="")
		{
			$notification=array();
				$notification[]=array(
						"message"=>"Your outfit was the highest rated last month. You got a bonus of $".$ebxOnHighestRated." EverBucks!",
						"token"=>$user["Setting"]["apple_device_token"],
						"badge"=>$user["Setting"]['badge']+1,
						"params"=>array()
				);
				$user_ids[]=$user["Setting"]["user_id"];
			$this->send_notification_bulk($notification);
			$this->UserSetting->updateAll(array('badge'=>'badge + 1'),array('user_id'=>$user["Setting"]["user_id"]));
		}*/
		$this->Session->setFlash("Highest rated drobe selected");		
		$this->redirect(array('controller'=>'admins','action'=>'index','admin'=>true));
	}
	/*
	 * script for update season of drobe in old databse
	 */
	function admin_update_season()
	{
		$this->Drobe->recursive=-1;
		$allDrobes = $this->Drobe->find('all');
		foreach($allDrobes as $drobe)
		{
			$updateFields=array();
			if(in_array(date('n',strtotime($drobe['Drobe']['uploaded_on'])),array(6,7,8))) $updateFields["Drobe.season"]="'summer'";
			if(in_array(date('n',strtotime($drobe['Drobe']['uploaded_on'])),array(9,10,11))) $updateFields["Drobe.season"]="'fall'";
			if(in_array(date('n',strtotime($drobe['Drobe']['uploaded_on'])),array(12,1,2))) $updateFields["Drobe.season"]="'winter'";
			if(in_array(date('n',strtotime($drobe['Drobe']['uploaded_on'])),array(3,4,5))) $updateFields["Drobe.season"]="'spring'";
			$this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe['Drobe']['id']));
		}
		$this->Session->setFlash("Updated successfully");
		$this->redirect(array('controller'=>'admins','action'=>'index','admin'=>true));
	}
	
	function rate_response()
	{
		if(!empty($this->data))
		{
			$comment_error=false;
			if($this->data['rate']==0)
			{
				$this->request->data['comment']="";
			}
			if($this->data['rate']==2 && $this->data['comment']=="")
			{
				$this->set("comment_need",true);
				$comment_error=true;
			}
			elseif($this->data['rate']==2 && $this->data['comment']!="")
			{
				$this->request->data['rate']=0;
				$this->set("only_commented",true);
			}
			if(!$comment_error)
			{
			
				$this->Drobe->recursive=-1;
				$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$this->data['drobe_id'])));
				$data=array();
				$data['Rate']['drobe_id']=$drobeData['Drobe']['id'];
				$data['Rate']['user_id']=$this->Auth->user('id');
				$data['Rate']['rate']=$this->data['rate'];
				$data['Rate']['comment']=$this->data['comment'];
				
				
				
				if($data['Rate']['comment']!="")
				{
					$data['Rate']['conversation_id']=uniqid();
					$data['Rate']['conversation_status']="open";
					$data['Rate']['new_reply']=1;
				}
				else $data['Rate']['conversation_status']="close";
				
				$data['Rate']['last_conversation_time']=date('Y-m-d H:i:s');
				
				if($this->Drobe->Rate->find('count',array('conditions'=>array('Rate.drobe_id'=>$data['Rate']['drobe_id'],'Rate.user_id'=>$data['Rate']['user_id'])))>0)
				{
					$this->set('already_rated',true);
				}
				else if($this->Drobe->Rate->save($data['Rate']))
				{
					// updates total fields for drobe uploader
					$update_total=array();
					if($this->data['rate']>0) $update_total[]='total_in';
					else if($this->data['rate']<0) $update_total[]='total_out';
					$this->Drobe->User->updateUserTotalField($drobeData['Drobe']['user_id'],$update_total,1);
					
					// updates total fields for voter
					if($this->data['rate']==0)
					{
						$update_total=array();
					}
					else
					{
						$update_total=array('vote_given');
					}
					
					if($data['Rate']['comment']!="") $update_total[]='feedback_given';
					$this->Drobe->User->updateUserTotalField($this->Auth->user('id'),$update_total,1);
					
					// update received comment to uploaded drobe user @13/01/2014
					$update_total = array();
					if($data['Rate']['comment']!="") $update_total[]='feedback_received';
					$this->Drobe->User->updateUserTotalField($drobeData['Drobe']['user_id'],$update_total,1);
					
					//update uploader total
	
					$updateFields=array();
					if($this->data['rate']!=0)
					{
						if($this->data['rate']>0)
						{
							$updateFields["Drobe.total_in"]="Drobe.total_in + 1 ";
							$drobeData['Drobe']['total_in']++;
						}
						else 
						{
							$updateFields["Drobe.total_out"]="Drobe.total_out + 1 ";
							$drobeData['Drobe']['total_out']++;
						}
						$updateFields["Drobe.total_rate"]="Drobe.total_rate + 1 ";
					}
					if($data['Rate']['comment']!="")
					{
						$updateFields['Drobe.total_comments']="Drobe.total_comments + 1 ";
						if($drobeData['Drobe']['user_id']!=$this->Auth->user('id'))
						{
							//$updateFields["Drobe.new_response"]="Drobe.new_response + 1 ";
							
							// send pushnotification to uploader
							if($this->getUserSetting($drobeData['Drobe']['user_id'],'got_comment'))
							{
								$this->send_notification_user("New feedback recieved",$drobeData['Drobe']['user_id'],array('action'=>"got_feedback",'drobe_id'=>$drobeData['Drobe']['id']));
							}
						}
						
					}
					if(!empty($updateFields))
					{
						$this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobeData['Drobe']['id']));
					}
					
					//updating rating_index
					$this->Drobe->updateDrobeRateIndex($drobeData['Drobe']['id']);
					
					$this->set('already_rated',false);
					
				}
				$total=$drobeData['Drobe']['total_in']+$drobeData['Drobe']['total_out'];
				/*$inPercent=$total>0 ? round($drobeData['Drobe']['total_in']/$total*100) : 0;
				$outPercent=$total>0 ? round($drobeData['Drobe']['total_out']/$total*100) : 0;*/
				$inPercent=$drobeData['Drobe']['total_in'];
				$outPercent=$drobeData['Drobe']['total_out'];
				$this->set('your_rate',intval($this->data['rate']));
				$this->set('rate_in',$inPercent);
				$this->set('rate_out',$outPercent);
			}
		}
	}
	function close()
	{
		$response=array();
		if(!empty($this->data))
		{
			$this->Drobe->recursive=-1;
			$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$this->data['drobe_id'],'Drobe.user_id'=>$this->Auth->user('id'))));
			$this->Drobe->id=$drobeData['Drobe']['id'];
			$response['data']=$drobeData;
			$this->Drobe->recursive=0;
			
			$updateFields=array('Drobe.rate_status'=>"'close'","Drobe.new_response"=>0);
			
			if($drobeData['Drobe']['post_type']=='sell')
			{
				$updateFields['Drobe.post_type']="'post'";
				/*$this->loadModel("SellDrobe");
				 $updateFields=array('SellDrobe.status'=>"'pending'");
				$this->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$drobeData['Drobe']['id']));*/
			}
			
			if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobeData['Drobe']['id'])))
			{
				$this->Drobe->recursive=0;
				$this->Drobe->User->updateUserTotalField($drobeData['Drobe']['user_id'],'total_drobes',-1);
				
				// cloading all conversations and updateing badges
				$this->loadModel('Rate');
				//$updateFields=array('Rate.conversation_id'=>"''",'Rate.conversation_status'=>"'close'",'Rate.new_reply'=>0,'Rate.new_response'=>0);
				$updateFields=array('Rate.conversation_status'=>"'close'",'Rate.new_reply'=>0,'Rate.new_response'=>0);
				$this->Rate->updateAll($updateFields,array('Rate.drobe_id'=>$drobeData['Drobe']['id']));
				
				if($drobeData['Drobe']['post_type']=='sell')
				{
					$this->loadModel('SellDrobe');
					$sell_drobe = $this->SellDrobe->find('first',array('fields'=>array('shopify_product_id'),'conditions'=>array('SellDrobe.drobe_id'=>$drobeData['Drobe']['id'])));
					
					/* Remove image from shop but not from application */
					$shopify_product_id = $sell_drobe['SellDrobe']['shopify_product_id'];
					$res=$this->shopify("/products/".$shopify_product_id,"","DELETE");
				}
				
				$this->_removeNotifyFollowers($drobeData['Drobe']['id']);
				//$this->_removeDrobeConversations($drobeData['Drobe']['id']);
				$this->Drobe->updateRecentCache();
				
				
				$response['type']="success";
				$response['message']="Flag applied successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="Opps error occured in sumbit your flag for this drobe please try again later";
			}
				
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		echo json_encode($response);
		exit();
		
	}
	/*
	 * webservice for close drobe for rate
	 */
	function close_drobe($drobe_id=null)
	{
		$response=array();
		if($drobe_id>0)
		{
			
			$this->Drobe->recursive=-1;
			$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobe_id)));
			//$this->Drobe->id=$drobeData['Drobe']['id'];
				
			$updateFields=array('Drobe.rate_status'=>"'close'","Drobe.new_response"=>0);
			if($drobeData['Drobe']['post_type']=='sell')
			{
				$updateFields['Drobe.post_type']="'post'";
				/*$this->loadModel("SellDrobe");
				$updateFields=array('SellDrobe.status'=>"'pending'");
				$this->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$drobeData['Drobe']['id']));*/
			}			
			
			
			if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
			{
				$this->Drobe->recursive=0;
				$this->Drobe->User->updateUserTotalField($drobeData['Drobe']['user_id'],'total_drobes',-1);
				
				// closing all conversations and updateing badges
				$this->loadModel('Rate');
				//$updateFields=array('Rate.conversation_id'=>"''",'Rate.conversation_status'=>"'close'",'Rate.new_reply'=>0,'Rate.new_response'=>0);
				$updateFields=array('Rate.conversation_status'=>"'close'",'Rate.new_reply'=>0,'Rate.new_response'=>0);
				$this->Rate->updateAll($updateFields,array('Rate.drobe_id'=>$drobe_id));
				
				if($drobeData['Drobe']['post_type']=='sell')
				{
					$this->loadModel('SellDrobe');
					$sell_drobe = $this->SellDrobe->find('first',array('fields'=>array('shopify_product_id'),'conditions'=>array('SellDrobe.drobe_id'=>$drobeData['Drobe']['id'])));
					
					/* Remove image from shop but not from application */
					$shopify_product_id = $sell_drobe['SellDrobe']['shopify_product_id'];
					$res=$this->shopify("/products/".$shopify_product_id,"","DELETE");
				}
				
				// remove notification badges for the follower
				$this->_removeNotifyFollowers($drobe_id);
				// remove conversations
				//$this->_removeDrobeConversations($drobe_id);
				$this->Drobe->updateRecentCache();
				
				$response['type']="success";
				$response['message']="Drobe closed successfully for rating.";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in closing drobe for rate.";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide drobe id in url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
	}
	function flag()
	{
		$response=array();
		if(!empty($this->data))
		{
			
			$data=array();
			$data['Flag']=$this->data;
			$data['Flag']['user_id']=$this->Auth->user('id');
			$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$data['Flag']['drobe_id'])));
			$data['Flag']['drobe_id']=$drobeData['Drobe']['id'];
			if($this->Drobe->Flag->save($data['Flag']))
			{
				$response['type']="success";
				$response['message']="Flag applied successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="Opps error occured in sumbit your flag for this drobe please try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		echo json_encode($response);
		exit();
		
	}
	function add_to_flag($drobe_id=null)
	{
		$response=array();
		if(!empty($this->data))
		{
			$data=array();
			$data['Flag']=$this->data;
			if(!isset($data['Flag']['flag_category_id']))
			{
				$data['Flag']['flag_category_id']=$data['Flag']['flag_catefory_id'];
			}
			$data['Flag']['drobe_id']=$drobe_id;
			if($this->Drobe->Flag->save($data['Flag']))
			{
				$response['type']="success";
				$response['message']="Flag applied successfully";
				
			}
			else
			{
				$response['type']="error";
				$response['message']="Opps error occured in sumbit your flag for this drobe please try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
	}
	function faves($user_unique_id=null)
	{
		//This code for search filter
		$this->loadModel('UserFilter');
		$userFilter  = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
		$this->set('userFilter',$userFilter);
		
		
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		
		// loading size list from cache
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		$sizeList = Cache::read('size_list');
		$this->set('sizeList',$sizeList);
		
		
		// loading brand list from cache @sadikhasan
		if(Cache::read('brand_list')==null)
		{
			$this->loadModel('Brand');
			$this->Brand->_updateBrandCache();
		}
		
		if($user_unique_id!=null)
		{
			$user_full_name=$this->Drobe->User->field('concat(User.username)',array("User.unique_id"=>$user_unique_id));
			if($user_full_name==false)
			{
				$user_full_name=$this->Drobe->User->field("concat('User.first_name','User.lastname')",array("User.unique_id"=>$user_unique_id));
			}
			$this->set('user_unique_id',$user_unique_id);
			$this->set('title',$user_full_name."'s Favourite");
			$this->set('title_for_layout',"My Favourite Drobes of ".$user_full_name);
		}
		else
		{
			$this->set('user_unique_id',$this->Auth->user('unique_id'));
			$this->set('title',"My Favourite");
			$this->set('title_for_layout',"My Favourite Drobes");
		}
		
	}
	function results()
    {
        // counting new rate response on my uploaded drobe
        $new_rate_response=array_pop(array_pop($this->Drobe->find('all',array('fields'=>array('sum(Drobe.new_response) as total'),'conditions'=>array("Drobe.user_id"=>$this->Auth->user('id'),'Drobe.rate_status'=>"open",'Drobe.deleted'=>0)))));
        //counting gave feedback conversations new reponse
        $new_conversation_response=array_pop(array_pop($this->Drobe->Rate->find('all',array('fields'=>array('sum(Rate.new_response) as total'),'conditions'=>array("Rate.user_id"=>$this->Auth->user('id'),'Rate.conversation_status'=>"open")))));
        // counting new conversations reply from my drobe conversations
        $mydrobes=$this->Drobe->find('list',array('fields'=>array('Drobe.id'),'conditions'=>array('Drobe.user_id'=>$this->Auth->user('id'),'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
        $new_conversation_reply=array_pop(array_pop($this->Drobe->Rate->find('all',array('fields'=>array('sum(Rate.new_reply) as total'),'conditions'=>array("Rate.drobe_id"=>$mydrobes,'Rate.conversation_status'=>"open")))));
        $this->log(print_r($new_conversation_reply,true),"test_bug");
        $this->set('new_rate_reponse',  $new_rate_response['total']+$new_conversation_reply['total']);
        $this->set('gave_feedback_reponse',$new_conversation_response['total']);
        $this->set('title_for_layout', "Drobe Results" );
    }
	
	/*
	 *
	* Webservice for result notification which is display within got feedback and gave feedback
	*/
	function result_notification($user_id=null)
	{
		$response=array();
		if($user_id>0)
		{
			// counting new rate response on my uploaded drobe
			$new_rate_response=array_pop(array_pop($this->Drobe->find('all',array('fields'=>array('sum(Drobe.new_response) as total'),'conditions'=>array("Drobe.user_id"=>$user_id,'Drobe.rate_status'=>"open",'Drobe.deleted'=>0)))));
			
			// counting new conversations reply from my drobe conversations
			$mydrobes=$this->Drobe->find('list',array('fields'=>array('Drobe.id'),'conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
			$new_conversation_reply=array_pop(array_pop($this->Drobe->Rate->find('all',array('fields'=>array('sum(Rate.new_reply) as total'),'conditions'=>array("Rate.drobe_id"=>$mydrobes,'Rate.conversation_status'=>"open")))));
			
			//counting gave feedback conversations new reponse
			$new_conversation_response=array_pop(array_pop($this->Drobe->Rate->find('all',array(
							'fields'=>array('sum(Rate.new_response) as total'),
					
					'joins'=>array(
							array(
									'table' => 'drobes',
									'alias' => 'Drobe',
									'type' => 'LEFT',
									'conditions' => array(
											'Drobe.id = Rate.drobe_id')
							)),
					
							'conditions'=>array("Rate.user_id"=>$user_id,'Rate.conversation_status'=>"open",'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')
					
							))));
			
			
			$response['type']="success";
			$response['notification']=array();
			$response['notification']['got_feedback']="".intval($new_rate_response['total']+$new_conversation_reply['total']);
			$response['notification']['gave_feedback']="".intval($new_conversation_response['total']);
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function result($drobe_id=null,$result="all")
	{
		$conditions=array();
		$conditions['Drobe.user_id']=$this->Auth->user('id');
		$conditions['Drobe.unique_id']=$drobe_id;
		$conditions['Drobe.rate_status']='open';
		$drobeData=$this->Drobe->find('first',array(
			'fields'=>array('Drobe.id','Drobe.unique_id','Drobe.uploaded_on','Drobe.user_id','Drobe.is_highest_rated','Drobe.category_id','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.total_out'),
			'conditions'=>$conditions
		));
		if($drobeData)
		{
			$total=$drobeData['Drobe']['total_in']+$drobeData['Drobe']['total_out'];
			/*$drobeData['Drobe']['total_in']=$total>0 ? round($drobeData['Drobe']['total_in']/$total*100) : 0;
			$drobeData['Drobe']['total_out']=$total>0 ? round($drobeData['Drobe']['total_out']/$total*100) : 0;*/
			$drobeData['Drobe']['total_in']=$drobeData['Drobe']['total_in'];
			$drobeData['Drobe']['total_out']=$drobeData['Drobe']['total_out'];
			$drobeData['Drobe']['neutral']=$this->Drobe->Rate->find('count',array('conditions'=>array("Rate.drobe_id"=>$drobeData['Drobe']['id'],'Rate.rate'=>0)));
			
			$this->Drobe->recursive=0;
			$this->Drobe->updateAll(array('Drobe.new_response'=>0),array('Drobe.id'=>$drobeData['Drobe']['id']));
				
			
			$this->set('drobeData',$drobeData['Drobe']);
			
			
			if(Cache::read('drobe_category')==null)
			{
				$this->loadModel('Category');
				$this->Category->_updateCategoryCache();
			}
			$this->set('categoryList',Cache::read('drobe_category'));
			$this->set('title_for_layout', "Result - ".$drobeData['Drobe']['comment'] );
		}
		else
		{
			throw new NotFoundException("Not found");
		}
	}
	function conversation($drobe_id,$conversation_id)
	{
		$conditions=array();
		$conditions['OR']['Drobe.user_id']=$this->Auth->user('id');
		$conditions['OR']['Rate.user_id']=$this->Auth->user('id');

		$conditions['Drobe.unique_id']=$drobe_id;
		$conditions['Rate.conversation_id']=$conversation_id;
		$conditions['Drobe.rate_status']='open';
		//$this->Drobe->recursive=1;
		$drobeData=$this->Drobe->Rate->find('first',array(
					'fields'=>array('Rate.*','Drobe.id','Drobe.user_id','Drobe.unique_id','Drobe.uploaded_on','Drobe.category_id','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.total_out',
					'User.unique_id','User.first_name','User.username','User.last_name','User.photo','User.star_user','User.gender','User.province','User.country','User.id'),
					'joins'=>array(
						array(
							'table' => 'drobes',
							'alias' => 'Drobe',
							'type' => 'INNER',
							'conditions' => array(
							'Drobe.id = Rate.drobe_id')
						),
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'INNER',
							'conditions' => array(
								'User.id = Rate.user_id'
							)
						)
					),
					'conditions'=>$conditions
		));
		
		if($drobeData)
		{
			//update new reposnse or new reply in conversations between two users
			$updateFields=array();
			if($drobeData['Rate']['user_id']==$drobeData['Drobe']['user_id'])
			{
				// if given rate by self
				$updateFields['Rate.new_response']=0;
				$updateFields['Rate.new_reply']=0;
			}
			else if($drobeData['Rate']['user_id']==$this->Auth->user('id'))
			{
				$updateFields['Rate.new_response']=0;
			}
			else
			{
				$updateFields['Rate.new_reply']=0;
			}
			$this->Drobe->Rate->updateAll($updateFields,array('Rate.id'=>$drobeData['Rate']['id']));
			
			
			$total=$drobeData['Drobe']['total_in']+$drobeData['Drobe']['total_out'];
			/*$drobeData['Drobe']['total_in']=$total>0 ? round($drobeData['Drobe']['total_in']/$total*100) : 0;
			$drobeData['Drobe']['total_out']=$total>0 ? round($drobeData['Drobe']['total_out']/$total*100) : 0;*/
			$drobeData['Drobe']['total_in']=$drobeData['Drobe']['total_in'];
			$drobeData['Drobe']['total_out']=$drobeData['Drobe']['total_out'];
			$drobeData['Drobe']['neutral']=$this->Drobe->Rate->find('count',array('conditions'=>array("Rate.drobe_id"=>$drobeData['Drobe']['id'],'Rate.rate'=>0)));
			$this->set('drobeData',$drobeData);
			if(Cache::read('drobe_category')==null)
			{
				$this->loadModel('Category');
				$this->Category->_updateCategoryCache();
			}
			$this->set('categoryList',Cache::read('drobe_category'));
		}
		else
		{
			throw new NotFoundException("Not Found");
		}
		$this->set('title_for_layout', "Conversations" );
	}
	
	
	function admin_conversation($drobe_id,$conversation_id)
	{
		$conditions=array();
		$conditions['Drobe.id']=$drobe_id;
		$conditions['Rate.conversation_id']=$conversation_id;
		$conditions['Drobe.rate_status']='open';
		//$this->Drobe->recursive=1;
		$drobeData=$this->Drobe->Rate->find('first',array(
				'fields'=>array('Rate.*','Drobe.id','Drobe.user_id','Drobe.unique_id','Drobe.category_id','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.total_out',
						'User.unique_id','User.first_name','User.username','User.last_name','User.photo','User.star_user','User.gender','User.province','User.country','User.id'),
				'joins'=>array(
						array(
								'table' => 'drobes',
								'alias' => 'Drobe',
								'type' => 'INNER',
								'conditions' => array(
										'Drobe.id = Rate.drobe_id')
						),
						array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'INNER',
								'conditions' => array(
										'User.id = Rate.user_id'
								)
						)
				),
				'conditions'=>$conditions
		));
	
		if($drobeData)
		{
			//update new reposnse or new reply in conversations between two users
			$updateFields=array();
			if($drobeData['Rate']['user_id']==$drobeData['Drobe']['user_id'])
			{
				// if given rate by self
				$updateFields['Rate.new_response']=0;
				$updateFields['Rate.new_reply']=0;
			}
			else if($drobeData['Rate']['user_id']==$this->Auth->user('id'))
			{
				$updateFields['Rate.new_response']=0;
			}
			else
			{
				$updateFields['Rate.new_reply']=0;
			}
			$this->Drobe->Rate->updateAll($updateFields,array('Rate.id'=>$drobeData['Rate']['id']));
				
				
			$total=$drobeData['Drobe']['total_in']+$drobeData['Drobe']['total_out'];
			/*$drobeData['Drobe']['total_in']=$total>0 ? round($drobeData['Drobe']['total_in']/$total*100) : 0;
			$drobeData['Drobe']['total_out']=$total>0 ? round($drobeData['Drobe']['total_out']/$total*100) : 0;*/
			$drobeData['Drobe']['neutral']=$this->Drobe->Rate->find('count',array('conditions'=>array("Rate.drobe_id"=>$drobeData['Drobe']['id'],'Rate.rate'=>0)));
			$this->set('drobeData',$drobeData);
			if(Cache::read('drobe_category')==null)
			{
				$this->loadModel('Category');
				$this->Category->_updateCategoryCache();
			}
			$categoryList=Cache::read('drobe_category');
			$this->set('categoryList',$categoryList);
			
			
			// getting list of conversation users
			$users=array();
			$users[]=$drobeData['Drobe']['user_id'];
			$users[]=$this->Drobe->Rate->field('user_id',array("conversation_id"=>$conversation_id));
			$users=$this->Drobe->User->find('all',array("fields"=>array("User.id","User.first_name","User.username","User.last_name"),"conditions"=>array("User.id"=>$users)));
			$conversation_users=array();
			foreach($users as $user)
			{
				$conversation_users[$user['User']['id']]=$user['User']['first_name']." ".$user['User']['last_name']." (".$user['User']['username'].")";
			}
			$this->set('conversation_users',$conversation_users);
			
		}
		else
		{
			$this->set('notFound',true);
		}
		$this->set('title_for_layout', "Conversations" );
		$this->set('drobeData',$drobeData);
		
		
		
	}
	/*
	 * webservice for Getting drobe conversation 
	 */
	function get_conversation($drobe_id=null,$user_id=null,$conversation_id=null)
	{
		$response=array();
		if($drobe_id!=null)
		{
			if($user_id!=null)
			{
				if($conversation_id!=null)
				{
					$conditions=array();
					$conditions['OR']['Drobe.user_id']=$user_id;
					$conditions['OR']['Rate.user_id']=$user_id;
				
					$conditions['Drobe.id']=$drobe_id;
					$conditions['Rate.conversation_id']=$conversation_id;
					$conditions['Drobe.rate_status']='open';
					//$this->Drobe->recursive=1;
					$drobeData=$this->Drobe->Rate->find('first',array(
							'fields'=>array('Rate.*','Drobe.id','Drobe.user_id','Drobe.category_id','Drobe.file_name','Drobe.comment','Drobe.total_rate',
									'Drobe.views','Drobe.uploaded_on','Drobe.total_in','Drobe.total_out','Category.category_name',
									'User.first_name','User.last_name','User.username','User.photo','User.star_user','User.gender','User.province','User.country','User.id'),
							'joins'=>array(
									array(
											'table' => 'drobes',
											'alias' => 'Drobe',
											'type' => 'INNER',
											'conditions' => array(
													'Drobe.id = Rate.drobe_id')
									),
									array(
											'table' => 'users',
											'alias' => 'User',
											'type' => 'INNER',
											'conditions' => array(
													'User.id = Rate.user_id'
											)
									),
									array(
											'table' => 'categories',
											'alias' => 'Category',
											'type' => 'INNER',
											'conditions' => array(
													'Category.id = Drobe.category_id'
											)
									)
							),
							'conditions'=>$conditions
					));
				
					if($drobeData)
					{
						//update new reposnse or new reply in conversations between two users
						$updateFields=array();
						
						
						if($drobeData['Rate']['user_id']==$drobeData['Drobe']['user_id'])
						{
								//if given rate by self
								$updateFields['Rate.new_response']=0;
								$updateFields['Rate.new_reply']=0;
						}
						else if($drobeData['Rate']['user_id']==$user_id)
						{
							$updateFields['Rate.new_response']=0;
						}
						else
						{
							$updateFields['Rate.new_reply']=0;
						}
						$this->Drobe->Rate->updateAll($updateFields,array('Rate.id'=>$drobeData['Rate']['id']));
						$total=$drobeData['Drobe']['total_in']+$drobeData['Drobe']['total_out'];
						$drobeData['Drobe']['total_in']="".$drobeData['Drobe']['total_in'];
						$drobeData['Drobe']['total_out']="".$drobeData['Drobe']['total_out'];
						$drobeData['Drobe']['neutral']=$this->Drobe->Rate->find('count',array('conditions'=>array("Rate.drobe_id"=>$drobeData['Drobe']['id'],'Rate.rate'=>0)));
						$drobeData['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobeData['Drobe']['file_name'],true);
						$drobeData['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobeData['Drobe']['file_name'],true);
						$drobeData['Drobe']['category_name']=$drobeData['Category']['category_name'];
						if($drobeData['User']['photo']!="")
						{
							$drobeData['User']['photo']=Router::url("/profile_images/thumb/".$drobeData['User']['photo'],true);;
						}
						unset($drobeData['Drobe']['file_name'],$drobeData['Category']);
						$response['drobe']=$drobeData;
						$this->loadModel("Conversation");
						$this->Conversation->recursive=-1;
						$conversations=$this->Conversation->find('all',array('fields'=>array("Conversation.user_id","Conversation.conversation","Conversation.created_on"),'conditions'=>array('Conversation.unique_id'=>$conversation_id)));
						$response['conversations']=array();
						
						App::import( 'Helper', 'Time' );
            			$time = new TimeHelper;
						
						
						foreach ($conversations as $conv) 
						{
							$conv['Conversation']['created_on']=$time->timeAgoInWords($conv['Conversation']['created_on']);
							$response['conversations'][]=$conv['Conversation']; 
						}
					}
					else
					{
						$response['type']="error";
						$response['message']="drobe not found";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']="provide conversation id in url";
				}
				
			}
			else
			{
				$response['type']="error";
				$response['message']="provide user id in url";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide drobe id in url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	/*
	 * 
	 * Webservice for Add to favourite orunfavourite drobe
	 * 
	 */
	function set_favourite($drobe_id=null,$user_id=null,$action="favourite")
	{
		$response=array();
		if($drobe_id!=null)
		{
			if($user_id!=null)
			{
				$data=array();
				$data['Favourite']=array();
				$data['Favourite']['user_id']=$user_id;
				$data['Favourite']['drobe_id']=$drobe_id;
				
				
				$this->Drobe->recursive=-1;
				$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobe_id)));
				$this->Drobe->recursive=0;
				if($action=="favourite")
				{
					try{
						$this->Drobe->Favourite->save($data['Favourite']);
						if($this->Drobe->Favourite->getAffectedRows())
						{
							if($this->getUserSetting($drobeData['Drobe']['user_id'],'drobe_faved'))
							{
								$params=array('action'=>"drobe_faved");
									
								//send push notification to the uploaded drobe user when it is faved by other user.
								$this->send_notification_user("Your drobe has been favorited",$drobeData['Drobe']['user_id'],$params);
							}
							
							// update users total
							$this->Drobe->User->updateUserTotalField($user_id,'total_favourites',1);
							
							// increament total favs
							$this->Drobe->id=$drobeData['Drobe']['id'];
							$this->Drobe->saveField('total_favs',$drobeData['Drobe']['total_favs']+1);
							
							
							// counting favourite drobe
							$response['count']=$drobeData['Drobe']['total_favs']+1;
							$response['type']="success";
							$response['message']="Added in your favourite list";
						}
						else
						{
							$response['type']="error";
							$response['message']="Opps error occured in your favourite list, please try again later";
						}
					}
					catch(PDOException $e)
					{
						$response['type']="error";
						$response['count']=$drobeData['Drobe']['total_favs'];
						$response['message']="Already exist in your favourite list";
					}
				}
				else if($action=="unfavourite")
				{
					if($this->Drobe->Favourite->deleteAll($data['Favourite']))
					{
						if($this->Drobe->Favourite->getAffectedRows())
						{
							// update users total
							$this->Drobe->User->updateUserTotalField($user_id,'total_favourites',-1);
							
							// increament total favs
							$this->Drobe->id=$drobeData['Drobe']['id'];
							$this->Drobe->saveField('total_favs',$drobeData['Drobe']['total_favs']-1);
							
							// counting favourite drobe
							$response['count']=$drobeData['Drobe']['total_favs']-1;
							$response['type']="success";
							$response['message']="Removed from your favourite list";
						}
						else
						{
							$response['count']=$drobeData['Drobe']['total_favs'];
							$response['type']="success";
							$response['message']="Removed from your favourite list";
						}
					}
					else
					{
						$response['type']="error";
						$response['message']="Opps error occured in your favourite list, please try again later";
					}	
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="provide user id in url";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide drobe id in url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function addtofaves()
	{
		$response=array();
		if(!empty($this->data))
		{
			$data=array();
			$data['Favourite']=$this->data;
			$data['Favourite']['user_id']=$this->Auth->user('id');
			
			$this->Drobe->recursive=0;
			$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$data['Favourite']['drobe_id'])));
			
			$data['Favourite']['drobe_id']=$drobeData['Drobe']['id'];
			if($this->Drobe->Favourite->save($data['Favourite']))
			{
				// update users total
				$this->Drobe->User->updateUserTotalField($this->Auth->user('id'),'total_favourites',1);
				
				
				// increament total favs
				$this->Drobe->id=$drobeData['Drobe']['id'];
				$this->Drobe->saveField('total_favs',$drobeData['Drobe']['total_favs']+1);
				
				if($this->getUserSetting($drobeData['Drobe']['user_id'],'drobe_faved'))
				{
					$params=array('action'=>"drobe_faved");
					
					//send push notification to the uploaded drobe user when it is faved by other user.
					$this->send_notification_user("Your drobe has been favorited",$drobeData['Drobe']['user_id'],$params);
				}
				
				if($this->Session->read('UserSetting.fb_connected') && $this->Session->read('UserSetting.fb_post_fave'))
				{
					$content_pages=Cache::read('content_pages');
					$post = array(
							'picture' => Router::url('/drobe_images/thumb/'.$drobeData['Drobe']['file_name'],true),
							'link' => Router::url('/rate/'.$drobeData['Drobe']['unique_id'],true),
							'name' => $drobeData['Drobe']['comment'],
							'caption' => 'www.everdrobe.com',
							'message' => "",
							'description' => $content_pages['meta_description']['page_content']
					);
					try{
					$this->facebook_post($post);
					}
					catch(Exception $e){
					}
					
				}
				
				if($this->Session->read('UserSetting.tw_connected') && $this->Session->read('UserSetting.tw_post_fave'))
				{
					$message=$drobeData['Drobe']['comment'].", ".$drobeData['Drobe']['short_url'];
					try{
						$image_path= WWW_ROOT.Configure::read('drobe.upload_dir').DS.$drobeData['Drobe']['file_name'];
						//$this->twitter_post($message,$image_path);
						$this->twitter_post($message);
					}
					catch(Exception $e){
					}
				}
				
				// counting favourite drobe
				$response['count']=$this->convertToReadeable($drobeData['Drobe']['total_favs']+1);
				
				$response['type']="success";
				$response['message']="Added in your favourite list";
			}
			else
			{
				$response['type']="error";
				$response['message']="Opps error occured in your favourite list, please try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		echo json_encode($response);
		exit();
	}
	function removefromfaves()
	{
		
		$response=array();
		if(!empty($this->data))
		{
	
			$data=array();
			$data['Favourite']=$this->data;
			$data['Favourite']['user_id']=$this->Auth->user('id');
			
			$this->Drobe->recursive=0;
			$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$data['Favourite']['drobe_id'])));
			$data['Favourite']['drobe_id']=$drobeData['Drobe']['id'];
			if($this->Drobe->Favourite->deleteAll($data['Favourite']))
			{
				// update users total
				$this->Drobe->User->updateUserTotalField($this->Auth->user('id'),'total_favourites',-1);
				
				// decreament total favs
				$this->Drobe->id=$drobeData['Drobe']['id'];
				$this->Drobe->saveField('total_favs',$drobeData['Drobe']['total_favs']-1);
				
				// counting favourite drobe
				$response['count']=$this->convertToReadeable($drobeData['Drobe']['total_favs']-1);
	
				$response['type']="success";
				$response['message']="Drobe remove from your favourite List";
			}
			else
			{
				$response['type']="error";
				$response['message']="Opps error occured in your favourite list, please try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		echo json_encode($response);
		exit();
	}
	
	function mydrobe()
	{		
	}
	
	
	function mydrobe_post()
	{	
		if(!empty($this->data)){
			/* Check user status for uploading drobe if user is inactive then it cannot upload drobe.*/
			$this->check_user_status();
			$drobeData=$this->data;
			
			$this->_cropDrobe();
				
			$drobeData['Drobe']['user_id']=$this->Auth->user('id');
			$drobeData['Drobe']['id']=$this->Drobe->id;
			$drobeData['Drobe']['unique_id']=uniqid();
			$drobeData['Drobe']['comment']=trim($drobeData['Drobe']['comment']);
			$drobeData['DrobeSetting']['ask_public']= 1 ;
			$drobeData['Drobe']['device_type']="web";
				
			if(!isset($drobeData['DrobeSetting']['target']) || $drobeData['DrobeSetting']['target']=='both')
			{
				$drobeData['DrobeSetting']['male']=1;
				$drobeData['DrobeSetting']['female']=1;
			}
			else if($drobeData['DrobeSetting']['target']=='male')
			{
				$drobeData['DrobeSetting']['male']=1;
				$drobeData['DrobeSetting']['female']=0;
			}
			else if($drobeData['DrobeSetting']['target']=='female')
			{
				$drobeData['DrobeSetting']['male']=0;
				$drobeData['DrobeSetting']['female']=1;
			}
				
			$drobeData['Drobe']['short_url']=$this->makeTinyUrl(Router::url('/rate/'.$drobeData['Drobe']['unique_id'],true));
			unset($drobeData['DrobeSetting']['target']);
			
			$this->Drobe->_setValidation();
			
			if(in_array(date('n'),array(6,7,8))) $drobeData['Drobe']['season']="summer";
			if(in_array(date('n'),array(9,10,11))) $drobeData['Drobe']['season']="fall";
			if(in_array(date('n'),array(12,1,2))) $drobeData['Drobe']['season']="winter";
			if(in_array(date('n'),array(3,4,5))) $drobeData['Drobe']['season']="spring";
				
			/* Save season year plus one if current date month is 12 other wise save current year */
			if(date('m')==12)
			{
				$drobeData['Drobe']['season_year'] = date('Y')+1;
			}
			else
			{
				$drobeData['Drobe']['season_year'] = date('Y');
			}
			
			if($this->Drobe->saveAll($drobeData))
			{
				// update users total
				$this->Drobe->User->updateUserTotalField($this->Auth->user('id'),'total_drobes',1);
		
				//add to user ebx point
				$this->add_drobe_ebx($this->Auth->user('id'),$this->Drobe->id);
		
				$this->set('drobe_uploaded',true);
				$this->_notifyFollowers($this->Drobe->id,$this->Auth->user('id'));
		
				//if($this->Session->read('UserSetting.fb_connected') && $this->Session->read('UserSetting.fb_post_drobe'))
				if($this->Session->read('UserSetting.fb_connected') && $this->Session->read('UserSetting.fb_post_drobe') && $drobeData['Drobe']['is_fb_post']==1)
				{
					$content_pages=Cache::read('content_pages');
					if(empty($content_pages))
					{
						$this->loadModel('Page');
						$this->Page->updateCache();
						$content_pages=Cache::read('content_pages');
					}
					$post = array(
							'picture' => Router::url('/drobe_images/thumb/'.$drobeData['Drobe']['file_name'],true),
							'link' => Router::url('/rate/'.$drobeData['Drobe']['unique_id'],true),
							'name' => $drobeData['Drobe']['comment'],
							'caption' => 'www.everdrobe.com',
							'message' => "",
							'description' => $content_pages['meta_description']['page_content']
					);
					$this->log("FB post data :: ".print_r($post,true),"My_fb_data");
					$this->facebook_post($post);
				}
		
				if($this->Session->read('UserSetting.tw_connected') && $this->Session->read('UserSetting.tw_post_drobe')&& $drobeData['Drobe']['is_tw_post']==1)
				{
					$message=$drobeData['Drobe']['comment'].", ".$drobeData['Drobe']['short_url'];
					try{
						$image_path= WWW_ROOT.Configure::read('drobe.upload_dir').DS.$drobeData['Drobe']['file_name'];
						$this->log("For Twitter ::".print_r($post,true),"Twitter_Post");
						$this->twitter_post($message);
					}
					catch(Exception $e){
					}
				}
				$category=Cache::read('drobe_category');
			}
			else
			{
				$this->Session->setFlash("Couldn't upload the product, try again later");
				$this->set('drobe_uploaded',false);
			}
		}
		
		// loading categories list from cache
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
		
		/* Set twitter url for post drobe when user click on twitter icon on bottom of screen for posting drobe on twitter. */
		$this->set_twitter_url();
	}
	
	
	function mydrobe_sell()
	{
		if(!empty($this->data)){
				
			/* Check user status for uploading drobe if user is inactive then it cannot upload drobe.*/
			$this->check_user_status();
			$drobeData=$this->data;
			
			$this->_cropDrobe();
			
			$drobeData['Drobe']['user_id']=$this->Auth->user('id');
			$drobeData['Drobe']['id']=$this->Drobe->id;
			$drobeData['Drobe']['unique_id']=uniqid();
			$drobeData['Drobe']['comment']=trim($drobeData['Drobe']['comment']);
			$drobeData['DrobeSetting']['ask_public']= 1 ;
			$drobeData['Drobe']['device_type']="web";
				
			if(!isset($drobeData['DrobeSetting']['target']) || $drobeData['DrobeSetting']['target']=='both')
			{
				$drobeData['DrobeSetting']['male']=1;
				$drobeData['DrobeSetting']['female']=1;
			}
			else if($drobeData['DrobeSetting']['target']=='male')
			{
				$drobeData['DrobeSetting']['male']=1;
				$drobeData['DrobeSetting']['female']=0;
			}
			else if($drobeData['DrobeSetting']['target']=='female')
			{
				$drobeData['DrobeSetting']['male']=0;
				$drobeData['DrobeSetting']['female']=1;
			}
				
			$drobeData['Drobe']['short_url']=$this->makeTinyUrl(Router::url('/rate/'.$drobeData['Drobe']['unique_id'],true));
			unset($drobeData['DrobeSetting']['target']);
				
			$this->Drobe->_setValidation();
				
			//custome size name as per id
			if(isset($drobeData['SellDrobe']['size_name']))
			{
				$drobeData['SellDrobe']['size_name']=$drobeData['SellDrobe']['size_name'];
			}
			elseif(isset($drobeData['SellDrobe']['size_id']))
			{
				$sizeNames=Cache::read('size_list');
				if(empty($sizeNames))
				{
					$this->loadModel('Size');
					$this->Size->_updateSizeCache();
					$sizeNames=Cache::read('size_list');
				}
				$drobeData['SellDrobe']['size_name']=$sizeNames[$drobeData['SellDrobe']['size_id']];
			}
			unset($drobeData['SellDrobe']['size_id']);
			/************************************/
				
			//custome brand name as per id
			if(isset($drobeData['SellDrobe']['sell_brand_name']))
			{
				$drobeData['SellDrobe']['sell_brand_name']=$drobeData['SellDrobe']['sell_brand_name'];
			}
			elseif(isset($drobeData['SellDrobe']['brand_id']))
			{
				$brandNames=Cache::read('brand_list');
				if(empty($brandNames))
				{
					$this->loadModel('Brand');
					$this->Brand->_updateBrandCache();
					$brandNames=Cache::read('brand_list');
				}
				$drobeData['SellDrobe']['sell_brand_name']=$brandNames[$drobeData['SellDrobe']['brand_id']];
			}
			unset($drobeData['SellDrobe']['brand_id']);
			/************************************/
				
			if(in_array(date('n'),array(6,7,8))) $drobeData['Drobe']['season']="summer";
			if(in_array(date('n'),array(9,10,11))) $drobeData['Drobe']['season']="fall";
			if(in_array(date('n'),array(12,1,2))) $drobeData['Drobe']['season']="winter";
			if(in_array(date('n'),array(3,4,5))) $drobeData['Drobe']['season']="spring";

			/* Save season_year plus one if current uploaded month is 12 other wise store current year. */
			if(date('m')==12)
			{
				$drobeData['Drobe']['season_year'] = date('Y')+1;
			}
			else
			{
				$drobeData['Drobe']['season_year'] = date('Y');
			}
			
			if($this->Drobe->saveAll($drobeData))
			{
				// update users total
				$this->Drobe->User->updateUserTotalField($this->Auth->user('id'),'total_drobes',1);
		
				//add to user ebx point
				$this->add_drobe_ebx($this->Auth->user('id'),$this->Drobe->id);
		
		
				$this->set('drobe_uploaded',true);
				$this->_notifyFollowers($this->Drobe->id,$this->Auth->user('id'));
		
				//if($this->Session->read('UserSetting.fb_connected') && $this->Session->read('UserSetting.fb_post_drobe'))
				if($this->Session->read('UserSetting.fb_connected') && $this->Session->read('UserSetting.fb_post_drobe') && $drobeData['Drobe']['is_fb_post']==1)
				{
					$content_pages=Cache::read('content_pages');
					if(empty($content_pages))
					{
						$this->loadModel('Page');
						$this->Page->updateCache();
						$content_pages=Cache::read('content_pages');
					}
					$post = array(
							'picture' => Router::url('/drobe_images/thumb/'.$drobeData['Drobe']['file_name'],true),
							'link' => Router::url('/rate/'.$drobeData['Drobe']['unique_id'],true),
							'name' => $drobeData['Drobe']['comment'],
							'caption' => 'www.everdrobe.com',
							'message' => "",
							'description' => $content_pages['meta_description']['page_content']
					);
					$this->log("My fb Post data :: ".print_r($myProfile,true),"my_fb_post_data");
					$this->facebook_post($post);
				}
		
				if($this->Session->read('UserSetting.tw_connected') && $this->Session->read('UserSetting.tw_post_drobe')&& $drobeData['Drobe']['is_tw_post']==1)
				{
					$message=$drobeData['Drobe']['comment'].", ".$drobeData['Drobe']['short_url'];
					try{
						$image_path= WWW_ROOT.Configure::read('drobe.upload_dir').DS.$drobeData['Drobe']['file_name'];
						//$this->twitter_post($message,$image_path);
						$this->log("For Twitter ::".print_r($post,true),"Twitter_Post");
						$this->twitter_post($message);
					}
					catch(Exception $e){
					}
				}
		
				$category=Cache::read('drobe_category');
				$drobe_size=Cache::read('size_list');
				$drobe_brand=Cache::read('brand_list');  //for brand name as per the selected from combo box @sadikhasan
				/*
				 * send mail to admin for notification
				*/
				if($drobeData['Drobe']['post_type']=="sell") {
					//when new drobe added assign it to max order number and default feature is set to 1
					/*$max_drobe_order=$this->Drobe->field('max(Drobe.order)',array("Drobe.id != "=>$this->Drobe->id, "Drobe.featured"=>1));
					$this->Drobe->updateAll(array("Drobe.order"=>$max_drobe_order+1),array('Drobe.id'=>$this->Drobe->id)); */
						
					// setting up variables for email
					if(isset($drobeData['SellDrobe']['size_name'])) {
						$size_name=$drobeData['SellDrobe']['size_name'];
					}
					else {
						$size_name=$drobe_size[$drobeData['SellDrobe']['size_id']];
					}
						
					// setting up variables for email
					if(isset($drobeData['SellDrobe']['sell_brand_name'])) {
						$brand_name=$drobeData['SellDrobe']['sell_brand_name'];
					}
					else {
						$brand_name=$drobe_size[$drobeData['SellDrobe']['brand_id']];
					}
						
					$variables=array(
							//"##full_name"=>$this->Auth->user('username'),
							"##image_source"=>Router::url('/drobe_images/thumb/'.$drobeData['Drobe']['file_name'],true),
							"##drobe_comment"=>$drobeData['Drobe']['comment'],
							"##category"=>$category[$drobeData['Drobe']['category_id']],
							"##posted_from"=>"Website",
							"##brand_name"=>$brand_name, //$drobeData['SellDrobe']['sell_brand_name'], @sadikhasan
							"##drobe_size"=>$size_name,
							"##price"=>$drobeData['SellDrobe']['sell_price'],
							"##description"=>$drobeData['SellDrobe']['sell_description'],
							"##app_version"=>"1.4",
							"##os_version"=>"1.4",
							"##user_agent"=>$_SERVER['HTTP_USER_AGENT'],
							"##ip_address"=>$_SERVER['REMOTE_ADDR']
					);
						$username_var = $this->Auth->user('username');
					if(isset($username_var) && $username_var!="")
					{
						$variables["##full_name"]=$username_var;
					}
					else
					{
						$variables["##full_name"]=$this->Auth->user('first_name')." ".$this->Auth->user('last_name');
					}
					/* New code for additional image upload @sadikhasan */
					if(is_array($drobeData['SellDrobeImage']))
					{
						//$this->SellDrobe->SellDrobeImage->removeDeletedImages($drobeData['SellDrobeImage'],$this->Drobe->id);
						foreach($drobeData['SellDrobeImage'] as $sell_drobe_image)
						{
							if(trim($sell_drobe_image['file_name'])!="")
							{
								$this->loadModel('SellDrobeImage');
								$sell_drobe_image['sell_drobe_id']=$this->Drobe->SellDrobe->field('id',array("SellDrobe.drobe_id"=>$this->Drobe->id));;
								$this->SellDrobeImage->save($sell_drobe_image);
							}
						}
					}
					/******		end here additional image upload code	****/
						
					//this function is called for add this drobe to everdrobe.myshopify.com/product withot pending request from admin side
					$this->add_drobe_to_shopify($this->Drobe->id);
						
					// sending mail after registration
					$this->sendNotifyMail('sell_drobe_posted', $variables,Configure::read('admin_email'),1);
				}
				else
				{
					// setting up variables for email
					$variables=array(
							//"##full_name"=>$this->Auth->user('username'),
							"##image_source"=>Router::url('/drobe_images/thumb/'.$drobeData['Drobe']['file_name'],true),
							"##drobe_comment"=>$drobeData['Drobe']['comment'],
							"##category"=>$category[$drobeData['Drobe']['category_id']],
							"##posted_from"=>"Website",
							"##app_version"=>"1.4",
							"##os_version"=>"1.4",
							"##user_agent"=>$_SERVER['HTTP_USER_AGENT'],
							"##ip_address"=>$_SERVER['REMOTE_ADDR']
					);

					$username_var = $this->Auth->user('username');
					if(isset($username_var) && $username_var!="")
					{
						$variables["##full_name"]=$username_var;
					}
					else
					{
						$variables["##full_name"]=$this->Auth->user('first_name')." ".$this->Auth->user('last_name');
					}

					// sending mail after registration
					$this->sendNotifyMail('new_drobe_posted', $variables,Configure::read('admin_email'),1);
				}
		
				//$this->redirect(array('action'=>'mydrobes'));
			}
			else
			{
				$this->Session->setFlash("Couldn't upload the product, try again later");
				//$this->Drobe->delete($this->Drobe->id);
				$this->set('drobe_uploaded',false);
			}
		}
		
		// for calculating earning for seller when sold
		$this->set('earning_percent',Configure::read('setting.everdrobe_earning_percent'));
		
		
		// loading categories list from cache
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
		
		// loading size list from cache
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		$this->set('sizeList',Cache::read('size_list'));
		
		/*
		 * loading brand list from cache
		* @sadikhasan
		*/
		if(Cache::read('brand_list')==null)
		{
			$this->loadModel('Brand');
			$this->Brand->_updateBrandCache();
		}
		$this->set('brandList',Cache::read('brand_list'));
		
		/** Set twitter url for uploading drobe on twitter when user click on twitter icon bottom of screen **/
		$this->set_twitter_url();
	}
	
	
	function set_twitter_url()
	{
		/*****  This code is done for Twitter login window   ******/
		App::import('Vendor', 'twitter/twitterAuth');
		App::import('Vendor', 'twitterOauth/twitteroauth');
		if(!$this->Session->read('UserSetting.tw_connected'))
		{
			if(!isset($this->params->query['oauth_verifier']))
			{
				$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"));
				$tmp_auth=$this->twitterObj->getRequestToken(Configure::read("twitter.callback_url"));
				$this->Session->write('twitter_temp_auth',$tmp_auth);
				$this->set("twitterLoginUrl",$this->twitterObj->getAuthorizeURL($tmp_auth));
			}
		}
		else
		{
			try {
				$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$this->Auth->user('twitter_access_token'),$this->Auth->user('twitter_access_token_secret'));
				$tw_connected=$this->twitterObj->get('https://api.twitter.com/1/users/show/'.$this->Auth->user('twitter_id').'.json');
				$this->set("tw_connected_info",$tw_connected);
			}
			catch(Exception $e)
			{
				$this->set("tw_connected_info",false);
				$this->set("twitterLoginUrl",$this->twitterObj->getAuthorizeURL($tmp_auth));
			}
		}
		
		/*****   End of twitter login ocde ******/
	}
	
	
	/** Check user status for uploading drobe if user is inactive then it cannot uploading drobe.*/
	function check_user_status()
	{
		/*
		 * If user status is inactive then prevent to upload new drobe
		*/
		if($this->Auth->user('status')=="inactive")
		{
				
			/*
			 * resend activation mail to user's email
			* in login session we will send mail only one time. User receive mail again on next login session
			*/
			if(true || $this->Session->read("activation_mail_sent")!=true)
			{
				$userData=$this->Auth->user();
				$this->Drobe->User->id=$userData['id'];
				if($userData['activation_key']=="")
				{
					$userData['activation_key']=uniqid();
					$this->Drobe->User->saveField('activation_key',$userData['activation_key']);
				}
				// seetting up variables for email
				$variables=array(
						//"##user_name"=>$userData['username'],
						"##activation_link"=>Router::url('/users/activate/'.$userData['activation_key'],true)
				);
				if(isset($userData['username']) && $userData['username']!="")
				{
					$variables["##user_name"]=$userData['username'];
				}
				else
				{
					$variables["##user_name"]=$userData['first_name']." ".$userData['last_name'];
				}
				// sending mail
				$this->sendNotifyMail('account_activate', $variables, $userData['email'],1);
				$this->Session->write("activation_mail_sent",true);
			}
			echo "inactive_account";
			exit();
		}
	}
	
	
	function add_drobe_ebx($user_id,$drobe_id){
		//update users ebx point
		$updateFields=array();
		$ebxAddDrobe=Configure::read('setting.ebx_add_drobe_point');
		$updateFields["UserTotal.current_ebx_point"]="UserTotal.current_ebx_point + ".$ebxAddDrobe;
		$updateFields["UserTotal.earned_ebx_point"]="UserTotal.earned_ebx_point + ".$ebxAddDrobe;
		$this->loadModel('UserTotal');
		$userTotalsData=$this->UserTotal->find('first',array('conditions'=>array('UserTotal.user_id'=>$user_id)));
		$this->UserTotal->updateAll($updateFields,array('UserTotal.user_id'=>$user_id));
		$this->ebx_transactions_add("drobe",$user_id,$ebxAddDrobe,$userTotalsData['UserTotal']['current_ebx_point']+$ebxAddDrobe,$drobe_id,"Drobe uploaded.");
	}
	
	function _cropDrobe()
	{
		$uploadDir = Configure::read('drobe.upload_dir');
			
		$thumbWidth=Configure::read('drobe.thumb.width');
		$thumbHeight=Configure::read('drobe.thumb.height');
		$thumbFolder = Configure::read('drobe.thumb.upload_dir');
			
			
		$iPhoneWidth=Configure::read('drobe.iphone.width');
		$iPhoneHeight=Configure::read('drobe.iphone.height');
		$iPhoneFolder = Configure::read('drobe.iphone.upload_dir');
			
		
		$iPhoneThumbWidth=Configure::read('drobe.iphone.thumb.width');
		$iPhoneThumbHeight=Configure::read('drobe.iphone.thumb.height');
		$iPhoneThumbFolder = Configure::read('drobe.iphone.thumb.upload_dir');
		
		
		$newWidth=Configure::read('drobe.width');
		$newHeight=Configure::read('drobe.height');
		$uploadFolder=Configure::read('drobe.upload_dir');
	
		$image_src=WWW_ROOT. $uploadFolder.DS.$this->data['Drobe']['file_name'];
	
		$left=$this->data['pos_left'];
		$top=$this->data['pos_top'];
		$cropped_width=$this->data['new_width'];
		$cropped_height=$this->data['new_height'];
	
	
		// generating iphone image
		$iphone_target=WWW_ROOT. $iPhoneFolder.DS.$this->data['Drobe']['file_name'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $iPhoneWidth, $iPhoneHeight, $iphone_target);
	
		
		// creating thumb image
		$thumb_target=WWW_ROOT. $thumbFolder.DS.$this->data['Drobe']['file_name'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $thumbWidth, $thumbHeight, $thumb_target);
	
		// creating iphone thumb image
		$iphone_thumb_target=WWW_ROOT. $iPhoneThumbFolder.DS.$this->data['Drobe']['file_name'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $iPhoneThumbWidth, $iPhoneThumbHeight, $iphone_thumb_target);
		
		// generating web image
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $newWidth, $newHeight, $image_src);
		
		
	}
	function _notifyFollowers($drobe_id,$user_id)
	{
		$this->loadModel("Follower");
		$followers=$this->Follower->find('list',array(
				'fields'=>array('Follower.follower_id'),
				'joins'=>array(
					array(  
						'table' => 'user_settings',
						'alias' => 'Setting',
						'type' => 'LEFT',
						'conditions' => array(
							'Setting.user_id = Follower.follower_id',
						)
					)
				), 
				'conditions'=>array(
						"Follower.user_id"=>$user_id,
						"Follower.request"=>"accepted",
					//	"Setting.follow_post_notification"=>1 // only notify that followers who had switched on drob post notification setting
						)
				)
		);
		$newDrobes=array();
		foreach ($followers as $follower_id)
		{
			$newDrobes[]=compact('user_id','drobe_id','follower_id');
		}
		if(count($newDrobes))
		{
			$this->loadModel('NewDrobe');
			$this->NewDrobe->saveAll($newDrobes);
		}
		
		// sending push notifications
		$followers=$this->Follower->find('all',array(
				'fields'=>array('Setting.user_id','Setting.apple_device_token','Setting.device_type','Setting.badge'),
				'joins'=>array(
						array(
								'table' => 'user_settings',
								'alias' => 'Setting',
								'type' => 'LEFT',
								'conditions' => array(
										'Setting.user_id = Follower.follower_id'
								)
						)
				),
				'conditions'=>array(
						"Follower.user_id"=>$user_id,
						"Follower.request"=>"accepted",
						"Setting.apple_device_token !="=>"", // only notify that followers who had switched on drob post notification setting
						"Setting.follow_post_notification"=>1 // only notify that followers who had switched on drob post notification setting
				)
			)
		);
		if($followers)
		{
			$user_ids=array();
			$notification_ios=array();
			$notification_android=array();
			foreach ($followers as $user)
			{
				if($user["Setting"]["device_type"]!="android")
				{
					$notification_ios[]=array(
							"message"=>"New drobe posted",
							"token"=>$user["Setting"]["apple_device_token"],
							"badge"=> $user["Setting"]['badge']+1,
							"params"=>array(
									"action"=>"following",
									"user_id"=>$user_id
								)
					);
					$user_ids[]=$user["Setting"]["user_id"];
				}
				else
				{
					$notification_android[]=$user["Setting"]["apple_device_token"];
				}
			}
			if(count($notification_ios)>0)
			{
				$this->log("Device Token :: ".print_r($notification_ios,true),'push_notification');
				$this->send_notification_bulk($notification_ios);
				
				$this->loadModel("UserSetting");
				$this->UserSetting->updateAll(array('badge'=>'badge + 1'),array('user_id'=>$user_ids));
			}
			if(count($notification_android)>0)
			{
				$notification=array(
						'tokens'=>$notification_android,
						"message"=>array(
								"message"=>"New drobe posted",
								"action"=>"following",
								"user_id"=>$user_id
							)
						);
				$this->send_notification_bulk_android($notification);
			}
		}
	}
	
	function _removeNotifyFollowers($drobe_id,$follower_id=null)
	{
		$this->loadModel('NewDrobe');
		if($follower_id!=null)
		{
			$this->NewDrobe->deleteAll(compact('drobe_id','follower_id'));
			return $this->NewDrobe->getAffectedRows() > 0;
		}
		else
		{
			$this->NewDrobe->deleteAll(compact('drobe_id'));
			return $this->NewDrobe->getAffectedRows() > 0;
		}
	}
	
	
	function add_drobe($user_id=null){
		
		$response=array();
		if(intval($user_id)>0)
		{
			if(!empty($this->data)){
				$category=Cache::read('drobe_category');
				if(empty($category))
				{
					$this->loadModel('Category');
					$this->Category->_updateCategoryCache();
					$category=Cache::read('drobe_category');
				}
				
				$image_file="";
				//uploading image based on base64 encoded string
				if(isset($this->data['image']) && $this->data['image']!="")
				{
					$image_file=$this->_createDrobeImage($this->data['image']);
				}
				if(isset($this->data['image_url']) && $this->data['image_url']!="")
				{
					$image_file=$this->_createDrobeImage($this->data['image_url'],true);
				}
				if(is_array($image_file) && count($image_file)>0)
				{
					// partially uploaded detected
					$response=$image_file;
				}
				else if($image_file!="")
				{
					$image_path=WWW_ROOT . 'drobe_images' .DS. $image_file;
					$img_size=getimagesize($image_path);
					
					$drobeData=$this->data;
					$drobeData['Drobe']['user_id']=$user_id;
					$drobeData['Drobe']['unique_id']=uniqid();
					$drobeData['Drobe']['comment']=$this->data['comment'];
					
					/* Save season_year plus one if current uploaded month is 12 other wise store current year. */
					if(date('m')==12)
					{
						$drobeData['Drobe']['season_year'] = date('Y')+1;
					}
					else
					{
						$drobeData['Drobe']['season_year'] = date('Y');
					}
					
					$this->loadModel('Category');
					//$find_category = $this->Category->find('first',array('conditions'=>array('Category.id'=>$this->data['category_id'])));
					if(array_key_exists($this->data['category_id'],$category))
					{
						$drobeData['Drobe']['category_id']=$this->data['category_id'];
					}
					else 
					{
						$drobeData['Drobe']['category_id']=configure::read('other_category_id');
					}
					$drobeData['DrobeSetting']['ask_public']= 1 ;
					$drobeData['DrobeSetting']['male']=intval($this->data['male'])==1 ? 1 : 1 ;
					$drobeData['DrobeSetting']['female']=intval($this->data['female'])==1 ? 1 :  1;
					$drobeData['Drobe']['file_name']=$image_file;
					
					if(isset($this->data['device_type']))
					{
						$drobeData['Drobe']['device_type']=$this->data['device_type'];
						$drobeData['Drobe']['app_version']=$this->data['app_version'];
						$drobeData['Drobe']['os_version']=$this->data['os_version'];
					}
					
					
					// create short url
					$drobeData['Drobe']['short_url']=$this->makeTinyUrl(Router::url('/rate/'.$drobeData['Drobe']['unique_id'],true));
					$this->log("Drobe Data".print_r($drobeData,true),"add_drobe");
					
					//for bulk data script
					if(isset($this->data['image_url']))
					{
						$drobeData['Drobe']['uploaded_on']=date('Y-m-d h:i:s',rand(strtotime('-5 days'),strtotime('today')));
					}
					// end of bulk data script
					
					
					
					/*
					 * If Drobe not for sell then clear sell drobe related fields values
					*/
					if($this->data['post_type']=="sell")
					{
						$drobeData['Drobe']['post_type']="sell";
						$drobeData['SellDrobe']=array();
						//$drobeData['SellDrobe']['sell_brand_name']=$this->data['sell_brand_name'];
						
						//custome size as a text code
						if(isset($this->data['size_id']))
						{
							$sizeNames=Cache::read('size_list');
							if(empty($sizeNames))
							{
								$this->loadModel('Size');
								$this->Size->_updateSizeCache();
								$sizeNames=Cache::read('size_list');
							}
							$drobeData['SellDrobe']['size_name']=$sizeNames[$this->data['size_id']];
						}
						elseif(isset($this->data['size_name']))
						{
							$drobeData['SellDrobe']['size_name']=$this->data['size_name'];
						}
						/**********************/
						
						//custome Brand as a text code
						if(isset($this->data['brand_id']) && $this->data['brand_id']!="")
						{
							$brandNames=Cache::read('brand_list');
							if(empty($brandNames))
							{
								$this->loadModel('Brand');
								$this->Brand->_updateBrandCache();
								$brandNames=Cache::read('brand_list');
							}
							$drobeData['SellDrobe']['sell_brand_name']=$brandNames[$this->data['brand_id']];
						}
						elseif(isset($this->data['sell_brand_name']))
						{
							$drobeData['SellDrobe']['sell_brand_name']=$this->data['sell_brand_name'];
						}
						/**********************/
						
						$drobeData['SellDrobe']['sell_price']=$this->data['sell_price'];
						$drobeData['SellDrobe']['original_sell_price']=$this->data['original_sell_price'];
						$drobeData['SellDrobe']['is_brand_new']=$this->data['is_brand_new'];
						$drobeData['SellDrobe']['sell_description']=$this->data['sell_description'];
						if(isset($this->data['sell_gender']))
						{
							/* Check for old iphone app compatibility */
							if(strtolower($this->data['sell_gender'])=='men')
							{
								$drobeData['SellDrobe']['sell_gender'] = 'male';
							}
							else if(strtolower($this->data['sell_gender'])=='women')
							{
								$drobeData['SellDrobe']['sell_gender'] = 'female';
							}
							else
							{
								$drobeData['SellDrobe']['sell_gender']=$this->data['sell_gender'];
							}
							
						}
						else $drobeData['SellDrobe']['sell_gender']="everyone";
					}
					else $drobeData['Drobe']['post_type']="post";
					
					if(in_array(date('n'),array(6,7,8))) $drobeData['Drobe']['season']="summer";
					if(in_array(date('n'),array(9,10,11))) $drobeData['Drobe']['season']="fall";
					if(in_array(date('n'),array(12,1,2))) $drobeData['Drobe']['season']="winter";
					if(in_array(date('n'),array(3,4,5))) $drobeData['Drobe']['season']="spring";
					
					if($this->Drobe->saveAll($drobeData))
					{
						// update users total
						$this->Drobe->User->updateUserTotalField($drobeData['Drobe']['user_id'],'total_drobes',1);
						
						//add to user ebx point
						$this->add_drobe_ebx($drobeData['Drobe']['user_id'],$this->Drobe->id);
						
						
						$this->_notifyFollowers($this->Drobe->id,$user_id);
						$response['type']="success";
						$response['message']="Drobe uploaded successfully";
						
						// return drobe information  for facebook or twitter post
						$response['drobe']=array();
						$response['drobe']['id']=$this->Drobe->id;
						$response['drobe']['url']=Router::url("/rate/".$drobeData['Drobe']['unique_id'],true);
						$response['drobe']['image_url']=Router::url("/drobe_images/".$drobeData['Drobe']['file_name'],true);
						$response['drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobeData['Drobe']['file_name'],true);
						$response['drobe']['short_url']=$drobeData['Drobe']['short_url'];
						
						$drobe_size=Cache::read('size_list');
						
						/*
						 * send mail to admin for notification
						*/
						$full_name=$this->Drobe->User->field("concat('User.first_name','User.lastname')",array('User.id'=>$user_id));
						$username=$this->Drobe->User->field("concat(User.username)",array('User.id'=>$user_id));
						if($drobeData['Drobe']['post_type']=="sell")
						{
							//when new drobe added assign it to max order number and default feature is set to 1
							/*$max_drobe_order=$this->Drobe->field('max(Drobe.order)',array("Drobe.id != "=>$this->Drobe->id, "Drobe.featured"=>1));
							$this->Drobe->updateAll(array("Drobe.order"=>$max_drobe_order+1),array('Drobe.id'=>$this->Drobe->id));*/
							
							// setting up variables for email
							$variables=array(
									//"##full_name"=>$username,
									"##image_source"=>Router::url('/drobe_images/thumb/'.$drobeData['Drobe']['file_name'],true),
									"##drobe_comment"=>$drobeData['Drobe']['comment'],
									"##category"=>$category[$drobeData['Drobe']['category_id']],
									"##posted_from"=>$drobeData['Drobe']['device_type'],
									"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
									"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
									"##price"=>$drobeData['SellDrobe']['sell_price'],
									"##description"=>$drobeData['SellDrobe']['sell_description'],
									"##app_version"=>$drobeData['Drobe']['app_version'],
									"##os_version"=>$drobeData['Drobe']['os_version'],
									"##user_agent"=>$_SERVER['HTTP_USER_AGENT'],
									"##ip_address"=>$_SERVER['REMOTE_ADDR']
									
							);
							if(isset($username) && $username!="")
							{
								$variables["##full_name"]=$username;
							}
							else
							{
								$variables["##full_name"]=$full_name;
							}
							//this function is called for add this drobe to everdrobe.myshopify.com/product withot pending request from admin side
							$this->add_drobe_to_shopify($this->Drobe->id);
							
							
							// sending mail after registration
							$this->sendNotifyMail('sell_drobe_posted', $variables,Configure::read('admin_email'),1);
						}
						else
						{
							// setting up variables for email
							$variables=array(
									//"##full_name"=>$username,
									"##image_source"=>Router::url('/drobe_images/thumb/'.$drobeData['Drobe']['file_name'],true),
									"##drobe_comment"=>$drobeData['Drobe']['comment'],
									"##category"=>$category[$drobeData['Drobe']['category_id']],
									"##posted_from"=>$drobeData['Drobe']['device_type'],
									"##app_version"=>$drobeData['Drobe']['app_version'],
									"##os_version"=>$drobeData['Drobe']['os_version'],
									"##user_agent"=>$_SERVER['HTTP_USER_AGENT'],
									"##ip_address"=>$_SERVER['REMOTE_ADDR']
							);
							if(isset($username) && $username!="")
							{
								$variables["##full_name"]=$username;
							}
							else
							{
								$variables["##full_name"]=$full_name;
							}
							// sending mail after registration
							$this->sendNotifyMail('new_drobe_posted', $variables,Configure::read('admin_email'),1);
						}
					}
					else
					{
						$response['type']="error";
						$response['message']="Couldn't upload the product, try again later";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']="Source of image is invalid accept only base64encoded source";
				}
			}
			else{
				$response['type']="error";
				$response['message']="Invalid parameter";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid user id";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	/*
	 * getting results of all drobe received rate even if rate not received 
	 */
	
	function received()
	{
		$conditions=array();
		//$conditions['Rate.comment !=']="";
		$conditions['Drobe.user_id']=$this->Auth->user('id');
		$conditions['Drobe.rate_status']='open';
		$conditions['Drobe.deleted']=0;
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$search = trim($this->params->params['named']['search']);
			$keyword= preg_split("/[\s,]+/", $search);
			foreach($keyword as $key)
			{
				$conditions['OR'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('Rate.comment LIKE' => $key."%"), 'Rate.comment LIKE' => "% ".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('RateUser.first_name LIKE' => $key."%"), 'RateUser.first_name LIKE' => "% ".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('RateUser.last_name LIKE' => $key."%"), 'RateUser.last_name LIKE' => "% ".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('RateUser.username LIKE' => "@".$key."%"), 'RateUser.username LIKE' => "% ".$key."%"));
			}
			/*$search = trim($this->params->params['named']['search']);
			$conditions['OR']['Drobe.comment like']="%".$search."%";
			$conditions['OR']['Rate.comment like']="%".$search."%";
			$conditions['OR']['User.first_name like']="%".$search."%";
			$conditions['OR']['User.last_name like']="%".$search."%";*/
		}
		
		$this->paginate = array(
				'fields'=>array('sum(Rate.new_reply)+Drobe.new_response as total_response','Drobe.id','Drobe.unique_id','Drobe.uploaded_on','Drobe.category_id','Drobe.new_response','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.total_out','Drobe.is_highest_rated','User.first_name','User.last_name','User.username','User.id','User.unique_id'),
				'joins'=>array(
						array(
								'table' => 'rates',
								'alias' => 'Rate',
								'type' => 'LEFT',
								'conditions' => array(
									'Drobe.id = Rate.drobe_id',
									"Rate.rate !="=>0
								)
							),
						array(
								'table' => 'users',
								'alias' => 'RateUser',
								'type' => 'LEFT',
								'conditions' => array(
										'RateUser.id = Rate.user_id',
										"Rate.rate !="=>0
								)
						)
						
				),
				'group'=>"Drobe.id",
				//'order'=>"total_response DESC,Drobe.new_response DESC , Rate.created_on DESC",
				'order'=>"total_response DESC,Drobe.new_response DESC , Rate.last_conversation_time DESC",
				'conditions'=>$conditions,
				'limit'=>10
		);
		$result = $this->paginate('Drobe');
		
		
		foreach($result as &$drobe)
		{
			$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
			/*$drobe['Drobe']['total_in']=$total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0;
			$drobe['Drobe']['total_out']=$total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0;*/
			$drobe['Drobe']['total_in']=$drobe['Drobe']['total_in'];
			$drobe['Drobe']['total_out']=$drobe['Drobe']['total_out'];
		}
		$this->set('drobes',$result);
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
	}
	
	
	
	/**  Got feedback webservice14 with search parameter */
	function got_feedback_v14($user_id=null)
	{
		
		$response=array();
		if($user_id>0)
		{
			$conditions=array();
			if(isset($this->data['search']) && $this->data['search']!="")
			{
				$search = trim($this->data['search']);
				$keyword= preg_split("/[\s,]+/", $search);
				foreach($keyword as $key)
				{
					$conditions['OR'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('Rate.comment LIKE' => $key."%"), 'Rate.comment LIKE' => "% ".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('RateUser.first_name LIKE' => $key."%"), 'RateUser.first_name LIKE' => "% ".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('RateUser.last_name LIKE' => $key."%"), 'RateUser.last_name LIKE' => "% ".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('RateUser.username LIKE' => "@".$key."%"), 'RateUser.username LIKE' => "% ".$key."%"));
				}
				/*$search=$this->data['search'];
				$conditions['OR']['Drobe.comment like']=$search."%";
				$conditions['OR']['Rate.comment like']=$search."%";
				$conditions['OR']['User.first_name like']=$search."%";
				$conditions['OR']['User.last_name like']=$search."%";*/
			}
			$conditions['Drobe.user_id']=$user_id;
			$conditions['Drobe.rate_status']='open';
			$conditions['Drobe.deleted']=0;
	
			$this->paginate=array(
					'fields'=>array('sum(Rate.new_reply)+Drobe.new_response as total_response','User.star_user',
							'SellDrobe.sell_price','Drobe.short_url','Drobe.is_highest_rated','Drobe.id','Drobe.unique_id',
							'Drobe.uploaded_on','Drobe.category_id','Drobe.new_response','Drobe.file_name','Drobe.comment',
							'Drobe.total_rate','Drobe.total_comments','Drobe.total_in','Drobe.total_out','Category.category_name',
							'User.first_name','User.last_name','User.username','User.id','User.photo'),
					'joins'=>array(
							array(
									'table' => 'rates',
									'alias' => 'Rate',
									'type' => 'LEFT',
									'conditions' => array(
											'Drobe.id = Rate.drobe_id',
											"Rate.rate !="=>0
									)),
							array(
								'table' => 'users',
								'alias' => 'RateUser',
								'type' => 'LEFT',
								'conditions' => array(
										'RateUser.id = Rate.user_id',
										"Rate.rate !="=>0
								)
						)			
					),
					'group'=>"Drobe.id",
					//'order'=>"total_response DESC,Drobe.new_response DESC, Rate.created_on DESC",
					'order'=>"total_response DESC,Drobe.new_response DESC, Rate.last_conversation_time DESC",
					'conditions'=>$conditions,
					'limit'=>10
				);

			$result = $this->paginate('Drobe');
			
			foreach($result as &$drobe)
			{
				$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
				/*$drobe['Drobe']['total_in']="".($total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0);
				$drobe['Drobe']['total_out']="".($total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0);*/
				$drobe['Drobe']['total_in']="".$drobe['Drobe']['total_in'];
				$drobe['Drobe']['total_out']="".$drobe['Drobe']['total_out'];
				$drobe['Drobe']['total_rate']="".$drobe['Drobe']['total_rate'];
				$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['category']=$drobe['Category']['category_name'];
				$drobe['User']['photo_url']=Router::url("/profile_images/".$drobe['User']['photo'],true);
				$drobe['User']['star_user']="".$drobe['User']['star_user'];
				$drobe['User']['thumb_url']=Router::url("/profile_images/thumb/".$drobe['User']['photo'],true);
	
				if($drobe['SellDrobe']['sell_price']!=null)
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
	
				unset($drobe['SellDrobe']);
	
				// counting total response
				if($drobe['0']['total_response']==null)
				{
					$drobe['Drobe']['new_response']="0";
				}
				else
				{
					$drobe['Drobe']['new_response']="".$drobe['0']['total_response'];
				}
	
				unset($drobe['Drobe']['file_name'],$drobe['0'],$drobe['Rate'],$drobe['Category'],$drobe['Favourite'],$drobe['Flag']);
	
			}
			$this->set('feedbacks',$result);
			$response['type']="success";
			$response['feedback']=$result;
	
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	/**  End Got feedback webservice14 here**/
	
	
	
	
	/*
	 * Webservice for got feedback listing all drobes even if feedback not received
	 * 
	 */
	
	function got_feedback($user_id=null)
	{
		$response=array();
		if($user_id>0)
		{
			$conditions=array();
			$conditions['Drobe.user_id']=$user_id;
			$conditions['Drobe.rate_status']='open';
			$conditions['Drobe.deleted']=0;
	
			$this->paginate=array(
					'fields'=>array('sum(Rate.new_reply)+Drobe.new_response as total_response','User.star_user','SellDrobe.sell_price','Drobe.short_url','Drobe.is_highest_rated','Drobe.id','Drobe.unique_id','Drobe.uploaded_on','Drobe.category_id','Drobe.new_response','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.total_comments','Drobe.total_in','Drobe.total_out','Category.category_name','User.first_name','User.last_name','User.username','User.id','User.photo'),
					'joins'=>array(
							array(
									'table' => 'rates',
									'alias' => 'Rate',
									'type' => 'LEFT',
									'conditions' => array(
											'Drobe.id = Rate.drobe_id',
											"Rate.rate !="=>0
									))
							
					),
					'group'=>"Drobe.id",
					//'order'=>"total_response DESC,Drobe.new_response DESC, Rate.created_on DESC",
					'order'=>"total_response DESC,Drobe.new_response DESC, Rate.last_conversation_time DESC",
					'conditions'=>$conditions,
					'limit'=>10
			);
			$result = $this->paginate('Drobe');
			
			foreach($result as &$drobe)
			{
				$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
				/*$drobe['Drobe']['total_in']="".($total>0 ? round($drobe['Drobe']['total_in']/$total*100) : 0);
				$drobe['Drobe']['total_out']="".($total>0 ? round($drobe['Drobe']['total_out']/$total*100) : 0);*/
				$drobe['Drobe']['total_in']="".$drobe['Drobe']['total_in'];
				$drobe['Drobe']['total_out']="".$drobe['Drobe']['total_out'];
				$drobe['Drobe']['total_rate']="".$drobe['Drobe']['total_rate'];
				$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['category']=$drobe['Category']['category_name'];
				$drobe['User']['photo_url']=Router::url("/profile_images/".$drobe['User']['photo'],true);
				$drobe['User']['star_user']="".$drobe['User']['star_user'];
				$drobe['User']['thumb_url']=Router::url("/profile_images/thumb/".$drobe['User']['photo'],true);
				
				if($drobe['SellDrobe']['sell_price']!=null)
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
				
				unset($drobe['SellDrobe']);
				
				// counting total response
				if($drobe['0']['total_response']==null)
				{
					$drobe['Drobe']['new_response']="0";
				}
				else
				{
					$drobe['Drobe']['new_response']="".$drobe['0']['total_response'];
				}
				
				
				unset($drobe['Drobe']['file_name'],$drobe['0'],$drobe['Rate'],$drobe['Category'],$drobe['Favourite'],$drobe['Flag']);
	
			}
			$this->set('feedbacks',$result);
			$response['type']="success";
			$response['feedback']=$result;
	
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	function _getDrobeFileProperty(&$drobeData)
	{
		$image_path=WWW_ROOT . 'drobe_images' .DS. $drobeData['file_name'];
		$img_size=getimagesize($image_path);
		$drobeData['file_type']=$img_size['mime'];
		$drobeData['file_size']=filesize($image_path);
	}
	
	private function _createDrobeImage($imageData,$is_url=false)
	{
		if($is_url)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $imageData);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$data = curl_exec($ch);
			$src = imagecreatefromstring($data);
			
			/* $ext=strtolower(array_pop(explode(".",$imageData)));
			if($ext=="jpg" || $ext="jpeg") $src = imagecreatefromjpeg($imageData);
			else if($ext=="png")$src = imagecreatefrompng($imageData);
			else if($ext=="gif")$src = imagecreatefromgif($imageData); */
		}
		else
		{
			if(isset($this->data['file_size']) && strlen($imageData)!=$this->data['file_size'])
			{
				return array('type'=>"error","message"=>"Error occured while upload your drobe image.");
			}
			$data = base64_decode($imageData);
			$src = imagecreatefromstring($data);
		}
		
		// making square image
		
		$width = imagesx($src);
		$height = imagesy($src);
		
		// square white image
		$minHeight=Configure::read('drobe.iphone.height');
		$minWidth=Configure::read('drobe.iphone.width');
		
		$new_width= ($width<$minWidth) ? $minWidth : $width;
		$new_height= ($height<$minHeight) ? $minHeight : $height;
		
		// make aspect ratio in square image
		if($new_width<$new_height) $new_width=$new_height;
		if($new_width>$new_height) $new_height=$new_width;
		
		$square_image = imagecreatetruecolor( $new_width, $new_height );
		imagefill($square_image, 0, 0, imagecolorallocate($square_image, 255, 255, 255));
		
		//set image in center
		$new_left=intval(($new_width - $width) / 2);
		$new_top=intval(($new_height - $height) / 2);
		
		imagecopymerge($square_image, $src, $new_left, $new_top,0,0, $width , $height, 100);
		imagedestroy($src);
		
		// creating unique file name
		$file_name=uniqid().'.jpg';
		$target_path = WWW_ROOT . Configure::read('drobe.upload_dir').DS.$file_name;
		
		$height=Configure::read('drobe.height');
		$width=Configure::read('drobe.width');
		
		if($this->_resizeDrobeImage($square_image, $target_path,$width, $height)!="")
		{
			$target_path = WWW_ROOT . Configure::read('drobe.iphone.upload_dir').DS.$file_name;
			$height=Configure::read('drobe.iphone.height');
			$width=Configure::read('drobe.iphone.width');
			$this->_resizeDrobeImage($square_image, $target_path,$width, $height);
			
			$target_path = WWW_ROOT . Configure::read('drobe.thumb.upload_dir').DS.$file_name;
			$height=Configure::read('drobe.thumb.height');
			$width=Configure::read('drobe.thumb.width');
			$this->_resizeDrobeImage($square_image, $target_path,$width, $height);
			
			$target_path = WWW_ROOT . Configure::read('drobe.iphone.thumb.upload_dir').DS.$file_name;
			$width=Configure::read('drobe.iphone.thumb.width');
			$height=Configure::read('drobe.iphone.thumb.height');
			$this->_resizeDrobeImage($square_image, $target_path,$width, $height);
			
			return $file_name;
		}
		else return false;
	}
	private function _createDrobeImageServer($imageData,$is_url=false)
	{
		if($is_url)
		{
			$ext=strtolower(array_pop(explode(".",$imageData)));
			if($ext=="jpg" || $ext="jpeg") $src = imagecreatefromjpeg($imageData);
			else if($ext=="png")$src = imagecreatefrompng($imageData);
			else if($ext=="gif")$src = imagecreatefromgif($imageData);
		}
		else
		{
			if(isset($this->data['file_size']) && strlen($imageData)!=$this->data['file_size'])
			{
				return array('type'=>"error","message"=>"Error occured while upload your drobe image.");
			}
			$data = base64_decode($imageData);
			$src = imagecreatefromstring($data);
		}
	
		// making square image
	
		$width = imagesx($src);
		$height = imagesy($src);
	
		if($width>$height)
		{
			$currWidth=$width;
			$currHeight=$width;
		}
		else if($height>$width)
		{
			$currWidth=$height;
			$currHeight=$height;
		}
		else
		{
			$currWidth=$width;
			$currHeight=$height;
		}
		// square white image
		$square_image = imagecreatetruecolor( $currWidth, $currHeight );
		imagefill($square_image, 0, 0, imagecolorallocate($square_image, 255, 255, 255));
	
		//set image in center
		$new_left=intval(($currWidth - $width) / 2);
		$new_top=intval(($currHeight - $height) / 2);
	
		imagecopymerge($square_image, $src, $new_left, $new_top,0,0, $currWidth , $currHeight, 100);
		imagedestroy($src);
	
		if($new_left>0 || $new_top >0)
		{
			// filling white color instead of black in black space
			$white = imagecolorallocate($square_image, 255, 255, 255);
			$this->_fillColor($square_image, 1, 1, $white);
			$this->_fillColor($square_image, $currWidth-1, $currHeight-1, $white);
		}
		// finish generate square image
	
	
		// creating unique file name
		$file_name=uniqid().'.jpg';
		$target_path = WWW_ROOT . Configure::read('drobe.upload_dir').DS.$file_name;
	
		$height=Configure::read('drobe.height');
		$width=Configure::read('drobe.width');
	
		if($this->_resizeDrobeImage($square_image, $target_path,$width, $height)!="")
		{
			$target_path = WWW_ROOT . Configure::read('drobe.iphone.upload_dir').DS.$file_name;
			$height=Configure::read('drobe.iphone.height');
			$width=Configure::read('drobe.iphone.width');
			$this->_resizeDrobeImage($square_image, $target_path,$width, $height);
				
			$target_path = WWW_ROOT . Configure::read('drobe.thumb.upload_dir').DS.$file_name;
			$height=Configure::read('drobe.thumb.height');
			$width=Configure::read('drobe.thumb.width');
			$this->_resizeDrobeImage($square_image, $target_path,$width, $height);
				
			$target_path = WWW_ROOT . Configure::read('drobe.iphone.thumb.upload_dir').DS.$file_name;
			$width=Configure::read('drobe.iphone.thumb.width');
			$height=Configure::read('drobe.iphone.thumb.height');
			$this->_resizeDrobeImage($square_image, $target_path,$width, $height);
				
			return $file_name;
		}
		else return false;
	}
	
	function _resizeDrobeImage($src,$target_path,$newWidth,$newHeight)
	{
		$width = imagesx($src);
		$height = imagesy($src);
		/* create a new, "virtual" image */
		$virtual_image = imagecreatetruecolor($newWidth,$newHeight);
	
		/* copy source image at a resized size */
		imagecopyresampled($virtual_image,$src,0,0,0,0,$newWidth,$newHeight,$width,$height);
		/* create the physical thumbnail image to its destination */
		if(imagejpeg($virtual_image,$target_path))
		{
			imagedestroy($virtual_image);
			return array_pop(explode(DS,$target_path));
		}
		else return false;
	}
	

	function delete_image($filename)
	{
		Configure::write('debug',2);
		
		$filename=str_replace("__", ".", $filename);
		$uploadFolder=Configure::read('drobe.upload_dir');
		$iPhoneFolder = Configure::read('drobe.iphone.upload_dir');
		$thumbFolder = Configure::read('drobe.thumb.upload_dir');
		
		$image_src=WWW_ROOT. $uploadFolder.DS.$filename;
		if(is_file($image_src)) unlink($image_src);
		
		$image_src=WWW_ROOT. $iPhoneFolder.DS.$filename;
		if(is_file($image_src)) unlink($image_src);
		
		$image_src=WWW_ROOT. $thumbFolder.DS.$filename;
		if(is_file($image_src)) unlink($image_src);
		
		echo "success";
		exit();
	}
	
	/*
	 * Call this action when you processing cropping image
	 */
	function crop()
	{
		$this->layout="ajax";
		$uploadDir = Configure::read('drobe.upload_dir');
			
		$thumbWidth=Configure::read('drobe.thumb.width');
		$thumbHeight=Configure::read('drobe.thumb.height');
		$thumbFolder = Configure::read('drobe.thumb.upload_dir');
			
			
		$iPhoneWidth=Configure::read('drobe.iphone.width');
		$iPhoneHeight=Configure::read('drobe.iphone.height');
		$iPhoneFolder = Configure::read('drobe.iphone.upload_dir');
			
		$newWidth=Configure::read('drobe.width');
		$newHeight=Configure::read('drobe.height');
		$uploadFolder=Configure::read('drobe.upload_dir');
		
		$image_src=WWW_ROOT. $uploadFolder.DS.$this->data['filename'];
		
		$left=$this->data['pos_left'];
		$top=$this->data['pos_top'];
		$cropped_width=$this->data['new_width'];
		$cropped_height=$this->data['new_height'];
	
		
		$iphone_target=WWW_ROOT. $iPhoneFolder.DS.$this->data['filename'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $iPhoneWidth, $iPhoneHeight, $iphone_target);
		
		
		// creating thumb image
		$thumb_target=WWW_ROOT. $thumbFolder.DS.$this->data['filename'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $thumbWidth, $thumbHeight, $thumb_target);
		
		//resizing actual image
		//$new_top= $top + (($iPhoneHeight-$newHeight)* $iPhoneHeight/$cropped_height/2);
		$new_cropped_height=$cropped_width*$newHeight/$newWidth;
		$new_top=$top+($cropped_height-$new_cropped_height)/2;
		$this->_cropImage($image_src, $left, $new_top, $cropped_width, $new_cropped_height, $newWidth, $newHeight, $image_src);
		
		$response=array();
		$response['type']="success";
		$response['message']="Cropped successfully";
		
		echo json_encode($response);
		exit();
	}
	
	function _cropImage($src,$left,$top,$width,$height,$new_width,$new_height,$target_path)
	{
		/* read the source image */
		$ext=strtolower(array_pop(explode(".", $src)));
		if($ext=="jpg" || $ext=="jpeg") $image = imagecreatefromjpeg($src);
		else if($ext=="gif") $image = imagecreatefromgif($src);
		else if($ext=="png") $image = imagecreatefrompng($src);
		
		if(!($width>0)) $width=imagesx($image);
		if(!($height>0)) $height=imagesy($image);
		 
		$new_image = ImageCreateTrueColor( $new_width, $new_height );
		// filling white color in extra part
		
		imagecopyresampled($new_image,$image,0,0,$left,$top,$new_width,$new_height,$width,$height);
		
		// filling white color in extra part
		/*$white = imagecolorallocate($new_image, 255, 255, 255);
		if($new_width<$width)
		{
			$this->_fillColor($new_image, $height, 0, $white);
		}
		if($new_height<$height)
		{
			$this->_fillColor($new_image, 0, $width, $white);
		}*/
		 
		if($ext=="jpg" || $ext=="jpeg") imagejpeg($new_image,$target_path);
		else if($ext=="gif") imagegif($new_image,$target_path);
		else if($ext=="png") imagepng($new_image,$target_path);
		 
	}
	function _fillColor(&$im,$left,$top,$color)
	{
		$rgb = imagecolorat($im, $left, $left);
		if($rgb==0)
		{
			imagefill($im, $left, $top, $color);
		}
	}
	
	/*
	 * 
	 * Fixing Comment Issue
	 */
	function rate_issue()
	{
		$rates=$this->Drobe->find('list',array('fields'=>array('Drobe.comment')));
		foreach($rates as $id=>$val)
		{
			$this->Drobe->updateDrobeRateIndex($id);
		}
	}
	
	function add_drobe_to_shopify($drobe_id,$updateMainImage=""){
		
		$drobe_data=$this->Drobe->findById($drobe_id);
		$request_data=array("product"=>array());
		$size_list=Cache::read('size_list');
			
		/*
		 * Create collection if not exist on everdrobe shop
		*/
		/*if($drobe_data['Category']['collection_id']==0)
		 {
		$collection_id=$this->_createCollection($drobe_data['Category']);
		}
		else
		{
		$collection_id=$drobe_data['Category']['collection_id'];
		}*/
			
		/*
		 * End of create collection
		*/
		$this->log("Tags:: ".print_r($drobe_data,true),'shopify_drobe_data');
			
		if($drobe_data['SellDrobe']['shopify_product_id']>0)
		{
			/*
			 * UPDATING Existing product in everdrobe shop
			*
			* For Some limitations/bugs of shopify we need to use XML format for update product
			*/
		
			// getting current product detail
			$response=$this->shopify("/products/".$drobe_data['SellDrobe']['shopify_product_id'], "","GET");
		
			/*
			 * Creating product tags and product size
			*/
			$product_size_name=$drobe_data['SellDrobe']['size_name'];
			if(trim($product_size_name)=="") $product_size_name="Default";
		
			$uploader_name=$drobe_data['User']['username']. " ".$drobe_data['User']['id'];
		
			$product_tags=array();
			$product_tags[]=$drobe_data['SellDrobe']['sell_brand_name'];
			$product_tags[]=$drobe_data['Category']['category_name'];
			$product_tags[]=$product_size_name;
			$product_tags[]="For ".Inflector::humanize($drobe_data['SellDrobe']['sell_gender']);
			$product_tags[]=$uploader_name;
			$this->log("Tags:: ".print_r($product_tags,true),'shopify_tags');
		
			$request_data['product']=array(
					"id"=>$drobe_data['SellDrobe']['shopify_product_id'],
					"title"=>$drobe_data['Drobe']['comment'],
					"vendor"=>$drobe_data['SellDrobe']['sell_brand_name'],
					"body-html"=>$drobe_data['SellDrobe']['sell_description'],
					"product-type"=>$uploader_name,
					"tags"=> implode(",",$product_tags),
					"variants"=>array(
							'variant'=>array(
									// need to add variant id from current product
									"id"=>$response['product']['variants'][0]['id'],
									"price"=>$drobe_data['SellDrobe']['sell_price'],
									"compare_at_price"=>$drobe_data['SellDrobe']['original_sell_price'],
									"title"=> $product_size_name,
									"option1"=> $product_size_name
							)
					)
			);
		
			$response=$this->shopify("/products/".$drobe_data['SellDrobe']['shopify_product_id'], json_encode($request_data),"PUT");
			if(isset($response['product']) && $response['product']['id']!="")
			{
				// adding product to collection
				/*if($collection_id>0)
				 {
				$this->_updateProductCollection($response['product']['id'],$collection_id);
				}*/
					
				$this->_updateShopifyImages($drobe_data['SellDrobe']['id'],$response['product']['id'],$updateMainImage);
				//$this->Session->setFlash("Selected product is updated on Everdrobe Shop","default",array("class"=>"success"));
			}
			else
			{
				if(isset($response['errors']['base']) && count($response['errors']['base'])>0)
				{
					$this->Session->setFlash("Shopify Upload Error: ". $response['errors']['base'][0],"default",array("class"=>"error"));
				}
				else $this->Session->setFlash("Error occured in upadate product in everdrobe shop","default",array("class"=>"error"));
			}
				
		}
		else
		{
			/*
			 * CREATING new product in everdrobe shop
			*/
		
			// Creating product tags and product size
			$product_size_name=$drobe_data['SellDrobe']['size_name'];
			if(trim($product_size_name)=="") $product_size_name="Default";
		
			$uploader_name=$drobe_data['User']['username']. " ".$drobe_data['User']['id'];
		
			$product_tags=array();
			$product_tags[]=$drobe_data['SellDrobe']['sell_brand_name'];
			$product_tags[]=$drobe_data['Category']['category_name'];
			$product_tags[]=$product_size_name;
			$product_tags[]="For ".Inflector::humanize($drobe_data['SellDrobe']['sell_gender']);
			$product_tags[]=$uploader_name;
			$this->log("Tags:: ".print_r($product_tags,true),'shopify_tags');
			$request_data['product']=array(
					"title"=>$drobe_data['Drobe']['comment'],
					"vendor"=>$drobe_data['SellDrobe']['sell_brand_name'],
					"body_html"=>$drobe_data['SellDrobe']['sell_description'],
					"product_type"=>$uploader_name,
					"tags"=> implode(",",$product_tags),
					"variants"=>array(array(
							"price"=>$drobe_data['SellDrobe']['sell_price'],
							"compare_at_price"=>$drobe_data['SellDrobe']['original_sell_price'],								
							"inventory_management"=> "shopify",
							//"inventory_policy"=> "continue",
							"requires_shipping"=> true,
							"taxable"=> true,
							"inventory_quantity"=>1,
							"fulfillment_service"=> "manual",
							"sku"=>sprintf("%04d",intval($drobe_data['Drobe']['id'])),
							"title"=> $product_size_name,
							"option1"=> $product_size_name
					))
			);
		
				
			/*
			 *  add main image in product
			*/
			if($_SERVER['HTTP_HOST']=="localhost")
			{
				$image_path=WWW_ROOT . Configure::read('drobe.upload_dir') .DS . $drobe_data['Drobe']['file_name'];
				$request_data['product']['images'][]=array(
						"attachment"=>base64_encode(file_get_contents($image_path))
				);
			}
			else
			{
				$request_data['product']['images'][]=array(
						"src" => Router::url("/drobe_images/iphone/".$drobe_data['Drobe']['file_name'],true)
				);
			}

			$response=$this->shopify("/products", json_encode($request_data),"POST");
			if(isset($response['product']) && $response['product']['id']!="")
			{
				//updating buy URL for the product
				$buy_url=Configure::read("sell_drobe.shop_url").$response['product']['handle'];
				$buy_url="'".$buy_url."'";
				$this->Drobe->updateAll(array('Drobe.buy_url_actual'=>$buy_url,'Drobe.buy_url'=>$buy_url),array('Drobe.id'=>$drobe_id));
		
				// adding product to collection
				/*if($collection_id>0)
				 {
				$this->_addProductToCollection($response['product']['id'],$collection_id);
				}*/
					
				$this->Drobe->SellDrobe->id=$drobe_data['SellDrobe']['id'];
				if($this->Drobe->SellDrobe->saveField('shopify_product_id',$response['product']['id']))
				{
					// uploading product images to shopify
					$this->_updateShopifyImages($drobe_data['SellDrobe']['id'],$response['product']['id'],$updateMainImage);
				}
				//$this->Session->setFlash("Selected product is published on Everdrobe Shop","default",array("class"=>"success"));
			}
			else
			{
				if(isset($response['errors']['base']) && count($response['errors']['base'])>0)
				{
					$this->Session->setFlash("Shopify Upload Error: ". $response['errors']['base'][0],"default",array("class"=>"error"));
				}
				else $this->Session->setFlash("Error occured in publish product in everdrobe shop","default",array("class"=>"error"));
			}
		}
	}
	
	
	//This service is used for Update Sell Drobe
	function update_sell_drobe($drobeid = null)
	{
		if($drobeid!=null)
		{
			if(!empty($this->data))
			{
				$drobe = array();
				try{
					$this->Drobe->recursive = 0;
					$find = $this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobeid)));
					if($find)
					{
						if(isset($this->data['image']) && $this->data['image']!="")
						{
							$file_name = $find['Drobe']['file_name'];
							$image_path=WWW_ROOT . Configure::read('drobe.upload_dir') .DS . $file_name;
							unlink($image_path);
							$image_path=WWW_ROOT . Configure::read('drobe.iphone.thumb.upload_dir') .DS . $file_name;
							unlink($image_path);
							$image_path=WWW_ROOT . Configure::read('drobe.iphone.upload_dir') .DS . $file_name;
							unlink($image_path);
							$image_path=WWW_ROOT . Configure::read('drobe.thumb.upload_dir') .DS . $file_name;
							unlink($image_path);
							
							$image_file=$this->_createDrobeImage($this->data['image']);
							$response['image_url']=Router::url("/drobe_images/".$image_file,true);
							$response['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$image_file,true);
							$drobe['Drobe.file_name'] = "'$image_file'";
							//$this->Drobe->updateAll(array('file_name'=>"'$image_file'"),array('Drobe.id'=>$drobeid));

							
							/****code by devang edit shopify image*****/
							$shopify_product_id=$find['SellDrobe']['shopify_product_id'];
							$images_in_shop=$this->shopify("/products/".$shopify_product_id."/images", "","GET");
							$first_image = array_shift($images_in_shop['images']);
							$res=$this->shopify("/products/".$shopify_product_id."/images/".$first_image['id'],"","DELETE");
							if($_SERVER['HTTP_HOST']=="localhost")
							{
								$image_path=WWW_ROOT . Configure::read('drobe.upload_dir') .DS .$image_file;
								$product_image=array(
										"image"=>array(
												"attachment"=>base64_encode(file_get_contents($image_path)),
												"filename"=>uniqid().".gif",
												"position" => "1"
										)
								);
							}
							else
							{
								$product_image=array(
										"image"=>array(
												"src" => Router::url('/drobe_images/'.$image_file,true),
												"position" => "1"
										)
								);
							}
							$this->log(print_r($product_image,true),"shopify");
							$img_response=$this->shopify("/products/".$shopify_product_id."/images", json_encode($product_image),"POST");
							$this->log(print_r($img_response,true),"shopify");
							/*****************************************/
							
						} 
						$drobe['Drobe.comment'] = "'".$this->data['comment']."'";
						$category=Cache::read('drobe_category');
						if(empty($category))
						{
							$this->loadModel('Category');
							$this->Category->_updateCategoryCache();
							$category=Cache::read('drobe_category');
						}
						if(array_key_exists($this->data['category_id'],$category))
						{
							$drobe['Drobe.category_id'] ="'".$this->data['category_id']."'";
						}
						else
						{
							$drobe['Drobe.category_id'] ="'".configure::read('other_category_id')."'";
						}
						
						//To update drobe field like comment, category_id or image if updated
						$this->Drobe->updateAll($drobe , array('Drobe.id'=>$drobeid));
						
						//custome size as a text code
						if(isset($this->data['size_id']) && $this->data['size_id']!="")
						{
							$sizeNames=Cache::read('size_list');
							if(empty($sizeNames))
							{
								$this->loadModel('Size');
								$this->Size->_updateSizeCache();
								$sizeNames=Cache::read('size_list');
							}
							$sizeName=$sizeNames[$this->data['size_id']];
						}
						elseif(isset($this->data['size_name']))
						{
							$sizeName=$this->data['size_name'];
						}
						/**********************/
						
						//custome Brand as a text code
						if(isset($this->data['brand_id']) && $this->data['brand_id']!="")
						{
							$brandNames=Cache::read('brand_list');
							if(empty($brandNames))
							{
								$this->loadModel('Brand');
								$this->Brand->_updateBrandCache();
								$brandNames=Cache::read('brand_list');
							}
							$brandName = $brandNames[$this->data['brand_id']];
						}
						elseif(isset($this->data['sell_brand_name']))
						{
							$brandName = $this->data['sell_brand_name'];
						}
						/**********************/
						
						$this->loadModel('SellDrobe');
						$sellDrobe = $this->SellDrobe->updateAll(
								array(
										'sell_brand_name'=>"'".$brandName."'",
										'size_name'=>"'".$sizeName."'",
										'sell_price'=>"'".$this->data['sell_price']."'",
										'is_brand_new'=>"'".$this->data['is_brand_new']."'",
										'original_sell_price'=>"'".$this->data['original_sell_price']."'",
										'sell_gender'=>"'".$this->data['sell_gender']."'",
										'sell_description'=>"'".$this->data['sell_description']."'"
									),
								array('SellDrobe.drobe_id'=>$drobeid)
						);
						if($sellDrobe)
						{	
							$this->Drobe->recursive =0;
							$this->add_drobe_to_shopify($drobeid);
							$response['type'] = 'success';
							$response['message'] = "Drobe updated successfully!!!";
						}
						else
						{
							$variables=array(
									"##drobe_comment"=>$this->data['comment'],
									"##brand_name"=>$brandName,
									"##drobe_size"=>$sizeName,
									"##sell_price"=>$this->data['sell_price']
							);
							/* Send email to admin when any problem occurs while updating drobe */
							$this->sendNotifyMail('drobe_update_admin', $variables,Configure::read('admin_email'),1);
							$response['type'] = 'error';
							$response['message'] = "Couldn't update the product";
						}	
					}
					else
					{
						$response['type'] = 'error';
						$response['message'] = 'Drobe not found';
					}
				}
				catch (Exception $e)
				{
					$response['message'] = "Couldn't update the product";
				}
			}				
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Invalid Parameter';
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
//function for delete unused images for drobes
	function delete_unused_images()
	{
		$this->Drobe->recursive=-1;
		$count=$count_th=$count_ip=$count_ip_th=$count_additional=$count_additional_th=0;
		$str="";
		
		$drobeImages=array_values($this->Drobe->find('list',array('fields'=>'file_name','conditions'=>array('Drobe.file_name !='=>""))));
		$dir=WWW_ROOT."drobe_images/";
		$files = scandir($dir);
		$unusedImages=array_diff($files,$drobeImages);
		
		foreach($unusedImages as $file)
		{
			if($file=="." || $file=="..") continue;
			$path=$dir.$file;
			if(is_file($path))
			{
				$count++;
				unlink($path);
				//$str.= "Deleted : ".$path."\n";
			}
		}
	
		$dir=WWW_ROOT."drobe_images/thumb/";
		$files = scandir($dir);
		$unusedImages=array_diff($files,$drobeImages);
	
		foreach($unusedImages as $file)
		{
			if($file=="." || $file=="..") continue;
			$path=$dir.$file;
			if(is_file($path))
			{
				$count_th++;
				unlink($path);
				//$str.=  "Deleted : ".$path."\n";
			}
		}
	
		$dir=WWW_ROOT."drobe_images/iphone/";
		$files = scandir($dir);
		$unusedImages=array_diff($files,$drobeImages);
	
		foreach($unusedImages as $file)
		{
			if($file=="." || $file=="..") continue;
			$path=$dir.$file;
			if(is_file($path))
			{
				$count_ip++;
				unlink($path);
				//$str.=  "Deleted : ".$path."\n";
			}
		}
	
		$dir=WWW_ROOT."drobe_images/iphone/thumb/";
		$files = scandir($dir);
		$unusedImages=array_diff($files,$drobeImages);
	
		foreach($unusedImages as $file)
		{
			if($file=="." || $file=="..") continue;
			$path=$dir.$file;
			if(is_file($path))
			{
				$count_ip_th++;
				unlink($path);
				//$str.=  "Deleted : ".$path."\n";
			}
		}
		
		
		$this->loadModel('SellDrobeImage');
		$this->SellDrobeImage->recursive=-1;
		$drobeImages=array_values($this->SellDrobeImage->find('list',array('fields'=>'file_name','conditions'=>array('SellDrobeImage.file_name !='=>""))));
		
		$dir=WWW_ROOT."drobe_images/additional/";
		$files = scandir($dir);
		$unusedImages=array_diff($files,$drobeImages);
		
		foreach($unusedImages as $file)
		{
			if($file=="." || $file=="..") continue;
			$path=$dir.$file;
			if(is_file($path))
			{
				$count_additional++;
				unlink($path);
				//$str.=  "Deleted : ".$path."\n";
			}
		}
		
		$dir=WWW_ROOT."drobe_images/additional/thumb/";
		$files = scandir($dir);
		$unusedImages=array_diff($files,$drobeImages);
		
		foreach($unusedImages as $file)
		{
			if($file=="." || $file=="..") continue;
			$path=$dir.$file;
			if(is_file($path))
			{
				$count_additional_th++;
				unlink($path);
				//$str.=  "Deleted : ".$path."\n";
			}
		}
		
		
		$str.=  "\n-:Total Deleted:-\n\nDrobe Files : ".$count;
		$str.=  "\nThumb Files : ".$count_th;
		$str.=  "\niPhone Files : ".$count_ip;
		$str.=  "\niPhone Thumb Files : ".$count_ip_th;
		$str.=  "\nAdditional Files : ".$count_additional;
		$str.=  "\nAdditional Thumb Files : ".$count_additional_th;
		return $str;
	}

	function admin_highest_rated_drobe()
	{
		$this->helpers[]='Time';
		$this->paginate = array(
				'fields'=>array('Drobe.id','Drobe.comment','Drobe.file_name','Drobe.uploaded_on','Drobe.views','Drobe.total_rate','Drobe.total_in','Drobe.total_out','Drobe.rate_index','User.id','User.first_name','User.username','User.last_name','Category.category_name','Drobe.total_favs','Drobe.total_in/(Drobe.total_in+Drobe.total_out) as percent_in','Drobe.total_out/(Drobe.total_in+Drobe.total_out) as percent_out'),
				'limit' => 20,
				'conditions' =>array('Drobe.deleted'=>0,'Drobe.rate_status'=>'open'),
				'order' =>  $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC",
				'group' =>"Drobe.id"
		);
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	
	/* Display closed drobe which is deleted by user itself*/
	function admin_closed_drobe()
	{
		$this->helpers[]='Time';
		$conditions=array("Drobe.rate_status"=>'close',"Drobe.deleted"=>0);
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['Drobe.id']=$search;
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['User.email LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
		}
		$this->paginate = array(
				'limit' => 10,
				'order' => "Drobe.uploaded_on DESC",
				'conditions'=>$conditions
		);
		$drobes = $this->paginate('Drobe');
		$this->set(compact('drobes'));
	}
	
	
	
	function total_url()
	{
		$total_url = $this->Drobe->find('count',array('conditions'=>array('Drobe.buy_url_actual !='=>'')));
		echo "Total Featured Drobes :: ".$total_url."<br>";
		$remain_url = $this->Drobe->find('count',array('conditions'=>array('Drobe.google_url'=>0,'Drobe.buy_url_actual !='=>'')));
		echo "Remaining Buy URL Drobes :: ".$remain_url."<br>";
		$tiny_drobe = $this->Drobe->find('count',array('conditions'=>array('Drobe.buy_url LIKE'=>'%tiny%')));
		echo "Tiny Drobes :: ".$tiny_drobe."<br>";
		$updated_short = $this->Drobe->find('count',array('conditions'=>array('Drobe.google_url'=>1)));
		echo "Update Buy URL Drobes :: ".$updated_short."<br>";
		$null_url = $this->Drobe->find('count',array('conditions'=>array('Drobe.buy_url'=>null,'Drobe.buy_url_actual !='=>'')));
		echo "NULL Buy URL Drobes :: ".$null_url."<br>";
		exit;
	
	}
	
	
	/* Replace tiny url to Google shortend url */
	function update_short_url()
	{
		$this->Drobe->recursive=-1;
		$drobe_data = $this->Drobe->find('all',array('limit'=>35,'order'=>'Drobe.id DESC','conditions'=>array('Drobe.google_url'=>0,'Drobe.buy_url_actual !='=>'')));
		foreach ($drobe_data as $data)
		{
				$url= $data['Drobe']['buy_url_actual'];
				$postData = array('longUrl' => $url);
				$jsonData = json_encode($postData);
				$curlObj = curl_init();
				curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key='.Configure::read('shorten_url.api_key'));
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlObj, CURLOPT_HEADER, 0);
				curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
				curl_setopt($curlObj, CURLOPT_POST, 1);
				curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
				//curl_setopt($curlObj, CURLOPT_REFERER, "http://www.everdrobe.com/");
				$response = curl_exec($curlObj);
				
				// Change the response json string to object
				$json = json_decode($response);
				curl_close($curlObj);
				
				if(isset($json->id) && $json->id!=null)
				{
				    $this->Drobe->updateAll(array('Drobe.google_url'=>'1','Drobe.buy_url'=>"'".$json->id."'"),array('Drobe.id'=>$data['Drobe']['id']));
				}
				else
				{
					echo "<pre>";
					echo "Buy URL ".$this->id."<br>".print_r($json);
					echo "Drobe Id : ".$data['Drobe']['id']."<br>";
					echo "<br>Actual Url : ".$url."<br>";
					exit;
				}
				usleep(rand(11000,15000)*100);
		}
		exit;
	}	
	
	
	
	/* Reset featured drobe order script @13/01/2014 */
	function reset_drobe_order()
	{
		$this->Drobe->recursive = -1;
		$drobes = $this->Drobe->find('all',array('conditions'=>array('Drobe.featured'=>1,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open'),'order'=>'Drobe.uploaded_on desc'));
		$i = 1;
		foreach ($drobes as $drobe)
		{
			$this->Drobe->id = $drobe['Drobe']['id'];
			$this->Drobe->saveField('order',$i);
			$i++;
		}
		echo "Success";
		exit;
	}
}
?>
