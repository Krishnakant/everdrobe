<?php 
class UserActivitiesController extends AppController
{

	function beforeFilter()
	{
		parent::beforeFilter();
		
	}
	
	function admin_index($user_id=null)
	{
		if($user_id>0)
		{
			$conditions=array('UserActivity.user_id'=>$user_id);
		}
		else $conditions=array();
		
		$this->UserActivity->bindModel(array("belongsTo"=>array("User")));
		$this->paginate = array(
			'fields'=>array('UserActivity.*','User.first_name','User.username',"User.last_name"),
			'limit' => 15,
			'order'=>'UserActivity.created_on ASC',
			'conditions'=>$conditions
		);
		$this->set('user_id',$user_id);
		$data = $this->paginate('UserActivity');
		$this->set(compact('data'));
	}
}
?>