<?php

class HighestRatedDrobesController extends AppController
{
	//var $scaffold;
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index(){
		
		//$this->HighestRatedDrobe->bindModel(array('belongsTo'=>array('Drobe')));
		//$this->HighestRatedDrobe->Drobe->bindModel(array('belongsTo'=>array('User')));
		
		
		//$this->set("data",$this->HighestRatedDrobe->find('all'));
		$this->helpers[]='Time';
		$this->paginate = array(
				'fields'=>array('HighestRatedDrobe.*','Category.category_name','Drobe.file_name','Drobe.total_favs','User.first_name','User.username','User.id','User.last_name','Drobe.uploaded_on'),
				'limit' => 30,
				'order'=>"HighestRatedDrobe.created_on DESC",
				'joins'=>array(
						array(
								'table' => 'drobes',
								'alias' => 'Drobe',
								'type' => 'INNER',
								'conditions' => array(
								'Drobe.id = HighestRatedDrobe.drobe_id')
						),
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'INNER',
							'conditions' => array(
							'User.id = Drobe.user_id')
						),
						array(
								'table' => 'categories',
								'alias' => 'Category',
								'type' => 'INNER',
								'conditions' => array(
										'Drobe.category_id = Category.id')
						)
						)
		);
		$data = $this->paginate('HighestRatedDrobe');
		//pr($data);exit;
		$this->set(compact('data'));
	}
}