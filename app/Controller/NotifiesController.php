<?php
App::uses('CakeEmail', 'Network/Email');
class NotifiesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		$notifies=$this->Notify->find('all',array('fields'=>array('Notify.id','Notify.action','Notify.subject','Notify.type')));
		$this->set('notifyList',$notifies);
	}
	function admin_add()
	{
		if($this->RequestHandler->isPost())
		{
			if($this->Notify->save($this->data))
			{
				$this->Session->setFlash("New Notification message added successfully",'default',array('class'=>"success"));
				$this->redirect(array('action'=>"index"));
			}
			else
			{
				if(!empty($this->Notify->validationErrors))
				{
					$this->set('errors',$this->Notify->validationErrors);
					$this->Notify->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Notify->id = $id;
		if(!empty($this->data))
		{
			if($this->Notify->save($this->data))
			{
				$this->Session->setFlash("Notidication message updated successfully",'default',array('class'=>"success"));
				$this->redirect(array('action'=>"index"));
			}
			else
			{
				if(!empty($this->Notify->validationErrors))
				{
					$this->set('errors',$this->Notify->validationErrors);
					$this->Notify->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Notify->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if($this->Notify->delete($id))
			{
				$this->Session->setFlash("Selected record deleted successfully",'default',array('class'=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in delete record");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	function get_content()
	{
		$response=array();
		$notifications_cache=Cache::read('notification_cache');
		if(empty($notifications_cache))
		{
			$this->Notify->updateNotificationCache();
			$notifications_cache=Cache::read('notification_cache');
		}
		$actions=array('twitter_invitation','facebook_invitation','sms_invitation','email_invitation');
		$data=array();
		foreach($actions as $action)
		{
			if(isset($notifications_cache[$action]))
			{
				$data[$action]=$notifications_cache[$action];
			}
			else 
			{
				$data[$action]=array();
			}
		}
		$response['type']="success";
		$response['data']=$data;
		
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
}