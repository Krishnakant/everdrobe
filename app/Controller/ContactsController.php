<?php
App::uses('CakeEmail', 'Network/Email');
class ContactsController extends AppController
{
	//public $components = array('RecaptchaPlugin.Recaptcha');
	//public $helpers = array('RecaptchaPlugin.Recaptcha');
	
	var $components = array('Recaptcha');
	var $helpers = array('Recaptcha');
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}
	function index()
	{
		if(!empty($this->data))
		{
			$error=false;
			if(!$this->Recaptcha->valid($this->data)) // return true or false
			{
				$this->Contact->set($this->data);
				$this->Contact->validates();
				$this->Contact->validationErrors[]=array("Your enter text not matched with text written in image");
				$error=true;
			}
			else
			{
				if(!$error && $this->Contact->save($this->data))
				{
					
					// seetting up variables for email
					$variables=array(
							"##full_name"=>$this->data['Contact']['full_name'],
							"##email_id"=>$this->data['Contact']['email_id'],
							"##message"=>$this->data['Contact']['message'],
							"##ip_addess"=>$this->RequestHandler->getClientIP()
					);
					// sending mail
					$this->sendNotifyMail('contact_us', $variables, Configure::read('contact_mail'),2);
					
					$this->Session->setFlash("Thanks, your message submitted successfully to EverDrobe administrator.",'default',array('class'=>"success"));
					$this->redirect(array('action'=>"index"));
				}
				else
				{
					$error=true;
				}
			}
			if($error && !empty($this->Contact->validationErrors))
			{
				$this->set('errors',$this->Contact->validationErrors);
				$this->Contact->validationErrors=array();
			}
		}
		
	}
}