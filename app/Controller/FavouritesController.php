<?php
class FavouritesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
	//	$this->Auth->allow();
	}
	function getlist($user_unique_id=null,$sort_by="recent",$last_faved_on="0")
	{
		$user_id=0;
		if($this->Auth->user('unique_id')==$user_unique_id)
		{
			$user_id=$this->Auth->user('id');
		}
		else
		{
			$this->loadModel('User');
			$user_id=$this->User->field('id',array('unique_id'=>$user_unique_id));
		}
		$conditions=array('Favourite.user_id'=>$user_id);
		$conditions['Drobe.deleted']=0;
		
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
		
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
		}
		//echo "<pre>";print_r($conditions);exit;
		$order="";
		$joins=array(
			array(
				'table'=> 'drobes',
				'type' => 'INNER',
				'alias' => 'Drobe',
			    'conditions' => array('Favourite.drobe_id = Drobe.id')
			),
			array(
				'table'=> 'users',
				'type' => 'INNER',
				'alias' => 'User',
				'conditions' => array('Drobe.user_id = User.id')
			),
			array(
					'table' => 'sell_drobes',
					'alias' => 'SellDrobe',
					'type' => 'LEFT',
					'conditions' => array(
							'SellDrobe.drobe_id = Drobe.id'
					)
			)
		);
		if($sort_by=="recent") 
		{
			// fixing issue of drobe repeat on next page;
			if($last_faved_on != "0")
			{
				$last_faved_on=date('Y-m-d H:i:s',$last_faved_on);
				if($last_faved_on!=null) $conditions['Favourite.created_on <= ']=$last_faved_on;
			}
			else
			{
				$latest=$this->Favourite->find('first',array('conditions'=>array('user_id'=>$user_id),'order'=>"id DESC"));
				if($latest) $last_faved_on=strtotime($latest['Favourite']['created_on']);
			}
			
			
			/*
			 * When user is logged in the in most resent tab need to display unrated drobes first
			*/
			if($this->Auth->user('id')>0)
			{
				$joins[]=array(
								'table' => 'rates',
								'alias' => 'Rate',
								'type' => 'LEFT',
								'conditions' => array(
										'Rate.drobe_id = Drobe.id',
										'Rate.user_id' => $this->Auth->user('id')
								)
						);
				
				
				$order="Rate.user_id ASC, Drobe.uploaded_on DESC";
			}
			else $order="Drobe.uploaded_on DESC";
		}
		if($sort_by=="rated") 
		{
			//$order="Drobe.total_rate DESC";
			//$order="Drobe.total_in/Drobe.total_rate DESC, Drobe.views DESC";
			$order="Drobe.rate_index DESC, Drobe.views DESC";
		}
		if($sort_by=="viewed") $order="Drobe.views DESC";
		
		if($sort_by=="featured")
		{
			$order="Drobe.featured DESC, Drobe.order ASC,Drobe.id DESC";
			$conditions[]=array("OR"=>array("Drobe.featured"=>1,"AND"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved")));
		}
		
		if($this->Auth->user('id')>0)
		{
		$this->loadModel('UserFilter');
			
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
			
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}  
			
			
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."' OR User.gender is null)))";
			}
			
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new") 
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
			
			
			
				
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
				
		}
		
		$fields =array('Drobe.*','SellDrobe.*','Favourite.*');
		
		if($sort_by=="recent" && strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		
		$this->paginate = array(
					'fields'=>$fields,
					'limit' => 24,
					'order'=>$order,
					'conditions'=>$conditions,
					'joins' => $joins,
					'group' => "Drobe.id"
		);
		
		$data = $this->paginate('Favourite');
		
		
		
		$this->set(compact('data'));
		//keep track on last_faved_on date for
		$this->set('last_faved_on',$last_faved_on);
	}
	function getlist_highest_rated($page=1,$user_unique_id=null)
	{
		$user_id=0;
		if($this->Auth->user('unique_id')==$user_unique_id)
		{
			$user_id=$this->Auth->user('id');
		}
		else
		{
			$this->loadModel('User');
			$user_id=$this->User->field('id',array('unique_id'=>$user_unique_id));
		}
		$conditions=array('Favourite.user_id'=>$user_id);
		$conditions['Drobe.deleted']=0;
	
		/**  If search paramter exist then this code will executes */
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
		
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
		}
		
		$order="";
		$joins=array(
				array(
						'table'=> 'drobes',
						'type' => 'INNER',
						'alias' => 'Drobe',
						'conditions' => array('Favourite.drobe_id = Drobe.id')
				),
				array(
						'table'=> 'users',
						'type' => 'INNER',
						'alias' => 'User',
						'conditions' => array('Drobe.user_id = User.id')
				),
			array(
					'table' => 'sell_drobes',
					'alias' => 'SellDrobe',
					'type' => 'LEFT',
					'conditions' => array(
							'SellDrobe.drobe_id = Drobe.id'
					)
			)
		);
		//$order="Drobe.rate_index DESC, Drobe.views DESC";
		
		$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		
		
		if($this->Auth->user('id')>0)
		{
		$this->loadModel('UserFilter');
			
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
			
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}  
			
			
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."' OR User.gender is null)))";
			}
			
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new") 
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
			
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
	
		}
		
		
		$fields =array('Drobe.*','SellDrobe.*','Favourite.*');
		
		if(strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		$data = $this->Favourite->find('all',array(
				'fields'=>$fields,
				'limit' => 24*$page,
				'offset'=>0,
				'order'=>$order,
				'conditions'=>$conditions,
				'joins' => $joins,
				'group' => "Drobe.id"
		));
		$this->set(compact('data'));
		//keep track on last_faved_on date for
		$this->set('last_faved_on',0);
	}
	function admin_reset_counter()
	{
		$favedata=$this->Favourite->find('all',array(
			'fields'=>array('Favourite.drobe_id','count(Favourite.id) as total'),
			'group' => 'Favourite.drobe_id'
		));
		$this->loadModel('Drobe');
		foreach($favedata as $fav)
		{
			$this->Drobe->updateAll(array('Drobe.total_favs'=>$fav[0]['total']),array('Drobe.id'=>$fav['Favourite']['drobe_id']));
		}
		exit();
	}
}
?>