<?php
App::import('Vendor', 'Gmail/apiClient');
App::import('Vendor', 'Gmail/contrib/apiOauth2Service');

class GmailConnectsController extends AppController {
	var $gmailClient;
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
		$this->gmailClient = new apiClient();
		$this->gmailClient->setApplicationName("Test Application");
		// create application from https://code.google.com/apis/console/
		// register callback/return url in application
		$this->gmailClient->setRedirectUri(Router::url('/gmail_connects/callback',true));
		
		// user application id and keysecret for registered callback 
		$this->gmailClient->setClientId('728629996183.apps.googleusercontent.com');
		$this->gmailClient->setClientSecret('Lof3A94pu0nge6LlyOm8A17W');
		
	}
	public function index() {
		$this->set("authUrl",$this->gmailClient->createAuthUrl());
	}
	public function callback() {
		
		if (isset($this->params->query['code'])) {
			$this->gmailClient->authenticate();
			$this->Session->write('token',$this->gmailClient->getAccessToken());
			$redirect = $this->params->self;
			return $this->redirect(filter_var($redirect, FILTER_SANITIZE_URL));
		}
		
		if ($this->Session->read('token')!=null) {
			$this->gmailClient->setAccessToken($this->Session->read('token'));
		}
		
		if (isset($this->params->query['logout'])) {
			// setting for logout
			unset($_SESSION['token']);
			$this->gmailClient->revokeToken();
		}
		
		if ($this->gmailClient->getAccessToken()) {
			// getting profile info	
			$GmailOauth = new apiOauth2Service($this->gmailClient);
			$this->set('user_profile',$GmailOauth->userinfo->get());
			$this->Session->write('token',$this->gmailClient->getAccessToken());
		}
	}
}