<?php
App::uses('CakeEmail', 'Network/Email');
class SettingsController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		$notifies=$this->Setting->find('all');
		$this->set('settingList',$notifies);
	}
	function admin_add()
	{
		if($this->RequestHandler->isPost())
		{
			if($this->Setting->save($this->data))
			{
				$this->Session->setFlash("New Setting added successfully",'default',array('class'=>"success"));
				$this->redirect(array('action'=>"index"));
			}
			else
			{
				if(!empty($this->Setting->validationErrors))
				{
					$this->set('errors',$this->Setting->validationErrors);
					$this->Setting->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Setting->id = $id;
		if(!empty($this->data))
		{
			if($this->Setting->save($this->data))
			{
				$this->Session->setFlash("Setting updated successfully",'default',array('class'=>"success"));
				$this->redirect(array('action'=>"index"));
			}
			else
			{
				if(!empty($this->Setting->validationErrors))
				{
					$this->set('errors',$this->Setting->validationErrors);
					$this->Setting->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Setting->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if($this->Setting->delete($id))
			{
				$this->Session->setFlash("Selected Setting deleted successfully",'default',array('class'=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in delete record");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	function getSetting($name=null)
	{
		$response=array();
		$settings_cache=Cache::read('settings_cache');
		if(empty($notifications_cache))
		{
			$this->Setting->updateSettingCache();
			$settings_cache=Cache::read('settings_cache');
		}
		if($name==null)
		{
			$data=$settings_cache;
		}
		else
		{
			$data=$settings_cache[$name];
		}
		$response['type']="success";
		$response['data']=$data;
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	/* Webservice for iphone app when application is under maintainance */
	function under_maintenance($type="ios")
	{
		$response = array();
		$data = array(
				'app_version'=>$type=="android"?Configure::read('android_app_version'):Configure::read('ios_app_version'),
				'under_maintenance'=> Configure::read('under_maintenance')? 'yes': 'no',
				"in_review"=>Configure::read('in_review')
				);
		
		//$response['type']= $type=="android"?"success":"error";
		$response['type']= "success";
		$response['data']=$data;
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	
}
