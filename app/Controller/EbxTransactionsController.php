<?php 
class EbxTransactionsController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_user($userid=null)
	{
		if($userid!=null)
		{
			$this->helpers[]='Time';
			$conditions=array();
			$conditions['EbxTransaction.user_id']=$userid;
			$this->paginate = array(
					'limit' => 30,
					'conditions'=>$conditions,
					'order'=>"EbxTransaction.created_on DESC"
			);
			$data = $this->paginate('EbxTransaction');
			$this->loadModel("User");
			$this->User->recursive=-1;
			$userData=$this->User->findById($userid);
			$this->set("username",$userData['User']['first_name']." ".$userData['User']['last_name']." (".$userData['User']['username'].")");
			$this->set("userid",$userData['User']['id']);
			$this->set(compact('data'));
		}
	}
	function admin_user_redeem_request()
	{
		$this->helpers[]='Time';
		$conditions=array();
		$conditions['EbxTransaction.transaction_type']="redeem";
		$this->paginate = array(
				'limit' => 30,
				'conditions'=>$conditions,
				'order'=>"EbxTransaction.created_on DESC"
		);
		$data = $this->paginate('EbxTransaction');
		$this->set(compact('data'));
	}
	function admin_manage_ebx($userid=null){
		if($userid!=null)
		{
			/*load user data*/
			$this->loadModel("User");
			$data=$this->data;
			$this->User->recursive=0;
			
			/*bind using model and unbind unused model*/
			$this->User->bindModel(array('hasOne'=>array("UserTotal")));
			$this->User->unbindModel(array("hasMany"=>array("Drobe","Rate","Follower","StreamCategory"),"hasOne"=>array("UserSetting")));
			$userData=$this->User->findById($userid);
			$this->set("userData",$userData);
			if(!empty($data))
			{
				$this->EbxTransaction->set($data);
				//if validate data
				if($this->EbxTransaction->validates())
				{
					//find user total ebx data
					$userTotalsData=$this->User->UserTotal->find('first',array('conditions'=>array('UserTotal.user_id'=>$userid)));
					
					//set final current balance
					$data['EbxTransaction']['current_balance'] = $userTotalsData['UserTotal']['current_ebx_point']+$this->data['EbxTransaction']['points'];
					
					//update current ebx point
					$userTotals=array();
					$userTotals['UserTotal.current_ebx_point']=$data['EbxTransaction']['current_balance'];
					
					//if increament by admin then also add points to total earned points
					if($this->data['EbxTransaction']['points']>0)
					{
						$userTotals['UserTotal.earned_ebx_point']=$userTotalsData['UserTotal']['earned_ebx_point']+$this->data['EbxTransaction']['points'];
					}
					
					if($this->User->UserTotal->updateAll($userTotals,array('UserTotal.user_id'=>$userid)))
					{
						//save ebx transaction
						$this->EbxTransaction->save($data);
						$this->redirect(array("controller"=>"ebx_transactions","action"=>"user","admin"=>true,$userid));
					}
				}
				else 
				{
					if(!empty($this->EbxTransaction->validationErrors))
					{
						$this->set('errors',$this->EbxTransaction->validationErrors);
						$this->EbxTransaction->validationErrors=array();
					}
				}	
			}
		}
	}
	
	/**** This function read user request for redeem points which are credit for user and subtract redeem point if credit point is greater than 500 points. ****/
	function redeem_ebx_point(){
		
		if(!empty($this->data))
		{
			if(isset($this->data['ebx_redeem']) && $this->data['ebx_redeem']>=500)
			{
				//deduct ebx
				$this->loadModel("UserTotal");
				$this->UserTotal->recursive=-1;
				$userTotals=$this->UserTotal->find('first',array('conditions'=>array('UserTotal.user_id'=>$this->Auth->user('id'))));
				if($userTotals['UserTotal']['current_ebx_point']>=$this->data['ebx_redeem'])
				{
					$balance = $userTotals['UserTotal']['current_ebx_point']-$this->data['ebx_redeem'];
					$this->UserTotal->id=$userTotals['UserTotal']['id'];
					if($this->UserTotal->saveField('current_ebx_point',$balance))
					{
						$this->Session->setFlash("Redeem successfully","default",array("class"=>"success"));
											
					}
		
					//entry to ebx deduct history with generate code 13 char uppercase+number
					$couponCode = strtoupper(uniqid());
					$description="Redeem ebx requested from user for ".$this->data['ebx_redeem']." ebx, Generated Coupon code : ".$couponCode;
					$this->ebx_transactions_add("redeem",$this->Auth->user('id'),($this->data['ebx_redeem']*-1),$balance,0,$description);
		
					// Sending mail to support and user for notify
					$this->loadModel("User");
					$this->User->recursive=-1;
					$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
					$variables=array();
					$email_to=$userData['User']['email']."||".Configure::read('contact_mail');
					$this->sendNotifyMail('redeem_request', array(),$email_to,1);
					$this->redirect(array('controller'=>"users","action"=>"my_everbucks"));
				}
				else
				{
					$this->Session->setFlash("You have no sufficient Balance of EBX","default",array("class"=>"error"));
					$this->redirect(array('controller'=>"users","action"=>"my_everbucks"));
				}
			}
			else 
			{
				$this->Session->setFlash("Please pass EBX greter than 500","default",array("class"=>"error"));
				$this->redirect(array('controller'=>"users","action"=>"my_everbucks"));
			}
		}
		else
		{
			$this->Session->setFlash("Please pass EBX to redeem","default",array("class"=>"error"));
			$this->redirect(array('controller'=>"users","action"=>"my_everbucks"));
		}
	}
	function redeem_ebx($user_id=null){
		$response=array();
		if($user_id!=null)
		{
			if(!empty($this->data))
			{
				if(isset($this->data['ebx_redeem']) && $this->data['ebx_redeem']>=500)
				{
					//deduct ebx
					$this->loadModel("UserTotal");
					$this->UserTotal->recursive=-1;
					$userTotals=$this->UserTotal->find('first',array('conditions'=>array('UserTotal.user_id'=>$user_id)));
					if($userTotals['UserTotal']['current_ebx_point']>=$this->data['ebx_redeem'])
					{
						$balance = $userTotals['UserTotal']['current_ebx_point']-$this->data['ebx_redeem'];
						$this->UserTotal->id=$userTotals['UserTotal']['id'];
						if($this->UserTotal->saveField('current_ebx_point',$balance))
						{
							$response['type']="success";
							$response['current_ebx']="".$balance;
							$response['message']="Redeem successfully";
						}
						
						//entry to ebx deduct history with generate code 13 char uppercase+number
						$couponCode = strtoupper(uniqid());
						$description="Redeem ebx requested from user for ".$this->data['ebx_redeem']." ebx, Generated Coupon code : ".$couponCode;
						$this->ebx_transactions_add("redeem",$user_id,($this->data['ebx_redeem']*-1),$balance,0,$description);
						
						// Sending mail to support and user for notify
						$this->loadModel("User");
						$this->User->recursive=-1;
						$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
						$variables=array();
						$email_to=$userData['User']['email']."||".Configure::read('contact_mail');
						$this->sendNotifyMail('redeem_request', array(),$email_to,1);
					}
					else
					{
						$response['type']="error";
						$response['message']="You have no sufficient Balance of EBX";
					}
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Please pass EBX to redeem";
			}			
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid Parameter";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>