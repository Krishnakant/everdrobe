<?php 
class ProvincesController extends AppController
{
	//var $scaffold;
	function beforeFilter()
	{
		parent::beforeFilter();
		$country=$this->Province->Country->find('first',array('conditions'=>array('id'=>$this->params->params['pass'][0])));
		$this->set('country',$country['Country']);
	}
	function admin_index($country_id)
	{
		//$provinces=$this->Province->find('all',array('conditions'=>array('country_id'=>$country_id)));
		//$this->set('provinces',$provinces);
		$this->paginate = array(
			'limit' => 15,
			'conditions'=>array('country_id'=>$country_id)
		);
		$data = $this->paginate('Province');
		$this->set(compact('data'));
	}
	function admin_add($country_id)
	{
		if(!empty($this->data))
		{
			$province_data = $this->data['Province'];
			$province_data['short_name']=$province_data['province_name'];
			if($this->Province->save($province_data))
			{
				$this->Session->setFlash("New State/Province added successfully.",'default',array('class'=>"success"));
				$this->redirect(array('action'=>"index",$country_id));
			}
			else 
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($country_id=null,$id=null)
	{
		$this->Province->id=$id;
		if(!empty($this->data))
		{
			//$this->Country->validate['country_name']['isUnique']['on']='update';
			//$this->Province->validate['province_name']['isUnique']['on']='update';
			$province_data = $this->data['Province'];
			$province_data['short_name']=$province_data['province_name'];
			if($this->Province->save($province_data))
			{
				$this->Session->setFlash("State/Province name changed successfully",'default',array('class'=>"success"));
				$this->redirect(array('action'=>"index",$country_id));
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Province->read();
		}
	}
	function admin_delete($country_id=null,$id=null)
	{
		if($id>0)
		{
			if($this->Province->delete($id))
			{
				$this->Session->setFlash("Selected State/Province deleted successfully",'default',array('class'=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in delete category");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect($this->referer());
	}
	function getlist($country_id=null)
	{
		
		$provinces=$this->Province->find('all',array('conditions'=>array('Province.country_id'=>$country_id)));
		$data=array();
		foreach($provinces as $province)
		{
			unset($province['Province']['country_id'],$province['Province']['status']);
			if($province['Province']['province_name']!=null)
			{
				$data[]=$province['Province'];
			}
		}
		if(count($data)==0)
		{
			$response['type']="error";
			$response['message']="Provinces not found for this country";
		}
		else 
		{
			$response['type']="success";
			$response['provinces']=$data;
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
		
	}
}
?>