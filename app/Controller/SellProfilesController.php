<?php 
class SellProfilesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		if($this->Auth->user('type')!="admin")
		{
			$this->layout="sell_drobe";
		}
	}
	function sell_profile()
	{
		if(Configure::read('setting.drobe_sale')!="on")
		{
			// if sell drobe feature is off from admin
			throw new NotFoundException("Not found");
		}
		
		if(!empty($this->data))
		{
			
			$profile_data=$this->data['SellProfile'];
			$profile_data['user_id']=$this->Auth->user('id');
			$this->SellProfile->set($profile_data);
			/*if($profile_data['payment_type']=="cheque")
			{
				$this->SellProfile->setChequeValidation();
			}
			else
			{
				$this->SellProfile->setPaypalValidation();
			}*/
			
			/*		If payment type is cheque then email ID is not required so it is unset from validation*/
			if($profile_data['payment_type']=="cheque")
			{
				unset($this->SellProfile->validate['paypal_email']);
			}
			
			if($this->SellProfile->validates())
			{
				if($this->SellProfile->save($profile_data))
				{
					$this->Session->setFlash("Your sell profile has been updated successfully",'default',array('class'=>"success"));
					//$this->redirect($this->referer());					
				}
				else
				{
					$this->Session->setFlash("Error occured in updating your payment profile detail",'default');
				}
			}
			else
			{
				if(!empty($this->SellProfile->validationErrors))
				{
					$this->set('errors',$this->SellProfile->validationErrors);
					$this->SellProfile->validationErrors=array();
				}
			}
		}
		else
		{
			$this->SellProfile->id=$this->SellProfile->field('id',array('user_id'=>$this->Auth->user('id')));
			$this->data=$this->SellProfile->read();
		}
	}
	
	
	//This webservice for get profile for seller
	function get_seller_profile($userid=null)
	{
		$response = array();
		if($userid!=null)
		{
			$profile = $this->SellProfile->find('first',array(
					'fields'=>array('SellProfile.full_name','SellProfile.street1','SellProfile.street2','SellProfile.city','SellProfile.state','SellProfile.zip',
							'SellProfile.paypal_email','SellProfile.payment_type','SellProfile.requested_amount',
							'SellProfile.deposit_name','SellProfile.account_no','SellProfile.routing_no','SellProfile.bank_name','SellProfile.account_type'
							),
					'conditions'=>array('SellProfile.user_id'=>$userid)
					));
			if($profile)
			{
				$response['type'] = 'success';
				$response['SellProfile'] = $profile['SellProfile'];
			}
			else 
			{
				$response['type'] = 'success';
				$response['message'] = 'Seller not found';
				$response['SellProfile'] = array();
			}
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Invalid parameter';
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	//This webservice for update seller profile
	function update_seller_profile($userid=null)
	{
		if($userid!=null)
		{
			if(!empty($this->data))
			{	
				$seller_profile = array();
				
					$is_profile = $this->SellProfile->find('first',array('conditions'=>array('SellProfile.user_id'=>$userid)));
					if(!empty($is_profile))
					{	
						if(isset($this->data['payment_type']) && $this->data['payment_type']=='paypal')
						{
							$seller_profile['paypal_email'] = "'".$this->data['paypal_email']."'";
						}
										
						$seller_profile['SellProfile.full_name'] = "'".$this->data['full_name']."'";
						$seller_profile['SellProfile.street1'] = "'".$this->data['street1']."'";
						$seller_profile['SellProfile.street2'] = "'".$this->data['street2']."'";
						$seller_profile['SellProfile.city'] = "'".$this->data['city']."'";
						$seller_profile['SellProfile.state'] = "'".$this->data['state']."'";
						$seller_profile['SellProfile.zip'] = "'".$this->data['zip']."'";
						$seller_profile['SellProfile.payment_type'] = "'".$this->data['payment_type']."'";
						$this->SellProfile->updateAll($seller_profile,array('SellProfile.user_id'=>$userid));
						$response['type'] = 'success';
						$response['message'] = "Profile updated successfully!!!";
					}
					else 
					{
						$this->loadModel('User');
						$this->User->recursive = -1;
						$is_valid_user = $this->User->find('first',array('conditions'=>array('User.id'=>$userid)));
						if(!empty($is_valid_user))
						{
							$seller_profile = $this->data;
							$seller_profile['user_id'] = $userid;
							$profile = $this->SellProfile->save($seller_profile);
							$response['type'] = 'success';
							$response['message'] = "Profile updated successfully!!!";
						}
						else
						{
							$response['type'] = 'error';
							$response['message'] = "User not found.";
						}
					}
					
			}
			else 
			{
				$response['type'] = 'error';
				$response['message'] = 'Parameters are required';
			}			
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Invalid parameter';
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>