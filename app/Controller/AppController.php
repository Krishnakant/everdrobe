<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	var $components = array( 'Session','Auth','RequestHandler');
	var $isFbLoggedin=false;
	var $facebook;
	var $twitterObj;
	var $gmailClient;
	function beforeFilter()
	{
		parent::beforeFilter();
		date_default_timezone_set('UTC');
		$this->fetch_settings();
		// restrict access user of blocked ip address
		$this->_is_blocked_ip();
		if(isset($this->params->params['ext']) && ($this->params->params['ext']=="json" || $this->params->params['ext']=="xml"))
		{
			
			$this->webservice_start=microtime(true);
			$this->Auth->Allow();
			Configure::write('debug',0);
			
			/*if(isset($this->params->query['device_type']) && $this->params->query['device_type']!="" && Configure::read('under_maintenance'))
			{
				$this->setAction("under_maintenance_all",$this->params->query['device_type']);
			}*/
				
			
			if(!empty($this->data)) $this->log("POST: ". http_build_query($this->data),'webservice');
			
			if($this->params->params['controller']=="drobes" && $this->params->params['action']=="add_drobe")
			{
				/* $this->log("URL: ". $this->params->url,'drobe_upload');
				$this->log("POST: ". print_r($this->data,true),"drobe_upload");
				$this->log("Before Webservice Processed: ". $this->webservice_start, 'drobe_upload'); */
			}
		}
		else
		{
			if($this->RequestHandler->isAjax())
			{
				Configure::write('debug',0);
			}
			if(isset($this->params['prefix']) && $this->params['prefix']=="admin")
			{
				$this->layout="admin";
				$this->Auth->userModel="Admin";
				$this->Auth->loginAction=array('controller' => 'admins', 'action' => 'login', 'admin'=>true);
			}
			else
			{
				if($this->Auth->user('id')>0)
				{
					if(!($this->request->params['controller']=="users" && ( in_array($this->request->params['action'],array("change_username","logout","my_totals")))))
					{
						if($this->Auth->user('username')=="")
						{
							$this->redirect(array('controller' => 'users', 'action' => 'change_username'));
							
						}
					}
				}
				
					$this->Auth->fields = array('username' => 'email', 'password' => 'password');
				$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'index');
				$this->Auth->logoutRedirect = array('controller' => '/', 'action' => 'index');
				
			}
			$this->Auth->authorize = 'Controller';
				
			// initialize facebook connection
			$this->initialize_facebook_connection();
			if(!($this->Auth->user('id')>0))
			{
				$this->initialize_gmail_connection();
			}
		}
	}
	function afterFilter()
	{
		parent::afterFilter();
		if(isset($this->params->params['ext']) && ($this->params->params['ext']=="json" || $this->params->params['ext']=="xml"))
		{
			$taken_time=microtime(true) -$this->webservice_start;
			$this->log("Time Taken: ". $taken_time,'webservice');
			if($this->params->params['controller']=="drobes" && $this->params->params['action']=="add_drobe")
			{
				$this->log("Time Taken: ". $taken_time,'drobe_upload');
				$this->log("======================================================",'drobe_upload');
			}
		}
	} 
	function getUserStatus()
	{
		if(isset($this->params->requested) && $this->params->requested==1)
		{
			// if internal request then no need of ckeck user status
			return;
		}
		// create map of controller and action in which for check user status
		$check_actions=array(
				"drobes"=>array("index","mydrobe","rate","conversation","faves","results","result"),
				"users"=>array("drobe_list","profile","following","follower","edit_profile","forgot_password","change_password","change_email"),
				"sell_drobes"=>array("index","drobes","edit","relist","mark_sell","sell_drobes","sold_drobes"),
				"sold_drobes"=>array("index","profile"),
				"sell_profiles"=>array("sell_profile")
		);
	
		$controller=$this->params->controller;
		$action=$this->params->action;
	
		if(!(isset($check_actions[$controller]) && in_array($action, $check_actions[$controller])))
		{
			// if controller and action not matched then return;
			return;
		}
	
		$this->loadModel('User');
		$userData=$this->Session->read('Auth');
		$status=$this->User->field('status',array('id'=>$userData['User']['id']));
		if($status=="new")
		{
			$reg_time=strtotime($userData['User']['created_on']);
			$curr_time=time();
			if($curr_time-$reg_time > (Configure::read('suspended_after')*3600) )
			{
				$status="inactive";
			}
			else
			{
				$status="active";
			}
		}
		else if($status=="suspended")
		{
			$this->Session->setFlash("Account is not active, please contact us by sending an email to: support@everdrobe.com","default",array("class"=>"popup_error"));
			if($this->Auth->logout())
			{
				return $this->redirect(array('controller' => 'drobes', 'action' => 'index'));
			}
		}
	
		if($userData['User']['status']!=$status)
		{
			$userData['User']['status']=$status;
			$this->Session->write('Auth',$userData);
		}
	}
	function fetch_settings()
	{
		$settings=Cache::read('settings_cache');
		if(empty($settings))
		{
			$this->loadModel('Setting');
			$this->Setting->updateSettingCache();
			$settings=Cache::read('settings_cache');
		}
		foreach($settings as $key=>$value)
		{
			Configure::write('setting.'.$key,$value);
		}
	}
	function track_user_activity($activity_data)
	{
		$activity_data['ip_address']=$this->RequestHandler->getClientIp();
		$activity_data['created_on']=date('Y-m-d H:i:s');
		
		if(!isset($activity_data['user_id'])) $activity_data['user_id']=0;
		if(!isset($activity_data['activity'])) $activity_data['activity']="";
		if(!isset($activity_data['device_type']))
		{
			if(isset($this->params->query['device_type'])) $activity_data['device_type']= $this->params->query['device_type'];
		}
		$this->loadModel('UserActivity');
		$this->UserActivity->save($activity_data);
	}
	function initialize_facebook_connection()
	{
		App::import('Vendor', 'facebook/facebook');
		//$this->set('facebookUrl','https://www.facebook.com/dialog/oauth?client_id='.Configure::read('facebook.api_key').'&redirect_uri=http%3A%2F%2Flocalhost%2Feverdrobe%2Fusers%2Ffacebook_connect&state=86123154215d3922876b51bd22277151&scope=email%2Cuser_about_me%2Cfriends_about_me%2Cread_stream%2Cpublish_stream%2Coffline_access');
		$this->facebook = new Facebook(array("appId"=>Configure::read("facebook.api_key"),"secret"=> Configure::read("facebook.app_secrete")));
		$this->set("facebookUrl",$this->facebook->getLoginUrl(
				array(
						'scope' => 'email,user_about_me,friends_about_me,read_stream,publish_stream,offline_access',
						'redirect_uri' => "http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"facebook_connect")),
						'display'=>"popup"
				)
		));
	}
	function initialize_gmail_connection()
	{
		
		App::import('Vendor', 'Gmail/apiClient');
		App::import('Vendor', 'Gmail/contrib/apiOauth2Service');
		
		//gmail_connect
		$this->gmailClient = new apiClient();
		$this->gmailClient->setApplicationName("Everdrobe");
		// register callback/return url in application
		$this->gmailClient->setRedirectUri(Router::url('/users/gmail_connect',true));
		$this->gmailClient->setScopes("https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile");
		// user application id and keysecret for registered callback
		$this->gmailClient->setClientId(Configure::read("gmail.client_id"));
		$this->gmailClient->setClientSecret(Configure::read("gmail.secret_key"));
		$this->set("gmailAuthUrl",$this->gmailClient->createAuthUrl());
	}
	function beforeRender()
	{
		
		parent::beforeRender();
		if($this->name == 'CakeError') {
			$this->initialize_facebook_connection();
			$this->initialize_gmail_connection();
			$this->layout = 'default';
		}
		
		if(!(isset($this->params->params['ext']) && ($this->params->params['ext']=="json" || $this->params->params['ext']=="xml")))
		{
			if($this->Auth->user('id')>0 && $this->Auth->user('type')=="admin")
			{
				
				/*New sell drobes uploaded by seller which are pending for approval default it display red badges. */
				$this->loadModel('Drobe');
				$this->Drobe->recursive = 1;
				/*$pending_drobes= $this->Drobe->find('count',array('conditions'=>array('Drobe.post_type'=>"sell",'SellDrobe.status'=>'pending','Drobe.deleted'=>0,"OR"=>array(array('Drobe.rate_status'=>'open'),
						array('Drobe.rate_status'=>'close','SellDrobe.shopify_product_id >'=>0))))); */
				
				/*New label request send by seller which are pending for approval by admin it display as red badges */
				$this->loadModel('RequestForLabel');
				$pending_request = $this->RequestForLabel->find('count',array('conditions'=>array('RequestForLabel.status'=>'pending')));
				
				/* New problem send by seller which are pending for approval by admin it display as red badges. */
				$this->loadModel('SellerProblem');
				$pending_problem = $this->SellerProblem->find('count',array('conditions'=>array('SellerProblem.status'=>'pending')));
				
				/* New redeem request send by seller which are unpaid  by admin it display as red badges. */
				$this->loadModel('RedeemRequest');
				$unpaid_redeem = $this->RedeemRequest->find('count',array('conditions'=>array('RedeemRequest.status'=>'unpaid')));
				
				$this->set(compact('pending_drobes','pending_request','pending_problem','unpaid_redeem'));
			}
		}
		
		if(!(isset($this->params->params['ext']) && ($this->params->params['ext']=="json" || $this->params->params['ext']=="xml")))
		{
			if($this->Auth->user('id')>0 && $this->Auth->user('type')!="admin")
			{
				// checking user status wether admin has changed
				if(!$this->RequestHandler->isAjax() && $this->Auth->user('id')>0 && ($this->RequestHandler->isGet() || $this->RequestHandler->isPost()))
				{
					$this->getUserStatus();
				}
				if($this->layout!="sell_drobe")
				{
					// counting new drobes of followings
					$this->loadModel('NewDrobe');
					$new_drobes=$this->NewDrobe->find('count',array('conditions'=>array("NewDrobe.follower_id"=>$this->Auth->user('id'))));
				
				
					// counting new rate response on my uploaded drobe
					$this->loadModel('Drobe');
					$new_rate_response=$this->Drobe->find('all',array('fields'=>array('sum(Drobe.new_response) as total'),'conditions'=>array("Drobe.user_id"=>$this->Auth->user('id'),'Drobe.rate_status'=>"open",'Drobe.deleted'=>0,"Drobe.id > "=>0)));
					$new_rate_response=array_pop($new_rate_response);
					$new_rate_response=array_pop($new_rate_response);
					
					//counting gave feedback conversations new response
					$this->loadModel('Rate');
					$new_conversation_response=$this->Rate->find('all',array('fields'=>array('sum(Rate.new_response) as total'),'conditions'=>array("Rate.user_id"=>$this->Auth->user('id'),"Rate.rate !="=>0,'Rate.conversation_status'=>"open")));
					$new_conversation_response=array_pop($new_conversation_response);
					$new_conversation_response=array_pop($new_conversation_response);
					
					// counting new conversations reply from my drobe conversations
					$mydrobes=$this->Drobe->find('list',array('fields'=>array('Drobe.id'),'conditions'=>array('Drobe.user_id'=>$this->Auth->user('id'),'Drobe.rate_status'=>"open", 'Drobe.deleted'=>0)));
					$new_conversations_reply=$this->Rate->find('all',array('fields'=>array('sum(Rate.new_reply) as total'),'conditions'=>array("Rate.drobe_id"=>$mydrobes,"Rate.rate !="=>0,'Rate.conversation_status'=>"open")));
					$new_conversations_reply=array_pop($new_conversations_reply);
					$new_conversations_reply=array_pop($new_conversations_reply);
					
					$this->set('new_drobe_notifications', $new_drobes);
					$this->set('new_response_notifications', $new_rate_response['total']+$new_conversation_response['total']+$new_conversations_reply['total']);
				}
			}
		}
		else
		{
			if(!empty($this->params->query))
			{
				$this->set('query_params',$this->params->query);
				$this->set('_serialize',array('response','query_params'));
			}	
		}
	}
	function _is_blocked_ip()
	{
		if ($this->name == 'CakeError') return;
		if($this->params->params["controller"]=="users" && $this->params->params["action"]=="block")
		{
			return;
		}
		$block_list=Cache::read("block_list");
		if($block_list==null)
		{
			$this->loadModel('Block');
			$this->Block->_updateBlockCache();
			$block_list=Cache::read("block_list");
		}
		$clientIp=$this->RequestHandler->getClientIp();
		foreach($block_list as $block_ip)
		{
			if($this->_cidr_match($clientIp,$block_ip))
			{
				$this->redirect(array("controller"=>"users","action"=>"block"));
			}
		}
	}
	function _cidr_match($ip, $range)
	{
		@list ($subnet, $bits) = split('/', $range);
		$ip = ip2long($ip);
		$subnet = ip2long($subnet);
		$bits=intval($bits);
		if($bits==0) return $ip==$subnet;
		$mask = -1 << (32 - $bits);
		$subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
		return ($ip & $mask) == $subnet;
	}
	function isAuthorized()
	{
		if($this->Auth->user('id')>0)
		{
			if(isset($this->params->params['prefix']) && $this->params->params['prefix']=="admin")
			{
				return $this->Auth->user('type')=="admin"; 	
			}
			else 
			{
				if( $this->Auth->user('type')=="admin" && in_array($this->params->params['action'],array("link_with_facebook","link_with_twitter","disconnect_from_facebook","disconnect_from_twitter")))
				{
					$this->layout="ajax";
					return true;			
				}
				return $this->Auth->user('type')!="admin";
			}
		}
		else return true;
	}
	
	
	function sendNotifyMail_working($action,$variables,$email_to)
	{
		App::import('Vendor', 'PhpMailer', array('file' => 'phpMailer' . DS . 'class.phpmailer.php'));
		$this->loadModel("Notify");
		$data=$this->Notify->get_mail_data($action);
		
		// replacing variables in appropriate place in subject line and mail body
		$mail_sub=str_replace(array_keys($variables), $variables, $data['Notify']['subject']);
		$mail_message=str_replace(array_keys($variables), $variables, $data['Notify']['message']);
		$mail_message=eregi_replace("[\]",'',$mail_message);
		
		$mail= new PHPMailer();
		if(Configure::read('smtp.enabled'))
		{
			$mail->IsSMTP(); // telling the class to use SMTP
			$mail->SMTPAuth      = true;                  // enable SMTP authentication
			$mail->SMTPSecure = 'ssl';
			$mail->Host          = Configure::read('smtp.host'); // sets the SMTP server
			$mail->Port          = Configure::read('smtp.port');                    // set the SMTP port for the GMAIL server
			$mail->Username      = Configure::read('smtp.username'); // SMTP account username
			$mail->Password      = Configure::read('smtp.password');        // SMTP account password
		}
		else
		{
			$mail->SMTPAuth      = false;
		}
		$mail->SetFrom(Configure::read('mail.from'), Configure::read('mail.from_name'));
		$mail->AddReplyTo(Configure::read('mail.reply_to'), Configure::read('mail.to_name'));
		
		$mail->Subject       = $mail_sub;
		$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$mail->MsgHTML($mail_message);
		$mail->AddAddress($email_to, '');
		
		if(!$mail->Send()) {
			return false;
		} else {
			return true;
		}
	}
	
	function sendNotifyMail($action,$variables,$email_to,$priority=0)
	{
		//return true;
		// for developemnt email we are escaping
        if(strpos($email_to,"_dvl@")>0)
		{
			return;
		}
		$this->loadModel("Notify");
		$data=$this->Notify->get_mail_data($action);
	
		// replacing variables in appropriate place in subject line and mail body
		$mail_sub=str_replace(array_keys($variables), $variables, $data['Notify']['subject']);
		$mail_message=str_replace(array_keys($variables), $variables, $data['Notify']['message']);
		$mail_message=eregi_replace("[\]",'',$mail_message);
	
		if(Configure::read('mail_from_queue')==true)
		{
			// Save mail in queue instend of send instantly
			$this->loadModel('MailQueue');
			$queueData= array(
					'mail_to'=> $email_to."||"."",
					'mail_from'=>Configure::read('mail.from')."||".Configure::read('mail.from_name'),
					'subject'=>$mail_sub,
					'body'=>$mail_message,
					'alt_body'=>"To view the message, please use an HTML compatible email viewer!",
					'priority'=>$priority,
			);
			$this->MailQueue->save($queueData);
			$this->MailQueue->id=null;
			return true;
		}
		else
		{
			App::import('Vendor', 'PhpMailer', array('file' => 'phpMailer' . DS . 'class.phpmailer.php'));
			$mail= new PHPMailer();
			if(Configure::read('smtp.enabled'))
			{
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->SMTPAuth      = true;                  // enable SMTP authentication
				$mail->SMTPSecure = 'ssl';
				$mail->Host          = Configure::read('smtp.host'); // sets the SMTP server
				$mail->Port          = Configure::read('smtp.port');                    // set the SMTP port for the GMAIL server
				$mail->Username      = Configure::read('smtp.username'); // SMTP account username
				$mail->Password      = Configure::read('smtp.password');        // SMTP account password
			}
			else
			{
				$mail->SMTPAuth      = false;
			}
			$mail->SetFrom(Configure::read('mail.from'), Configure::read('mail.from_name'));
			$mail->AddReplyTo(Configure::read('mail.reply_to'), Configure::read('mail.to_name'));
		
			$mail->Subject       = $mail_sub;
			$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
			$mail->MsgHTML($mail_message);
			$mail->AddAddress($email_to, '');
		
			if(!$mail->Send()) {
				return false;
			} 
			else 
			{
				return true;
			}
		}
		
	}
	function getUserSetting($user_id,$setting_name=null)
	{
		
		$this->loadModel('UserSetting');
		$this->UserSetting->recursive=-1;
		
		if($setting_name!=null)
		{
			return $this->UserSetting->field($setting_name.'_notification',array('user_id'=>$user_id))>0;
		}
		else
		{
			return array_pop($this->UserSetting->findByUserId($user_id));
		}
	}
	function facebook_post($post=array())
	{
		if($this->Auth->user('facebook_access_token')!=null)
		{
			$post['access_token']=$this->Auth->user('facebook_access_token');
			if($this->facebook==null)
			{
				$this->facebook = new Facebook(array("appId"=>Configure::read("facebook.api_key"),"secret"=> Configure::read("facebook.app_secrete")));
			}
			//and make the request
			
			$res = $this->facebook ->api('/me/feed', 'POST', $post);
			
		}
		
	}
	function twitter_post($post,$image_path=false)
	{
		$this->log("For Twitter ::".print_r($post,true),"Twitter_Post_AppController");
		if($this->Auth->user('twitter_access_token')!=null)
		{
			
			if($image_path==false)
			{
				App::import('Vendor', 'twitterOauth/twitteroauth');
				$tweet = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"), $this->Auth->user('twitter_access_token'), $this->Auth->user('twitter_access_token_secret'));
				$tweet->post('statuses/update', array('status' => $post));
			}
			else
			{
				App::import('Vendor', 'tmhOAuth/tmhOAuth');
				App::import('Vendor', 'tmhOAuth/tmhUtilities');
				
				//Configure::read("twitter.consumer_secret"), $this->Auth->user('twitter_access_token'), $this->Auth->user('twitter_access_token_secret')
				$tmhOAuth = new tmhOAuth(array(
						'consumer_key'    => Configure::read("twitter.consumer_key"),
						'consumer_secret' => Configure::read("twitter.consumer_secret"),
						'user_token'      => $this->Auth->user('twitter_access_token'),
						'user_secret'     => $this->Auth->user('twitter_access_token_secret')
				));
				$code = $tmhOAuth->request(
					'POST','https://upload.twitter.com/1.1/statuses/update_with_media.json',
					array(
						'media[]'  => "@{$image_path}",
						'status'   => $post
					),
					true,true
				);
			}
		}
	}
	function makeTinyUrl($url)
	{
		/*This code for create tiny url from tiny.cc
		if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['HTTP_HOST']=="192.168.1.161")
		{
			return $url;
		}
		$response=file_get_contents("http://tiny.cc/?c=rest_api&m=shorten&version=2.0.3&format=json&longUrl=".urlencode($url)."&login=".Configure::read('tiny_url.login')."&apiKey=".Configure::read('tiny_url.api_key'));
		if($response!="")
		{
			$res=json_decode($response,true);
			
			if($res['errorCode']==0)
			{
				return $res['results']['short_url'];
			}
			return $url;
		}
		else 
		{
			return $url;
		}*/
		if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['HTTP_HOST']=="192.168.1.161")
		{
			return $url;
		}
		$postData = array('longUrl' => $url);
		$jsonData = json_encode($postData);
		$curlObj = curl_init();
		curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key='.Configure::read('shorten_url.api_key'));
		curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlObj, CURLOPT_HEADER, 0);
		curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
		curl_setopt($curlObj, CURLOPT_POST, 1);
		curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt($curlObj, CURLOPT_REFERER, "http://www.everdrobe.com/");
		$response = curl_exec($curlObj);
		
		// Change the response json string to object
		$json = json_decode($response);
		curl_close($curlObj);
		return $json->id;
	}
	
	
	
	function send_notification($device_token,$message,$params=array())
	{
		if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['HTTP_HOST']=="192.168.1.161")
		{
			return true;
		}
		$this->log("Device Token :: ".print_r($device_token,true),'push_notification');
		$ctx = stream_context_create();
		$cert_path= APP.DS.Configure::read('push_notification.pem_cert');
		stream_context_set_option($ctx, 'ssl', 'local_cert', $cert_path);
		stream_context_set_option($ctx, 'ssl', 'passphrase', Configure::read('push_notification.keyphrase'));
	
		// Create the payload body
		$body['aps'] = array(
				'alert' => $message,
				'badge' => 1,
				'sound' => 'default'
		);
		if(count($params)>0)
		{
			$body['aps']=array_merge($body['aps'],$params);
		}
		// Encode the payload as JSON
		$payload = json_encode($body);
		// Open a connection to the APNS server
		$apns = stream_socket_client(
				Configure::read('push_notification.socket'), $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	
		// if not connected
		if (!$apns)
		{
			$this->log("Failed to connect: $err $errstr" . PHP_EOL);
			return false;
		}
	
		if(is_array($device_token))
		{
			foreach($device_token as $token)
			{
				$imsg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;
				// Send it to the server
				$this->log("Token::".$token,'push_notification');
				$this->log("Pack Message :: ".$imsg,'push_notification');
				$res = fwrite($apns, $imsg, strlen($imsg));
			}
		}
		else
		{
			$imsg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;
			
			$this->log("Token::".$token,'push_notification');
			$this->log("Pack Message :: ".$imsg,'push_notification');
			// Send it to the server
			$res = fwrite($apns, $imsg, strlen($imsg));
		}
		if (!$res) $return=false;
		else $return= true;
		fclose($apns);
		return $return;
	}
	
	
	function send_notification_bulk($data,$queue=true)
	{
		if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['HTTP_HOST']=="192.168.1.161")
		{
			return true;
		}
		
		$ctx = stream_context_create();
		$cert_path= APP.DS.Configure::read('push_notification.pem_cert');
		stream_context_set_option($ctx, 'ssl', 'local_cert', $cert_path);
		stream_context_set_option($ctx, 'ssl', 'passphrase', Configure::read('push_notification.keyphrase'));
	
		
		// Open a connection to the APNS server
		$apns = stream_socket_client(
				Configure::read('push_notification.socket'), $err,
				$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	
		
		$this->log("Sadik Notification :: ".print_r($apns,true),'push_notification');
		// if not connected
		if (!$apns)
		{
			$this->log("Failed to connect: $err $errstr" . PHP_EOL);
			return false;
		}
	
		
		$return=array();
		foreach($data as $notification)
		{
			$this->log("Test Nitification :: ".print_r($notification,true),'push_notification');
			
			// Create the payload body
			$body['aps'] = array(
					'alert' => $notification['message'],
					'badge' => $notification['badge'],
					'sound' => 'default'
			);
			if(isset($notification['params']) && count($notification['params'])>0)
			{
				$body['aps']=array_merge($body['aps'],$notification['params']);
			}
			// Encode the payload as JSON
			$payload = json_encode($body);
				
				
			$imsg = chr(0) . pack('n', 32) . pack('H*', $notification['token']) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$res = fwrite($apns, $imsg, strlen($imsg));
			if($queue==true)
			{
				$return[$notification['queue_id']]=$res;
			}
//			echo $res."<br>";
		}
		fclose($apns);
		if(count($return)>0)
		{
			return $return;
		}
		if($queue!=true)
		{
			if (!$res) $return=false;
			else $return= true;
		}
		
		return $return;
	}
	
	
	public function send_notification_bulk_android($data,$queue=true){
		$response = array();
		if (count($data['tokens'])>0 && isset($data['message']))
		{
			App::import('Vendor', 'GCM');
			$gcm = new GCM(Configure::read('push_notification.google_api_key'));
			$message = array("title" => $message);
			if($result = $gcm->send_notification($data['tokens'], $data['message']))
			{
				$result=json_decode($result,true);
				if($queue=true)
				{
					return $result;
				}
				else
				{
					return $result['success']>0;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function send_notification_user($message,$user_id,$params=array())
	{
		if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['HTTP_HOST']=="192.168.1.161") return true;
		
		
		$this->loadModel('UserSetting');
		$this->UserSetting->recursive=-1;
		$user=$this->UserSetting->findByUserId($user_id);
		$this->log("UserSetting ::".print_r($user,true),"push_notification");
		if($user["UserSetting"]["apple_device_token"]!="" && strlen($user["UserSetting"]["apple_device_token"])>30)
		{
			$notification=array();
			if($user["UserSetting"]["device_type"]=="ios")
			{
				$notification[]=array(
						"message"=>$message,
						"token"=>$user["UserSetting"]["apple_device_token"],
						"badge"=> $user["UserSetting"]['badge']+1,
						"params"=>$params
				);
				$this->log("Notification ".print_r($notification,true),'push_notification');
				$this->log("Function arguments".print_r(func_get_args(),true),'push_notification');
				$this->send_notification_bulk($notification);
				$this->UserSetting->id=$user["UserSetting"]['id'];
				$this->UserSetting->saveField('badge', $user["UserSetting"]['badge']+1);
				return true;
			}
			else if($user["UserSetting"]["device_type"]=="android")
			{
				$this->log("usersetting ::".print_r($user,true),"push_notification");
				if(is_array($params) && count($params)>0)
				{
					$messageNew=array_merge(array("message"=>$message),$params);
				}
				else
				{
					$messageNew=array("message"=>$message);
				}
				
				$notification=array(
						"tokens"=>array($user["UserSetting"]["apple_device_token"]),
						"message"=>$messageNew
				);
				
				$this->send_notification_bulk_android($notification);
				return true;
			}
		}
		return false;
	}
	
	
	
	function shopify($path,$json,$method="GET",$params="")
	{
		$this->log(print_r(func_get_args(),true),"shopify");	
		$conf=Configure::read('shopify');
		$url = 'https://' . $conf['api_key'] . ':' . $conf['secret'] . '@' . $conf['host_name'] . '/admin'.$path.'.'.$conf['format'];
		if($params!="") $url .= "?".$params;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$http_header=array();
		
		if($method=="GET")
		{
			curl_setopt($ch, CURLOPT_HTTPGET, 1);
		}
		else if($method=="POST")
		{
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			$http_header[]='Accept: application/'.$conf['format'];
			$http_header[]='Content-Type: application/'.$conf['format'];
		}
		else if($method=="PUT")
		{
			// For Some limitations/bugs of shopify we need to use XML content for pass to PUT method
			curl_setopt($ch, CURLOPT_POST, 1);
			
			$xml=$this->_shopifyArrayToXML(json_decode($json,true));
			$http_header[]='Content-Length: ' . strlen($xml);
			$http_header[]='Accept: application/xml';
			$http_header[]='Content-Type: application/xml';
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			$this->log(htmlspecialchars($xml),"shopify");
			curl_setopt($ch, CURLOPT_POSTFIELDS,$xml);
			
		}
		else if($method=="DELETE")
		{
			curl_setopt($ch,  CURLOPT_CUSTOMREQUEST, "DELETE");
		}
		if(count($http_header)>0) 
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, $http_header);
		}
		if(ereg("^(https)",$url)) 
		{
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
		}
		$response = curl_exec($ch);
		$this->log($response,"shopify");
		curl_close($ch);
		return json_decode($response,true);
	}
	function _shopifyArrayToXML($array, $xml = '', $specialCaseTag = '')
	{
		if ($xml == "") $xml = '<?xml version="1.0" encoding="UTF-8"?>';
		$specialCases = $this->_shopifySpecialCases();
		foreach($array as $k => $v){
			if (is_numeric($k) && !empty($specialCaseTag)) $k = $specialCaseTag;
			if (is_array($v)){
				if (array_key_exists($k, $specialCases)){
					$xml .= '<' . $k . ' type="array">';
					$xml = $this->_shopifyArrayToXML($v, $xml, $specialCases[$k]);
				}else{
					$xml .= '<' . $k . '>';
					$xml = $this->_shopifyArrayToXML($v, $xml);
				}
				$xml .= '</' . $k . '>';
			}else{
				$xml .= '<' . $k . '>' . htmlspecialchars($v) . '</' . $k . '>';
			}
		}
		return $xml;
	}
	function _shopifySpecialCases($key = ''){
		return array(
				'variants' => 'variant',
				'images' => 'image',
				'options' => 'option',
				'line-items' => 'line-item',
				'collects' =>'collect'
		);
	}
	/*
	 * Added by haresh Vidja
	 * for convert 1950 => 1.9K
	 */
	public function convertToReadeable($number)
	{
		$return="";
		if($number >= 1000000)
		{
			$return=sprintf("%0.1fM",($number/1000000));
		}
		else if($number >= 1000)
		{
			$return=sprintf("%0.1fK",($number/1000));
		}
		else $return=$number;
		return $return;
	}
	
	/*
	 * Function for store ebx transaction logs
	 * */
	public function ebx_transactions_add($trans_type,$user_id,$point,$current_balance,$rel_id,$description)
	{
		if(in_array($trans_type, array('drobe','highest_rated_drobe', 'reward', 'admin','redeem')))
		{
			$this->loadModel("EbxTransaction");
			$ebxData=array();
			$ebxData["EbxTransaction"]=array();
			if($trans_type=='drobe' || $trans_type=='highest_rated_drobe')
			{
				$ebxData["EbxTransaction"]['drobe_id']=$rel_id;
			}
			elseif($trans_type=='reward')
			{
				$ebxData["EbxTransaction"]['rate_id']=$rel_id;
			}
			$ebxData["EbxTransaction"]['description']=$description;
			$ebxData["EbxTransaction"]['user_id']=$user_id;
			$ebxData["EbxTransaction"]['points']=$point;
			$ebxData["EbxTransaction"]['current_balance']=$current_balance;
			$ebxData["EbxTransaction"]['transaction_type']=$trans_type;
			$this->EbxTransaction->save($ebxData);
		}
	}
	
	/*public function drobe_season_order_query()
	{
		//"SELECT * FROM `everdrobe_drobes`
		// ORDER BY `season` = 'fall' DESC, `season` = 'summer' DESC, `season` = 'spring' DESC, `season` = 'winter' DESC,
		//YEAR(`uploaded_on`) DESC, `rate_index` DESC, `views` DESC";
	
		if(in_array(date('n'),array(6,7,8))) {
			$seasonOrder="Drobe.season = 'summer' DESC, Drobe.season = 'spring' DESC, Drobe.season = 'winter' DESC, Drobe.season = 'fall' DESC, YEAR(Drobe.uploaded_on) DESC,";
		}
		if(in_array(date('n'),array(9,10,11))) {
			$seasonOrder="Drobe.season = 'fall' DESC, Drobe.season = 'summer' DESC, Drobe.season = 'spring' DESC, Drobe.season = 'winter' DESC, YEAR(Drobe.uploaded_on) DESC,";
		}
		if(in_array(date('n'),array(12,1,2))) {
			$seasonOrder="Drobe.season = 'winter' DESC, Drobe.season = 'fall' DESC, Drobe.season = 'summer' DESC, Drobe.season = 'spring' DESC, YEAR(Drobe.uploaded_on) DESC,";
		}
		if(in_array(date('n'),array(3,4,5))) {
			$seasonOrder="Drobe.season = 'spring' DESC, Drobe.season = 'winter' DESC, Drobe.season = 'fall' DESC, Drobe.season = 'summer' DESC, YEAR(Drobe.uploaded_on) DESC,";
		}
		return $seasonOrder;
	}*/
	
	
	
	
	
	public function drobe_season_order_query()
	{
		if(in_array(date('n'),array(6,7,8))) {
			$seasonOrder="Drobe.season = 'summer' DESC, Drobe.season = 'spring' DESC, Drobe.season = 'winter' DESC, Drobe.season = 'fall' DESC, Drobe.season_year DESC,";
		}
		if(in_array(date('n'),array(9,10,11))) {
			$seasonOrder="Drobe.season = 'fall' DESC, Drobe.season = 'summer' DESC, Drobe.season = 'spring' DESC, Drobe.season = 'winter' DESC, Drobe.season_year DESC,";
		}
		if(in_array(date('n'),array(12,1,2))) {
			$seasonOrder="Drobe.season = 'winter' DESC, Drobe.season = 'fall' DESC, Drobe.season = 'summer' DESC, Drobe.season = 'spring' DESC, Drobe.season_year DESC,";
		}
		if(in_array(date('n'),array(3,4,5))) {
			$seasonOrder="Drobe.season = 'spring' DESC, Drobe.season = 'winter' DESC, Drobe.season = 'fall' DESC, Drobe.season = 'summer' DESC, Drobe.season_year DESC,";
		}
	
		return $seasonOrder;
	}
	
	public function under_maintenance_all($type="ios")
	{
		$response = array();
		$data = array(
				'app_version'=>$type=="android"?Configure::read('android_app_version'):Configure::read('ios_app_version'),
				'under_maintenance'=> Configure::read('under_maintenance')? 'yes': 'no'
		);
		
		$response['type']="success";
		$response['data']=$data;
		echo json_encode(array('response'=>$response));
		exit();
		/*$this->set('response',$response);
		$this->set('_serialize',array('response'));*/
	}
}
