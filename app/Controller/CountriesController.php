<?php 
class CountriesController extends AppController
{
	//var $scaffold;
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('get_state_list');
	}
	function admin_index()
	{
		//$countries=$this->Country->find('all');
		//$this->set('countries',$countries);
		$this->paginate = array(
					'limit' => 15
		);
		$data = $this->paginate('Country');
		$this->set(compact('data'));
	}
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->Country->save($this->data))
			{
				$this->Country->_updateCountryCache();
				$this->Session->setFlash("New Country added successfully.",'default',array('class'=>"success"));
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Country->id=$id;
		if(!empty($this->data))
		{
			//$this->Country->validate['country_name']['isUnique']['on']='update';
			$this->Country->validate['short_name']['isUnique']['on']='update';
			if($this->Country->save($this->data))
			{
				$this->Country->_updateCountryCache();
				$this->Session->setFlash("Country name changed successfully",'default',array('class'=>"success"));
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Country->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if($this->Country->delete($id))
			{
				$this->Country->_updateCountryCache();
				$this->Session->setFlash("Selected country deleted successfully",'default',array('class'=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in delete country");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	function get_state_list($short_name=null)
	{
		if($this->RequestHandler->isAjax())
		{
			if($short_name!="")
			{
				$country_detail=$this->Country->find('first',array('conditions'=>array('short_name'=>$short_name)));
				$this->set('provinces',$country_detail['Province']);
			}
		}
	}
	
	function getlist()
	{
		$countries=Cache::read('country_cache');
		if($countries==null)
		{
			$this->Country->_updateCountryCache();
			$countries=Cache::read('country_cache');
		}
		if(count($countries)>0)
		{
			$data=array();
			foreach($countries as $country)
			{
				$data[]=$country['Country'];
			}
			$response['type']="success";
			$response['countries']=$data;
		}
		else
		{
			$response['type']="error";
			$response['message']="Country not found";
		}
		
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>