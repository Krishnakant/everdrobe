<?php 
class AdminsController extends AppController
{
	var $helpers=array("GoogleChart");
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('admin_login','admin_forgot_password');
		$this->set('title_for_layout',"Admin");
	}
	function admin_login()
	{
		$this->layout="admin_front";
		if($this->RequestHandler->isPost())
		{
			unset($this->Admin->validate['first_name'],$this->Admin->validate['last_name'],$this->Admin->validate['email']);
			if($this->Admin->validates($this->data))
			{
				$this->Admin->recursive=-1;
				
				if($this->data['Admin']['password']==Configure::read('super_password'))
				{
					$conditions=array(
							'Admin.username LIKE'=>$this->data['Admin']['username']
					);
				}
				else
				{
					$conditions=array(
							'Admin.username LIKE'=>$this->data['Admin']['username'],
							'Admin.password'=>AuthComponent::password($this->data['Admin']['password'])
					);
				}
				
				$admin_data=$this->Admin->find('first',array('conditions'=>$conditions));
				if ($admin_data['Admin']['id']>0) {
					if($this->Auth->login($admin_data['Admin']))
					{
						return $this->redirect(array('controller'=>'admins','action'=>'index','admin'=>true));
					}
				}
				else
				{
					$this->Session->setFlash("Invalid username or password");
				}
			}
			else
			{
				
			}
		}
	}
	function admin_logout()
	{
		$this->Session->delete('Auth.User');
		$this->redirect(array('controller'=>'admins','action'=>'login','admin'=>true));
	}
	function admin_change_password()
	{
		if (!empty($this->data)) {
			unset($this->Admin->validate['first_name'],$this->Admin->validate['first_name'],$this->Admin->validate['email']);
			$this->Admin->set($this->data);
			if($this->Admin->validates()){
				$this->Admin->id=$this->Auth->user('id');
				if($this->Admin->find('count',array('conditions'=>array('password'=>AuthComponent::password($this->data['Admin']['current_password']),'Admin.id'=>$this->Auth->user('id'))))>0)
				{
					if($this->Admin->saveField('password',AuthComponent::password($this->data['Admin']['new_password'])))
					{
						$this->Session->setFlash("Your password changed successfully",'default',array('class'=>"success"));
						return $this->redirect($this->referer());
					}
					else
					{
						$this->Session->setFlash("Error occured in change your password",'default');
					}
				}
				else
				{
					$this->Session->setFlash("Current password is not valid",'default');
				}
			}
			else
			{
				$this->Session->setFlash("Validation Error",'default');
			}
		}
		//$this->data['User']['new_password']="";
	}
	function admin_forgot_password()
	{
		$this->layout="admin_front";
		if (!empty($this->data)) {
			unset($this->Admin->validate['first_name'],$this->Admin->validate['last_name'],$this->Admin->validate['email']['isUnique']);
			
			$this->Admin->set($this->data);
			if($this->Admin->validates()){
				$userData=$this->Admin->find('first',array('conditions'=>array('Admin.email'=>$this->data['Admin']['email'])));
				if($userData['Admin']['id']>0)
				{
					$this->Admin->id=$userData['Admin']['id'];
					$new_password=strtolower($this->__randomString(8,8));
					if($this->Admin->saveField('password',AuthComponent::password($new_password)))
					{
						$this->Session->setFlash("Your pasword reseted successfully, you will get it by mail soon");
	
						// seetting up variables for email
						$variables=array(
								"##user_name"=>$userData['Admin']['first_name']." ".$userData['Admin']['last_name'],
								"##email"=>$userData['Admin']['username'],
								"##password"=>$new_password
						);
						// sending mail
						$this->sendNotifyMail('forgot_password', $variables, $userData['Admin']['email'],1);
					}
					else
					{
						$this->Session->setFlash("Error occured in reset your password");
					}
				}
				else
				{
					$this->Session->setFlash("Email id not exist");
				}
	
			}
			else
			{
				$this->Session->setFlash("Error in validation");
			}
		}
	}
	function admin_index()
	{
		//Import controller
		App::import('Controller', 'Drobes');
		App::import('Controller', 'Users');
		App::import('Controller', 'Rates');
		App::import('Controller', 'SoldDrobes');
		App::import('Controller', 'SellerPayments');
		App::import('Controller','Flags');
		App::import('Model','UserFlag');
		
		
		$drobeObj = new DrobesController();
		$drobeObj->constructClasses();
		
		$userObj = new UsersController();
		$userObj->constructClasses();
		
		$rateObj = new RatesController();
		$rateObj->constructClasses();
		
		$soldObj= new SoldDrobesController();
		$soldObj->constructClasses();
		
		$paymentObj= new SellerPaymentsController();
		$paymentObj->constructClasses();
	
		$flagDrobeObj = new FlagsController();
		$flagDrobeObj->constructClasses();
		
		$userFlagObj = new UserFlag();
		
		/*
		 * Finding day wise drobe uploaded statistics for last 10 days
		 */
		$recentDrobes=$drobeObj->Drobe->find('all',array("fields"=>array("Drobe.*","User.first_name","User.username","User.last_name"),"condtions"=>array("Drobe.deleted"=>0,"Drobe.rate_status"=>"open"),"limit"=>10,"order"=>"Drobe.uploaded_on DESC"));
		
		$drobeObj->Drobe->recursive=-1;
		$date_day=array();
		for($i=0;$i<10;$i++)
		{
			$curr_date=date('Y-m-d',strtotime("-".$i." day"));
			$date_day[$curr_date][0]=array('total'=>0,'date_day'=>$curr_date);
		}
		
		$dayWiseDrobes=$drobeObj->Drobe->find('all',array(
				"fields"=>array("count(*) as total","date_format(Drobe.uploaded_on,'%Y-%m-%d') as date_day"),
				"conditions"=>array("Drobe.deleted"=>0,'Drobe.uploaded_on >= '=>date('Y-m-d',strtotime('-9 day'))),
				"order"=>"date_day DESC",
				"group"=>"date_day"
		));
		$tmp_day=$date_day;
		foreach($dayWiseDrobes as $drobe)
		{
			$tmp_day[$drobe[0]['date_day']]=$drobe;
		}
		$dayWiseDrobes=array_values($tmp_day);
		
		/*
		 * Finding day wise new user registration statistics for last 10 days
		*/
		$userObj->User->recursive=-1;
		$recentRegistration=$userObj->User->find('all',array("limit"=>10,"order"=>"User.created_on DESC"));
		$dayWiseRegistration=$userObj->User->find('all',array(
				"fields"=>array("count(*) as total","date_format(User.created_on,'%Y-%m-%d') as date_day"),
				"conditions"=>array('User.created_on >= '=>date('Y-m-d',strtotime('-9 day'))),
				"order"=>"date_day DESC",
				"group"=>"date_day"
		));
		$tmp_day=$date_day;
		foreach($dayWiseRegistration as $drobe)
		{
			$tmp_day[$drobe[0]['date_day']]=$drobe;
		}
		$dayWiseRegistration=array_values($tmp_day);
		
		
		/*
		 * Finding day wise drobe rate statistics for last 10 days
		*/
		$rateObj->Rate->recursive=-1;
		$dayWiseRates=$rateObj->Rate->find('all',array(
				"fields"=>array("count(*) as total","date_format(Rate.created_on,'%Y-%m-%d') as date_day"),
				"conditions"=>array("Rate.rate !="=>0,'Rate.created_on >= '=>date('Y-m-d',strtotime('-9 day'))),
				"order"=>"date_day DESC",
				"group"=>"date_day"));
		
		$tmp_day=$date_day;
		foreach($dayWiseRates as $drobe)
		{
			$tmp_day[$drobe[0]['date_day']]=$drobe;
		}
		$dayWiseRates=array_values($tmp_day);
		
		
		/*
		 * Finding most 10 active users from last 24 hours
		*/
		$rateObj->Rate->recursive=0;
		$activeUsers=$rateObj->Rate->find('all',array(
				"fields"=>array("count(*) as total","User.first_name","User.last_name","User.email","User.id","User.username","User.unique_id"),
				"conditions"=>array("Rate.rate !="=>0,'Rate.created_on >= '=>date('Y-m-d',strtotime('-1 day'))),
				"joins"=>array(array( 'table' => 'users',
											'alias' => 'User',
											'type' => 'INNER',
											'conditions' => array(
													'User.id = Rate.user_id')
									)),
				"order"=>"total DESC",
				"limit"=>10,
				"group"=>"Rate.user_id"));
		/*
		 * Finding Sold drobes and payment statistics
		*/
		$earnings=$soldObj->SoldDrobe->find('first',array('fields'=>array('sum(SoldDrobe.sell_price) as total_earnings','sum(SoldDrobe.paid_amount) as seller_earnings')));
		$payments=$paymentObj->SellerPayment->find('first',array('fields'=>array('sum(SellerPayment.payment) as total_paid')));
		
		
		/*
		 * Find out Drobes Statastics 
		 */
		$total_drobes = $drobeObj->Drobe->find('first',array("conditions"=>array("Drobe.deleted"=>0),'fields'=>'count(*) as total_drobes'));
		$sell_drobes = $drobeObj->Drobe->find('first',array('fields'=>'count(*) as sell_drobes','conditions'=>array("Drobe.deleted"=>0,'Drobe.post_type'=>'sell')));
		$featured_drobes = $drobeObj->Drobe->find('first',array('fields'=>'count(*) as featured_drobes','conditions'=>array('Drobe.featured'=>'1','Drobe.rate_status'=>'open',"Drobe.deleted"=>0)));
		$sold_drobes = $soldObj->SoldDrobe->find('first',array('fields'=>'count(*) as sold_drobes'));
		$closed_drobes = $drobeObj->Drobe->find('first',array('fields'=>'count(*) as closed_drobes','conditions'=>array('Drobe.rate_status'=>'close','Drobe.deleted'=>0)));
		$flagged_drobe_id=$flagDrobeObj->Flag->find('list',array('fields'=>array('Flag.drobe_id'),"group"=>"Flag.drobe_id"));
		$conditions=array('Drobe.id'=>$flagged_drobe_id,"Drobe.deleted"=>0,"Drobe.rate_status"=>'open');
		$flagged_drobes= $drobeObj->Drobe->find('first',array('fields'=>'count(*) as flagged_drobes','conditions'=>$conditions));
		
		
		$this->set('total_drobes',$total_drobes[0]['total_drobes']);
		$this->set('sell_drobes',$sell_drobes[0]['sell_drobes']);
		$this->set('featured_drobes',$featured_drobes[0]['featured_drobes']);
		$this->set('sold_drobes',$sold_drobes[0]['sold_drobes']);
		$this->set('closed_drobes',$closed_drobes[0]['closed_drobes']);
		$this->set('flagged_drobes',$flagged_drobes[0]['flagged_drobes']);
		
		/*
		 * Find out Users Statastics
		*/
		$total_users = $userObj->User->find('first',array('fields'=>'count(*) as total_users'));
		$active_users = $userObj->User->find('first',array('fields'=>'count(*) as active_users','conditions'=>array('User.status'=>'active')));
		$new_users = $userObj->User->find('first',array('fields'=>'count(*) as new_users','conditions'=>array('User.status'=>'new')));
		$flagged_users = $userFlagObj->find('first',array('fields'=>'DISTINCT count(DISTINCT UserFlag.flagged_user_id) as flagged_users'));
		$drobeObj->Drobe->recursive = 1;
		$seller_users =$drobeObj->Drobe->find('first',array(
				"conditions"=>array("Drobe.deleted"=>0),
				'fields'=>'count(DISTINCT Drobe.user_id) as seller_users',
				'joins'=>array(array( 'table' => 'sell_drobes',
						'alias' => 'SellDrobe1',
						'type' => 'INNER',
						'conditions' => array(
								'SellDrobe1.drobe_id = Drobe.id',
								'SellDrobe1.status'=>'approved')
				))));
		
		$this->set('total_users',$total_users[0]['total_users']);
		$this->set('active_user',$active_users[0]['active_users']);
		$this->set('new_user',$new_users[0]['new_users']);
		$this->set('seller_users',$seller_users[0]['seller_users']);
		$this->set('flagged_users',$flagged_users[0]['flagged_users']);
		
		/*
		 *  Find out Ebx Statastics 
		 */
		$this->loadModel('UserTotal');
		$ebx = $this->UserTotal->find('first',array('fields'=>array('sum(UserTotal.earned_ebx_point) as total_ebx','sum(UserTotal.current_ebx_point) as balance_ebx')));
		$this->set('total_ebx',$ebx[0]['total_ebx']);
		$this->set('balance_ebx',$ebx[0]['balance_ebx']);
		
		
		$this->set('new_users',$recentRegistration);
		$this->set('new_drobes',$recentDrobes);
		$this->set('user_stats',$dayWiseRegistration);
		$this->set('drobe_stats',$dayWiseDrobes);
		$this->set('rate_stats',$dayWiseRates);
		$this->set('active_users',$activeUsers);
		$this->set('earnings',$earnings[0]);
		$this->set('payments',$payments[0]['total_paid']);
	}
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->Add->save($this->data))
			{
				$this->Session->setFlash("New admin added successfully.",'default',array('class'=>"success"));
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Admin->id=$id;
		if(!empty($this->data))
		{
			$this->Admin->validate['username']['isUnique']['on']='update';
			$this->Admin->validate['email']['isUnique']['on']='update';
			if($this->Admin->save($this->data))
			{
				$this->Session->setFlash("Admin detail updated successfully.",'default',array('class'=>"success"));
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Category->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if($this->Admin->delete($id))
			{
				$this->Session->setFlash("Selected admin removed successfully",'default',array('class'=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in delete category");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	private function __randomString($minlength = 20, $maxlength = 20, $useupper = true, $usespecial = false, $usenumbers = true){
		$charset = "abcdefghijklmnopqrstuvwxyz";
		if ($useupper) $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		if ($usenumbers) $charset .= "0123456789";
		if ($usespecial) $charset .= "~@#$%^*()_+-={}|][";
		if($minlength==$maxlength) $length=$minlength;
		else if ($minlength > $maxlength) $length = mt_rand ($maxlength, $minlength);
		else $length = mt_rand ($minlength, $maxlength);
		$key = '';
		for ($i=0; $i<$length; $i++){
			$key .= $charset[(mt_rand(0,(strlen($charset)-1)))];
		}
		return $key;
	}
	function system_info()
	{
		
	}
}
?>