<?php 
class UploadsController extends AppController
{
	var $components=array('FileUpload');
	function beforeFilter()
	{
		parent::beforeFilter();
		Configure::write('debug',0);
		$this->Auth->allow();
		$this->FileUpload->fileModel=null;
		$this->FileUpload->randomFileName=true;
		$this->FileUpload->fields = array('name'=> 'name', 'type' => 'type', 'size' => 'size');
		$this->FileUpload->allowedTypes = "All";
		$this->FileUpload->createThumbImage=true;
		if($this->params->params['pass'][0]=="profile")
		{
			$this->FileUpload->uploadDir = Configure::read('profile.upload_dir');
			$this->FileUpload->thumbWidth=Configure::read('profile.thumb.width');
			$this->FileUpload->thumbHeight=Configure::read('profile.thumb.height');
			$this->FileUpload->thumbFolder = Configure::read('profile.thumb.upload_dir');
		}
		else if($this->params->params['pass'][0]=="sell_drobes")
		{
			$this->FileUpload->uploadDir = Configure::read('sell_drobe.upload_dir');
			$this->FileUpload->thumbWidth=Configure::read('sell_drobe.thumb.width');
			$this->FileUpload->thumbHeight=Configure::read('sell_drobe.thumb.height');
			$this->FileUpload->thumbFolder = Configure::read('sell_drobe.thumb.upload_dir');
			$this->FileUpload->resizeImage=false;
		} 
			
		else
		{
			$this->FileUpload->uploadDir = Configure::read('drobe.upload_dir');
			
			$this->FileUpload->thumbWidth=Configure::read('drobe.thumb.width');
			$this->FileUpload->thumbHeight=Configure::read('drobe.thumb.height');
			$this->FileUpload->thumbFolder = Configure::read('drobe.thumb.upload_dir');
			
			$this->FileUpload->createIPhoneImage=true;
			
			$this->FileUpload->iPhoneWidth=Configure::read('drobe.iphone.width');
			$this->FileUpload->iPhoneHeight=Configure::read('drobe.iphone.height');
			$this->FileUpload->iPhoneFolder = Configure::read('drobe.iphone.upload_dir');
			
			$this->FileUpload->resizeImage=true;
			$this->FileUpload->cropeImage=true;
			$this->FileUpload->newWidth=Configure::read('drobe.width');
			$this->FileUpload->newHeight=Configure::read('drobe.height');
			
			if($this->params->params['pass'][0]=="original")
			{
				$this->FileUpload->createIPhoneImage=false;
				$this->FileUpload->createThumbImage=false;
				$this->FileUpload->cropeImage=false;
				$this->FileUpload->resizeImage=false;
				$this->FileUpload->setMinWidth=320;
				$this->FileUpload->setMinHeight=320;
			}
			
		}
		$this->FileUpload->fileVar='qqfile';
	}
	function file($type=null){
		$this->layout="ajax";
		$response=array();
		if($type!=null){
			
			if($this->FileUpload->success){
				$response['type']="success";
				if($this->data['type']=="profile")
				{
					$validSize=$this->_validateProfileImageSize();
					if($validSize!="")
					{
						$this->FileUpload->removeFile($this->FileUpload->finalFile);
						$response['type']="error";
						$response['message']=$validSize;
					}
					else
					{
						$response['file']=$this->FileUpload->finalFile;
						$response['type']=="success";
						$response['message']=="Image uploaded successfully";
					}
				}
				else
				{
					$response['file']=$this->FileUpload->finalFile;
					$response['size']=$this->FileUpload->imageInfo;
					$response['type']=="success";
					$response['message']=="Image uploaded successfully";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']=$this->FileUpload->error[0];
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="data not found";
				
		}
		$this->set('response', $response);
	}
	function admin_file($type=null){
		$this->layout="ajax";
		$response=array();
		if($type!=null){
				
			if($this->FileUpload->success){
				$response['type']="success";
				if($this->data['type']=="profile")
				{
					$validSize=$this->_validateProfileImageSize();
					if($validSize!="")
					{
						$this->FileUpload->removeFile($this->FileUpload->finalFile);
						$response['type']="error";
						$response['message']=$validSize;
					}
					else
					{
						$response['file']=$this->FileUpload->finalFile;
						$response['type']=="success";
						$response['message']=="Image uploaded successfully";
					}
				}
				else
				{
					$response['file']=$this->FileUpload->finalFile;
					$response['size']=$this->FileUpload->imageInfo;
					$response['type']=="success";
					$response['message']=="Image uploaded successfully";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']=$this->FileUpload->error[0];
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="data not found";
	
		}
		echo json_encode($response);
		exit;
	}
	
	function image_from_url()
	{
		$response=array();
		if(isset($this->data['url']))
		{
			$url=$this->data['url'];

			$oldName=$name = basename($url);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$data = curl_exec($ch);
			
			$contentType=curl_getinfo($ch,CURLINFO_CONTENT_TYPE);
			$imageTypes=array('image/jpg'=>"jpg",'image/jpeg'=>'jpg','image/png'=>'png','image/gif'=>'gif');

			if(in_array($contentType, array_keys($imageTypes)))
			{
				$name = uniqid().".".$imageTypes[$contentType];
				$up_dir = WWW_ROOT . $this->FileUpload->uploadDir;
				$target_path = $up_dir . DS . $name;
				$upload = file_put_contents($target_path,$data);
				
				if($upload)
				{
					$this->FileUpload->uploadDetected=true;
					$this->FileUpload->detected_as_row_data=true;
					$this->FileUpload->row_file_name=$oldName;
					
					//Final File Name
					$this->FileUpload->finalFile = basename($target_path);
					if($this->FileUpload->resizeImage || $this->FileUpload->createThumbImage || $this->FileUpload->setMinWidth>0 || $this->FileUpload->setMinHeight>0)
					{
						$temp_path=TMP.array_pop(explode(DS,$target_path));
						$this->FileUpload->row_input_file_path=$temp_path;
						copy($target_path, $temp_path);
					}
					if($this->FileUpload->resizeImage)
					{
						if($this->FileUpload->cropeImage)
						{
							$this->FileUpload->_resizeAndCrop($temp_path,$this->FileUpload->newWidth,$this->FileUpload->newHeight,$target_path);
						}
						else $this->FileUpload->_resizeActualImage($temp_path,$this->FileUpload->newWidth,$this->FileUpload->newHeight,$target_path);
					}
					 
					// we need to use this in crop image area
					if($this->FileUpload->setMinWidth!=false && $this->FileUpload->setMinWidth>0 || $this->FileUpload->setMinHeight!=false && $this->FileUpload->setMinHeight>0)
					{
						$this->FileUpload->_setMinimumWidthHeight($temp_path,$target_path);
					
					}
					 
					if($this->FileUpload->createThumbImage)
					{
						$thumb_path= WWW_ROOT . $this->FileUpload->thumbFolder.DS.$this->FileUpload->finalFile;
						$this->FileUpload->_resizeAndCrop($temp_path,$this->FileUpload->thumbWidth,$this->FileUpload->thumbHeight,$thumb_path);
					}
					if($this->FileUpload->createIPhoneImage)
					{
						$iphone_path= WWW_ROOT . $this->FileUpload->iPhoneFolder.DS.$this->FileUpload->finalFile;
						$this->FileUpload->_resizeAndCrop($temp_path,$this->FileUpload->iPhoneWidth,$this->FileUpload->iPhoneHeight,$iphone_path);
					}
					if(isset($temp_path) && file_exists($temp_path))
					{
						unlink($temp_path);
					}
					 
					$model = $this->FileUpload->getModel();
					$save_data[$this->FileUpload->fields['name']] = $this->FileUpload->finalFile;
					$save_data[$this->FileUpload->fields['type']] = $this->FileUpload->uploadedFile['type'];
					$save_data[$this->FileUpload->fields['size']] = $this->FileUpload->uploadedFile['size'];
					if(!$model || $model->save($save_data))
					{
						$this->FileUpload->success = true;
						$this->FileUpload->imageInfo= $this->FileUpload->_getInfo($target_path);
						$response['type']="success";
						$response['file']=$name;
						$response['size']=$this->FileUpload->imageInfo;
					}
					else
					{
						$this->FileUpload->success = false;
						$response['type']="error";
						$response['message']="file creation failed";
					}
				}
			}
			else{
				$response['type']="error";
				$response['message']="Oops! Image is not accessible or invalid image type";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid parameters";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function _validateProfileImageSize()
	{
		$image_path=WWW_ROOT . $this->FileUpload->uploadDir .DS. $this->FileUpload->finalFile;
		$img_size=getimagesize($image_path);
		if($img_size[0] < Configure::read('profile.thumb.width'))
		{
			return "Image width must be at least ".Configure::read('profile.thumb.width')." pixels";
		}
		else if($img_size[1] < Configure::read('profile.thumb.height'))
		{
			return "Image height must be at least ".Configure::read('profile.thumb.height')." pixels";
		}
		return "";
	}
}
?>
