<?php 
class RequestForLabelsController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		if($this->Auth->user('type')!="admin")
		{
			$this->layout="sell_drobe";
		}
	}
	function admin_index()
	{
		$pending_request = $this->RequestForLabel->find('count',array('conditions'=>array('RequestForLabel.status'=>'pending')));
		$this->RequestForLabel->bindModel(array('belongsTo'=>array('User')));
		$conditions=array('RequestForLabel.status'=>"complete");
		$this->paginate = array(
				'fields'=>array('User.last_name','User.first_name','User.username','RequestForLabel.*'),
				'limit' => 10,
				'order' => "RequestForLabel.id DESC",
				'conditions'=>$conditions
		);
		$request_for_label = $this->paginate('RequestForLabel');
		$this->set('request_for_label',$request_for_label);
		$this->set('pending_request',$pending_request);
	}
	
	
	/**
	 * List of request shown at admin side which are pending for new label request. 
	 */
	function admin_pending_label_request()
	{
		$this->RequestForLabel->bindModel(array('belongsTo'=>array('User')));
		$conditions=array('RequestForLabel.status'=>"pending");
		$this->paginate = array(
				'fields'=>array('User.last_name','User.first_name','User.username','RequestForLabel.*'),
				'limit' => 10,
				'order' => "RequestForLabel.request_on DESC",
				'conditions'=>$conditions
		);
		$request_for_label = $this->paginate('RequestForLabel');
		$this->set(compact('request_for_label'));
	}
	
	
	/**
	 * @param unknown_type $request_id
	 * admin will approve the pending request for label.
	 */
	function admin_approve_request($request_id = null)
	{
		if($request_id!=null)
		{
			$this->RequestForLabel->updateAll(array('RequestForLabel.status'=>"'complete'"),array('RequestForLabel.id'=>$request_id));
			$this->Session->setFlash('Request for label approve successfully','default',array('class'=>'success'));
			$this->redirect(array('action'=>'pending_label_request'));
		}
		else
		{
			$this->Session->setFlash('Request for label not approved','default',array('class'=>'error'));
			$this->redirect(array('action'=>'pending_label_request'));
		}
	}
	
	/**
	 * @param unknown_type $sold_drobe_id
	 */
	
	
	function my_request($sold_drobe_id = null)
	{
		if(! empty($this->data))
		{
			$this->RequestForLabel->set($this->data);
			if($this->RequestForLabel->validates())
			{
				$this->loadModel('SoldDrobe');
				$this->SoldDrobe->recursive = -1;
				$order_id = $this->SoldDrobe->field('order_id',array('SoldDrobe.id'=>$sold_drobe_id));		
				
				$requestData = $this->data;
				$requestData['RequestForLabel']['user_id'] =$this->Auth->user('id');
				$requestData['RequestForLabel']['order_id'] =$order_id;
				
				$this->RequestForLabel->save($requestData); // save request in to database table which data pass by seller @sadikhasan
				
				$this->loadModel('User');
				$this->User->recursive = -1;
				$user_data = $this->User->find('first',array('fields'=>array('User.first_name','User.username','User.last_name','User.email'),'conditions'=>array('User.id'=>$this->Auth->user('id'))));
				
				$this->loadModel('SellProfile');
				$this->SellProfile->recursive = -1;
				//$sell_profile_data = $this->SellProfile->find('first',array('conditions'=>array('SellProfile.user_id'=>$this->Auth->user('id'))));
				
				//Getting address of seller who sold drobe and this address sent to admin in email for new shipping_label_request
				$address = $this->data['street1'].', '.
						   $this->data['street2'].', '.
						   $this->data['city'].', '.	
						   $this->data['state'].', '.
						   $this->data['zip'];
				
				//Update address in sell profile table 
				$this->SellProfile->updateAll(array(
									'SellProfile.street1'=>"'".$this->data['street1']."'",
									'SellProfile.street2'=>"'".$this->data['street2']."'",
									'SellProfile.city'=>"'".$this->data['city']."'",
									'SellProfile.state'=>"'".$this->data['state']."'",
									'SellProfile.zip'=>"'".$this->data['zip']."'"
									),
										array('SellProfile.user_id',array($this->Auth->user('id')))
						);
				/*
				 * Send Email to Admin for new user notification
				*
				*/
				
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$user_data['User']['username'],
						"##email"=>$this->data['RequestForLabel']['email'],
						"##reason_for"=>$this->data['RequestForLabel']['reason_for'],
						"##weight"=>$this->data['RequestForLabel']['weight'],
						"##address"=>$address
				);
				if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
				{
					$variables["##full_name"]=$user_data['User']['username'];
				}
				else
				{
					$variables["##full_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
				}
				// sending mail after registration
				$this->sendNotifyMail('shipping_label_request', $variables,Configure::read('admin_email'),1);
				$this->Session->setFlash('Label for request sent to admin for approval.','default',array('class'=>'success'));
				$this->redirect(array('controller'=>'sell','action'=>'sold_drobes'));
			}
			else
			{
				if(!empty($this->RequestForLabel->validationErrors))
				{
					//displaying errors in view line by line 
					$this->set('errors',$this->RequestForLabel->validationErrors);
					$this->RequestForLabel->validationErrors=array();
				}
			}
		}
		
		// loading categories list from cache
		if(Cache::read('label_weight')==null)
		{
			$this->loadModel('Weight');
			$this->Weight->_updateWeightCache();
		}
		$this->set('weightList',Cache::read('label_weight'));
		
		//For printing pre define email for user email in Request for label @sadikhasan
		$this->loadModel('User');
		$email  = $this->User->field('email',array('id'=>$this->Auth->user('id')));
		$this->set('email',$email);
		
		//Get user address from sell progile data
		$this->loadModel('SellProfile');
		$this->SellProfile->recursive = -1;
		$profile_data = $this->SellProfile->find('first',array('SellProfile.user_id'=>$this->Auth->user('id')));
		$this->set('profile_data',$profile_data);
		$this->set('profile_data',$profile_data['SellProfile']);
	}
	
	

	/**
	 * @param unknown_type $sold_drobe_id
	 * Webservice : request for label from seller to admin
	 */
	function shipping_label_request($sold_drobe_id = null)
	{
		$response = array();
		if($sold_drobe_id != null)
		{
			if(!empty($this->data))
			{
				$this->loadModel('SoldDrobe');
				$this->SoldDrobe->recursive = -1;
				$sold_drobe_data = $this->SoldDrobe->find('first',array('fields'=>array('SoldDrobe.order_id','SoldDrobe.user_id'),'conditions'=>array('SoldDrobe.id'=>$sold_drobe_id)));
			
				$request_data = $this->data;
				$request_data['user_id'] = $sold_drobe_data['SoldDrobe']['user_id'];
				$request_data['order_id'] = $sold_drobe_data['SoldDrobe']['order_id'];
				$this->RequestForLabel->save($request_data); // save request in to database table which data pass by seller @sadikhasan\
				
				
				$this->loadModel('User');
				$this->User->recursive = -1;
				$user_data = $this->User->find('first',array('fields'=>array('User.first_name','User.username','User.last_name'),'conditions'=>array('User.id'=>$request_data['user_id'])));
				
				
				$this->loadModel('SellProfile');
				$this->SellProfile->recursive = -1;
				$sell_profile_data = $this->SellProfile->find('first',array('conditions'=>array('SellProfile.user_id'=>$request_data['user_id'])));
				
				//Getting address of seller who sold drobe and this address sent to admin in email for new shipping_label_request
				$address = $sell_profile_data['SellProfile']['street1'].', '.
						$sell_profile_data['SellProfile']['street2'].', '.
						$sell_profile_data['SellProfile']['city'].', '.
						$sell_profile_data['SellProfile']['state'].', '.
						$sell_profile_data['SellProfile']['zip'];
				
				/*
				 * Send Email to Admin for new user notification
				*
				*/
				
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$user_data['User']['username'],
						"##email"=>$this->data['email'],
						"##reason_for"=>$this->data['reason_for'],
						"##weight"=>$this->data['weight'],
						"##address"=>$address
				);
				if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
				{
					$variables["##full_name"]=$user_data['User']['username'];
				}
				else
				{
					$variables["##full_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
				}
				// sending mail after registration
				$this->sendNotifyMail('shipping_label_request', $variables,Configure::read('admin_email'),1);
				
				$response['type'] = 'success';
				$response['message'] = 'Request for label sent successfully';
			}
			else
			{
				$response['type'] = 'error';
				$response['message'] = 'Parameters are required';
			}
		}
		else
		{
				$response['type'] = 'error';
				$response['message'] = 'Invalid parameter';
		}
		
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}		
?>