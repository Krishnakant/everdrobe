<?php 
class FlagCategoriesController extends AppController
{
	//var $scaffold;
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		$this->paginate = array(
			'limit' => 15
		);
		$data = $this->paginate('FlagCategory');
		$this->set(compact('data'));
	}
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->FlagCategory->save($this->data))
			{
				$this->Session->setFlash("Flag Category added successfully.",'default',array('class'=>"success"));
				$this->FlagCategory->_updateFlagCache();
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				$this->Session->setFlash("Error occured due to validations");
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->FlagCategory->id=$id;
		if(!empty($this->data))
		{
			$this->FlagCategory->validate['category_name']['isUnique']['on']='update';
			if($this->FlagCategory->save($this->data))
			{
				$this->Session->setFlash("Flag Category updated successfully.",'default',array('class'=>"success"));
				$this->FlagCategory->_updateFlagCache();
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				$this->Session->setFlash("Error occured due to validations");
			}
		}
		else
		{
			$this->data=$this->FlagCategory->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if($this->FlagCategory->delete($id))
			{
				$this->Session->setFlash("Selected flag category deleted successfully",'default',array('class'=>"success"));
				$this->FlagCategory->_updateFlagCache();
			}
			else
			{
				$this->Session->setFlash("Error occured in delete category");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	function getlist()
	{
		$categories=$this->FlagCategory->find('all');
		$data=array();
		foreach($categories as $category)
		{
			$data[]=$category['FlagCategory'];
		}
		$response=array();
		if(count($data)==0)
		{
			$response['type']="error";
			$response['message']="Flag category not found";
		}
		else 
		{
			$response['type']="success";
			$response['flag_categories']=$data;
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>