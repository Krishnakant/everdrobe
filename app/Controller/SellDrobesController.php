<?php 
class SellDrobesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		if($this->Auth->user('type')!="admin")
		{
			$this->layout="sell_drobe";
		}

		if($this->Auth->user('type')!="admin" && Configure::read('setting.drobe_sale')!="on")
		{
			// if sell drobe feature is off from admin
			throw new NotFoundException("Not found");
		}
	}
	function admin_update_size(){
		//echo "Please put condition first else it will update all";exit;
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		$sizes=Cache::read('size_list');
		$sellDrobes=$this->SellDrobe->find('all',array('conditions'=>array('SellDrobe.id <'=>582)));
		foreach($sellDrobes as $drobe)
		{
			$sizeName=$sizes[$drobe['SellDrobe']['size_name']];
			$this->SellDrobe->id=$drobe['SellDrobe']['id'];
			$this->SellDrobe->saveField('size_name',$sizeName);
		}
		exit;
	}
	function admin_relist($drobe_id)
	{
		$this->loadModel('Drobe');
		$unique_id=$this->Drobe->field('unique_id',array('id'=>$drobe_id));
		$this->setAction('relist',$unique_id);
	}
	function admin_remove_image($image_id=null)
	{
		$image_data=$this->SellDrobe->SellDrobeImage->findById($image_id);
		// removing image from everdrobe shop product
		$this->SellDrobe->SellDrobeImage->removeImageRecord($image_id);

		if($image_data['SellDrobeImage']['shopify_image_id']>0 && $image_data['SellDrobe']['shopify_product_id'] >0)
		{
			$res=$this->shopify("/products/".$image_data['SellDrobe']['shopify_product_id']."/images/".$image_data['SellDrobeImage']['shopify_image_id'],"","DELETE");
		}
		$this->Session->setFlash("Selected Image is removed from additional image","default",array("class"=>"success"));

		$this->redirect($this->referer());
	}
	function relist($unique_id=null)
	{
		$this->loadModel('Drobe');
		$this->Drobe->id=$this->Drobe->field('id',array('unique_id'=>$unique_id));
		$drobeData=$this->SellDrobe->findByDrobeId($this->Drobe->id);
		if($drobeData['SellDrobe']['sold']=="yes")
		{
			/*
			 * We had put user type condition so that
			* if this condition is true then sold drobe record in not deleted (in case of user admin)
			*/
				
			// need below assignment for updating sold drobe counter
			$this->SellDrobe->SoldDrobe->drobe_id=$this->Drobe->id;
				
			if(count($drobeData['SoldDrobe'])>0 && $this->SellDrobe->SoldDrobe->delete($drobeData['SoldDrobe'][0]['id']))
			{
				$updateFields=array();
				$updateFields['Drobe.featured']="1";
				$updateFields['SellDrobe.sold']="'no'";
				$updateFields['Drobe.post_type']="'sell'";
				if($this->Auth->user('type')!="admin")
				{
					$updateFields['SellDrobe.sold']="'yes'";
					$updateFields['SellDrobe.status']="'pending'";
				}


				$this->SellDrobe->updateAll($updateFields,array('SellDrobe.drobe_id'=>$this->Drobe->id));
				if($this->Auth->user('type')=="admin")
				{
					$this->Session->setFlash("Drobe relisted successfully for sale","default",array("class"=>"success"));
				}
				else
				{
					$this->Session->setFlash("Request sent to admin for re list drobe again for sale.","default",array("class"=>"success"));
				}
			}
			else
			{
				$this->Session->setFlash("Sold drobe tracking not found ","default",array("class"=>"error"));
			}
		}
		else
		{
			$this->Session->setFlash("Drobe is not sold.. ","default",array("class"=>"error"));
		}
		return $this->redirect($this->referer());
	}
	function admin_sellers()
	{
		$this->SellDrobe->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag")));
		$this->SellDrobe->bindModel(array('belongsTo'=>array("User")));

		$conditions=array("SellDrobe.id > "=>0,"SellDrobe.status = "=>"approved");
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
		}


		$this->paginate = array(
				'fields'=>array("SellProfile.*","User.id","User.first_name",'User.username',"User.last_name","User.email","User.photo","User.star_user","User.status","sum(SellDrobe.sold_items) as sold_items","count(SellDrobe.sold_items) as sell_drobes","sum(SellDrobe.total_paid) as seller_revenue","sum(SellDrobe.total_revenue) as total_revenue"),
				'conditions'=>$conditions,
				'joins'=>array(
						array(
								'table' => 'sell_profiles',
								'alias' => 'SellProfile',
								'type' => 'LEFT',
								'conditions' => array(
										'Drobe.user_id = SellProfile.user_id',
								)
						),
						array(
								'table' => 'sell_drobes',
								'alias' => '_SellDrobe',
								'type' => 'RIGHT',
								'conditions' => array(
										'Drobe.id = _SellDrobe.drobe_id',
								)
						)
				),
				"group"=>"Drobe.user_id"
		);

		$sellerData = $this->paginate('Drobe');


		$this->set('sellers',$sellerData);

	}
	function index()
	{
		
		$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag"),"belongsTo"=>array("User")));

		$activeSellDrobes=$this->Drobe->find('all',array("fields"=>array("Drobe.*","SellDrobe.sell_price"),"conditions"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved",'Drobe.user_id'=>$this->Auth->user('id'),'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		$soldDrobesIds=$this->SellDrobe->SoldDrobe->find('list',array('fields'=>array('SoldDrobe.drobe_id'),'conditions'=>array('SoldDrobe.user_id'=>$this->Auth->user('id')),'group'=>"SoldDrobe.drobe_id"));
		$soldDrobes=$this->Drobe->find('all',array("fields"=>array("Drobe.*","SellDrobe.sell_price"),"conditions"=>array("Drobe.id"=>$soldDrobesIds)));

		$this->Drobe->recursive=0;
		$totalDrobes=$this->Drobe->find('all',array("fields"=>array("Drobe.*"),"conditions"=>array("Drobe.post_type"=>"post","Drobe.rate_status"=>"open",'Drobe.user_id'=>$this->Auth->user('id'),'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));


		// calculating total revenue
		$totalRevenue=$this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',array('SoldDrobe.user_id'=>$this->Auth->user('id')));
		$totalRevenue = number_format((float)$totalRevenue, 2, '.', '');

		$this->loadModel("SellProfile");
		$sellerProfile=$this->SellProfile->findByUserId($this->Auth->user('id'));

		//$totalBalance=$this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.sell_price-SoldDrobe.paid_amount)',array('SoldDrobe.user_id'=>$this->Auth->user('id')));
		$totalBalance=$totalRevenue-number_format((float)$sellerProfile['SellProfile']['total_paid'], 2, '.', '');
		$totalBalance = number_format((float)$totalBalance, 2, '.', '');

		/**************************  New code for  Pending, Redeemable, Credit balance *******************/
		/* $pending_shipped = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
		 array('SoldDrobe.user_id'=>$this->Auth->user('id'),'SoldDrobe.status'=>'shipped','TIMESTAMPDIFF(HOUR,SoldDrobe.shipped_on,now()) <= '=>72)); */ //pending shipped balance contain balance which has drobe status is shipped within 72 hours

		/*		Pending balance = Drobe is sold but not shipped is known as pending baln	ace  */
		$pending_sold = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
				array('SoldDrobe.user_id'=>$this->Auth->user('id'),'SoldDrobe.status'=>'sold'));  // pending sold contain balance which is drobe is sold but not shipped.
		//$pendingBalance = $pending_shipped + $pending_sold;   // add pending_shipped and pending_sold balance to pending balance
		$pendingBalance  = $pending_sold;
		$pendingBalance = number_format((float)$pendingBalance, 2, '.', '');

		/* $redeemableBalance = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
		 array('SoldDrobe.user_id'=>$this->Auth->user('id'),'SoldDrobe.status'=>'shipped','TIMESTAMPDIFF(HOUR,SoldDrobe.shipped_on,now()) > '=>72)); */

		$redeemableBalance = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
				array('SoldDrobe.user_id'=>$this->Auth->user('id'),'SoldDrobe.status'=>'shipped'));

		$paid_to_seller = number_format((float)$sellerProfile['SellProfile']['total_paid'], 2, '.', '');   // paid_to_seller containg balance which is already paid to seller.

		
		$redeemableBalance = $redeemableBalance - $paid_to_seller;  //Subtract paid_to_seller balance which is already given to seller from redeemableBalance
		
		/*	Deduct requesteed amount from redeemable balance if seller request for redeem. */
		$redeemableBalance = $redeemableBalance - $sellerProfile['SellProfile']['requested_amount'];
		$redeemableBalance = number_format((float)$redeemableBalance, 2, '.', '');


		$creditBalance = $paid_to_seller;
		$creditBalance = number_format((float)$creditBalance, 2, '.', '');

		/**************************************************************************************************/

		/*********************************** For redemption history  *************************************/
		$this->loadModel('User');
		$this->User->recursive = -1;
		$user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		//echo($user_data['User']['first_name']);
		$this->set('full_name',$user_data['User']['username']);
		$this->loadModel('SellerPayment');
		$redemption_history = $this->SellerPayment->find('all', array('conditions'=>array('SellerPayment.user_id'=>$this->Auth->user('id')),'order'=>'SellerPayment.created_on DESC'));
		$this->set('redemption_history',$redemption_history);
		/**************************************************************************************************/


		$this->set(compact('activeSellDrobes','totalDrobes','soldDrobes','totalRevenue','sellerProfile','totalBalance','pendingBalance','redeemableBalance','creditBalance'));
	}

	//admin pay to seller payment history for seller date wise payment
	function redemption_history()
	{
		$this->loadModel('User');
		$this->User->recursive = -1;
		$user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		//echo($user_data['User']['first_name']);
		$this->set('full_name',$user_data['User']['username']);
		$this->loadModel('SellerPayment');
		$redemption_history = $this->SellerPayment->find('all', array('conditions'=>array('SellerPayment.user_id'=>$this->Auth->user('id')),'order'=>'SellerPayment.created_on DESC'));
		$this->set('redemption_history',$redemption_history);

	}


	function remove_post($drobe_id=null)
	{
		$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array('hasMany'=>array('Favourite','Flag','Rate'),'belongsTo'=>array('User','Category'),'hasOne'=>array('DrobeSetting')));

		$find = $this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobe_id,'SellDrobe.sold'=>'no','Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		if(!empty($find))
		{
			$this->Drobe->updateAll(array('Drobe.rate_status'=>"'close'",'Drobe.post_type'=>"'post'"),array('Drobe.id'=>$drobe_id,'SellDrobe.sold'=>'no'));
			$shopify_product_id = $find['SellDrobe']['shopify_product_id'];
		/*	$sell_drobe_id = $find['SellDrobe']['id'];
			$sell_drobe_images = $this->SellDrobe->SellDrobeImage->find('all',array('fields'=>array('SellDrobeImage.id'),'conditions'=>array('SellDrobeImage.sell_drobe_id'=>$sell_drobe_id)));
			foreach ($sell_drobe_images as $image_id)
			{
				$sell_drobe_image_ids[] = $image_id['SellDrobeImage']['id'];
			}
			$this->SellDrobe->SellDrobeImage->removeDeletedImagesService($sell_drobe_image_ids,$sell_drobe_id);
			$this->SellDrobe->SellDrobeImage->removeImageRecordService($sell_drobe_image_ids);
		*/
			/* Update cache when drobe is delete by user*/
			$this->Drobe->updateRecentCache();
			/*Remove badges of follower when drobe is deleted*/
			$this->_removeNotifyFollowers($drobe_id);
			
			
			$res=$this->shopify("/products/".$shopify_product_id,"","DELETE");
			$this->Session->setFlash("Drobe remove successfully","default",array("class"=>"success"));
			$this->redirect('drobes');
		}
	}

	function drobes()
	{
		$this->helpers[]='Time';

		$this->loadModel('Drobe');

		$this->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag"),"belongsTo"=>array("User")));

		$conditions=array('Drobe.user_id'=>$this->Auth->user('id'),'Drobe.deleted'=>0,'Drobe.post_type'=>'sell','Drobe.rate_status'=>'open');
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['SellDrobe.sell_brand_name LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
		}

		$this->paginate = array(
				'fields'=>array("Drobe.*","SellDrobe.*","DrobeSetting.*","Category.*"),
				'conditions'=>$conditions,
				'order'=>"Drobe.id DESC"
		);

		$drobeData = $this->paginate('Drobe');
		$this->set('drobes',$drobeData);
	}

function admin_mark_sold($drobe_id=null)
	{
		$this->loadModel('Drobe');
		$this->Drobe->id=$drobe_id;
		if($this->Drobe->id>0)
		{
			if(Cache::read('drobe_category')==null)
				{
					$this->loadModel('Category');
					$this->Category->_updateCategoryCache();
				}
					$this->set('categories',Cache::read('drobe_category'));

						// loading size list from cache
			if(Cache::read('size_list')==null)
				{
					$this->loadModel('Size');
					$this->Size->_updateSizeCache();
				}
					$this->set('sizeList',Cache::read('size_list'));
			if(!empty($this->data))
			{
				$drobeData=$this->Drobe->read();

				$sold_drobe=$this->data['SoldDrobe'];
				$sold_drobe['drobe_id']=$drobeData['Drobe']['id'];
				$sold_drobe['sell_drobe_id']=$drobeData['SellDrobe']['id'];
				$sold_drobe['user_id']=$drobeData['Drobe']['user_id'];
				$sold_drobe['sell_price']=$drobeData['SellDrobe']['sell_price'];
				$this->SellDrobe->SoldDrobe->set($sold_drobe);
				if($this->SellDrobe->SoldDrobe->validates())
					{
						// fetching order number and shipping address detail only from orders
						
						$order= $this->shopify("/orders", null,"GET","fields=created_at,order_number,shipping_address");
						//$order=json_decode('{"order":{"shipping_address": {"address1": "Chestnut Street 92","address2": "","city": "Louisville","company": null,"country": "United States","first_name": "Bob","last_name": "Norman","latitude": "45.41634","longitude": "-75.6868","phone": "555-625-1199","province": "Kentucky","zip": "40202","name": "Bob Norman","country_code": "US","province_code": "KY"}}}',true);
						$valid_order_number=false;
						$shipping_address=null;
						// checking for order id is exist
						
						if(isset($order['orders']) && count($order['orders'])>0)
							{
						   		foreach($order['orders'] as $ord)
						 			{
										if($ord['order_number']==trim($this->data['SoldDrobe']['order_id']))
											{
												// if order number found
												$valid_order_number=true;
												$shipping_address=$ord["shipping_address"];
												$order_date = $ord["created_at"];
												break;
											}
									}
							}
						
						if(!$valid_order_number)
							{
								$this->Session->setFlash("Order id is not exist, please recheck it","default",array("class"=>"error"));
								$this->_getDrobeData();
							}
							else
								{

									if($shipping_address!=null)
										{
							//
											$address_field_required=array('address1','address2','city','country','username','province','zip');
												foreach($shipping_address as $key=>$val)
													{
														if(!in_array($key, $address_field_required))
															{
																unset($shipping_address[$key]);
															}
													}
													$sold_drobe['shipping_address']=json_encode($shipping_address);
										}
										// need below assignment for updating sold drobe counter
										$this->SellDrobe->SoldDrobe->drobe_id=$drobeData['Drobe']['id'];
										$sold_drobe['order_date']= date('Y-m-d',strtotime($order_date));
									if($this->SellDrobe->SoldDrobe->save($sold_drobe))
										{
											// remove from featured
											$this->Drobe->saveField('featured',0);
											$this->Drobe->updateAll(array('Drobe.featured'=>0,"Drobe.post_type"=>"'post'"),array("Drobe.id"=>$drobeData['Drobe']['id']));

											// make tracking number field blank and mark as sold
											$this->SellDrobe->id=$drobeData['SellDrobe']['id'];
											$this->SellDrobe->updateAll(array('SellDrobe.tracking_number'=>"''","SellDrobe.sold"=>"'yes'"),array("SellDrobe.id"=>$drobeData['SellDrobe']['id']));
								
											// creating shipping address format
											$address="<address>".Configure::read('sell_drobe.shipping_address_format')."<address>";
												foreach ($shipping_address as $key=>$val)
												{
													$address=str_replace("<".$key.">",$val,$address);
												}
								
											// setting up variables for email
												$variables=array(
														//"##full_name"=>$drobeData['User']['username'],
														"##image_source"=>Router::url('/drobe_images/'.$drobeData['Drobe']['file_name'],true),
														"##drobe_comment"=>$drobeData['Drobe']['comment'],
														"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
														"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
														"##sell_price"=>$drobeData['SellDrobe']['sell_price'],
														"##revenue"=>$sold_drobe['paid_amount'],
														"##sold_comment"=>$sold_drobe['comment'],
														"##tracking_number"=>$sold_drobe['tracking_number'],
														"##shipping_address"=> $address
												);
												if(isset($drobeData['User']['username']) && $drobeData['User']['username']!="")
												{
													$variables["##full_name"]=$drobeData['User']['username'];
												}
												else
												{
													$variables["##full_name"]=$drobeData['User']['first_name']." ".$drobeData['User']['last_name'];
												}
											// Sending notification mail
											$this->sendNotifyMail('drobe_sold', $variables,$drobeData['User']['email'],1);

											$shopify_product_id = $drobeData['SellDrobe']['shopify_product_id'];
											//delete drobe from shopify.com when drob mark as sold @sadikhasan
											$res=$this->shopify("/products/".$shopify_product_id,"","DELETE");
											$this->Session->setFlash("Drobe is successfully mark as sold, you can reopen for sale from Manage drobes.","default",array("class"=>"success"));
											$this->redirect(array("controller"=>"drobes","action"=>"sell_drobes","admin"=>true));
										}
										else
										{
											$this->Session->setFlash("Error occured in mark drobe for sale","default",array("class"=>"error"));
										}
								}
				    }
					else
					{
						if(!empty($this->SellDrobe->SoldDrobe->validationErrors))
							{
								$this->set('errors',$this->SellDrobe->SoldDrobe->validationErrors);
								$this->SellDrobe->SoldDrobe->validationErrors=array();
							}
							$this->_getDrobeData();
					}
			}
			else
			{
				$this->_getDrobeData();
			}
		}
		else
		{
			$this->Session->setFlash("Invalid drobe id","default",array("class"=>"error"));
			return $this->redirect($this->referer());
		}
	}
	function admin_remind_ship($sell_drobe_id=null)
	{
		$sellDrobe=$this->SellDrobe->findById($sell_drobe_id);
		if(count($sellDrobe['SoldDrobe'])>0)
		{
			$this->loadModel('Drobe');
			$this->Drobe->id=$sellDrobe['SellDrobe']['drobe_id'];
			$drobeData=$this->Drobe->read();
				
			$sellDrobe['SoldDrobe'][0]['shipping_address']=json_decode($sellDrobe['SoldDrobe'][0]['shipping_address'],true);
				
				
				
			// creating shipping address format
			/* $address="<address>".Configure::read('sell_drobe.shipping_address_format')."</address>";
			 foreach ($sellDrobe['SoldDrobe'][0]['shipping_address']  as $key=>$val)
			 {
			$address=str_replace("<".$key.">",$val,$address);
			} */
				
			// setting up variables for email
			$variables=array(
					//"##full_name"=>$drobeData['User']['username'],
					"##image_source"=>Router::url('/drobe_images/'.$drobeData['Drobe']['file_name'],true),
					"##drobe_comment"=>$drobeData['Drobe']['comment'],
					"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
					"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
					"##sell_price"=>$drobeData['SellDrobe']['sell_price'],
					"##revenue"=>$sellDrobe['SoldDrobe'][0]['paid_amount'],
					"##sold_comment"=>$sellDrobe['SoldDrobe'][0]['comment'],
					"##tracking_number"=>$sellDrobe['SoldDrobe'][0]['tracking_number'],
					//"##shipping_address"=> $address
					"##shipping_address"=> ""
			);
			if(isset($drobeData['User']['username']) && $drobeData['User']['username']!="")
			{
				$variables["##full_name"]=$drobeData['User']['username'];
			}
			else
			{
				$variables["##full_name"]=$drobeData['User']['first_name']." ".$drobeData['User']['last_name'];
			}
			// Sending notification mail
			$this->sendNotifyMail('remind_seller', $variables,$drobeData['User']['email'],1);
			$this->Session->setFlash("Email sent succussfully to seller for remind ship product.","default",array("class"=>"success"));
		}
		else
		{
			$this->Session->setFlash("Sold drobe record not found for this drobe.","default",array("class"=>"error"));
		}
		return $this->redirect($this->referer());
	}
	function _getDrobeData()
	{
		$this->SellDrobe->id=$this->SellDrobe->field('id',array('drobe_id'=>$this->Drobe->id));
		$this->data=$this->SellDrobe->read();
		$drobe=$this->data['Drobe'];
		$total=$drobe['total_in']+$drobe['total_out'];
		/* $drobe['total_in']= $total>0 ? round($drobe['total_in']/$total*100) : 0;
		$drobe['total_out']= $total>0 ? round($drobe['total_out']/$total*100) : 0; */
		$drobe['total_in']= $drobe['total_in'];
		$drobe['total_out']= $drobe['total_out'];
		$this->set('drobe',$drobe);
	}
	function mark_sell($unique_id=null)
	{
		$this->loadModel('Drobe');

		$this->Drobe->id=$this->Drobe->field('id',array('unique_id'=>$unique_id));
		if($this->Drobe->id>0)
		{
			if(Cache::read('drobe_category')==null)
			{
				$this->loadModel('Category');
				$this->Category->_updateCategoryCache();
			}
			$this->set('categories',Cache::read('drobe_category'));
				
			// loading size list from cache
			if(Cache::read('size_list')==null)
			{
				$this->loadModel('Size');
				$this->Size->_updateSizeCache();
			}
			$this->set('sizeList',Cache::read('size_list'));
			if(!empty($this->data))
			{
				$drobeData=$this->data;
				$drobeData['Drobe']['post_type']="sell";
				$drobeData['SellDrobe']['drobe_id']=$this->Drobe->id;
				$drobeData['SellDrobe']['status']="approved";
				$drobeData['SellDrobe']['unmark_requested']=0;

				$this->Drobe->SellDrobe->id=$this->Drobe->SellDrobe->field('id',array('drobe_id'=>$this->Drobe->id));

				//custome size name as per id
				if(isset($drobeData['SellDrobe']['size_name']))
				{
					$drobeData['SellDrobe']['size_name']=$drobeData['SellDrobe']['size_name'];
				}
				elseif(isset($drobeData['SellDrobe']['size_id']))
				{
					$sizeNames=Cache::read('size_list');
					if(empty($sizeNames))
					{
						$this->loadModel('Size');
						$this->Size->_updateSizeCache();
						$sizeNames=Cache::read('size_list');
					}
					$drobeData['SellDrobe']['size_name']=$sizeNames[$drobeData['SellDrobe']['size_id']];
				}
				unset($drobeData['SellDrobe']['size_id']);

				if($this->Drobe->SellDrobe->save($drobeData))
				{
					$this->Drobe->saveField('post_type','sell');
					//add drobe to shopify when we mark as mark sell
					$this->add_drobe_to_shopify($this->Drobe->id);
						
					$this->Session->setFlash("Your drobe is sent for drobe sell list","default",array("class"=>"success"));
					$this->redirect(array("controller"=>"sell_drobes","action"=>"sell_drobes"));
				}
				else
				{
					$this->Session->setFlash("Error occured in mark drobe for sale","default",array("class"=>"error"));
				}
			}
			else
			{

				$this->data=$this->Drobe->read();
			}
		}
		else
		{
			$this->Session->setFlash("Invalid drobe id","default",array("class"=>"error"));
			return $this->redirect(array("controller"=>"sell_drobes","action"=>"drobes"));
		}
	}
	function admin_edit($drobe_id)
	{
		$this->loadModel('Drobe');
		$unique_id=$this->Drobe->field('unique_id',array('id'=>$drobe_id));
		$this->setAction('edit',$unique_id);
	}


	function edit($unique_id=null)
	{
		$this->loadModel('Drobe');
		$this->Drobe->id=$this->Drobe->field('id',array('unique_id'=>$unique_id));
		if($this->Drobe->id>0)
		{
			if(Cache::read('drobe_category')==null)
			{
				$this->loadModel('Category');
				$this->Category->_updateCategoryCache();
			}
			$this->set('categories',Cache::read('drobe_category'));

			// loading size list from cache
			if(Cache::read('size_list')==null)
			{
				$this->loadModel('Size');
				$this->Size->_updateSizeCache();
			}
			$this->set('sizeList',Cache::read('size_list'));
				
				
			// loading brand list from cache   @sadikhasan
			if(Cache::read('brand_list')==null)
			{
				$this->loadModel('Brand');
				$this->Brand->_updateBrandCache();
			}
			$this->set('brandList',Cache::read('brand_list'));
				
				
			if(!empty($this->data))
			{

				$drobeData=$this->data;
				/*
				 *  old condition 26/11/2013
				* if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']=='' )
				 *
				*/
				if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']==$this->data['old_file'] )
				{
					unset($drobeData['Drobe']['file_name']);
				}
				else
				{
					if(isset($this->data['old_file']) && $this->data['old_file']!='')
					{
						if(file_exists(WWW_ROOT. Configure::read('drobe.upload_dir').DS.$this->data['old_file']))
						{
							unlink(WWW_ROOT. Configure::read('drobe.upload_dir').DS.$this->data['old_file']);
						}
						if(file_exists(WWW_ROOT. Configure::read('drobe.iphone.upload_dir').DS.$this->data['old_file']))
						{
							unlink(WWW_ROOT. Configure::read('drobe.iphone.upload_dir').DS.$this->data['old_file']);
						}
						if(file_exists(WWW_ROOT. Configure::read('drobe.thumb.upload_dir').DS.$this->data['old_file']))
						{
							unlink(WWW_ROOT. Configure::read('drobe.thumb.upload_dir').DS.$this->data['old_file']);
						}
						if(file_exists(WWW_ROOT. Configure::read('drobe.iphone.thumb.upload_dir').DS.$this->data['old_file']))
						{
							unlink(WWW_ROOT. Configure::read('drobe.iphone.thumb.upload_dir').DS.$this->data['old_file']);
						}
					}
					/*	Delele cache memory when seller edit drobe image for displaying on home page.*/
					Cache::delete('latest_drobes');
					$this->_cropDrobe();
				}


				$drobeData['Drobe']['post_type']="sell";
				$drobeData['SellDrobe']['drobe_id']=$this->Drobe->id;

				if($this->Auth->user('type')!="admin")
				{
					//$drobeData['SellDrobe']['status']="pending";
				}

				$this->SellDrobe->id=$this->SellDrobe->field('id',array('drobe_id'=>$this->Drobe->id));
				//custome size name as per id
				if(isset($drobeData['SellDrobe']['size_name']))
				{
					$drobeData['SellDrobe']['size_name']=$drobeData['SellDrobe']['size_name'];
				}
				elseif(isset($drobeData['SellDrobe']['size_id']))
				{
					$sizeNames=Cache::read('size_list');
					if(empty($sizeNames))
					{
						$this->loadModel('Size');
						$this->Size->_updateSizeCache();
						$sizeNames=Cache::read('size_list');
					}
					$drobeData['SellDrobe']['size_name']=$sizeNames[$drobeData['SellDrobe']['size_id']];
				}
				unset($drobeData['SellDrobe']['size_id']);
				/************************************/

				//custome brand name as per id
				if(isset($drobeData['SellDrobe']['sell_brand_name']) && $drobeData['SellDrobe']['sell_brand_name']!="")
				{
					$drobeData['SellDrobe']['sell_brand_name']=$drobeData['SellDrobe']['sell_brand_name'];
				}
				elseif(isset($drobeData['SellDrobe']['brand_id']))
				{
					$brandNames=Cache::read('brand_list');
					if(empty($brandNames))
					{
						$this->loadModel('Brand');
						$this->Brand->_updateBrandCache();
						$brandNames=Cache::read('brand_list');
					}
					$drobeData['SellDrobe']['sell_brand_name']=$brandNames[$drobeData['SellDrobe']['brand_id']];
				}
				unset($drobeData['SellDrobe']['brand_id']);
				/************************************/


				if($this->SellDrobe->save($drobeData))
				{
					$drobUpdatedData = array(
							'Drobe.post_type'=>"'sell'",
							//'Drobe.featured'=>"'1'",
							'Drobe.category_id'=> "'".$drobeData['Drobe']['category_id']."'",
							'Drobe.comment'=>"'".addslashes($drobeData['Drobe']['comment'])."'",
					);
					if($drobeData['Drobe']['file_name']!="")
					{
						$drobUpdatedData['Drobe.file_name']="'".$drobeData['Drobe']['file_name']."'";
					}
						
					/*$drobUpdatedData['Drobe.category_id'] = $drobeData['Drobe']['category_id'];
					 $drobUpdatedData['Drobe.comment'] = $drobeData['Drobe']['comment']; */
					$this->Drobe->updateAll($drobUpdatedData,array('Drobe.id'=>$this->Drobe->id));
					if(is_array($drobeData['SellDrobeImage']))
					{
						$this->SellDrobe->SellDrobeImage->removeDeletedImages($drobeData['SellDrobeImage'],$this->Drobe->id);
						foreach($drobeData['SellDrobeImage'] as $sell_drobe_image)
						{
							if(trim($sell_drobe_image['file_name'])=="")
							{
								if($sell_drobe_image['id']>0)
								{
									$this->SellDrobe->SellDrobeImage->removeImageRecord($sell_drobe_image['id']);
								}
							}
							else
							{
								if($sell_drobe_image['id']>0)
								{
									$this->SellDrobe->SellDrobeImage->id=$sell_drobe_image['id'];
								}
								else $this->SellDrobe->SellDrobeImage->id=null;
								unset($sell_drobe_image['id']);
								$sell_drobe_image['sell_drobe_id']=$this->SellDrobe->id;
								$sell_drobe_image['shopify_image_id']=0;
								$this->SellDrobe->SellDrobeImage->save($sell_drobe_image);
							}
						}
					}
						
					$updateMainImage="";
						
					if(isset($drobeData['Drobe']['file_name']) && $drobeData['Drobe']['file_name']!='')
					{
						$updateMainImage=$drobeData['Drobe']['file_name'];
					}
						
					$this->add_drobe_to_shopify($this->Drobe->id,$updateMainImage);
						
					$this->Session->setFlash("Sell drobe saved successfully","default",array("class"=>"success"));
						
					if($this->Auth->user('type')!="admin") $this->redirect(array("controller"=>"sell_drobes","action"=>"sell_drobes"));
					else  $this->redirect(array("controller"=>"drobes","action"=>"sell_drobes"));
				}
				else
				{
					$variables=array(
							"##drobe_comment"=>$drobeData['Drobe']['comment'],
							"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
							"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
							"##sell_price"=>$drobeData['SellDrobe']['sell_price']
					);
					/* Send email to admin when any problem occurs while updating drobe */
					$this->sendNotifyMail('drobe_update_admin', $variables,Configure::read('admin_email'),1);
					$this->Session->setFlash("Couldn't update the product","default",array("class"=>"error"));
				}
			}
			else
			{
				$this->SellDrobe->id=$this->SellDrobe->field('id',array('drobe_id'=>$this->Drobe->id));
				$this->data=$this->SellDrobe->read();

				$drobe=$this->data['Drobe'];
				$total=$drobe['total_in']+$drobe['total_out'];
				/* $drobe['total_in']= $total>0 ? round($drobe['total_in']/$total*100) : 0;
				$drobe['total_out']= $total>0 ? round($drobe['total_out']/$total*100) : 0; */
				$drobe['total_in']= $drobe['total_in'];
				$drobe['total_out']= $drobe['total_out'];
				$this->set('drobe',$drobe);
			}
			// for calculating earning value  for seller when sold according to this percent value
			$this->set('earning_percent',Configure::read('setting.everdrobe_earning_percent'));
		}
		else
		{
			$this->Session->setFlash("Invalid drobe id","default",array("class"=>"error"));
			return $this->redirect(array("controller"=>"sell_drobes","action"=>"drobes"));
		}
	}
	function sell_drobes()
	{
		$this->helpers[]='Time';
		$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag"),"belongsTo"=>array("User")));

		$conditions=array('Drobe.user_id'=>$this->Auth->user('id'),"Drobe.post_type"=>"sell",'Drobe.deleted'=>0,'Drobe.rate_status'=>'open');
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['SellDrobe.sell_brand_name LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
		}

		$this->paginate = array(
				'fields'=>array("Drobe.*","SellDrobe.*","DrobeSetting.*","Category.*"),
				'conditions'=>$conditions,
				'order'=>"Drobe.id DESC"
		);
		$drobeData = $this->paginate('Drobe');
		$this->set('drobes',$drobeData);
	}
	function sold_drobes()
	{
		$this->helpers[]='Time';
		$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag"),"belongsTo"=>array("User")));
		/*
		 * Do not add deleted drobe condition here otherwise not counting drobe which is deleted after sold.
	 */
		$conditions=array('Drobe.user_id'=>$this->Auth->user('id'),'SellDrobe.sold_items > '=>0);

		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['SellDrobe.sell_brand_name LIKE']="%".$search;
			$conditions['OR']['Category.category_name LIKE']=$search;
		}

		$this->paginate = array(
				'fields'=>array("Drobe.*","SellDrobe.*","DrobeSetting.*","Category.*"),
				'conditions'=>$conditions,
				'order'=>"Drobe.id DESC"
		);
		$drobeData = $this->paginate('Drobe');
		$this->set('drobes',$drobeData);
	}
	function action($action=null,$unique_id=null)
	{
		$this->loadModel('Drobe');
		$drobe_id=$this->Drobe->field('id',array('unique_id'=>$unique_id));
		if($drobe_id>0)
		{
			if($action=="unmark_sale")
			{
				$updateFields=array();
				/*
				 $updateFields['Drobe.post_type']="'post'";
				$updateFields['Drobe.featured']="'0'";*/

				//if($this->Drobe->updateAll($updateFields,array('Drobe.id'=>$drobe_id)))
				if($this->Drobe->SellDrobe->updateAll(array('status'=>"'approved'",'unmark_requested'=>1),array('drobe_id'=>$drobe_id)))
				{
					$this->Drobe->updateAll(array('Drobe.post_type'=>"'post'",'Drobe.featured'=>0),array('Drobe.id'=>$drobe_id));
					//$this->Drobe->SellDrobe->updateAll(array('status'=>"'pending'",'unmark_requested'=>1),array('drobe_id'=>$drobe_id));
					$this->remove_drobe_from_shopify($drobe_id);
					$this->Session->setFlash("Your requested drobe has been remove from sell drobe list");
				}
				else
				{
					$this->Session->setFlash("Error occured in unmarked from sale drobe");
				}
				return $this->redirect($this->referer());
			}
		}
		else
		{
			$this->Session->setFlash("Invalid drobe id","default",array("class"=>"error"));
			return $this->redirect($this->referer());
		}
	}
	function admin_calculate_seller_earning()
	{
		$response=array();
		$this->SellDrobe->recursive=0;
		$this->SellDrobe->id=$this->SellDrobe->field('id',array('drobe_id'=>$this->data['sell_drobe_id']));
		$data=$this->SellDrobe->read();
		$percentage=floatval($this->data['earning_percent']);
		if($percentage<100 && $percentage>=0)
		{
			$seller_earning= $data['SellDrobe']['sell_price'] - round($data['SellDrobe']['sell_price']*$percentage/100,2);
			$response['type']="success";
			$response['amount']=$seller_earning;
		}
		else
		{
			$response['type']="error";
			$response['message']="Please enter valid percentage amount";
		}
		echo json_encode($response);
		exit();
	}

	function remove_drobe_from_shopify($drobe_id)
	{
		$SellDrobeData=$this->Drobe->SellDrobe->findByDrobeId($drobe_id);
		$SellDrobeData=$SellDrobeData['SellDrobe'];
		if($SellDrobeData['shopify_product_id']>0)
		{
			$response=$this->shopify("/products/".$SellDrobeData['shopify_product_id'], "","DELETE");
			if(count($response)==0)
			{
				/*
				 * Removing tracking id from everdrobe database
				*/
					
				$this->Drobe->SellDrobe->id=$SellDrobeData['id'];
				$this->Drobe->SellDrobe->saveField('shopify_product_id',0);
				$this->Drobe->SellDrobe->SellDrobeImage->updateAll(array('shopify_image_id'=>0),array('sell_drobe_id'=>$SellDrobeData['id']));
					
				$this->Session->setFlash("Selected product is deleted on Everdrobe Shop","default",array("class"=>"success"));
			}
			else
			{
				$this->Session->setFlash("Error occured in delete product in everdrobe shop","default",array("class"=>"error"));
			}
		}
	}


	function add_drobe_to_shopify($drobe_id,$updateMainImage=""){

		$this->loadModel('Drobe');
		$drobe_data=$this->Drobe->findById($drobe_id);
		$request_data=array("product"=>array());
		$size_list=Cache::read('size_list');
			
		/*
		 * Create collection if not exist on everdrobe shop
		*/
		/*if($drobe_data['Category']['collection_id']==0)
		 {
		$collection_id=$this->_createCollection($drobe_data['Category']);
		}
		else
		{
		$collection_id=$drobe_data['Category']['collection_id'];
		}*/
			
		/*
		 * End of create collection
		*/
			
			
		if($drobe_data['SellDrobe']['shopify_product_id']>0)
		{
			/*
			 * UPDATING Existing product in everdrobe shop
			*
			* For Some limitations/bugs of shopify we need to use XML format for update product
			*/

			// getting current product detail
			$response=$this->shopify("/products/".$drobe_data['SellDrobe']['shopify_product_id'], "","GET");

			/*
			 * Creating product tags and product size
			*/
			$product_size_name=$drobe_data['SellDrobe']['size_name'];
			if(trim($product_size_name)=="") $product_size_name="Default";

			$uploader_name=$drobe_data['User']['username']. " ".$drobe_data['User']['id'];

			$product_tags=array();
			$product_tags[]=$drobe_data['SellDrobe']['sell_brand_name'];
			$product_tags[]=$drobe_data['Category']['category_name'];
			$product_tags[]=$product_size_name;
			$product_tags[]="For ".Inflector::humanize($drobe_data['SellDrobe']['sell_gender']);
			$product_tags[]=$uploader_name;
				

			$request_data['product']=array(
					"id"=>$drobe_data['SellDrobe']['shopify_product_id'],
					"title"=>$drobe_data['Drobe']['comment'],
					"vendor"=>$drobe_data['SellDrobe']['sell_brand_name'],
					"body-html"=>$drobe_data['SellDrobe']['sell_description'],
					"product-type"=>$uploader_name,
					"tags"=> implode(",",$product_tags),
					"variants"=>array(
							'variant'=>array(
									// need to add variant id from current product
									"id"=>$response['product']['variants'][0]['id'],
									"price"=>$drobe_data['SellDrobe']['sell_price'],
									"compare_at_price"=>$drobe_data['SellDrobe']['original_sell_price'],
									"title"=> $product_size_name,
									"option1"=> $product_size_name
							)
					)
			);

			$response=$this->shopify("/products/".$drobe_data['SellDrobe']['shopify_product_id'], json_encode($request_data),"PUT");
			if(isset($response['product']) && $response['product']['id']!="")
			{
				// adding product to collection
				/*if($collection_id>0)
				 {
				$this->_updateProductCollection($response['product']['id'],$collection_id);
				}*/
					
				$this->_updateShopifyImages($drobe_data['SellDrobe']['id'],$response['product']['id'],$updateMainImage);
				$this->Session->setFlash("Selected product is updated on Everdrobe Shop","default",array("class"=>"success"));
			}
			else
			{
				if(isset($response['errors']['base']) && count($response['errors']['base'])>0)
				{
					$this->Session->setFlash("Shopify Upload Error: ". $response['errors']['base'][0],"default",array("class"=>"error"));
				}
				else $this->Session->setFlash("Error occured in upadate product in everdrobe shop","default",array("class"=>"error"));
			}

		}
		else
		{
			/*
			 * CREATING new product in everdrobe shop
			*/

			// Creating product tags and product size
			$product_size_name=$drobe_data['SellDrobe']['size_name'];
			if(trim($product_size_name)=="") $product_size_name="Default";

			$uploader_name=$drobe_data['User']['username']. " ".$drobe_data['User']['id'];

			$product_tags=array();
			$product_tags[]=$drobe_data['SellDrobe']['sell_brand_name'];
			$product_tags[]=$drobe_data['Category']['category_name'];
			$product_tags[]=$product_size_name;
			$product_tags[]="For ".Inflector::humanize($drobe_data['SellDrobe']['sell_gender']);
			$product_tags[]=$uploader_name;
				
			$request_data['product']=array(
					"title"=>$drobe_data['Drobe']['comment'],
					"vendor"=>$drobe_data['SellDrobe']['sell_brand_name'],
					"body_html"=>$drobe_data['SellDrobe']['sell_description'],
					"product_type"=>$uploader_name,
					"tags"=> implode(",",$product_tags),
					"variants"=>array(array(
							"price"=>$drobe_data['SellDrobe']['sell_price'],
							"compare_at_price"=>$drobe_data['SellDrobe']['original_sell_price'],
							"inventory_management"=> "shopify",
							//"inventory_policy"=> "continue",
							"requires_shipping"=> true,
							"taxable"=> true,
							"inventory_quantity"=>1,
							"fulfillment_service"=> "manual",
							"sku"=>sprintf("%04d",intval($drobe_data['Drobe']['id'])),
							"title"=> $product_size_name,
							"option1"=> $product_size_name
					))
			);


			/*
			 *  add main image in product
			*/
			if($_SERVER['HTTP_HOST']=="localhost")
			{
				$image_path=WWW_ROOT . Configure::read('drobe.upload_dir') .DS . $drobe_data['Drobe']['file_name'];
				$request_data['product']['images'][]=array(
						"attachment"=>base64_encode(file_get_contents($image_path))
				);
			}
			else
			{
				$request_data['product']['images'][]=array(
						"src" => Router::url("/drobe_images/iphone/".$drobe_data['Drobe']['file_name'],true)
				);
			}

			$response=$this->shopify("/products", json_encode($request_data),"POST");
			if(isset($response['product']) && $response['product']['id']!="")
			{
				//updating buy URL for the product
				$buy_url=Configure::read("sell_drobe.shop_url").$response['product']['handle'];
				$buy_url="'".$buy_url."'";
				$this->Drobe->updateAll(array('Drobe.buy_url_actual'=>$buy_url,'Drobe.buy_url'=>$buy_url),array('Drobe.id'=>$drobe_id));

				// adding product to collection
				/*if($collection_id>0)
				 {
				$this->_addProductToCollection($response['product']['id'],$collection_id);
				}*/
					
				$this->Drobe->SellDrobe->id=$drobe_data['SellDrobe']['id'];
				if($this->Drobe->SellDrobe->saveField('shopify_product_id',$response['product']['id']))
				{
					// uploading product images to shopify
					$this->_updateShopifyImages($drobe_data['SellDrobe']['id'],$response['product']['id']);
				}
				$this->Session->setFlash("Selected product is published on Everdrobe Shop","default",array("class"=>"success"));
			}
			else
			{
				if(isset($response['errors']['base']) && count($response['errors']['base'])>0)
				{
					$this->Session->setFlash("Shopify Upload Error: ". $response['errors']['base'][0],"default",array("class"=>"error"));
				}
				else $this->Session->setFlash("Error occured in publish product in everdrobe shop","default",array("class"=>"error"));
			}
		}
	}


	function _updateShopifyImages($sell_drobe_id, $shopify_product_id,$updateMainImage="")
	{
		$additional_images=$this->Drobe->SellDrobe->SellDrobeImage->find('all',array('conditions'=>array('SellDrobeImage.sell_drobe_id'=>$sell_drobe_id)));
		$images_in_shop=$this->shopify("/products/".$shopify_product_id."/images", "","GET");
		if(isset($images_in_shop['images']) && count($images_in_shop['images'])>1)
		{
			/*
			 * Considering that first image not from additional image (it is from original drobe post)
			*/
			$first_image = array_shift($images_in_shop['images']);

			$images=array();
			foreach($additional_images as $image)
			{
				$images[]=$image['SellDrobeImage']['shopify_image_id'];
			}
			/*
			 * Removing image from everdrobe shop id removed image from additional product
			*/
			foreach($images_in_shop['images'] as $existing_image)
			{
				if(!in_array($existing_image['id'], $images))
				{
					$res=$this->shopify("/products/".$shopify_product_id."/images/".$existing_image['id'],"","DELETE");
				}
			}
				
			//if first image is edited then update first image
			if($updateMainImage!="")
			{
				$res=$this->shopify("/products/".$shopify_product_id."/images/".$first_image['id'],"","DELETE");
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					$image_path=WWW_ROOT . Configure::read('drobe.upload_dir') .DS .$updateMainImage;
					$product_image=array(
							"image"=>array(
									"attachment"=>base64_encode(file_get_contents($image_path)),
									"filename"=>uniqid().".gif",
									"position" => "1"
							)
					);
				}
				else
				{
					$product_image=array(
							"image"=>array(
									"src" => Router::url('/drobe_images/'.$updateMainImage,true),
									"position" => "1"
							)
					);
				}

				$img_response=$this->shopify("/products/".$shopify_product_id."/images", json_encode($product_image),"POST");

				if(isset($img_response['image']) && $img_response['image']['id']!="")
				{
						
				}
			}
		}

		foreach($additional_images as $image)
		{
			if($image['SellDrobeImage']['shopify_image_id']==0)
			{
				/*
				 * Uploading additional image to everdrobe shop which not have shopify image id
				*/
				if($_SERVER['HTTP_HOST']=="localhost")
				{
					$image_path=WWW_ROOT . Configure::read('sell_drobe.upload_dir') .DS .$image['SellDrobeImage']['file_name'];
					$product_image=array(
							"image"=>array(
									"attachment"=>base64_encode(file_get_contents($image_path)),
									"filename"=>uniqid().".gif"
							)
					);
				}
				else
				{
					$product_image=array(
							"image"=>array(
									"src" => Router::url('/drobe_images/additional/'.$image['SellDrobeImage']['file_name'],true)
							)
					);
				}


				$img_response=$this->shopify("/products/".$shopify_product_id."/images", json_encode($product_image),"POST");

				if(isset($img_response['image']) && $img_response['image']['id']!="")
				{
					$this->Drobe->SellDrobe->SellDrobeImage->id=$image['SellDrobeImage']['id'];
					$this->Drobe->SellDrobe->SellDrobeImage->saveField('shopify_image_id',$img_response['image']['id']);
				}
			}
		}
	}


	//This webservice for manage Drobe
	function sell_drobe_list($userid=null)
	{
		$response = array();
		if($userid!=null)
		{
			$this->loadModel('Drobe');
			$this->Drobe->unbindModel(array('hasMany'=>array("Rate","Flag","Favourite")));
			$fields = array('Drobe.id','Drobe.file_name','Drobe.comment','Drobe.total_in','Drobe.uploaded_on','Drobe.views','Drobe.total_out',
					'Drobe.total_favs','Drobe.featured','Drobe.post_type','Drobe.buy_url','SellDrobe.id','SellDrobe.status','SellDrobe.sell_brand_name',
					'SellDrobe.sell_price','SellDrobe.original_sell_price','SellDrobe.is_brand_new','SellDrobe.sell_description','SellDrobe.sell_gender',
					'SellDrobe.tracking_number','SellDrobe.size_name','Category.id','Category.category_name',"User.first_name","User.last_name","User.username","User.photo");
			$data = $this->Drobe->find('all',array(
					'conditions'=>array(
							'Drobe.deleted'=>0,
							'Drobe.rate_status'=>'open',
							'Drobe.post_type'=>'sell',
							'Drobe.user_id'=>$userid,
							'SellDrobe.sold = '=>'no'
					),
					'fields'=>$fields,
					'order'=>array('Drobe.uploaded_on desc')
			));
			foreach ($data as $drobeinfo)
			{
				
				$drobeinfo['Drobe']['username'] =  $drobeinfo['User']['username'];
				$drobeinfo['Drobe']['first_name'] = $drobeinfo['User']['first_name'];
				$drobeinfo['Drobe']['last_name'] = $drobeinfo['User']['last_name'];
				$drobeinfo['Drobe']['profile_photo']="";
				if($drobeinfo['User']['photo']!="") {
					$drobeinfo['Drobe']['profile_photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$drobeinfo['User']['photo'],true);
				}
					
				unset($drobeinfo['User']);
				
				$drobeinfo['Drobe']['image_url'] = Router::url("/drobe_images/iphone/".$drobeinfo['Drobe']['file_name'],true);
				$drobeinfo['Drobe']['thumb_url'] = Router::url("/drobe_images/iphone/thumb/".$drobeinfo['Drobe']['file_name'],true);
				$drobeinfo['Drobe']['sell_drobe_id'] = $drobeinfo['SellDrobe']['id'];
				$drobeinfo['Drobe']['sell_brand_name'] = $drobeinfo['SellDrobe']['sell_brand_name'];
				$drobeinfo['Drobe']['sell_description'] = $drobeinfo['SellDrobe']['sell_description'];
				$drobeinfo['Drobe']['status'] = $drobeinfo['SellDrobe']['status'];
				$drobeinfo['Drobe']['gender'] = $drobeinfo['SellDrobe']['sell_gender'];
				$drobeinfo['Drobe']['tracking_number'] = ($drobeinfo['SellDrobe']['tracking_number']!="")?$drobeinfo['SellDrobe']['tracking_number']:"";

				$drobeinfo['Drobe']['size'] = ($drobeinfo['SellDrobe']['size_name']!="")?$drobeinfo['SellDrobe']['size_name']:"";
				$drobeinfo['Drobe']['sell_price'] = $drobeinfo['SellDrobe']['sell_price'];
				$drobeinfo['Drobe']['original_sell_price'] = ($drobeinfo['SellDrobe']['original_sell_price']!="")?$drobeinfo['SellDrobe']['original_sell_price']:"";
				$drobeinfo['Drobe']['is_brand_new'] = ($drobeinfo['SellDrobe']['is_brand_new']=="")?"new":$drobeinfo['SellDrobe']['is_brand_new'];   // This field for drobe is new or old
				$drobeinfo['Drobe']['category_name'] = $drobeinfo['Category']['category_name'];
				$drobeinfo['Drobe']['category_id'] = $drobeinfo['Category']['id'];
				$drobeinfo['Drobe']['buy_url'] = ($drobeinfo['Drobe']['buy_url'] != "")?$drobeinfo['Drobe']['buy_url']:"";


				//For getting addditional images for sell drobe which have additional images
				$this->loadModel('SellDrobeImage');
				$this->SellDrobeImage->recursive = -1;
				$sell_drobe_images= $this->SellDrobeImage->find('all',array('conditions'=>array('SellDrobeImage.sell_drobe_id'=>$drobeinfo['SellDrobe']['id'])));
				$sell_drobe_image_data = array();

				foreach ($sell_drobe_images as $sell_drobe_image)
				{
					$sell_drobe_image['SellDrobeImage']['shopify_image_id'] = ($sell_drobe_image['SellDrobeImage']['shopify_image_id'] == null)?"":$sell_drobe_image['SellDrobeImage']['shopify_image_id'];
					$sell_drobe_image['SellDrobeImage']['image_url'] = Router::url("/drobe_images/additional/".$sell_drobe_image['SellDrobeImage']['file_name'],true);
					$sell_drobe_image['SellDrobeImage']['thumb_url'] = Router::url("/drobe_images/additional/thumb/".$sell_drobe_image['SellDrobeImage']['file_name'],true);
					$sell_drobe_image_data[] = $sell_drobe_image['SellDrobeImage'];
				}

				$drobeinfo['Drobe']['additional_image'] = $sell_drobe_image_data;

				$selldrobe['Drobe'][] = $drobeinfo['Drobe'];
			}
			if($selldrobe)
			{
				$response['type'] = "success";
				$response['Drobe'] = $selldrobe['Drobe'];
			}
			else
			{
				$response['type'] = "error";
				$response['message'] = "No drobes found";
			}
		}
		else
		{
			$response['type'] = "error";
			$response['message'] = "Invalid Parameter";
		}


		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}

	//This service delete drobe which is post as sell drobe
	function delete_sell_drobe($drobe_id=null)
	{
		$response = array();
		if($drobe_id != null)
		{
			$this->loadModel('Drobe');
			$this->Drobe->unbindModel(array('hasMany'=>array('Favourite','Flag','Rate'),'belongsTo'=>array('User','Category'),'hasOne'=>array('DrobeSetting')));
				
			$find = $this->Drobe->find('first',array('conditions'=>array('Drobe.id'=>$drobe_id,'SellDrobe.sold'=>'no','Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
			if(!empty($find))
			{
				$this->Drobe->updateAll(array('Drobe.rate_status'=>"'close'",'Drobe.post_type'=>"'post'"),array('Drobe.id'=>$drobe_id,'SellDrobe.sold'=>'no'));
				$shopify_product_id = $find['SellDrobe']['shopify_product_id'];
			/*	$sell_drobe_id = $find['SellDrobe']['id'];
				$sell_drobe_images = $this->SellDrobe->SellDrobeImage->find('all',array('fields'=>array('SellDrobeImage.id'),'conditions'=>array('SellDrobeImage.sell_drobe_id'=>$sell_drobe_id)));
				foreach ($sell_drobe_images as $image_id)
				{
					$sell_drobe_image_ids[] = $image_id['SellDrobeImage']['id'];
				}
				
				$this->SellDrobe->SellDrobeImage->removeDeletedImagesService($sell_drobe_image_ids,$sell_drobe_id);
				$this->SellDrobe->SellDrobeImage->removeImageRecordService($sell_drobe_image_ids);*/

				$res=$this->shopify("/products/".$shopify_product_id,"","DELETE");

				/* Update cache when drobe is delete by user*/
				$this->Drobe->updateRecentCache();
				/*Remove badges of follower when drobe is deleted*/
				$this->_removeNotifyFollowers($drobe_id);
				
				$response['type'] = 'success';
				$response['message'] = 'Drobe deleted successfully!!!';
			}
			else
			{
				$response['type'] = 'error';
				$response['message'] = 'Drobe is not found!!!';
			}
		}
		else
		{
			$response['type']='error';
			$response['message']='Invalid Parameter';
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}


	function seller_financial_fields($userid=null)
	{
		$response = array();
		if($userid!=null)
		{
			$this->loadModel('Drobe');
			$this->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag"),"belongsTo"=>array("User")));
			$find = $this->Drobe->find('first',array('conditions'=>array('user_id'=>$userid)));
			if($find)
			{
				$activeSellDrobes=$this->Drobe->find('all',array("fields"=>array("Drobe.*","SellDrobe.sell_price"),"conditions"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved",'Drobe.user_id'=>$userid,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
				$soldDrobesIds=$this->SellDrobe->SoldDrobe->find('list',array('fields'=>array('SoldDrobe.drobe_id'),'conditions'=>array('SoldDrobe.user_id'=>$userid),'group'=>"SoldDrobe.drobe_id"));
				$soldDrobes=$this->Drobe->find('all',array("fields"=>array("Drobe.*","SellDrobe.sell_price"),"conditions"=>array("Drobe.id"=>$soldDrobesIds)));

				$this->Drobe->recursive=0;
				$totalDrobes=$this->Drobe->find('all',array("fields"=>array("Drobe.*"),"conditions"=>array("Drobe.post_type"=>"post","Drobe.rate_status"=>"open",'Drobe.user_id'=>$userid,'Drobe.deleted'=>0)));

				$totalRevenue=$this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',array('SoldDrobe.user_id'=>$userid));

				$active_drobe_amount=0;
				foreach($activeSellDrobes as $drobe)
				{
					$active_drobe_amount += $drobe['SellDrobe']['sell_price'];
				}

				$sell_drobe = array();
				$sell_drobe['active_sell_drobes'] = count($activeSellDrobes);
				$sell_drobe['amount_of_active_drobe'] = $active_drobe_amount;
				$sell_drobe['total_sold_drobes'] = count($soldDrobes);
				$sell_drobe['total_revenue'] = ($totalRevenue==null)?0:round($totalRevenue,2);
				$sell_drobe['active_normal_drobes'] = count($totalDrobes);

				$this->loadModel("SellProfile");
				$sellerProfile=$this->SellProfile->findByUserId($userid);
				$totalBalance = $totalRevenue - $sellerProfile['SellProfile']['total_paid'];
				$sell_drobe['total_balance'] = round($totalBalance,2);
				$response['type'] = 'success';
				$response['message'] = $sell_drobe;
			}
			else
			{
				$response['type'] = 'error';
				$response['message'] = 'user ID not found!!!';
			}
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Please enter user id';
		}

		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}


	private function _createAdditionalImage($imageData)
	{

		if(isset($this->data['file_size']) && strlen($imageData)!=$this->data['file_size'])
		{
			return array('type'=>"error","message"=>"Error occured while upload your drobe image.");
		}
		$data = base64_decode($imageData);
		$src = imagecreatefromstring($data);
		// making square image
		$width = imagesx($src);
		$height = imagesy($src);
		$square_image = imagecreatetruecolor( $width, $height );
		imagecopymerge($square_image, $src, 0, 0,0,0, $width , $height, 100);
		imagedestroy($src);

		// creating unique file name
		$file_name=uniqid().'.jpg';
		$target_path = WWW_ROOT . Configure::read('sell_drobe.upload_dir').DS.$file_name;
		if($this->_resizeDrobeImage($square_image, $target_path,$width, $height)!="")
		{
			$target_path = WWW_ROOT . Configure::read('sell_drobe.thumb.upload_dir').DS.$file_name;
			$width=Configure::read('sell_drobe.thumb.width');
			$height=Configure::read('sell_drobe.thumb.height');
			$this->_resizeDrobeImage($square_image, $target_path,$width, $height);

			return $file_name;
		}
		else return false;
	}


	function _resizeDrobeImage($src,$target_path,$newWidth,$newHeight)
	{
		$width = imagesx($src);
		$height = imagesy($src);
		/* create a new, "virtual" image */
		$virtual_image = imagecreatetruecolor($newWidth,$newHeight);

		/* copy source image at a resized size */
		imagecopyresampled($virtual_image,$src,0,0,0,0,$newWidth,$newHeight,$width,$height);
		/* create the physical thumbnail image to its destination */
		if(imagejpeg($virtual_image,$target_path))
		{
			imagedestroy($virtual_image);
			return array_pop(explode(DS,$target_path));
		}
		else return false;
	}

	function additional_image_upload($drobe_id=null)
	{
		$is_set_image = false;
		$is_error = false;
		if($drobe_id!=null)
		{
			$sellDrobeId = $this->SellDrobe->field('id',array('SellDrobe.drobe_id'=>$drobe_id));
			if($sellDrobeId != 0)
			{
				$this->loadModel('SellDrobeImage');
				for($i=1;$i<=3;$i++)
				{
					if(isset($this->data["image".$i]) && $this->data["image".$i]!="")
					{
						$is_set_image = true;
						$image_file= $this->_createAdditionalImage($this->data["image".$i]);
						if($image_file!="")
						{
							$response['image_url'][]=Router::url("/drobe_images/additional/".$image_file,true);
							$response['thumb_url'][]=Router::url("/drobe_images/additional/thumb/".$image_file,true);
							$sellDrobe[] = array('sell_drobe_id'=>$sellDrobeId,'file_name'=>$image_file);
						}
						else
						{
							$is_error = true;
							break;  //if any error occur in between uploading additional images then loop will terminate
						}
					}
				}

				//$is_error true when base64encoded data is not proper or image_file name empty
				if($is_error)
				{
					$response['type']="error";
					$response['message']="Source of image is invalid accept only base64encoded source";
				}
				else if($is_set_image) //$is_set_image is false only when no any image is uploaded
				{
					$this->loadModel('SellDrobeImages');
					$this->SellDrobeImages->saveAll($sellDrobe);
					$this->add_drobe_to_shopify($drobe_id);
					$response['type'] = 'success';
					$response['message'] = "Additional Image uploaded successfully!!!";
				}
				else
				{
					$response['type'] = 'error';
					$response['message'] = "Please upload additional images";
				}
			}
			else
			{
				$response['type'] = 'error';
				$response['message'] = "sell drobe not found!!!";
			}
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = "Please enter drobeid";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}


	function edit_additional_image($sell_drobe_id=null)
	{

		if($sell_drobe_id!=null)
		{
			if(!empty($this->data['removed_images']))
			{
				$sell_drobe_image_ids = explode(",",$this->data['removed_images']);
				$this->SellDrobe->SellDrobeImage->removeDeletedImagesService($sell_drobe_image_ids,$sell_drobe_id);
				$this->SellDrobe->SellDrobeImage->removeImageRecordService($sell_drobe_image_ids);
			}
				
			$this->loadModel('SellDrobeImage');
			for($i=1;$i<=3;$i++)
			{
				if(isset($this->data["image".$i]) && $this->data["image".$i]!="")
				{
					$is_set_image = true;
					$image_file= $this->_createAdditionalImage($this->data["image".$i]);
					if($image_file!="")
					{
						$response['image_url'][]=Router::url("/drobe_images/additional/".$image_file,true);
						$response['thumb_url'][]=Router::url("/drobe_images/additional/thumb/".$image_file,true);
						$sellDrobe[] = array('sell_drobe_id'=>$sell_drobe_id,'file_name'=>$image_file);
					}
					else
					{
						$is_error = true;
						break;  //if any error occur in between uploading additional images then loop will terminate
					}
				}
			}
				
			if($is_error)
			{
				$response['type']="error";
				$response['message']="Source of image is invalid accept only base64encoded source";
			}
			else
			{
				$drobe_id = $this->SellDrobe->field('drobe_id',array('SellDrobe.id'=>$sell_drobe_id));
				$this->loadModel('SellDrobeImages');
				$this->SellDrobeImages->saveAll($sellDrobe);
				$this->add_drobe_to_shopify($drobe_id);   //Add new drobe images on shopify.com
				$response['type'] = 'success';
				$response['message'] = 'Drobe updated successfully!!!';
			}
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Invalid parameters';
		}

		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}


	//THis webservice containing pending balance, redeemable balance and credit balance
	function my_balance($userid=null)
	{
		
		$response = array();
		if($userid!=null)
		{
			$find = $this->SellDrobe->SoldDrobe->field('id',array('SoldDrobe.user_id'=>$userid));
			if($find)
			{
				/* $pending_shipped = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
				 array('SoldDrobe.user_id'=>$userid,'SoldDrobe.status'=>'shipped','TIMESTAMPDIFF(HOUR,SoldDrobe.shipped_on,now()) <= '=>72)); */

				/* $redeemable = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
				 array('SoldDrobe.user_id'=>$userid,'SoldDrobe.status'=>'shipped','TIMESTAMPDIFF(HOUR,SoldDrobe.shipped_on,now()) > '=>72)); */
				$redeemable = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
						array('SoldDrobe.user_id'=>$userid,'SoldDrobe.status'=>'shipped'));
				$redeemable  = number_format((float)$redeemable, 2, '.', '');
				
				$this->loadModel("SellProfile");
				$sellerProfile=$this->SellProfile->findByUserId($userid);
				//Get Balance which is already paid to seller
				$paid_to_seller = $sellerProfile['SellProfile']['total_paid'];
				$paid_to_seller  = number_format((float)$paid_to_seller, 2, '.', '');

				$pending_sold = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
						array('SoldDrobe.user_id'=>$userid,'SoldDrobe.status'=>'sold'));

				$pending = number_format((float) $pending_sold, 2, '.', '');

				$this->loadModel('Drobe');
				$this->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag"),"belongsTo"=>array("User")));
				$for_sale = 0;

				// Active selldrobe which are Post for sell but actually it is not sold count its balance as a for_sale
				$activeSellDrobes=$this->Drobe->find('all',array("fields"=>array('sum(SellDrobe.sell_price) as for_sale'),"conditions"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved",'Drobe.user_id'=>$userid,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
				$for_sale = $activeSellDrobes[0][0]['for_sale'];
				$for_sale = number_format((float) $for_sale, 2, '.', '');
				$credit = $paid_to_seller;  // credit balance which is already paid to seller
				
				
				//$redeemable = ($redeemable>$paid_to_seller)?($redeemable - $paid_to_seller):$redeemable;  //deduct paid_to_seller balance from reedamable balance which already given to seller.
				$redeemable = $redeemable - $paid_to_seller;  //This subtraction will return float or integer value so it convert into string.
				$redeemable = number_format((float) $redeemable, 2, '.', '');
				 
				 
				/* Deduct requested amount from redeemable account if seller already requested for RedeemBalance */
				$requested_amount = number_format((float) $sellerProfile['SellProfile']['requested_amount'], 2, '.', '');
				if($redeemable>0)
				{
					$redeemable = $redeemable - $requested_amount;
				}
				
				
				/* It displays two decimal point after amount. */
				$redeemable  = number_format((float)$redeemable, 2, '.', '');
				$soldDrobe['credit'] =($credit==null)?"0":$credit."";
				$soldDrobe['redeemable'] =($redeemable == null)?"0":$redeemable."";
				$soldDrobe['pending'] = ($pending == null)?"0" : $pending."";
				$soldDrobe['for_sale'] = ($for_sale== null)?"0" : $for_sale."";
				$response['type'] = 'success';
				$response['data'] = $soldDrobe;
			}
			else
			{
				$response['type'] = 'error';
				$response['message'] = 'Balance not found';
			}
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Please enter user id';
		}

		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}



	function _cropDrobe()
	{
		$uploadDir = Configure::read('drobe.upload_dir');
			
		$thumbWidth=Configure::read('drobe.thumb.width');
		$thumbHeight=Configure::read('drobe.thumb.height');
		$thumbFolder = Configure::read('drobe.thumb.upload_dir');
			
			
		$iPhoneWidth=Configure::read('drobe.iphone.width');
		$iPhoneHeight=Configure::read('drobe.iphone.height');
		$iPhoneFolder = Configure::read('drobe.iphone.upload_dir');
			

		$iPhoneThumbWidth=Configure::read('drobe.iphone.thumb.width');
		$iPhoneThumbHeight=Configure::read('drobe.iphone.thumb.height');
		$iPhoneThumbFolder = Configure::read('drobe.iphone.thumb.upload_dir');


		$newWidth=Configure::read('drobe.width');
		$newHeight=Configure::read('drobe.height');
		$uploadFolder=Configure::read('drobe.upload_dir');

		$image_src=WWW_ROOT. $uploadFolder.DS.$this->data['Drobe']['file_name'];

		$left=$this->data['pos_left'];
		$top=$this->data['pos_top'];
		$cropped_width=$this->data['new_width'];
		$cropped_height=$this->data['new_height'];


		// generating iphone image
		$iphone_target=WWW_ROOT. $iPhoneFolder.DS.$this->data['Drobe']['file_name'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $iPhoneWidth, $iPhoneHeight, $iphone_target);


		// creating thumb image
		$thumb_target=WWW_ROOT. $thumbFolder.DS.$this->data['Drobe']['file_name'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $thumbWidth, $thumbHeight, $thumb_target);

		// creating iphone thumb image
		$iphone_thumb_target=WWW_ROOT. $iPhoneThumbFolder.DS.$this->data['Drobe']['file_name'];
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $iPhoneThumbWidth, $iPhoneThumbHeight, $iphone_thumb_target);

		// generating web image
		$this->_cropImage($image_src, $left, $top, $cropped_width, $cropped_height, $newWidth, $newHeight, $image_src);


	}

	function _cropImage($src,$left,$top,$width,$height,$new_width,$new_height,$target_path)
	{
		/* read the source image */
		$ext=strtolower(array_pop(explode(".", $src)));
		if($ext=="jpg" || $ext=="jpeg") $image = imagecreatefromjpeg($src);
		else if($ext=="gif") $image = imagecreatefromgif($src);
		else if($ext=="png") $image = imagecreatefrompng($src);

		if(!($width>0)) $width=imagesx($image);
		if(!($height>0)) $height=imagesy($image);
			
		$new_image = ImageCreateTrueColor( $new_width, $new_height );
		// filling white color in extra part

		imagecopyresampled($new_image,$image,0,0,$left,$top,$new_width,$new_height,$width,$height);

		// filling white color in extra part
		/*$white = imagecolorallocate($new_image, 255, 255, 255);
			if($new_width<$width)
			{
		$this->_fillColor($new_image, $height, 0, $white);
		}
		if($new_height<$height)
		{
		$this->_fillColor($new_image, 0, $width, $white);
		}*/
			
		if($ext=="jpg" || $ext=="jpeg") imagejpeg($new_image,$target_path);
		else if($ext=="gif") imagegif($new_image,$target_path);
		else if($ext=="png") imagepng($new_image,$target_path);
			
	}


	function mark_sold_shopify()
	{

		//$this->data coming from shopify.com when drobe is sold from shopify and shopify will automatically call this method and format of $this->data is given in above format
		$data = $this->data;

		foreach ($data['line_items'] as $sold_data)
		{
				
			$product_id = $sold_data['product_id'];
			$this->loadModel('SellDrobe');
			$drobe_id = $this->SellDrobe->field('SellDrobe.drobe_id',array("SellDrobe.shopify_product_id"=>$product_id));
				
			$this->loadModel('Drobe');
			$this->Drobe->id=$drobe_id;
			if($this->Drobe->id>0)
			{
				if(Cache::read('drobe_category')==null)
				{
					$this->loadModel('Category');
					$this->Category->_updateCategoryCache();
				}
				$this->set('categories',Cache::read('drobe_category'));
					
				// loading size list from cache
				if(Cache::read('size_list')==null)
				{
					$this->loadModel('Size');
					$this->Size->_updateSizeCache();
				}
				$this->set('sizeList',Cache::read('size_list'));

				$drobeData=$this->Drobe->read();
					
				//count paid amount after deduct admin percent from listing sell price
				if($drobeData['SellDrobe']['sold']=='no')
				{
					$sold_drobe = array();
					$sold_drobe['paid_amount'] = $drobeData['SellDrobe']['sell_price'] - (($drobeData['SellDrobe']['sell_price']*Configure::read("setting.everdrobe_earning_percent"))/100);
					$sold_drobe['order_id'] = $data['order_number'];
					$sold_drobe['drobe_id']=$drobeData['Drobe']['id'];
					$sold_drobe['sell_drobe_id']=$drobeData['SellDrobe']['id'];
					$sold_drobe['user_id']=$drobeData['Drobe']['user_id'];
					$sold_drobe['sell_price']=$drobeData['SellDrobe']['sell_price'];
					$this->SellDrobe->SoldDrobe->set($sold_drobe);
					if($this->SellDrobe->SoldDrobe->validates())
					{
						$shipping_address=null;
						// checking for order id is exist
						$shipping_address = $data["shipping_address"];
						$order_date = $data["created_at"];

						if($shipping_address!=null)
						{
							//
							$address_field_required=array('address1','address2','city','country','username','province','zip');
							foreach($shipping_address as $key=>$val)
							{
								if(!in_array($key, $address_field_required))
								{
									unset($shipping_address[$key]);
								}
							}
							$sold_drobe['shipping_address']=json_encode($shipping_address);
						}
							
						// need below assignment for updating sold drobe counter
						$this->SellDrobe->SoldDrobe->drobe_id=$drobeData['Drobe']['id'];
						$sold_drobe['order_date']= date('Y-m-d',strtotime($order_date));
						if($this->SellDrobe->SoldDrobe->save($sold_drobe))
						{
								
							// remove from featured
							$this->Drobe->saveField('featured',0);
							$this->Drobe->updateAll(array('Drobe.featured'=>0,"Drobe.post_type"=>"'post'"),array("Drobe.id"=>$drobeData['Drobe']['id']));
								
							// make tracking number field blank and mark as sold
							$this->SellDrobe->id=$drobeData['SellDrobe']['id'];
							$this->SellDrobe->updateAll(array('SellDrobe.tracking_number'=>"''","SellDrobe.sold"=>"'yes'"),array("SellDrobe.id"=>$drobeData['SellDrobe']['id']));
								
							if($this->getUserSetting($drobeData['Drobe']['user_id'],'sold_drobe'))
							{
								$params=array('action'=>"drobe_sold");
								//send push notification to the seller when it's drobe is sold from shopify
								$this->send_notification_user("Your drobe is sold",$drobeData['Drobe']['user_id'],$params);
									
							}


							// creating shipping address format
							$address="<address>".Configure::read('sell_drobe.shipping_address_format')."<address>";
							foreach ($shipping_address as $key=>$val)
							{
								$address=str_replace("<".$key.">",$val,$address);
							}

							// setting up variables for email to seller
							$variables=array(
									//"##full_name"=>$drobeData['User']['username'],
									"##image_source"=>Router::url('/drobe_images/'.$drobeData['Drobe']['file_name'],true),
									"##drobe_comment"=>$drobeData['Drobe']['comment'],
									"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
									"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
									"##sell_price"=>$drobeData['SellDrobe']['sell_price'],
									"##revenue"=>$sold_drobe['paid_amount'],
									"##sold_comment"=>"",//$sold_drobe['comment'],
									"##tracking_number"=>""//$sold_drobe['tracking_number'],

							);
							if(isset($drobeData['User']['username']) && $drobeData['User']['username']!="")
							{
								$variables["##full_name"]=$drobeData['User']['username'];
							}
							else
							{
								$variables["##full_name"]=$drobeData['User']['first_name']." ".$drobeData['User']['last_name'];
							}
							// Sending notification mail to seller
							$this->sendNotifyMail('drobe_sold', $variables,$drobeData['User']['email'],1);


							// setting up variables for email to admin
							$variables=array(
									"##image_source"=>Router::url('/drobe_images/'.$drobeData['Drobe']['file_name'],true),
									"##drobe_comment"=>$drobeData['Drobe']['comment'],
									"##brand_name"=>$drobeData['SellDrobe']['sell_brand_name'],
									"##drobe_size"=>$drobeData['SellDrobe']['size_name'],
									"##sell_price"=>$drobeData['SellDrobe']['sell_price']
							);
							// Sending notification mail
							$this->sendNotifyMail('drobe_sold_admin', $variables,Configure::read('admin_email'),1);
							//$this->sendNotifyMail('drobe_sold_admin', $variables,Configure::read('admin_email'),1);
								
							$shopify_product_id = $drobeData['SellDrobe']['shopify_product_id'];

							//delete drobe from shopify.com when drob mark as sold @sadikhasan
							$res=$this->shopify("/products/".$shopify_product_id,"","DELETE");

						}
					}


				}


			}
		}
		exit();
	}

	
function _removeNotifyFollowers($drobe_id,$follower_id=null)
	{
		$this->loadModel('NewDrobe');
		if($follower_id!=null)
		{
			$this->NewDrobe->deleteAll(compact('drobe_id','follower_id'));
			return $this->NewDrobe->getAffectedRows() > 0;
		}
		else
		{
			$this->NewDrobe->deleteAll(compact('drobe_id'));
			return $this->NewDrobe->getAffectedRows() > 0;
		}
	}



}
?>
