<?php 
class CategoriesController extends AppController
{
	//var $scaffold;
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		//$categories=$this->Category->find('all');
		//$this->set('categories',$categories);
		$this->paginate = array(
			'fields'=>array('Category.*','count(Drobe.id) as total_drobes'),
			'limit' => 15,
			'group' => "Category.id",
			'order'=>'Category.order ASC',
			'joins'=>array(
					array(
						'table' => 'drobes',
						'alias' => 'Drobe',
						'type' => 'LEFT',
						'conditions' => array(
							'Drobe.category_id = Category.id',
							'Drobe.deleted = 0'
						)
					)
				)
		);
		$data = $this->paginate('Category');
		$this->set(compact('data'));
	}
	function admin_position($action=null,$category_id=null)
	{
		$this->Category->recursive=-1;
		$this->Category->id=$category_id;
		$current_order=$this->Category->field('order',array('Category.id'=>$category_id));
		if($action=="up")
		{
			$previous_drobe=$this->Category->find('first',array("fields"=>array('Category.id','Category.order'),"conditions"=>array("Category.order < "=>$current_order,"Category.id != "=>0),"order"=>"Category.order DESC"));
			if($previous_drobe)
			{
				$this->Category->saveField('order',$previous_drobe['Category']['order']);
				$this->Category->id=$previous_drobe['Category']['id'];
				$this->Category->saveField('order',$current_order);
				$this->Session->setFlash("Category moved up successfully");
			}
			else
			{
				$this->Session->setFlash("Category not found in up order");
			}
		}
		else if($action=="down")
		{
			$next_drobe=$this->Category->find('first',array("fields"=>array('Category.id','Category.order'),"conditions"=>array("Category.order > "=>$current_order,"Category.id != "=>0),"order"=>"Category.order ASC"));
			if($next_drobe)
			{
				$this->Category->saveField('order',$next_drobe['Category']['order']);
				$this->Category->id=$next_drobe['Category']['id'];
				$this->Category->saveField('order',$current_order);
				$this->Session->setFlash("Category moved down successfully");
			}
			else
			{
				$this->Session->setFlash("Category not found in down order");
			}
		}
		else if($action=="first" || $action=="last")
		{
			
			$listing_order=$this->Category->find('list',array("fields"=>array("Category.order"),"conditions"=>array("Category.id NOT "=>array($category_id,0)),"order"=>"Category.order ASC"));
			if($action=="first")
			{
				$this->Category->saveField('order',1);
				$pos=2;
				foreach ($listing_order as $id=>$order)
				{
					$this->Category->id=$id;
					$this->Category->saveField('order',$pos);
					$pos++;
				}
			$this->Session->setFlash("Category moved first successfully");
			}
			else
			{
				$pos=1;
				foreach ($listing_order as $id=>$order)
				{
					$this->Category->id=$id;
					$this->Category->saveField('order',$pos);
					$pos++;
				}
				$this->Category->id=$category_id;
				$this->Category->saveField('order',$pos);
				$this->Session->setFlash("Category moved last successfully");
			}
				
		}
		else if($action=="reset")
		{
			$categories=$this->Category->find('all',array("order"=>"Category.category_name ASC",'conditions'=>array("Category.id != "=>0)));
			if($categories)
			{
				$order=1;
				foreach($categories as &$category)
				{
					$category['Category']['order']=$order;
					$order++;
				}
				$this->Category->saveAll($categories);
			}
		
		}
		$this->Category->_updateCategoryCache();
		return $this->redirect($this->referer());
	}
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->Category->save($this->data))
			{
				$this->Session->setFlash("Category added successfully.",'default',array('class'=>"success"));
				$this->Category->_updateCategoryCache();
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Category->id=$id;
		if(!empty($this->data))
		{
			$this->Category->validate['category_name']['isUnique']['on']='update';
			
			/*if($this->Category->save($this->data))
			{
				$this->Session->setFlash("Category updated successfully.",'default',array('class'=>"success"));
				$this->Category->_updateCategoryCache();
				$this->redirect(array('action'=>'index'));
			}*/
			
			$update_fields = array(
							'Category.category_name'=>"'".$this->data['Category']['category_name']."'",
							'Category.status'=>"'".$this->data['Category']['status']."'"
							);
			
			$this->Category->recursive = -1;
			if($this->Category->updateAll($update_fields,array('Category.id'=>$id)))
			{
				$this->Session->setFlash("Category updated successfully.",'default',array('class'=>"success"));
				$this->Category->_updateCategoryCache();
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Category->read();
		}
	}
	function admin_delete($id=null)
	{	
		if($id>0)
		{	
			if(configure::read('other_category_id')!=$id)
			{	
				if($this->Category->delete($id))
				{
					$this->Session->setFlash("Selected category deleted successfully",'default',array('class'=>"success"));
					//If admin delete any category then it's category related all drobe category set as other.
					$this->loadModel('Drobe');
					$this->Drobe->recursive = -1;
					//set other_category_id is configured in core.php default.
					$other_category_id = Configure::read('other_category_id');
					$this->Drobe->updateAll(array('Drobe.category_id'=>$other_category_id),array('Drobe.category_id'=>$id));
					$this->Category->_updateCategoryCache();
				}
				else
				{
					$this->Session->setFlash("Error occured in delete category");
				}
			}
			else
			{
				$this->Session->setFlash("You cannot delete this default category");
			}	
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	
	
	function getlist()
	{
		
		$categories=Cache::read('drobe_category');
		if($categories==null)
		{
			$this->Category->_updateCategoryCache();
			$categories=Cache::read('drobe_category');
		}
		if(count($categories)>0)
		{
			$data=array();
			foreach($categories as $id=>$category_name)
			{
				$data[]=array('id'=>"".$id,'category_name'=>$category_name);
			}
			$response['type']="success";
			$response['categories']=$data;
		}
		else 
		{
			$response['type']="error";
			$response['message']="Category not found";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>