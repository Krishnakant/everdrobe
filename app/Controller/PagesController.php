<?php
App::uses('AppController', 'Controller');

class PagesController extends AppController {
	public $name = 'Pages';
	public $helpers = array('Html', 'Session');
	public $uses = array();
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}
	
	public function display($page=null,$layout="content_page") {
		$pages=Cache::read('content_pages');
		if($pages==null)
		{
			$this->Page->updateCache();
			$pages=Cache::read('content_pages');
		}
		if($page=="home")
		{
			$user_agent=$_SERVER['HTTP_USER_AGENT'];
			if(preg_match("/iPhone/i", $user_agent) || preg_match("/iPod/i", $user_agent))
			{
				return $this->redirect("https://itunes.apple.com/in/app/id576116687");
			}
			if(preg_match("/android/i", $user_agent))
			{
				return $this->redirect("https://play.google.com/store/apps/details?id=com.bizhill1.everdrobe");
			}
			
			//$pages=$this->Page->find("all",array("conditions"=>array("Page.page_name"=>array("welcome-everdrobe","welcome-points"))));
			$this->set('welcome_points',$pages['welcome-points']['page_content']);
			$this->set('welcome_title',$pages['welcome-everdrobe']['page_title']);
			$this->set('welcome_content',$pages['welcome-everdrobe']['page_content']);
			
			$drobes=Cache::read('latest_drobes');
			if(Cache::read('latest_drobes')==null)
			{
				$this->loadModel('Drobe');
				$this->Drobe->updateRecentCache();
				$drobes=Cache::read('latest_drobes');
			}
			$this->set("drobes",$drobes);
			
			$this->set('title_for_layout',"Welcome");
			$this->layout="home";
			$this->render('home');
		}
		else
		{
			//$page=$this->Page->findByPageName($page);
			$this->set("page_title",$pages[$page]['page_title']);
			$this->set("page_content",$pages[$page]['page_content']);
			$this->set('title_for_layout',$pages[$page]['page_title']);
			$this->layout=$layout;
			$this->render('content');
		}
	}
	function admin_index()
	{
		$pages=$this->Page->find('all');
		$this->set('pageList',$pages);
	}
	
	function facebook_description()
	{
		$pages=Cache::read('content_pages');
		if($pages==null)
		{
			$this->Page->updateCache();
			$pages=Cache::read('content_pages');
		}
		$response=array();
		$response['type']="success";
		$response['message']=$pages['meta_description']['page_content'];
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function admin_edit($id=null)
	{
		$this->Page->id = $id;
		if(!empty($this->data))
		{
			if($this->Page->save($this->data))
			{
				$this->Page->updateCache();
				$this->Session->setFlash("Page detail updated successfully",'default',array('class'=>"success"));
				$this->redirect(array('action'=>"index"));
			}
			else
			{
				if(!empty($this->Page->validationErrors))
				{
					$this->set('errors',$this->Page->validationErrors);
					$this->Page->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Page->read();
		}
	}
}
