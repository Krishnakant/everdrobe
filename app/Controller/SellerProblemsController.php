<?php 
class SellerProblemsController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		if($this->Auth->user('type')!="admin")
		{
			$this->layout="sell_drobe";
		}
	}
	function admin_index()
	{
		$pending_request = $this->SellerProblem->find('count',array('conditions'=>array('SellerProblem.status'=>'pending')));
		$this->SellerProblem->bindModel(array('belongsTo'=>array('User')));
		$conditions=array('SellerProblem.status'=>"complete");
		$this->paginate = array(
				'fields'=>array('User.last_name','User.first_name','User.username','SellerProblem.*'),
				'limit' => 10,
				'order' => "SellerProblem.created_on DESC",
				'conditions'=>$conditions
		);
		$request_for_problem = $this->paginate('SellerProblem');
		
		$this->set('request_for_problem',$request_for_problem);
		$this->set('pending_problem',$pending_request);
	}
	
	
	/**
	 * List of request shown at admin side which are pending for new label request. 
	 */
	function admin_pending_problem_request()
	{
		$this->SellerProblem->bindModel(array('belongsTo'=>array('User')));
		$conditions=array('SellerProblem.status'=>"pending");
		$this->paginate = array(
				'fields'=>array('User.last_name','User.first_name','User.username','SellerProblem.*'),
				'limit' => 10,
				'order' => "SellerProblem.created_on DESC",
				'conditions'=>$conditions
		);
		$request_for_problem = $this->paginate('SellerProblem');
		$this->set(compact('request_for_problem'));
	}
	
	
	/**
	 * @param unknown_type $request_id
	 * admin will approve the pending request for label.
	 */
	function admin_approve_request($request_id = null)
	{
		if($request_id!=null)
		{
			$this->SellerProblem->updateAll(array('SellerProblem.status'=>"'complete'"),array('SellerProblem.id'=>$request_id));
			$this->Session->setFlash('Problem has been approve successfully','default',array('class'=>'success'));
			$this->redirect(array('action'=>'pending_problem_request'));
		}
		else
		{
			$this->Session->setFlash('Request for problem not approved','default',array('class'=>'error'));
			$this->redirect(array('action'=>'pending_problem_request'));
		}
	}
	
	
	function my_problem($sold_drobe_id = null)
	{
		if(! empty($this->data))
		{
			if(isset($this->data['SellerProblem']['problem_type']) && $this->data['SellerProblem']['problem_type']=='Delayed Shipment')
			{	
				//Check validation when problem_type is Dilayed Shipment
				$this->SellerProblem->setDelayShipmentValidation();
			}
			
			$this->SellerProblem->set($this->data);
			if($this->SellerProblem->validates())
			{
				$this->loadModel('SoldDrobe');
				$this->SoldDrobe->recursive =-1;
				$order_id = $this->SoldDrobe->field('order_id',array('SoldDrobe.id'=>$sold_drobe_id));
				$seller_problem_data = $this->data['SellerProblem'];
				if($this->data['SellerProblem']['problem_type']=='Delayed Shipment')
				{
					$seller_problem_data['expected_ship_date']= date('Y-m-d',strtotime($seller_problem_data['expected_ship_date']));
				}
				$seller_problem_data['user_id']=$this->Auth->user('id');
				$seller_problem_data['order_id']=$order_id;
				$this->SellerProblem->save($seller_problem_data);
				
				$this->loadModel('User');
				$this->User->recursive = -1;
				$user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
				
				/*
				 * Send Email to Admin for new user notification
				*
				*/
				
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$user_data['User']['username'],
						"##problem_type"=>$this->data['SellerProblem']['problem_type'],
						"##order_id"=>$order_id
				);
				if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
				{
					$variables["##full_name"]=$user_data['User']['username'];
				}
				else
				{
					$variables["##full_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
				}
				// sending mail after registration
				$this->sendNotifyMail('enquiery_from_seller', $variables,Configure::read('admin_email'),1);
				
				$this->Session->setFlash("Your problem has been sent to admin for approval",'default',array('class'=>'success'));
				$this->redirect(array('controller'=>'sell','action'=>'sold_drobes'));
			}
			else
			{
				if(!empty($this->SellerProblem->validationErrors))
				{
					//displaying errors in view line by line
					$this->set('errors',$this->SellerProblem->validationErrors);
					$this->SellerProblem->validationErrors=array();
				}
			}
		}
		
		//For getting information about drobe which is already sold.
		$fields = array('Drobe.comment','Drobe.file_name','SellDrobe.size_name','SoldDrobe.shipping_address','SoldDrobe.order_id','SoldDrobe.order_date','SoldDrobe.shipped_on','SoldDrobe.status'); 
		$this->loadModel('SoldDrobe');
		$this->SoldDrobe->recursive = -1;
		$drobe_data=$this->SoldDrobe->find('first',array(
				'fields'=>$fields,
				'conditions'=>array('SoldDrobe.id'=>$sold_drobe_id),
				'joins'=>array(
						array(
								'table' => 'sell_drobes',
								'alias' => 'SellDrobe',
								'type' => 'LEFT',
								'conditions' => array(
										'SellDrobe.id = SoldDrobe.sell_drobe_id',
								)
						),
						array(
								'table' => 'drobes',
								'alias' => 'Drobe',
								'type' => 'LEFT',
								'conditions' => array(
										'Drobe.id = SoldDrobe.drobe_id',
								)
						)
				)));
		$this->set('drobe_data',$drobe_data);
		
	}
	
	
	/**
	 * @param unknown_type $sold_drobe_id
	 * 
	 * Seller can send his problem for sold drobe and also he can do enquiry for that 
	 */
	function seller_problem_request($sold_drobe_id=null)
	{
		$response = array();
		if($sold_drobe_id != null)
		{
			if(!empty($this->data))
			{
				$this->loadModel('SoldDrobe');
				$this->SoldDrobe->recursive = -1;
				$sold_drobe_data = $this->SoldDrobe->find('first',array('fields'=>array('SoldDrobe.order_id','SoldDrobe.user_id'),'conditions'=>array('SoldDrobe.id'=>$sold_drobe_id)));
					
				$request_data = $this->data;
				if(isset($request_data['problem_type']) && $request_data['problem_type']=='Delayed Shipment')
				{
					$request_data['expected_ship_date'] = date('Y-m-d',strtotime($this->data['expected_ship_date']));
					$request_data['reason_for_delay'] = $this->data['reason_for_delay'];
				}
				$request_data['user_id'] = $sold_drobe_data['SoldDrobe']['user_id'];
				$request_data['order_id'] = $sold_drobe_data['SoldDrobe']['order_id'];
				$this->SellerProblem->save($request_data); // save request in to database table which data pass by seller @sadikhasan\
		
		
				$this->loadModel('User');
				$this->User->recursive = -1;
				$user_data = $this->User->find('first',array('fields'=>array('User.first_name','User.username','User.last_name'),'conditions'=>array('User.id'=>$request_data['user_id'])));
		
		
				/*
				 * Send Email to Admin for new user notification
				*
				*/
		
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$user_data['User']['username'],
						"##problem_type"=>$this->data['problem_type'],
						"##order_id"=>$request_data['order_id']
				);
				if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
				{
					$variables["##full_name"]=$user_data['User']['username'];
				}
				else
				{
					$variables["##full_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
				}
				// sending mail after registration
				$this->sendNotifyMail('enquiery_from_seller', $variables,Configure::read('admin_email'),1);
		
				$response['type'] = 'success';
				$response['message'] = 'Enquiry for drobe sent successfully';
			}
			else
			{
				$response['type'] = 'error';
				$response['message'] = 'Parameters are required';
			}
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Invalid parameter';
		}
		
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
}		
?>