<?php

class ChattingsController extends AppController
{
	var $helpers=array('Time');
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function index($friend_id=null)
	{
		$this->loadModel('User');
		$this->User->recursive=-1;
		$friend=$this->User->find('first',array(
					'fields'=>array("User.unique_id","User.first_name","User.last_name","User.username","User.photo"),
					'conditions'=>array("User.unique_id"=>$friend_id)
				));
		if($friend)
		{
			$this->set('friend',$friend['User']);
		}
		else
		{
			$this->set('invalid_friend',true);
		}
		$this->set('title_for_layout', "Chatting with ".$friend['User']['first_name']." ".$friend['User']['last_name'] );
	}
	function view($friend_id=null)
	{
		$this->loadModel('User');
		$this->User->recursive=-1;
		$friend=$this->User->find('first',array(
					'fields'=>array("User.id","User.unique_id","User.first_name","User.last_name","User.photo"),
					'conditions'=>array("User.unique_id"=>$friend_id)
				));
		$this->set('friend',$friend['User']);
		$this->set('me',$this->Auth->user());
		$chattings=$this->Chatting->find('all',array('conditions'=>array(
								'OR'=>array(
								array('AND'=>array('Chatting.poster' => $friend['User']['id'],'Chatting.receiver'=>$this->Auth->user('id'))),
								array('AND'=>array('Chatting.poster' => $this->Auth->user('id'),'Chatting.receiver'=>$friend['User']['id']))
								)
							),"order"=>"Chatting.created_on ASC"));
		$this->set('chattings',$chattings);
	}
	function add($friend_id=null)
	{
		
		$response=array();
		if(!empty($this->data) || $this->data['message']!=null)
		{
			$this->loadModel('User');
			$this->User->recursive=-1;
			$friend=$this->User->find('first',array(
								'fields'=>array("User.id","User.first_name","User.last_name","User.photo"),
								'conditions'=>array("User.unique_id"=>$friend_id)
			));
			
			if($this->Chatting->save(array('receiver'=>$friend['User']['id'],'poster'=>$this->Auth->user('id'),'message'=>$this->data['message'])))
			{
				$response['type']="success";
				$response['message']="Message posted successfully on this chatting";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in post your message in this chatting";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid data";
		}
		echo json_encode($response);
		exit();
	}
	function close()
	{
		$response=array();
		if(!empty($this->data) || $this->data['friend_id']!=null)
		{
			$this->loadModel('User');
			$this->User->recursive=-1;
			$friend=$this->User->find('first',array(
										'fields'=>array("User.id","User.first_name","User.last_name","User.photo"),
										'conditions'=>array("User.unique_id"=>$this->data['friend_id'])
			));
				
			if($this->Chatting->deleteAll(array(
								'OR'=>array(
								array('AND'=>array('Chatting.poster' => $friend['User']['id'],'Chatting.receiver'=>$this->Auth->user('id'))),
								array('AND'=>array('Chatting.poster' => $this->Auth->user('id'),'Chatting.receiver'=>$friend['User']['id']))
								))))
			{
				$response['type']="success";
				$response['message']="Chatting closed successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in close this chatting";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid data";
		}
		echo json_encode($response);
		exit();
	}
}
?>