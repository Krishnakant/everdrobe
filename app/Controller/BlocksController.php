<?php 
class BlocksController extends AppController
{
	//var $scaffold;
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		$this->paginate = array(
			'limit' => 15
		);
		
		$data = $this->paginate('Block');
		$this->set(compact('data'));
	}
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->Block->save($this->data))
			{
				$this->Session->setFlash("Ip address added  in block list successfully.",'default',array('class'=>"success"));
				$this->Block->_updateBlockCache();
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				$this->Session->setFlash("Error occured due to validations");
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Block->id=$id;
		if(!empty($this->data))
		{
			$this->Block->validate['ip_address']['isUnique']['on']='update';
			if($this->Block->save($this->data))
			{
				$this->Session->setFlash("Blocked IP address changed successfully.",'default',array('class'=>"success"));
				$this->Block->_updateBlockCache();
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				$this->Session->setFlash("Error occured due to validations");
			}
		}
		else
		{
			$this->data=$this->Block->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if($this->Block->delete($id))
			{
				$this->Session->setFlash("Selected Ip address removed from block list successfully",'default',array('class'=>"success"));
				$this->Block->_updateBlockCache();
			}
			else
			{
				$this->Session->setFlash("Error occured in delete category");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	function getlist()
	{
		$block_ips=$this->Block->find('all');
		$data=array();
		foreach($block_ips as $ip)
		{
			$data[]=$ip['Block'];
		}
		$response=array();
		if(count($data)==0)
		{
			$response['type']="error";
			$response['message']="Block ip address not found";
		}
		else 
		{
			$response['type']="success";
			$response['block_ip']=$data;
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>