<?php 
class SellerPaymentsController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		if($this->Auth->user('type')!="admin")
		{
			$this->layout="sell_drobe";
		}
	}
	function admin_payment_tracking()
	{
		$this->helpers[]="Time";
		$conditions=array();
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.username LIKE']="@"."%".$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['SellProfile.full_name LIKE']="%".$search;
		}

		$this->paginate = array(
				'fields'=>array("SellerPayment.*","SellProfile.*","User.first_name","User.username","User.last_name"),
				'conditions'=>$conditions,
				'joins'=>array(
						array(
								'table' => 'sell_profiles',
								'alias' => 'SellProfile',
								'type' => 'LEFT',
								'conditions' => array(
										'SellerPayment.user_id = SellProfile.user_id'
								)),
						array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'LEFT',
								'conditions' => array(
										'SellerPayment.user_id = User.id'
								))
				)
		);

		$paymentData = $this->paginate('SellerPayment');
		$this->set('paymentData',$paymentData);
	}
	function admin_pay($user_id,$request_id=null)
	{
		if(!empty($this->data))
		{
			// loading total earnings for seller
			$this->loadModel('SellDrobe');
			$this->SellDrobe->bindModel(array('belongsTo'=>array("Drobe")));
			$earningsData=$this->SellDrobe->find('all',array('fields'=>array('sum(total_paid) as total_earnings'),'conditions'=>array('Drobe.user_id'=>$user_id)));
			$earning = $earningsData[0][0]['total_earnings'];
			$earning  = number_format((float)$earning, 2, '.', '');
			$payment = $this->data['SellerPayment']['payment'];
			$paid = $this->SellerPayment->find('all',array(
					'fields'=>'sum(SellerPayment.payment) as paid',
					'conditions'=>array('SellerPayment.user_id'=>$user_id),
					'group'=>"SellerPayment.user_id")
			);
			$paid_to_user = number_format((float)$paid[0][0]['paid'], 2, '.', '');
			$already_give_payment = ($paid_to_user!="")?$paid_to_user:0;
			$difference  = $earning - $already_give_payment;
			$difference  = number_format((float)$difference, 2, '.', '');
			
			$arr = array('Difference'=>$difference,'payment'=>$this->data['SellerPayment']['payment'],'earning'=>$earning,'alreadyGiven'=>$already_give_payment);
			$this->log("PaymentLog".print_r($arr,true),"PaymentTrack");
			if($this->data['SellerPayment']['payment']!="")
			{
				if(is_numeric($this->data['SellerPayment']['payment']))
				{
					if($this->data['SellerPayment']['payment']>0)
					{
						if($earning>$already_give_payment)
						{
							if($difference>=$payment)
							{
								if($this->SellerPayment->save($this->data))
								{
									$this->loadModel("SellProfile");
									$updateField=array(
											"SellProfile.total_paid"=>"SellProfile.total_paid + ".$this->data['SellerPayment']['payment']
									);

									$this->SellProfile->updateAll($updateField,array("user_id"=>$this->data['SellerPayment']['user_id']));
									$sell_profile_data = $this->SellProfile->find('first',array('conditions'=>array('SellProfile.user_id'=>$user_id)));

									/* Deduct from requested_amount only if any seller requested for redeem balance */
									if($sell_profile_data['SellProfile']['requested_amount']>0 && $request_id!=null)
									{
										$updateField=array( "SellProfile.requested_amount"=>0 );
										$this->SellProfile->updateAll($updateField,array("user_id"=>$this->data['SellerPayment']['user_id']));
									}

									/*Admin will paid using redeem request then one entry must in RedeemRequest table. */
									if($request_id!=null)
									{
										$this->loadModel('RedeemRequest');
										$updateField=array(
												"RedeemRequest.status"=>"'paid'",
												"RedeemRequest.paid_amount"=>$this->data['SellerPayment']['payment']
										);
										$this->RedeemRequest->updateAll($updateField,array("RedeemRequest.id"=>$request_id));
									}

									//Getting address of seller who sold drobe and this address sent to admin in email for new shipping_label_request
									$address = $sell_profile_data['SellProfile']['street1'].', '.
											$sell_profile_data['SellProfile']['street2'].', '.
											$sell_profile_data['SellProfile']['city'].', '.
											$sell_profile_data['SellProfile']['state'].', '.
											$sell_profile_data['SellProfile']['zip'];

									$this->loadModel('User');
									$this->User->recursive = -1;
									$user_data = $this->User->find('first',array('fields'=>array('User.email'), 'conditions'=>array('User.id'=>$user_id)));
									/*
									 * Send Email to Seller when payment has been give by admin to seller
									*
									*/
									$request_number = ($this->data['SellerPayment']['request_id']!='')?$this->data['SellerPayment']['request_id']:"N/A";
									// setting up variables for email

									$this->loadModel('User');
									$userdata = $this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
									 $username= $userdata['User']['username'];
									$full_name=$userdata['User']['first_name']." ".$userdata['User']['last_name'];

									$variables=array(
											//"##full_name"=>$username,
											"##amount"=>$this->data['SellerPayment']['payment'],
											"##request_number"=>$request_number
									);
									if(isset($username) && $username!="")
									{
										$variables["##full_name"]=$username;
									}
									else
									{
										$variables["##full_name"]=$full_name;
									}
									// sending mail to the admin after payment has been given
									$this->sendNotifyMail('payment_given_to_seller', $variables,$user_data['User']['email'],1);


									$this->Session->setFlash("Payment tracking saved successfully","default",array("class"=>"success"));
									$this->redirect($this->referer());
								}
							}
							else
							{
								$this->Session->setFlash("Payment tracking failed insufficient balance.","default",array("class"=>"error"));
								$this->redirect($this->referer());
							}
						}
						else
						{
							$this->Session->setFlash("Payment tracking failed insufficient balance.","default",array("class"=>"error"));
							$this->redirect($this->referer());
						}
					}
					else
					{
						$this->Session->setFlash("Please enter amount greater than 0.","default",array("class"=>"error"));
						$this->redirect($this->referer());
					}
				}

				else
				{
					$this->Session->setFlash("Please enter numeric amount.","default",array("class"=>"error"));
					$this->redirect($this->referer());
				}
			}
			else
			{
				$this->Session->setFlash("Please enter amount.","default",array("class"=>"error"));
				$this->redirect($this->referer());
			}
		}
		$this->helpers[]="Time";
		// loading seller data
		$this->loadModel('SellProfile');
		$this->SellProfile->bindModel(array('belongsTo'=>array("User")));
		$sellerData=$this->SellProfile->find('first',array('conditions'=>array('user_id'=>$user_id)));
		// loading total earnings for seller
		$this->loadModel('SellDrobe');
		$this->SellDrobe->bindModel(array('belongsTo'=>array("Drobe")));
		$earningsData=$this->SellDrobe->find('all',array('fields'=>array('sum(total_paid) as total_earnings'),'conditions'=>array('Drobe.user_id'=>$user_id)));
		$this->set('earningsData',$earningsData[0][0]);
		$this->set('sellerData',$sellerData);

		if($request_id!=null)
		{
			$this->loadModel('RedeemRequest');
			$redeem_data = $this->RedeemRequest->find('first',array('conditions'=>array('RedeemRequest.id'=>$request_id)));
			$this->set('payment',$redeem_data['RedeemRequest']['requested_amount']);
			$this->set('request_id',$redeem_data['RedeemRequest']['id']);
		}
		else
		{
			$this->set('payment',"");
			$this->set('request_id',"");
		}

		// loading payment history
		$this->set('paymentHistory',$this->SellerPayment->find('all',array('conditions'=>array('SellerPayment.user_id'=>$user_id),'order'=>"SellerPayment.id DESC")));
	}

	function admin_get_amount($user_id=null,$request_id=null)
	{
		$this->loadModel('RedeemRequest');
		$redeem_data = $this->RedeemRequest->find('first',array('conditions'=>array('RedeemRequest.id'=>$request_id,'RedeemRequest.user_id'=>$user_id)));
		if($redeem_data)
		{
			echo $redeem_data['RedeemRequest']['requested_amount'];
		}
		else
		{
			echo "";
		}
		exit;
	}
	
	function admin_history($user_id=null)
	{
		$this->helpers[]="Time";
		$payment_history = array();
		if($user_id!=null)
		{
			 $history= $this->SellerPayment->find('all',array('conditions'=>array('SellerPayment.user_id'=>$user_id)));
			 foreach ($history as $pay_history)
			 {
			 	$payment_history[] = $pay_history['SellerPayment'];
			 }
		}
		$this->set('payment_history',$payment_history);
	}
	
}
?>