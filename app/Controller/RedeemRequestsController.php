<?php 
class RedeemRequestsController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		if($this->Auth->user('type')!="admin")
		{
			$this->layout="sell_drobe";
		}
	}

	/**
	 *  List of request which status is completed from admin side
	 */
	function admin_index()
	{
		//$pending_request = $this->RedeemRequest->find('count',array('conditions'=>array('RedeemRequest.status'=>'pending')));

		$this->RedeemRequest->bindModel(array('belongsTo'=>array('User')));		//$conditions=array('RedeemRequest.status'=>"complete");
		$this->paginate = array(
				'fields'=>array('User.id','User.last_name','User.first_name','User.username','RedeemRequest.*'),
				'limit' => 10,
				'order' => "RedeemRequest.created_on DESC"
		);
		$request_for_label = $this->paginate('RedeemRequest');
		$this->set('request_for_redeem_balance',$request_for_label);
		//$this->set('pending_request',$pending_request);
		
	}


	/**
	 * List of request shown at admin side which are pending for new label request.
	 */
	function admin_pending_redeem_request()
	{
		$this->RedeemRequest->bindModel(array('belongsTo'=>array('User')));
		$conditions=array('RedeemRequest.status'=>"pending");
		$this->paginate = array(
				'fields'=>array('User.id','User.last_name','User.first_name','User.username','RedeemRequest.*'),
				'limit' => 10,
				'order' => "RedeemRequest.created_on DESC",
				'conditions'=>$conditions
		);
		$request_for_redeem = $this->paginate('RedeemRequest');
		$this->set(compact('request_for_redeem'));
	}



	/**
	 * @param unknown_type $request_id
	 * admin will approve the pending request for label.
	 */
	function admin_approve_request($request_id = null)
	{
		if($request_id!=null)
		{
			$this->RedeemRequest->updateAll(array('RedeemRequest.status'=>"'complete'"),array('RedeemRequest.id'=>$request_id));
			$this->Session->setFlash('Request for redeem approve successfully','default',array('class'=>'success'));
			$this->redirect(array('action'=>'pending_redeem_request'));
		}
		else
		{
			$this->Session->setFlash('Request for redeem not approved','default',array('class'=>'error'));
			$this->redirect(array('action'=>'pending_redeem_request'));
		}
	}

	/**
	 *  Seller can request to admin for redeemable balance.
	 */
	function my_redeem_request()
	{
		$this->loadModel('SellProfile');
		$user = $this->SellProfile->findByUserId($this->Auth->user('id'));
		$profile_data['user_id'] = $this->Auth->user('id');

		if(!empty($this->data))
		{
			$profile_data['payment_type'] = $this->data['SellProfile']['payment_type'];
			if($this->data['SellProfile']['payment_type']=="cheque")
			{
				$profile_data['full_name'] = $this->data['SellProfile']['full_name'];
				$profile_data['street1'] = $this->data['SellProfile']['street1'];
				$profile_data['street2'] = $this->data['SellProfile']['street2'];
				$profile_data['city'] = $this->data['SellProfile']['city'];
				$profile_data['state'] = $this->data['SellProfile']['state'];
				$profile_data['zip'] = $this->data['SellProfile']['zip'];
				$requested_data['user_name'] = $this->data['SellProfile']['full_name'];
			}
			else if($this->data['SellProfile']['payment_type']=="deposit")
			{
				$profile_data['deposit_name'] = $this->data['SellProfile']['deposit_name'];
				$profile_data['account_no'] = $this->data['SellProfile']['account_no'];
				$profile_data['routing_no'] = $this->data['SellProfile']['routing_no'];
				$profile_data['bank_name'] = $this->data['SellProfile']['bank_name'];
				$profile_data['account_type'] = $this->data['SellProfile']['account_type'];
			}
			else
			{
				$profile_data['paypal_email'] = $this->data['SellProfile']['paypal_email'];
			}
				
			if($profile_data['payment_type']=="cheque")
			{
				$fields = array('full_name','street1','city','state','zip');
				unset($this->SellProfile->validate['paypal_email']);
			}
			else if($profile_data['payment_type']=="deposit")
			{
				$fields = array('deposit_name','account_no','routing_no','bank_name');
				unset($this->SellProfile->validate['paypal_email']);
			}
			else
			{
				$fields = array('paypal_email');
			}
				
			$this->SellProfile->set($profile_data);
				
			if($this->SellProfile->validates(array('fieldList'=>$fields)))
			{
				if($this->data['SellProfile']['payment_type']=="cheque")
				{
					if($user)
					{
						$this->SellProfile->updateAll(
								array(
										'SellProfile.street1'=>"'".$profile_data['street1']."'",
										'SellProfile.street2'=>"'".$profile_data['street2']."'",
										'SellProfile.city'=>"'".$profile_data['city']."'",
										'SellProfile.state'=>"'".$profile_data['state']."'",
										'SellProfile.zip'=>"'".$profile_data['zip']."'",
										'SellProfile.full_name'=>"'".$profile_data['full_name']."'",
										'SellProfile.payment_type'=>"'".$profile_data['payment_type']."'"
								),
								array('SellProfile.user_id'=>$this->Auth->user('id'))
						);
					}
					else
					{
						$this->SellProfile->save($profile_data);
					}
				}
				else if($this->data['SellProfile']['payment_type']=="deposit")
				{
					if($user)
					{
						$this->SellProfile->updateAll(
								array(
										'SellProfile.deposit_name'=>"'".$profile_data['deposit_name']."'",
										'SellProfile.account_no'=>"'".$profile_data['account_no']."'",
										'SellProfile.routing_no'=>"'".$profile_data['routing_no']."'",
										'SellProfile.bank_name'=>"'".$profile_data['bank_name']."'",
										'SellProfile.account_type'=>"'".$profile_data['account_type']."'",
										'SellProfile.payment_type'=>"'".$profile_data['payment_type']."'"
								),
								array('SellProfile.user_id'=>$this->Auth->user('id'))
						);
					}
					else
					{
						$this->SellProfile->save($profile_data);
					}
				}
					
				else
				{
					if($user)
					{
						$this->SellProfile->updateAll(
								array(
										'SellProfile.paypal_email'=>"'".$profile_data['paypal_email']."'",
										'SellProfile.payment_type'=>"'".$profile_data['payment_type']."'"
								),
								array('SellProfile.user_id'=>$this->Auth->user('id'))
						);
					}
					else
					{
						$this->SellProfile->save($profile_data);
					}
				}

				$requested_data['payment_type']=$this->data['SellProfile']['payment_type'];
				$requested_data['requested_amount']=$this->data['RedeemRequest']['requested_amount'];
				$requested_data['user_id']=$this->Auth->user('id');

				if($this->RedeemRequest->save($requested_data))
				{
					/*	Update Redem request balance to requested_amount field in sell_profile table */
					$updateField=array("SellProfile.requested_amount"=>"SellProfile.requested_amount + ".$this->data['RedeemRequest']['requested_amount']);
					$this->SellProfile->updateAll($updateField,array("user_id"=>$this->Auth->user('id')));
						
					$this->loadModel('User');
					$this->User->recursive = -1;
					$user_data = $this->User->find('first',array('fields'=>array('User.first_name','User.username','User.last_name','User.email'),'conditions'=>array('User.id'=>$this->Auth->user('id'))));
					$this->SellProfile->recursive = -1;
					$sell_profile_data = $this->SellProfile->find('first',array('conditions'=>array('SellProfile.user_id'=>$this->Auth->user('id'))));
					//Getting address of seller who sold drobe and this address sent to admin in email for new shipping_label_request
					$address = $sell_profile_data['SellProfile']['street1'].', '.
							$sell_profile_data['SellProfile']['street2'].', '.
							$sell_profile_data['SellProfile']['city'].', '.
							$sell_profile_data['SellProfile']['state'].', '.
							$sell_profile_data['SellProfile']['zip'];
					/*
					 * Send Email to Admin for new user notification
					*/
					// setting up variables for email
					$variables=array(
							//"##full_name"=>$user_data['User']['username'],
							"##amount"=>$this->data['RedeemRequest']['requested_amount'],
							"##payment_type"=>$this->data['SellProfile']['payment_type'],
							"##address"=>$address,
							"##email"=>$this->data['SellProfile']['paypal_email']
					);
					if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
					{
						$variables["##full_name"]=$user_data['User']['username'];
					}
					else
					{
						$variables["##full_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
					}
					// sending mail to the admin after request sent
					$this->sendNotifyMail('redeem_request_to_admin', $variables,Configure::read('admin_email'),1);

					// sending mail to the Seller after request sent
					$this->sendNotifyMail('redeem_request_to_seller',$variables, $user_data['User']['email'],1);
					$this->Session->setFlash("Your redeemption request has been sent successfully",'default',array('class'=>"success"));
				}
				else
				{
					$this->Session->setFlash("Error occured in sending request",'default');
				}
			}
			else
			{
				if(!empty($this->SellProfile->validationErrors))
				{
					$this->set('errors',$this->SellProfile->validationErrors);
					$this->SellProfile->validationErrors=array();
				}
			}
		}

		$this->loadModel('SellProfile');
		$profile_data = $this->SellProfile->findByUserId($this->Auth->user('id'));
		$this->SellProfile->id = $profile_data['SellProfile']['id'];
		$this->data = $this->SellProfile->read();

		//Setting redeemable account by default when user can request for it
		$this->SellProfile->recursive = -1;
		$total_paid_to_seller = $this->SellProfile->field('SellProfile.total_paid',array('SellProfile.user_id'=>$this->Auth->user('id')));
		$total_paid_to_seller = number_format((float)$total_paid_to_seller, 2, '.', '');
		$this->loadModel('SellDrobe');
		/* $redeemable_balance = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
		 array('SoldDrobe.user_id'=>$this->Auth->user('id'),'SoldDrobe.status'=>'shipped','TIMESTAMPDIFF(HOUR,SoldDrobe.shipped_on,now()) > '=>72)); */
		$redeemable_balance = $this->SellDrobe->SoldDrobe->field('sum(SoldDrobe.paid_amount)',
				array('SoldDrobe.user_id'=>$this->Auth->user('id'),'SoldDrobe.status'=>'shipped'));
		
		$redeemable_balance  = number_format((float)$redeemable_balance, 2, '.', '');
		$redeemable_balance = $redeemable_balance - $total_paid_to_seller;  //Subtract paid_to_seller balance which is already given to seller from redeemableBalance
		$redeemable_balance  = number_format((float)$redeemable_balance, 2, '.', '');
		//$redeemable_balance=$redeemable_balance."";
		
		/*    Requested amount by seller for Redeemable balance. */
		$requested_amount = $profile_data['SellProfile']['requested_amount'];
		if($redeemable_balance>0)
		{
			$redeemable_balance= $redeemable_balance - $requested_amount;
		}
		
		/* It displays two decimal point after amount. */
		$redeemable_balance  = number_format((float)$redeemable_balance, 2, '.', '');
		$this->set('redeem_balance',$redeemable_balance);
	}


	/**
	 * This webservice used for request from seller to admin for redeemable balance.
	 */
	function redeem_request($user_id=null)
	{
		if($user_id != null)
		{
			$this->loadModel('SellProfile');
			$user = $this->SellProfile->findByUserId($user_id);
				
			$profile_data['user_id'] = $user_id;
			if($this->data['payment_type']=="cheque")
			{
				$profile_data['street1'] = $this->data['street1'];
				$profile_data['street2'] = ($this->data['street2']!="")?$this->data['street2']:"";
				$profile_data['city'] = $this->data['city'];
				$profile_data['state'] = $this->data['state'];
				$profile_data['zip'] = $this->data['zip'];
				$profile_data['full_name'] = $this->data['user_name'];
				$requested_data['user_name'] = $this->data['user_name'];
				/*  If user already existed then update user_profile in sell_profile table.  */
				if($user)
				{
					$this->SellProfile->updateAll(
							array(
									'SellProfile.street1'=>"'".$profile_data['street1']."'",
									'SellProfile.street2'=>"'".$profile_data['street2']."'",
									'SellProfile.city'=>"'".$profile_data['city']."'",
									'SellProfile.state'=>"'".$profile_data['state']."'",
									'SellProfile.zip'=>"'".$profile_data['zip']."'",
									'SellProfile.full_name'=>"'".$profile_data['full_name']."'",
									'SellProfile.payment_type'=>"'".$profile_data['payment_type']."'"
							),
							array('SellProfile.user_id'=>$user_id)
					);
				}
				else
				{
					unset($this->SellProfile->validate['paypal_email']);
					$this->SellProfile->save($profile_data);
				}
			}
			else if($this->data['payment_type']=="deposit")
			{
				$profile_data['payment_type'] = $this->data['payment_type'];
				$profile_data['deposit_name'] = $this->data['deposit_name'];
				$profile_data['account_no'] = $this->data['account_no'];
				$profile_data['routing_no'] = $this->data['routing_no'];
				$profile_data['bank_name'] = $this->data['bank_name'];
				$profile_data['account_type'] = $this->data['account_type'];
				if($user)
				{
					$this->SellProfile->updateAll(
							array(
									'SellProfile.deposit_name'=>"'".$profile_data['deposit_name']."'",
									'SellProfile.account_no'=>"'".$profile_data['account_no']."'",
									'SellProfile.routing_no'=>"'".$profile_data['routing_no']."'",
									'SellProfile.bank_name'=>"'".$profile_data['bank_name']."'",
									'SellProfile.account_type'=>"'".$profile_data['account_type']."'",
									'SellProfile.payment_type'=>"'".$profile_data['payment_type']."'"
							),
							array('SellProfile.user_id'=>$user_id)
					);
				}
				else
				{
					unset($this->SellProfile->validate['paypal_email']);
					$this->SellProfile->save($profile_data);
				}

			}
			else
			{
				$profile_data['payment_type'] = $this->data['payment_type'];
				$profile_data['paypal_email'] = $this->data['paypal_email'];
				if($user)
				{
					$this->SellProfile->updateAll(
							array(
									'SellProfile.paypal_email'=>"'".$this->data['paypal_email']."'",
									'SellProfile.payment_type'=>"'".$this->data['payment_type']."'"
							),
							array('SellProfile.user_id'=>$user_id)
					);
				}
				else
				{
					$this->SellProfile->save($profile_data);
				}
			}
				
				
				
			$requested_data['payment_type']=$this->data['payment_type'];
			$requested_data['requested_amount']=$this->data['requested_amount'];
			$requested_data['user_id']=$user_id;
			//set Sell Profile id according to user id if profile is present of that user.
				
			if($this->RedeemRequest->save($requested_data))
			{
				/*	Update Redem request balance to requested_payment field */
				$updateField=array("SellProfile.requested_amount"=>"SellProfile.requested_amount + ".$this->data['requested_amount']);
				$this->SellProfile->updateAll($updateField,array("user_id"=>$user_id));
					
				$this->loadModel('User');
				$this->User->recursive = -1;
				$user_data = $this->User->find('first',array('fields'=>array('User.first_name','User.username','User.last_name','User.email'),'conditions'=>array('User.id'=>$user_id)));
					
				$address="";
				if($this->data['payment_type']=="cheque")
				{
					$this->SellProfile->recursive = -1;
					$sell_profile_data = $this->SellProfile->find('first',array('conditions'=>array('SellProfile.user_id'=>$user_id)));

					//Getting address of seller who sold drobe and this address sent to admin in email for new shipping_label_request
					$address = $sell_profile_data['SellProfile']['street1'].', '.
							$sell_profile_data['SellProfile']['street2'].', '.
							$sell_profile_data['SellProfile']['city'].', '.
							$sell_profile_data['SellProfile']['state'].', '.
							$sell_profile_data['SellProfile']['zip'];
				}
				/*
				 * Send Email to Admin for new user notification
				*
				*/
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$user_data['User']['username'],
						"##amount"=>$this->data['requested_amount'],
						"##payment_type"=>$this->data['payment_type'],
						"##address"=>$address,
						"##email"=>$this->data['paypal_email']
				);
				if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
				{
					$variables["##full_name"]=$user_data['User']['username'];
				}
				else
				{
					$variables["##full_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
				}
				// sending mail to the admin after request sent
				$this->sendNotifyMail('redeem_request_to_admin', $variables,Configure::read('admin_email'),1);
					
				// sending mail to the Seller after request sent
				$this->sendNotifyMail('redeem_request_to_seller',$variables, $user_data['User']['email'],1);
					
				$response['type']= 'success';
				$response['message']='Redeem request sent successfully to admin for approval';
			}
			else
			{
				$response['type']= 'error';
				$response['message']='Error occur during send redeem request';
			}
				
			/* }
			 else
			{
			$response['type']='error';
			$response['message'] = 'First update your profile detail.';
			} */
		}
		else
		{
			$response['type']='error';
			$response['message'] = 'Invalid parameter.';
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}


	//This webservice get message from admin side in setting table for payment type is other
	function payment_type_other()
	{
		$response['type'] = 'success';
		$response['message'] = Configure::read("setting.payment_type_other");

		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}


	/* Admin can cancel redeem request. */
	function admin_cancel_request($user_id=null,$request_id=null)
	{
		if($user_id!=null && $request_id!=null)
		{
			/* Get requested amount by seller. */
			$requested_data = $this->RedeemRequest->find('first',array(
					'fields'=>array('RedeemRequest.requested_amount'),
					'conditions'=>array('RedeemRequest.user_id'=>$user_id,'RedeemRequest.id'=>$request_id)
			));
			$requested_amount = $requested_data['RedeemRequest']['requested_amount'];
			
			/* Decrese requested amount from sell_profile table which is added when seller do request for redeem balance. */
			$this->loadModel('SellProfile');
			$updateField=array("SellProfile.requested_amount"=>"Round(SellProfile.requested_amount,2) - ".$requested_amount);
			$this->SellProfile->updateAll($updateField,array("user_id"=>$user_id));
				
			/* Delete canceled request from redeem_request table when admin cancel request. */
			$this->RedeemRequest->id = $request_id;
			if($this->RedeemRequest->delete())
			{
				$this->Session->setFlash("Request cancel successfully!",'default');
			}
			else
			{
				$this->Session->setFlash("Error occured in cancel request ",'default',array('class'=>'error'));
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameter to cancel request",'default',array('class'=>'error'));
		}
		$this->redirect($this->referer());
	}

}
?>