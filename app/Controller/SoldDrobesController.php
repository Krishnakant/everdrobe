<?php 
/*
 * API Key 	31ed571f43500f83f0197854b1259eb2
 * Password 	6b37f814f32f47548dc015d05032d255
 * Shared Secret 	8ec99bd0b14c4964a3c0acf3f76fc533
 * URL Format 	https://apikey:password@hostname/admin/resource.xml
 * Example URL 	https://31ed571f43500f83f0197854b1259eb2:6b37f814f32f47548dc015d05032d255@everdrobe.myshopify.com/admin/orders.xml
 */
class SoldDrobesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		if($this->Auth->user('type')!="admin")
		{
			$this->layout="sell_drobe";
		}
	}
	function admin_index($user_id=null)
	{
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
		
	
		$this->helpers[]='Time';
		//$this->loadModel('Drobe');
		$this->SoldDrobe->bindModel(array('belongsTo'=>array("Drobe","SellDrobe",'User')));
	
		$conditions=array();
		if($user_id!=null)
		{
			$conditions['SoldDrobe.user_id']=$user_id;
			$this->loadModel('User');
			$this->User->recursive=0;
			$this->set('userData',$this->User->findById($user_id));
		}
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.id']=$search;
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['SellDrobe.sell_brand_name LIKE']="%".$search;
			//$conditions['OR']['Category.category_name LIKE']=$search;
		}
	
		$this->paginate = array(
				'fields'=>array("Drobe.*","User.email","User.id","SellDrobe.*","SoldDrobe.*"),
				'conditions'=>$conditions,
				'order'=>"SoldDrobe.id DESC"
		);
		
		$drobeData = $this->paginate('SoldDrobe');
		$this->loadModel("Notify");
		foreach ($drobeData as $key=>$drobe)
		{
			$variables = $this->admin_send_mail_to_seller($drobe['SoldDrobe']['id']);
			$data=$this->Notify->get_mail_data('mail_to_seller');
			
			// replacing variables in appropriate place in subject line and mail body
			$mail_sub=str_replace(array_keys($variables), $variables, $data['Notify']['subject']);
			$mail_message=str_replace(array_keys($variables), $variables, $data['Notify']['message']);
			$mail_message=eregi_replace("[\]",'',$mail_message);
			
			$drobeData[$key]['SoldDrobe']['subject']=$mail_sub;
			
			$drobeData[$key]['SoldDrobe']['body']=str_replace("+", " ",urlencode($mail_message));
			
		}
		
		
		$this->set('drobes',$drobeData);
	}
	function index()
	{
		if(Configure::read('setting.drobe_sale')!="on")
		{
			// if sell drobe feature is off from admin
			throw new NotFoundException("Not found");
		}
		
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
		
		$this->helpers[]='Time';
		//$this->loadModel('Drobe');
		$this->SoldDrobe->bindModel(array('belongsTo'=>array("Drobe","SellDrobe")));
		
		$conditions=array('SoldDrobe.user_id'=>$this->Auth->user('id'));
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['Drobe.comment LIKE']="%".$search;
			$conditions['OR']['SellDrobe.sell_brand_name LIKE']="%".$search;
			//$conditions['OR']['Category.category_name LIKE']=$search;
		}
		
		$this->paginate = array(
				'fields'=>array("Drobe.*","SellDrobe.*","SoldDrobe.*"),
				'conditions'=>$conditions,
				'order'=>"SoldDrobe.id DESC"
		);
		$drobeData = $this->paginate('SoldDrobe');
		$this->set('drobes',$drobeData);
	}
	function admin_submit_tracking_number()
	{
		$this->setAction('submit_tracking_number');
	}
	function submit_tracking_number()
	{
		$response=array();
		if(!empty($this->data))
		{
			$this->SoldDrobe->id=$this->data['sold_drobe_id'];
			if($this->SoldDrobe->saveField('tracking_number',$this->data['tracking_number']))
			{
				$this->Session->setFlash("Tracking number updated successfully","default",array("class"=>"success"));
				$response['type']="success";
				$response['message']="Tracking number updated successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in update tracking number, pleas try again later.";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid parameters";
		}
		echo json_encode($response);
		exit();
	}
	
	function profile()
	{
		
	}
	
	
	
	
	//This webservice for Sold Drobe List
	function sold_drobe_list($userid=null)
	{
		if($userid!=null)
		{
			/*$this->loadModel('Drobe');
			$this->Drobe->unbindModel(array('hasMany'=>array("Rate","Flag","Favourite")));*/
			$fields = array('Drobe.id','Drobe.file_name','Drobe.comment','Drobe.total_in','Drobe.total_out','Drobe.total_favs','Drobe.featured',
							'Drobe.post_type','Drobe.buy_url','SellDrobe.sell_brand_name','SellDrobe.sell_price','SellDrobe.original_sell_price',
							'SellDrobe.sell_description','SellDrobe.size_name','SellDrobe.sell_gender','Category.category_name','Category.id',
							'SoldDrobe.order_id','SoldDrobe.id as sold_drobe_id','SoldDrobe.sell_price','SoldDrobe.paid_amount','SoldDrobe.tracking_number',
							'SoldDrobe.shipping_address','SoldDrobe.sold_on','SoldDrobe.shipped_on','SoldDrobe.status','SoldDrobe.order_date',
							"User.first_name","User.last_name","User.username","User.photo");
			$joins = array(
						array(
								'table'=>'categories',
								'alias'=>'_Category',
								'type'=>'right',
								'conditions'=> array('Drobe.category_id = _Category.id')
							),
					    array(
					    		'table'=>'sold_drobes',
					    		'alias'=>'SoldDrobe',
					    		'type'=>'right',
					    		'conditions'=> array('Drobe.id = SoldDrobe.drobe_id')
					    	)
					);
			
				$this->loadModel('Drobe');
				$this->Drobe->unbindModel(array('hasMany'=>array("Rate","Flag","Favourite")));
				$data = $this->Drobe->find('all',array(						
						'fields'=>$fields,
						'conditions'=>array('SoldDrobe.user_id'=>$userid),
						'order'=>'SoldDrobe.id DESC',
						'joins'=>$joins
				));
			
			foreach ($data as $drobeinfo)
			{
				$drobeinfo['Drobe']['username'] =  $drobeinfo['User']['username'];
				$drobeinfo['Drobe']['first_name'] = $drobeinfo['User']['first_name'];
				$drobeinfo['Drobe']['last_name'] = $drobeinfo['User']['last_name'];
				$drobeinfo['Drobe']['profile_photo']="";
				if($drobeinfo['User']['photo']!="") {
					$drobeinfo['Drobe']['profile_photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$drobeinfo['User']['photo'],true);
				}
					
				unset($drobeinfo['User']);
				
				$drobeinfo['Drobe']['sold_drobe_id'] = $drobeinfo['SoldDrobe']['sold_drobe_id'];
				$drobeinfo['Drobe']['image_url'] = Router::url("/drobe_images/iphone/".$drobeinfo['Drobe']['file_name'],true);
				$drobeinfo['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobeinfo['Drobe']['file_name'],true);
				$drobeinfo['Drobe']['sell_brand_name'] = $drobeinfo['SellDrobe']['sell_brand_name'];
				$drobeinfo['Drobe']['sell_description'] = $drobeinfo['SellDrobe']['sell_description'];
				$drobeinfo['Drobe']['gender'] = $drobeinfo['SellDrobe']['sell_gender'];
				$drobeinfo['Drobe']['size'] = ($drobeinfo['SellDrobe']['size_name']==null)?"":$drobeinfo['SellDrobe']['size_name'];
				$drobeinfo['Drobe']['listing_sell_price'] = $drobeinfo['SellDrobe']['sell_price'];
				$drobeinfo['Drobe']['original_sell_price'] = $drobeinfo['SellDrobe']['original_sell_price'];
				$drobeinfo['Drobe']['category_name'] = $drobeinfo['Category']['category_name'];
				$drobeinfo['Drobe']['category_id'] = $drobeinfo['Category']['id'];
				$drobeinfo['Drobe']['order_id'] = $drobeinfo['SoldDrobe']['order_id'];
				$drobeinfo['Drobe']['paid_amount'] = $drobeinfo['SoldDrobe']['paid_amount'];
				$drobeinfo['Drobe']['tracking_number'] = $drobeinfo['SoldDrobe']['tracking_number'];
				$drobeinfo['Drobe']['shipping_address'] = $drobeinfo['SoldDrobe']['shipping_address'];
				$drobeinfo['Drobe']['buy_url'] = ($drobeinfo['Drobe']['buy_url'] == null)?"":$drobeinfo['Drobe']['buy_url'];
				$drobeinfo['Drobe']['sold_on'] = $drobeinfo['SoldDrobe']['sold_on'];
				$drobeinfo['Drobe']['shipped_on'] = $drobeinfo['SoldDrobe']['shipped_on'];
				$drobeinfo['Drobe']['status'] = $drobeinfo['SoldDrobe']['status'];
				$drobeinfo['Drobe']['order_date'] = $drobeinfo['SoldDrobe']['order_date'];
				
				//Getting Buyer Name from shipping address
				$shipping_address=json_decode($drobeinfo['SoldDrobe']['shipping_address'],true);
				$address="<address>".Configure::read('sell_drobe.shipping_address_format')."<address>";
				foreach ($shipping_address as $key=>$val)
				{
					$address=str_replace("<".$key.">",$val,$address);
				}
				$address= substr($address,strpos($address, "<b>"));
				$address = str_replace("<b>", "", $address);
				$buyer_name= substr($address,0,strpos($address, "<"));
				$drobeinfo['Drobe']['buyer_name']= $buyer_name;
				// end for getting buyer name
				$selldrobe['Drobe'][] = $drobeinfo['Drobe'];
			}
			
			if($selldrobe)
			{
				$response['type'] = "success";
				$response['SoldDrobe'] = $selldrobe['Drobe'];
			}
			else
			{
				$response['type'] = "error";
				$response['message'] = "No drobes found";
			}
		}
		else
		{
			$response['type'] = "error";
			$response['message'] = "Please enter userid";
		}
	
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	function update_sold_drobe($drobeid=null)
	{
		if($drobeid!=null)
		{
			$updateData=array();
			if(isset($this->data['tracking_number']) && $this->data['tracking_number']!=null)
			{
				$updateData['tracking_number']="'". $this->data['tracking_number']."'";
			}
			
			$updateData['status']="'shipped'";
			$updateData['shipped_on']="'". date('Y-m-d h:i:s')."'";
			
			$find = $this->SoldDrobe->find('first',array('conditions'=>array('SoldDrobe.drobe_id'=>$drobeid)));
			if($find)
			{
				if(!empty($updateData))
				{
					$sold_drobe = $this->SoldDrobe->updateAll($updateData, array('SoldDrobe.drobe_id'=>$drobeid));
					$response['shipped_on'] = date('Y-m-d h:i:s');
					$response['type'] = 'success';
					$response['message'] = 'SoldDrobe updated successfully';
				}
				else
				{
					$response['type'] = 'error';
					$response['message'] = 'SoldDrobe not updated!!!';
				}
			}
			else
			{
				$response['type'] = 'error';
				$response['message'] = 'Drobe not found!!!';
			}
		}
		else
		{
			$response['type'] = 'error';
			$response['message'] = 'Invalid parameter';
		}
		
		
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	function change_status($sold_drobe_id=null)
	{
		$this->SoldDrobe->id = $sold_drobe_id;
		$this->SoldDrobe->saveField('status','shipped');
		$this->SoldDrobe->saveField('shipped_on',date('Y-m-d H:i:s'));
		$this->Session->setFlash("Status change successfully","default",array("class"=>"success"));
		//$this->redirect(array('action'=>'index'));
		$this->redirect($this->referer());
	}
	
	function sold_drobe_detail($sold_drobe_id=null)
	{
		$this->SoldDrobe->bindModel(array('belongsTo'=>array("Drobe","SellDrobe")));
		$conditions=array(
				'SoldDrobe.id'=>$sold_drobe_id,
				'SoldDrobe.user_id'=>$this->Auth->user('id')
				);
		
		$drobeData = $this->SoldDrobe->find('first',array(
				'fields'=>array("Drobe.*","SellDrobe.*","SoldDrobe.*"),
				'conditions'=>$conditions
			
		));
		
		$this->set('drobes',$drobeData);
	}
	
	function admin_change_status($sold_drobe_id=null)
	{
		$this->change_status($sold_drobe_id);
	}
	
	function admin_send_mail_to_seller($sold_drobe_id=null)
	{
		if($sold_drobe_id!=null)
		{
			$this->loadModel('User');
			$sold_drobe = $this->SoldDrobe->find('first',
												array(
													'fields'=>array('SoldDrobe.*','Drobe.*','SellDrobe.*'),
													'conditions'=>array('SoldDrobe.id'=>$sold_drobe_id),
													));
			$user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$sold_drobe['SoldDrobe']['user_id'])));
			$shipping_address=json_decode($sold_drobe['SoldDrobe']['shipping_address'],true);
		/*	$address="<address>".Configure::read('sell_drobe.shipping_address_format')."<address>";
			
			*/
			$address="";
			foreach ($shipping_address as $key=>$val)
			{
				//$address=str_replace("<".$key.">",$val,$address);
				$address.=$val.",";
			} 
			/* $address = rtrim($address,","); */
			$address = str_replace(",,", ",", $address);
			/* Removing last comma character from string */
			$address = substr($address,0,-1);
			$address.=".";
			
			// setting up variables for email
			$variables=array(
					//"##image_source"=>Router::url('/drobe_images/'.$sold_drobe['Drobe']['file_name'],true),
					"##image_source"=>Router::url(array('controller'=>'drobes','action'=>'rate',$sold_drobe['Drobe']['unique_id'],'admin'=>false),true),
					//"##user_name"=>$user_data['User']['username'],
					"##shipping_address"=>$address,
					"##orderid"=>$sold_drobe['SoldDrobe']['order_id'],
					"##drobeid"=>$sold_drobe['SoldDrobe']['drobe_id'],
					"##item_name"=>$sold_drobe['Drobe']['comment'],
					"##buyer_name"=>$shipping_address['first_name'],
					"##sell_price"=>$sold_drobe['SoldDrobe']['sell_price']
					
			);
			if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
			{
				$variables["##user_name"]=$user_data['User']['username'];
			}
			else
			{
				$variables["##user_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
			}
			
			// sending mail after registration
		/*	$this->sendNotifyMail('mail_to_seller', $variables, $user_data['User']['email'],1);
			$this->Session->setFlash("Email sent succussfully to seller.","default",array("class"=>"success"));
			$this->redirect(array('action'=>'index')); */
			return $variables;
		}
		else 
		{
			$this->Session->setFlash("Email send failure due to invalid parameter.","default",array("class"=>"error"));
			$this->redirect(array('action'=>'index'));
		}
		
	}
	
}
?>