<?php 
/***************************************************
* FileUpload Component
*
* Manages uploaded files to be saved to the file system.
*
* @copyright    Copyright 2009, Webtechnick
* @link         http://www.webtechnick.com
* @author       Nick Baker
* @version      1.4
* @license      MIT
*/
class FileUploadComponent extends Object{
  /***************************************************
    * fileModel is the name of the model used if we want to 
    *  keep records of uploads in a database.
    * 
    * if you don't wish to use a database, simply set this to null
    *  $this->FileUpload->fileModel = null;
    *
    * @var mixed
    * @access public
    */
  var $fileModel = 'Upload';
  
  /***************************************************
    * uploadDir is the directory name in the webroot that you want
    * the uploaded files saved to.  default: files which means
    * webroot/files must exist and set to chmod 777
    *
    * @var string
    * @access public
    */
  var $uploadDir = 'files';
  
  /***************************************************
    * fileVar is the name of the key to look in for an uploaded file
    * For this to work you will need to use the
    * $form-input('file', array('type'=>'file)); 
    *
    * If you are NOT using a model the input must be just the name of the fileVar
    * input type='file' name='file'
    *
    * @var string
    * @access public
    */
  var $fileVar = 'file';
  
  
  /***************************************************
    * allowedTypes is the allowed types of files that will be saved
    * to the filesystem.  You can change it at anytime without
    * $this->FileUpload->allowedTypes = array('text/plain',etc...);
    *
    * @var array
    * @access public
    */
  var $allowedTypes = array(
    'image/jpeg',
    'image/jpg',
    'image/gif',
    'image/png',
    'image/pjpeg',
    'image/x-png'
  );
  
  /***************************************************
    * fields are the fields relating to the database columns
    *
    * @var array
    * @access public
    */
  var $fields = array('name'=>'name','type'=>'type','size'=>'size');
  
  /***************************************************
    * This will be true if an upload is detected even
    * if it can't be processed due to misconfiguration
    *
    * @var boolean
    * @access public
    */
  var $uploadDetected = false;
  
  /***************************************************
    * This will hold the uploadedFile array if there is one
    *
    * @var boolean|array
    * @access public
    */
  var $uploadedFile = false;
  
  /***************************************************
    * data and params are the controller data and params
    *
    * @var array
    * @access public
    */
  var $data = array();
  var $params = array();
  
  /***************************************************
    * Final file is set on move_uploadedFile success.
    * This is the file name of the final file that was uploaded
    * to the uploadDir directory.
    *
    * @var string
    * @access public
    */
  var $finalFile = null;
  
  /***************************************************
    * success is set if we have a fileModel and there was a successful save
    * or if we don't have a fileModel and there was a successful file uploaded.
    *
    * @var boolean
    * @access public
    */
  var $success = false;
  
  /***************************************************
    * errors holds any errors that occur as string values.
    * this can be access to debug the FileUploadComponent
    *
    * @var array
    * @access public
    */
  var $errors = array();
  var $imageInfo=array();

  //added by haresh
  var $randomFileName=false;
  
  var $resizeImage=false;
  var $setMinWidth=false;
  var $setMinHeight=false;
  var $cropeImage=false;
  var $newWidth=0;
  var $newHeight=0;
  
  var $createThumbImage=false;
  var $thumbWidth=100;
  var $thumbHeight=0;
  var $thumbFolder='thumbs';
  
  var $createIPhoneImage=false;
  var $iPhoneFolder='iPhone';
  var $iPhoneWidth=100;
  var $iPhoneHeight=0;
  
  var $detected_as_row_data=false;
  var $row_input_file_path = null;
  var $row_file_name = null;
  
  
  
  function beforeRender()
  {
  	
  }
  
  function shutdown(){
  	
  }
  
  /***************************************************
    * Initializes FileUploadComponent for use in the controller
    *
    * @param object $controller A reference to the instantiating controller object
    * @return void
    * @access public
    */
  function initialize(&$controller){
    $this->data = $controller->data;
    $this->params = $controller->params;
  }
  /***************************************************
    * Main execution method.  Handles file upload automatically upon detection and verification.
    *
    * @param object $controller A reference to the instantiating controller object
    * @return void
    * @access public
    */
  function startup(&$controller){
    $this->uploadDetected = ($this->_multiArrayKeyExists("tmp_name", $this->data) || $this->_multiArrayKeyExists("tmp_name",$this->data));
    if(!$this->uploadDetected)
    {
    	/*
    	 * 
    	 * This if block added by haresh vidja for uploading image from file uploader
    	 * 
    	 */
    	$headers = apache_request_headers();
    	if (isset($headers['Content-Length']) && (int)$headers['Content-Length'] == 0){
    	//	die ('{error: "content length is zero"}');
    	}
    	else if(isset($_GET['qqfile']))
    	{
    		$this->detected_as_row_data=true;
    		$this->uploadDetected=true;
    		$input = fopen("php://input", "r");
    		$this->row_file_name = $_GET['qqfile'];
    		$temp = tmpfile();
    		$realSize = stream_copy_to_stream($input, $temp);
    		fclose($input);
    		$tmp_file_path= TMP.uniqid().".".strtolower(array_pop(explode(".",$_GET['qqfile'])));
    		$this->row_input_file_path=$tmp_file_path;
    		$target = fopen($tmp_file_path, "w");
    		fseek($temp, 0, SEEK_SET);
    		stream_copy_to_stream($temp, $target);
    		fclose($target);
    	}
    }
    if(!$this->detected_as_row_data)
    {
    	$this->uploadedFile = $this->_uploadedFileArray();
    }
    else
    {
       	$this->uploadedFile['name']=$_GET['qqfile'];
    	$this->uploadedFile['tmp_name']=$this->row_input_file_path;
    	$this->uploadedFile['error']=0;
    	$this->uploadedFile['type']=filetype($this->row_input_file_path);
    }
    if($this->_checkFile() && $this->_checkType()){
    	
      $this->_processFile();
    }
  }
  
  /*************************************************
    * removeFile removes a specific file from the uploaded directory
    *
    * @param string $name A reference to the filename to delete from the uploadDirectory
    * @return boolean
    * @access public
    */
  function removeFile($name = null){
    if(!$name) return false;
    
    $up_dir = WWW_ROOT . $this->uploadDir;
    $target_path = $up_dir . DS . $name;
    if(unlink($target_path)) return true;
    else return false;
  }
  
  /*************************************************
    * showErrors itterates through the errors array
    * and returns a concatinated string of errors sepearated by
    * the $sep
    *
    * @param string $sep A seperated defaults to <br />
    * @return string
    * @access public
    */
  function showErrors($sep = "<br />"){
    $retval = "";
    foreach($this->errors as $error){
      $retval .= "$error $sep";
    }
    return $retval;
  }
  
  
  
  /**************************************************
    * _processFile takes the detected uploaded file and saves it to the
    * uploadDir specified, it then sets success to true or false depending
    * on the save success of the model (if there is a model).  If there is no model
    * success is meassured on the success of the file being saved to the uploadDir
    *
    * finalFile is also set upon success of an uploaded file to the uploadDir
    *
    * @return void
    * @access private
    */
  function _processFile(){
  	
  	$up_dir = WWW_ROOT . $this->uploadDir;
    if($this->randomFileName)
    {
    	$this->_randomFileName();
    }
    $target_path = $up_dir . DS . $this->uploadedFile['name'];
    $temp_path = substr($target_path, 0, strlen($target_path) - strlen($this->_ext())); //temp path without the ext
    //make sure the file doesn't already exist, if it does, add an itteration to it
        $i=1;
        while(file_exists($target_path)){
            $target_path = $temp_path . "-" . $i . $this->_ext();
            $i++;
        }
        
    $save_data = array();
	/*if($this->detected_as_row_data)
	{
		$uploaded_path=$this->row_input_file_path;
	}
	else $uploaded_path=$this->uploadedFile['tmp_name'];*/
	if(!$this->detected_as_row_data)
	{
		$uploaded=move_uploaded_file($this->uploadedFile['tmp_name'], $target_path);
	}
	else
	{
		$uploaded=copy($this->uploadedFile['tmp_name'], $target_path);
	}
	
	if($uploaded){
      	//Final File Name
      	$this->finalFile = basename($target_path);
      	if($this->resizeImage || $this->createThumbImage || $this->setMinWidth>0 || $this->setMinHeight>0)
      	{
      		$temp_path=TMP.array_pop(explode(DS,$target_path));
      		copy($target_path, $temp_path);
      	}
      	
      	if($this->resizeImage)
      	{
      		if($this->cropeImage)
      		{
      			$this->_resizeAndCrop($temp_path,$this->newWidth,$this->newHeight,$target_path);
      		}
      		
      		else $this->_resizeActualImage($temp_path,$this->newWidth,$this->newHeight,$target_path);
      	}
      	// we need to use this in crop image area
      	if($this->setMinWidth!=false && $this->setMinWidth>0 || $this->setMinHeight!=false && $this->setMinHeight>0)
      	{
      		$this->_setMinimumWidthHeight($temp_path,$target_path);
      		
      	}
      	if($this->createThumbImage)
      	{
      		$thumb_path= WWW_ROOT . $this->thumbFolder.DS.$this->finalFile;
      		$this->_resizeAndCrop($temp_path,$this->thumbWidth,$this->thumbHeight,$thumb_path);
      	}
      	if($this->createIPhoneImage)
      	{
      		$iphone_path= WWW_ROOT . $this->iPhoneFolder.DS.$this->finalFile;
      		$this->_resizeAndCrop($temp_path,$this->iPhoneWidth,$this->iPhoneHeight,$iphone_path);
      	}
      	if(isset($temp_path) && file_exists($temp_path))
      	{
      		unlink($temp_path);
      	}
      	
	if(isset($this->uploadedFile['tmp_name']) && file_exists($this->uploadedFile['tmp_name']))
      	{
      		unlink($this->uploadedFile['tmp_name']);
      	}
      	
      	$model = $this->getModel();
      	$save_data[$this->fields['name']] = $this->finalFile;
      	$save_data[$this->fields['type']] = $this->uploadedFile['type'];
      	$save_data[$this->fields['size']] = $this->uploadedFile['size'];
      	if(!$model || $model->save($save_data)) 
      	{
      		$this->success = true;
      		$this->imageInfo= $this->_getInfo($target_path);
      	}
      	else $this->success = false;
      	
    }
    else{
      $this->_error('FileUpload::processFile() - Unable to save temp file to file system.');
    }
  }
  function _getInfo($src)
  {
  	$info=array();
  	$ext=str_replace(".", "", $this->_ext());
  	if($ext==null)
  	{
  		$ext=array_pop(explode(".", $src));
  	}
  	if($ext=="jpg" || $ext=="jpeg") $image = imagecreatefromjpeg($src);
  	else if($ext=="gif") $image = imagecreatefromgif($src);
  	else if($ext=="png") $image = imagecreatefrompng($src);
  	$info['width'] = imagesx($image);
  	$info['height'] = imagesy($image);
  	imagedestroy($image);
  	return $info;
  }
  function _setMinimumWidthHeight($src,$target_path)
  {
  	
  	/* read the source image */
  	$ext=str_replace(".", "", $this->_ext());
  	if($ext==null)
  	{
  		$ext=array_pop(explode(".", $target_path));
  	}
  	if($ext=="jpg" || $ext=="jpeg") $image = imagecreatefromjpeg($src);
  	else if($ext=="gif") $image = imagecreatefromgif($src);
  	else if($ext=="png") $image = imagecreatefrompng($src);
  	
  	$width = imagesx($image);
  	$height = imagesy($image);
  	if(($this->setMinWidth!=false && $width<$this->setMinWidth) || ($this->setMinHeight!=false && $height<$this->setMinHeight))
  	{
  		$new_width= ($this->setMinWidth!=false && $width<$this->setMinWidth) ? $this->setMinWidth : $width;
  		$new_height= ($this->setMinHeight!=false && $height<$this->setMinHeight) ? $this->setMinHeight : $height;

  		// make aspect ratio in square image
  		if($new_width<$new_height) $new_width=$new_height;
  		if($new_width>$new_height) $new_height=$new_width;
  		
  		$thumb_new = imagecreatetruecolor( $new_width, $new_height );
  		
  		imagefill($thumb_new, 0, 0, imagecolorallocate($thumb_new, 255, 255, 255));
  		
  		// Copy new image to memory after cropping.
  		$new_left=intval(($new_width - $width) / 2);
  		$new_top=intval(($new_height - $height) / 2);
  
  		imagecopymerge($thumb_new, $image, $new_left, $new_top,0,0, $width , $height,100);
  		imagedestroy($image);
  
  		// filling white color in extra part
  		//$white = imagecolorallocate($thumb_new, 255, 255, 255);
  		//$this->_fillColor($thumb_new, 1, 1,$white);
  		//$this->_fillColor($thumb_new, $new_width-1, $new_height-1,$white);
  
  			
  		if($ext=="jpg" || $ext=="jpeg") imagejpeg($thumb_new,$target_path);
  		else if($ext=="gif") imagegif($thumb_new,$target_path);
  		else if($ext=="png") imagepng($thumb_new,$target_path);
  	}
  }
  
  function _fillColor(&$im,$left,$top,$color)
  {
  	$rgb = imagecolorat($im, $left, $left);
  	if($rgb==0)
  	{
  		imagefill($im, $left, $top, $color);
  	}
  }
  function _resizeActualImage($src,$new_width,$new_height,$target_path)
  {
  	/* read the source image */
  	$ext=str_replace(".", "", $this->_ext());
  	if($ext=="jpg" || $ext=="jpeg") $image = imagecreatefromjpeg($src);
  	else if($ext=="gif") $image = imagecreatefromgif($src);
  	else if($ext=="png") $image = imagecreatefrompng($src);
  	$width = imagesx($image);
  	$height = imagesy($image);
  	
  	
  	
  	// keep width and height ratio in ascpect ratio
  
  	if($width>$new_width)
  	{
  		$newWidth=$new_width;
  		$newHeight = floor($height*($newWidth/$width));
  	}
  	else
  	{
  		$newWidth=$width;
  		$newHeight=$height;
  	}
  	if($new_height>0)
  	{
	  	if($newHeight>$new_height)
	  	{
	  		$newHeight=$new_height;
	  		$newWidth=floor($width*($newHeight/$height));
	  	}
  	}
  	/* create a new, "virtual" image */
  	$thumb= imagecreatetruecolor($newWidth,$newHeight);
  
  	/* copy source image at a resized size */
  	imagecopyresampled($thumb,$image,0,0,0,0,$newWidth,$newHeight,$width,$height);
  	imagedestroy($image);
  	
  	$width = imagesx($thumb);
  	$height = imagesy($thumb);
  	 
  
  	if($width>$min_width) $min_width=$width;
  	if($height>$min_height) $min_height=$height;
  	
  	$thumb_new = imagecreatetruecolor( $min_width, $min_height );
  	// Copy new image to memory after cropping.
  	$new_left=intval(0 - ($min_width - $width) / 2);
  	$new_top=intval(0 - ($min_height - $height) / 2);
  	 
  	imagecopy($thumb_new, $thumb, 0, 0, $new_left, $new_top, $min_width , $min_height);
  	imagedestroy($thumb);
  	
  	
  	// filling white color in extra part
  	$white = imagecolorallocate($thumb_new, 255, 255, 255);
  	imagefill($thumb, 0, 0, $white);
  	if($min_width>$width)
  	{
  		imagefill($thumb_new, 0, 0, $white);
  		imagefill($thumb_new, $min_width, 0, $white);
  	}
  	if($min_height>$height)
  	{
  		imagefill($thumb_new, 0, 0, $white);
  		imagefill($thumb_new, $min_width, $min_height, $white);
  	}
  	
  	if($ext=="jpg" || $ext=="jpeg") imagejpeg($thumb_new,$target_path);
  	else if($ext=="gif") imagegif($thumb_new,$target_path);
  	else if($ext=="png") imagepng($thumb_new,$target_path);
  }
  
  function _resizeAndCrop($src,$new_width,$new_height,$target_path)
  {
  	
  	/* read the source image */
  	$ext=str_replace(".", "", $this->_ext());
  	if($ext=="jpg" || $ext=="jpeg") $image = imagecreatefromjpeg($src);
  	else if($ext=="gif") $image = imagecreatefromgif($src);
  	else if($ext=="png") $image = imagecreatefrompng($src);
  	
  	$width = imagesx($image);
  	$height = imagesy($image);
  	
  	if($width>$new_width)
  	{
  		//adjust width of image
  		$newWidth=$new_width;
  		$newHeight = floor($height*($newWidth/$width));
  	}
  	else
  	{
  		$newWidth=$width;
  		$newHeight=$height;
  	}
  	if($width>$height && $newHeight<$new_height && $height>$new_height)
  	{
  		$newHeight=$new_height;
  		$newWidth = floor($width*($newHeight/$height));
  	}
  	$virtual_image = imagecreatetruecolor($newWidth,$newHeight);
  	/* copy source image at a resized size */
  	//imagecopyresized($virtual_image,$image,0,0,0,0,$newWidth,$newHeight,$width,$height);
  	imagecopyresampled($virtual_image,$image,0,0,0,0,$newWidth,$newHeight,$width,$height);
  	imagedestroy($image);
  	
  	$width = imagesx($virtual_image);
  	$height = imagesy($virtual_image);
  	
  	$thumb = imagecreatetruecolor( $new_width, $new_height );
   	// Copy new image to memory after cropping.
   	$new_left=intval(0 - ($new_width - $width) / 2);
   	$new_top=intval(0 - ($new_height - $height) / 2);
 
  	imagecopy($thumb, $virtual_image, 0, 0, $new_left, $new_top, $new_width , $new_height);
  	imagedestroy($virtual_image);
	
  	
  	// filling white color in extra part
  	$white = imagecolorallocate($thumb, 255, 255, 255);
  	imagefill($thumb, 0, 0, $white);
  	
  	if($new_width>$width)
  	{
  		imagefill($thumb, $new_height, 0, $white);
  	}
  	if($new_height>$height)
  	{
  		imagefill($thumb, 0, $new_width, $white);
  	}
  	
  	if($ext=="jpg" || $ext=="jpeg") imagejpeg($thumb,$target_path);
  	else if($ext=="gif") imagegif($thumb,$target_path);
  	else if($ext=="png") imagepng($thumb,$target_path);
  	
  	imagedestroy($thumb);
  	
  }
  
 
  function _createThumb($src)
  {
  	/* read the source image */
  	$ext=str_replace(".", "", $this->_ext());
  	if($ext=="jpg" || $ext=="jpeg") $source_image = imagecreatefromjpeg($src);
  	else if($ext=="gif") $source_image = imagecreatefromgif($src);
  	else if($ext=="png") $source_image = imagecreatefrompng($src);
  	$width = imagesx($source_image);
  	$height = imagesy($source_image);
  	
  	/* find the "desired height" of this thumbnail, relative to the desired width  */
  	if($this->thumbHeight==0)
  	$this->thumbHeight = floor($height*($this->thumbWidth/$width));
  	
  	/* create a new, "virtual" image */
  	$virtual_image = imagecreatetruecolor($this->thumbWidth,$this->thumbHeight);
  	
  	/* copy source image at a resized size */
  	imagecopyresized($virtual_image,$source_image,0,0,0,0,$this->thumbWidth,$this->thumbHeight,$width,$height);
  	
  	/* create the physical thumbnail image to its destination */
  	$dest= WWW_ROOT . $this->thumbFolder.DS.$this->finalFile;
  	
  	if($ext=="jpg" || $ext=="jpeg") imagejpeg($virtual_image,$dest);
  	else if($ext=="gif") imagegif($virtual_image,$dest);
  	else if($ext=="png") imagepng($virtual_image,$dest);
  }
  /***************************************************
    * Returns a reference to the model object specified, and attempts
    * to load it if it is not found.
    *
    * @param string $name Model name (defaults to FileUpload::$fileModel)
    * @return object A reference to a model object
    * @access public
    */
    function getModel($name = null) {
        $model = null;
        if (!$name) {
            $name = $this->fileModel;
        }
    
    if($name){
      $model = ClassRegistry::init($name);
      if (empty($model) && $this->fileModel) {
        $this->_error('FileUpload::getModel() - Model is not set or could not be found');
        return null;
      }
    }
        return $model;
    }
  
  /***************************************************
    * Adds error messages to the component
    *
    * @param string $text String of error message to save
    * @return void
    * @access protected
    */
  function _error($text){
    $message = __($text,true);
    $this->errors[] = $message;
    trigger_error($message,E_USER_WARNING);
  }
  
  /***************************************************
    * Checks if the uploaded type is allowed defined in the allowedTypes
    *
    * @return boolean if type is accepted
    * @access protected
    */
  function _checkType(){
  	if($this->allowedTypes=="All")
  	{
  		return true;
  	}
    foreach($this->allowedTypes as $value){
      if(strtolower($this->uploadedFile['type']) == strtolower($value)){
        return true;
      }
    }
    $this->_error("FileUpload::_checkType() {$this->uploadedFile['type']} is not in the allowedTypes array.");
    return false;
  }
  
  /***************************************************
    * Checks if there is a file uploaded
    *
    * @return void
    * @access protected
    */
  function _checkFile(){
  	if($this->detected_as_row_data)
  	{
  		return file_exists($this->row_input_file_path);
  	}
    else if($this->uploadedFile && $this->uploadedFile['error'] == UPLOAD_ERR_OK ) return true;
    else return false;
  }
  
  /***************************************************
    * Returns the extension of the uploaded filename.
    *
    * @return string $extension A filename extension
    * @access protected
    */
  function _ext(){
    return strtolower(strrchr($this->uploadedFile['name'],"."));
  }
  
  /***************************************************
    * Returns an array of the uploaded file or false if there is not a file
    *
    * @param string $text String of error message to save
    * @return array|boolean Array of uploaded file, or false if no file uploaded
    * @access protected
    */
  function _uploadedFileArray(){
  	if($this->fileModel){
    	// commented and changed by haresh
  		$retval = isset($this->params['form'][$this->fileVar]) ? $this->params['form'][$this->fileVar] : false;
      //$retval = isset($this->data[$this->fileModel][$this->fileVar]) ? $this->data[$this->fileModel][$this->fileVar] : false;
    }
    else {
      $retval = isset($this->params['form'][$this->fileVar]) ? $this->params['form'][$this->fileVar] : false;
    }
    
    if($this->uploadDetected && $retval === false){
      $this->_error("FileUpload: A file was detected, but was unable to be processed due to a misconfiguration of FileUpload. Current config -- fileModel:'{$this->fileModel}' fileVar:'{$this->fileVar}'");
    }
    return $retval;
  }
  
  /***************************************************
    * Searches through the $haystack for a $key.
    *
    * @param string $needle String of key to search for in $haystack
    * @param array $haystack Array of which to search for $needle
    * @return boolean true if given key is in an array
    * @access protected
    */
  function _multiArrayKeyExists($needle, $haystack) {
    if(is_array($haystack)){
      foreach ($haystack as $key=>$value) {
        if ($needle==$key) {
          return true;
        }
        if (is_array($value)) {
          if($this->_multiArrayKeyExists($needle, $value)){
            return true;
          }
        }
      }
    }
    return false;
  }
  function _randomFileName()
  {
  	$ext=$this->_ext();
  	$this->uploadedFile['name']=uniqid().$ext;
  }
}
?> 
