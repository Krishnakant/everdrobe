<?php
class RatesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
		$this->Auth->deny('mydrobes','create');
	}
	function index()
	{
		
	}
	
	 function update_last_conversation_time()
	{
		$this->Rate->recursive=-1;
		$rateData=$this->Rate->find('all',array('conditions'=>array('conversation_id !='=>"")));
		//echo count($rateData);exit;
		//pr($rateData);exit;
		$this->loadModel('Conversation');
		$this->Conversation->recursive=-1;
		foreach ($rateData as $rate)
		{
			$conData=$this->Conversation->find('first',array(
					'conditions'=>array('Conversation.unique_id'=>$rate['Rate']['conversation_id']),
					'order'=>array('Conversation.created_on DESC')			
					));
			if($conData)
			{
				$this->Rate->updateAll(array('last_conversation_time'=>"'".$conData['Conversation']['created_on']."'"),array("Rate.id"=>$rate['Rate']['id']));
			}
		}
		echo "Done";
		exit;
	} 
	
	function admin_view($drobe_id)
	{
		$this->_getDrobeData($drobe_id);
		$this->Rate->bindModel(array("belongsTo"=>array("User")));
		$rateData=$this->Rate->find('all',array('conditions'=>array('Rate.drobe_id'=>$drobe_id,"Rate.rate !="=>0)));
		$this->set('rates',$rateData);
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
	}
	function admin_modify($drobe_id,$rate_id)
	{
		if(!empty($this->data))
		{
			if($this->Rate->save($this->data))
			{
				$this->Rate->updateRateCounter($drobe_id);
				if($this->data['Rate']['comment']!="")
				{
					$this->Rate->updateAll(array('conversation_id'=>"'".uniqid()."'",'conversation_status'=>"'open'"),array("Rate.id"=>$this->data['Rate']['id'],"Rate.conversation_id"=>""));
				}
				
				/*
				 * updating user totals counter of drobe uploder
				 */
				$this->loadModel('Drobe');
				$user_id=$this->Drobe->field('user_id',array('Drobe.id'=>$drobe_id));
				if($user_id>0)
				{
					$this->Drobe->User->resetUserTotalFields($user_id);
				}
				
				/*
				 * updating user totals counter of voter
				*/
				$user_id=$this->Rate->field('user_id',array('Rate.id'=>$rate_id));
				if($user_id>0)
				{
					$this->Drobe->User->resetUserTotalFields($user_id);
				}
				$this->Session->setFlash("Feedback modified successfully","default",array("class"=>"success"));
				$this->redirect(array("action"=>"admin_view",$drobe_id));
			}
		}
		$this->_getDrobeData($drobe_id);
		$this->Rate->bindModel(array("belongsTo"=>array("User")));
		$this->Rate->id=$rate_id;
		$this->data=$this->Rate->read();
	}
	function admin_update_counter($drobe_id)
	{
		$this->Rate->updateRateCounter($drobe_id);
		$this->Session->setFlash("Rate counter updated successfully","default",array("class"=>"success"));
		$this->redirect($this->referer());
	}
	function admin_remove($drobe_id,$rate_id)
	{
		$conversation_id=$this->Rate->field('conversation_id',array('Rate.id'=>$rate_id));
		// getting id of voter
		$voter_id=$this->Rate->field('user_id',array('Rate.id'=>$rate_id));
		$this->loadModel('Conversation');
		if($conversation_id!="")
		{
			$this->Conversation->deleteAll(array('Conversation.unique_id'=>$conversation_id));
		}
		if($this->Rate->deleteAll(array('Rate.id'=>$rate_id)))
		{
			$this->Rate->updateRateCounter($drobe_id);
			/*
			 * updating user totals counter of drobe uploder
			*/
			$this->loadModel('Drobe');
			$user_id=$this->Drobe->field('user_id',array('Drobe.id'=>$drobe_id));
			if($user_id>0)
			{
				$this->Drobe->User->resetUserTotalFields($user_id);
			}
			/*
			 * updating user totals counter of voter
			*/
			if($voter_id>0)
			{
				$this->Drobe->User->resetUserTotalFields($voter_id);
			}
			
			$this->Session->setFlash("Selected feedback removed successfully","default",array("class"=>"success"));
		}
		else
		{
			$this->Session->setFlash("Error occured in remove this feedback try again later","default",array("class"=>"success"));
		}
		$this->redirect($this->referer());
	}
	/*
	 * Webservice for remove feedback
	 */
	function remove($conversation_id)
	{
		$response=array();
		
		$rate_data=$this->Rate->findByConversationId($conversation_id);

		$this->loadModel('Conversation');
		if($rate_data['Rate']['conversation_id']!="")
		{
			$this->Conversation->deleteAll(array('Conversation.unique_id'=>$rate_data['Rate']['conversation_id']));
		}
		$this->Rate->id=$rate_id;
		$update_fields=array('Rate.comment'=>'""','Rate.new_reply'=>0,'Rate.new_response'=>0,"Rate.conversation_status"=>'"close"',"Rate.conversation_id"=>'""');
		if($this->Rate->updateAll($update_fields,array('Rate.id'=>$rate_data['Rate']['id'])))
		{
			$this->Rate->updateRateCounter($rate_data['Rate']['drobe_id']);
			
			/*
			 * updating user totals counter of drobe uploder
			*/
			$this->loadModel('Drobe');
			$user_id=$this->Drobe->field('user_id',array('Drobe.id'=>$rate_data['Rate']['drobe_id']));
			if($user_id>0)
			{
				$this->Drobe->User->resetUserTotalFields($user_id);
			}
			//updating user totals counter of voter
			$this->Drobe->User->resetUserTotalFields($rate_data['Rate']['user_id']);
			
			$response['type']="success";
			$response['message']="Feedback removed successfully";
		}
		else
		{
			$response['type']="error";
			$response['message']="error occured in remove feedback";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function remove_feedback()
	{
		$response=array();
		if(!empty($this->data))
		{
			if(isset($this->data['conversation_id']) && $this->data['conversation_id']!="")
			{
				$conversation_id=$this->data['conversation_id'];
				//$rate_id=$this->Rate->field('id',array('conversation_id'=>$conversation_id));
				$rate_data=$this->Rate->findByConversationId($conversation_id);
				
				//$drobe_id=$this->Rate->field('drobe_id',array('conversation_id'=>$conversation_id));
				$this->loadModel('Conversation');
				if($conversation_id!="")
				{
					$this->Conversation->deleteAll(array('Conversation.unique_id'=>$conversation_id));
				}
				$this->Rate->id=$rate_data['Rate']['id'];
				$update_fields=array('Rate.comment'=>'""','Rate.new_reply'=>0,'Rate.new_response'=>0,"Rate.conversation_status"=>'"close"',"Rate.conversation_id"=>'""');
				if($this->Rate->updateAll($update_fields,array('Rate.id'=>$rate_data['Rate']['id'])))
				{
					$this->Rate->updateRateCounter($rate_data['Rate']['drobe_id']);
					
					/*
					 * updating user totals counter of drobe uploder
					*/
					$this->loadModel('Drobe');
					$user_id=$this->Drobe->field('user_id',array('Drobe.id'=>$rate_data['Rate']['drobe_id']));
					if($user_id>0)
					{
						$this->Drobe->User->resetUserTotalFields($user_id);
					}
					//updating user totals counter of voter
					$this->Drobe->User->resetUserTotalFields($rate_data['Rate']['user_id']);
					
					$response['type']="success";
					$response['message']="Feedback removed successfully";
				}
				else
				{
					$response['type']="error";
					$response['message']="error occured in remove feedback";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Invalid conversation id";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid post fields";
		}
		echo json_encode($response);
		exit();
	}
	function _getDrobeData($drobe_id)
	{
		
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
		
		$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array("hasMany"=>array("Rate","Flag","Favourite"),"hasOne"=>array("DrobeSetting")));
		$drobe=$this->Drobe->findById($drobe_id);
		$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
		if($total>0)
		{
		/*$drobe['Drobe']['total_in']=round($drobe['Drobe']['total_in']/$total*100);
		$drobe['Drobe']['total_out']=round($drobe['Drobe']['total_out']/$total*100);*/
		$drobe['Drobe']['total_in']=$drobe['Drobe']['total_in'];
		$drobe['Drobe']['total_out']=$drobe['Drobe']['total_out'];
		}
		else
		{
			$drobe['Drobe']['total_in']=0;
			$drobe['Drobe']['total_out']=0;
		}
		$this->set('drobeData',$drobe);
	}
	
	function reward_ebx($user_id,$rate_id){
		
		$updateFields=array();
		$ebxAddDrobe=Configure::read('setting.ebx_reward_point');
		
		$drobe_id=$this->Rate->field('drobe_id',array('Rate.id'=>$rate_id));
		$this->loadModel('Drobe');
		$this->Drobe->recursive=-1;
		$drobe_user_id=$this->Drobe->field('user_id',array('Drobe.id'=>$drobe_id));
		$this->loadModel("User");
		$this->User->recursive=-1;
		$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$drobe_user_id)));
		$userName=$userData['User']['username'];
		
		//push notification
		$this->send_notification_user($userName." just rewared you $".$ebxAddDrobe." EverBucks for your comment, Thanks!", $user_id,array('action'=>'reward'));
		
		/*$this->loadModel("UserSettings");
		$userData=$this->UserSettings->find('first',array('conditions',array('UserSettings.user_id'=>$user_id)));
		$this->send_notification($userData['UserSettings']['apple_device_token'], "You got reward EBX on your comment");*/

		//update users ebx point
		$updateFields["UserTotal.current_ebx_point"]="UserTotal.current_ebx_point + ".$ebxAddDrobe;
		$updateFields["UserTotal.earned_ebx_point"]="UserTotal.earned_ebx_point + ".$ebxAddDrobe;
		$this->loadModel('UserTotal');
		$userTotalsData=$this->UserTotal->find('first',array('conditions'=>array('UserTotal.user_id'=>$user_id)));
		$this->UserTotal->updateAll($updateFields,array('UserTotal.user_id'=>$user_id));
		$this->ebx_transactions_add("reward",$user_id,$ebxAddDrobe,$userTotalsData['UserTotal']['current_ebx_point']+$ebxAddDrobe,$rate_id,"Rewarded on feedback of drobe.");
		return $ebxAddDrobe;
	}
	
	function reward()
	{
		$response=array();
		if(!empty($this->data))
		{
			$this->Rate->recursive=-1;
			$rateData=$this->Rate->find('first',array('conditions'=>array('Rate.conversation_id'=>$this->data['conversation_id'])));
			
			if($rateData['Rate']['rewarded']==0)
			{
				$updateFields=array();
				$updateFields["Rate.rewarded"]="1";
				
				if($this->Rate->updateAll($updateFields,array('Rate.id'=>$rateData['Rate']['id'])))
				{
					$ebx=$this->reward_ebx($rateData['Rate']['user_id'],$rateData['Rate']['id']);
					
					$this->loadModel('User');
					$this->User->recursive=-1;
					$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$rateData['Rate']['user_id'])));

					$response['type']="success";
					$response['message']="You've just rewarded ".$userData['User']['username']." $".$ebx." EverBucks, Thanks!";
				}
				else
				{
					$response['type']="error";
					$response['message']="error occured in give reward";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Already rewarded";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		echo json_encode($response);
		exit();
	}
	/* 
	 * webservice for reward on comment give ebx point to user 
	 */
	function reward_on_comment($rate_id=null)
	{
		$response=array();
		if($rate_id>0)
		{
			$this->Rate->recursive=-1;
			$rateData=$this->Rate->find('first',array('conditions'=>array('Rate.id'=>$rate_id)));
			if($rateData['Rate']['rewarded']==0)
			{
				$updateFields=array();
				$updateFields["Rate.rewarded"]="1";
	
				if($this->Rate->updateAll($updateFields,array('Rate.id'=>$rateData['Rate']['id'])))
				{
					$ebx=$this->reward_ebx($rateData['Rate']['user_id'],$rateData['Rate']['id']);
					
					$this->loadModel('User');
					$this->User->recursive=-1;
					$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$rateData['Rate']['user_id'])));
					
					$response['type']="success";
					$response['message']="You've just rewarded ".$userData['User']['username']." $".$ebx." EverBucks, Thanks!";
				}
				else
				{
					$response['type']="error";
					$response['message']="error occured in give reward";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Already rewarded";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function received()
	{
		$conditions=array();
		//$conditions['Rate.comment !=']="";
		$conditions['Drobe.user_id']=$this->Auth->user('id');
		$conditions['Drobe.rate_status']='open';
		$conditions['Drobe.deleted']=0;
		$conditions['Rate.rate !=']=0;
		$this->paginate = array(
					'fields'=>array('Rate.*','Drobe.id','Drobe.unique_id','Drobe.category_id','Drobe.new_response','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.total_out','User.first_name','User.username','User.last_name','User.id','User.unique_id'),
							'joins'=>array(
								array(
									'table' => 'drobes',
									'alias' => 'Drobe',
									'type' => 'INNER',
									'conditions' => array(
									'Drobe.id = Rate.drobe_id'
								)),
								array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'INNER',
									'conditions' => array(
										'User.id = Drobe.user_id'
									))
							),
						'group'=>"Drobe.id",
						'order'=>"Drobe.new_response DESC, Rate.created_on DESC",
						'conditions'=>$conditions,
						'limit'=>10
		);
		$result = $this->paginate('Rate');
		foreach($result as &$drobe)
		{
			$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
			/*$drobe['Drobe']['total_in']=round($drobe['Drobe']['total_in']/$total*100);
			$drobe['Drobe']['total_out']=round($drobe['Drobe']['total_out']/$total*100);*/
			$drobe['Drobe']['total_in']=$drobe['Drobe']['total_in'];
			$drobe['Drobe']['total_out']=$drobe['Drobe']['total_out'];
		}
		$this->set('drobes',$result);
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
	}
	
	function result($drobe_id=null,$result="all")
	{
		$conditions=array();
		if($result=="comment")
		{
			$conditions['Rate.comment !=']="";
		}
		else
		{
			$conditions['Rate.rate !=']=0;
		}
		$this->loadModel('Drobe');
		$this->Drobe->recursive=-1;
		$drobeData=$this->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$drobe_id,'Drobe.user_id'=>$this->Auth->user('id'),'Drobe.rate_status'=>'open')));
		$this->Drobe->updateAll(array('Drobe.new_response'=>0),array('Drobe.id'=>$drobeData['Drobe']['id']));
		
		$conditions['Rate.drobe_id']=$drobeData['Drobe']['id'];
		
		
		
		$result=$this->Rate->find('all',array(
					'fields'=>array('Rate.*','User.unique_id','User.star_user','User.first_name','User.last_name','User.username','User.photo','User.gender','User.province','User.country','User.id'),
					'joins'=>array(
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'INNER',
							'conditions' => array(
							'User.id = Rate.user_id'
						))
					),
					'conditions'=>$conditions,
					'order'=> "Rate.new_reply DESC, Rate.created_on DESC",
					'limit'=>10
			));
		$this->set('rates',$result);
	}
	
	function given()
	{
		$conditions=array();
		$conditions['Rate.comment !=']="";
		//$conditions['Rate.rate !=']=0;
		$conditions['Rate.user_id']=$this->Auth->user('id');
		$conditions['Drobe.deleted']=0;
		$conditions['Drobe.rate_status']='open';
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$search = trim($this->params->params['named']['search']);
			$keyword= preg_split("/[\s,]+/", $search);
			foreach($keyword as $key)
			{
				$conditions['OR'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%"), 'Drobe.comment LIKE' => "%".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('Rate.comment LIKE' => $key."%"), 'Rate.comment LIKE' => "%".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('User.first_name LIKE' => $key."%"), 'User.first_name LIKE' => "%".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('User.last_name LIKE' => $key."%"), 'User.last_name LIKE' => "%".$key."%"));
				$conditions['OR'][] = array('OR'=>array('OR'=>array('User.username LIKE' => $key."%"), 'User.username LIKE' => "@"."%".$key."%"));
			}
			/*$conditions['OR']['Drobe.comment like']="%".$search."%";
			$conditions['OR']['Rate.comment like']="%".$search."%";
			$conditions['OR']['User.first_name like']="%".$search."%";
			$conditions['OR']['User.last_name like']="%".$search."%";*/
		}
		
		$this->paginate = array(
							'fields'=>array('Rate.*','Drobe.id','Drobe.is_highest_rated','Drobe.unique_id','Drobe.category_id','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.total_out','User.first_name','User.username','User.last_name','User.id','User.unique_id'),
								'joins'=>array(
									array(
										'table' => 'drobes',
										'alias' => 'Drobe',
										'type' => 'INNER',
										'conditions' => array(
										'Drobe.id = Rate.drobe_id'
									)),
									array(
										'table' => 'users',
										'alias' => 'User',
										'type' => 'INNER',
										'conditions' => array(
											'User.id = Drobe.user_id'
									))
								),
							'conditions'=>$conditions,
							//'order'=>'Rate.new_response DESC, Rate.created_on DESC',
							'order'=>'Rate.new_response DESC, Rate.last_conversation_time DESC',
							'limit'=>10
					);
		$result = $this->paginate('Rate');
		$this->set('drobes',$result);
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		$this->set('categoryList',Cache::read('drobe_category'));
	}
	
	function close_conversation()
	{
		$response=array();
		if(!empty($this->data))
		{
			$rateData=$this->Rate->find('first',array('conditions'=>array('Rate.conversation_id'=>$this->data['conversation_id'])));
			$this->Rate->id=$rateData['Rate']['id'];
			
			$this->Rate->recursive = -1;
			if($this->Rate->saveField('conversation_status','close'))
			{
				$response['type']="success";
				$response['message']="Conversation closed successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in close conversation, try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		echo json_encode($response);
		exit();
		
	}
	
/*	New webservice for gave feedback14  using search parameter */	
	function gave_feedback_v14($user_id=null)
	{
		$response=array();
		if($user_id>0)
		{
			$conditions=array();
			if(isset($this->data['search'])&&$this->data['search']!='')
			{
				
				$search = trim($this->data['search']);
				$keyword= preg_split("/[\s,]+/", $search);
				foreach($keyword as $key)
				{
					$conditions['OR'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%"), 'Drobe.comment LIKE' => "%".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('Rate.comment LIKE' => $key."%"), 'Rate.comment LIKE' => "%".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('User.first_name LIKE' => $key."%"), 'User.first_name LIKE' => "%".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('User.last_name LIKE' => $key."%"), 'User.last_name LIKE' => "%".$key."%"));
					$conditions['OR'][] = array('OR'=>array('OR'=>array('User.username LIKE' => $key."%"), 'User.username LIKE' => "@"."%".$key."%"));
				}
				/*$search = $this->data['search'];
				$conditions['OR']['Rate.comment like']=$search."%";
				$conditions['OR']['Drobe.comment like']=$search."%";
				$conditions['OR']['User.first_name like']=$search."%";
				$conditions['OR']['User.last_name like']=$search."%";*/
			}
			$conditions['Rate.comment !=']="";
			$conditions['Rate.user_id']=$user_id;
			$conditions['Drobe.deleted']=0;
			//$conditions['Rate.rate !=']=0;
			$conditions['Drobe.rate_status']='open';
			
			$this->paginate = array(
					'fields'=>array('SellDrobe.sell_price','Rate.rate','Rate.comment','Rate.conversation_id',
							'Rate.conversation_status','Rate.new_reply','Rate.new_response','Drobe.id',
							'Drobe.unique_id','Drobe.uploaded_on','Category.category_name','Drobe.file_name','Drobe.comment',
							'Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.is_highest_rated','Drobe.total_out',
							'User.first_name','User.username','User.last_name','User.id','User.photo'),
					'joins'=>array(
							array(
									'table' => 'drobes',
									'alias' => 'Drobe',
									'type' => 'INNER',
									'conditions' => array(
											'Drobe.id = Rate.drobe_id'
									)),
							array(
									'table' => 'sell_drobes',
									'alias' => 'SellDrobe',
									'type' => 'LEFT',
									'conditions' => array(
											'SellDrobe.drobe_id=Drobe.id'
									)),
							array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'INNER',
									'conditions' => array(
											'User.id = Drobe.user_id'
									)),
							array(
									'table' => 'categories',
									'alias' => 'Category',
									'type' => 'INNER',
									'conditions' => array(
											'Category.id = Drobe.category_id'
									))
					),
					'conditions'=>$conditions,
					//'order'=>'Rate.new_response DESC, Rate.created_on DESC',
					'order'=>'Rate.new_response DESC, Rate.last_conversation_time DESC',
					'limit'=>10
			);
			$result = $this->paginate('Rate');
			
			
			foreach($result as &$drobe)
			{
	
				$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
				/*$drobe['Drobe']['total_in']=round($drobe['Drobe']['total_in']/$total*100);
				$drobe['Drobe']['total_out']=round($drobe['Drobe']['total_out']/$total*100);*/
				$drobe['Drobe']['total_in']=$drobe['Drobe']['total_in'];
				$drobe['Drobe']['total_out']=$drobe['Drobe']['total_out'];
	
				$drobe['Drobe']['total_rate']="".$drobe['Drobe']['total_rate'];
				$drobe['Drobe']['views']="".$drobe['Drobe']['views'];
	
				$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['category_name']=$drobe['Category']['category_name'];
				$drobe['User']['photo_url']=Router::url("/profile_images/".$drobe['User']['photo'],true);
				$drobe['User']['thumb_url']=Router::url("/profile_images/thumb/".$drobe['User']['photo'],true);
	
				if($drobe['SellDrobe']['sell_price']!=null)
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
	
				unset($drobe['SellDrobe']);
	
				unset($drobe['Drobe']['file_name'],$drobe['User']['photo'],$drobe['Category']);
					
			}
			$this->set('feedbacks',$result);
			$response['type']="success";
			$response['feedback']=$result;
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
/**     End of new developed method for gave feedback  */	
	
	
	
	
	/*
	 * Webservice for gave feedback
	*/
	function gave_feedback($user_id=null)
	{
		$response=array();
		if($user_id>0)
		{
			$conditions=array();
			$conditions['Rate.comment !=']="";
			$conditions['Rate.user_id']=$user_id;
			$conditions['Drobe.deleted']=0;
			$conditions['Rate.rate !=']=0;
			$conditions['Drobe.rate_status']='open';
			
			$this->paginate = array(
					'fields'=>array('SellDrobe.sell_price','Rate.rate','Rate.comment','Rate.conversation_id','Rate.conversation_status','Rate.new_reply','Rate.new_response','Drobe.id','Drobe.unique_id','Category.category_name','Drobe.file_name','Drobe.comment','Drobe.total_rate','Drobe.views','Drobe.total_in','Drobe.is_highest_rated','Drobe.total_out','User.first_name','User.username','User.last_name','User.id','User.photo'),
					'joins'=>array(
							array(
									'table' => 'drobes',
									'alias' => 'Drobe',
									'type' => 'INNER',
									'conditions' => array(
											'Drobe.id = Rate.drobe_id'
									)),
							array(
									'table' => 'sell_drobes',
									'alias' => 'SellDrobe',
									'type' => 'LEFT',
									'conditions' => array(
											'SellDrobe.drobe_id=Drobe.id'
									)),
							array(
									'table' => 'users',
									'alias' => 'User',
									'type' => 'INNER',
									'conditions' => array(
											'User.id = Drobe.user_id'
									)),
							array(
									'table' => 'categories',
									'alias' => 'Category',
									'type' => 'INNER',
									'conditions' => array(
											'Category.id = Drobe.category_id'
									))
					),
					'conditions'=>$conditions,
					//'order'=>'Rate.new_response DESC, Rate.created_on DESC',
					'order'=>'Rate.new_response DESC, Rate.last_conversation_time DESC',
					'limit'=>10
			);
			$result = $this->paginate('Rate');
			foreach($result as &$drobe)
			{
				
				$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
				/*$drobe['Drobe']['total_in']=round($drobe['Drobe']['total_in']/$total*100);
				$drobe['Drobe']['total_out']=round($drobe['Drobe']['total_out']/$total*100);*/
				$drobe['Drobe']['total_in']=$drobe['Drobe']['total_in'];
				$drobe['Drobe']['total_out']=$drobe['Drobe']['total_out'];
				
				$drobe['Drobe']['total_rate']="".$drobe['Drobe']['total_rate'];
				$drobe['Drobe']['views']="".$drobe['Drobe']['views'];
				
				$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
				$drobe['Drobe']['category_name']=$drobe['Category']['category_name'];
				$drobe['User']['photo_url']=Router::url("/profile_images/".$drobe['User']['photo'],true);
				$drobe['User']['thumb_url']=Router::url("/profile_images/thumb/".$drobe['User']['photo'],true);
				
				if($drobe['SellDrobe']['sell_price']!=null)
					$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
				
				unset($drobe['SellDrobe']);
				
				unset($drobe['Drobe']['file_name'],$drobe['User']['photo'],$drobe['Category']);
			
			}
			$this->set('feedbacks',$result);
			$response['type']="success";
			$response['feedback']=$result;
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	/*
	 * webservice for get rate result detail
	*/
	function drobe_result($drobe_id=null)
	{
		$response=array();
		if($drobe_id>0)
		{
			$this->loadModel('Drobe');
			$this->Drobe->recursive=-1;
			
			$drobe=$this->Drobe->find('first',array(
					'joins'=>array(
							array(
									'table' => 'categories',
									'alias' => 'Category',
									'type' => 'INNER',
									'conditions' => array(
											'Category.id = Drobe.category_id'
									)
							)
					),
					'conditions'=>array('Drobe.id'=>$drobe_id),
					'fields'=>array('Drobe.id','Drobe.comment','Drobe.file_name',
							'Drobe.views','Drobe.uploaded_on','Drobe.total_rate','Drobe.short_url','Drobe.total_in','Drobe.total_out',
							'Category.category_name')
					)
			);
			$total=$drobe['Drobe']['total_in']+$drobe['Drobe']['total_out'];
			/* $drobe['Drobe']['total_in']=round($drobe['Drobe']['total_in']/$total*100);
			$drobe['Drobe']['total_out']=round($drobe['Drobe']['total_out']/$total*100); */
			$drobe['Drobe']['total_in']=$drobe['Drobe']['total_in'];
			$drobe['Drobe']['total_out']=$drobe['Drobe']['total_out'];
			$drobe['Drobe']['views']="".$drobe['Drobe']['views'];
			$drobe['Drobe']['total_rate']="".$drobe['Drobe']['total_rate'];
			
			$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
			$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
			$drobe['Drobe']['category']=$drobe['Category']['category_name'];
			
			$this->Drobe->updateAll(array('Drobe.new_response'=>0),array('Drobe.id'=>$drobe_id));
			
			unset($drobe['Drobe']['file_name'],$drobe['Rate'],$drobe['Category']);
			
			$conditions=array();
			$conditions['Rate.drobe_id']=$drobe['Drobe']['id'];
			//$conditions['Rate.rate !=']=0;
			$result=$this->Rate->find('all',array(
					'fields'=>array('Rate.id','Rate.rewarded','Rate.rate','Rate.comment','Rate.conversation_id','Rate.conversation_status','Rate.new_reply','Rate.new_response','User.first_name','User.last_name','User.username','User.photo','User.star_user','User.gender','User.province','User.country','User.id'),
					'joins'=>array(
							array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'INNER',
								'conditions' => array(
									'User.id = Rate.user_id'
								)
							)
					),
					'conditions'=>$conditions,
					'order'=> "Rate.new_reply DESC, Rate.created_on DESC"
			));
			foreach($result as &$rs)
			{
				if($rs['User']['photo']!="")
				{
					$rs['User']['photo']=Router::url("/profile_images/thumb/".$rs['User']['photo'],true);
				}
				
				$rs['User']['gender']=($rs['User']['gender']!=""?$rs['User']['gender']:"");
				
			}
		
			$response['type']="success";
			$response['drobe']=$drobe;
			$response['rates']=$result;
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide drobe id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	/*
	 * Webservice for close conversations
	 */
	function close_drobe_conversation($conversation_id=null)
	{
		$response=array();
		if($conversation_id!=null)
		{
			$rateData=$this->Rate->find('first',array('conditions'=>array('Rate.conversation_id'=>$conversation_id)));
			$this->Rate->recursive=-1;
			$this->Rate->id=$rateData['Rate']['id'];
			if($this->Rate->updateAll(array('Rate.conversation_status'=>'\'close\''),array('Rate.id'=>$rateData['Rate']['id'])))
			{
				$response['type']="success";
				$response['message']="Conversation closed successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="Error occured in close conversation, try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide conversation id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
	}
}
?>