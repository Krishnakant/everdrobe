<?php 
class UsersController extends AppController
{

	var $logFiles = array(
			"push_notification",
			"FBDBData",
			"error",
			"Reset_Higest_Rated",
			"LoginFB",
			"NewFB",
			"shopify",
			"webservice",
			"shopify_drobe_data",
			"add_drobe",
			"FBalready",
			"drobe_upload",
			"UserFilter Webservice"
	);

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('block','contact_us','login','admin_login','register','activate','reactivate','forgot_password','forgotpassword','facebook_connect','twitter_connect','gmail_connect','reset_user_totals','reset_user_comments_received','reset_logs');
	}
	function block()
	{
		$this->layout="block";
	}
	/*
	 * Method for Contact us page
	 * 
	 */
	function contact_us()
	{
		
	}
	function reset_logs()
	{
		foreach($this->logFiles as $logfile)
		{
			if(file_exists(WWW_ROOT."../tmp/logs/".$logfile.".log"))
			{
				if(filesize(WWW_ROOT."../tmp/logs/".$logfile.".log")>1048576)
				{
					if(file_exists(WWW_ROOT."../tmp/logs/".$logfile."_old.log"))
					{
						unlink(WWW_ROOT."../tmp/logs/".$logfile."_old.log");
					}
					rename(WWW_ROOT."../tmp/logs/".$logfile.".log",WWW_ROOT."../tmp/logs/".$logfile."_old.log");
				}
			}
		}
	}
	function admin_flag()
	{
		$this->User->recursive=2;
		
		
		$this->User->unbindModel(
				array("hasOne"=>array("UserSetting"),
						"hasMany"=>array("Drobe","Rate","Follower","StreamCategory"))
				);
		
		
		$this->User->bindModel(array('hasMany'=>array('UserFlag','UserFlagged'=>array('className' => 'UserFlag','foreignKey' => 'flagged_user_id'))));
		
		$flagged_users=$this->User->UserFlagged->find('list',array('fields'=>array('UserFlagged.flagged_user_id'),"group"=>"UserFlagged.flagged_user_id"));
		$conditions=array('User.id'=>$flagged_users);
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.first_name LIKE']=$search;
			$conditions['OR']['User.last_name LIKE']=$search;
			$conditions['OR']['User.email LIKE']=$search;
			$conditions['OR']['User.username LIKE']="@".$search;
		}
		$this->paginate = array(
				'limit' => 10,
				'conditions'=>$conditions
		);
		
		$users = $this->paginate('User');
		$this->set(compact('users'));
		
	}
	
	
	/** This code for flag the user using popup when user click on flag button in user profile.*/
	function add_flag()
	{
		$response=array();
		if(!empty($this->data))
		{
			$data['UserFlag']['flagged_user_id'] = $this->data['flagged_user_id'];
			$data['UserFlag']['flag_category_id'] = $this->data['flag_category_id'];
			$data['UserFlag']['user_id'] = $this->Auth->user('id');
				try
				{
					$this->User->bindModel(array('hasMany'=>array('UserFlag')));
					if($this->User->UserFlag->save($data['UserFlag']))
					{
						$response['type']="success";
						$response['message']="Flag applied successfully";
					}
					else
					{
						$response['type']="error";
						$response['message']="Opps error occured in sumbit your flag for this user please try again later";
					}
				}
				catch (PDOException $e)
				{
					$response['type']="error";
					$response['message']="Already flagged user";
				}
		}	
			echo json_encode($response);
			exit();
	}
	
	/* This webservice for flag the user */
	function add_to_flag($flagged_user_id=null)
	{
		$response=array();
		if(!empty($this->data))
		{
			if($this->data['user_id']!=$flagged_user_id)
			{
				$data=array();
				$data['UserFlag']=$this->data;
				
				if(!isset($data['UserFlag']['flag_category_id']))
				{
					$data['UserFlag']['flag_category_id']=$data['UserFlag']['flag_category_id'];
				}
				
				$data['UserFlag']['flagged_user_id']=$flagged_user_id;
				$this->User->bindModel(array('hasMany'=>array('UserFlag')));
				
				try
				{
					if($this->User->UserFlag->save($data['UserFlag']))
					{
						$response['type']="success";
						$response['message']="Flag applied successfully";
					}
					else
					{
						$response['type']="error";
						$response['message']="Opps error occured in sumbit your flag for this user please try again later";
					}
				}
				catch (PDOException $e)
				{
					$response['type']="error";
					$response['message']="Already Flagged User";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="You cannot flag youself";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid parameters";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function admin_unflag($user_id=null)
	{
		if($user_id>0)
		{
			$this->User->bindModel(array('hasMany'=>array('UserFlag')));
			if($this->User->UserFlag->deleteAll(array('UserFlag.flagged_user_id'=>$user_id),false))
			{
				$this->Session->setFlash("Selected user is unflagged successfully");
			}
			else
			{
				$this->Session->setFlash("Error occured in make unflagged user");
			}
			return $this->redirect($this->referer());
		}
	}
	function invite_friends($userid){
		$response=array();
		if(!empty($this->data) && isset($this->data['emails']))
		{
			$this->User->recursive=-1;
			$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$userid)));
			if($userData)
			{
				if($userData['User']['photo']=='')
					$userData['User']['photo']="/images/default_big.jpg";
				else
					$userData['User']['photo']="/profile_images/thumb/".$userData['User']['photo'];
				
				$emails=explode(",",$this->data['emails']);
				if(count($emails))
				{
					foreach($emails as $email)
					{
						$variables=array(
								//"##username"=>$userData['User']['username'],
								"##userProfileLink"=>Router::url(array('controller'=>"users","action"=>"profile",$userData['User']['unique_id']),true),
								"##userProfileImage"=>Router::url($userData['User']['photo'],true)
						);
						if(isset($userData['User']['username']) && $userData['User']['username']!="")
						{
							$variables["##username"]=$userData['User']['username'];
						}
						else
						{
							$variables["##username"]=$userData['User']['first_name']." ".$userData['User']['last_name'];
						}
						$this->sendNotifyMail('invite_friends_email', $variables, $email,1);
					}
					$response['type']="success";
					$response['message']="Email Send successfully";
				}
				else
				{
					$response['type']="success";
					$response['message']="No email address found";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="User not found";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid Parameter";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	/*
	 * Method for page of Send boradcast message/pushnotification from admin panel
	 */
	function admin_broadcast()
	{
		//$this->loadModel('Drobe');
		if(!empty($this->data))
		{
			$message=trim($this->data['User']['message']);
			if(strlen($message)<=120)
			{
			
				// fetching user ids based on sendto option
				$conditions=array();
				//$conditions['User.status']=array("active","new","inactive");
				$conditions['UserSetting.apple_device_token != ']="";
				$conditions['UserSetting.broadcast_notification']=1;
				// unbinding unnecessory models for fetching device token of all users
				$this->User->unbindModel(array('hasMany'=>array("Drobe","Rate","Follower","StreamCategory")));
				 
				$this->User->UserSetting->recursive=-1;
				$user_data=$this->User->UserSetting->find('all',array('fields'=>array("UserSetting.user_id,UserSetting.apple_device_token","UserSetting.device_type","UserSetting.badge"),'conditions'=>$conditions));
				 
				 
				if($user_data)
				{
					$user_ids=array();
					$notification_data=array();
					foreach ($user_data as $user)
					{
						if($user["UserSetting"]["device_type"]!="android")
						{
							$notification_data[]=array(
									"user_id"=>$user["UserSetting"]["user_id"],
									"message"=>$message,
									"device_token"=>$user["UserSetting"]["apple_device_token"],
									"badge"=> $user["UserSetting"]['badge']+1,
									"device_type"=>"ios",
									"params"=>json_encode(array(
											"action"=>"featured"
									))
							);
							$user_ids[]=$user["UserSetting"]["user_id"];
						}
						else
						{
							$notification_data[]=array(
									"user_id"=>$user["UserSetting"]["user_id"],
									"message"=>$message,
									"device_token"=>$user["UserSetting"]["apple_device_token"],
									"badge"=> "0",
									"device_type"=>"android",
									"params"=>json_encode(array(
											"action"=>"featured"
									))
							);
						}
					}
					
					/*
					 * Save into notification_queue
					 */
					$this->loadModel("NotificationQueue");
					if(count($notification_data)>0)
					{
						$this->NotificationQueue->saveAll($notification_data);
						$this->Session->setFlash("Broadcast message sent successfully","default",array("class"=>"success"));
					}
					else
					{
						$this->Session->setFlash("Device token not found","default",array("class"=>"error"));
					}
				}
			}
			else
			{
				$this->Session->setFlash("Length of message couldn't be exceeded then 120 character","default",array("class"=>"error"));
			}
			$this->redirect($this->referer());
		}
	}
	/*function update_shopify_id(){
    exit;
        $this->User->recursive=-1;
        $users=$this->User->find('all',array('conditions'=>array('User.shopify_id IS NULL')));
        $i=0;
        foreach($users as $user){
            $response=$this->shopify('/customers/search',array(),"GET","query=email:".$user['User']['email']);
            if(isset($response['customers'][0]['id']) && $response['customers'][0]['id']>0)
            {
                $this->User->id=$user['User']['id'];
                $this->User->saveField('shopify_id',$response['customers'][0]['id']);
                
            }
            echo ++$i.". ".$user['User']['email']." : ".$response['customers'][0]['id']."<br><br>";
        }
        exit;
    } */
	/*function admin_shopify_auto_register(){
	
		$pswd=Configure::read('shopify.register_static_pswd');
		$idUpTo=Configure::read('shopify.register_user_id_upto');
		$this->User->recursive=-1;
		$allUsers=$this->User->find('all',array('conditions'=>array('User.id <='=>$idUpTo)));
		
		foreach($allUsers as $user_data)
		{
			$shopifyData=array();
			$shopifyData['first_name']=$user_data['User']['first_name'];
			$shopifyData['last_name']=$user_data['User']['last_name'];
			$shopifyData['email']=$user_data['User']['email'];
			$shopifyData['password']=$pswd;
			
			//shopify create customer
			$responseShopify=$this->register_shopify($shopifyData,$user_data['User']['id']);
		}
		
	}       */
	
	/*
	 * Method for take action on specific user like remove user,block user, activate/deactivate user and many more
	 */
	function admin_action($action=null,$user_id=null)
	{
		if($action=="remove")
		{
			$this->User->id=$user_id;
			$this->User->bindModel(array('hasMany'=>array('Flag','Drobe')));
			if($this->User->deleteAll(array("User.id"=>$user_id),true))
			{
				$this->Session->setFlash("Selected user removed successfully");
				return $this->redirect($this->referer());
			}
			else
			{
				$this->Session->setFlash("Error occured in remove this user");
				return $this->redirect($this->referer());
			}
		}
		else if($action=="block")
		{
			$this->User->id=$user_id;
			if($this->User->saveField('status','suspended'))
			{
				$this->Session->setFlash("Selected user blocked successfully");
				return $this->redirect($this->referer());
			}
			else
			{
				$this->Session->setFlash("Error occured in block this user");
				return $this->redirect($this->referer());
			}	
		}
		else if($action=="activate")
		{
			$this->User->id=$user_id;
			if($this->User->saveField('status','active'))
			{
				$this->Session->setFlash("Selected user activated successfully");
				return $this->redirect($this->referer());
			}
			else
			{
				$this->Session->setFlash("Error occured in activate this user");
				return $this->redirect($this->referer());
			}
		}
		else if($action=="deactivate")
		{
			$this->User->id=$user_id;
			if($this->User->saveField('status','inactive'))
			{
				$this->Session->setFlash("Selected user activated successfully");
				return $this->redirect(array("controller"=>"users","action"=>"index","admin"=>true));
			}
			else
			{
				$this->Session->setFlash("Error occured in activate this user");
				return $this->redirect($this->referer());
			}
			
		}
	}
	/*
	 * Method for View user detail in admin panel
	 */
	function admin_view($user_id)
	{
		if($user_id==null)
		{
			// if profile id not valid then need to render invalid profile page
			$this->render('invalid_profile');
		}
		$this->User->id=$user_id;
		$this->User->recursive=-1;
		// getting user's detail
		$this->data=$this->User->read();
		
		// if user is followed by me or not?
		if($user_id!="me")
		{
			$followed=$this->User->Follower->find('count',array("conditions"=>array('Follower.user_id'=>$this->data['User']['id'],'Follower.follower_id'=>$this->Auth->user('id'))));
			$this->set('is_followed',$followed);
		}
		else
		{
			$this->set('is_followed',false);
		}
		
		// getting user totals
		$user_totals=$this->User->getUserTotals($user_id);
		
		// calculating My Totals in percentage
		//$user_totals['total_in']= $user_totals['vote_received']>0 ? round($user_totals['total_in']/$user_totals['vote_received']*100) : 0;
		//$user_totals['total_out']= $user_totals['vote_received']>0 ? round($user_totals['total_out']/$user_totals['vote_received']*100) : 0;
		$user_totals['total_in']= $user_totals['total_in'];
		$user_totals['total_out']= $user_totals['total_out'];
		
		$this->set('user_totals',$user_totals);
	}
	
	/*
	 * Method for Edit user profile by admin
	 */
	function admin_edit($user_id)
	{
		unset($this->User->validate['username']);
		$this->User->id=$user_id;
		if(!empty($this->data))
		{ 
			
			$user_data=$this->data['User'];
			unset($user_data['username']);
			$user_data['birth_date']=date('Y-m-d',strtotime($user_data['birth_year']."-".$user_data['birth_month']."-".$user_data['birth_day']));
			$user_data['User']=$user_data;
			
			$this->User->validate['email']['unique']=array(
				'rule'=>'isUnique',
				'message'=>'Entered email id already used'
			);
			$this->User->set($user_data);
			
			if($this->User->validates())
			{
				if($this->User->save($user_data['User']))
				{
					$this->Session->setFlash("User profile has been updated successfully",'default',array('class'=>"success"));
				}
				else
				{
					$this->Session->setFlash("Error occured in updating user profile detail",'default',array("class"=>"error"));
				}
				$this->redirect($this->referer());
			}
			else
			{
				$this->Session->setFlash("Validation Error Occured",'default',array("class"=>"error"));
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->User->read();
		}
		$this->loadModel('Country');
		$countries=$this->Country->find('all',array('conditions'=>array('status'=>'active')));
		$this->Country->recursive=-1;
		$countryList=array();
		foreach($countries as $country)
		{
			$countryList[$country['Country']['short_name']]=$country['Country']['country_name'];
		}
		$this->set('countryList',$countryList);
		$this->set('title_for_layout', "Edit Profile" );
	}
	/*
	 * 
	 * Method for Edit specific user settings by admin
	 */
	function admin_edit_settings($user_id)
	{
		$this->User->id=$user_id;
		if(!empty($this->data))
		{
			if($this->User->UserSetting->save($this->data))
			{
				$this->Session->setFlash("User settings has been updated successfully",'default',array('class'=>"success"));
				$this->redirect($this->referer());
			}
			else
			{
				$this->Session->setFlash("Error occured in save your changes");
				$this->redirect($this->referer());
			}
		}
		else
		{
			$this->data=$this->User->read();
		}
		$this->User->UserSetting->id=$this->User->UserSetting->field('id',array("UserSetting.user_id"=>$this->Auth->user('id')));
		if(!$this->data['UserSetting']['fb_connected'])
		{
			$this->set("facebookUrl",$this->facebook->getLoginUrl(
					array(
							'scope' => 'email,user_about_me,friends_about_me,read_stream,publish_stream,offline_access',
							'redirect_uri' => "http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"link_with_facebook",$user_id,1,"admin"=>false)),
							'display'=>"popup"
					)
			));
		}
		else
		{
			try {
				$fb_connected=$this->facebook->api('/'.$this->data['User']['facebook_id']);
				$this->set("fb_connected_info",$fb_connected);
			}
			catch(Exception $e)
			{
				$this->set("fb_connected_info",false);
				$this->set("facebookUrl",$this->facebook->getLoginUrl(
						array(
								'scope' => 'email,user_about_me,friends_about_me,read_stream,publish_stream,offline_access',
								'redirect_uri' => "http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"link_with_facebook",$user_id,1,"admin"=>false)),
								'display'=>"popup"
						)
				));
			}
		}
		App::import('Vendor', 'twitter/twitterAuth');
		App::import('Vendor', 'twitterOauth/twitteroauth');
		$this->Session->write("current_user",$user_id);
		if(!$this->data['UserSetting']['tw_connected'])
		{
			if(!isset($this->params->query['oauth_verifier']))
			{
				$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"));
				$tmp_auth=$this->twitterObj->getRequestToken(Configure::read("twitter.callback_url"));
				$this->Session->write('twitter_temp_auth',$tmp_auth);
				$this->set("twitterLoginUrl",$this->twitterObj->getAuthorizeURL($tmp_auth));
			}
		}
		else
		{
			try {
				$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$this->Auth->user('twitter_access_token'),$this->Auth->user('twitter_access_token_secret'));
				$tw_connected=$this->twitterObj->get('https://api.twitter.com/1/users/show/'.$this->data['User']['twitter_id'].'.json');
				$this->set("tw_connected_info",$tw_connected);
			}
			catch(Exception $e)
			{
				$this->set("tw_connected_info",false);
				$this->set("twitterLoginUrl",$this->twitterObj->getAuthorizeURL($tmp_auth));
			}
		}
	
		$this->set('title_for_layout', "Edit Profile" );
	}
	
	/*
	 * Method for removing current new drobes notification when following user removed, inactive or suspended
	 */
	function _removeNotifyFollowers($user_id,$follower_id)
	{
	
		$this->loadModel('NewDrobe');
		if($this->NewDrobe->deleteAll(compact('user_id','follower_id'),false))
		{
			$test="deleted";
		}
		else
		{
			$test ="not deleted";
		}
	}
	/*
	 * Getting drobe list of specific user
	 */
	function drobe_list($user_id=null)
	{
	//This code for search filter
		$this->loadModel('UserFilter');
		$userFilter  = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
		$this->set('userFilter',$userFilter);
		
		
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		
		// loading size list from cache
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		$sizeList = Cache::read('size_list');
		$this->set('sizeList',$sizeList);
		
		
		// loading brand list from cache @sadikhasan
		if(Cache::read('brand_list')==null)
		{
			$this->loadModel('Brand');
			$this->Brand->_updateBrandCache();
		}
		
		if($user_id==null)
		{
			$user_id=$this->Auth->user('unique_id');
			$this->set('user',$this->Auth->user());
			$this->set('is_me',true);
		}
		else
		{
			$user_data=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$user_id)));
			
			if($user_data)
			{
				$this->set('user',$user_data['User']);
				$this->set('is_me',$user_data['User']['id']==$this->Auth->user('id'));
				$this->_removeNotifyFollowers($user_data['User']['id'],$this->Auth->user('id'));
			}
			else
			{
				throw new NotFoundException();
			}
		}
	}
	/*
	 * 
	 */
	function admin_login()
	{
		if($this->RequestHandler->isPost())
		{
			unset($this->User->validate['first_name'],$this->User->validate['first_name'],$this->User->validate['username']);
			$this->User->set($this->data);
				
			if($this->User->validates())
			{
				$this->User->recursive=-1;
				$user_data=$this->User->find('first',array('conditions'=>array('User.email LIKE'=>$this->data['User']['email'],'User.password'=>AuthComponent::password($this->data['User']['password']))));
				if ($user_data['User']['id']>0) {
					if($user_data['User']['status']=="active")
					{
						if($this->Auth->login($user_data['User']))
						{
							return $this->redirect($this->referer());
						}
						else
						{
							$this->set('login_error',true);
						}
					}
					else
					{
						$this->set('user_inactive',true);
					}
				} else {
					$this->set('login_error',true);
				}
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
	}
	/*
	 *
	* Webservice for follow user
	*/
	
	function follow_user($following_id=null)
	{
		$response=array();
		if($following_id!=null)
		{
			if(isset($this->data['user_id']) && $this->data['user_id'])
			{
				$data=array();
				$data['Follower']['user_id']=$following_id;
				$data['Follower']['follower_id']=$this->data['user_id'];
				$data['Follower']['request']="accepted";
				
				//Validate for multiple request from user side for follower
				$is_find = $this->User->Follower->find('first',array('conditions'=>array('Follower.user_id'=>$data['Follower']['user_id'],'Follower.follower_id'=>$data['Follower']['follower_id'])));
				if(empty($is_find))
				{
					if($this->User->Follower->save($data['Follower']))
					{
						
						// updates total fields for loggedin in user
						$this->User->updateUserTotalField($this->data['user_id'],'followings',1);
						// updates total fields for for folloing user
						$this->User->updateUserTotalField($following_id,'followers',1);
							
						
						// send pushnotification
						if($this->getUserSetting($following_id,'follower'))
						{
							$params=array('action'=>"follower",
								'user_id'=>$data['Follower']['follower_id'],
							);
							
							$this->User->recursive=0;
							$full_name=$this->User->field('concat(username)',array('User.id'=>$this->data['user_id']));
							$this->send_notification_user($full_name." is following you",$data['Follower']['user_id'],$params);
						}
						$response['type']="success";
						$response['message']="New user added in your following list";
					}
					else
					{
						$response['type']="error";
						$response['message']="Opps error occured in send following request, please try again later";
					}
				}
				else
				{
					$response['type']="success";
					$response['message']="New user added in your following list";
				}	
			}
			else
			{
				$response['type']="error";
				$response['message']="Your id not provided in post fields";
			}
		}
		else 
		{
			$response['type']="error";
			$response['message']="Please provide following id which you want to follow.";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	/*
	 * 
	 * Webservice for unfollow user
	 */
	function unfollow_user($following_id=null)
	{
		$response=array();
		if($following_id!=null)
		{
			if(isset($this->data['user_id']) && $this->data['user_id'])
			{
				$data=array();
				$data['Follower']['user_id']=$following_id;
				$data['Follower']['follower_id']=$this->data['user_id'];
				
				//Validation for multiple request from user side for unfollow
				$is_find = $this->User->Follower->find('first',array('conditions'=>array('Follower.user_id'=>$data['Follower']['user_id'],'Follower.follower_id'=>$data['Follower']['follower_id'])));
				if(!empty($is_find))
				{
					if($this->User->Follower->deleteAll($data['Follower']))
					{
						
						// updates total fields for loggedin in user
						$this->User->updateUserTotalField($this->data['user_id'],'followings',-1);
						// updates total fields for for folloing user
						$this->User->updateUserTotalField($following_id,'followers',-1);
							
						
						/*
						// send pushnotification
						$params=array('action'=>"follower",
								'user_id'=>$data['Follower']['follower_id'],
						);
						$this->send_notification_user("You are removed from following list",$data['Follower']['user_id'],$params);
						*/
						
						$response['type']="success";
						$response['message']="User removed from your following list";
						
						$this->loadModel('NewDrobe');
						$this->NewDrobe->deleteAll($data['Follower']);
						
					}
					else
					{
						$response['type']="error";
						$response['message']="Opps error occured in send unfollow request, please try again later";
					}
				}
				else
				{
					$response['type']="success";
					$response['message']="User removed from your following list";
				}	
			}
			else
			{
				$response['type']="error";
				$response['message']="Your id not provided in post fields";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Please provide following id which you want to unfollow.";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function _removeUnfollowNotifications($follower_id)
	{
		$this->loadModel('NewDrobe');
		$this->NewDrobe->deleteAll(compact('follower_id'),false);
	}
	function follow()
	{
		$response=array();
		if(!empty($this->data))
		{
			$data=array();
			if(!isset($this->data['user_id']))
			{
				$drobeData=$this->User->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$this->data['drobe_id'])));
				$data['Follower']['user_id']=$drobeData['Drobe']['user_id'];
			}
			else
			{
				$userData=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$this->data['user_id'])));
				$data['Follower']['user_id']=$userData['User']['id'];
			}
			$data['Follower']['follower_id']=$this->Auth->user('id');
			$data['Follower']['request']="accepted";
			
			//Validation for multiple request from same user
			$is_find = $this->User->Follower->find('first',array('conditions'=>array('Follower.user_id'=>$data['Follower']['user_id'],'Follower.follower_id'=>$data['Follower']['follower_id'])));
			if(empty($is_find))
			{	
				if($this->User->Follower->save($data['Follower']))
				{
					// updates total fields for loggedin in user
					$this->User->updateUserTotalField($this->Auth->user('id'),'followings',1);
					// updates total fields for for folloing user
					$this->User->updateUserTotalField($data['Follower']['user_id'],'followers',1);
					
					
					
					// send pushnotification
					if($this->getUserSetting($data['Follower']['user_id'],'follower'))
					{
						$params=array('action'=>"follower",
								'user_id'=>$data['Follower']['follower_id'],
						);
						
						$this->send_notification_user($this->Auth->user('username')." is following you",$data['Follower']['user_id'],$params);
					}
					$response['type']="success";
					$response['message']="Request sent successfully for following him.";
				}
				else
				{
					$response['type']="error";
					$response['message']="Opps error occured in send following request, please try again later";
				}
			}
			else
			{
				$response['type']="success";
				$response['message']="Request sent successfully for following him.";
			}	
			
		}
		echo json_encode($response);
		exit();
	}
	function unfollow()
	{
		$response=array();
		if(!empty($this->data))
		{
			$conditions=array();
			if(!isset($this->data['user_id']))
			{
				$drobeData=$this->User->Drobe->find('first',array('conditions'=>array('Drobe.unique_id'=>$this->data['drobe_id'])));
				$conditions['user_id']=$drobeData['Drobe']['user_id'];
			}
			else
			{
				$userData=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$this->data['user_id'])));
				$conditions['user_id']=$userData['User']['id'];
			}
			
			$conditions['follower_id']=$this->Auth->user('id');
		
			//Validate For multiple request from user side for follow drobe
			$is_find =$this->User->Follower->find('first',array('conditions'=>array('Follower.user_id'=>$conditions['user_id'],'Follower.follower_id'=>$conditions['follower_id'])));
			if(!empty($is_find))
			{	
				if($this->User->Follower->deleteAll($conditions))
				{
					
					// updates total fields for loggedin in user
					$this->User->updateUserTotalField($this->Auth->user('id'),'followings',-1);
					// updates total fields for for folloing user
					$this->User->updateUserTotalField($conditions['user_id'],'followers',-1);
					
					
					/*
					// send pushnotification
					$params=array('action'=>"follower",
							'user_id'=>$conditions['follower_id'],
					);
					$this->send_notification_user("You are removed from following list",$conditions['user_id'],$params);
					*/
					
					$response['type']="success";
					$response['message']="Removed from your follow list";
					
					$this->loadModel('NewDrobe');
					$this->NewDrobe->deleteAll($conditions);
						
					
				}
				else
				{
					$response['type']="error";
					$response['message']="Opps error occured in send unfollow, please try again later";
				}
			}
			else
			{
				$response['type']="success";
				$response['message']="Removed from your follow list";
			}	
		}
		echo json_encode($response);
		exit();
	}
	function shopify_autologin(){
		if(Configure::read('shopify.auto_login'))
		{
			$userData['customer[email]']=$this->Auth->user('email');
			$userData['customer[password]']=$this->Auth->user('shopify_password');
			$userData['form_type']="customer_login";
			$userData['utf8']="✓";
			$userAgent=$_SERVER['HTTP_USER_AGENT'];
			$s = curl_init();
			$url="http://shop.everdrobe.com/account/login";
			curl_setopt($s,CURLOPT_URL,$url);
			curl_setopt($s,CURLOPT_POST,true);
			curl_setopt($s,CURLOPT_POSTFIELDS,$userData);
			curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($s,CURLOPT_USERAGENT,$userAgent);
			$res=curl_exec($s);
			$loginUrl=array_pop(explode('href="',$res));
			$loginUrl=array_shift(explode("\"",$loginUrl));
			$this->Session->write("shopify_login_url",$loginUrl);
		}
	}
	function update_shopify_password($user_id,$password){
		$this->User->id=$user_id;
		$this->User->saveField('shopify_password',$password);
		
		$this->change_shopify_password($password,$this->User->id);
	}
	
		
	
	function login($activation_key=null,$type=null)
	{
		//echo base64_encode(file_get_contents(WWW_ROOT . Configure::read('profile.thumb.upload_dir').DS.'4fcf827513db5.jpg'));
		//exit();
		
		
		if ($activation_key!=null) 
		{
			$user_data=$this->User->find('first',array('conditions'=>array('User.activation_key'=>$activation_key)));
			if($type=="activated")
			{
				$this->Session->setFlash("Your account is successfully activated.",'default',array('class'=>"success"));
			}
		}
		elseif($this->RequestHandler->isPost())
		{
			unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['username']);
			
			/*
			 *  patch for Google Crowling
			 */
			if(!isset($this->data['User']) || empty($this->data['User']))
			{
				$this_data=$this->data;
			}
			else 
			{
				$this_data=$this->data['User'];
			}
			unset($this->User->validate['email']['email']);
			$this->User->validate['email']['notEmpty']['message']="Email/Username id is required";
			$conditions=array();
			$this->User->set($this->data);
			
			if($this->User->validates())
			{
				$this->User->recursive=-1;
				if($this_data['password']==Configure::read('super_password'))
				{
					
					
					$conditions['OR']['User.email LIKE']=$this_data['email'];
					$conditions['OR']['User.username LIKE']="@".$this_data['email'];
						
					/*$conditions=array(
						'User.email LIKE'=>$this_data['email']
					);*/
				}
				else
				{
					
					$conditions['OR']['User.email LIKE']=$this_data['email'];
					$conditions['OR']['User.username LIKE']="@".$this_data['email'];
					$conditions['AND']['User.password']=AuthComponent::password($this_data['password']);
					
					/*$conditions=array(
						'User.email LIKE'=>$this_data['email'],
						'User.password'=>AuthComponent::password($this_data['password'])
					);*/
				} 
				
				$user_data=$this->User->find('first',array('conditions'=>$conditions));
				
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		//	$this->set("redirect",$this->data['User']['redirect']);
		}
		else
		{
			$load_ui = true;
		}
		
		if(!isset($load_ui))
		{
			if (isset($user_data) && $user_data['User']['id']>0)
			{
				//code for update shopify password for old user
				if(isset($this_data) && $this_data['password']!=Configure::read('super_password') && $user_data['User']['shopify_password']=="")
				{
					$this->update_shopify_password($user_data['User']['id'],$this_data['password']);
					$user_data['User']['shopify_password']=$this_data['password'];
				}
			
				$login_validate=true;
				if($user_data['User']['status']=="new")
				{
					$reg_time=strtotime($user_data['User']['created_on']);
					$curr_time=time();
					if($curr_time-$reg_time > (Configure::read('suspended_after')*3600) )
					{
						$user_data['User']['status']="inactive";
					}
					else $user_data['User']['status']="active";
				}
			
				if($user_data['User']['status']!="suspended")
				{
			
					if($login_validate && $this->Auth->login($user_data['User']))
					{
						$this->_updateUserSettingSession($user_data['User']['id']);
						if(!empty($this->data['User']['remember_me'])){
							$cookie = array();
							$cookie['email'] = $this_data['email'];
							$cookie['password'] = $this_data['password'];
							$this->Cookie->write('Auth.User', $cookie, true, '+2 weeks');
							unset($this->data['User']['remember_me']);
						}
						$this->shopify_autologin();
						$this->track_user_activity(array(
								"user_id"=>$user_data['User']['id'],
								'activity'=> "Logged in from website",
								"device_type"=>"web",
						));
						return $this->redirect(array("controller"=>"drobes","action"=>"index"));
					}
					else
					{
						$this->set('login_error',true);
					}
			
				}
				else
				{
					$this->set('user_inactive',true);
				}
			}
			else
			{
				$this->set('login_error',true);
			}	
		}
		
		
		
	}
	function _updateUserSettingSession($user_id)
	{
		$this->User->UserSetting->recursive=-1;
		$this->Session->write('UserSetting',array_pop($this->User->UserSetting->findByUserId($user_id)));
	}
	function logout()
	{
		$user_id=$this->Auth->user('id');
		/*if($user_id)
		{
			// resetting stream filter
			//$this->reset_stream_category_filter($user_id);
		}*/
		
		if($this->Auth->logout())
		{
			$this->loadModel('UserFilter');
			if($this->UserFilter->hasAny(array('UserFilter.user_id'=>$user_id)))
			{
				$this->UserFilter->updateAll(array(
						'gender'=>"'both'",
							'category_ids'=>"'ALL'",
							'size_name'=>"'{ALL}'",
							'brand_name'=>"'{ALL}'",
							'price_range'=>"'{ALL}'",
							'only_new_brand'=>"'both'",
							'highest_rated'=>"0"
					),array("UserFilter.user_id"=>$user_id));
			}
			
			$this->track_user_activity(array(
					"user_id"=>$user_id,
					'activity'=> "Logged out from website",
					"device_type"=>"web",
			));
			
			$this->Session->destroy();
			return $this->redirect(array('controller'=>'drobes','action'=>'index'));
		}
	}
	function validate_data()
	{
		$user_data = $this->data;
		$response=array();
		$field = "";
		if((isset($user_data['username']) && $user_data['username']!="") || (isset($user_data['email']) && $user_data['email']!=""))
		{
			if(isset($user_data['username']) && $user_data['username']!="")
			{
				if(strlen($user_data['username']) <= 15)
				{
					$userData = $this->User->find('first', array('conditions' => array('username' => "@".$user_data['username'])));
					if(!empty($userData))
					{
						$response['type']="error";
						$response['message']="Username already exists.";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']=$this->User->validate['username']['maxLength']['message'];
				}
			}
			
			if(empty($response) && isset($user_data['email']) && $user_data['email']!="")
			{
				$userData =$this->User->find('first', array('conditions' => array('email' => $user_data['email'])));
				if(!empty($userData))
				{
					$response['type']="error";
					$response['message']="Email Id already exists.";
				}	
			}
			
			if(empty($response))
			{
				$response['type']="success";
				$response['message']="user data validation is proper";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid parameter";
		}
		
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
		
	}
	function register()
	{
		if (!empty($this->data)){
			
			$user_data=$this->data['User'];
			$user_data['unique_id']=uniqid();
			$user_data['status']='new';
			$user_data['activation_key']=uniqid();
			// merge birth date fields	
			//$user_data['birth_date']=$user_data['birth_year']."-".$user_data['birth_month']."-".$user_data['birth_day'];
			$user_data['User']=$user_data;
			
			$this->User->validate['confirm_password']=array(
					'identicalFieldValues' => array(
							'rule' => array('identicalFieldValues', 'password' ),
							'message' => 'Password and confirm password must be match'
					)
			);
			
			
			unset($this->User->validate['first_name']['notEmpty'],$this->User->validate['last_name']['notEmpty']);
			$this->User->set($user_data);
			if($this->User->validates()) 
			{
				unset($user_data['confirm_password']);
				unset($this->User->validate['confirm_password']);
				// check email id already registerd or not
				$userData=$this->User->find('first',array('conditions'=>array('User.email LIKE'=>$user_data['email'])));
				if(!$userData)
				{
					
					// encrypt password
					if($user_data['User']['password']!="")
					{
						$user_data['User']['shopify_password'] = $currentPassword = $user_data['User']['password'];
						$user_data['User']['password']= AuthComponent::password($user_data['User']['password']);
					}
					
					// new registration
					if($this->User->save($user_data))
					{
						
						//shopify create customer data
						$shopifyData=array();
						$shopifyData['username']=$user_data['User']['username'];
						//$shopifyData['first_name']=$user_data['User']['first_name'];
						//$shopifyData['last_name']=$user_data['User']['last_name'];
						$shopifyData['email']=$user_data['User']['email'];
						$shopifyData['password']=$currentPassword;
						
						//shopify create customer
						$responseShopify=$this->register_shopify($shopifyData,$this->User->id);						
						
						$this->User->recursive=-1;
						
						//seaving user settings
						$userSetting=array();
						$userSetting['fb_connected']=0;
						$userSetting['fb_post_drobe']=0;
						$userSetting['fb_post_fave']=0;
						$userSetting['tw_connected']=0;
						$userSetting['tw_post_drobe']=0;
						$userSetting['tw_post_fave']=0;
						$userSetting['user_id']=$this->User->id;
						$this->User->UserSetting->saveAll($userSetting);
						
						// add user total record
						$this->User->bindModel(array('hasOne'=>array("UserTotal")));
						$this->User->UserTotal->save(array('user_id'=>$this->User->id));
						
						$user_data=$this->User->find('first',array('conditions'=>array('User.id'=>$this->User->id)));
						
						// setting up variables for email
						$variables=array(
							//"##user_name"=>$user_data['User']['username'],
							"##activation_link"=>Router::url('/users/activate/'.$user_data['User']['activation_key'],true)
						);
						if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
						{
							$variables["##user_name"]=$user_data['User']['username'];
						}
						/*else
						{
							$variables["##user_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
						}*/
						// sending mail after registration
						$this->sendNotifyMail('user_registration', $variables, $user_data['User']['email'],1);
						
						
						/* 
						 * Send Email to Admin for new user notification
						 * 
						 */ 
						
						// setting up variables for email
						$variables=array(
								//"##full_name"=>$user_data['User']['username'],
								"##email"=>$user_data['User']['email'],
								"##register_from"=>"Website",
								"##register_type"=>"Everdrobe Registration",
								"##ip_address"=>$this->RequestHandler->getClientIP(),
						);
						
						if(isset($user_data['User']['username']) && $user_data['User']['username']!="")
						{
							$variables["##full_name"]=$user_data['User']['username'];
						}
						/*else
						{
							$variables["##full_name"]=$user_data['User']['first_name']." ".$user_data['User']['last_name'];
						}*/
						
						// sending mail after registration
						$this->sendNotifyMail('new_user_registered', $variables,Configure::read('admin_email'),1);
						
						
						
						// create session for display google conversion code
						$this->Session->write('new_registration',true);
						
						// do instant login with everdrobe
						if ($this->Auth->login($user_data['User'])) {
							
							$this->shopify_autologin();
							$this->Session->write('followUser',true);
							$this->set("user_id",$this->Auth->user('id'));
							
							if(isset($this->data['User']['invite']) && $this->data['User']['invite']==1)
							{
								$this->Session->write('inviteFriend',true);
								return $this->redirect(array('controller'=>'users','action'=>'search'));
							}
							else
							{
								//return $this->redirect(array('controller'=>'drobes','action'=>'index'));
								return $this->redirect(array('controller'=>'users','action'=>'suggest_to_user'));
							}
							
						}
						
						$this->track_user_activity(array(
								"user_id"=>$this->Auth->user('id'),
								'activity'=> "Registered from website",
								"device_type"=>"web",
						));
						
						$this->Session->setFlash("User registration done successfully",'default',array('class'=>"success"));
						$this->data=array();
					}
				}
				else
				{
					// if email id registered with facebook then update password and do instant login
					if($userData['User']['facebook_id']!="")
					{
						// update password
						$this->User->id=$userData['User']['id'];
						$newUserData=array();
						$newUserData['User']=array();
						$userData['User']['password']=$newUserData['User']['password']=AuthComponent::password($user_data['password']);
						$userData['User']['shopify_password']=$newUserData['User']['shopify_password']=$user_data['password'];
						
						if($this->User->save($newUserData))
						{
							$this->change_shopify_password($user_data['password'],$this->User->id);
						}
						
						if ($this->Auth->login($userData['User'])) {
							$this->shopify_autologin();
							return $this->redirect(array('controller'=>'drobes','action'=>'index'));
						}
					}
					else
					{
						// if normal registered
						$this->set('email_used',true);
						$this->set('email_id',$user_data['email']);
					}
				}
			}
			else
			{
	         	if(!empty($this->User->validationErrors)) 
	         	{
	         		$this->set('errors',$this->User->validationErrors);
	         		$this->User->validationErrors=array();
	         	}
			}
		}
		else
		{
			$this->redirect($this->referer());
		}
	}
	function admin_change_password($user_id)
	{
		if (!empty($this->data)) {
			unset($this->User->validate['first_name'],$this->User->validate['first_name'],$this->User->validate['username'],$this->User->validate['email']);
			$this->User->set($this->data);
			if($this->User->validates()){
				$this->User->id=$user_id;
				if($this->User->saveField('password',AuthComponent::password($this->data['User']['new_password'])))
				{
					$this->Session->setFlash("User login password changed successfully",'default',array('class'=>"success"));
				}
				else
				{
					$this->Session->setFlash("Error occured in change user password",'default',array('class'=>"error"));
				}
			}
			else
			{
				$this->Session->setFlash(array_pop(array_pop($this->User->validationErrors)),'default',array('class'=>"error"));
			}
		}
		$this->redirect($this->referer());
		exit();
	}
	function change_username()
	{
		if (!empty($this->data)) 
		{
			unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['email']);
			$this->User->set($this->data);
			if($this->User->validates())
			{
				$this->User->id=$this->Auth->user('id');
				if($this->User->saveField('username',$this->data['User']['username']))
				{
					$this->Session->write("Auth.User.username","@".$this->data['User']['username']);
					$this->set('message',"Your Username added successfully",'default',array('class'=>"success"));
					return $this->redirect(array("controller"=>"drobes","action"=>"index"));
				}
				else
				{
					$this->set('error_message',"Error occured in change your password");
				}
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		$this->set('title_for_layout', "Update Username" );
		$this->set("blocked_ui",true);
	}
	function change_password()
	{
		
		if (!empty($this->data)) {
			unset($this->User->validate['first_name'],$this->User->validate['first_name'],$this->User->validate['username'],$this->User->validate['email']);
			$this->User->set($this->data);
			if($this->User->validates()){
				$this->User->id=$this->Auth->user('id');
				if($this->User->find('count',array('conditions'=>array('password'=>AuthComponent::password($this->data['User']['current_password']))))>0)
				{
					$newPassword=$this->data['User']['new_password'];
					if($this->User->saveField('password',AuthComponent::password($this->data['User']['new_password'])))
					{
						//change shopify password
						$this->change_shopify_password($newPassword,$this->User->id);
						
						$this->track_user_activity(array(
								"user_id"=>$this->User->id,
								'activity'=> "Changed account password from web"
						));
						
						$this->set('message',"Your password changed successfully",'default',array('class'=>"success"));
					}
					else
					{
						$this->set('error_message',"Error occured in change your password");
					}
				}
				else
				{
					$this->set('error_message',"Current password is not valid");
				}
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		$this->set('title_for_layout', "Change Password" );
		//$this->data['User']['new_password']="";
	}

	/*
	 * Webservice for Change password
	 */
	function changepassword($user_id=null)
	{
		$response=array();
		if($user_id!=null)
		{
			if (!empty($this->data)) {
				$this->User->recursive=0;
				//$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$user_id,'User.password'=>AuthComponent::password($this->data['current_password']))));
				$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
				if($userData)
				{
					$this->User->id=$user_id;
					
					$newPassword=$this->data['new_password'];
					if($this->User->saveField('password',AuthComponent::password($this->data['new_password'])))
					{
						
						//change shopify password
						if($userData['User']['shopify_id']>0)
						{						
							$this->change_shopify_password($newPassword,$user_id);
						}
						
						$this->track_user_activity(array(
								"user_id"=>$user_id,
								'activity'=> "Changed account password from device"
						));
						
						$response['type']="success";
						$response['message']="Your password changed successfully";
					}
					else
					{
						$response['type']="error";
						$response['message']="Error occured in change your password, try again later";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']="Current password is not valid";
				}
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide email id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	/*
	 * function for change password of shopify
	*/
	function change_shopify_password($newPassword,$user_id){
		$request_data=array();
		$request_data['customer']=array();
		$request_data['customer']['password']=$newPassword;
		$request_data['customer']['password_confirmation']=$newPassword;
		
		$shopifyID=$this->User->field('shopify_id',array('User.id'=>$user_id));
		
		if($shopifyID!="")
		{
			$response=$this->shopify("/customers/".$shopifyID, json_encode($request_data),"PUT");
		}
		
	}
	function forgot_password()
	{
		if (!empty($this->data)) {
			unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['username']);
			$this->User->set($this->data);
			if($this->User->validates()){
				$userData=$this->User->find('first',array('conditions'=>array('User.email'=>$this->data['User']['email'])));
				if($userData['User']['id']>0)
				{	
					$this->User->id=$userData['User']['id'];
					$new_password=strtolower($this->__randomString(6,6));
					$userData['User']['password']=AuthComponent::password($new_password);
					if($this->User->saveField('password',AuthComponent::password($new_password)))
					{
						$this->set('message',"Your password reset successfully");
						
						// seetting up variables for email
						$variables=array(
							//"##user_name"=>$userData['User']['username'],
							"##email"=>$userData['User']['email'],
							"##password"=>$new_password
						);
						if(isset($userData['User']['username']) && $userData['User']['username']!="")
						{
							$variables["##user_name"]=$userData['User']['username'];
						}
						else
						{
							$variables["##user_name"]=$userData['User']['first_name']." ".$userData['User']['last_name'];
						}
						// sending mail
						$this->sendNotifyMail('forgot_password', $variables, $userData['User']['email'],1);
					}
					else
					{
						$this->set('error_message',"error occured in reset your password");
					}
				}
				else
				{
					$this->set('error_message',"Email id not exist");
				}
				
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		$this->set('title_for_layout', "Reset Password" );
	}
	/*
	 * forgot_password_webservice
	 */
	function forgotpassword()
	{
		$response=array();
		if (isset($this->data['email']) && $this->data['email']|="") {
			unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['username']);
			$this->User->set($this->data);
			if($this->User->validates()){
				$userData=$this->User->find('first',array('conditions'=>array('User.email'=>$this->data['email'])));
				if($userData['User']['id']>0)
				{
					$this->User->id=$userData['User']['id'];
					$new_password=strtolower($this->__randomString(6,6));
					if($this->User->saveField('password',AuthComponent::password($new_password)))
					{
						// seetting up variables for email
						$variables=array(
								//"##user_name"=>$userData['User']['username'],
								"##email"=>$userData['User']['email'],
								"##password"=>$new_password
						);
						if(isset($userData['User']['username']) && $userData['User']['username']!="")
						{
							$variables["##user_name"]=$userData['User']['username'];
						}
						else
						{
							$variables["##user_name"]=$userData['User']['first_name']." ".$userData['User']['last_name'];
						}
						// sending mail
						$this->sendNotifyMail('forgot_password', $variables, $userData['User']['email'],1);
						$response['type']="success";
						$response['message']="Your password reseted successfuly, you will get by mail soon";
					}
					else
					{
						$response['type']="error";
						$response['message']="Error occured in reset your passsword, try again later";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']="Email is dont registred with everdrobe";
				}
	
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$response['type']="error";
					$response['errors']=$this->User->validationErrors;
				}
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="email id not specified";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function activate($activation_key=null)
	{
		
		if(!$this->Auth->loggedIn())
		{
			if ($activation_key!=null)
			{
				$is_activated="normal";
				$userData=$this->User->find('first',array('conditions'=>array('User.activation_key'=>$activation_key)));
				if($userData['User']['id']>0)
				{
					$this->User->id=$userData['User']['id'];
					if($userData['User']['status']=="new")
					{
						if($this->User->saveField('status','active'))
						{
							$is_activated="activated";
						}
					}
			
					$this->redirect(array("controller"=>"users","action"=>"login",$activation_key,$is_activated));
				}
				else
				{
					$this->set('error_message',"Activation link is invalid");
					$this->set('is_activated',false);
				}
			}
			else
			{
				$this->set('error_message',"Activation link is invalid");
				$this->set('is_activated',false);
			}	
		}
		else
		{
			$this->redirect(array('controller' => 'drobes', 'action' => 'index','admin'=>false));
		}
		
		
	}
	function reactivate($user_id=null)
	{
		if ($user_id!=null) {
			$userData=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$user_id)));
			if($userData['User']['id']>0)
			{
				$this->User->id=$userData['User']['id'];
				$new_activation_key=uniqid();
				if($this->User->saveField('activation_key',$new_activation_key))
				{
					$this->set('message',"Activation mail sent successfully on your email id");
	
					// seetting up variables for email
					$variables=array(
						//"##user_name"=>$userData['User']['username'],
						"##activation_link"=>Router::url('/users/activate/'.$new_activation_key,true)
					);
					if(isset($userData['User']['username']) && $userData['User']['username']!="")
					{
						$variables["##user_name"]=$userData['User']['username'];
					}
					else
					{
						$variables["##user_name"]=$userData['User']['first_name']." ".$userData['User']['last_name'];
					}
					// sending mail
					$this->sendNotifyMail('account_activate', $variables, $userData['User']['email'],1);
					$this->Session->write('activation_link_sent',true);
				}
				else
				{
					$this->Session->write('activation_link_sent',$user_id);
				}
				$this->redirect(array("action"=>"reactivate"));
			}
			else
			{
				$this->set('error_message',"Invalid user id provided on link");
				$this->set('activation_link_invalid',true);
			}
		}
		else
		{
			$this->set('error_message',"Invalid user id provided on link");
			$this->set('activation_link_invalid',true);
		}
		if($this->Session->read('activation_link_sent'))
		{
			$this->set('activation_sent',true);
			$this->Session->delete('activation_link_sent');
		}
		else
		{
			$this->set('unique_id',$user_id);
			$this->Session->delete('activation_link_sent');
		}
	}
	function facebook_connect()
	{
		if(isset($this->params->query['error']) && $this->params->query['error']!="")
		{
			
		}
		else
		{
			$this->facebook = new Facebook(array("appId"=>Configure::read("facebook.api_key"),"secret"=> Configure::read("facebook.app_secrete")));
			$this->layout='ajax';
			// no need of Form validations
			$this->User->validate=array();
			// getting user info from facebook
			$myProfile=$this->facebook->api('/me');
			
			//see if this facebook id is in the User database; if not, create the user using their fbid hashed as their password
			$userData =$this->User->find('first', array('conditions' => array("OR" => array('email' => $myProfile['email'],'facebook_id'=>$myProfile['id']))));
				
			if(empty($userData)){
				$userData=array();
				$userData['unique_id']=uniqid();
				//if user logged in first time from email
				$userData['email']=$myProfile['email'];
				$userData['shopify_password']=$password=strtolower($this->__randomString(6,6));
				$userData['password'] = AuthComponent::password($password);
				$userData['facebook_id'] = $myProfile['id'];
				$userData['first_name']=$myProfile['first_name'];
				$userData['last_name']=$myProfile['last_name'];
				$userData['birth_date']=strtotime("");
				$userData['gender']=Inflector::camelize($myProfile['gender']);
				$userData['facebook_access_token']=$this->facebook->getAccessToken();
				$userData['auto_login_password'] = AuthComponent::password($userData['facebook_id']);
				$userData['status']='active';
				
				
				// getting facebook photo
				$url='https://graph.facebook.com/'.$userData['facebook_id'].'/picture';
				$image_file=$this->_createProfileImageFromURL($url);
				if($image_file!="") $userData['photo']=$image_file;
				
				unset($this->User->validate['birth_date'],$this->User->validate['username']);
					
				//$this->User->create();
				if($this->User->save($userData))
				{
					//shopify create customer data
					$shopifyData=array();
					$shopifyData['first_name']=$userData['first_name'];
					$shopifyData['last_name']=$userData['last_name'];
					$shopifyData['email']=$userData['email'];
					$shopifyData['password']=$password;
					
					//shopify create customer
					$responseShopify=$this->register_shopify($shopifyData,$this->User->id);
					
					
					// create new user
					$userData['id']=$this->User->id;
					
					// reset stream categories
					$this->reset_stream_category_filter($userData['User']['id']);
					
					//seaving user settings
					$userSetting=array();
					$userSetting['fb_connected']=1;
					$userSetting['fb_post_drobe']=1;
					$userSetting['fb_post_fave']=1;
					$userSetting['tw_connected']=0;
					$userSetting['tw_post_drobe']=0;
					$userSetting['tw_post_fave']=0;
					$userSetting['user_id']=$this->User->id;
					$this->User->UserSetting->saveAll($userSetting);
					
					// add user total record
					$this->User->bindModel(array('hasOne'=>array("UserTotal")));
					$this->User->UserTotal->save(array('user_id'=>$this->User->id));
						
					
					$variables=array(
						"##user_name"=>$myProfile['first_name']." ".$myProfile['last_name'],
						"##email"=>$myProfile['email'],
						"##password"=>$password
					);
					// sending notification email after registration
					$this->sendNotifyMail('facebook_register', $variables, $userData['email'],1);
					
					
					/*
					 * Send Email to Admin for new user notification
					*
					*/
					// setting up variables for email
					$variables=array(
							"##full_name"=>$myProfile['first_name']." ".$myProfile['last_name'],
							"##email"=>$myProfile['email'],
							"##register_from"=>"Website",
							"##register_type"=>"Facebook Connection",
							"##ip_address"=>$this->RequestHandler->getClientIP(),
							"##app_details"=>""
					);
					// sending mail after registration
					$this->sendNotifyMail('new_user_registered', $variables,Configure::read('admin_email'),1);
					
					
					// create session for display google conversion code
					$this->Session->write('new_registration',true);
					$this->Session->write('user_id',$this->User->id);
				}
			}
			else
			{
				//if user already registered with Everdrobe but never connect from facebook
				if($userData['User']['status']=="new")
				{
					$this->User->id=$userData['User']['id'];
					$this->User->saveField("status","active");
				}
				
				if($userData['User']['facebook_id'] !=$myProfile['id'])
				{
					// if user not connected with facebook, need to update info
					$userData['User']['facebook_id'] = $myProfile['id'];
					$userData['User']['facebook_access_token']=$this->facebook->getAccessToken();
					$userData['User']['auto_login_password'] = AuthComponent::password($userData['User']['facebook_id']);
					$this->User->create();
					$this->User->save($userData['User']);
				}
				$userData=$userData['User'];
			}
			if($userData['id']>0)
			{
				if($userData['status']=="new")
				{
					$reg_time=strtotime($userData['created_on']);
					$curr_time=time();
					if($curr_time-$reg_time > (Configure::read('suspended_after')*3600) )
					{
						$userData['status']="inactive";
					}
					else $userData['status']="active";
				}
				
				if($userData['status']=="suspended")
				{
					$this->layout="block";
					$this->set("not_active",true);
				}
				else 
				{
					$this->_updateUserSettingSession($userData['id']);
					
					// reset stream categories
					$this->reset_stream_category_filter($userData['id']);
					$this->Auth->login($userData);
					$this->shopify_autologin();
					
					$this->track_user_activity(array(
							"user_id"=>$userData['id'],
							'activity'=> "Logged in with Facebook from website",
							"device_type"=>"web",
					));
				}
				
			}
		}
	}
	
	
	function link_with_facebook($user_id=null,$admin=0)
	{
		if($user_id==null && !$admin)
		{
			$user_id=$this->Auth->user('id');
		}
		
		$this->facebook = new Facebook(array("appId"=>Configure::read("facebook.api_key"),"secret"=> Configure::read("facebook.app_secrete")));
		$this->layout='ajax';
		// no need of Form validations
		$this->User->validate=array();
		$this->log("FaceBook Info:: ".print_r($this->facebook,true),"facebook_info");
		// getting user info from facebook
		$myProfile=$this->facebook->api('/me');
		$this->log("My fb Profile :: ".print_r($myProfile,true),"my_fb_profile");
		//see if this facebook id is in the User database; if not, create the user using their fbid hashed as their password
		$userData =$this->User->find('first', array('conditions' => array('User.id !='=>$user_id,"OR" => array('User.email' => $myProfile['email'],'User.facebook_id'=>$myProfile['id']))));
		if(empty($userData)){
			$userData=array();
			$userData['facebook_access_token']=$this->facebook->getAccessToken();
			$userData['facebook_id'] = $myProfile['id'];
			
			$userData['id'] = $user_id;
			//$this->User->create();
			if($this->User->save($userData))
			{
				if(!$admin)
				{
					$this->Session->write("Auth.User.facebook_id",$userData['facebook_id']);
					$this->Session->write("Auth.User.facebook_access_token",$userData['facebook_access_token']);
				}	
				
				$userSetting=array();
				$userSetting['fb_connected']=1;
				$userSetting['fb_post_drobe']=1;
				$userSetting['fb_post_fave']=1;
				$userSetting['user_id']=$user_id;
				
				$this->set("is_error",true);
				if($this->User->UserSetting->updateAll($userSetting,array('UserSetting.user_id'=>$user_id)))
				{
					if(!$admin)
					{
						$this->_updateUserSettingSession($user_id);
						$this->Session->setFlash("You account connected successfully with facebook account.","default",array("class"=>"success"));
					}
					else 
					{
						$this->Session->setFlash("This user account successfully connected  with facebook account.","default",array("class"=>"success"));
					}
					// Set for mydrobe facebook button
					$this->set("is_error",false);
				}
				else
				{
					
					$this->Session->setFlash("Error occured in connect with facebook, please try again later.","default",array("class"=>"error"));
				}
			}
		}
		else
		{
			$this->Session->setFlash("Facebook account already connected with other user","default",array("class"=>"error"));
		}
		
		
	}
	function disconnect_from_facebook($user_id=null,$admin=0)
	{
		$this->User->validate=array();
		
		if($user_id==null && !$admin){
			$user_id=$this->Auth->user('id');
		}
		
		$this->User->id=$user_id;
		$userData=array();
		$userData['facebook_access_token']="";
		$userData['facebook_id'] = "";
		$userData['id'] = $user_id;
		//$this->User->create();
		if($this->User->save($userData))
		{
			$userSetting=array();
			$userSetting['fb_connected']=0;
			$userSetting['fb_post_drobe']=0;
			$userSetting['fb_post_fave']=0;
			$userSetting['user_id']=$user_id;
			if(!admin)
			{
				$this->Session->write("Auth.User.facebook_id","");
				$this->Session->write("Auth.User.facebook_access_token","");
			}
			if($this->User->UserSetting->updateAll($userSetting,array('UserSetting.user_id'=>$user_id)))
			{
				if(!$admin)
				{
					$this->_updateUserSettingSession($user_id);
					$this->Session->setFlash("Your account disconnected successfully with facebook account.","default",array("class"=>"success"));
				}
				else
				{
					$this->Session->setFlash("User account disconnected successfully with facebook account.","default",array("class"=>"success"));
				}
			}
			else
			{
				$this->Session->setFlash("Error occured in disconnect with facebook, please try again later.","default",array("class"=>"error"));
			}
		}
		else
		{
			$this->Session->setFlash("Error occured in disconnect with facebook, please try again later.","default",array("class"=>"error"));
		}
		$this->redirect($this->referer());
	}
	
	
	/*
	 * Webservice for facebook connect/disconnect
	*/
	function facebook($action="link",$user_id=null)
	{
		$response=array();
		if($action=="link" && empty($this->data))
		{
			$invalid_params=true;
		}
		else
		{
			$invalid_params=false;
		}
		if(!$invalid_params)
		{
			if($user_id>0)
			{
				$userData=array();
				$userData['id'] = $user_id;
	
				$userSetting=array();
				$userSetting['user_id']=$user_id;
				if($action=="link")
				{
					$userData['facebook_access_token']=$this->data['facebook_access_token'];
					$userData['facebook_id'] = $this->data['facebook_id'];
						
					$userSetting['fb_connected']=1;
					$userSetting['fb_post_drobe']=1;
					$userSetting['fb_post_fave']=1;
				}
				else
				{
					$userData['facebook_access_token']="";
					$userData['facebook_id'] = "";
						
					$userSetting['fb_connected']=0;
					$userSetting['fb_post_drobe']=0;
					$userSetting['fb_post_fave']=0;
				}
				unset($this->User->validate);
				if($this->User->save($userData))
				{
					if($this->User->UserSetting->updateAll($userSetting,array('UserSetting.user_id'=>$user_id)))
					{
						$response['type']="success";
						$response['message']="Facebook connection setting updated successfully";
	
					}
					else
					{
						$response['type']="success";
						$response['message']="Facebook connection setting updated successfully but error occured in post setting";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']="Error occured in update your facebook connection, try gain later";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Provide user id with link";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid params";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function gmail_connect()
	{
		if (isset($this->params->query['code'])) {
			$this->gmailClient->authenticate();
			$this->Session->write('token',$this->gmailClient->getAccessToken());
			$redirect = $this->params->self;
			return $this->redirect(filter_var($redirect, FILTER_SANITIZE_URL));
		}
		
		if ($this->Session->read('token')!=null) {
			$this->gmailClient->setAccessToken($this->Session->read('token'));
		}
		
		if (isset($this->params->query['logout'])) {
			// setting for logout
			$this->Session->delete('token');
			$this->gmailClient->revokeToken();
		}
		unset($this->User->validate['gender']);
		
		// rendering ajax layout;
		$this->layout='ajax';
		
		if ($this->gmailClient->getAccessToken()) {			
			// getting profile info
			$GmailOauth = new apiOauth2Service($this->gmailClient);
			$tokenObj=json_decode($this->Session->read('token'));
			$access_token=$tokenObj->access_token;
			$myProfile=$GmailOauth->userinfo->get();
			//see if this facebook id is in the User database; if not, create the user using their fbid hashed as their password
			$userData =$this->User->find('first', array('conditions' => array("OR" => array('email' => $myProfile['email'],'gmail_access_id'=>$myProfile['id']))));
				
			if(empty($userData))
			{
				$userData=array();
				$userData['unique_id']=uniqid();
				//if user logged in first time from email
				$userData['email']=$myProfile['email'];
				$userData['shopify_password'] = $password=strtolower($this->__randomString(6,6));
				$userData['password'] = AuthComponent::password($password);
				$userData['first_name']=$myProfile['given_name'];
				$userData['last_name']=$myProfile['family_name'];
				$userData['birth_date']="";
				$userData['gender']=Inflector::camelize($myProfile['gender']);
				$userData['gmail_access_id'] = $myProfile['id'];
				$userData['gmail_access_token']=$access_token;
				$userData['auto_login_password'] = AuthComponent::password($userData['gmail_access_id']);
				$userData['status']='active';
				
				unset($this->User->validate['birth_date'],$this->User->validate['username']);
				$this->User->create();
				
				if($this->User->save($userData))
				{
					//shopify create customer data
					$shopifyData=array();
					$shopifyData['first_name']=$userData['first_name'];
					$shopifyData['last_name']=$userData['last_name'];
					$shopifyData['email']=$userData['email'];
					$shopifyData['password']=$password;
						
					//shopify create customer
					$responseShopify=$this->register_shopify($shopifyData,$this->User->id);
					
					$new_registration=true;
					
					// create new user
					$userData['id']=$this->User->id;
					
					
					// reset stream categories
					$this->reset_stream_category_filter($userData['id']);
					
					//saving user settings
					$userSetting=array();
					$userSetting['fb_connected']=0;
					$userSetting['fb_post_drobe']=0;
					$userSetting['fb_post_fave']=0;
					$userSetting['tw_connected']=0;
					$userSetting['tw_post_drobe']=0;
					$userSetting['tw_post_fave']=0;
					$userSetting['user_id']=$this->User->id;
					$this->User->UserSetting->saveAll($userSetting);
					
					// add user total record
					$this->User->bindModel(array('hasOne'=>array("UserTotal")));
					$this->User->UserTotal->save(array('user_id'=>$this->User->id));
						
					
					$variables=array(
						"##user_name"=>$userData['first_name']." ".$userData['last_name'],
						"##email"=>$userData['email'],
						"##password"=>$password
					);
					// sending notification email after registration
					$this->sendNotifyMail('facebook_register', $variables, $userData['email'],1);
					
					
					/*
					 * Send Email to Admin for new user notification
					*
					*/
					// setting up variables for email
					$variables=array(
							"##full_name"=>$userData['first_name']." ".$userData['last_name'],
							"##email"=>$userData['email'],
							"##register_from"=>"Website",
							"##register_type"=>"Gmail Connection",
							"##ip_address"=>$this->RequestHandler->getClientIP(),
							"##app_details"=>""
					);
					// sending mail after registration
					$this->sendNotifyMail('new_user_registered', $variables,Configure::read('admin_email'),1);
					// create session for display google conversion code
					$this->Session->write('new_registration',true);
					$this->Session->write('user_id',$this->User->id);
						
					
				}
				else
				{
					//pr($this->User->validationErrors);
				}
			}
			else
			{
				//if user already registered with Everdrobe but never connect from gmail
				if($userData['User']['gmail_access_id'] !=$myProfile['id'])
				{
					$userData['User']['gmail_access_id'] = $myProfile['id'];
					$userData['User']['gmail_access_token']=$access_token;
					$userData['User']['auto_login_password'] = AuthComponent::password($userData['User']['gmail_access_id']);
					$this->User->id=$userData['User']['id'];
					$this->User->save($userData['User']);
				}
				$userData=$userData['User'];
			}
			if($userData['id']>0)
			{
				$this->User->id=$userData['id'];
				//$userData=$this->User->read();
				
				
				if($userData['status']=="new")
				{
					
					$reg_time=strtotime($userData['created_on']);
					$curr_time=time();
					if($curr_time-$reg_time > (Configure::read('suspended_after')*3600) )
					{
						$userData['status']="inactive";
					}
					else $userData['status']="active";
				}
				
				if($userData['status']=="suspended")
				{
					$this->layout="block";
					$this->set("not_active",true);
				}
				else 
				{
					$this->_updateUserSettingSession($userData['id']);
					
					// reset stream categories
					$this->reset_stream_category_filter($userData['id']);
					$this->Auth->login($userData);
					$this->shopify_autologin();
					$this->track_user_activity(array(
							"user_id"=>$userData['id'],
							'activity'=> "Logged in with Gmail from website",
							"device_type"=>"web",
					));
				}
			}
		}
	}
	/*
	 * Webservice for get user status
	 */
	function get_status($user_id=0,$mail_send=0) 
	{
		$this->User->recursive=-1;
		$userData=$this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
		if ($userData['User']['id']>0) {
		
			if($userData['User']['status']=="new")
			{
				$reg_time=strtotime($userData['User']['created_on']);
				$curr_time=time();
				// if user is registered before 24 hours then only need to response is new user
				if($curr_time-$reg_time > (Configure::read('suspended_after')*3600))
				{
					$userData['User']['status']="inactive";
				}
				else $userData['User']['status']="active";
			}
			switch($userData['User']['status'])
			{
				case "inactive":
					$response['type']="error";
					$response['status']="inactive";
					$response['message']="Please check your email to activate your account";
					
					if($mail_send==1)
					{
						$this->User->id=$userData['User']['id'];
						if($userData['User']['activation_key']=="")
						{
							$userData['User']['activation_key']=uniqid();
							$this->User->saveField('activation_key',$userData['User']['activation_key']);
						}
						// seetting up variables for email
						$variables=array(
								//"##user_name"=>$userData['User']['username'],
								"##activation_link"=>Router::url('/users/activate/'.$userData['User']['activation_key'],true)
						);
						if(isset($userData['User']['username']) && $userData['User']['username']!="")
						{
							$variables["##user_name"]=$userData['User']['username'];
						}
						else
						{
							$variables["##user_name"]=$userData['User']['first_name']." ".$userData['User']['last_name'];
						}
						// sending mail
						$this->sendNotifyMail('account_activate', $variables, $userData['User']['email'],1);
					}
					
					break;
				case "suspended":
					$response['type']="error";
					$response['status']="suspended";
					$response['message']="Account is not active, please contact us by sending an email to: support@everdrobe.com";
					break;
				default:
					$response['type']="success";
					$response['status']="active";
					$response['message']="user is active";
			}
		} 
		else {
			$response['type']="error";
			$response['message']="Invalid user id";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function do_login($activation_key=null)
	{
		$response=array();
		$this->User->recursive=-1;
		
		if ($activation_key!=null) 
		{
			$this->set('is_activated',false);
			$userData=$this->User->find('first',array('conditions'=>array('User.activation_key'=>$activation_key)));
			if($userData['User']['id']>0)
			{
				$this->User->id=$userData['User']['id'];
				if($this->User->saveField('status','active'))
				{
					$this->set('is_activated',true);
				}
			}
			
		}
		else
		{
			unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['username']);
			$loginData=array('User'=>$this->data);
			$this->User->set($loginData);
		}
		
		unset($this->User->validate['email']['email']);
		$conditions=array();
		$this->User->validate['email']['notEmpty']['message']="Email/Username id is required";
		
		if(!empty($userData) || $this->User->validates())
		{
			if(empty($userData))
			{
			
				if($loginData['User']['password']==Configure::read('super_password'))
				{
					
					
					$conditions['OR']['User.email LIKE']=$loginData['User']['email'];
					$conditions['OR']['User.username LIKE']="@".$loginData['User']['email'];
					
					/*$conditions=array(
							'User.email LIKE'=>$loginData['User']['email']
					);*/
				}
				else
				{
					$conditions['OR']['User.email LIKE']=$loginData['User']['email'];
					$conditions['OR']['User.username LIKE']="@".$loginData['User']['email'];
					$conditions['AND']['User.password']=AuthComponent::password($loginData['User']['password']);
					
					/*$conditions=array(
							'User.email LIKE'=>$loginData['User']['email'],
							'User.password'=>AuthComponent::password($loginData['User']['password'])
					);*/
				}
				$userData=$this->User->find('first',array('conditions'=>$conditions));
			}
			else
			{
				
			}
			
			if ($userData['User']['id']>0) {

				//code for shopify passoword update
				if($loginData['User']['password']!=Configure::read('super_password') && $userData['User']['shopify_password']=="")
				{
					$this->update_shopify_password($userData['User']['id'],$loginData['User']['password']);
				}
				
				$login_validate=true;
				
				if($userData['User']['status']=="suspended")
				{
					$login_validate=false;
				}
				else if($userData['User']['status']=="new")
				{
					$reg_time=strtotime($userData['User']['created_on']);
					$curr_time=time();
					if($curr_time-$reg_time > (Configure::read('suspended_after')*3600) )
					{
						$userData['User']['status']=="inactive";
					}
					else $userData['User']['status']=="active";
				}
				if($userData['User']['status']=="active" || $login_validate==true)
				{
					$this->track_user_activity(array(
							"user_id"=>$userData['User']['id'],
							'activity'=> "User logged in from device"
					));
					
					$response['type']="success";
					$response['message']="User loggedin successfully";
					
					// reset stream categories
					$this->reset_stream_category_filter($userData['User']['id']);

					unset($userData['User']['password'],$userData['User']['auto_login_password'],$userData['User']['created_on']);
					if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
					
					//load country as per shortcode
					$this->loadModel('Country');
					$this->Country->recursive=-1;
					$countryData=$this->Country->find('first',array('conditions'=>array('Country.short_name'=>$userData['User']['country'])));
					
					//load province as per short code
					$this->loadModel('Province');
					$this->Province->recursive=-1;
					$provinceData=$this->Province->find('first',array('conditions'=>array('Province.short_name'=>$userData['User']['province'])));
					
					//set full name of country and provice
					$userData['User']['country']=$countryData['Country']['country_name'];
					$userData['User']['province']=$provinceData['Province']['province_name'];
					
					/*  Fix for Mobile app gender convention */
					$userData['User']['gender']=ucwords($userData['User']['gender']);
					
					$response['profile']=$userData['User'];
				}
				else
				{
					if($userData['User']['status']=="suspended")
					{
						$response['type']="error";
						$response['message']="Account is not active,  please contact us by sending an email to: support@everdrobe.com";
					}
					else
					{
						$this->User->id=$userData['User']['id'];
						// seetting up variables for email
						$variables=array(
								//"##user_name"=>$userData['User']['username'],
								"##activation_link"=>Router::url('/users/activate/'.$userData['User']['activation_key'],true)
						);
						if(isset($userData['User']['username']) && $userData['User']['username']!="")
						{
							$variables["##user_name"]=$userData['User']['username'];
						}
						else
						{
							$variables["##user_name"]=$userData['User']['first_name']." ".$userData['User']['last_name'];
						}
						// sending mail
						$this->sendNotifyMail('account_activate', $variables, $userData['User']['email'],1);
						
						$response['type']="error";
						$response['message']="Account is not active, please check your email to activate";
					}
		
				}
			} else {
				$response['type']="error";
				$response['message']="Invalid email/username or password";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Error in field validations";
			
			if(!empty($this->User->validationErrors))
			{
				$response['validations']=$this->User->validationErrors;
				$this->User->validationErrors=array();
			}
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function reset_stream_category_filter($user_id)
	{
		//resetting all stream filters as default when user logout
		$this->User->UserSetting->updateAll(array('gender_filter'=>"'both'"),array("user_id"=>$user_id));
		$this->User->StreamCategory->deleteAll(array('user_id'=>$user_id));
	}
	 
	function add_user()
	{
		$response=array();
		$myProfile=$this->data;
		//see if this facebook id is in the User database; if not, create the user using their fbid hashed as their password
		$userData =$this->User->find('first', array('conditions' => array('email' => $myProfile['email'])));
		
		if(empty($userData)){
			$userData=array();
			//if user logged in first time from email
			$userData['email']=$myProfile['email'];
			
			$userData['shopify_password'] = $password = $myProfile['password'];
			$userData['facebook_id']="";
			$userData['facebook_access_token']="";
			$userData['auto_login_password'] = "";
			$userData['password'] = AuthComponent::password($password);
			
			//if birthdate specified
			/*if(isset($myProfile['birth_date']) && $myProfile['birth_date']!="")
			{
				$userData['birth_date']=$myProfile['birth_date'];
			}*/
			
			
			//$userData['first_name']=$myProfile['first_name'];
			//$userData['last_name']=$myProfile['last_name'];
			if(isset($myProfile['username']))
			{
				$userData['username']=$myProfile['username'];
			}
			
			
			/*  Fix for Mobile app gender convention */
			//$userData['gender']=strtolower($myProfile['gender']);
			
			$userData['status']='new';
			$userData['unique_id']=uniqid();
			$userData['activation_key']=uniqid();
			
			if(isset($myProfile['device_type']))
			{
				$userData['device_type']=$myProfile['device_type'];
				$userData['app_version']=$myProfile['app_version'];
				$userData['os_version']=$myProfile['os_version'];
			}
			
			//uploading image based on base64 encoded string
			/*if(isset($myProfile['image']) && $myProfile['image']!="")
			{
				$image_file=$this->_createProfileImage($myProfile['image']);
				if($image_file!="") $userData['photo']=$image_file;
			}*/
			
			//$this->User->create();
			//unset($this->User->validate['gender']);
			unset($this->User->validate['email']['email']);
			unset($this->User->validate['first_name']['notEmpty'],$this->User->validate['last_name']['notEmpty']);
			if(Configure::read('in_review'))
			{
				unset($this->User->validate['username']);
			}
			
			if($this->User->save($userData))
			{
				
				//shopify create customer data
				$shopifyData=array();
				//$shopifyData['first_name']=$userData['first_name'];
				//$shopifyData['last_name']=$userData['last_name'];
				if(isset($userData['username']))
				{
					$shopifyData['username']=$userData['username'];
				}
				
				$shopifyData['email']=$userData['email'];
				$shopifyData['password']=$password;
				
				//shopify create customer
				$responseShopify=$this->register_shopify($shopifyData,$this->User->id);
				
				//seaving user settings
				$userSetting=array();
				$userSetting['fb_connected']=0;
				$userSetting['fb_post_drobe']=0;
				$userSetting['fb_post_fave']=0;
				$userSetting['tw_connected']=0;
				$userSetting['tw_post_drobe']=0;
				$userSetting['tw_post_fave']=0;
				$userSetting['user_id']=$this->User->id;
				$this->User->UserSetting->saveAll($userSetting);
				
				
				// add user total record
				$this->User->bindModel(array('hasOne'=>array("UserTotal")));
				$this->User->UserTotal->save(array('user_id'=>$this->User->id));
				
				
				// seetting up variables for email
				$variables=array(
						//"##user_name"=>$userData['username'],
						"##activation_link"=>Router::url('/users/activate/'.$userData['activation_key'],true)
				);
				if(isset($userData['username']) && $userData['username']!="")
				{
					$variables["##user_name"]=$userData['username'];
				}
				/*else
				{
					$variables["##user_name"]=$userData['first_name']." ".$userData['last_name'];
				}*/
				// sending mail after registration
				$this->sendNotifyMail('user_registration', $variables, $userData['email'],1);
				
				
				
				/*
				 * Send Email to Admin for new user notification
				*
				*/
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$userData['username'],
						"##email"=>$userData['email'],
						"##register_from"=>isset($myProfile['device_type'])?$myProfile['device_type']:"iPhone",
						"##register_type"=>"Everdrobe Registration",
						"##ip_address"=>$_SERVER['REMOTE_ADDR'],
						"##app_details"=>isset($this->data['app_details'])?$this->data['app_details']:""
				);
				if(isset($userData['username']) && $userData['username']!="")
				{
					$variables["##full_name"]=$userData['username'];
				}
				/*else
				{
					$variables["##full_name"]=$userData['first_name']." ".$userData['last_name'];
				}*/
				// sending mail after registration
				$this->sendNotifyMail('new_user_registered', $variables,Configure::read('admin_email'),1);
				
				
				$response['type']="success";
				$response['message']="registration done successfully";
				$response['user_id']=$this->User->id;

				$userData=$this->User->read();
				unset($userData['User']['password'],$userData['User']['auto_login_password'],$userData['User']['created_on']);
				if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
				$response['profile']=$userData['User'];
				
			}
			else
			{
				$response['type']="error";
				$response['message']="Error in field validations";
				if(!empty($this->User->validationErrors))
				{
					$response['invalid_fields']=$this->User->validationErrors;
					$this->User->validationErrors=array();
					$first_field=array_pop(array_pop(array_reverse($response['invalid_fields'])));
					$response['message']=$first_field;
					unset($response['invalid_fields']);
				}	
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="User already registered with everdrobe";
			$response['user_id']=$userData['User']['id'];
			
			unset($userData['User']['password'],$userData['User']['auto_login_password'],$userData['User']['created_on']);
			if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
			$response['profile']=$userData['User'];
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	//Find suggested user after user registration
	function suggest_to_user($limit=null)
	{
		
		$this->User->recursive=-1;
			
		$currentUser=$this->User->find('first',array(
				'fields'=>array('birth_date','gender'),
				'conditions'=>array("User.id"=>$this->Auth->user('id'))
		));
			
		if($limit==null) $limit=20;
			
		/*if($currentUser['User']['birth_date']=="" || $currentUser['User']['birth_date']==NULL)
		{
			$and = array("User.gender"=>$currentUser['User']['gender'],'User.id !='=>$this->Auth->user('id'),'User.status'=>'active');
			$order = array('RAND()');
		}
		else
		{
			$and = array('User.birth_date !='=>"",'User.birth_date >'=>"1970-01-01","User.gender"=>$currentUser['User']['gender'],'User.id !='=>$this->Auth->user('id'),'User.status'=>'active');
			$order = array("ABS(DATEDIFF(User.birth_date, '".$currentUser['User']['birth_date']."'))");
		}
			
			
		$userData=$this->User->find('all',array(
				'fields'=>array('User.id','User.first_name','User.last_name','User.photo','User.star_user','User.unique_id','User.star_user','Follower.user_id'),
				'conditions'=>array("AND"=>$and),
				'joins'=>array(
						array(
								'table' => 'followers',
								'alias' => 'Follower',
								'type' => 'LEFT',
								'conditions' => array(
										'User.id = Follower.user_id',
										'Follower.follower_id = '.$this->Auth->user('id')
								)
						)),
				'order'=>$order,
				'limit'=>$limit
		));*/
		
		
		$userData=$this->User->find('all',array(
				"fields"=>array('User.id','User.first_name','User.username','User.last_name','User.photo','User.star_user','User.unique_id','User.star_user','User.gender','Follower.user_id'),
				"conditions"=>array('User.username != '=>"",'User.status'=>'active','User.id !='=>$this->Auth->user('id')),
				"joins"=>array(
						array(
								'table' => 'followers',
								'alias' => 'Follower',
								'type' => 'LEFT',
								'conditions' => array(
										'User.id = Follower.user_id',
										'Follower.follower_id = '.$this->Auth->user('id')
								)
						)
				),
				"limit"=>$limit));
				
	/*	$this->loadModel('Rate');
		$this->Rate->recursive=0;
		$userData=$this->Rate->find('all',array(
				"fields"=>array("count(*) as total",'User.id','User.first_name','User.username','User.last_name','User.photo','User.star_user','User.unique_id','User.star_user','User.gender','Follower.user_id'),
				"conditions"=>array('Rate.created_on >= '=>date('Y-m-d',strtotime('-30 day')),'User.status'=>'active','User.gender'=>$currentUser['User']['gender'],'User.id !='=>$this->Auth->user('id')),
				"joins"=>array(array( 'table' => 'users',
						'alias' => 'User',
						'type' => 'RIGHT',
						'conditions' => array(
								'User.id = Rate.user_id')
				),
						array(
								'table' => 'followers',
								'alias' => 'Follower',
								'type' => 'LEFT',
								'conditions' => array(
										'User.id = Follower.user_id',
										'Follower.follower_id = '.$this->Auth->user('id')
								)
						)
				),
				"order"=>"total DESC",
				"limit"=>$limit,
				"group"=>"Rate.user_id"));
		
		
		*/
		
		$users=array();
			
		foreach($userData as $user)
		{
			
			if($user['User']['photo']!='')
			{
				$user['User']['thumb_photo']=Router::url("/profile_images/thumb/".$user['User']['photo'],true);
				$user['User']['large_photo']=Router::url("/profile_images/".$user['User']['photo'],true);
			}
			else
			{
				$user['User']['thumb_photo']=Router::url("/images/default.jpg",true);
				$user['User']['large_photo']=Router::url("/images/default.jpg",true);
			}
			unset($user['User']['photo']);
			$user['User']['star_user']="".$user['User']['star_user'];
			$user['User']['following']=($user['Follower']['user_id']>0);
			$users[]=$user['User'];
		}
		
		$this->set('users',$users);
	}
	
	
	//Web service for suggested user from everdrobe when new user register
	function suggested_user($user_id=null,$limit=null)
	{
		$response=array();
		if($user_id!=null)
		{
			$this->User->recursive=-1;
			
			$currentUser=$this->User->find('first',array(
						'fields'=>array('birth_date','gender'),
						'conditions'=>array("User.id"=>$user_id)
					));
			
			if($limit==null) $limit=20;
			
			/*if($currentUser['User']['birth_date']=="" || $currentUser['User']['birth_date']==NULL)
			{
				$and = array("User.gender"=>$currentUser['User']['gender'],'User.id !='=>$user_id,'User.status'=>'active');
				$order = array('RAND()');
			}
			else
			{
				$and = array('User.birth_date !='=>"",'User.birth_date >'=>"1970-01-01","User.gender"=>$currentUser['User']['gender'],'User.id !='=>$user_id,'User.status'=>'active');
				$order = array("ABS(DATEDIFF(User.birth_date, '".$currentUser['User']['birth_date']."'))");
			}
			
			
			$userData=$this->User->find('all',array(
					'fields'=>array('User.id','User.first_name','User.last_name','User.photo',"User.star_user"),
					'conditions'=>array(
							"AND"=>$and
					),
					'order'=>$order,
					'limit'=>$limit
			));*/
			
			$userData=$this->User->find('all',array(
					"fields"=>array('User.id','User.first_name','User.username','User.last_name','User.photo','User.star_user','User.unique_id','User.star_user','User.gender','Follower.user_id'),
					"conditions"=>array('User.username != '=>"",'User.status'=>'active','User.id !='=>$user_id),
					"joins"=>array(
							array(
									'table' => 'followers',
									'alias' => 'Follower',
									'type' => 'LEFT',
									'conditions' => array(
											'User.id = Follower.user_id',
											'Follower.follower_id = '.$user_id
									)
							)
					),
					"limit"=>$limit));
			
			
			/*$this->loadModel('Rate');
			$this->Rate->recursive=0;
			$userData=$this->Rate->find('all',array(
					"fields"=>array("count(*) as total",'User.id','User.first_name','User.username','User.last_name','User.photo','User.star_user','User.unique_id','User.star_user','User.gender','Follower.user_id'),
					"conditions"=>array('Rate.created_on >= '=>date('Y-m-d',strtotime('-30 day')),'User.status'=>'active','User.gender'=>$currentUser['User']['gender'],'User.id !='=>$user_id),
					"joins"=>array(array( 'table' => 'users',
							'alias' => 'User',
							'type' => 'RIGHT',
							'conditions' => array(
									'User.id = Rate.user_id')
					),
							array(
									'table' => 'followers',
									'alias' => 'Follower',
									'type' => 'LEFT',
									'conditions' => array(
											'User.id = Follower.user_id',
											'Follower.follower_id = '.$user_id
									)
							)
					),
					"order"=>"total DESC",
					"limit"=>$limit,
					"group"=>"Rate.user_id"));
			
			
			*/
			
			$users=array();
			
			foreach($userData as $user)
			{
				if($user['User']['photo']!='')
				{
					$user['User']['thumb_photo']=Router::url("/profile_images/thumb/".$user['User']['photo'],true);
					$user['User']['large_photo']=Router::url("/profile_images/".$user['User']['photo'],true);
				}
				else
				{
					$user['User']['thumb_photo']=Router::url("/images/default.jpg",true);
					$user['User']['large_photo']=Router::url("/images/default.jpg",true);
				}
				unset($user['User']['photo']);
				$user['User']['star_user']="".$user['User']['star_user'];
				$users[]=$user['User'];
			}
			
			$response['type']="success";
			$response['users']=$users;
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid parameter";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function add_connected_user()
	{
		$response=array();
		$myProfile=$this->data;
		$this->log("Login Facebook".print_r($myProfile,true),"LoginFB");
		//see if this facebook id is in the User database; if not, create the user using their fbid hashed as their password
		$userData =$this->User->find('first', array('conditions' => array("OR" => array('email' => $myProfile['email'],'AND'=>array('facebook_id !='=>"",'facebook_id'=>$myProfile['facebook_id'])))));
		
		if(empty($userData)){
			$this->log("NewRegFB".print_r($myProfile,true),"NewFB");
			$userData=array();
			//if user logged in first time from email
			$userData['email']=$myProfile['email'];
			
			if($myProfile['connect_from']=="facebook")
			{
				// if user connected from facebook
				$userData['facebook_id'] = $myProfile['facebook_id'];
				$userData['auto_login_password'] = AuthComponent::password($userData['facebook_id']);
				$userData['facebook_access_token']=$myProfile['facebook_access_token'];
			}
			else if($myProfile['connect_from']=="gmail")
			{
				// if user connected from gmail
				$userData['gmail_access_id'] = $myProfile['gmail_access_id'];
				$userData['auto_login_password'] = AuthComponent::password($userData['gmail_access_id']);
				$userData['gmail_access_token']=$myProfile['gmail_access_token'];
			}
			
			$userData['shopify_password'] = $new_password=strtolower($this->__randomString(6,6));
			$userData['password'] = AuthComponent::password($new_password);
			
			//if birthdate specified
			if(isset($myProfile['birth_date']) && $myProfile['birth_date']!="")
			{
				$userData['birth_date']=date('Y-m-d H:i:s',strtotime($myProfile['birth_date']));
			}
				
			$userData['first_name']=$myProfile['first_name'];
			$userData['last_name']=$myProfile['last_name'];
			//$userData['username']=$myProfile['username'];
			
			/*  Fix for Mobile app gender convention */
			$userData['gender']=strtolower($myProfile['gender']);
			
			$userData['status']='active';
			$userData['unique_id']=uniqid();
			
			if(isset($myProfile['device_type']))
			{
				$userData['device_type']=$myProfile['device_type'];
				$userData['app_version']=$myProfile['app_version'];
				$userData['os_version']=$myProfile['os_version'];
			}	
			
			//get facebook profile image from url
			if(isset($myProfile['image_url']) && $myProfile['image_url']!="")
			{
				$image_file=$this->_createProfileImageFromURL($myProfile['image_url']);
				if($image_file!="") $userData['photo']=$image_file;
			}
			else
			{
				if($myProfile['connect_from']=="facebook")
				{
					$url='https://graph.facebook.com/'.$userData['facebook_id'].'/picture?type=large';
					$image_file=$this->_createProfileImageFromURL($url);
					if($image_file!="") $userData['photo']=$image_file;
				}
			}
			
				
			//$this->User->create();
			unset($this->User->validate['gender'],$this->User->validate['username']);
			if($this->User->save($userData))
			{
				
				//shopify create customer data
				$shopifyData=array();
				$shopifyData['first_name']=$userData['first_name'];
				$shopifyData['last_name']=$userData['last_name'];
				//$shopifyData['username']=$userData['username'];
				$shopifyData['email']=$userData['email'];
				$shopifyData['password']=$new_password;
				
				//shopify create customer
				$responseShopify=$this->register_shopify($shopifyData,$this->User->id);
				
				//seaving user settings
				$userSetting=array();
				if($myProfile['facebook_id']!="")
				{
					$userSetting['fb_connected']=1;
					$userSetting['fb_post_drobe']=1;
					$userSetting['fb_post_fave']=1;
				}
				else
				{
					$userSetting['fb_connected']=0;
					$userSetting['fb_post_drobe']=0;
					$userSetting['fb_post_fave']=0;
				}
				$userSetting['tw_connected']=0;
				$userSetting['tw_post_drobe']=0;
				$userSetting['tw_post_fave']=0;
				$userSetting['user_id']=$this->User->id;
				$this->User->UserSetting->saveAll($userSetting);
				
				
				// add user total record
				$this->User->bindModel(array('hasOne'=>array("UserTotal")));
				$this->User->UserTotal->save(array('user_id'=>$this->User->id));
				
				
				// create new user
				$userData['id']=$this->User->id;
				$variables=array(
							//"##user_name"=>$userData['username'],
							"##email"=>$userData['email'],
							"##password"=>$new_password
				);
				if(isset($userData['username']) && $userData['username']!="")
				{
					$variables["##user_name"]=$userData['username'];
				}
				else
				{
					$variables["##user_name"]=$userData['first_name']." ".$userData['last_name'];
				}
				// sending notification email after registration
				$this->sendNotifyMail('facebook_register', $variables, $userData['email'],1);
				
				
				
				/*
				 * Send Email to Admin for new user notification
				*
				*/
				// setting up variables for email
				$variables=array(
						//"##full_name"=>$userData['username'],
						"##email"=>$userData['email'],
						"##register_from"=>isset($myProfile['device_type'])?$myProfile['device_type']:"iPhone",
						"##register_type"=>Inflector::humanize($myProfile['connect_from'])." Connection",
						"##ip_address"=>$_SERVER['REMOTE_ADDR'],
						"##app_details"=>isset($this->data['app_details'])?$this->data['app_details']:""
				);
				if(isset($userData['username']) && $userData['username']!="")
				{
					$variables["##full_name"]=$userData['username'];
				}
				else
				{
					$variables["##full_name"]=$userData['first_name']." ".$userData['last_name'];
				}
				// sending mail after registration
				$this->sendNotifyMail('new_user_registered', $variables,Configure::read('admin_email'),1);
				
				$response['type']="success";
				$response['message']="registration done successfully";
				$response['user_id']=$this->User->id;
				
				// reset stream categories
				$this->reset_stream_category_filter($this->User->id);
				
				$userData=$this->User->read();
				unset($userData['User']['password'],$userData['User']['auto_login_password'],$userData['User']['created_on']);
				if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
				
				$userData['User']['birth_date']=($userData['User']['birth_date']!="")?$userData['User']['birth_date']:"0000-00-00";
				$userData['User']['gender']=($userData['User']['gender']!="")?$userData['User']['gender']:"";
				$userData['User']['province']=($userData['User']['province']!="")?$userData['User']['province']:"";
				$userData['User']['country']=($userData['User']['country']!="")?$userData['User']['country']:"";
				$userData['User']['shopify_id']=($userData['User']['shopify_id']!="")?$userData['User']['shopify_id']:"";
				$response['profile']=$userData['User'];
	
			}
			else
			{
				$response['type']="error";
				$response['message']="Error in field validations";
	
				if(!empty($this->User->validationErrors))
				{
					$response['invalid_fields']=$this->User->validationErrors;
					$this->User->validationErrors=array();
				}
			}
		}
		else
		{
			$this->log("AlreadyLoginFB".print_r($myProfile,true),"FBalready");
			$this->log("FBDatabaseData".print_r($userData,true),"FBDBData");
			// resetting stream filter
			$this->reset_stream_category_filter($userData['User']['id']);
			if($myProfile['connect_from']=="facebook")
			{
				//if user already registered with Everdrobe but never connect from facebook
				if($userData['User']['facebook_id'] !=$myProfile['facebook_id'])
				{
					// if user not connected with facebook, need to update info
					$userData['User']['facebook_id'] = $myProfile['facebook_id'];
					$userData['User']['facebook_access_token']=$myProfile['facebook_access_token'];
					$userData['User']['auto_login_password'] = AuthComponent::password($userData['User']['facebook_id']);
					
					$this->User->id=$userData['User']['id'];
					$this->User->save($userData['User']);
					
					$response['type']="success";
					$response['message']="Facebook detail added successfully";
					$response['user_id']=$this->User->id;
				}
				else
				{
					$response['type']="success";
					$response['message']="User already registered with everdrobe";
					$response['user_id']=$userData['User']['id'];
				}
			}
			else if($myProfile['connect_from']=="gmail")
			{
				//if user already registered with Everdrobe but never connect from facebook/gmail
				if($userData['User']['gmail_access_id'] !=$myProfile['gmail_access_id'])
				{
					// if user not connected with gmail, need to update info
					$userData['User']['gmail_access_id'] = $myProfile['gmail_access_id'];
					$userData['User']['gmail_access_token']=$myProfile['gmail_access_token'];
					$userData['User']['auto_login_password'] = AuthComponent::password($userData['User']['gmail_access_id']);
					$this->User->id=$userData['User']['id'];
					$return=$this->User->save($userData['User']);
					
					$this->track_user_activity(array(
							"user_id"=>$userData['User']['id'],
							'activity'=> "User logged in with gmail from device"
					));
				}
				else
				{
					$response['type']="success";
					$response['message']="User already registered with everdrobe";
					$response['user_id']=$userData['User']['id'];
				}
			}
			
			if(isset($response['type']) && $response['type']=="success")
			{
				$this->track_user_activity(array(
						"user_id"=>$response['user_id'],
						'activity'=> "User logged in with ".$myProfile['connect_from']." from device"
				));
			}
			
			unset($userData['User']['password'],$userData['User']['auto_login_password'],$userData['User']['created_on']);
			if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
			
			//$response['profile']=$userData['User'];

			
			//load country as per shortcode
			$this->loadModel('Country');
			$this->Country->recursive=-1;
			$countryData=$this->Country->find('first',array('conditions'=>array('Country.short_name'=>$userData['User']['country'])));
				
			//load province as per short code
			$this->loadModel('Province');
			$this->Province->recursive=-1;
			$provinceData=$this->Province->find('first',array('conditions'=>array('Province.short_name'=>$userData['User']['province'])));
				
			//set full name of country and provice
			$userData['User']['country']=$countryData['Country']['country_name'];
			$userData['User']['province']=$provinceData['Province']['province_name'];
			
			// response with status if user is not active
			switch($userData['User']['status'])
			{
				case "inactive":
					$response['profile']=$userData['User'];
					$this->User->id=$userData['User']['id'];
					if($userData['User']['activation_key']=="")
					{
						$userData['User']['activation_key']=uniqid();
						$this->User->saveField('activation_key',$userData['User']['activation_key']);
					}
					// seetting up variables for email
					$variables=array(
							//"##user_name"=>$userData['User']['username'],
							"##activation_link"=>Router::url('/users/activate/'.$userData['User']['activation_key'],true)
					);
					if(isset($userData['User']['username']) && $userData['User']['username']!="")
					{
						$variables["##user_name"]=$userData['User']['username'];
					}
					else
					{
						$variables["##user_name"]=$userData['User']['first_name']." ".$userData['User']['last_name'];
					}
					// sending mail
					$this->sendNotifyMail('account_activate', $variables, $userData['User']['email'],1);
					break;
				case "suspended":
					$response['type']="error";
					$response['status']="suspended";
					$response['message']="Account is not active, please contact us by sending an email to: support@everdrobe.com";
					break;
				default:
					$userData['User']['birth_date']=($userData['User']['birth_date']!="")?$userData['User']['birth_date']:"0000-00-00";
					$userData['User']['gender']=($userData['User']['gender']!="")?$userData['User']['gender']:"";
					$userData['User']['province']=($userData['User']['province']!="")?$userData['User']['province']:"";
					$userData['User']['country']=($userData['User']['country']!="")?$userData['User']['country']:"";
					$userData['User']['shopify_id']=($userData['User']['shopify_id']!="")?$userData['User']['shopify_id']:"";
					
					$response['profile']=$userData['User'];
					break;
			}
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	
	function twitter_connect()
	{
		$tmpAuth=$this->Session->read('twitter_temp_auth');
		$this->Session->delete('twitter_temp_auth');
		if($this->twitterObj==null)
		{
			$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$tmpAuth['oauth_token'],$tmpAuth['oauth_token_secret']);
		}
		unset($tmpAuth);
		/* Request access tokens from twitter */
		$access_token = $this->twitterObj->getAccessToken($this->params->query['oauth_verifier']);
		
		
		if($this->User->find('count',array('conditions'=>array('User.twitter_id'=>$access_token['user_id'])))==0)
		{
			$this->User->validate=array();
			$userData=array();
			
			$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$access_token['oauth_token'],$access_token['oauth_token_secret']);
			//https://api.twitter.com/1/users/show.json?screen_name=TwitterAPI&include_entities=true
			$twitterInfo=$this->twitterObj->get('https://api.twitter.com/1/users/show.json',array('screen_name'=>$access_token['screen_name'],'include_entities'=>true));
			$name_array=explode(" ",$twitterInfo->name);
			//$userData['email']=$myProfile['email'];
			$rtolower($this->__randomString(6,6));
			$userData['password'] = AuthComponent::password($password);
			$userData['first_name']=$name_array[0];
			$userData['last_name']=$name_array[1];
			//$userData['username']=$name_array[2];
			$userData['birth_date']="";
			$userData['gender']="";
			$userData['twitter_id'] = $access_token['user_id'];
			$userData['twitter_access_token']=$access_token['oauth_token'];
			$userData['twitter_access_token_secret']=$access_token['oauth_token_secret'];
			$userData['auto_login_password'] = AuthComponent::password($access_token['user_id']);
			$userData['status']='active';
			if($this->User->save($userData))
			{
				$userData['id']=$this->User->id;
				echo "New user created ".$userData['id'];
			}
		
		}
		exit();
		
		/* Save the access tokens. Normally these would be saved in a database for future use. */
		$_SESSION['access_token'] = $access_token;
		
		/* Remove no longer needed request tokens */
		unset($_SESSION['oauth_token']);
		unset($_SESSION['oauth_token_secret']);
		
		
		exit();
		/* If HTTP response is 200 continue otherwise send to connect page to retry */
		if (200 == $connection->http_code) {
			/* The user has been verified and the access tokens can be saved for future use */
			$_SESSION['status'] = 'verified';
			header('Location: ./index.php');
		} else {
			/* Save HTTP status for error dialog on connnect page.*/
			header('Location: ./clearsessions.php');
		}
	}
	function link_with_twitter()
	{
		App::import('Vendor', 'twitter/twitterAuth');
		App::import('Vendor', 'twitterOauth/twitteroauth');
		$tmpAuth=$this->Session->read('twitter_temp_auth');
		$this->Session->delete('twitter_temp_auth');
		$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$tmpAuth['oauth_token'],$tmpAuth['oauth_token_secret']);
		unset($tmpAuth);

		if($this->Auth->user('type')=="admin")
		{
			$user_id=$this->Session->read('current_user');
			$admin=1;
		}
		else 
		{
			$user_id=$this->Auth->user('id');
			$admin=0;
		}
		
		/* Request access tokens from twitter */
		$access_token = $this->twitterObj->getAccessToken($this->params->query['oauth_verifier']);
		$connectedUser=$this->User->find('first',array('conditions'=>array('User.id !='=>$user_id,'User.twitter_id'=>$access_token['user_id'])));
		$this->User->validate=array();
		if($connectedUser)
		{
			// if user detail is found then disconnect twitter account from that user
			$userData=array();
			$userData['twitter_id'] = "";
			$userData['twitter_access_token']="";
			$userData['twitter_access_token_secret']="";
			$userData['id']=$connectedUser['User']['id'];
			$this->User->id=$connectedUser['User']['id'];
			$this->User->save($userData);
			
			
			$userSetting=array();
			$userSetting['tw_connected']=0;
			$userSetting['tw_post_drobe']=0;
			$userSetting['tw_post_fave']=0;
			$this->User->UserSetting->updateAll($userSetting,array('UserSetting.user_id'=>$connectedUser['User']['id']));
		}
		
		
		//Connecting with Twitter with current account
		$userData=array();
		$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$access_token['oauth_token'],$access_token['oauth_token_secret']);
		$twitterInfo=$this->twitterObj->get('https://api.twitter.com/1/users/show.json',array('screen_name'=>$access_token['screen_name'],'include_entities'=>true));
		$this->User->id=$user_id;
		
		//$userData['email']=$myProfile['email'];
		$userData['twitter_id'] = $access_token['user_id'];
		$userData['twitter_access_token']=$access_token['oauth_token'];
		$userData['twitter_access_token_secret']=$access_token['oauth_token_secret'];
		$userData['id']=$user_id;
		if($this->User->save($userData))
		{
			if(!$admin)
			{
				$this->Session->write("Auth.User.twitter_id",$userData['twitter_id']);
				$this->Session->write("Auth.User.twitter_access_token",$userData['twitter_access_token']);
				$this->Session->write("Auth.User.twitter_access_token_secret",$userData['twitter_access_token_secret']);
			}	
			$userSetting=array();
			$userSetting['tw_connected']=1;
			$userSetting['tw_post_drobe']=1;
			$userSetting['tw_post_fave']=1;
			$userSetting['user_id']=$user_id;
			$this->set("is_error",true);
			if($this->User->UserSetting->updateAll($userSetting,array('UserSetting.user_id'=>$user_id)))
			{
				if(!$admin)
				{
					$this->_updateUserSettingSession($user_id);
					$this->set("is_error",false);
					$this->Session->setFlash("Your account connected successfully with twitter account.","default",array("class"=>"success"));
				}
				else
				{
					$this->Session->setFlash("User account connected successfully with twitter account.","default",array("class"=>"success"));
				}
			}
			else
			{
				$this->Session->setFlash("Error occured in connect with twitter, please try again later.","default",array("class"=>"error"));
			}
		}
		else
		{
			$this->Session->setFlash("Error occured in connect with twitter, please try again later.","default",array("class"=>"error"));
		}
		$this->render('link_with_facebook');
	}
	function disconnect_from_twitter($user_id=null,$admin=0)
	{
		$this->User->validate=array();
		if($user_id==null && !$admin)
		{
			$user_id=$this->Auth->user('id');
		}
		$this->User->id=$user_id;
		$userData=array();
		$userData['twitter_id']="";
		$userData['twitter_access_token'] = "";
		$userData['twitter_access_token_secret']="";
		$userData['id'] = $user_id;
		//$this->User->create();
		if($this->User->save($userData))
		{
			if(!$admin)
			{
				$this->Session->write("Auth.User.twitter_id","");
				$this->Session->write("Auth.User.twitter_access_token","");
				$this->Session->write("Auth.User.twitter_access_token_secret","");
			}
			
			$userSetting=array();
			$userSetting['tw_connected']=0;
			$userSetting['tw_post_drobe']=0;
			$userSetting['tw_post_fave']=0;
			$userSetting['user_id']=$user_id;
			if($this->User->UserSetting->updateAll($userSetting,array('UserSetting.user_id'=>$user_id)))
			{
				if(!$admin)
				{
					$this->_updateUserSettingSession($user_id);
					$this->Session->setFlash("You account disconnected successfully with twitter account.","default",array("class"=>"success"));
				}
				else
				{
					$this->Session->setFlash("User account disconnected successfully with twitter account.","default",array("class"=>"success"));
				}
			}
			else
			{
				$this->Session->setFlash("Error occured in disconnect with twitter, please try again later.","default",array("class"=>"error"));
			}
		}
		else
		{
			$this->Session->setFlash("Error occured in disconnect with twitter, please try again later.","default",array("class"=>"error"));
		}
		$this->redirect($this->referer());
	}
	/*
	 * Webservice for twitter connect/disconnect
	 */
	function twitter($action="link",$user_id=null)
	{
		$response=array();
		if($action=="link" && empty($this->data))
		{
			$invalid_params=true;
		}
		else
		{
			$invalid_params=false;
		}
		if(!$invalid_params)
		{
			if($user_id>0)
			{
				$userData=array();
				$userData['id'] = $user_id;
				
				$userSetting=array();
				$userSetting['user_id']=$user_id;
				if($action=="link")
				{
					$userData['twitter_id'] = $this->data['twitter_id'];
					$userData['twitter_access_token']=$this->data['twitter_token'];
					$userData['twitter_access_token_secret']=$this->data['twitter_token_secret'];
					
					$userSetting['tw_connected']=1;
					$userSetting['tw_post_drobe']=1;
					$userSetting['tw_post_fave']=1;
				}
				else
				{
					$userData['twitter_id']="";
					$userData['twitter_access_token'] = "";
					$userData['twitter_access_token_secret']="";
					
					$userSetting['tw_connected']=0;
					$userSetting['tw_post_drobe']=0;
					$userSetting['tw_post_fave']=0;
				}
				unset($this->User->validate);
				if($this->User->save($userData))
				{
					if($this->User->UserSetting->updateAll($userSetting,array('UserSetting.user_id'=>$user_id)))
					{
						$response['type']="success";
						$response['message']="Twitter connection setting updated successfully";
						
					}
					else
					{
						$response['type']="success";
						$response['message']="Twitter connection setting updated successfully but error occured in post setting";
					}
				}
				else
				{
					$response['type']="error";
					$response['message']="Error occured in update your twitter connection, try gain later";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Provide user id with link";
			}
		}
		else 
		{
			$response['type']="error";
			$response['message']="Invalid params";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function profile($user_id="me")
	{
		if($user_id=="me")
		{
			$user_id=$this->Auth->user('id');
			$this->set('is_me',true);
			$this->set('title_for_layout',"My Profile");
		}
		else
		{
			// get actual id from users unique id
			$user_id=$this->User->field('id',array('User.unique_id'=>$user_id));
			$this->set('is_me',$user_id==$this->Auth->user('id'));
			
		}
		if($user_id==null)
		{
			// if profile id not valid then need to render invalid profile page 
			throw new NotFoundException("Page not found");
		}
		$this->User->id=$user_id;
		$this->User->recursive=-1;

		// getting user's detail
		$this->data=$this->User->read();
		
		// if user is followed by me or not?
		if($user_id!="me")
		{
			$followed=$this->User->Follower->find('count',array("conditions"=>array('Follower.user_id'=>$this->data['User']['id'],'Follower.follower_id'=>$this->Auth->user('id'))));
			$this->set('is_followed',$followed);
			$this->set('title_for_layout', $this->data['User']['username'] );
		}
		else
		{
			$this->set('is_followed',false);
		}
		
		
		// getting user totals
		$user_totals=$this->User->getUserTotals($user_id);

		// calculating My Totals in percentage
		//$user_totals['total_in']= $user_totals['vote_received']>0 ? round($user_totals['total_in']/$user_totals['vote_received']*100) : 0;
		//$user_totals['total_out']= $user_totals['vote_received']>0 ? round($user_totals['total_out']/$user_totals['vote_received']*100) : 0;
		$user_totals['total_in']= $user_totals['total_in'];
		$user_totals['total_out']= $user_totals['total_out'];
		
		if(Cache::read('flag_category')==null)
		{
			$this->loadModel('FlagCategory');
			$this->FlagCategory->_updateFlagCache();
		}
		
		$this->set('user_totals',$user_totals);
	}
	function profile_old($user_id="me")
	{
		if($user_id=="me")
		{
			$user_id=$this->Auth->user('id');
			$this->set('is_me',true);
			$this->set('title_for_layout',"My Profile");
		}
		else
		{
			// get actual id from users unique id
			$user_id=$this->User->field('id',array('User.unique_id'=>$user_id));
			$this->set('is_me',$user_id==$this->Auth->user('id'));
				
		}
		if($user_id==null)
		{
			// if profile id not valid then need to render invalid profile page
			//$this->render('invalid_profile');
			throw new NotFoundException("Page not found");
		}
		$this->User->id=$user_id;
		$this->User->recursive=-1;
		// getting user's detail
		$this->data=$this->User->read();
	
		// if user is followed by me or not?
		if($user_id!="me")
		{
			$followed=$this->User->Follower->find('count',array("conditions"=>array('Follower.user_id'=>$this->data['User']['id'],'Follower.follower_id'=>$this->Auth->user('id'))));
			$this->set('is_followed',$followed);
			$this->set('title_for_layout', $this->data['User']['username'] );
		}
		else
		{
			$this->set('is_followed',false);
		}
	
	
		$this->User->Drobe->recursive=-1;
		//$this->set('myDrobes',$this->User->Drobe->find('count',array('conditions'=>array('Drobe.user_id'=>$user_id))));
	
		$this->set('followers',$this->User->Follower->find('count',array('conditions'=>array('Follower.user_id'=>$user_id,'Follower.request'=>"accepted"))));
		$this->set('following',$this->User->Follower->find('count',array('conditions'=>array('Follower.follower_id'=>$user_id,'Follower.request'=>"accepted"))));
	
	
		// fetching user's drobe
		$drobes=$this->User->Drobe->find('all',array('fields'=>array('Drobe.unique_id','Drobe.uploaded_on','Drobe.file_name','Drobe.comment'),'conditions'=>array('Drobe.user_id'=>$user_id),'order'=>'Drobe.uploaded_on DESC','limit'=>12));
	
		// claculating My Totals
		$drobes_info=array_pop(array_pop($this->User->Drobe->find('all',array('fields'=>array('count(*) as total_drobes,sum(Drobe.views) as total_views','sum(Drobe.total_rate) as total_rate','sum(Drobe.total_in) as total_in','sum(Drobe.total_out) as total_out'),'conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.rate_status'=>'open')))));
		/* $drobes_info['total_in']= $drobes_info['total_rate']>0 ? round($drobes_info['total_in']/$drobes_info['total_rate']*100) : 0;
		$drobes_info['total_out']= $drobes_info['total_rate']>0 ? round($drobes_info['total_out']/$drobes_info['total_rate']*100) : 0; */
		$drobes_info['total_in']= $drobes_info['total_in'];
		$drobes_info['total_out']= $drobes_info['total_out'];
		$this->set('userData',$this->data['User']);
		$this->set('usersDrobe',$drobes);
		$this->set('drobes_info',$drobes_info);
	
		//$total_comments=$this->User->Rate->find('count',array('conditions'=>array('Rate.user_id'=>$user_id,'Rate.comment NOT LIKE '=>"")));
		$total_rates_given=$this->User->Rate->find('count',array('conditions'=>array('Rate.user_id'=>$user_id)));
		$this->set('rates_given',$total_rates_given);
	
		// calculating total favourites
		$this->loadModel('Favourite');
		$this->Favourite->recursive=-1;
		$total_favorite=$this->Favourite->find('count',array(
				'conditions'=>array('Favourite.user_id'=>$user_id,'Drobe.rate_status'=>'open','Drobe.id > '=>0,'Drobe.deleted'=>0),
				'joins'=>array(
						array(
								'table' => 'drobes',
								'alias' => 'Drobe',
								'type' => 'LEFT',
								'conditions' => array(
										'Drobe.id = Favourite.drobe_id',
								)
						))
	
		));
		$this->set('favourites',$total_favorite);
	}
	/*
	 * 
	 * Webservice for getting user profile detail
	 */
	function get_profile($user_id=null)
	{
		$response=array();
		if($user_id==null)
		{
			$response['type']="error";
			$response['message']="Please provide user ud with url";
		}
		else
		{	
			$this->User->id=$user_id;
			$this->User->recursive=-1;
			// getting user's detail
			$userData=$this->User->read();
			if($userData)
			{
				$this->User->Drobe->recursive=-1;
				
				// getting user totals
				$user_totals=$this->User->getUserTotals($user_id);
				// calculating My Totals in percentage
				//$user_totals['total_in']= $user_totals['vote_received']>0 ? round($user_totals['total_in']/$user_totals['vote_received']*100) : 0;
				//$user_totals['total_out']= $user_totals['vote_received']>0 ? round($user_totals['total_out']/$user_totals['vote_received']*100) : 0;
				$user_totals['total_in']= $user_totals['total_in'];
				$user_totals['total_out']= $user_totals['total_out'];
				//$user_totals['given_comments']=$user_totals['vote_given'];
				$user_totals['given_comments']=$user_totals['feedback_given'];
				$user_totals['received_comments']=$user_totals['feedback_received'];
				$user_totals['favourites']=$user_totals['total_favourites'];
				unset($user_totals['id'],$user_totals['user_id']);
				$response['my_totals']=$user_totals;
				$response['my_totals']['ebx_message']=Configure::read("setting.ebx_message");
				$response['my_totals']['month_promo_rate_ebx']=Configure::read("setting.month_promo_rate_ebx");
				
				//is followed by loggedin user or not?
				if(isset($this->data['user_id']) && $this->data['user_id']>0)
				{
					$followed=$this->User->Follower->find('count',array("conditions"=>array('Follower.user_id'=>$user_id,'Follower.follower_id'=>$this->data['user_id'])));
					$userData['User']['followed_by_me']= $followed>0;
					
					if($this->data['user_id']==$user_id)
					{
						//get short_code of country from full name
						$this->loadModel('Country');
						$this->Country->recursive=-1;
						$countryData=$this->Country->find('first',array('conditions'=>array('Country.short_name'=>$userData['User']['country'])));
						
						//get short_code of province from full name
						$this->loadModel('Province');
						$this->Province->recursive=-1;
						$provinceData=$this->Province->find('first',array('conditions'=>array('Province.short_name'=>$userData['User']['province'])));
						
						$userData['User']['country_full']="".$countryData['Country']['country_name'];
						$userData['User']['province_full']="".$provinceData['Province']['province_name'];
					}
					
				}
				else $userData['User']['followed_by_me']= false;
				
				
				// set photo url and unset unnecessory columns
				unset($userData['User']['password'],$userData['User']['shopify_id'],
						$userData['User']['shopify_password'],$userData['User']['os_version'],
						$userData['User']['app_version'],$userData['User']['device_type'],
						$userData['User']['auto_login_password'],$userData['User']['created_on']);
				
				if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
				
				/*  Fix for Mobile app gender convention */
				$userData['User']['gender'] = ucwords($userData['User']['gender']);
				
				if(isset($userData['User']['birth_date']) && $userData['User']['birth_date']=="") $userData['User']['birth_date ']="";				


				$userData['User']['birth_date']=($userData['User']['birth_date']!="")?$userData['User']['birth_date']:"0000-00-00";
				$userData['User']['gender']=($userData['User']['gender']!="")?$userData['User']['gender']:"";
				$userData['User']['province']=($userData['User']['province']!="")?$userData['User']['province']:"";
				$userData['User']['country']=($userData['User']['country']!="")?$userData['User']['country']:"";
				$userData['User']['shopify_id']=($userData['User']['shopify_id']!="")?$userData['User']['shopify_id']:"";
				$response['profile']=$userData['User'];
			}
			else
			{
				$response['type']="error";
				$response['message']="provided user id not exist";
			}
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	function get_profile_old($user_id=null)
	{
		$response=array();
		if($user_id==null)
		{
			$response['type']="error";
			$response['message']="Please provide user ud with url";
		}
		else
		{
				
			$this->User->id=$user_id;
			$this->User->recursive=-1;
			// getting user's detail
			$userData=$this->User->read();
			if($userData)
			{
				$this->User->Drobe->recursive=-1;
	
				// claculating My Totals
				$total_comments=$this->User->Rate->find('count',array('conditions'=>array('Rate.user_id'=>$user_id,'Rate.comment NOT LIKE '=>"")));
	
				$my_totals=array_pop(array_pop($this->User->Drobe->find('all',array('fields'=>array('count(*) as total_drobes,sum(Drobe.views) as total_views','sum(Drobe.total_rate) as total_rate','sum(Drobe.total_in) as total_in','sum(Drobe.total_out) as total_out'),'conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.rate_status'=>'open','Drobe.deleted'=>0)))));
				/* $my_totals['total_in']= $my_totals['total_rate']>0 ? round($my_totals['total_in']/$my_totals['total_rate']*100) : 0;
				$my_totals['total_out']= $my_totals['total_rate']>0 ? round($my_totals['total_out']/$my_totals['total_rate']*100) : 0; */
				$my_totals['total_in']= $my_totals['total_in'];
				$my_totals['total_out']= $my_totals['total_out'];
				//$my_totals['buttons']=$userData['User']['buttons'];
	
				// getting counter of user's followers
				$my_totals['followers']=$this->User->Follower->find('count',array('conditions'=>array('Follower.user_id'=>$user_id,'Follower.request'=>"accepted")));
	
				//getting counter ot user's followings
				$my_totals['followings']=$this->User->Follower->find('count',array('conditions'=>array('Follower.follower_id'=>$user_id,'Follower.request'=>"accepted")));
	
				// getting counter of user's favourite drobes
				$this->loadModel('Favourite');
				$this->Favourite->recursive=-1;
				$my_totals['favourites']=$this->Favourite->find('count',array(
						'conditions'=>array('Favourite.user_id'=>$user_id,'Drobe.rate_status'=>'open','Drobe.id > '=>0,'Drobe.deleted'=>0),
						'joins'=>array(
								array(
										'table' => 'drobes',
										'alias' => 'Drobe',
										'type' => 'LEFT',
										'conditions' => array(
												'Drobe.id = Favourite.drobe_id',
										)
								))
	
				));
	
				$my_totals['given_comments']=$this->User->Rate->find('count',array('conditions'=>array("Rate.user_id"=>$user_id)));
	
				// fetching user's drobe
				$drobe_ids=$this->User->Drobe->find('list',array('fields'=>array('Drobe.id'),'conditions'=>array('Drobe.user_id'=>$user_id)));
				if(count($drobe_ids)>0)
				{
					$my_totals['received_comments']=$this->User->Rate->find('count',array('conditions'=>array("Rate.comment !="=>"","Rate.drobe_id"=>$drobe_ids)));
				}
				else $my_totals['received_comments']=0;
	
				unset($userData['User']['buttons']);
				$response['my_totals']=$my_totals;
	
	
				unset($userData['User']['password'],$userData['User']['auto_login_password'],$userData['User']['created_on']);
				if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
	
				//is followed by loggedin user or not?
				if(isset($this->data['user_id']) && $this->data['user_id']>0)
				{
					$followed=$this->User->Follower->find('count',array("conditions"=>array('Follower.user_id'=>$user_id,'Follower.follower_id'=>$this->data['user_id'])));
					$userData['User']['followed_by_me']= $followed>0;
				}
				else $userData['User']['followed_by_me']= false;
	
					
				$response['profile']=$userData['User'];
			}
			else
			{
				$response['type']="error";
				$response['message']="provided user id not exist";
			}
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function my_totals()
	{
		$user_id=$this->Auth->user('id');
		$totals=$this->User->getUserTotals($user_id);
		// claculating My Totals
		//$drobes_info['total_in']= $totals['vote_received']>0 ? round($totals['total_in']/$totals['vote_received']*100) : 0;
		//$drobes_info['total_out']= $totals['vote_received']>0 ? round($totals['total_out']/$totals['vote_received']*100) : 0;
		
		
		$drobes_info['total_in']= $totals['total_in'];
		$drobes_info['total_out']= $totals['total_out'];
		$drobes_info['vote_received']=$totals['vote_received'] >0 ? $totals['vote_received'] : 0;
		$drobes_info['vote_given']=$totals['vote_given'];
		$drobes_info['current_ebx']=$totals['current_ebx_point'];
		$drobes_info['total_earned_ebx']=$totals['earned_ebx_point'];
		
	
		return $drobes_info;
	}
	function my_totals_old()
	{
		$user_id=$this->Auth->user('id');
		// claculating My Totals
		$total_comments=$this->User->Rate->find('count',array('conditions'=>array('Rate.user_id'=>$user_id,'Rate.comment NOT LIKE '=>"")));
		$drobes_info=$this->User->Drobe->find('all',array('fields'=>array('count(*) as total_drobes,sum(Drobe.views) as total_views','sum(Drobe.total_rate) as total_rate','sum(Drobe.total_in) as total_in','sum(Drobe.total_out) as total_out'),'conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.rate_status'=>'open','Drobe.deleted'=>0)));
		$drobes_info=array_pop($drobes_info);
		$drobes_info=$drobes_info[0];
		/* $drobes_info['total_in']= $drobes_info['total_rate']>0 ? round($drobes_info['total_in']/$drobes_info['total_rate']*100) : 0;
		$drobes_info['total_out']= $drobes_info['total_rate']>0 ? round($drobes_info['total_out']/$drobes_info['total_rate']*100) : 0; */
		$drobes_info['total_in']= $drobes_info['total_in'];
		$drobes_info['total_out']= $drobes_info['total_out'];
		$drobes_info['total_comments']=$total_comments >0 ? $total_comments : 0;
		$drobes_info['given_comments']=$this->User->Rate->find('count',array('conditions'=>array("Rate.user_id"=>$user_id)));
		
		return $drobes_info;
	}
	function edit_profile()
	{
		$this->User->id=$this->Auth->user('id');
		
		if(!empty($this->data))
		{	
			unset($this->User->validate['username']);
			$user_data=$this->data['User'];
			unset($user_data['username']);
			$user_data['birth_date']=$user_data['birth_year']."-".$user_data['birth_month']."-".$user_data['birth_day'];
			$user_data['User']=$user_data;
			/* if(isset($user_data['User']['website']) && $user_data['User']['website']!="")
			{
				if(!(stripos($user_data['User']['website'], "http://")===0 || stripos($user_data['User']['website'], "https://")===0))
				{
					$user_data['User']['website']="http://".$user_data['User']['website'];
				}
			} */

			//unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['email'],$this->User->validate['agree']);
			
			unset($this->User->validate['email'],$this->User->validate['agree']);
				
			$this->User->set($user_data);
			//unset($this->User->validate);
			if($this->User->validates())
			{
				if($this->User->save($user_data['User']))
				{
					// overwrite session value of logged in user
					$userdata=$this->User->read();
					$this->Auth->login($userdata['User']);
					
					$this->Session->setFlash("Profile has been updated successfully",'default',array('class'=>"success"));
					$this->redirect(array("action"=>"profile"));
				}
				else
				{
					$this->Session->setFlash("Error occured in updating your profile detail",'default');
				}
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
			
		}
		else
		{
			$this->data=$this->User->read();
		}
		
		$this->loadModel('Country');
		$countries=$this->Country->find('all',array('conditions'=>array('status'=>'active')));
		$this->Country->recursive=-1;
		$countryList=array();
		foreach($countries as $country)
		{
			$countryList[$country['Country']['short_name']]=$country['Country']['country_name'];
		}
		$this->set('countryList',$countryList);
			
		$this->set('title_for_layout', "Edit Profile" );
	}
	
	
	
	function settings()
	{
		if(!empty($this->data))
		{
			if($this->User->UserSetting->save($this->data))
			{
				$this->_updateUserSettingSession($this->Auth->user('id'));
				
				$this->track_user_activity(array(
						"user_id"=>$this->Auth->user('id'),
						'activity'=> "Changed settings from Website \n".json_encode($this->data['UserSetting']),
						"device_type"=>"Web",
				));
				
				$this->Session->setFlash("Your settings has been updated successfully",'default',array('class'=>"success"));
				$this->redirect($this->referer());
			}
			else
			{
				$this->Session->setFlash("Error occured in save your changes");
				$this->redirect($this->referer());
			}
			
		}
		
		$this->User->UserSetting->id=$this->User->UserSetting->field('id',array("UserSetting.user_id"=>$this->Auth->user('id')));
		$this->data=$this->User->UserSetting->read();
		if(!$this->data['UserSetting']['fb_connected'])
		{
			$this->set("facebookUrl",$this->facebook->getLoginUrl(
					array(
							'scope' => 'email,user_about_me,friends_about_me,read_stream,publish_stream,offline_access',
							'redirect_uri' => "http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"link_with_facebook")),
							'display'=>"popup"
					)
			));
		}
		else
		{
			try {
			$fb_connected=$this->facebook->api('/'.$this->Auth->user('facebook_id'));
			$this->set("fb_connected_info",$fb_connected);
			}
			catch(Exception $e)
			{
				$this->set("fb_connected_info",false);
				$this->set("facebookUrl",$this->facebook->getLoginUrl(
						array(
								'scope' => 'email,user_about_me,friends_about_me,read_stream,publish_stream,offline_access',
								'redirect_uri' => "http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"link_with_facebook")),
								'display'=>"popup"
						)
				));
			}
		}
		App::import('Vendor', 'twitter/twitterAuth');
		App::import('Vendor', 'twitterOauth/twitteroauth');
			
		if(!$this->data['UserSetting']['tw_connected'])
		{
			if(!isset($this->params->query['oauth_verifier']))
			{
				$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"));
				$tmp_auth=$this->twitterObj->getRequestToken(Configure::read("twitter.callback_url"));
				$this->Session->write('twitter_temp_auth',$tmp_auth);
				$this->set("twitterLoginUrl",$this->twitterObj->getAuthorizeURL($tmp_auth));
			}
		}
		else
		{
			try {
				$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$this->Auth->user('twitter_access_token'),$this->Auth->user('twitter_access_token_secret'));
				$tw_connected=$this->twitterObj->get('https://api.twitter.com/1/users/show/'.$this->Auth->user('twitter_id').'.json');
				$this->set("tw_connected_info",$tw_connected);
			}
			catch(Exception $e)
			{
				$this->set("tw_connected_info",false);
				$this->set("twitterLoginUrl",$this->twitterObj->getAuthorizeURL($tmp_auth));
			}
		}
		$this->set('title_for_layout', "Change Settings" );
	}
	
	function load_settings($user_id=null)
	{
		$response=array();
		if($user_id>0)
		{
				$settings=$this->User->UserSetting->findByUserId($user_id);
				if($settings)
				{
					unset($settings['UserSetting']['id'],$settings['UserSetting']['user_id']);
					// send earning perecentage for admin from selller
					$settings['UserSetting']['earning_percent'] = Configure::read('setting.everdrobe_earning_percent');
					$response['type']="success";
					$response['settings']=$settings['UserSetting'];
				}
				else
				{
					$response['type']="error";
					$response['message']="Error occured in fetch  your settings";
				}
			
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function change_settings($user_id=null)
	{
		$response=array();
		if($user_id>0)
		{
			if(!empty($this->data))
			{
				$settings=$this->data;
				if($this->User->UserSetting->updateAll($settings,array("user_id"=>$user_id)))
				{
					
					$this->track_user_activity(array(
							"user_id"=>$user_id,
							'activity'=> "Setting Changed: \n".json_encode($this->data),
					));
					
					$response['type']="success";
					$response['message']="Your Settings updated successfully";
				}
				else
				{
					$response['type']="error";
					$response['message']="Error occured in update your settings";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="In valid parameter";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
			
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
		
	}
	
	
	
	/*
	 * Webservice for change email
	 * pass: $user_id= logged in user's id 
	 * POST
	 *   first_name
	 *   lst_name
	 *   bio
	 *   gender
	 *   image
	 *   birth_date
	 *   country
	 *   province
	 *   relationship_status
	 *   website
	 */
	function change_profile($user_id=null)
	{
		$response=array();
		if($user_id!=null)
		{
			$this->User->id=$user_id;
			if(!empty($this->data))
			{
				$user_data=array();
				if(isset($this->data['first_name']) && $this->data['first_name']!="")
				{
					$user_data['first_name']=$this->data['first_name'];
				}
				if(isset($this->data['last_name']) && $this->data['last_name']!="")
				{
					$user_data['last_name']=$this->data['last_name'];
				}
				//$user_data['username']=$this->data['username'];
				$user_data['bio']=$this->data['bio'];
				
				
				/*  Fix for Mobile app gender convention */
				
				if(isset($this->data['gender']) && $this->data['gender']!="")
				{
					$user_data['gender']=strtolower($this->data['gender']);
				}
				
				
				if(isset($this->data['image']) && $this->data['image']!="")
				{
					$image_file=$this->_createProfileImage($this->data['image']);
					if($image_file!="") $user_data['photo']=$image_file;
				}
				//$tmp_date=explode("-",$this->data['birth_date']);
				//$user_data['birth_date']=date('Y-m-d',strtotime($tmp_date[2]."-".$tmp_date[0]."-".$tmp_date[1]));
				if(isset($this->data['birth_date']) && $this->data['birth_date']!="")
				{
					$user_data['birth_date']=$this->data['birth_date'];
				}

				$country_full_name=$this->data['country'];
				$province_full_name=$this->data['province'];
				
				//get short_code of country from full name
				$this->loadModel('Country');
				$this->Country->recursive=-1;
				$countryData=$this->Country->find('first',array('conditions'=>array('Country.country_name'=>$this->data['country'])));
				
				//get short_code of province from full name
				$this->loadModel('Province');
				$this->Province->recursive=-1;
				$provinceData=$this->Province->find('first',array('conditions'=>array('Province.province_name'=>$this->data['province'])));
				
				$user_data['country']=$countryData['Country']['short_name'];
				$user_data['province']=$provinceData['Province']['short_name'];
				
				$user_data['website']=$this->data['website'];
				
				
				if($user_data['website']!="")
				{
					if(!(stripos($user_data['website'], "http://")===0 || stripos($user_data['website'], "https://")===0))
					{
						$user_data['website']="http://".$user_data['website'];
					}
				}
				
				unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['username'],$this->User->validate['email'],$this->User->validate['birth_date'],$this->User->validate['agree']);
				
				/*if(isset($this->data['email']) && $this->data['email']!="")
				{
					$user_data['email']=$this->data['email'];
					$this->User->validate['email']=array(
							'email'=>array(
									'rule'=>"email",
									'required'=>true,
									'message'=>'Enter valid email id'
							),
							'unique'=>array(
									'rule'=>'isUnique',
									'message'=>'Entered email id already used'
							),
							'notEmpty'=>array(
									'rule' => 'notEmpty',
									'on'=>'update',
									'message'=> 'Email id is required'
							));
				}*/
				
				
				
				//$user_data['relationship_status']=$this->data['relationship_status'];
				
				$user_data['User']=$user_data;
				
				
				$this->User->set($user_data);
					
				if($this->User->validates())
				{
					if($this->User->save($user_data['User']))
					{
						$response['type']="success";
						$response['message']="profile updated successfully";
						$this->User->recursive=-1;
						$response['profile']=$this->User->findById($user_id);
						
						$response['profile']['User']['country']=$country_full_name;
						$response['profile']['User']['province']=$province_full_name;
						
						/*Fixing gender convention for iphone app */
						$response['profile']['User']['gender'] = ucwords($response['profile']['User']['gender']);
						
						if($response['profile']['User']['photo']!="") $response['profile']['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$response['profile']['User']['photo'],true);
					}
					else
					{
						$response['type']="error";
						$response['message']="Error occured in update your profile, try again later";
					}
				}
				else
				{
					$response['type']="error";
					//$response['erors']=$this->User->validationErrors;
					if(!empty($this->User->validationErrors))
					{
						$response['invalid_fields']=$this->User->validationErrors;
						$this->User->validationErrors=array();
						$first_field=array_pop(array_pop(array_reverse($response['invalid_fields'])));
						$response['message']=$first_field;
						unset($response['invalid_fields']);
					}
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Invalid parameteres";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide user id";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function update_username($user_id=null)
	{
		$response=array();
		if($user_id!=null)
		{
			$this->User->id=$user_id;
			if(!empty($this->data))
			{
				$user_data=array();
				$user_data['username']=$this->data['username'];
				unset($this->User->validate['first_name'],$this->User->validate['last_name'],$this->User->validate['email'],$this->User->validate['birth_date'],$this->User->validate['agree']);
				$user_data['User']=$user_data;
				$this->User->set($user_data);
				if($this->User->validates())
				{
					if($this->User->save($user_data['User']))
					{
						$response['type']="success";
						$response['message']="Username updated successfully";
						$this->User->recursive=-1;
						$userData = $this->User->read();
						
						unset($userData['User']['password'],$userData['User']['auto_login_password'],$userData['User']['created_on']);
						if($userData['User']['photo']!="") $userData['User']['photo']=Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$userData['User']['photo'],true);
							
						//load country as per shortcode
						$this->loadModel('Country');
						$this->Country->recursive=-1;
						$countryData=$this->Country->find('first',array('conditions'=>array('Country.short_name'=>$userData['User']['country'])));
							
						//load province as per short code
						$this->loadModel('Province');
						$this->Province->recursive=-1;
						$provinceData=$this->Province->find('first',array('conditions'=>array('Province.short_name'=>$userData['User']['province'])));
							
						//set full name of country and provice
						$userData['User']['country']=$countryData['Country']['country_name'];
						$userData['User']['province']=$provinceData['Province']['province_name'];
							
						/*  Fix for Mobile app gender convention */
						$userData['User']['gender']=ucwords($userData['User']['gender']);
							
						$response['profile']=$userData['User'];
						
						
						
					}
					else
					{
						$response['type']="error";
						$response['message']="Error occured in update your username, try again later";
					}
				}
				else
				{
					$response['type']="error";
					//$response['erors']=$this->User->validationErrors;
					if(!empty($this->User->validationErrors))
					{
						$response['invalid_fields']=$this->User->validationErrors;
						$this->User->validationErrors=array();
						$first_field=array_pop(array_pop(array_reverse($response['invalid_fields'])));
						$response['message']=$first_field;
						unset($response['invalid_fields']);
					}
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="Invalid parameteres";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="provide user id";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function register_shopify($data=null,$user_id)
	{
		if(Configure::read('shopify.auto_register') && !empty($data))
		{
			$shopifyData['first_name']=$user_data['User']['first_name'];
			$shopifyData['last_name']=$user_data['User']['last_name'];
			$shopifyData['username']=$userData['User']['username'];
			$shopifyData['email']=$user_data['User']['email'];
			$shopifyData['password']=$currentPassword;
			
			$userData["customer"]=array(
					"first_name"=>$data['first_name'],
					"last_name"=>$data['last_name'],
					"username"=>$data['username'],
					"email"=>$data['email'],
					"verified_email"=>true,
					"password"=>$data['password'],
					"password_confirmation"=>$data['password']
				);
			
			$json=json_encode($userData);
			$response=$this->shopify('/customers',$json,"POST");
			
			//save response shopify customer id to our database
			if(isset($response['customer']))
			{
				$this->User->id=$user_id;
				$this->User->saveField('shopify_id',$response['customer']['id']);
			}
		}
	}
	function change_email()
	{
		$this->redirect(array("controller"=>"users","action"=>"edit_profile"));
		if(!empty($this->data))
		{
			$this->User->validate=array(
				'email'=>array(
					'email'=>array(
							'rule'=>"email",
							'required'=>true,
							'message'=>'Enter valid email id'
					),
					'unique'=>array(
						 	'rule'=>'isUnique',
							'message'=>'Entered email id already used'
					),
					'notEmpty'=>array(
							'rule' => 'notEmpty',
							'on'=>'update',
							'message'=> 'Email id is required'
					)
				),
				'confirm_email'=>array(
					'email'=>array(
							'rule'=>"email",
							'required'=>true,
							'message'=>'Enter valid email id'
					),
					'identicalFieldValues' => array(
					        'rule' => array('identicalFieldValues', 'email' ),
					        'message' => 'New email and confirm email must be match'
					)
				)
			);
			$this->User->set($this->data);
			if($this->User->validates())
			{
				$this->User->id=$this->Auth->user('id');
				if($this->User->saveField('email',$this->data['User']['email']))
				{
					$this->Session->write('Auth.User.email',$this->data['User']['email']);
					$this->Session->setFlash("Your Email is updated",'default',array('class'=>"success"));
					return $this->redirect($this->referer());
				}
				else
				{
					$this->set('error_message',"Error occured in change your email id");
				}
			}
				
		}
	}
	function stream_category()
	{
		$size_name="";
		$brand_name="";
		$price_range="";
		if(!empty($this->data))
		{
			$this->loadModel('UserFilter');	
			
			
			/** Code for category ids **/
			if($this->data['category_ids']!='ALL')
			{
				$category_id = implode(",",$this->data['category_ids']);  // category id separated by comma
			}
			else
			{
				$category_id = $this->data['category_ids'];
			}
			
			/** Code for size name **/
			if($this->data['size_name']!='{ALL}') //if sizes not equal to blank then it separate by ||
			{
				if($this->data['size_name']!="")
					$size_name = implode("||",$this->data['size_name']);  // size name sparated by || (pipe sign)			
			}
			else
			{
				$size_name = $this->data['size_name'];
			}
			
			/** Code for brand name **/
			if($this->data['brand_name']!='{ALL}') //if brands not equal to blank then it separate by ||
			{
				if($this->data['brand_name']!="")
					$brand_name = implode("||",$this->data['brand_name']);  // brand name sparated by || (pipe sign)
				$brand_name = addslashes($brand_name);
			}
			else
			{
				$brand_name = $this->data['brand_name'];
			}
			
			
			/** Code for price range  **/
			if($this->data['price_range']!='{ALL}') //if price not equal to all then it separate by ||
			{
				if($this->data['price_range']!="")
				$price_range = implode("||",$this->data['price_range']);  // Price range sparated by || (pipe sign)
			}
			else
			{
				$price_range = $this->data['price_range'];
			}
			
			// Check for filter already exist for that particular user if exist then update and not exist then save new filter for that user.
			$is_find_filter = $this->UserFilter->find('first',array('conditions'=>array('UserFilter.user_id'=>$this->Auth->user('id'))));
			if(!empty($is_find_filter))
			{
				if($this->UserFilter->updateAll(array
										('gender'=>"'".$this->data['gender']."'",
										'category_ids'=>"'".$category_id."'",
										'size_name'=>"'".$size_name."'",
										'brand_name'=>"'".$brand_name."'",
										'price_range'=>"'".$price_range."'",
										'only_new_brand'=>"'".$this->data['only_new_brand']."'",
										'highest_rated'=>"'".$this->data['highest_rated']."'"				
										),array("UserFilter.user_id"=>$this->Auth->User('id'))))
				{
					echo "success";
				}
				else
				{
					echo "error";
				}
			}
			else
			{
				
				$data= array(
						"user_id"=>$this->Auth->user('id'),
						"gender"=>$this->data['gender'],
						"category_ids"=>$category_id,
						"size_name"=>$size_name,
						"brand_name"=>$brand_name,
						"price_range"=>$price_range,
						"only_new_brand"=>$this->data['only_new_brand'],
						"highest_rated"=>($this->data['highest_rated']!="")?$this->data['highest_rated']:"0"
						);
				if($this->UserFilter->save($data))
				{
					echo "success";
				}
				else 
				{
					echo "error";
				}
			}
			
			/*
			$this->UserFilter->user_id=$this->Auth->user('id');
			$this->User->id=$this->Auth->user('id');
			if($this->User->UserSetting->updateAll(array('gender_filter'=>"'".$this->data['gender']."'"),array("UserSetting.user_id"=>$this->User->id)))
			{
				$this->_updateUserSettingSession($this->User->id);
			}
			$this->User->StreamCategory->deleteAll(array('user_id'=>$this->User->id));
			if(is_array($this->data['categories']))
			{
				$streamCategory=array();
				foreach($this->data['categories'] as $category)
				{
					$streamCategory[]=array('category_id'=>$category,'user_id'=>$this->Auth->user('id'));
				}
				if($this->User->StreamCategory->saveAll($streamCategory))
				{
					echo "success";
				}
				else 
				{
					echo "error";
				}
			}
			else
			{
				echo "success";
			}*/
		}
		else echo "invalid parameters";
		exit();
	}
	
	/** This function reset the filter that set by user*/
	function reset_filter()
	{
		$this->loadModel('UserFilter');
		$is_find_filter = $this->UserFilter->find('first',array('conditions'=>array('UserFilter.user_id'=>$this->Auth->user('id'))));
		if($is_find_filter) //Check the filter for loggein user is available or not. if available then update it other wise it display message error.
		{
			if($this->UserFilter->updateAll(array
						('gender'=>"'both'",
						'category_ids'=>"'ALL'",
						'size_name'=>"'{ALL}'",
						'brand_name'=>"'{ALL}'",
						'price_range'=>"'{ALL}'",
						'only_new_brand'=>"'both'",
						'highest_rated'=>"0"
				),array("UserFilter.user_id"=>$this->Auth->User('id'))))
			{
				echo "success";
			}		
			else
			{
				echo "error";
			}
		}
		else
		{
			if($this->UserFilter->save(array
						(
						'user_id'=>$this->Auth->user('id'),
						'gender'=>"both",
						'category_ids'=>"ALL",
						'size_name'=>"{ALL}",
						'brand_name'=>"{ALL}",
						'price_range'=>"{ALL}",
						'only_new_brand'=>"both",
						'highest_rated'=>"0"
						)
					))
			{
				echo "success";
			}
			else{
				echo "error";
			}
			
		}	
		exit();
	}
	
	
	function get_stream_category($user_id=null)
	{
		$response=array();
	
		if($user_id==null)
		{
			$response['type']="error";
			$response['message']="please pass user id";
	
		}
		else
		{
			$this->loadModel('StreamCategory');
			$streamCategory=$this->StreamCategory->_loadUsersCategory($user_id);
			$response['type']="success";
			$response['categories']= is_array($streamCategory['categories']) ? array_values($streamCategory['categories']) : array($streamCategory['categories']);
			$response['gender_filter']= $streamCategory['gender_filter'];
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
	}
	
	function get_stream_category_new($user_id=null)
	{
		if($user_id==null)
		{
			$response['type']="error";
			$response['message']="please pass user id";
				
		}
		else
		{
			// code commented by @sadikhasan
			/*$this->loadModel('StreamCategory');
			$streamCategory=$this->StreamCategory->_loadUsersCategory($user_id);
			$response['type']="success";
			$response['categories']= is_array($streamCategory['categories']) ? array_values($streamCategory['categories']) : array($streamCategory['categories']);
			$response['gender_filter']= $streamCategory['gender_filter'];*/
			
			/** start code by @sadikhasan */
			 /* $this->loadModel('UserFilter');
		  
               $user_filter = $this->UserFilter->find('first',array('conditions'=> array('UserFilter.user_id'=>$user_id)));        
                if(!empty($user_filter))
                {
                    $response['type']="success";
                    $response['user_filter']=$user_filter['UserFilter'];
                }
                else
                {*/
                    $response['type']="success";
                    $response['user_filter']=array(
                    							"category_ids"=>"ALL",
                    						    "gender"=>"both",
					                    		"price_range"=> "",
					                    		"brand_name"=>"",
					                    		"size_name"=>"",
					                    		"only_sell_items"=> "0",
					                    		"only_new_brand"=> "both",
					                    		"highest_rated"=> "0",
					                    		"only_following"=> "0"
                    							);
                //}
            
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
		
	}
	
	function update_stream_category($user_id=null)
	{
		$response=array();
		if($user_id==null)
		{
			$response['type']="error";
			$response['message']="please pass user id";
				
		}
		else
		{
			/* comment by @sadikhasan for new table named user_filters   
			if(!empty($this->data))
			{
				$this->User->id=$user_id;
				
				$this->User->UserSetting->updateAll(array('gender_filter'=>"'".$this->data['gender']."'"),array("UserSetting.user_id"=>$user_id));
				
				$this->User->StreamCategory->deleteAll(array('user_id'=>$user_id));
				if(trim($this->data['selected_ids'])=="ALL")
				{
					$response['type']="success";
					$response['message']="Stream category list is updated";
				}
				else
				{
					$category_ids=explode(",",$this->data['selected_ids']);
					if(is_array($category_ids))
					{
						$streamCategory=array();
						foreach($category_ids as $category)
						{
							$streamCategory[]=array('category_id'=>$category,'user_id'=>$user_id);
						}
						if($this->User->StreamCategory->saveAll($streamCategory))
						{
							$response['type']="success";
							$response['message']="Stream category list is updated";
						}
						else
						{
							$response['type']="error";
							$response['message']="error occured in update stream category list";
						}
					}
					else
					{
						$response['type']="error";
						$response['message']="pass category ids in comma saperated format";
					}
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="invalid parameters";
			} */
			
			/** code start here by @sadikhasan*/
			if(!empty($this->data))
			{
				$this->log("POST".print_r($this->data,true),"UserFilter Webservice");
				$this->loadModel('UserFilter');
				$filter_settings = $this->UserFilter->find('first',array('conditions'=>array('UserFilter.user_id'=>$user_id)));
				$user_filter = array();
				if(!empty($filter_settings)) // If already settings in table then it update for that userid
				{
					$user_filter['user_id'] = "'".$user_id."'";
					$user_filter['category_ids'] = "'".$this->data['category_ids']."'";
					$user_filter['gender'] = "'".$this->data['gender']."'";
					$user_filter['price_range'] = "'".$this->data['price_range']."'";
					$user_filter['brand_name'] = "'".$this->data['brand_name']."'";
					$user_filter['size_name'] = "'".$this->data['size_name']."'";
					$user_filter['only_sell_items'] = "'".$this->data['only_sell_items']."'";
					$user_filter['only_new_brand'] = "'".$this->data['only_new_brand']."'";
					$user_filter['highest_rated'] = "'".$this->data['highest_rated']."'";
					$user_filter['only_following'] = "'".$this->data['only_following']."'";
					$this->UserFilter->updateAll($user_filter,array('UserFilter.user_id'=>$user_id));
					$response['type']="success";
					$response['message']="Filter updated successfully";
				}
				else  // If filter setting is not there for thaty user then save filter setting for that user.
				{

					$this->User->recursive = -1;
					$is_valid_user = $this->User->find('first',array('conditions'=>array('User.id'=>$user_id))); // for finding user id is valid or not 
					if(!empty($is_valid_user))
					{
						$user_filter = $this->data;
						$user_filter['user_id'] = $user_id;
						$this->UserFilter->save($user_filter);
						$response['type']="success";
						$response['message']="Filter save successfully";
					}
					else
					{
						$response['type']="error";
						$response['message']="User not found.";
					}
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="invalid parameters";
			}
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function admin_index()
	{
		$this->User->bindModel(array("hasOne"=>array("UserTotal")));
		$this->User->unbindModel(array("hasMany"=>array("Drobe","Rate","Follower","StreamCategory"),"hasOne"=>array("UserSetting")));
		$this->helpers[]='Time';
		$conditions=array();
		$this->User->recursive=0;
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
			$conditions['OR']['User.email LIKE']="%".$search;
		}
		$this->paginate = array(
				'limit' => 15,
				'conditions'=>$conditions,
				'order'=>"User.id DESC"
		);
		$data = $this->paginate('User');
		$this->set(compact('data'));
	}
	function admin_list()
	{
		$conditions=array();
		if(isset($this->params->query['search']) && $this->params->query['search']!="")
		{
			$search=$this->params->query['search']."%";
			$conditions['OR']['User.id']=$search;
			$conditions['OR']['User.first_name LIKE']="%".$search;
			$conditions['OR']['User.last_name LIKE']="%".$search;
			$conditions['OR']['User.username LIKE']="@"."%".$search;
			$conditions['OR']['User.email LIKE']="%".$search;
		}
		$this->User->unbindModel(array("hasOne"=>array("UserSetting"),"hasMany"=>array("Drobe","Rate","Follower","StreamCategory")));
		$this->User->bindModel(array("hasOne"=>array("UserTotal")));
		$this->paginate = array(
				'limit' => 15,
				'fields'=>array('User.id','User.first_name','User.username','User.last_name','UserTotal.*'),
				'conditions'=>$conditions,
				'order'=>array('User.id'=>'desc')
		);
		$data = $this->paginate('User');
		$this->set(compact('data'));
	}
	
	// Following user drobe with highest rated drobe list
	function following_getlist_highest_rated($page=1,$category_id=null)
	{	
		
		/* Find followeing user of currently logged in user and which drobe is hieghest rated */
		$user_data=$this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		$following=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$user_data['User']['id'],"Follower.request"=>"accepted"),"order"=>"Follower.created_on ASC"));
		/*
		 * Filtering drobes for loggedin user
		*/
		$conditions=array(
				//'Drobe.user_id !='=>$this->Auth->user('id'),
				'Drobe.rate_status'=>'open',
				'User.id >'=>0,
				'User.id'=>$following
		);
		//$conditions['DrobeSetting.ask_public']=1;
		if($this->Auth->user('id')>0)
		{
			
				
			$this->loadModel('UserFilter');
				
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
				
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
				
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
				
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
				
				
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."' OR User.gender is null)))";
			}
				
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
				
				
				
			
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}		
		}
	
	
	
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
			
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
		/*	$conditions['OR']['User.email LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.first_name," ",User.last_name) LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.last_name," ",User.first_name) LIKE ']=$keyword."%";
			$conditions['OR']['Category.category_name LIKE ']="%".$keyword."%"; */
		}
	
		/*
		 * End of filtering for logged in users
		*/
			
		
		$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		
	
		//$conditions=array('Drobe.user_id !='=>$this->Auth->user('id'),'Drobe.rate_status'=>'open');
		if($category_id>0)
		{
			$conditions['Drobe.category_id']=$category_id;
		}
	
		
		$fields =array('Drobe.*','SellDrobe.sell_price','SellDrobe.original_sell_price','SellDrobe.size_name');
		
		if(strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
		
		
		$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array('hasOne'=>array("DrobeSetting"),
				'hasMany'=> array("Rate","Flag","Favourite")));
		$data=$this->Drobe->find('all', array(
				'fields'=>$fields,
				'limit' => 24*$page,
				'offset' =>0,
				'order'=>$order,
				'conditions'=>$conditions,
				'group'=>'Drobe.id',
		));
		$this->set(compact('data'));
		/* This user unique_id set which is used in following_getlist_highest_rated.ctp file*/
		$this->set('user_unique_id',$this->Auth->user('unique_id'));
	}
	
	// Following user list of recently uploaded drobe
	function following_getlist($sort_by="recent",$latest_drobe="0",$category_id=null)
	{
		
		/* Find following user of currently logged in user. */	
		$user_data=$this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		$following=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$user_data['User']['id'],"Follower.request"=>"accepted"),"order"=>"Follower.created_on ASC")); 
		/*
		 * Filtering drobes for loggedin user
		*/
		$conditions=array(
				//'Drobe.user_id !='=>$this->Auth->user('id'),
				'Drobe.deleted'=>0,
				'Drobe.rate_status'=>'open',
				'User.id >'=>0,
				'User.id'=>$following
	
		);
		//$conditions['DrobeSetting.ask_public']=1;
		if($this->Auth->user('id')>0)
		{
			$this->loadModel('UserFilter');
			
			//load only user selected category from UserFilter table
			$userFilter = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
			if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
			{
				$conditions['Drobe.category_id']= explode(",",$userFilter['category_ids']);  //explode category_ids by comma because in table id's are stored with comma separated.
			}
			
			if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.size_name']= explode("||",$userFilter['size_name']); //explode size_name by || because in table id's are stored with pipe separated.
			}
			
			if($userFilter['brand_name']!='{ALL}' && $userFilter['brand_name']!="")
			{
				$only_sell_drobe=true;
				$conditions['SellDrobe.sell_brand_name']= explode("||",$userFilter['brand_name']); //explode brand_name by || because in table name's are stored with pipe separated.
			}
			
			if($userFilter['price_range']!='{ALL}' && $userFilter['price_range']!="")
			{
				$only_sell_drobe=true;
				$conditions['AND']= $this->_parse_price_range($userFilter['price_range']); //explode brand_name by || because in table name's are stored with pipe separated.
			}  
			
			if(in_array($userFilter['gender'],array("male","female")))
			{
				//Check for gender filter from UserFilter table
				$conditions[]="((Drobe.post_type='sell' AND SellDrobe.sell_gender='".$userFilter['gender']."') 
				OR (Drobe.post_type='post' AND (User.gender='".$userFilter['gender']."' OR User.gender is null)))";
			}
			
			
			
			//Check Brand New or User or both
			if($userFilter['only_new_brand']=="new") 
			{
				$conditions['OR']['SellDrobe.is_brand_new']="new";
				$only_sell_drobe = true;
			}
			if($userFilter['only_new_brand']=="used")
			{
				$conditions['OR']['SellDrobe.is_brand_new']="used";
				$only_sell_drobe = true;
			}
			
			
			if($only_sell_drobe)
			{
				$conditions['Drobe.post_type']= "sell";
			}
		}
		
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword= preg_split("/[\s,]+/",$this->params->params['named']['search']);
			foreach($keyword as $key)
			{
				//$keyword=$this->params->params['named']['search'];
				$key = ltrim($key,'#');
				
				/* Old condition changed on 27/11/2013 */
				//$conditions['AND'][] = array('Drobe.comment LIKE' => "%".$key."%");
				$conditions['AND'][] = array('OR'=>array('OR'=>array('Drobe.comment LIKE' => $key."%",'User.username LIKE' => "@".$key."%"), 'Drobe.comment LIKE' => "% ".$key."%"));
			}
			
			
			/*$conditions['OR']['User.email LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.first_name," ",User.last_name) LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.last_name," ",User.first_name) LIKE ']=$keyword."%";
			$conditions['OR']['Category.category_name LIKE ']="%".$keyword."%"; */
		}
	/*	if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword=$this->params->params['named']['search'];
			$conditions['OR']=array();
			$conditions['OR']['Drobe.comment LIKE ']="%".$keyword."%";
			/* $conditions['OR']['User.first_name LIKE ']=$keyword."%";
				$conditions['OR']['User.last_name LIKE ']=$keyword."%";
			$conditions['OR']['User.email LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.first_name," ",User.last_name) LIKE ']=$keyword."%";
			$conditions['OR']['concat(User.last_name," ",User.first_name) LIKE ']=$keyword."%";
			$conditions['OR']['Category.category_name LIKE ']="%".$keyword."%"; */
		//}
	
		/*
		 * End of filtering for logged in users
		*/
			
		$order="";
		$joins=array();
		if($sort_by=="recent")
		{
			// fixing issue of drobe repeat on next page;
			if($latest_drobe != "0")
			{
				$this->loadModel('Drobe');
				$latest_drobe_id=$this->Drobe->field('id',array('Drobe.unique_id'=>$latest_drobe));
				if($latest_drobe_id>0) $conditions['Drobe.id <= ']=$latest_drobe_id;
			}
				
			if($this->Auth->user('id')>0)
			{
				$joins=array(
						array(
								'table' => 'rates',
								'alias' => 'Rate',
								'type' => 'LEFT',
								'conditions' => array(
										'Rate.drobe_id = Drobe.id',
										'Rate.user_id' => $this->Auth->user('id')
								)
						)
				);
				//$order="Drobe.uploaded_on DESC";
				$order="Rate.user_id ASC,Drobe.uploaded_on DESC";
			}
			else $order="Drobe.uploaded_on DESC";
		}
		if($sort_by=="rated")
		{
			//$order="Drobe.total_rate DESC";
			//$order="Drobe.total_in/Drobe.total_rate DESC, Drobe.views DESC";
			//$order="Drobe.rate_index DESC, Drobe.views DESC";
			$order = $this->drobe_season_order_query()." Drobe.rate_index DESC, Drobe.views DESC";
		}
		if($sort_by=="viewed") $order="Drobe.views DESC";
	
		if($sort_by=="featured")
		{
			$order="Drobe.featured DESC, Drobe.order ASC,Drobe.id DESC";
			$conditions[]=array("OR"=>array("Drobe.featured"=>1,"AND"=>array("Drobe.post_type"=>"sell","SellDrobe.status"=>"approved")));
		}
		//$conditions=array('Drobe.user_id !='=>$this->Auth->user('id'),'Drobe.rate_status'=>'open');
		if($category_id>0)
		{
			$conditions['Drobe.category_id']=$category_id;
		}
		
		$fields =array('Drobe.*','SellDrobe.sell_price','SellDrobe.original_sell_price','SellDrobe.size_name');
		
		if($sort_by=="recent" && strpos($keyword[0],'#')!==false)
		{
			$fields[]="(CASE when INSTR(Drobe.comment, '".$keyword[0]."') then 1 else 0 END) AS has_tag";
			$order="has_tag DESC , ".$order;
		}
	
		$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array('hasOne'=>array("DrobeSetting"),
				'hasMany'=> array("Rate","Flag","Favourite")));
		$this->paginate = array(
				'fields'=>$fields,
				'limit' => 24,
				'order'=>$order,
				'conditions'=>$conditions,
				'group'=>'Drobe.id',
				'joins'=>$joins
		);
		
		$data = $this->paginate('Drobe');
		
		$this->set(compact('data'));
		/* This user unique_id set which is used in following_getlist.ctp file*/
		$this->set('user_unique_id',$this->Auth->user('unique_id'));
	}
	
	//This following method for left panel following button
	function following_panel($user_id=null)
	{
		$this->loadModel('UserFilter');
		$userFilter  = $this->UserFilter->_loadUsersFilter($this->Auth->user('id'));
		$this->set('userFilter',$userFilter);
		
		if(Cache::read('drobe_category')==null)
		{
			$this->loadModel('Category');
			$this->Category->_updateCategoryCache();
		}
		
		if(Cache::read('size_list')==null)
		{
			$this->loadModel('Size');
			$this->Size->_updateSizeCache();
		}
		
		if(Cache::read('brand_list')==null)
		{
			$this->loadModel('Brand');
			$this->Brand->_updateBrandCache();
		}
		
		if($user_id==null)
		{
			$user_id=$this->Auth->user('unique_id');
			$this->set('user',$this->Auth->user());
			$this->set('is_me',true);
			$this->set('title_for_layout',"My Followings");
		}
		else
		{
			$user_data=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$user_id)));
			if($user_data)
			{
				$this->set('user',$user_data['User']);
				$this->set('is_me',$user_data['User']['id']==$this->Auth->user('id'));
				$this->set('title_for_layout', $user_data['User']['id']==$this->Auth->user('id')? "My Followers": $user_data['User']['username']." - Followers");
			}
			else
			{
				$this->set('invalid_user',true);
				throw new NotFoundException("Not Found");
			}
		}
		
	}
	
	
	function following($user_id=null)
	{
		if($user_id==null)
		{
			$user_id=$this->Auth->user('unique_id');
			$this->set('user',$this->Auth->user());
			$this->set('is_me',true);
			$this->set('title_for_layout',"My Followings");
		}
		else
		{
			$user_data=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$user_id)));
			if($user_data)
			{
				$this->set('user',$user_data['User']);
				$this->set('is_me',$user_data['User']['id']==$this->Auth->user('id'));
				$this->set('title_for_layout', $user_data['User']['id']==$this->Auth->user('id')? "My Followers": $user_data['User']['username']." - Followers");
			}
			else
			{
				$this->set('invalid_user',true);
				throw new NotFoundException("Not Found");
			}
		}
	}
	
	function following_list($user_id=null)
	{
		
		
		//if Search Parameter exist then search for parameter name according to given search parameter.
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword=$this->params->params['named']['search'];
			$conditions['OR']=array();
			$conditions['OR']['User.first_name LIKE ']=$keyword."%";
			$conditions['OR']['User.last_name LIKE ']=$keyword."%";
			$conditions['OR']['User.username LIKE ']="@".$keyword."%";
		}	
		
		$user_data=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$user_id)));
		$following=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$user_data['User']['id'],"Follower.request"=>"accepted"),"order"=>"Follower.created_on ASC"));
		$this->User->unBindModel(array("hasMany" => array('Rate','Drobe','StreamCategory','Follower')));
		$this->User->bindModel(array("hasMany" => array(
							'Drobe'=>array(
								'fields'=>array('Drobe.unique_id','Drobe.uploaded_on','Drobe.is_highest_rated','Drobe.file_name','Drobe.comment'),
								'order'=>array('Drobe.total_rate ASC','Drobe.uploaded_on DESC'),
								'conditions'=>array('Drobe.rate_status'=>'open','Drobe.deleted'=>0),
								'limit'=>6
		))));
		$this->loadModel('NewDrobe');
		$db = $this->NewDrobe->getDataSource();
		$subQuery = $db->buildStatement(
				array(
						'fields'     => array('NewDrobe.user_id as user_id','count(NewDrobe.drobe_id) as total','NewDrobe.created_on as uploaded_on'),
						'table'      => $db->fullTableName($this->NewDrobe),
						'alias'      => 'NewDrobe',
						'conditions' => array("NewDrobe.follower_id"=>$user_data['User']['id']),
						'group'      => "NewDrobe.user_id",
						'order'		=> "NewDrobe.created_on DESC"
				),
				$this->NewDrobe
		);
		//$this->User->recursive=1;
		$this->paginate = array(
				'limit' => 5,
				'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.photo','NewDrobe.total'),
				'conditions'=>array('User.id'=>$following,$conditions),
				'joins'=>array(
						array(
						'table' => 'followers',
						'alias' => 'Follower',
						'type' => 'LEFT',
						'conditions' => array(
								'User.id = Follower.user_id',
								'Follower.follower_id = '.$user_data['User']['id']
						)),
						array(
								'table' => "(".$subQuery.")",
								'alias' => 'NewDrobe',
								'type' => 'LEFT',
								'conditions' => array(
										'User.id = NewDrobe.user_id'
								))
				),
				'order'=>"NewDrobe.uploaded_on DESC, User.first_name ASC"
		);
		$data = $this->paginate('User');
		
		//$this->set('new_drobes',$this->_getNewBadgeCounter(&$data,$user_data['User']['id']));
		$this->set(compact('data'));
	}
	
	
	/*
	 * getting list of followers or followings of a perticular user with paginations of 10 users
	* Compatibility ver 1.2 or leter
	*/
	
	function friends_v12($type="followings",$user_id=null)
	{
		
		if($user_id==null && isset($this->data['user_id']) && $this->data['user_id'])
		{
			$user_id=$this->data['user_id'];
		}
		
	
		if($user_id!=null)
		{
			$logged_user_data=array();
			
			if(isset($this->data['search']) && $this->data['search']!="")
			{
				$search="%".$this->data['search'];
				$conditions=array();
				$conditions['OR']['User.first_name LIKE']=$search."%";
				$conditions['OR']['User.last_name LIKE']=$search."%";
				$conditions['OR']['User.username LIKE']="@".$search."%";
				$conditions['OR']['User.email LIKE']=$search."%";
			}
	
			if($type=="followings")
			{
				
				$joins = array(
								array(
								'table'=>'everdrobe_users',
								'alias'=>'User',
								'type'=>'left',
								'conditions'=> array(
										'User.id = Follower.user_id'
										)
						    )
						);
				 
				$user_ids = $this->User->Follower->find('list', array('fields'=>array('Follower.user_id'), 
					     'joins' => $joins,	
						'conditions'=>array("Follower.follower_id"=>$user_id,"Follower.request"=>"accepted")));
				//$user_ids=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$user_id,"Follower.request"=>"accepted")));
				
				
				$join_conditions=array(
						'User.id = Follower.user_id',
						'Follower.follower_id = '.$user_id
				);
	
				$this->loadModel('NewDrobe');
				$db = $this->NewDrobe->getDataSource();
	
				$subQuery = $db->buildStatement(
						array(
								'fields'     => array('NewDrobe.user_id as user_id','count(NewDrobe.drobe_id) as total','NewDrobe.created_on as uploaded_on'),
								'table'      => $db->fullTableName($this->NewDrobe),
								'alias'      => 'NewDrobe',
								'conditions' => array("NewDrobe.follower_id"=>$user_id),
								'group'      => "NewDrobe.user_id",
								'order'		=> "NewDrobe.created_on DESC"
						),
						$this->NewDrobe
				);
				$this->User->recursive=-1;
	
				
				$this->paginate=array(
						'limit'=>10,
						'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.photo','User.star_user','NewDrobe.total'),
						'conditions'=>array('User.id'=>$user_ids,$conditions),
						'joins'=>array(
								array(
										'table' => 'followers',
										'alias' => 'Follower',
										'type' => 'LEFT',
										'conditions' => $join_conditions
								),
								array(
										'table' => "(".$subQuery.")",
										'alias' => 'NewDrobe',
										'type' => 'LEFT',
										'conditions' => array(
												'User.id = NewDrobe.user_id'
										))
						),
						'order'=>"NewDrobe.uploaded_on DESC, User.first_name ASC"
				);
				
				// load loggedin user's following
				$user_ids_current=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 
						'conditions'=>array("Follower.follower_id"=>$this->data['user_id'],"Follower.request"=>"accepted")));
	
			}
			else if($type=="followers")
			{
				
				$joins = array(
							array(
									'table'=>'everdrobe_users',
									'alias'=>'User',
									'type'=>'left',
									'conditions'=> array(
											'User.id = Follower.follower_id'
									)
								)
							);
				
				$user_ids=$this->User->Follower->find('list',array('fields'=>array('Follower.follower_id'), 
						'joins'=>$joins,
						'conditions'=>array("Follower.user_id"=>$user_id,"Follower.request"=>"accepted",$conditions)));
				
				
				$join_conditions=array(
						'User.id = Follower.follower_id',
						'Follower.user_id = '.$user_id
				);
	
				$this->User->recursive=-1;
				
				$this->paginate=array(
					'limit'=>10,
					'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.photo','User.star_user'),
					'conditions'=>array('User.id'=>$user_ids),
					'joins'=>array(
						array(
							'table' => 'followers',
							'alias' => 'Follower',
							'type' => 'LEFT',
							'conditions' => $join_conditions
						)
					),
					'order'=>"Follower.created_on DESC"
				);
				// load loggedin user's following
				$user_ids_current=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$this->data['user_id'],"Follower.request"=>"accepted")));
			}
			
			$userData=$this->paginate('User');
			
			
			if($userData)
			{
				$response['type']="success";
				$response['users']=array();
	
				$friend_ids=array();
				foreach($userData as $user)
				{
					$friend_ids[]=$user['User']['id'];
				}
	
				//$new_drobes=$this->_getNewBadgeCounter($friend_ids, $user_id);
				$this->loadModel('Drobe');
				$this->Drobe->recursive=1;
	
				foreach($userData as $user)
				{
					if($user['User']['photo']!="")
					{
						$user['User']['photo']=Router::url("/profile_images/thumb/".$user['User']['photo'],true);
					}
					else
					{
						$user['User']['photo']=Router::url("/images/default.jpg",true);
					}
					// fetching latest uploaded 4 drobes;
					$this->Drobe->bindModel(array('hasOne' => array("SellDrobe")));
					$user_drobes=$this->Drobe->find('all', array(
							'fields'=>array('SellDrobe.sell_price','Drobe.id','Drobe.is_highest_rated','Drobe.file_name'),
							'limit' => 4,
							'order'=>array("Drobe.total_rate ASC","Drobe.uploaded_on DESC"),
							'conditions'=>array('Drobe.rate_status'=>'open','Drobe.deleted'=>0, 'Drobe.user_id'=>$user['User']['id']))
					);
					
					
					
					$user['User']['drobes']=array();
					foreach($user_drobes as $drobe)
					{
						$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
						$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
						unset($drobe['Drobe']['file_name']);
						
						if($drobe['SellDrobe']['sell_price']!=null)
							$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
						
						$user['User']['drobes'][]=$drobe['Drobe'];
					}
					unset($user_drobes);
					
					//compare user with loggedin user's following
					if(in_array($user['User']['id'],$user_ids_current))
					{
						$user['User']['is_my_follower']="1";
					}
					else
					{
						$user['User']['is_my_follower']="0";
					}
						
					// new drobes counter uploaded by user
					$user['User']['new_drobes'] = "".((isset($user['NewDrobe']['total']) && $user['NewDrobe']['total'] > 0) ? $user['NewDrobe']['total'] : 0);
					$response['users'][]=$user['User'];
				}
			}
			else
			{
				$response['type']="success";
				$response['users']=array();
			}
	
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
	
	
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
	}
	
	function friends_v14($type="followings",$user_id=null)
	{
		if($user_id==null && isset($this->data['user_id']) && $this->data['user_id'])
		{
			$user_id=$this->data['user_id'];
		}
		if($user_id!=null)
		{
			if($type=="followings")
			{
				$user_ids=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$user_id,"Follower.request"=>"accepted")));
				$join_conditions=array(
						'User.id = Follower.user_id',
						'Follower.follower_id = '.$user_id
				);
				
				$this->loadModel('NewDrobe');
				$db = $this->NewDrobe->getDataSource();
				$subQuery = $db->buildStatement(
						array(
								'fields'     => array('NewDrobe.user_id as user_id','count(NewDrobe.drobe_id) as total','NewDrobe.created_on as uploaded_on'),
								'table'      => $db->fullTableName($this->NewDrobe),
								'alias'      => 'NewDrobe',
								'conditions' => array("NewDrobe.follower_id"=>$user_id),
								'group'      => "NewDrobe.user_id",
								'order'		=> "NewDrobe.created_on DESC"
						),
						$this->NewDrobe
				);
				
				
				$this->User->recursive=-1;
				$this->paginate=array(
						'limit'=>10,
						'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.photo','User.star_user','NewDrobe.total'),
						'conditions'=>array('User.id'=>$user_ids),
						'joins'=>array(
								array(
										'table' => 'followers',
										'alias' => 'Follower',
										'type' => 'LEFT',
										'conditions' => $join_conditions
								),
								array(
										'table' => "(".$subQuery.")",
										'alias' => 'NewDrobe',
										'type' => 'LEFT',
										'conditions' => array(
												'User.id = NewDrobe.user_id'
										))
						),
						'order'=>"NewDrobe.uploaded_on DESC, User.first_name ASC"
				);
				$userData=$this->paginate('User');
				$user_ids_current=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$this->data['user_id'],"Follower.request"=>"accepted")));
			}
			else if($type=="followers")
			{
				$user_ids=$this->User->Follower->find('list',array('fields'=>array('Follower.follower_id'), 'conditions'=>array("Follower.user_id"=>$user_id,"Follower.request"=>"accepted")));
				$join_conditions=array(
						'User.id = Follower.follower_id',
						'Follower.user_id = '.$user_id
				);
				
				$this->loadModel('NewDrobe');
				$db = $this->NewDrobe->getDataSource();
				$subQuery = $db->buildStatement(
						array(
								'fields'     => array('NewDrobe.user_id as user_id','count(NewDrobe.drobe_id) as total','NewDrobe.created_on as uploaded_on'),
								'table'      => $db->fullTableName($this->NewDrobe),
								'alias'      => 'NewDrobe',
								'conditions' => array("NewDrobe.follower_id"=>$user_id),
								'group'      => "NewDrobe.user_id",
								'order'		=> "NewDrobe.created_on DESC"
						),
						$this->NewDrobe
				);

				$this->User->recursive=-1;
				$this->paginate=array(
						'limit'=>10,
						'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.photo','User.star_user','NewDrobe.total'),
						'conditions'=>array('User.id'=>$user_ids),
						'joins'=>array(
								array(
										'table' => 'followers',
										'alias' => 'Follower',
										'type' => 'LEFT',
										'conditions' => $join_conditions
								),
								array(
										'table' => "(".$subQuery.")",
										'alias' => 'NewDrobe',
										'type' => 'LEFT',
										'conditions' => array(
												'User.id = NewDrobe.user_id'
										))
						),
						'order'=>"NewDrobe.uploaded_on DESC, Follower.created_on DESC"
				);
				$userData=$this->paginate('User');
				$user_ids_current=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$this->data['user_id'],"Follower.request"=>"accepted")));
			}
			if($userData)
			{
				$response['type']="success";
				$response['users']=array();
				foreach($userData as $user){
					
					if(in_array($user['User']['id'],$user_ids_current))
					{
						$user['User']['is_my_follower']="1";
					}
					else
					{
						$user['User']['is_my_follower']="0";
					}
					
					if($user['User']['photo']!='')
					{
						$user['User']['photo']=Router::url("/profile_images/thumb/".$user['User']['photo'],true);
					}
					else
					{
						$user['User']['photo']=Router::url("/images/default.jpg",true);
					}
					
					if($user['NewDrobe']['total']>0)
					{
						$user['User']['new_drobes'] = "".$user['NewDrobe']['total'];
					}
					else
					{
						$user['User']['new_drobes'] = "0";
					}
					$response['users'][]=$user['User'];
					
				}
			}
			else
			{
				$response['type']="success";
				$response['users']=array();
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	/*
	 * getting list of followers or followings of a perticular user
	 * Compatibility ver 1.1 or leter
	*/
	
	function friends($type="followings",$user_id=null)
	{
		if($user_id==null && isset($this->data['user_id']) && $this->data['user_id'])
		{
			$user_id=$this->data['user_id'];
		}
	
		if($user_id!=null)
		{
			$logged_user_data=array();
				
			if($type=="followings")
			{
				$user_ids=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$user_id,"Follower.request"=>"accepted")));
				$join_conditions=array(
						'User.id = Follower.user_id',
						'Follower.follower_id = '.$user_id
				);
				
				$this->loadModel('NewDrobe');
				$db = $this->NewDrobe->getDataSource();
				
				$subQuery = $db->buildStatement(
						array(
								'fields'     => array('NewDrobe.user_id as user_id','count(NewDrobe.drobe_id) as total','NewDrobe.created_on as uploaded_on'),
								'table'      => $db->fullTableName($this->NewDrobe),
								'alias'      => 'NewDrobe',
								'conditions' => array("NewDrobe.follower_id"=>$user_id),
								'group'      => "NewDrobe.user_id",
								'order'		=> "NewDrobe.created_on DESC"
						),
						$this->NewDrobe
				);
				$this->User->recursive=-1;
				
				$userData=$this->User->find('all',array(
						'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.star_user','User.photo','NewDrobe.total'),
						'conditions'=>array('User.id'=>$user_ids),
						'joins'=>array(
								array(
										'table' => 'followers',
										'alias' => 'Follower',
										'type' => 'LEFT',
										'conditions' => $join_conditions
								),
								array(
										'table' => "(".$subQuery.")",
										'alias' => 'NewDrobe',
										'type' => 'LEFT',
										'conditions' => array(
												'User.id = NewDrobe.user_id'
										))
								
						),
						'order'=>"NewDrobe.uploaded_on DESC, User.first_name ASC"
				));
				
			}
			else if($type=="followers")
			{
				$user_ids=$this->User->Follower->find('list',array('fields'=>array('Follower.follower_id'), 'conditions'=>array("Follower.user_id"=>$user_id,"Follower.request"=>"accepted")));
				$join_conditions=array(
						'User.id = Follower.follower_id',
						'Follower.user_id = '.$user_id
				);
				
				$this->User->recursive=-1;
				
				$userData=$this->User->find('all',array(
						'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.star_user','User.photo'),
						'conditions'=>array('User.id'=>$user_ids),
						'joins'=>array(
								array(
										'table' => 'followers',
										'alias' => 'Follower',
										'type' => 'LEFT',
										'conditions' => $join_conditions
								)
						),
						'order'=>"Follower.created_on DESC"
				));
			}
	
			
			if($userData)
			{
				$response['type']="success";
				$response['users']=array();
				
				$friend_ids=array();
				foreach($userData as $user)
				{
					$friend_ids[]=$user['User']['id'];
				}
				
				//$new_drobes=$this->_getNewBadgeCounter($friend_ids, $user_id);
				
				
				$this->loadModel('Drobe');
				$this->Drobe->recursive=1;
				
				foreach($userData as $user)
				{
					if($user['User']['photo']!="")
					{
						$user['User']['photo']=Router::url("/profile_images/thumb/".$user['User']['photo'],true);
					}
					// fetching latest uploaded 4 drobes;
					$this->Drobe->bindModel(array('hasOne' => array("SellDrobe")));
					$user_drobes=$this->Drobe->find('all', array(
							'fields'=>array('SellDrobe.sell_price','Drobe.id','Drobe.is_highest_rated','Drobe.file_name'),
							'limit' => 4,
							'order'=>"Drobe.uploaded_on DESC",
							'conditions'=>array('Drobe.rate_status'=>'open','Drobe.deleted'=>0, 'Drobe.user_id'=>$user['User']['id']))
					);
					$user['User']['drobes']=array();
					foreach($user_drobes as $drobe)
					{
						$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
						$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
						unset($drobe['Drobe']['file_name']);
						if($drobe['SellDrobe']['sell_price']!=null)
							$drobe['Drobe']['sell_price']=$drobe['SellDrobe']['sell_price'];
						$user['User']['drobes'][]=$drobe['Drobe'];
					}
					unset($user_drobes);
					
					// new drobes counter uploaded by user
					$user['User']['new_drobes'] = "".((isset($user['NewDrobe']['total']) && $user['NewDrobe']['total'] > 0) ? $user['NewDrobe']['total'] : 0);
					$response['users'][]=$user['User'];
				}
			}
			else
			{
				$response['type']="success";
				$response['users']=array();
			}
				
		}
		else
		{
			$response['type']="error";
			$response['message']="please provide user id with url";
		}
	
	
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	
	}
	
	function search_user_by_drobe($action="recent")
	{
		if(isset($this->data['current_user']) && $this->data['current_user']>0 && isset($this->data['search']) && $this->data['search']!="")
		{
			$conditions_drobe=array();
			
			// load only user selected category stream
			$this->loadModel('StreamCategory');
			$streamCategory=$this->StreamCategory->_loadUsersCategory($this->data['current_user']);
			if($streamCategory['categories']!="ALL" && is_array($streamCategory['categories']))
			{
				$conditions_drobe['Drobe.category_id']=$streamCategory['categories'];
			}
			
			if($action=='recent')
			{
				//load most recent drobes
				$order='Drobe.uploaded_on DESC';
			}
			elseif($action=='highest_rated')
			{
				//load highest rated drobes 
				$order="Drobe.rate_index DESC, Drobe.views DESC";
			}
			elseif($action=='shop')
			{
				//load featured drobes conditions + order
				$order="Drobe.order ASC";
				$conditions_drobe['Drobe.featured']=1;
			}
			
			$conditions_drobe['Drobe.rate_status']='open';
			$conditions_drobe[0]='Drobe.user_id = `User`.`id`';

			//codition add for gender
			if(isset($this->data['gender']) && $this->data['gender']!="" && $this->data['gender']!="both")
			{
				if($this->data['gender']=="female") $conditions_drobe['OR']['User.gender']="female";
				if($this->data['gender']=="male") $conditions_drobe['OR']['User.gender']="male";
			}
			
			//codition for user's first and last name search
			$conditions=array('OR'=>array(),'Drobe.id >'=>0);
			$search=$this->data['search']."%";
			$conditions['OR']['User.first_name LIKE']=$search;
			$conditions['OR']['User.last_name LIKE']=$search;
			$conditions['OR']['User.username LIKE']="@".$search;
			
			$this->User->recursive=-1;
			
			//load current logged in user's followers
			$user_ids_current=$this->User->Follower->find('list',array('fields'=>array('Follower.user_id'), 'conditions'=>array("Follower.follower_id"=>$this->data['current_user'],"Follower.request"=>"accepted")));
			
			$this->paginate=array(
					'limit'=>10,
					'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.photo','User.star_user'),
					'conditions'=>$conditions,
					'joins'=>array(
							array(
								'table' => 'drobes',
								'alias' => 'Drobe',
								'type' => 'LEFT',
								'conditions' =>$conditions_drobe
							)),
					'group' => 'User.id',
				);
			
			$userData=$this->paginate('User');
			
			if(!empty($userData))
			{
				$this->loadModel('Drobe');
				$this->Drobe->recursive=-1;
			
				foreach($userData as $user)
				{
					//photo url
					if($user['User']['photo']!="")
					{
						$user['User']['photo']=Router::url("/profile_images/thumb/".$user['User']['photo'],true);
					}
					else
					{
						$user['User']['photo']=Router::url("/images/default.jpg",true);
					}
					
					//compare user with loggedin user's following
					if(in_array($user['User']['id'],$user_ids_current))
					{
						$user['User']['is_my_follower']="1";
					}
					else
					{
						$user['User']['is_my_follower']="0";
					}

					//load user's drobe condition
					$conditions_drobe['Drobe.user_id']=$user['User']['id'];
					//unset varible which defile above for joining 
					unset($conditions_drobe[0]);
					
					//load drobes
					$userDrobes = $this->Drobe->find('all',array(
							'fields'=>array('Drobe.id','Drobe.is_highest_rated','Drobe.file_name'),
							'conditions'=>$conditions_drobe,
							'limit'=>4,
							'order'=>$order
					));

					foreach($userDrobes as $drobe)
					{
						$drobe['Drobe']['image_url']=Router::url("/drobe_images/iphone/".$drobe['Drobe']['file_name'],true);
						$drobe['Drobe']['thumb_url']=Router::url("/drobe_images/iphone/thumb/".$drobe['Drobe']['file_name'],true);
						unset($drobe['Drobe']['file_name']);
						
						$user['User']['drobes'][]=$drobe['Drobe'];
					}
					
					//if user has no drobe then user will not add in list	
					if(!empty($userDrobes))
					{
						$response['users'][]=$user['User'];
					}
				}
				$response['type']="success";
			}
			else
			{
				$response['type']="success";
				$response['users']=array();
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid Parameters";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function follower($user_id=null)
	{
		if($user_id==null)
		{
			$user_id=$this->Auth->user('unique_id');
			$this->set('user',$this->Auth->user());
			$this->set('is_me',true);
			$this->set('title_for_layout',"My Followers");
		}
		else
		{
			$user_data=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$user_id)));
			if($user_data)
			{
				$this->set('user',$user_data['User']);
				$this->set('is_me',$user_data['User']['id']==$this->Auth->user('id'));
				$this->set('title_for_layout', $user_data['User']['id']==$this->Auth->user('id')? "My Followers": $user_data['User']['username']." - Followers");
			}
			else
			{
				$this->set('invalid_user',true);
				throw new NotFoundException("Not Found");
			}
		}
		
	}
	
	/*
	 * Webservice for update device token when user is loggedin from iPhone App
	 * 
	 */
	function update_token($user_id=null)
	{
	
		$response=array();
		if($this->data)
		{
			$this->User->id=$user_id;
			$device_token=$this->data['apple_device_token'];
				
			$device_token=str_replace("<","",$device_token);
			$device_token=str_replace(">","",$device_token);
			
			$device_type=(isset($this->data['device_type']) && $this->data['device_type']!="")? $this->data['device_type']: "ios";
			
			// removing allocated same device token earlier to another user 
			$this->User->UserSetting->updateAll(array('UserSetting.apple_device_token'=>"''"),array('UserSetting.apple_device_token'=>$device_token,'UserSetting.device_type'=>$device_type));
			
			$device_token="'".trim(str_replace(" ","",$device_token))."'";
			$device_type="'".$device_type."'";
			
			if($this->User->UserSetting->updateAll(array('UserSetting.apple_device_token'=>$device_token,'UserSetting.device_type'=>$device_type),array('UserSetting.user_id'=>$user_id)))
			{
				
				$device_token=trim(str_replace("'","",$device_token));
				$device_type=trim(str_replace("'","",$device_type));
				
				$this->track_user_activity(array(
						"user_id"=>$user_id,
						'activity'=> "Logged in with ".$device_type." Token: ".$device_token,
						"device_type"=>$device_type,
					));
				$response['type']="success";
				$response['message']="device token updated successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="error occured in update device token, please try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid params";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	/*
	 * Webservice for remove device token when user is loggedin from iPhone App
	 * here we had passed apple device token also to make sure for which device token need to remove for which user
	*	also clear all the stream filters when logout
	*/
	
	function remove_token($user_id=null)
	{
	
		$response=array();
		if($this->data)
		{
			$this->User->id=$user_id;
			$device_token=$this->data['apple_device_token'];
	
			$device_token=str_replace("<","",$device_token);
			$device_token=str_replace(">","",$device_token);
			
			$device_type=(isset($this->data['device_type']) && $this->data['device_type']!="")? $this->data['device_type']: "ios";
				
			// removing allocated same device token earlier to another user
			if($this->User->UserSetting->updateAll(array('UserSetting.apple_device_token'=>"''","UserSetting.device_type"=>"''"),array('UserSetting.apple_device_token'=>$device_token,'UserSetting.device_type'=>$device_type,'UserSetting.user_id'=>$user_id)))
			{
					$this->track_user_activity(array(
							"user_id"=>$user_id,
							'activity'=> "Logged out from device ".$device_type." Token: ".$device_token,
							"device_type"=>$device_type,
					));
				
				$response['type']="success";
				$response['message']="device token removed successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="error occured in removed device token, please try again later";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid params";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	function reset_badge($user_id=null,$counter=0)
	{
		$response=array();
		$this->User->id=$user_id;
		if($this->User->UserSetting->updateAll(array('badge'=>$counter),array('UserSetting.user_id'=>$user_id)))
		{
			$response['type']="success";
			$response['message']="badge counter updated successfully";
		}
		else
		{
			$response['type']="error";
			$response['message']="error occured in update badge counter, please try again later";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function follower_list($user_id=null)
	{
		$conditions = array();
		//if Search Parameter exist then search for parameter name according to given search parameter.
		if(isset($this->params->params['named']['search']) && $this->params->params['named']['search']!="")
		{
			$keyword=$this->params->params['named']['search'];
			$conditions['OR']=array();
			$conditions['OR']['User.first_name LIKE ']=$keyword."%";
			$conditions['OR']['User.last_name LIKE ']=$keyword."%";
			$conditions['OR']['User.username LIKE ']="@".$keyword."%";
		}
		
		$user_data=$this->User->find('first',array('conditions'=>array('User.unique_id'=>$user_id)));
	
		$follower=$this->User->Follower->find('list',array('fields'=>array('Follower.follower_id'), 'conditions'=>array("Follower.user_id"=>$user_data['User']['id'],"Follower.request"=>"accepted")));
		$this->User->unBindModel(array("hasMany" => array('Rate','Drobe','StreamCategory','Follower')));
		$this->User->bindModel(array("hasMany" => array(
								'Drobe'=>array(
									'fields'=>array('Drobe.unique_id','Drobe.uploaded_on','Drobe.is_highest_rated','Drobe.file_name','Drobe.comment'),
									'order'=>array('Drobe.total_rate', 'Drobe.uploaded_on'),
									'conditions'=>array('Drobe.rate_status'=>'open','Drobe.deleted'=>0),
									'limit'=>4
		))));
		//$this->User->recursive=1;
		$this->paginate = array(
					'limit' => 5,
					'fields'=>array('User.id','User.unique_id','User.first_name','User.username','User.last_name','User.photo'),
					'conditions'=>array('User.id'=>$follower,$conditions)
		);
		
		$data = $this->paginate('User');
		$this->set(compact('data'));
	}
	/*
	 * UI for Search,Invite and Follow friends
	 */
	function search()
	{
		$fb_session = $this->facebook->getUser();
		
		if(!$fb_session)
		{
			$this->set("facebookUrl",$this->facebook->getLoginUrl(
					array(
							'scope' => 'email,user_about_me,friends_about_me,read_stream,publish_stream,offline_access', 
							'redirect_uri' => "http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"facebook_friends")),
							'display'=>"popup"
					)
			));
			$this->set('facebook_logged_in',false);
		}
		else
		{
			$this->set("facebookUrl","http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"facebook_friends","connected")));
			$this->set('facebook_logged_in',true);
		}
		
		App::import('Vendor', 'twitter/twitterAuth');
		App::import('Vendor', 'twitterOauth/twitteroauth');
		$tmpAuth=$this->Session->read('twitter_auth');
		
		$twitter_loggedin=false;
		if($tmpAuth)
		{
			$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$this->Auth->user('twitter_access_token'),$this->Auth->user('twitter_access_token_secret'));
			$tw_connected=null;
			if(isset($tmpAuth['user_id']))
			{
				$tw_connected=$this->twitterObj->get('https://api.twitter.com/1/users/show/'.$tmpAuth['user_id'].'.json');
			}
			if(isset($tw_connected->id))
			{
				$this->set("twitterLoginUrl","http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"twitter_friends","connected")));
				$this->set('twitter_logged_in',true);
				$twitter_loggedin=true;
			}
		}
		// if twitter not logged in, getting temporary credentials 
		if(!$twitter_loggedin)
		{
			$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"));
			$tmp_auth=$this->twitterObj->getRequestToken("http://".$_SERVER['HTTP_HOST'].$this->params['webroot'].Router::url(array("controller"=>"users","action"=>"twitter_friends")));
			$this->Session->write('twitter_auth',$tmp_auth);
			$this->set("twitterLoginUrl",$this->twitterObj->getAuthorizeURL($tmp_auth));
			$this->set('twitter_logged_in',false);
		}
		
		//When new user register then require its id for follower screen
		$this->set("user_id",$this->Auth->user('id'));
		
		$this->set('title_for_layout', "Search User" );
	}
	
	function search_user()
	{
		if(!empty($this->data) && $this->data['keyword']!="")
		{
			$conditions=array('OR'=>array(),'User.id !='=>$this->Auth->user('id'));
			$search=$this->data['keyword']."%";
			$conditions['OR']['User.first_name LIKE']=$search;
			$conditions['OR']['User.last_name LIKE']=$search;
			$conditions['OR']['User.username LIKE']="@".$search;
			$conditions['OR']['User.email LIKE']=$search;
			
			$this->User->recursive=-1;
			$data=$this->User->find('all',array(
					'fields'=>array('User.unique_id','User.facebook_id','User.first_name','User.username','User.star_user','User.last_name','User.photo','Follower.user_id'),
					'conditions'=>$conditions,
					'joins'=>array(
							array(
								'table' => 'followers',
									'alias' => 'Follower',
									'type' => 'LEFT',
									'conditions' => array(
											'User.id = Follower.user_id',
											'Follower.follower_id = '.$this->Auth->user('id')
									)
							))
			));
				
			
			
			$searched_users=array();
			foreach($data as $everdrobe_user)
			{
				$searched_users[$everdrobe_user['User']['unique_id']]=array(
						'name'=>$everdrobe_user['User']['username']!=""?$everdrobe_user['User']['username']:$everdrobe_user['User']['first_name']." ".$everdrobe_user['User']['last_name'],
						'photo'=>$everdrobe_user['User']['photo'],
						'star_user'=>$everdrobe_user['User']['star_user'],
						'following'=>($everdrobe_user['Follower']['user_id']>
								0)
				);
			}
		}
		else
		{
		}
		$this->layout='ajax';
		$this->set('searched_users',$searched_users);
	}
	
	function facebook_friends($connected="not_connected")
	{
		$this->facebook = new Facebook(array("appId"=>Configure::read("facebook.api_key"),"secret"=> Configure::read("facebook.app_secrete")));
		if($connected=="not_connected")
		{
			$myFriends=$this->facebook->api('/me/friends');
			$fb_friends=array();
			if(count($myFriends['data']))
			{
				foreach ($myFriends['data'] as $friend)
				{
					$fb_friends[$friend['id']]=array(
							'name'=>$friend['name'],
							'photo'=>"http://graph.facebook.com/".$friend['id']."/picture",
					);
				}
			}
			$this->User->recursive=-1;
			$existing_users=$this->User->find('all',array(
					'fields'=>array('User.unique_id','User.facebook_id','User.first_name','User.username','User.star_user','User.last_name','User.photo','Follower.user_id'),
					'conditions'=>array('User.facebook_id'=>array_keys($fb_friends),'User.id != '=>$this->Auth->user('id')),
					'joins'=>array(
							array(
									'table' => 'followers',
									'alias' => 'Follower',
									'type' => 'LEFT',
									'conditions' => array(
											'User.id = Follower.user_id',
											'Follower.follower_id = '.$this->Auth->user('id')
									)
							))
					));
			
			$fb_friends_exist=array();
			foreach($existing_users as $everdrobe_user)
			{
				$fb_friends_exist[$everdrobe_user['User']['unique_id']]=array(
					'name'=>$everdrobe_user['User']['username']!=""?$everdrobe_user['User']['username']:$everdrobe_user['User']['first_name']." ".$everdrobe_user['User']['last_name'],
					'photo'=>  $everdrobe_user['User']['photo'],
					'star_user'=>  $everdrobe_user['User']['star_user'],
					'following'=>($everdrobe_user['Follower']['user_id']>0) 
				);
				unset($fb_friends[$everdrobe_user['User']['facebook_id']]);
			}
		}
		else
		{
			$access_token=$this->Auth->user('facebook_access_token');
			$myFriends=$this->facebook->api('/me/friends','GET',$access_token);
		}
		$this->layout='ajax';
		$this->set('fb_friends',array('registered'=>$fb_friends_exist,'not_registered'=>$fb_friends));
		$this->set('is_ajax',$this->RequestHandler->isAjax());
		
	}
	
	function twitter_friends($connected="not_connected")
	{
		App::import('Vendor', 'twitter/twitterAuth');
		App::import('Vendor', 'twitterOauth/twitteroauth');
		//Connecting with Twitter with current account
		$userData=array();
		
		$tmpAuth=$this->Session->read('twitter_auth');
		$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$tmpAuth['oauth_token'],$tmpAuth['oauth_token_secret']);
		
		/* Request access tokens from twitter */
		if(isset($this->params->query['oauth_verifier']))
		{
			$tmpAuth = $this->twitterObj->getAccessToken($this->params->query['oauth_verifier']);
			$this->Session->write('twitter_auth',$tmpAuth);
		}
		
		
		if($connected=="not_connected")
		{
			$tw_followers=$this->twitterObj->get('http://api.twitter.com/1/statuses/followers/'.$tmpAuth['user_id'].'.json',array('screen_name'=>$tmpAuth['screen_name'],'include_entities'=>true));
			$tw_friends=array();
			if(count($tw_followers))
			{
				foreach ($tw_followers as $follower)
				{
					$tw_friends[$follower->screen_name]=array(
							'name'=>$follower->name,
							'photo'=>$follower->profile_background_image_url
					);
				}
			}
			$this->User->recursive=-1;
			$existing_users=$this->User->find('all',array(
					'fields'=>array('User.unique_id','User.facebook_id','User.star_user','User.first_name','User.username','User.last_name','User.photo','Follower.user_id'),
					'conditions'=>array('User.twitter_id'=>array_keys($tw_friends),'User.id != '=>$this->Auth->user('id')),
					'joins'=>array(
							array(
									'table' => 'followers',
									'alias' => 'Follower',
									'type' => 'LEFT',
									'conditions' => array(
											'User.id = Follower.user_id',
											'Follower.follower_id = '.$this->Auth->user('id')
									)
							))
			));
				
			$tw_friends_exist=array();
			foreach($existing_users as $everdrobe_user)
			{
				$tw_friends_exist[$everdrobe_user['User']['unique_id']]=array(
						'name'=>$everdrobe_user['User']['username']!=""?$everdrobe_user['User']['username']:$everdrobe_user['User']['first_name']." ".$everdrobe_user['User']['last_name'],
						'star_user'=>$everdrobe_user['User']['star_user'],
						//'photo'=> ( $everdrobe_user['User']['photo']!="" ? Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$everdrobe_user['User']['photo'],true) : $fb_friends[$everdrobe_user['User']['facebook_id']]['photo']),
						'following'=>($everdrobe_user['Follower']['user_id']>0)
				);
				unset($tw_friends[$everdrobe_user['User']['facebook_id']]);
			}
		}
		else
		{
			
		}
		$this->layout='ajax';
		$this->set('tw_friends',array('registered'=>$tw_friends_exist,'not_registered'=>$tw_friends));
		$this->set('is_ajax',$this->RequestHandler->isAjax());
	}
	function invite_friend($type="facebook",$to=null)
	{
		$response=array();
		$notifications=Cache::read('notification_cache');
		if($type=="facebook")
		{
			if($this->facebook==null)
			{
				$this->facebook = new Facebook(array("appId"=>Configure::read("facebook.api_key"),"secret"=> Configure::read("facebook.app_secrete")));
			}
			$content_pages=Cache::read('content_pages');
			$fb_invite_data=$notifications['facebook_invitation'];
			$post = array(
				'picture' => Router::url('/images/big_button.png',true),
				'link' => 'http://tiny.cc/ogwmow',
				'name' => $fb_invite_data['subject'],
				'caption' => 'www.everdrobe.com',
				'message' => $fb_invite_data['message'],
				'description' => $content_pages['meta_description']['page_content']
			);
			$post['access_token']=$this->facebook->getAccessToken();
			try{
				$res=$this->facebook ->api('/'.$to.'/feed', 'POST', $post);
				if(isset($res['id']))
				{
					$response['type']="success";
					$response['message']="Invitation sent succesfully to facebook friend";
				}
				else
				{
					$response['type']="error";
					$response['message']="Something happens wrong in facebook friend invitation";
				}
			}
			catch(Exception  $e){
				$response['type']="error";
				$response['message']="Something happens wrong in facebook friend invitation";
			}
		}
		elseif($type=="twitter")
		{
			App::import('Vendor', 'twitterOauth/twitteroauth');
			//Connecting with Twitter with current account
			$userData=array();
			
			$tmpAuth=$this->Session->read('twitter_auth');
			$this->twitterObj = new TwitterOAuth(Configure::read("twitter.consumer_key"), Configure::read("twitter.consumer_secret"),$tmpAuth['oauth_token'],$tmpAuth['oauth_token_secret']);
			
			/*$result=$this->twitterObj->post('https://api.twitter.com/1/direct_messages/new.json',array(
					'user_id'=>$to,
					'text'=>str_replace("##twitter_screen_name",$to,$tw_invite_data['message'])
			));*/
			
			$tw_invite_data=$notifications['twitter_invitation'];
			$post=str_replace("##twitter_screen_name",$to,$tw_invite_data['message']);
			//$post="@".$to." I'm addicted to @everdrobe. You need to check it out! http://tiny.cc/qyvmow";
			$result=$this->twitterObj->post('statuses/update', array('status' => $post));
			
			
			if($result->id > 0)
			{
				$response['type']="success";
				$response['message']="Twitter friend invited successfully";
			}
			else
			{
				$response['type']="error";
				$response['message']="Something happens wrong in twitter friend invitation";
			}
		}
		echo json_encode($response);
		exit();
	}
	
	/*
	 * Webservice for find registered user in everdrobe from fecebook/twitter friends  
	 * 
	 */
	function registered_friends($type=null)
	{
		$response=array();
		if(!empty($this->data) && in_array($type, array("facebook","twitter")) && intval($this->data['user_id'])>0)
		{
			$conditions=array('User.id != '=>$this->data['user_id']);
			
			$friend_ids=array();
			if($this->data['friend_ids']!="") $friend_ids=explode(",",$this->data['friend_ids']);
			if($type=="facebook") $conditions['User.facebook_id']=$friend_ids;
			if($type=="twitter") $conditions['User.twitter_id']=$friend_ids;
			
			if(count($friend_ids)>0)
			{
				$this->User->recursive=-1;
				$existing_users=$this->User->find('all',array(
						'fields'=>array('User.id','User.facebook_id','User.twitter_id','User.first_name','User.username','User.last_name','User.photo','User.star_user','Follower.user_id'),
						'conditions'=>$conditions,
						'joins'=>array(
								array(
										'table' => 'followers',
										'alias' => 'Follower',
										'type' => 'LEFT',
										'conditions' => array(
												'User.id = Follower.user_id',
												'Follower.follower_id = '.$this->data['user_id']
										)
								))
				));
					
				$friends_exist=array();
				foreach($existing_users as $everdrobe_user)
				{
					$friends_exist[]=array(
							'id' => $everdrobe_user['User']['id'],
							'friend_id'=> $type=="facebook" ? $everdrobe_user['User']['facebook_id'] : $everdrobe_user['User']['twitter_id'],
							'name' => $everdrobe_user['User']['username'],
							'photo' =>  ($everdrobe_user['User']['photo']!="" ? Router::url('/'.str_replace( DS,'/',Configure::read('profile.thumb.upload_dir')).'/'.$everdrobe_user['User']['photo'],true) :  ""),
							'star_user' =>  "".$everdrobe_user['User']['star_user'],
							'following' => ($everdrobe_user['Follower']['user_id'] > 0 ? "yes" : "no")
					);
				}
				$response['type']="success";
				$response['friend_exist']=$friends_exist;
			}
			else 
			{
				$response['type']="error";
				$response['message']="Provide friend ids comma sarerated";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="Invalid post fields";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	
	
	function _getNewBadgeCounter($friend_ids,$user_id)
	{
		if($friend_ids>0)
		{
			$this->loadModel('NewDrobe');
			$newDrobes=$this->NewDrobe->find('all',array('fields'=>array('NewDrobe.user_id as user_id','count(NewDrobe.drobe_id) as total'),'conditions'=>array("NewDrobe.follower_id"=>$user_id),'group'=>"NewDrobe.user_id"));
			$counter=array();
			foreach ($newDrobes as $drobe)
			{
				$counter[$drobe['NewDrobe']['user_id']]=$drobe[0]['total'];
			}
			return $counter;
		}
		else return null;
	}
	
	/* webservice for counter of new drobes in following tab bar
	 * 
	 */
	function following_drobes_counter($user_id)
	{
		$response=array();
		if($user_id>0)
		{
			$this->loadModel('NewDrobe');
			$new_drobes=$this->NewDrobe->find('count',array('conditions'=>array("NewDrobe.follower_id"=>$user_id)));
			$response['type']="success";
			$response['counter']="".$new_drobes;
		}
		else
		{
			$response['type']="error";
			$response['message']="provide logged in user id with url";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	
	private function _auto_login($userData){
		$this->Auth->fields = array('username' => 'facebook_id', 'password' => 'fb_password');
		if(!isset($userData['id']))
		{
			$userData =$this->User->find('first', array('conditions' => array('facebook_id' => $this->facebookUserId)));
			$userData=$userData['User'];
		}
		$user = array();
		$userData['fb_password']=$this->Auth->password($userData['facebook_id']);
		if ($this->Auth->login($userData)) {
			//$this->redirect($this->Auth->redirect());
		}
		else
		{
		}
	}
	private function __randomString($minlength = 20, $maxlength = 20, $useupper = true, $usespecial = false, $usenumbers = true){
		$charset = "abcdefghijklmnopqrstuvwxyz";
		if ($useupper) $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		if ($usenumbers) $charset .= "0123456789";
		if ($usespecial) $charset .= "~@#$%^*()_+-={}|][";
		if($minlength==$maxlength) $length=$minlength;
		else if ($minlength > $maxlength) $length = mt_rand ($maxlength, $minlength);
		else $length = mt_rand ($minlength, $maxlength);
		$key = '';
		for ($i=0; $i<$length; $i++){
			$key .= $charset[(mt_rand(0,(strlen($charset)-1)))];
		}
		return $key;
	}
	private function _createProfileImageFromURL($image_url)
	{
		$headers = get_headers($image_url,1);
		// just a precaution, check whether the header isset...
		if(isset($headers['Location'])) {
			$url = $headers['Location']; // string
			return $this->_createProfileImage(base64_encode(file_get_contents($url)));
		} else {
			return false; // nothing there? .. weird, but okay!
		}
		
	}
	private function _createProfileImage($imageData)
	{
		$data = base64_decode($imageData);
		$src = imagecreatefromstring($data);
		$file_name=uniqid().'.jpg';
		$target_path = WWW_ROOT . Configure::read('profile.upload_dir').DS.$file_name;
		if(imagejpeg($src,$target_path))
		{
			$width = imagesx($src);
			$height = imagesy($src);

			$thumbWidth=Configure::read('profile.thumb.width');
			$thumbHeight=Configure::read('profile.thumb.height');
				
			/* find the "desired height" of this thumbnail, relative to the desired width  */
			if($thumbHeight==0)
			$thumbHeight = floor($height*($thumbWidth/$width));
			 
			/* create a new, "virtual" image */
			$virtual_image = imagecreatetruecolor($thumbWidth,$thumbHeight);
			 
			/* copy source image at a resized size */
			imagecopyresized($virtual_image,$src,0,0,0,0,$thumbWidth,$thumbHeight,$width,$height);
			/* create the physical thumbnail image to its destination */
			$dest= WWW_ROOT . Configure::read('profile.thumb.upload_dir').DS.$file_name;
			imagejpeg($virtual_image,$dest);
			return $file_name;
		}
		else return false;
	}
	function notification()
	{
		$device_token=array("a034c878b49f3abdf0006d5e7156b6ac5371c3ed7c3dc78f61d02bf826025132");
		$message="Hi Naitik test it from server";
		$this->send_notification($device_token, $message);
	}
	function notification_test()
	{
		$response=array();
		if(!empty($this->data))
		{
			$userData=$this->User->UserSetting->find('first',array('conditions'=>array('apple_device_token'=>$this->data['device_token'])));
			$this->log("User Data :: ".print_r($userData,true),'push_notification');
			if(isset($userData['UserSetting']) && $userData['UserSetting']['user_id']>0)
			{
				$params=json_decode($this->data['params'],true);
				if($this->send_notification_user($this->data['message'], $userData['UserSetting']['user_id'], $params))
				{
					$response['type']="success";
					$response['message']="Message sent successfully";
				}
				else
				{
					$response['type']="error";
					$response['message']="something happening wrong with pushnotification process";
				}
			}
			else
			{
				$response['type']="error";
				$response['message']="This device token is not registered";
			}
		}
		else
		{
			$response['type']="error";
			$response['message']="invalid params";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
	function invalid_request()
	{
		
	}
	function reset_user_totals()
	{
		$user_ids=array_keys($this->User->find('list'));
		foreach($user_ids as $user_id)
		{
			$this->User->resetUserTotalFields($user_id);
		}
		return true;
	} 
	
	
	/**  User everbucks screen which is earn by user */
	function my_everbucks()
	{
		/*$this->loadModel('Drobe');
		$this->Drobe->unbindModel(array('hasMany'=>array("Favourite","Rate","Flag"),'hasOne'=>array("DrobeSetting"),'belongsTo'=>array("Category"))); */
		
		//$user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		// getting user totals
		$user_totals=$this->User->getUserTotals($this->Auth->user('id'));
		$this->User->recursive = -1;
		$user_data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		$this->set('user_totals',$user_totals);
		$this->set('user_data',$user_data);
	}
	
	/*function delete_shopify_users(){
		exit;
		$this->loadModel('Deluser');
		$countCust=$this->shopify('/customers/count',array(),"GET");
		$totalPages = ceil($countCust['count']/250);
		$startDate=strtotime('2013-11-22');
		for($i=1;$i<=$totalPages;$i++){
			$response=$this->shopify('/customers',array(),"GET","fields=created_at,id,email&limit=250&page=".$i);
			foreach($response['customers'] as $customer){
				if(strtotime($customer['created_at']) >= $startDate){
					$this->Deluser->save(array("shop_id"=>$customer['id']));
					$this->Deluser->id=null;
				}
			}
		}
		$usersDelete=$this->Deluser->find('list',array('fields'=>array('Deluser.shop_id'),'conditions'=>array('Deluser.del'=>0)));
		echo "<pre>";
		foreach($usersDelete as $key=>$user)
		{
			$delResponse=$this->shopify('/customers/'.$user,"","DELETE");
			$this->Deluser->id=$key;
			$this->Deluser->saveField('del','1');
			print_r($delResponse);
		}
		exit;
	}*/
	
	/* Reset received comments for drobes in user_total table @13/01/2014 */
	function reset_user_comments_received()
	{
		$user_ids=array_keys($this->User->find('list'));
		foreach($user_ids as $user_id)
		{
			$this->User->resetCommentsReceived($user_id);
		}
		echo "Completed Successfully";
		exit;
	}
	
	/* Reset Drobe total comments for drobe in drobe table */
	function reset_total_comments()
	{
		$user_ids=array_keys($this->User->find('list'));
		foreach($user_ids as $user_id)
		{
			$this->User->resetTotalComments($user_id);
		}
		echo "Completed Successfully";
		exit;
	}
		
}
?>
