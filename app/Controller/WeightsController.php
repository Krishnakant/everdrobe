<?php 
class WeightsController extends AppController
{
	
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		$this->paginate = array(
			'fields'=>array('Weight.*'),
			'limit' => 15,
			'order'=>'Weight.id ASC'
		);
		$data = $this->paginate('Weight');
		$this->set(compact('data'));
	}
	
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->Weight->save($this->data))
			{
				$this->Session->setFlash("Weight added successfully.",'default',array('class'=>"success"));
				$this->Weight->_updateWeightCache();
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				if(!empty($this->Weight->validationErrors))
				{
					$this->set('errors',$this->Weight->validationErrors);
					$this->Weight->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Weight->id=$id;
		if(!empty($this->data))
		{
			$this->Weight->validate['weight']['isUnique']['on']='update';
			if($this->Weight->save($this->data))
			{
				$this->Session->setFlash("Weight updated successfully.",'default',array('class'=>"success"));
				$this->Weight->_updateWeightCache();
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				if(!empty($this->Weight->validationErrors))
				{
					$this->set('errors',$this->Weight->validationErrors);
					$this->Weight->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Weight->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if($this->Weight->delete($id))
			{
				$this->Session->setFlash("Selected weight deleted successfully",'default',array('class'=>"success"));
				$this->Weight->_updateWeightCache();
			}
			else
			{
				$this->Session->setFlash("Error occured in delete weight");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	function getlist()
	{
		$label_weight=Cache::read('label_weight');
		if($label_weight==null)
		{
			$this->Weight->_updateWeightCache();
			$label_weight=Cache::read('label_weight');
		}
		if(count($label_weight)>0)
		{
			$response['type']="success";
			$response['Weight']=$label_weight;
		}
		else 
		{
			$response['type']="error";
			$response['message']="weight not found";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>