<?php 
class MailQueuesController extends AppController
{
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}
	function send()
	{
		$mailQueue=$this->MailQueue->find('all',array(
				"conditions"=>array("MailQueue.sent"=>"no"),
				"order"=>"MailQueue.priority ASC, MailQueue.attempts DESC, MailQueue.id ASC"
				));
		if($mailQueue)
		{
			// setting up phpMailer
			App::import('Vendor', 'PhpMailer', array('file' => 'phpMailer' . DS . 'class.phpmailer.php'));
			$mail= new PHPMailer();
			if(Configure::read('smtp.enabled'))
			{
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->SMTPAuth      = true;                  // enable SMTP authentication
				$mail->SMTPSecure = 'ssl';
				$mail->Host          = Configure::read('smtp.host'); // sets the SMTP server
				$mail->Port          = Configure::read('smtp.port');                    // set the SMTP port for the GMAIL server
				$mail->Username      = Configure::read('smtp.username'); // SMTP account username
				$mail->Password      = Configure::read('smtp.password');        // SMTP account password
			}
			else
			{
				$mail->SMTPAuth      = false;
			}
			$mail->SMTPKeepAlive = true;                  // SMTP connection will not close
			$mail_id=array('sent'=>array(),'not_sent'=>array());
			
			foreach($mailQueue as $data)
			{
				$mail_from=explode("||",$data['MailQueue']['mail_from']);
				$mail->SetFrom($mail_from[0], $mail_from[1]);
				$mail->AddReplyTo(Configure::read('mail.reply_to'), Configure::read('mail.to_name'));
				
				$mail->Subject       = $data['MailQueue']['subject'];
				$mail->AltBody    = $data['MailQueue']['alt_body']; // optional, comment out and test
				$mail->MsgHTML($data['MailQueue']['body']);
				$mail_to=explode("||",$data['MailQueue']['mail_to']);
				$mail->AddAddress($mail_to[0],  $mail_to[1]);
				
				try 
				{
					if(!$mail->Send()) {
						$mail_id['not_sent'][]=$data['MailQueue']['id'];
					} else {
						$mail_id['sent'][]=$data['MailQueue']['id'];
					}
				}
				catch(Exception $e)
				{
					
				}
				// clear all fields for next loop
				$mail->ClearAddresses();
				$mail->ClearAllRecipients();
				$mail->ClearReplyTos();
				$mail->ClearAttachments();
			}
			if(count($mail_id['sent'])>0)
			{
				// removeing mail which is sent
				$this->MailQueue->deleteAll(array('MailQueue.id'=>$mail_id['sent']));	
			}
			else
			{
				// increamenting total attempts for not sent mail
				$this->MailQueue->updateAll(array('MailQueue.attempts'=>'MailQueue.attempts + 1'),array('MailQueue.id'=>$mail_id['sent']));
			}
			// removing mail from queue which is attempted ateast 3 times
			$this->MailQueue->deleteAll(array('MailQueue.attempts >= '=>3));
		}
	}
	function send_new()
	{
		
			// setting up phpMailer
			App::import('Vendor', 'PhpMailer', array('file' => 'phpMailer' . DS . 'class.phpmailer.php'));
			$mail= new PHPMailer();
			if(Configure::read('smtp.enabled'))
			{
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->SMTPAuth      = true;                  // enable SMTP authentication
				$mail->SMTPSecure = 'ssl';
				$mail->Host          = Configure::read('smtp.host'); // sets the SMTP server
				$mail->Port          = Configure::read('smtp.port');                    // set the SMTP port for the GMAIL server
				$mail->Username      = Configure::read('smtp.username'); // SMTP account username
				$mail->Password      = Configure::read('smtp.password');        // SMTP account password
			}
			else
			{
				$mail->SMTPAuth      = false;
			}
			$mail->SMTPKeepAlive = true;                  // SMTP connection will not close
			$mail_id=array('sent'=>array(),'not_sent'=>array());
				
			
				//$mail_from=explode("||",$data['MailQueue']['mail_from']);
				$mail->SetFrom("devang.bhagdev@slktechlabs.com", "devang");
				$mail->AddReplyTo(Configure::read('mail.reply_to'), Configure::read('mail.to_name'));
	
				$mail->Subject       = "hello";
				$mail->AltBody    = "hi"; // optional, comment out and test
				$mail->MsgHTML("how r u");
				//$mail_to=explode("||",$data['MailQueue']['mail_to']);
				$mail->AddAddress("100004422186997@facebook.com");
	
				try
				{
					if(!$mail->Send()) {
						echo "not sent";
					} else {
						echo "sent";
					}
				}
				catch(Exception $e)
				{
						
				}
				// clear all fields for next loop
				$mail->ClearAddresses();
				$mail->ClearAllRecipients();
				$mail->ClearReplyTos();
				$mail->ClearAttachments();
				exit;
	}
}
?>