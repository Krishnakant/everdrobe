<?php
App::uses('AppController', 'Controller');
class LogsController extends AppController
{
	
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
	}
	function admin_upload()
	{
	}
	function admin_clear($type=null)
	{
		if(file_exists(LOGS.$type.".log") && $fp=fopen(LOGS.$type.".log", "w"))
		{
			fclose($fp);
			$this->Session->setFlash(Inflector::humanize($type)." log cleared successfully",'default',array("class"=>"success"));
		}
		else
		{
			$this->Session->setFlash("Error occured in clear file please check permission of file or file is exist on ".LOGS." directory",'default',array("class"=>"error"));
		}
		$this->redirect($this->referer());
	}
	function admin_clear_cache_file($file)
	{
		if(file_exists(TMP.'cache'.DS.$file) && $fp=fopen(TMP.'cache'.DS.$file, "w"))
		{
			fclose($fp);
			$this->Session->setFlash(Inflector::humanize($file)." cache cleared successfully",'default',array("class"=>"success"));
		}
		else
		{
			$this->Session->setFlash("Error occured in clear cache please check permission of file or file is exist on ".TMP.'cache'.DS." directory",'default',array("class"=>"error"));
		}
		$this->redirect($this->referer());
	}
	function admin_clean_cache()
	{
		Cache::clear();
		
		$this->Session->setFlash("Cache cleaned sucessfully",'default',array("class"=>"success"));
		$this->redirect($this->referer());
	}
	
}
