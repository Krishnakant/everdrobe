<?php 
class SizesController extends AppController
{
	//var $scaffold;
	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		//$categories=$this->Size->find('all');
		//$this->set('categories',$categories);
		
		$joins =
		array(
				array(
						'table'=>'sell_drobes',
						'alias'=>'SellDrobe',
						'type'=>'left',
						'conditions'=>array('SellDrobe.size_name=Size.size_name')
				)
		);
		
		$this->paginate = array(
			'fields'=>array('Size.*','count(SellDrobe.size_name) as drobes'),
			'joins'=>$joins,
			'limit' => 15,
			'group'=>array('Size.size_name'),
			'order'=>'Size.order ASC'
		);
		$data = $this->paginate('Size');
		$this->set(compact('data'));
	}
	function admin_position($action=null,$category_id=null)
	{
		$this->Size->recursive=-1;
		$this->Size->id=$category_id;
		$current_order=$this->Size->field('order',array('Size.id'=>$category_id));
		if($action=="up")
		{
			$previous_drobe=$this->Size->find('first',array("fields"=>array('Size.id','Size.order'),"conditions"=>array("Size.order < "=>$current_order),"order"=>"Size.order DESC"));
			if($previous_drobe)
			{
				$this->Size->saveField('order',$previous_drobe['Size']['order']);
				$this->Size->id=$previous_drobe['Size']['id'];
				$this->Size->saveField('order',$current_order);
				$this->Session->setFlash("Size moved up successfully");
			}
			else
			{
				$this->Session->setFlash("Size not moved in up order");
			}
		}
		else if($action=="down")
		{
			$next_drobe=$this->Size->find('first',array("fields"=>array('Size.id','Size.order'),"conditions"=>array("Size.order > "=>$current_order),"order"=>"Size.order ASC"));
			if($next_drobe)
			{
				$this->Size->saveField('order',$next_drobe['Size']['order']);
				$this->Size->id=$next_drobe['Size']['id'];
				$this->Size->saveField('order',$current_order);
				$this->Session->setFlash("Size moved down successfully");
			}
			else
			{
				$this->Session->setFlash("Size not found in down order");
			}
		}
		else if($action=="first" || $action=="last")
		{
			$listing_order=$this->Size->find('list',array("fields"=>array("Size.order"),"conditions"=>array("Size.id != "=>$category_id),"order"=>"Size.order ASC"));
			if($action=="first")
			{
				$this->Size->saveField('order',1);
				$pos=2;
				foreach ($listing_order as $id=>$order)
				{
					$this->Size->id=$id;
					$this->Size->saveField('order',$pos);
					$pos++;
				}
			}
			else
			{
				$pos=1;
				foreach ($listing_order as $id=>$order)
				{
					$this->Size->id=$id;
					$this->Size->saveField('order',$pos);
					$pos++;
				}
				$this->Size->id=$category_id;
				$this->Size->saveField('order',$pos);
			}
				
		}
		else if($action=="reset")
		{
			$categories=$this->Size->find('all',array("order"=>"CAST(Size.size_name as DECIMAL(8,2)) ASC"));
			if($categories)
			{
				$order=1;
				foreach($categories as &$category)
				{
					$category['Size']['order']=$order;
					$order++;
				}
				$this->Size->saveAll($categories);
			}
		
		}
		$this->Size->_updateSizeCache();
		return $this->redirect($this->referer());
	}
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->Size->save($this->data))
			{
				$this->Session->setFlash("Size added successfully.",'default',array('class'=>"success"));
				$this->Size->_updateSizeCache();
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
	}
	function admin_edit($id=null)
	{
		$this->Size->id=$id;
		if(!empty($this->data))
		{
			$this->Size->validate['size_name']['isUnique']['on']='update';
			if($this->Size->save($this->data))
			{
				$this->Session->setFlash("Size updated successfully.",'default',array('class'=>"success"));
				$this->Size->_updateSizeCache();
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				if(!empty($this->User->validationErrors))
				{
					$this->set('errors',$this->User->validationErrors);
					$this->User->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Size->read();
		}
	}
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if(configure::read('other_size_id')!=$id)
			{
				/* Read size from cache file */
				$sizes=Cache::read('size_list');
				if($sizes==null)
				{
					$this->Size->_updateSizeCache();
					$sizes=Cache::read('size_list');
				}
				if($this->Size->delete($id))
				{
					$this->loadModel('SellDrobe');
					$this->SellDrobe->recursive = -1;
					//set other_size_id is configured in core.php default.
					$other_size_id = Configure::read('other_size_id');
					$size_name = $sizes[$other_size_id];
					$this->SellDrobe->updateAll(array('SellDrobe.size_name'=>"'".$size_name."'"),array('SellDrobe.size_name'=>$sizes[$id]));
					$this->Session->setFlash("Selected size deleted successfully",'default',array('class'=>"success"));
					$this->Size->_updateSizeCache();
				}
				else
				{
					$this->Session->setFlash("Error occured in delete category");
				}
			}
			else
			{
				$this->Session->setFlash("You cannot delete this default size");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	function getlist()
	{
		
		$categories=Cache::read('size_list');
		if($categories==null)
		{
			$this->Size->_updateSizeCache();
			$categories=Cache::read('size_list');
		}
		if(count($categories)>0)
		{
			$data=array();
			foreach($categories as $id=>$size_name)
			{
				$data[]=array('id'=>"".$id,'size_name'=>$size_name);
			}
			$response['type']="success";
			$response['sizes']=$data;
		}
		else 
		{
			$response['type']="error";
			$response['message']="Size not found";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>