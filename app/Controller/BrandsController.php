<?php 
class BrandsController extends AppController
{

	function beforeFilter()
	{
		parent::beforeFilter();
	}
	function admin_index()
	{
		$joins = 
			array(
				array(
						'table'=>'sell_drobes',
						'alias'=>'SellDrobe',
						'type'=>'left',
						'conditions'=>array('SellDrobe.sell_brand_name=Brand.brand_name')
					)
			);
		$this->paginate = array(
			'fields'=>array('Brand.*','count(SellDrobe.sell_brand_name) as drobes'),
			'joins'=>$joins,	
			'limit' => 15,
			'group'=>array('Brand.brand_name'),	
			'order'=>'Brand.id ASC'
		);
		$data = $this->paginate('Brand');
		$this->set(compact('data'));
	}
	
	
	function admin_add()
	{
		if(!empty($this->data))
		{
			if($this->Brand->save($this->data))
			{
				$this->Session->setFlash("Brand added successfully.",'default',array('class'=>"success"));
				$this->Brand->_updateBrandCache();
				$this->redirect(array('action'=>'index'));
			}
			else 
			{
				if(!empty($this->Brand->validationErrors))
				{
					$this->set('errors',$this->Brand->validationErrors);
					$this->Brand->validationErrors=array();
				}
			}
		}
	}
	
	
	/**
	 * @param unknown_type $id
	 * only Admin can edit brand_name
	 */
	function admin_edit($id=null)
	{
		$this->Brand->id=$id;
		if(!empty($this->data))
		{
			$this->Brand->validate['brand_name']['isUnique']['on']='update';
			if($this->Brand->save($this->data))
			{
				$this->Session->setFlash("Brand updated successfully.",'default',array('class'=>"success"));
				$this->Brand->_updateBrandCache();
				$this->redirect(array('action'=>'index'));
			}
			else
			{
				if(!empty($this->Brand->validationErrors))
				{
					$this->set('errors',$this->Brand->validationErrors);
					$this->Brand->validationErrors=array();
				}
			}
		}
		else
		{
			$this->data=$this->Brand->read();
		}
	}
	
	/**
	 * @param unknown_type $id
	 * only admin can delete brand.
	 */
	function admin_delete($id=null)
	{
		if($id>0)
		{
			if(configure::read('other_brand_id')!=$id)
			{
				$brands=Cache::read('brand_list');
				if($brands==null)
				{
					$this->Brand->_updateBrandCache();
					$brands=Cache::read('brand_list');
				}
				if($this->Brand->delete($id))
				{
					$this->loadModel('SellDrobe');
					$this->SellDrobe->recursive = -1;
					//set other_size_id is configured in core.php default.
					$other_brand_id = Configure::read('other_brand_id');
					$brand_name = $brands[$other_brand_id];
					$this->SellDrobe->updateAll(array('SellDrobe.sell_brand_name'=>"'".$brand_name."'"),array('SellDrobe.sell_brand_name'=>$brands[$id]));
					$this->Session->setFlash("Selected brand deleted successfully",'default',array('class'=>"success"));
					$this->Brand->_updateBrandCache();
				}
				else
				{
					$this->Session->setFlash("Error occured in delete brand");
				}
			}
			else
			{
				$this->Session->setFlash("You cannot delete this default brand");
			}
		}
		else
		{
			$this->Session->setFlash("Invalid parameters passed");
		}
		$this->redirect(array('action'=>"index"));
	}
	
	
	/**
	 * Get list of brand_id and brand_name 
	 */
	function getlist()
	{
		
		$brands=Cache::read('brand_list');
		if($brands==null)
		{
			$this->Brand->_updateBrandCache();
			$brands=Cache::read('brand_list');
		}
		if(count($brands)>0)
		{
			$data=array();
			foreach($brands as $id=>$brand_name)
			{
				$data[]=array('id'=>"".$id,'brand_name'=>$brand_name);
			}
			$response['type']="success";
			$response['brands']=$data;
		}
		else 
		{
			$response['type']="error";
			$response['message']="Brand not found";
		}
		$this->set('response',$response);
		$this->set('_serialize',array('response'));
	}
}
?>