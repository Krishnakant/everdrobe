<?php 
class AppError {
	public static function handleError($code, $description, $file = null, $line = null, $context = null) {
		list(, $level) = ErrorHandler::mapErrorCode($code);
		if ($level === LOG_ERROR) {
			
			// send mail for fatal error issue
			if(Configure::read('enable_error_notification')==true)
			{
				App::import('Vendor', 'PhpMailer', array('file' => 'phpMailer' . DS . 'class.phpmailer.php'));
				// replacing variables in appropriate place in subject line and mail body
				$mail_sub=str_replace(array_keys($variables), $variables, $data['Notify']['subject']);
				$mail_message="<pre>".print_r(compact('code','description','file','line','context'),true)."</pre>";
				$mail_message=eregi_replace("[\]",'',$mail_message);
				$mail= new PHPMailer();
				$mail->SMTPAuth      = false;
				$mail->SetFrom(Configure::read('mail.from'), Configure::read('mail.from_name'));
				$mail->AddReplyTo(Configure::read('mail.reply_to'), Configure::read('mail.to_name'));
				$mail->Subject = "Everdrobe Fatal Error Alert on ".date('d-m-Y H:i:s');
				$mail->AltBody = $mail_message;
				$mail->MsgHTML($mail_message);
				$mail->AddAddress(Configure::read('error_notification_to'), '');
				$mail->Send();
			}
			// Ignore fatal error. It will keep the PHP error message only
			return false;
		}
		return ErrorHandler::handleError($code, $description, $file, $line, $context);
	}
}
?>