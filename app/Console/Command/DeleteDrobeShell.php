<?php
/*
 * Call Format <<
 * 
 */


/*
 * How to call This using command line
 *  d:\wamp\www\event_organizer\lib\Cake\Console\cake -app d:\wamp\www\event_organizer\app mail_send
 *  php /mnt/r10-vhosts/vhosts/dvl/htdocs/lib/Cake/Console/cake.php -app /mnt/r10-vhosts/vhosts/dvl/htdocs/app delete_drobe
 *  php /var/www/everdrobe/lib/Cake/Console/cake.php -app /var/www/everdrobe/app delete_drobe
 *  delete_deleted_drobes
 */

App::import('Core', 'Controller');
App::import('Controller', 'Drobes');
class DeleteDrobeShell extends AppShell {

	public function main() {
        $queueObj = new DrobesController();
        $queueObj->constructClasses();
        $queueObj->delete_deleted_drobes();
        $this->out('Process Completed.');
    }
    
}