<?php
/*
 * Call Format <<
 * 
 */


/*
 * How to call This using command line
 *  d:\wamp\www\event_organizer\lib\Cake\Console\cake -app d:\wamp\www\event_organizer\app mail_send
 *  php /home/vhosts/everdrobe.com/htdocs/e-d-v-l/lib/Cake/Console/cake.php -app /home/vhosts/everdrobe.com/htdocs/e-d-v-l/app notification_send
 */

App::import('Core', 'Controller');
App::import('Controller', 'NotificationQueues');
class NotificationSendShell extends AppShell {

	public function main() {
        $queueObj = new NotificationQueuesController();
        $queueObj->constructClasses();
        $queueObj->send();
        $this->out('Process Completed.');
    }
    
}