<?php
/*
 * Call Format <<
 * 
 */


/*
 * How to call This using command line
 *  d:\wamp\www\everdrobe\lib\Cake\Console\cake -app d:\wamp\www\everdrobe\app clean_images 
 *  * * * * * php /mnt/r10-vhosts/vhosts/dvl/htdocs/lib/Cake/Console/cake.php -app /mnt/r10-vhosts/vhosts/dvl/htdocs/app clean_images
 */

App::import('Core', 'Controller');
App::import('Controller', 'Drobes');
class CleanImagesShell extends AppShell {

	public function main() {
        $drobeObj = new DrobesController();
        $drobeObj->constructClasses();
        $return=$drobeObj->delete_unused_images();
        $this->out($return);
    }
    
}