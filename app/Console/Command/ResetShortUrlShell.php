<?php
/*
 * Call Format <<
 * 
 */


/*
 * How to call This using command line
 *  d:\wamp\www\event_organizer\lib\Cake\Console\cake -app d:\wamp\www\event_organizer\app mail_send
 *  php /home/vhosts/everdrobe.com/htdocs/e-d-v-l/lib/Cake/Console/cake.php -app /home/vhosts/everdrobe.com/htdocs/e-d-v-l/app mail_send
 */

App::import('Core', 'Controller');
App::import('Controller', 'Drobes');
class ResetShortUrlShell extends AppShell {

	public function main() {
        $drobeObj = new DrobesController();
        $drobeObj->constructClasses();
        $drobeObj->update_short_url();
        $this->out('Process Completed.');
    }
    
}