<?php
/*
 * Call Format <<
 * 
 */


/*
 * How to call This using command line
 *  d:\wamp\www\event_organizer\lib\Cake\Console\cake -app d:\wamp\www\event_organizer\app reset_log
 *  php /home/vhosts/everdrobe.com/htdocs/e-d-v-l/lib/Cake/Console/cake.php -app /home/vhosts/everdrobe.com/htdocs/e-d-v-l/app reset_log
 *  php /var/www/everdrobe/lib/Cake/Console/cake.php -app /var/www/everdrobe/app reset_log
 */

App::import('Core', 'Controller');
App::import('Controller', 'Users');
class ResetLogShell extends AppShell {

	public function main() {
        $queueObj = new UsersController();
        $queueObj->constructClasses();
        $queueObj->reset_logs();
        $this->out('Process Completed.');
    }
    
}