<?php
/*
 * Call Format <<
 * 
 */


/*
 * How to call This using command line
 *  f:\wamp\www\everdrobe\lib\Cake\Console\cake -app f:\wamp\www\everdrobe\app vote 1 1156 
 *  * * * * * php /home/vhosts/everdrobe.com/htdocs/e-d-v-l/lib/Cake/Console/cake.php -app /home/vhosts/everdrobe.com/htdocs/e-d-v-l/app vote 1 1156
 */

App::import('Core', 'Controller');
App::import('Controller', 'Drobes');
class VoteShell extends AppShell {

	public function main() {
		
		$vote=intval($this->args[0]);
		$user_id=$this->args[1];
		if($vote==0)
		{
			$vote=-1;
		}
		else if($vote==2)
		{
			$vote=0;
		}
		if(!in_array($vote, array(0,1,-1)))
		{
			$this->out('Invalid vote argument, use 1 for Cute, 0 for Hot, 2 for Skip');
		}
		elseif(!(intval($user_id)>0 && intval($user_id)<10000))
		{
			$this->out('Please enter valid user code between 1 to 9999');
		}
		else
		{
	        $drobeObj = new DrobesController();
	        $drobeObj->constructClasses();
	        $return=$drobeObj->do_vote($vote,$user_id);
	        $this->out($GLOBALS['vote_return']);
		}
    }
    
}