<?php 
class Contact extends AppModel
{
	var $name="Contact";
	var $validate=array(
		'full_name'=>array(
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Enter your full name'
			)
		),
		'email_id'=>array(
			'email'=>array(
				'rule'=>"email",
					'allowEmpty'=>true,
				'message'=>'Enter valid email id'
			)
		),
		'message'=>array(
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Message name is required'
			)
		),
	);
}
?>