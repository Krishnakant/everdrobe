<?php 
class Province extends AppModel
{
	var $name="Province";
	var $belongsTo="Country";
	var $displayField = 'province_name';
	var $validate=array(
		'province_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"State/Province name is required"
			)
		)
	);
}
?>