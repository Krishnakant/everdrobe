<?php 
class User extends AppModel
{
	var $name="User";
	var $hasOne=array("UserSetting");
	var $hasMany=array("Drobe",
			"Rate",
			"Follower",
			"StreamCategory");
	var $validate=array(
		'first_name'=>array(
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'required'=>true,
				'message' => 'First name must in alpha numeric only'
			),
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'First name is required'
			)
		),
		'last_name'=>array(
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Last name must in alpha numeric only'
			),
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Last name is required'
			)
		),
		'birth_date'=>array(
			'custom_birthdate'=>array(
				'rule'=>'validatebirthdate',
				'message'=> 'Enter valid birth date and your age must be atleast 13 year'
			)
		),
		'email'=>array(
			'email'=>array(
				'rule'=>"email",
				'required'=>true,
				'message'=>'Enter valid email id'
			),
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Email id is required'
			)
		),
		'website'=>array(
			'url'=>array(
				'rule'=>"url",
				'allowEmpty' => true,
				'required'=>false,
				'message'=>'Enter valid website URL'
			)
			
		),
		'password'=>array(
			'rule' => array('minLength', '6'),
			'message' => 'Password must be minimum 6 character long'
		),
		'current_password'=>array(
			'rule' => array('minLength', '6'),
			'message' => 'Current password must be minimum 6 character long'
		),
		'new_password'=>array(
			'rule' => array('minLength', '6'),
			'message' => 'New password must be minimum 6 character long'
		),
		'confirm_password'=>array(
			'identicalFieldValues' => array(
			        'rule' => array('identicalFieldValues', 'new_password' ),
			        'message' => 'New password and confirm password must be match'
			)
		),
		
		'agree'=>array(
			'notEmpty'=>array(
				'rule'=>'notEmpty',
				'message'=>'Please accept terms and conditions'
			),
			'range' => array(
				'rule' => array('range', 0, 2),
				'message'=>'Please accept Terms of Use and Privacy Policy agreement.'
			)
			
		)
	);
	function validatebirthdate()
	{
		if($this->data['User']['birth_date'])
		{
			return is_int(strtotime($this->data['User']['birth_date'])) && strtotime($this->data['User']['birth_date'])<strtotime(date('Y-m-d H:i:s')." -13 year");
		}
		else return false;	
	}
	function identicalFieldValues( $field=array(), $compare_field=null )
	{
		foreach( $field as $key => $value ){
			$v1 = $value;
			$v2 = $this->data[$this->name][ $compare_field ];
			if($v1 !== $v2) {
				return FALSE;
			} else {
				continue;
			}
		}
		return TRUE;
	}
}
?>