<?php 
class Notify extends AppModel
{
	var $name="Notify";
	var $validate=array(
		'action'=>array(
			'unique'=>array(
				'rule'=>'isUnique',
				'on'=>'create',
				'message'=>'Action name id already exist please set new action name'
			),
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Action field is required'
			)
		),
		'subject'=>array(
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Subject is required'
			)
		),
		'message'=>array(
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Message name is required'
			)
		),
	);
	function get_mail_data($action)
	{
		return $this->find('first',array('conditions'=>array('action'=>$action,'type'=>'email'),'fields'=>array('Notify.subject','Notify.message')));
	}
	function afterSave($created)
	{
		$this->updateNotificationCache();
	}
	function updateNotificationCache()
	{
		$notifications_cache=Cache::read('notification_cache');
		if(!empty($notifications_cache))
		{
			Cache::drop('notification_cache');
		}
		
		$this->recursive=-1;
		$notifications=$this->find('all',array("fields"=>array("Notify.action","Notify.subject","Notify.message")));
		$data=array();
		foreach($notifications as $notification)
		{
			$data[$notification['Notify']['action']]=$notification['Notify'];
		}
		Cache::write('notification_cache',$data);
		$this->recursive=0;
	}
	
}
?>