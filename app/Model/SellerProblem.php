<?php 
class SellerProblem extends AppModel
{
	var $name="SellerProblem";
	
	var $validate = array(); 
	
	function setDelayShipmentValidation(){

		$this->validate = array(
				'expected_ship_date'=>array(
						'notEmpty'=>array(
								'rule'=>'notEmpty',
								'required'=>true,
								'message'=>"Expected ship date is required"
						)
				),
				'reason_for_delay'=>array(
						'notEmpty'=>array(
								'rule'=>'notEmpty',
								'required'=>true,
								'message'=>'Reason for delay is required'
						)
				)
		);
	}
	
}
?>