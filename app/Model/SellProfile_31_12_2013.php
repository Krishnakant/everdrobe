<?php 
class SellProfile extends AppModel
{
	var $name="SellProfile";
	var $validate=array(
			'paypal_email'=>array(
					'notEmpty'=>array(
							'rule' => 'notEmpty',
							'message'=> 'Email id is required'
					),
					'email'=>array(
							'rule'=>"email",
							'required'=>true,
							'message'=>'Enter valid email id'
					)
			),
			'full_name'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'Your check payable name is required.'
					)
						
			),'street1'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'street1 is required.'
					)
			),
			/*'street2'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'street2 is required.'
					)
			),*/
			'city'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'city is required.'
					)
			),
			'state'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'state is required.'
					)
			),
			'zip'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'zip is required.'
					)
			)
	);
	
	
}
?>