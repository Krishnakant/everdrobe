<?php 
class User extends AppModel
{
	var $name="User";
	
	var $hasOne=array("UserSetting");
	
	var $hasMany=array("Drobe",
			"Rate",
			"Follower",
			"StreamCategory");
	
	var $validate=array(
			'username'=>array(
					 'alphaNumeric' => array(
							'rule'      => array('custom', '/^[A-Za-z0-9_]*[A-Za-z0-9][A-Za-z0-9_]*$/'),
							'required'=>true,
							'message' => 'User name must in alpha numeric and underscores only'
					), 
					'maxLength'=>array(
							'rule' => array('maxLength', 15),
							'message' => 'Username must be no larger than 15 characters long.'
							),
					'unique' => 	array(
       						 'rule' => 'isUnique_own',
       						 //'required' => 'create',
							 'message' => 'Username already exists.'
  				    ),
					'notEmpty'=>array(
							'rule' => 'notEmpty',
							'message'=> 'User name is required'
					)
			),			
		'first_name'=>array(
				'notEmpty'=>array(
						'rule' => 'notEmpty',
						'message'=> 'First name is required'
				),
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
		//		'required'=>true,
				'message' => 'First name must in alpha numeric only'
			)
			
		),
		'last_name'=>array(
				'notEmpty'=>array(
						'rule' => 'notEmpty',
						'message'=> 'Last name is required'
				),
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Last name must in alpha numeric only'
			)
			
		),
		'birth_date'=>array(
			'custom_birthdate'=>array(
				'rule'=>'validatebirthdate',
				'message'=> 'Enter valid birth date and your age must be atleast 13 year'
			)
		),
		'email'=>array(
			'email'=>array(
				'rule'=>"email",
				'required'=>true,
				'message'=>'Enter valid email id'
			),
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Email id is required'
			)
		),
		'website'=>array(
			'url'=>array(
				'rule'=>"url",
				'allowEmpty' => true,
				'required'=>false,
				'message'=>'Enter valid website URL'
			)
			
		),
		'password'=>array(
			'rule' => array('minLength', '6'),
			'message' => 'Password must be minimum 6 character long'
		),
		'current_password'=>array(
			'rule' => array('minLength', '6'),
			'message' => 'Current password must be minimum 6 character long'
		),
		'new_password'=>array(
			'rule' => array('minLength', '6'),
			'message' => 'New password must be minimum 6 character long'
		),
		'confirm_password'=>array(
			'identicalFieldValues' => array(
			        'rule' => array('identicalFieldValues', 'new_password' ),
			        'message' => 'New password and confirm password must be match'
			)
		),
		'agree'=>array(
			'notEmpty'=>array(
				'rule'=>'notEmpty',
				'message'=>'Please accept terms and conditions'
			),
			'range' => array(
				'rule' => array('range', 0, 2),
				'message'=>'Please accept Terms of Use and Privacy Policy agreement.'
			)
		)
	);
	public function beforeSave($options = array()) 
	{
		
		if (isset($this->data['User']['username']) && $this->data['User']['username']!="")
		{
			$this->data['User']['username']="@".str_replace("@","",$this->data['User']['username']);
		}
		
		return true;
	}
	
	/*function afterFind($results, $primary = false)
	{
		if(isset($results[0]))
		{
			foreach ($results as &$user)
			{
				if(isset($user['User']['username']) && $user['User']['username']!="")
				{
					$user['User']['username']="@".$user['User']['username'];
				}
				
			}
		}
		return $results;
	}*/
	/*
	 * Custom validation function for birthdate
	 */
	function isUnique_own()
	{
		if($this->data['User']['username'])
		{
			$user_exists = $this->find('count',array('conditions'=>array('User.username'=>"@".$this->data['User']['username'])));
			if($user_exists)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else return false;
	}
	function validatebirthdate()
	{
		if($this->data['User']['birth_date'])
		{
			return is_int(strtotime($this->data['User']['birth_date'])) && strtotime($this->data['User']['birth_date'])<strtotime(date('Y-m-d H:i:s')." -13 year");
		}
		else return false;	
	}
	
	/*
	 * Custom Validation function for match two fields value
	*/
	function identicalFieldValues( $field=array(), $compare_field=null )
	{
		foreach( $field as $key => $value ){
			$v1 = $value;
			$v2 = $this->data[$this->name][ $compare_field ];
			if($v1 !== $v2) {
				return FALSE;
			} else {
				continue;
			}
		}
		return TRUE;
	}
	
	/*
	 * update changes in perticular field of user total for perrticular user
	 */
	function updateUserTotalField($user_id,$field_name=null,$change=1)
	{
		if($field_name==null || $user_id==null) return;
		$updateFields=array();
		if(is_array($field_name))
		{
			foreach($field_name as $field)
			{
				$updateFields['UserTotal.'.$field]='UserTotal.'.$field.' + ('.$change.')';
			}
		}
		else
		{
			$updateFields['UserTotal.'.$field_name]='UserTotal.'.$field_name.' + ('.$change.')'; 
		}
		
		if(!isset($this->hasOne['UserTotal']))
		{
			$this->bindModel(array('hasOne'=>array("UserTotal")));
		}
		
		$this->UserTotal->updateAll($updateFields,array('UserTotal.user_id'=>$user_id));
	}
	/*
	 * Resets counter of total uploaded drobes by user
	 * 
	 */
	function resetMyDrobes($user_id)
	{
		$total_drobes=$this->Drobe->find('count',array('conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		$this->updateUserTotalCounter($user_id,'total_drobes',$total_drobes);
	}
	
	/*
	 * Resets counter of total following of user
	*/
	function resetMyFollowings($user_id)
	{
		$total_followings=$this->Follower->find('count',array('conditions'=>array('Follower.follower_id'=>$user_id)));
		$this->updateUserTotalCounter($user_id,'followings',$total_followings);
	
	}
	
	/*
	 * Resets counter of total followers of user
	*/
	function resetMyFollowers($user_id)
	{
		$total_followers=$this->Follower->find('count',array('conditions'=>array('Follower.user_id'=>$user_id)));
		$this->updateUserTotalCounter($user_id,'followers',$total_followers);
	}
	
	/*
	 * Resets counter of total IN received from uploaded drobes by users
	*/
	function resetMyIn($user_id)
	{
		$my_drobes=$this->Drobe->find('list',array('conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		$my_drobes=array_keys($my_drobes);
		$total_in=$this->Rate->find('count',array('conditions'=>array('Rate.drobe_id'=>$my_drobes,"Rate.rate > "=>0)));
		$this->updateUserTotalCounter($user_id,'total_in',$total_in);
	}
	
	/*
	 * Resets counter of total OUT received from uploaded drobes by users
	*/
	function resetMyOut($user_id)
	{
		$my_drobes=$this->Drobe->find('list',array('conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		$my_drobes=array_keys($my_drobes);
		$total_out=$this->Rate->find('count',array('conditions'=>array('Rate.drobe_id'=>$my_drobes,"Rate.rate < "=>0)));
		$this->updateUserTotalCounter($user_id,'total_out',$total_out);
	}
	
	/*
	 * Resets counter of total feedback given by user
	*/
	function resetCommentsGiven($user_id)
	{
		$commnet_given=$this->Rate->find('count',array('conditions'=>array('Rate.user_id'=>$user_id,'Rate.comment NOT LIKE '=>"")));
		$this->updateUserTotalCounter($user_id,'feedback_given',$commnet_given);
	}
	
	
	
	
	/*
	 * Resets counter of total votes received from uploaded drobes by user
	*/
	function resetVotesReceived($user_id)
	{
		$my_drobes=$this->Drobe->find('list',array('conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		$my_drobes=array_keys($my_drobes);
		$rate_given=$this->Rate->find('count',array('conditions'=>array('Rate.drobe_id'=>$my_drobes)));
		//$this->updateUserTotalCounter($user_id,'vote_received',$rate_given);
	}
	
	/*
	 * Resets counter of total votes given by user
	*/
	function resetVotesGiven($user_id)
	{
		$rate_given=$this->Rate->find('count',array('conditions'=>array('Rate.user_id'=>$user_id,"Rate.rate !="=>0)));
		$this->updateUserTotalCounter($user_id,'vote_given',$rate_given);
	}
	/*
	 * Resets counter of total drobes added in favs list by user
	*/
	function resetMyFaves($user_id)
	{
		$this->bindModel(array('hasMany'=>array('Favourite')));
		$total_faves=$this->Favourite->find('count',array(
					'joins'=>array(
							array(
									'table' => 'drobes',
									'alias' => 'Drobe',
									'type' => 'LEFT',
									'conditions' => array(
											'Drobe.id = Favourite.drobe_id',
									)
							)
						),
					'conditions'=>array('Favourite.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')
				)
			);
		$this->updateUserTotalCounter($user_id,'total_favourites',$total_faves);
	}
	
	/*
	 * Resets all the fields of specific user
	 */
	function resetUserTotalFields($user_id)
	{
		$this->resetMyDrobes($user_id);
		$this->resetMyFollowings($user_id);
		$this->resetMyFollowers($user_id);
		$this->resetMyIn($user_id);
		$this->resetMyOut($user_id);
		$this->resetCommentsGiven($user_id);
		$this->resetVotesReceived($user_id);
		$this->resetVotesGiven($user_id);
		$this->resetMyFaves($user_id);
		$this->resetCommentsReceived($user_id);
		$this->resetTotalComments($user_id);
	}
	
	/*
	 * Reset counter of perticular field for perticular user
	 */
	function updateUserTotalCounter($user_id,$field_name=null,$counter=0)
	{
		if($user_id==null || $field_name==null) return;
		
		$updateFields=array();
		$updateFields['UserTotal.'.$field_name]=$counter;
		if(!isset($this->hasOne['UserTotal']))
		{
			$this->bindModel(array('hasOne'=>array("UserTotal")));
		}
		$this->UserTotal->updateAll($updateFields,array('UserTotal.user_id'=>$user_id));
	}
	
	/*
	 * 
	 */
	function getUserTotals($user_id)
	{
		if(!isset($this->hasOne['UserTotal']))
		{
			$this->bindModel(array('hasOne'=>array("UserTotal")));
		}
		$this->UserTotal->recursive=-1;
		$totals=$this->UserTotal->findByUserId($user_id);
		return $totals['UserTotal'];
	}
	
	/*
	 * Resets counter of total received comments for each user
	*/
	function resetCommentsReceived($user_id)
	{
		$my_drobes=$this->Drobe->find('list',array('conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		$my_drobes=array_keys($my_drobes);
		$total_received_comments=$this->Rate->find('count',array('conditions'=>array('Rate.drobe_id'=>$my_drobes,"Rate.comment NOT LIKE "=>"")));
		$this->updateUserTotalCounter($user_id,'feedback_received',$total_received_comments);
	}
	
	/*
	 * Resets counter of total comments for each drobe in drobe table
	*/
	function resetTotalComments($user_id)
	{
		$my_drobes=$this->Drobe->find('list',array('conditions'=>array('Drobe.user_id'=>$user_id,'Drobe.deleted'=>0,'Drobe.rate_status'=>'open')));
		$my_drobes=array_keys($my_drobes);
		foreach ($my_drobes as $drobe)
		{
			$total_received_comments=$this->Rate->find('count',array('conditions'=>array('Rate.drobe_id'=>$drobe,"Rate.comment NOT LIKE "=>"")));
			$this->Drobe->updateAll(array('Drobe.total_comments'=>"'".$total_received_comments."'"),array('Drobe.id'=>$drobe));
		}
	
	}
	
}
?>