<?php 
class RequestForLabel extends AppModel
{
	var $name="RequestForLabel";
	
	var $validate = array(
		'reason_for'=>array(
			'notEmpty'=>array(
				'rule'=>'notEmpty',
				'required'=>true,		
				'message'=>"Reason for request is required"
			)
		),
		'email'=>array(
				'notEmpty'=>array(
						'rule'=>'notEmpty',
						'message'=>'Email id is required'
						),
				'emailid'=>array(
						'rule'=>'email',
						'required'=>true,
						'message'=>'Enter valid email id'
						)
				)	
	);
}
?>