<?php 
class Page extends AppModel
{
	var $name="Page";
	var $validate=array(
		'page_name'=>array(
			'unique'=>array(
				'rule'=>'isUnique',
				'on'=>'create',
				'message'=>'Page name is already exist please add nother page name'
			),
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Page Name field is required'
			)
		),
		'page_title'=>array(
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Page Title is required'
			)
		),
		'page_content'=>array(
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Page Content name is required'
			)
		),
	);
	function updateCache()
	{
		$pages=Cache::read('content_pages');
		if(!empty($pages))
		{
			Cache::drop('content_pages');
		}
		$pages=$this->find('all');
		$content=array();
		foreach($pages as $page)
		{
			$content[$page['Page']['page_name']]=array('page_title'=>$page['Page']['page_title'],'page_content'=>$page['Page']['page_content']);
		}
		Cache::write('content_pages',$content);
	}
}
?>