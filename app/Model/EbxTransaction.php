<?php 
class EbxTransaction extends AppModel
{
	var $name="EbxTransaction";
	var $validate=array(
		'points'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Points is required"
			),
			'numeric'=>array(
				'rule'=>"numeric",
				'required'=>true,
				'message'=>"Points must be numeric"
			),
			'notZero'=>array(
				'rule'=>array('comparison', '!=', 0),
				'required'=>true,
				'message'=>"Points is not equal to zero"
			)
		),
		'description'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Description is required"
			)
		)
	);
	
	function afterSave($created)
	{
		if($this->data['EbxTransaction']['user_id']>0)
		{
			$user_total_model = ClassRegistry::init('UserTotal');
			$userTotalData=$user_total_model->find('first',array('conditions'=>array('UserTotal.user_id'=>$this->data['EbxTransaction']['user_id'])));
			if($userTotalData['UserTotal']['earned_ebx_point']>=2000)
			{
				$user_model = ClassRegistry::init('User');
				$user_model->id=$this->data['EbxTransaction']['user_id'];
				$user_model->saveField('star_user',1);
			}
			
		}
	}
}
?>