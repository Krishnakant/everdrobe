<?php 
class Admin extends AppModel
{
	var $name="Admin";
	var $validate=array(
		'email'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Email is required"
			),
			'email'=>array(
				'rule'=>"email",
				'required'=>true,
				'message'=>"Enter valid email id"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Email id is already taken"
			)
		),
		'username'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'message'=>"Username is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Username is already taken"
			)
		),
		'password'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'message'=>"Password is required"
			),
		),
		'current_password'=>array(
			'rule' => array('minLength', '8'),
			'message' => 'Current password must be minimum 8 character long'
		),
		'new_password'=>array(
			'rule' => array('minLength', '8'),
			'message' => 'New password must be minimum 8 character long'
		),
		'confirm_password'=>array(
			'identicalFieldValues' => array(
		        'rule' => array('identicalFieldValues', 'new_password' ),
		        'message' => 'New password and confirm password must be match'
			)
		),
		'first_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'message'=>"First Name is required"
			)
		),
		'last_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'message'=>"Last Name is required"
			)
		)
	);
	function identicalFieldValues( $field=array(), $compare_field=null )
	{
		foreach( $field as $key => $value ){
			$v1 = $value;
			$v2 = $this->data[$this->name][ $compare_field ];
			if($v1 !== $v2) {
				return FALSE;
			} else {
				continue;
			}
		}
		return TRUE;
	}
}
?>