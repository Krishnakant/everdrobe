<?php 
class Brand extends AppModel
{
	var $name="Brand";
	var $validate=array(
		'brand_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Brand Name is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Brand is already taken"
			)
		)
	);
	function _updateBrandCache()
	{
		$flagBrands=Cache::read('brand_list');
		if(!empty($flagBrands))
		{
			Cache::drop('brand_list');
		}
// 		$brand_list=$this->find('list',array('conditions'=>array('Brand.status'=>"active"),'order'=>"Brand.id ASC"));

		$brand_list=$this->find('list',array('fields'=>array('Brand.id','Brand.brand_name'),'conditions'=>array('Brand.status'=>"active"),'order'=>"Brand.id ASC"));;
		Cache::write('brand_list',$brand_list);
	}
}
?>