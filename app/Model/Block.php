<?php 
class Block extends AppModel
{
	var $name="Block";
	var $displayField = 'ip_address';
	var $validate=array(
		'ip_address'=>array(
			'clientip' => array(
				'rule' => 'checkipcidr',
				'message' => 'Please enter a valid IP address or CIDR notation'
			),
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"IP Address is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"IP Address already exist in block list"
			)
		)
	);
	function checkipcidr()
	{
		if(isset($this->data[$this->alias]['ip_address']))
		{
			$ip_part=explode("/",$this->data[$this->alias]['ip_address']);
			if(count($ip_part)>2) return false;
			$ip_addr=ip2long($ip_part[0]);
			if($ip_addr===false) return false;
			if(isset($ip_part[1]) && $ip_part[1]!="")
			{
				$mask=intval($ip_part[1]);
				if(trim($ip_part[1])!=$mask || $mask>32 || $mask<0)
				{
					return false;
				}
			}
		}
		return true; 
	}
	function _updateBlockCache()
	{
		$flagCategories=Cache::read('block_list');
		if(!empty($flagCategories))
		{
			Cache::drop('block_list');
		}
		Cache::write('block_list',$this->find('list',array('conditions'=>array('Block.status'=>"active"))));
	}
}
?>