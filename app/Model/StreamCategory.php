<?php 
class StreamCategory extends AppModel
{
	var $name="StreamCategory";
	var $belongsTo=array("User");
	
	public function _loadUsersCategory($user_id)
	{
		$category=array();
		$category['gender_filter']=$this->User->UserSetting->field('gender_filter',array('user_id'=>$user_id));
		$category['categories']=$this->find('list',array('fields'=>array('StreamCategory.category_id'),'conditions'=>array('StreamCategory.user_id'=>$user_id)));
		if(!(count($category['categories'])>0)) $category['categories']="ALL";
		return $category;
	}
}
?>