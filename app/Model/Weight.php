<?php 
class Weight extends AppModel
{
	var $name="Weight";
	
	var $validate=array(
		'weight'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Weight is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Weight is already taken"
			)
		),
		'price'=>array(
				'required'=>array(
						'rule'=>"notEmpty",
						'required'=>true,
						'message'=>"Price is required"
					)
				)	
	);
	function _updateWeightCache()
	{
		$flagWeights=Cache::read('label_weight');
		if(!empty($flagWeights))
		{
			Cache::drop('label_weight');
		}
		//read weight data from database and write in to cache for efficiency use.
		$weight_data = $this->find('all',array('fields'=>array('Weight.weight','Weight.price')),
				array('conditions'=>array('Weight.status'=>"active"),'order'=>"Weight.id ASC"));
		
		$label_weights = array();
		foreach ($weight_data as $weight)
		{
			$label_weights[$weight['Weight']['weight']."lbs"] = $weight['Weight']['weight']."lbs for $".$weight['Weight']['price'];
		}
		
		Cache::write('label_weight',$label_weights);
	}
}
?>