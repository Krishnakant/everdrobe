<?php 
class Category extends AppModel
{
	var $name="Category";
	var $haseMany='Drobes';
	var $displayField = 'category_name';
	var $validate=array(
		'category_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Category Name is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Category is already taken"
			)
		)
	);
	function _updateCategoryCache()
	{
		$flagCategories=Cache::read('drobe_category');
		if(!empty($flagCategories))
		{
			Cache::drop('drobe_category');
		}
		Cache::write('drobe_category',$this->find('list',array('conditions'=>array('Category.status'=>"active"),'order'=>"Category.order ASC")));
	}
}
?>