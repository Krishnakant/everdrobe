<?php 
class SellProfile extends AppModel
{
	var $name="SellProfile";
	var $validate=array(
			'paypal_email'=>array(
					'notEmpty'=>array(
							'rule' => 'notEmpty',
							'message'=> 'Email id is required'
					),
					'email'=>array(
							'rule'=>"email",
							'required'=>true,
							'message'=>'Enter valid email id'
					)
			),
			'full_name'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'Your check payable name is required.'
					)
						
			),'street1'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'Street1 is required.'
					)
			),
			/*'street2'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'street2 is required.'
					)
			),*/
			'city'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'City is required.'
					)
			),
			'state'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'State is required.'
					)
			),
			'zip'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'Zip is required.'
					)
			),
			'deposit_name'=>array(
					'notEmpty'=>array(
							'rule' => 'notEmpty',
							'message'=> 'Deposit name is required'
					)
			),
			'account_no'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'Account number is required.'
					)
			
			),
			'routing_no'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'Routing number is required.'
					)
			),
			'bank_name'=>array(
					'noEmpty' => array(
							'rule' => 'notEmpty',
							'message' => 'Bank name is required.'
					)
			)
	);
	
	
	
}
?>