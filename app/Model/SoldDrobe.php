<?php 
class SoldDrobe extends AppModel
{
	var $name="SoldDrobe";
	var $belongsTo=array("SellDrobe","Drobe");
	var $validate=array(
			'order_id'=>array(
				'required'=>array(
					'rule'=>"notEmpty",
					'required'=>true,
					'message'=>"Order ID name is required"
				)
			),
		/*	'tracking_number'=>array(
				'required'=>array(
					'rule'=>"notEmpty",
					'required'=>true,
					'message'=>"Tracking Number is required"
				)
			), */
			'paid_amount'=>array(
				'required'=>array(
					'rule'=>"notEmpty",
					'required'=>true,
					'message'=>"Paid Amount to Seller is required"
				)
			)
		);
	
	function afterSave($created)
	{
		parent::afterSave($created);
		$this->updateCounter();
	}
	function afterDelete()
	{
		parent::afterDelete();
		$this->updateCounter();
	}
	function updateCounter()
	{
		/*
		 * here Total paid amount is indicates Paid to Seller, total revenue is indicates total price which revenue for everdrobe
		 */
		$drobe_id=$this->drobe_id;
		$data=$this->find('first',array('fields'=>array('sum(SoldDrobe.paid_amount) as total_paid','sum(SoldDrobe.sell_price) as total_revenue','count(*) sold_items'),'conditions'=>array('SoldDrobe.drobe_id'=>$drobe_id)));
		$this->SellDrobe->updateAll($data[0],array('SellDrobe.drobe_id'=>$drobe_id));
	}
}
?>