<?php 
class Drobe extends AppModel
{
	var $name="Drobe";
	var $belongsTo=array("User","Category");
	var $hasOne=array("DrobeSetting","SellDrobe");
	var $hasMany= array("Rate"=>array("conditions"=>array("Rate.rate !="=>0)),"Flag","Favourite");
	//var $hasMany="Comment";
	var $validate=array();
	function _setValidation()
	{
		$this->validate=array(
			/*'comment'=>array(
					'required'=>array(
					'rule'=>'notEmpty',
					'message'=>"Your comment is required"
				)
			),*/
			'category_id'=>array(
					'required'=>array(
					'rule'=>'notEmpty',
					'message'=>"Select Category"
				)
			)
		);
	}
	function afterSave($created)
	{
		if($created)
		{
			$this->updateRecentCache();
		}
	} 
	function updateRecentCache()
	{
		$this->recursive=-1;
		$drobes=$this->find('all',array("fields"=>array("Drobe.file_name","Drobe.comment"),"conditions"=>array("Drobe.comment !="=>"","Drobe.rate_status"=>"open","Drobe.user_id > "=>0,'Drobe.deleted'=>0),"order"=>array("Drobe.uploaded_on DESC"),"limit"=>2));
		Cache::write('latest_drobes',$drobes);
		$this->recursive=0;
	}
	function updateDrobeRateIndex($drobe_id=null)
	{
		return;
		$this->recursive=-1;
		$rate_index=$this->field('((total_in + 1.9208) / (total_in + total_out) - 
                   1.96 * SQRT((total_in * total_out) / (total_in + total_out) + 0.9604) / 
                          (total_in + total_out)) / (1 + 3.8416 / (total_in + total_out))',array('Drobe.id'=>$drobe_id));
		$this->id=$drobe_id;
		$this->saveField('rate_index', round($rate_index,6));
		$this->recursive=0;
	}
	
	/* Update short url field in table with google shortend url */
	function updateShortUrl($start=null,$length=null)
	{
		if(! $start>0)
			$start=0;
		if(! $length>0)
			$length=5;
		
		$this->recursive=-1;
		/* Get data which are not null in short url and not converted for google shorten url */
		$drobe_data = $this->find('all',array('limit'=>"$start,$length",'order'=>'Drobe.id DESC'));
		foreach ($drobe_data as $data)
		{
			$this->id = $data['Drobe']['id'];
			$url = Router::url(array("controller"=>"drobes","action"=>"rate",$data['Drobe']['unique_id']),true);
			
			/* If already Google shorten url then don't need to convert for shorten url */
			if($data['Drobe']['short_url']=="" || strpos($data['Drobe']['short_url'], "http://goo.gl")===false)
			{
				usleep(1100000);
				$postData = array('longUrl' => $url, 'key' => Configure::read('shorten_url.api_key'));
				$jsonData = json_encode($postData);
				$curlObj = curl_init();
				curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlObj, CURLOPT_HEADER, 0);
				curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
				curl_setopt($curlObj, CURLOPT_POST, 1);
				curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
				$response = curl_exec($curlObj);
				
				// Change the response json string to object
				$json = json_decode($response);
				curl_close($curlObj);
				if($json->id=="")
				{
					echo "<pre>";
					echo "ShortUrl ".$this->id."<br>".print_r($json);
					exit;
				}	
				$this->saveField('short_url',$json->id);
			}
			
			
			$url= $data['Drobe']['buy_url_actual'];
			if(strpos($data['Drobe']['buy_url'],"http://goo.gl")===false && $url!="")
			{
				usleep(1100000);
				$postData = array('longUrl' => $url, 'key' => Configure::read('shorten_url.api_key'));
				$jsonData = json_encode($postData);
				$curlObj = curl_init();
				curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url');
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlObj, CURLOPT_HEADER, 0);
				curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
				curl_setopt($curlObj, CURLOPT_POST, 1);
				curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
				$response = curl_exec($curlObj);
				
				// Change the response json string to object
				$json = json_decode($response);
				curl_close($curlObj);
				if($json->id=="")
				{
					echo "<pre>";
					echo "BuyUrl ".$this->id."<br>".print_r($json);
					exit;
				}
				$this->saveField('buy_url',$json->id);
			}
			
			$this->id = null;
		}
	}
	
}
?>