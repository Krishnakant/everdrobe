<?php 
class FlagCategory extends AppModel
{
	var $name="FlagCategory";
	var $displayField = 'category_name';
	var $validate=array(
		'category_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Flag category Name is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Flag category is already taken"
			)
		)
	);
	function _updateFlagCache()
	{
		$flagCategories=Cache::read('flag_category');
		if(!empty($flagCategories))
		{
			Cache::drop('flag_category');
		}
		Cache::write('flag_category',$this->find('list',array('conditions'=>array('FlagCategory.status'=>"active"))));
	}
}
?>