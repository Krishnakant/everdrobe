<?php 
class Country extends AppModel
{
	var $name="Country";
	var $hasMany='Province';
	var $displayField = 'country_name';
	var $validate=array(
		'country_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Country name is required"
			),
		/*	'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Country name is already exist"
			)*/
		),
		'short_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Country name is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Country name is already exist"
			)
		)
	);
	
	function _updateCountryCache()
	{
		$country_cache=Cache::read('country_cache');
		if(!empty($country_cache))
		{
			Cache::drop('country_cache');
		}
		$this->unbindModel(array("hasMany"=>array("Province")));
		Cache::write('country_cache',$this->find('all'));
	}
	
}
?>