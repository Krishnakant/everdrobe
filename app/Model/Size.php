<?php 
class Size extends AppModel
{
	var $name="Size";
	var $displayField = 'size_name';
	var $validate=array(
		'size_name'=>array(
			'required'=>array(
				'rule'=>"notEmpty",
				'required'=>true,
				'message'=>"Size Name is required"
			),
			'isUnique'=>array(
				'rule'=>"isUnique",
				'on'=>'create',
				'message'=>"Size is already taken"
			)
		)
	);
	function _updateSizeCache()
	{
		$flagCategories=Cache::read('size_list');
		if(!empty($flagCategories))
		{
			Cache::drop('size_list');
		}
		$size_list=$this->find('list',array('conditions'=>array('Size.status'=>"active"),'order'=>"Size.order ASC"));
		/* Read other size id from configure file core.php */
		$other_size_id = Configure::read('other_size_id');
		$size_list[$other_size_id]="N/A";
		ksort($size_list);
		Cache::write('size_list',$size_list);
	}
}
?>