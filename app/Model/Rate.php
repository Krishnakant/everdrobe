<?php 
class Rate extends AppModel
{
	var $name="Rate";
	var $validate=array();
	
	function afterSave($created)
	{
		if(!$created)
		{
			if(isset($this->data['Rate']['drobe_id']))
			{
				$this->updateRateCounter($this->data['Rate']['drobe_id']);
			}
		}
		else
		{
			
		}
	}
	function updateRateCounter($drobe_id)
	{
		$drobeData=array();
		$drobeData['total_comments']=$this->find('count',array("conditions"=>array("Rate.drobe_id"=>$drobe_id,"Rate.comment !="=>"")));
			
		$vote_summary=$this->find('all',array('fields'=>array("Rate.rate as rate","count(*) as total"),"conditions"=>array('Rate.drobe_id'=>$drobe_id,"Rate.rate !="=>0),"group"=>array('Rate.rate')));
		$drobeData['total_rate']=0;
		$drobeData['total_in']=0;
		$drobeData['total_out']=0;
		foreach ($vote_summary as $rate)
		{
			if($rate['Rate']['rate']=="-1") $drobeData['total_out']=$rate[0]['total'];
			else if($rate['Rate']['rate']=="1") $drobeData['total_in']=$rate[0]['total'];
			$drobeData['total_rate'] = $drobeData['total_rate'] + $rate[0]['total'];
		}
		
		App::import('model','Drobe');
		$drobe = new Drobe();
		$drobe->updateAll($drobeData,array('Drobe.id'=>$drobe_id));
		$drobe->updateDrobeRateIndex($drobe_id);
	}
}
?>