<?php 
class SellDrobeImage extends AppModel
{
	var $name="SellDrobeImage";
	var $belongsTo=array("SellDrobe");
	var $validate=array();
	
	/*
	 * Remove additional image record  with image from folder
	 */
	function removeImageRecord($id)
	{
		$file_name=$this->field('file_name',array('id'=>$id));
		$this->deleteAll(array('SellDrobeImage.id'=>$id));
		$image_path=WWW_ROOT . Configure::read('sell_drobe.upload_dir') .DS . $file_name;
		if(is_file($image_path)) unlink($image_path);
		$image_path=WWW_ROOT . Configure::read('sell_drobe.thumb.upload_dir') .DS . $file_name;
		if(is_file($image_path)) unlink($image_path);
	}
	
	/*
	 * Removing deleted images
	 */
	function removeDeletedImages($files,$sell_drobe_id)
	{
		$new_file_names=array();
		foreach($files as $shell_image)
		{
			$new_file_names[]=$shell_image['file_name'];
		}
		$this->recursive= -1;
		$file_names=$this->find('all',array('fields'=>array('file_name'),'conditions'=>array('SellDrobeImage.sell_drobe_id'=>$sell_drobe_id)));
		foreach($file_names as $file)
		{
			if(!in_array($file['SellDrobeImage']['file_name'],$new_file_names))
			{
				$image_path=WWW_ROOT . Configure::read('sell_drobe.upload_dir') .DS . $file['SellDrobeImage']['file_name'];
				if(is_file($image_path)) unlink($image_path);
				$image_path=WWW_ROOT . Configure::read('sell_drobe.thumb.upload_dir') .DS . $file['SellDrobeImage']['file_name'];
				if(is_file($image_path)) unlink($image_path);
			}
		}
		
	}
	
	
	
	
	/*
	 * Removing deleted images for edit additional images webservice.
	*/
	
	function removeDeletedImagesService($sell_drobe_image_ids,$sell_drobe_id)
	{
		$this->recursive = -1;
		$sell_drobe_image_db_ids = $this->find('all',array('fields'=>array('id'),'conditions'=>array('SellDrobeImage.sell_drobe_id'=>$sell_drobe_id)));
		
		// retrieve file name based on sellDrobeImage id and unlink file path		
		foreach($sell_drobe_image_db_ids as $sell_drobe_image)
		{
			$id = $sell_drobe_image['SellDrobeImage']['id'];
			if(in_array($id,$sell_drobe_image_ids))
			{				
				$file = $this->field('file_name',array('id'=>$id));
				$image_path=WWW_ROOT . Configure::read('sell_drobe.upload_dir') .DS . $file;
				if(is_file($image_path)) unlink($image_path);
				$image_path=WWW_ROOT . Configure::read('sell_drobe.thumb.upload_dir') .DS . $file;
				if(is_file($image_path)) unlink($image_path);
			}
		}
	}
	
	
	/*
	 * Remove additional image record  with image from folder for edit additional images webservice.
	*/
	function removeImageRecordService($sell_drobe_image_ids)
	{
		foreach ($sell_drobe_image_ids as $sell_drobe_image_id)
		{
			$this->deleteAll(array('SellDrobeImage.id'=>$sell_drobe_image_id));
		}
	}
}
?>