<?php 
class Setting extends AppModel
{
	var $name="Setting";
	var $validate=array(
		'setting_name'=>array(
			'unique'=>array(
				'rule'=>'isUnique',
				'on'=>'create',
				'message'=>'Setting name id already exist please set new setting name'
			),
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Action field is required'
			)
		),
		'value'=>array(
			'notEmpty'=>array(
				'rule' => 'notEmpty',
				'message'=> 'Setting value is required'
			)
		)
	);
	function afterSave($created)
	{
		$this->updateSettingCache();
	}
	function updateSettingCache()
	{
		$settings_chache=Cache::read('settings_cache');
		if(!empty($settings_chache))
		{
			Cache::drop('settings_cache');
		}
		Cache::write('settings_cache',$this->find('list',array("fields"=>array("Setting.setting_name","Setting.value"))));
	}
	
}
?>