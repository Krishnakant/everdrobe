<h2>EBX Redeem Request</h2>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th width="50px"><?php echo $this->Paginator->sort('EbxTransaction.id','#');?></th>
			<th width="50px"><?php echo $this->Paginator->sort('EbxTransaction.user_id','User ID');?></th>
			<th width="120px"><?php echo $this->Paginator->sort('EbxTransaction.created_on','Date');?></th>
			<th>Description</th>
		</tr>
		<?php 
		foreach($data as $user)
		{
			?>
			<tr class="<?php echo $user['EbxTransaction']['id']; ?>">
				<td><?php echo $user['EbxTransaction']['id'];?></td>
				<td><?php echo $user['EbxTransaction']['user_id'];?></td>
				<td><?php echo date(Configure::read('datetime.display_format')." H:i",strtotime($user['EbxTransaction']['created_on']));?></td>
				<td><?php echo $user['EbxTransaction']['description'];?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>