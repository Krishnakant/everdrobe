<h2>Edit EBX: <?php echo $userData['User']['first_name']." ".$userData['User']['last_name']." (".$userData['User']['username'].")";?></h2>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
<?php echo $this->Html->link("Back to EBX Transactions", array("controller"=>"ebx_transactions","action"=>"user","admin"=>true,$userData['User']['id']))." "; ?>
</div>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="spacer"></div>
<div class="page_8_middel">
<div class="left" style="clear: none; width:500px">
<fieldset><legend>Manage EBX points</legend>
<?php
echo $this->Form->create('EbxTransaction');
echo "<div class='spacer'></div>";
echo "Current EBX : ".$userData['UserTotal']['current_ebx_point'];
echo "<div class='spacer'></div>";
echo $this->Form->input('points', array('before' => '<div>','after' => '</div>', 'between' => '', 'separator' => '</div><div> ','legend'=>false,"label"=>"Add/Remove EBX point",'type' => 'text'));
echo $this->Form->input('description');
echo $this->Form->hidden('user_id',array('value'=>$userData['User']['id']));
echo $this->Form->hidden('transaction_type',array('value'=>"admin"));
echo $this->Form->submit('Update',array("class"=>"green_btn"));
echo "<div class='spacer'></div>";
echo $this->Form->end();
?>
</fieldset>
</div>
</div>