<h2><?php echo $username;?> EBX Transactions</h2>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
<?php echo $this->Html->link("Add/Remove EBX point", array("controller"=>"ebx_transactions","action"=>"manage_ebx","admin"=>true,$userid))." "; ?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th width="50px"><?php echo $this->Paginator->sort('EbxTransaction.id','#');?></th>
			<th width="120px"><?php echo $this->Paginator->sort('EbxTransaction.created_on','Date');?></th>
			<th>Description</th>
			<th width="75px">Point</th>
			<th width="75px">Balance</th>
		</tr>
		<?php 
		foreach($data as $user)
		{
			?>
			<tr class="<?php echo $user['EbxTransaction']['id']; ?>">
				<td><?php echo $user['EbxTransaction']['id'];?></td>
				<td><?php echo date(Configure::read('datetime.display_format')." H:i",strtotime($user['EbxTransaction']['created_on']));?></td>
				<td><?php echo $user['EbxTransaction']['description'];?></td>
				<td><?php echo $user['EbxTransaction']['points'];?></td>
				<td><?php echo $user['EbxTransaction']['current_balance'];?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>