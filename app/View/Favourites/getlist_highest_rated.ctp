<?php 
if(count($data)>0)
{
	$i=0;
	if(isset($this->params->pass[0])) $link_arr=array("controller"=>"drobes","action"=>"rate","rated","favourite",$this->params->pass[1]);
	else $link_arr=array("controller"=>"drobes","action"=>"rate",$this->params->pass[1],"favourite");
	foreach($data as $drobe){
		
		$original_sell_price = "";
		$div = "";
		if($drobe['SellDrobe']['sell_price']<$drobe['SellDrobe']['original_sell_price'])
		{
			$original_sell_price = "$".$drobe['SellDrobe']['original_sell_price'];
		}
			
		//if featured_drobe option is on then and then it will display price size and buy button.
		if($drobe['Drobe']['featured']==1 && $drobe['Drobe']['buy_url']!="" && Configure::read('setting.featured_drobe')=="on")
		{
			if($drobe['Drobe']['post_type']=='sell')
			{
				//set Buy button, price and size if the drobe is post for sell
				$div = "<div class='sell_drobe_detail'><span class='price_detail'>$".
								"<b>".$drobe['SellDrobe']['sell_price']."</b>".
								"&nbsp;<strike><b>".$original_sell_price."</b></strike></span>".
								"<span class='size_detail'>size:<b>".$drobe['SellDrobe']['size_name']."</b></span>".
								"<div class='buy_btn'><div class='buy_most_recent'><span value=".$drobe['Drobe']['buy_url']."></span><b>Buy</b></div> </div>".
						"</div>";
			}
		}
		
	 echo  $this->Html->link(
	 	$div.$this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],
	 		array('alt'=>$drobe['Drobe']['comment'],'height'=>Configure::read('drobe.thumb.height'), 'width'=>Configure::read('drobe.thumb.width'))).($drobe['Drobe']['is_highest_rated']>0 ? "<div class='highest-rated-drobe'></div>":""),
	 		array_merge(array($drobe['Drobe']['unique_id']),$link_arr),
	 		array("border"=>0,'escape'=>false,"class"=>"drobe_thumb","rel"=>$last_faved_on)); 
	}
}
else
{
	if(!isset($this->params->named['page']) || $this->params->named['page']==1)
	{
	?>
		<div class="spacer"></div>
		<p class="empty_message">Your faves will be here once you heart a few, click on rate drobes (make a button) to get started</p>
	<?php
	} 
}
?>

<script>
$(document).ready(function(){
	//Click on buy button it open new window for displaying that item in shopify.
	$(".highest_rated").click(function(e)
    {
	    javascript:window.open($(this).find('span').attr('value'),"_blank");
	    return false;   
    });
});
</script>