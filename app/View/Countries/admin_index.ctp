<h2>Countries List</h2>
<div class="actions right">
<?php echo $this->Html->link('Add new',array('controller'=>'countries','action'=>'add','admin'=>true))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th>Country Name</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($data as $country)
		{
			?>
			<tr>
				<td><?php echo $country['Country']['id'];?></td>
				<td><?php echo $country['Country']['country_name'];?></td>
				<td><?php echo $country['Country']['status'];?></td>
				<td>
				<div class="actions">
				<?php 
					echo $this->Html->link('Edit',array('controller'=>'countries','action'=>'edit',$country['Country']['id'],'admin'=>true)). " "; 
					echo $this->Html->link('Delete',array('controller'=>'countries','action'=>'delete',$country['Country']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'))." ";
					echo $this->Html->link('View Provinces',array('controller'=>'provinces','action'=>'index',$country['Country']['id'],'admin'=>true));
				?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>