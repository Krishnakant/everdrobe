<h2>Edit : <?php echo $this->data['Country']['country_name']?></h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
echo $this->Form->create("Country");
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("short_name");
echo $this->Form->input("country_name");
echo $this->Form->input('status', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Category Status",'options' => array("active"=>"Active","inactive"=>"Inactive"),'type' => 'radio','default'=>'active'));
echo $this->Form->hidden('id');
?>
<div class="spacer"></div>
<?php
echo $this->Form->submit('Edit');
echo $this->Form->end();
?>