<h2>Edit : <?php echo $this->data['FlagCategory']['category_name']?></h2>
<div class="spacer"></div>
<?php 
echo $this->Form->create("FlagCategory");
echo $this->Form->input("category_name");
echo $this->Form->input('status', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Category Status",'options' => array("active"=>"Active","inactive"=>"Inactive"),'type' => 'radio','default'=>'active'));
echo $this->Form->hidden('id');
?>
<div class="spacer"></div>
<?php
echo $this->Form->submit('Edit');
?>