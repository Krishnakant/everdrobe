<h2>Flag Categories List</h2>
<div class="actions right">
<?php echo $this->Html->link('Add New Flag Category',array('action'=>'add','admin'=>true))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th>Flag Category Name</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($data as $category)
		{
			?>
			<tr>
				<td><?php echo $category['FlagCategory']['id'];?></td>
				<td><?php echo $category['FlagCategory']['category_name'];?></td>
				<td><?php echo $category['FlagCategory']['status'];?></td>
				<td>
				<div class="actions">
				<?php 
					echo $this->Html->link('Edit',array('action'=>'edit',$category['FlagCategory']['id'],'admin'=>true)). " "; 
					echo $this->Html->link('Delete',array('action'=>'delete',$category['FlagCategory']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'));
				?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
