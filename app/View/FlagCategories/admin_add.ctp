<h2>Add New Flag Catgory</h2>
<div class="spacer"></div>
<?php
echo $this->Form->create("FlagCategory");
echo $this->Form->input("category_name");
echo $this->Form->input('status', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Category Status",'options' => array("active"=>"Active","inactive"=>"Inactive"),'type' => 'radio','default'=>'active'));

?>
<div class="spacer"></div>
<?php
echo $this->Form->submit('Submit');
echo $this->Form->end();
?>