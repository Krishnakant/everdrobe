<h2>Broadcast Message Report</h2>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
<?php 
echo $this->Html->link("Send Broadcast Message", array("controller"=>"users","action"=>"broadcast","admin"=>true)).' &nbsp;';
echo $this->Html->link('Clear Log',array('controller'=>'NotificationQueues','action'=>'clear_log'),'','Are you sure to clear logs?');
?>
</div>
<div class="spacer"></div>
<div class="search_box" >
<form action="" method="get">                          
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Seach: <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Keyword, First Name,Last Name or Device ID" style="display: inline; width: 400px !important; " /> 
<input type="submit" value="Search"  style=""/>  
<input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('NotificationQueue.id','#');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name','Send To');?></th>
			<th><?php echo $this->Paginator->sort('User.username','Send To User Name')?>
			<th><?php echo $this->Paginator->sort('NotificationQueue.message','Message');?></th>
			<th><?php echo $this->Paginator->sort('NotificationQueue.device_token','Device ID');?></th>
			<th><?php echo $this->Paginator->sort('NotificationQueue.sent','Sent');?></th>
			<th><?php echo $this->Paginator->sort('NotificationQueue.response','Response');?></th>
		</tr>
		<?php 
		foreach($data as $user)
		{
			?>
			<tr>
				<td><?php echo $user['NotificationQueue']['id'];?></td>
				<td><?php echo $this->Html->link($user['User']['first_name'].' '.$user['User']['last_name'],array("controller"=>"users","action"=>"view",$user['User']['id'],"admin"=>true)); ?></td>
				<td><?php echo $user['User']['username'];?>
				<td><?php echo $user['NotificationQueue']['message'];?></td>
				<td><?php echo substr($user['NotificationQueue']['device_token'], 0, 10) . '...' . substr($user['NotificationQueue']['device_token'], -10);?></td>
				<td><?php echo $user['NotificationQueue']['sent'];?></td>
				<td><?php echo $user['NotificationQueue']['response'];?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
	<?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="user_options" style="display: none;">
	<ul class="submenu actions">
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"view","admin"=>true)); ?>  class="view_detail_link">View Detail</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"edit","admin"=>true)); ?>  class="remove_link">Edit Detail</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","block","admin"=>true)); ?> onclick="return confirm('Are you sure to block this user?');" class="block_link">Block</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","activate","admin"=>true)); ?> onclick="return confirm('Are you sure to activate this user?');" class="activate_link">Activate</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","deactivate","admin"=>true)); ?> onclick="return confirm('Are you sure to deactivate this user?');" class="deactivate_link">Deactivate</a></li>
	</ul>
	<div class="clear"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	});
});
</script>