<h2>Block List</h2>
<div class="actions right">
<?php echo $this->Html->link('Add New in Block List',array('action'=>'add','admin'=>true))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th>IP Address</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($data as $category)
		{
			?>
			<tr>
				<td><?php echo $category['Block']['id'];?></td>
				<td><?php echo $category['Block']['ip_address'];?></td>
				<td><?php echo $category['Block']['status'];?></td>
				<td>
				<div class="actions">
				<?php 
					echo $this->Html->link('Edit',array('action'=>'edit',$category['Block']['id'],'admin'=>true)). " "; 
					echo $this->Html->link('Delete',array('action'=>'delete',$category['Block']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'));
				?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
