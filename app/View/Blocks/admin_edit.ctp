<h2>Edit : <?php echo $this->data['Block']['ip_address']?></h2>
<div class="spacer"></div>
<?php 
echo $this->Form->create("Block");
echo $this->Form->input("ip_address");
echo $this->Form->input('status', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Category Status",'options' => array("active"=>"Active","inactive"=>"Inactive"),'type' => 'radio','default'=>'active'));
echo $this->Form->hidden('id');
?>
<div class="spacer"></div>
<?php
echo $this->Form->submit('Edit');
?>