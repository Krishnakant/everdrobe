<h2>System Logs</h2>
<?php 
$files=scandir(LOGS);
foreach ($files as $file)
{
	if($file=="." || $file==".." || $file=="empty") continue;
	$ext=strtolower(array_pop(explode(".",$file)));
	if($ext!="log") continue;
	$file_name=str_replace(".".$ext,"",$file);
	if(file_exists(LOGS."error.log"))
	{
	?>
	<div>
	<h4 class="left" style="margin: 0; padding: 0"><?php echo Inflector::humanize($file_name)." Log ( Size ".round(filesize(LOGS.$file)/1024,2)."KB)"; ?></h4>
		<div class="actions right"  style="margin: 0; padding: 0">
		<?php 
			echo $this->Html->link("Clear ".Inflector::humanize($file_name)." Log", array('action' =>'clear',"admin"=>true,$file_name),array("class"=>"button btnred")); 
		?>
		</div>
		<div class="spacer"></div>
		<div style="height: 200px; overflow: auto; padding: 5px; border: 1px solid #AAAAAA"><pre><?php echo file_get_contents(LOGS.$file); ?></pre></div>
	</div>
	<div class="spacer"></div>
	<?php
	}
}
?>
<div class="spacer"></div>
<h2>System Cache</h2>
<div class="actions right"  style="margin: 0; padding: 0">
		<?php 
			echo $this->Html->link("Reset All Cache", array('action' =>'clean_cache',"admin"=>true),array("class"=>"button btnred")); 
		?>
		</div>
<table>
<tr>
<th>Cache Name</th>
<th>Size</th>
<th>Action</th>
</tr>
<?php 
$files=scandir(TMP."cache".DS);
foreach ($files as $file)
{
	
	if($file=="." || $file==".." || $file=="empty") continue;
	if(is_file(TMP."cache".DS.$file))
	{
		$lable=Inflector::humanize(trim(str_replace("cake","",$file)));
	?>
	<tr>
		<td><?php echo $lable." Cache"; ?></td>
		<td width="100px"><?php echo sprintf("%0.2f KB",round(filesize(TMP."cache".DS.$file)/1024,2)); ?></td>
		<td width="100px">
			<div class="actions left"  style="margin: 0; padding: 0">
			<?php 
				echo $this->Html->link("Reset Cache", array('action' =>'clear_cache_file',"admin"=>true,$file),array("class"=>"button btnred")); 
			?>
			</div>
		</td>
	</tr>
	<?php
	}
}
?>
</table>
<div class="spacer"></div>
