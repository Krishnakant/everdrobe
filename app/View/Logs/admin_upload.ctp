<h2>Drobe Upload Logs</h2>
<?php 
$file_name="drobe_upload";
$file=LOGS."drobe_upload.log";
	if(file_exists($file))
	{
	?>
	<div>
		<div class="actions right"  style="margin: 0; padding: 0">
		<?php 
			echo $this->Html->link("Clear ".Inflector::humanize($file_name)." Log", array('action' =>'clear',"admin"=>true,$file_name),array("class"=>"button btnred")); 
		?>
		</div>
		<div class="spacer"></div>
		<div style="height: 600px; overflow: auto; padding: 5px; border: 1px solid #AAAAAA"><pre><?php echo file_get_contents($file); ?></pre></div>
	</div>
	<div class="spacer"></div>
	<?php
	}
	else
	{
		?>
		<div>File not generated </div>
		<?php 
	}
?>
