<h2>Brand List</h2>
<div class="actions right">
<?php 
echo $this->Html->link('Add New Brand',array('controller'=>'brands','action'=>'add','admin'=>true),array("class"=>"button"))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th><?php echo $this->Paginator->sort('Brand.brand_name','Brand Name');?></th>
			<th>Status</th>
			<th>Drobes</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($data as $brand)
		{
			?>
			<tr>
				<td><?php echo $brand['Brand']['id'];?></td>
				<td><?php echo $brand['Brand']['brand_name'];?></td>
				<td><?php echo $brand['Brand']['status'];?></td>
				<td><?php echo $brand[0]['drobes'];?></td>
				<td>
				<div class="actions">
					<?php 
						echo $this->Html->link('Edit',array('controller'=>'brands','action'=>'edit',$brand['Brand']['id'],'admin'=>true)). " ";
						if($brand['Brand']['id']==configure::read('other_brand_id'))
						{
							echo $this->Html->link('Delete','javascript:void(0);',array('style'=>'opacity:0.5'))."&nbsp;&nbsp;&nbsp;";
						}
						else
						{
							echo $this->Html->link('Delete',array('controller'=>'brands','action'=>'delete',$brand['Brand']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'))."&nbsp;&nbsp;&nbsp;";
						} 
					//	echo $this->Html->link('Delete',array('controller'=>'brands','action'=>'delete',$brand['Brand']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'))."&nbsp;&nbsp;&nbsp;";					
					?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
