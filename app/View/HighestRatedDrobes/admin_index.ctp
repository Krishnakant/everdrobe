<h2>Everdrobe Winners Drobe</h2>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('HighestRatedDrobe.id','#');?></th>
			<th><?php echo $this->Paginator->sort('HighestRatedDrobe.created_on','Date');?></th>
			<th>Drobe Image</th>
			<th><?php echo $this->Paginator->sort('Drobe.category_id','Category');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name','Drobe User');?></th>
			<th><?php echo $this->Paginator->sort('User.username','Drobe UserName')?>
			<th><?php echo $this->Paginator->sort('Drobe.uploaded_on','Drobe Posted On');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.total_favs','Total Favs');?></th>
			<th><?php echo $this->Paginator->sort('views','Total Views');?></th>
			<th><?php echo $this->Paginator->sort('rate','Total Votes');?></th>
			<th><?php echo $this->Paginator->sort('in','Total Cute');?></th>
			<th><?php echo $this->Paginator->sort('out','Total Hot');?></th>
			<th><?php echo $this->Paginator->sort('index','Rate index');?></th>
			
		</tr>
		<?php 
		foreach($data as $user)
		{
			?>
			<tr class="<?php echo $user['HighestRatedDrobe']['id']; ?>">
				<td><?php echo $user['HighestRatedDrobe']['id'];?></td>
				<td><?php echo date(Configure::read('datetime.display_format')." H:i",strtotime($user['HighestRatedDrobe']['created_on']));?></td>
				<td><?php echo $this->Html->link($this->Html->image('/drobe_images/thumb/'.$user['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0,'width'=>50,'id'=>"profile_image" ,"alt"=>"drobe prview Image")),$this->Html->url('/drobe_images/'.$user['Drobe']['file_name'],true),array("escape"=>false,"class"=>"example8"));?></td>
				<td align="center"><?php echo $user['Category']['category_name'];?></td>
				<td><?php echo $this->Html->link($user['User']['first_name']." ".$user['User']['last_name'],array("controller"=>"users","action"=>"view","admin"=>true,$user['User']['id']));?></td>
				<td><?php echo $user['User']['username'];?></td>
				<td><?php echo $this->Time->niceShort($user['Drobe']['uploaded_on']);?></td>
				<td align="center"><?php echo $user['Drobe']['total_favs'];?></td>
				<td align="center"><?php echo $user['HighestRatedDrobe']['views'];?></td>
				<td align="center"><?php echo $user['HighestRatedDrobe']['rate'];?></td>
				<td align="center"><?php echo $user['HighestRatedDrobe']['in'];?></td>
				<td align="center"><?php echo $user['HighestRatedDrobe']['out'];?></td>
				<td align="center"><?php echo $user['HighestRatedDrobe']['index'];?></td>
				
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>