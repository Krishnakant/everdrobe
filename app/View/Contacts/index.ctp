<h2 class="text-center">Contact Us</h2>
<div class="col-sm-6 col-sm-offset-3"> 
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
echo $this->Form->create('Contact',array("id"=>"contact_form"));
echo $this->Form->input("full_name",array("label"=>"Your Name","class"=>"form-control"));
echo $this->Form->input("email_id",array("label"=>"Your Email","type"=>"text","class"=>"form-control"));
echo $this->Form->input("message",array("label"=>"Your Message","class"=>"form-control"));
?>
<div class="spacer"></div>
<div class="left" style="margin: 0; padding: 0; clear: none;">
<?php 
//echo $this->Recaptcha->show();
//echo $this->Recaptcha->error();
echo $this->Recaptcha->display_form('echo');
?>
</div>
<div class="right" style="margin: 0; padding: 0; clear: none;">
<?php 
echo $this->Form->submit("Submit",array("class"=>"btn btn-success"));
?>
</div>
<?php 
echo $this->Form->end();
?>
</div>