<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <?php if($this->params->params['action']=="rate" && $this->params->params['controller']=="drobes"){ echo 'xmlns:fb="http://ogp.me/ns/fb#"';} ?>>
<head>
<?php echo $this->Html->charset(); ?>
<title>Everdrobe : <?php echo $title_for_layout; ?></title>
<?php 
//echo $this->Html->meta('icon',$this->Html->url('/icon.png'));
echo $this->Html->meta('icon');
echo $this->Html->css(array('style','fixed','sell_drobe','colorbox'));
//comment by @sadikhasan
//echo $this->Html->css(array('style.min','fixed','sell_drobe','bootstrap.min'));
//echo $this->Html->script(array('utilities.min','script'));
/*
 below one line comment by @sadikhasan
 */
//echo $this->Html->script(array('utilities.min','script.min'));
echo $this->Html->script(array('jquery.min','bootstrap','utilities.min','script','jquery.colorbox','feather'));
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
<base href="<?php echo Router::url('/',true);?>">
<?php 
$content_pages=Cache::read('content_pages');
?>
<meta name="description" content="<?php echo $content_pages['meta_description']['page_content']; ?>" >
<meta name="keywords" content="<?php echo $content_pages['meta_keywords']['page_content']; ?>" >
</head>

<body class="demos">
<div id="fix">
<?php echo $this->element('sell-drobe-left-panel')?>
<div id="container" class="sell-drobe-container">
<?php echo $this->Session->flash(); 
 echo $this->fetch('content'); ?>
</div> <!-- #container -->
<div class="clear"></div>
<div class="clear footer_space"></div>
</div>
<div class="clear"></div>
<div id="footer">
<?php 
echo $this->element('footer');
?>
</div>

<?php 
//echo $this->element('sql_dump'); 
echo $content_pages['google_analytics']['page_content'];
if($this->Session->read('new_registration')==true)
{

	echo $content_pages['google_conversion_tracking']['page_content'];
	// delete session of new registration 
	// we can not delete session from Helper so we need session component
	SessionComponent::delete('new_registration');
} 
?>
</body>
</html>
