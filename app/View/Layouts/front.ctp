<?php
$cakeDescription = __d('cake_dev', 'Everdrobe: Welcome');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('front.min');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
	<div class="iphone_pic">
	<?php echo $this->Html->image("iphone_img.png",array("alt"=>"iPhone Image","border"=>"0"));?>
	</div>
	<div class="logo">
	<?php echo $this->Html->image("logo.png",array("alt"=>"Logo","border"=>"0"));?>
	</div>
	<div class="about_everdrobe">
	About Everedrobe
	</div>
	<?php echo $this->Html->link("ENTER Site",array("controller"=>"drobes","action"=>"index"),array("class"=>"button_enter_site"));?>
	<div class="live_timeline">
	Live Time Line
	</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
