<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->Html->charset(); ?>
<title>Everdrobe</title>
<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('front');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
<div id="banner">
	<div id="fix" style="width:500px">
    	<?php echo $this->Html->link("","javascript:void(0)",array("class"=>"logo","style"=>"float:left; width:500px;"));?>
        <div class="company">
        	<div class="apple"></div>
            <div class="android"></div>
        </div>
        <div class="link-box block">
        	<?php 
 			echo $this->fetch('content'); 
 			?>
        </div>
    </div>
</div>

</body>
</html>
