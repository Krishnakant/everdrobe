<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <?php if($this->params->params['action']=="rate" && $this->params->params['controller']=="drobes"){ echo 'xmlns:fb="http://ogp.me/ns/fb#"';} ?>>
<head>
<?php echo $this->Html->charset(); ?>
 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
<title>Outfit Ideas, Selfies, <?php echo $title_for_layout; ?> - Everdrobe.com</title>
<meta name="description" content="Everdrobe.com is a social wardrobe where you can get outfit ideas and instant fashion feedback from real people for your ootd and selfies." />
<?php 
//echo $this->Html->meta('icon',$this->Html->url('/icon.png'));
echo $this->Html->meta('icon');
//echo $this->Html->css(array('style','fixed'));
//commented by sadik for post drobe button css change
//echo $this->Html->css(array('style.min','fixed','bootstrap.min'));
echo $this->Html->css(array('bootstrap.min','font-awesome.min','style.min'));
//echo $this->Html->script(array('utilities.min','script'));
/*
 * commented by sadikhasan
 * */
//echo $this->Html->script(array('utilities.min','script.min'));
echo $this->Html->script(array('jquery.min','bootstrap','utilities.min','script.min','feather'));


echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
<base href="<?php echo Router::url('/',true);?>">
<?php
if($this->params->params['action']=="rate" && $this->params->params['controller']=="drobes")
{
	
	?>
	<script type="text/javascript">
	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo Configure::read('facebook.api_key')?>";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	
	
	<script src="https://apis.google.com/js/plusone.js"></script>
	<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
	<?php 
	if(isset($drobe) && !empty($drobe))
	{
	?>
	<meta property="og:title" content="<?php echo $drobe['Drobe']['comment']." on Everdrobe - Always know what to wear"; ?>"/> 
	<meta property="og:image" content="<?php echo Router::url('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],true);?>"/> 
	<meta property="og:url" content="<?php echo Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true);?>"/> 
	<meta property="og:site_name" content="Everdrobe"/> 
	<meta property="og:app_id" content="<?php echo Configure::read('facebook.api_key');?>"/> 
	<?php
	} 
}
$content_pages=Cache::read('content_pages');
?>

<?php if(isset($blocked_ui) && $blocked_ui==true) { ?>
		<script  type="text/javascript">
			$(document).ready(function(){
				count_height();
			});
			$(window).resize(function(){
				count_height();
			});
			$(window).load(function(){
				count_height();
			});
			function count_height()
			{
				var height = $(document).height()>=$(window).height()?$(document).height():$(window).height()
				//$(".blocked_ui_div").height(height+"px");
			}
		</script>
		<style>
			.right-panel{z-index: -1}
			/*#container{margin: 10px auto;position: static;float: none;}
			.blocked_ui_div{position: absolute;background: rgba(0,0,0,0.5);left:0;width: 100%}*/
		</style>
	<?php }?>

<meta name="description" content="<?php echo $content_pages['meta_description']['page_content']; ?>" >
<meta name="keywords" content="<?php echo $content_pages['meta_keywords']['page_content']; ?>" >
<script type="text/javascript">
var star_user_text="<?php echo Configure::read("setting.star_user_explanation");?>";
</script>
</head>

<body class="demos <?php if($this->Session->read('Auth.User.id')>0){ ?>after-login <?php } else{ ?> before-login<?php  } ?>">
<div class="whole_loading_parent">
	<img class="loading_img" src="images/process1.png">
	<label class="loading_label">Loading...</label>
</div>
<div class="content_wrapper">
<header id="header" class="main-header">
<?php echo $this->element('header')?>
</header>
<!-- Banner for Home page -->
	<?php
		$type=""; 
		if($this->params->params['action']=="index" && $this->params->params['controller']=="drobes"){
			$type="home";
		}
	 ?>
	<div class="page_banner <?php echo $type;?>">
		<div class="container text-center">
			<p class="banner-info">Use Everdrobe to snap a pic and get instant feedback</p>
		</div>
	</div>
<!-- Banner for Home page End -->
<div class="container111">
<?php //echo $this->element('left-panel')?>
<div class="blocked_ui_div">    
<div id="container" class="container">
<?php echo $this->Session->flash(); 
 echo $this->fetch('content'); ?>
</div> <!-- #container -->
</div>
<?php echo $this->element('right-panel'); ?>
</div>
<div class="clear"></div>
<div id="footer" class="951">
<?php 
echo $this->element('footer');
?>
</div>
<div class="clear"></div>
</div>
<?php 
//echo $this->element('sql_dump'); 
echo $content_pages['google_analytics']['page_content'];
if($this->Session->read('new_registration')==true)
{
	echo $content_pages['google_conversion_tracking']['page_content'];
	// delete session of new registration 
	// we can not delete session from Helper so we need session component
	SessionComponent::delete('new_registration');
} 
?>

<?php
if($this->Session->read('shopify_login_url')!='')
{
	?>
	<iframe id="shopify-auto-login" src="<?php echo $this->Session->read('shopify_login_url')?>" style="display: none" height="0px" width="0px"></iframe>
	<?php 
	SessionComponent::delete('shopify_login_url');
}
?>

</body>
</html>
