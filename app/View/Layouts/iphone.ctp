<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<?php echo $this->Html->charset(); ?>
<title>Everdrobe : <?php echo $title_for_layout; ?></title>
<?php 
echo $this->Html->meta('icon');
//echo $this->Html->css(array('style'));
echo $this->Html->css(array('bootstrap.min','style.min'));
//echo $this->Html->script(array('flexcroll','jquery-1.4.2.min.js','qtip.min','jquery.livequery.min','ajax_submit','script'));
echo $this->Html->script(array('flexcroll','jquery-1.4.2.min.js','qtip.min','jquery.livequery.min','ajax_submit','script.min'));
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
<base href="<?php echo Router::url('/',true);?>">
</head>
<body style="margin: 10px">
<h1>
<?php 
echo $page_title;
?></h1>
<div class="clear"></div>
<div id="container" class="content_page iphone" style="width: auto !important;">
<?php  echo $this->fetch('content'); ?> </div> 
<!-- #container -->
<div class="spacer"></div>
</body>
</html>
