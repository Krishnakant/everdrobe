<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->Html->charset(); ?>
<title>Everdrobe : <?php echo $title_for_layout; ?></title>
<?php 
 echo $this->Html->meta('icon'); 
//echo $this->Html->meta('icon',$this->Html->url('/icon.png'));
//echo $this->Html->css(array('style','admin','flexcrollstyles','SpryTabbedPanels','colorbox','qtip.min'));
//echo $this->Html->css(array('style.min','admin','flexcrollstyles','SpryTabbedPanels','colorbox','qtip.min'));
echo $this->Html->css(array('style.admin.min','admin','flexcrollstyles','SpryTabbedPanels','colorbox','qtip'));
//echo $this->Html->script(array('SpryTabbedPanels','flexcroll','jquery.min','jquery.colorbox','lazy_load','fileuploader','qtip.min','jquery.livequery.min','ajax_submit','all','script'));
echo $this->Html->script(array('SpryTabbedPanels','flexcroll','jquery.admin.min','jquery.colorbox','lazy_load','fileuploader','qtip.min','jquery.livequery.min','ajax_submit','all','script.min'));
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
<base href="<?php echo Router::url('/',true);?>" />
</head>

<body class="demos">

<div id="fix">
<div class="clear"></div>
<div id="container" class="admin_container admin_front">
<?php echo $this->element('front-admin-top');?>

<div class="spacer"></div>
<div class="spacer"></div>

<div class="middle">
<?php echo $this->Session->flash(); 
 echo $this->fetch('content'); ?>
 </div>
</div> <!-- #container -->
</div>
<div class="clear"></div>
<div id="footer">
<?php 
echo $this->element('footer');
?>
</div>
<div class="clear"></div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
