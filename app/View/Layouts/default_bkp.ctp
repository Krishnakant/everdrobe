<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <?php if($this->params->params['action']=="rate" && $this->params->params['controller']=="drobes"){ echo 'xmlns:fb="http://ogp.me/ns/fb#"';} ?>>
<head>
<?php echo $this->Html->charset(); ?>
<title>Everdrobe : <?php echo $title_for_layout; ?></title>
<?php 
echo $this->Html->meta('icon');
//echo $this->Html->css(array('style','fixed','flexcrollstyles','SpryTabbedPanels','colorbox','qtip.min'));
echo $this->Html->css(array('style.min','fixed','flexcrollstyles','SpryTabbedPanels','colorbox','qtip.min','font-awesome.min','bootstrap.min'));
echo $this->Html->script(array('SpryTabbedPanels','flexcroll','jquery.js','jquery.colorbox','lazy_load','fileuploader','qtip.min','jquery.livequery.min','ajax_submit','all','script'));
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
<base href="<?php echo Router::url('/',true);?>">
<?php 
if($this->params->params['action']=="mydrobe")
{
	echo $this->Html->css(array('jquery.Jcrop'));
	echo $this->Html->script(array('jquery.Jcrop'));
	
}
if($this->params->params['action']=="rate" && $this->params->params['controller']=="drobes")
{
	
	?>
	<script type="text/javascript">
	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo Configure::read('facebook.api_key')?>";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		
	</script>
	<meta property="og:title" content="<?php echo $drobe['Drobe']['comment']; ?>"/> 
	<meta property="og:image" content="<?php echo Router::url('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],true);?>"/> 
	<meta property="og:url" content="<?php echo Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true);?>"/> 
	<meta property="og:site_name" content="Everdrobe"/> 
	<meta property="og:app_id" content="<?php echo Configure::read('facebook.api_key');?>"/> 
	<?php 
}
?>
</head>

<body class="demos <?php if($this->Session->read('Auth.User.id')>0){ ?>after-login <?php } else{ ?> before-login<?php  } ?>">

<div id="fix">
<?php echo $this->element('top');?>
<div class="clear"></div>
<?php echo $this->element('left-panel')?>
    
<div id="container" >
<?php echo $this->Session->flash(); 
 echo $this->fetch('content'); ?>
</div> <!-- #container -->
<?php echo $this->element('right-panel'); ?>
</div>
<div class="clear"></div>
<div id="footer">
<?php 
echo $this->element('footer');
?>
</div>
<div class="clear"></div>
<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
