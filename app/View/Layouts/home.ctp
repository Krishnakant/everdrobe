<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->Html->charset(); ?>
<title>Outfit Ideas, Fashion Feedback & Selfies - Everdrobe.com</title>
<meta name="description" content="We help you share outfits and selfies with real people & get instant fashion feedback. Find out what to wear, shop people's closets & sell yours with our app." />
<meta content="fashion, everdrobe, sale, vintage, clothing, accessories, bags, jewelry, trends, style" name="keywords" />
<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('front');
		echo $this->Html->script(array('jquery.min','crossfade.min'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>

<body>
<div class="content_wrapper">
<div id="banner">
	<div id="fix">
    	<!--  <div id="home_mobile_bg">
    	<?php //echo $this->Html->image('/images/iphone.png',array('class'=>'iphone_slider'))?>
    	<?php //echo $this->Html->image('/images/android_img.png',array('class'=>'android_slider'))?>	
  		</div>
  		-->
  		<ul id="gallery">
		<li><?php echo $this->Html->image('/images/iphone.png',array('class'=>'iphone_slider'))?></li>
		<li><?php echo $this->Html->image('/images/android_img.png',array('class'=>'android_slider'))?></li>
		</ul>
    	<?php echo $this->Html->link("",array("controller"=>"drobes","action"=>"index"),array("class"=>"logo"));?>
        <div class="company">
        	<a href="https://itunes.apple.com/in/app/id576116687" target="_blank"><span class="apple"></span></a>
            <a href="https://play.google.com/store/apps/details?id=com.bizhill1.everdrobe" target="_blank"><span class="android"></span></a>
        </div>
        <div class="link-box">
        	<?php echo $welcome_points;?>
        </div>
    </div>
</div>

<div id="fix" >
<div class="timeline">
	<h1>Fashion Feedback - Live!</h1>
	<div id="last_timeline">
	<?php 
	if(count($drobes)>0)
	{
		foreach ($drobes as $drobe)
		{
			?>
			<div class="testi">
				<div class="line"></div>
				<div class="photo">
		    	<?php echo $this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array("border"=>0,"width"=>70,"height"=>70));?>
		    	</div>
		        <div class="testi-txt">
		        	<?php echo $drobe['Drobe']['comment'];?>
		        </div>
		    </div>
			<?php 
		}
	}
	?>
	</div>
<div class="testi">
<div class="line"></div>
<div class="photo"><?php echo $this->Html->image('/time-logo.jpg',array("border" =>0,"width"=>70,"height"=>70));?></div>
<div class="testi-txt">
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-502b123b43c9a2fb"></script>
<!-- AddThis Button END -->
</div>
</div>

</div>

<div class="welcome">
	<h1><?php echo $welcome_title;?></h1>
    <div class="line"></div>
    <?php echo $welcome_content;?>
    
    
    <?php echo $this->Html->link("",array("controller"=>"drobes","action"=>"index"),array("class"=>"enter"));?>
    
</div>
</div>

<div id="footer" class="111" >
	<?php echo $this->element('footer'); ?>
</div>
</div>
<script type="text/javascript">
function get_timeline()
{
	$.ajax({
		url: 'drobes/timeline',
		success: function(data) {
			$newData=$(data).find(".testi");
			
			if($newData.eq(0).html()!=$('#last_timeline .testi').eq(0).html())
			{
				$('#last_timeline .testi').eq(0).hide().html($newData.eq(0).html()).fadeIn();
			}
			if($newData.eq(1).html()!=$('#last_timeline .testi').eq(1).html())
			{
				$('#last_timeline .testi').eq(1).hide().html($newData.eq(1).html()).fadeIn();
			}
			setTimeout('get_timeline()',5000);
	  	}
	});
}
$(document).ready(function(){
	get_timeline();
});
</script>
<?php 
$pages=Cache::read('content_pages');
echo $pages['google_analytics']['page_content']; 
?>
</body>
</html>
