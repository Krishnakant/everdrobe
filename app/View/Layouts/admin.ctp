<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->Html->charset(); ?>
<title>Everdrobe : <?php echo $title_for_layout; ?></title>
<?php 
echo $this->Html->meta('icon');
echo $this->Html->css(array('style.admin.min','admin','sell_drobe.min','flexcrollstyles','SpryTabbedPanels','colorbox','qtip.min'));
//echo $this->Html->css(array('style.min','admin','sell_drobe','flexcrollstyles','SpryTabbedPanels','colorbox','qtip.min'));
//echo $this->Html->script(array('/ckeditor/ckeditor.js','SpryTabbedPanels','flexcroll','jquery.min','jquery.colorbox','fileuploader','qtip.min','jquery.livequery.min','ajax_submit','all','script'));
echo $this->Html->script(array('/ckeditor/ckeditor.js','SpryTabbedPanels','flexcroll','jquery.admin.min','jquery.colorbox','fileuploader','qtip.min','jquery.livequery.min','ajax_submit','all','script.min','jquery.Jcrop','feather'));
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
if(in_array($this->params['action'],array("admin_broadcast","admin_broadcast_stub")))
{
	echo $this->Html->script('jquery_custom_select');
}

if(in_array($this->params['action'],array("admin_index")))
{
	echo $this->Html->script(array('highcharts','exporting'));
}
?>

<base href="<?php echo Router::url('/',true);?>" />
</head>
<script type="text/javascript">
var drobe_image_dir="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir'))).'/'; ?>";
var star_user_text="<?php echo Configure::read("setting.star_user_explanation");?>";
</script>
<body class="demos">

<div id="fix" style="width: 1024px;">
<?php echo $this->element('admin-top');?>
<div class="clear"></div>
<?php echo $this->element('admin-left-panel')?>
<div id="container" class="admin_container">
<?php echo $this->Session->flash(); 
 echo $this->fetch('content'); ?>
</div> <!-- #container -->
</div>
<div class="clear"></div>
<div id="footer">
<?php 
echo $this->element('footer');
?>
</div>
<div class="clear"></div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
