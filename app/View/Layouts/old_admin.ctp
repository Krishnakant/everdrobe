<?php
$cakeDescription = __d('cake_dev', 'Everdrobe');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
    </title>
    <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('default','uploadify','admin'));
        echo $this->Html->script(array('jquery','ajax_submit','jquery.uploadify.min','all','script'));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body>
    <div id="container">
        <div id="header">
            <div id="logo">
            </div>
            <?php 
            if($this->Session->read('Auth.User.type')=="admin")
            {
            ?>
            <div class="main_links">
                <ul>
                <li><?php echo $this->Html->link("Dashboard",array('controller'=>'admins','action'=>'index','admin'=>true),array()); ?></li>
                <li><?php echo $this->Html->link("Change Password",array('controller'=>'admins','action'=>'change_password','admin'=>true),array()); ?></li>
                <li><?php echo $this->Html->link("Logout",array('controller'=>'admins','action'=>'logout','admin'=>true),array()); ?></li>
                </ul>
            </div>
            <?php 
            }
            ?>
        </div>
        <div id="content">
            <?php 
            if($this->Session->read('Auth.User.type')=="admin")
            {
            ?>
            <div class="left_panel">
	            <div class="my_actions">
	                <ul>
	                    <li><?php echo $this->Html->link("User List",array('controller'=>'users','action'=>'index','admin'=>true),array()); ?></li>
	                    <li><?php echo $this->Html->link("Drobes List","javascript:void(0)",array()); ?></li>
	                    <li><?php echo $this->Html->link("Category",array('controller'=>'categories','action'=>'index','admin'=>true),array()); ?></li>
	                    <li><?php echo $this->Html->link("Flag Category",array('controller'=>'flag_categories','action'=>'index','admin'=>true),array()); ?></li>
	                    <li><?php echo $this->Html->link("Country/Province",array('controller'=>'countries','action'=>'index','admin'=>true),array()); ?></li>
	                    <li><?php echo $this->Html->link("Notifications",array('controller'=>'notifies','action'=>'index','admin'=>true),array()); ?></li>
	                </ul>
	            </div>
            </div>
            <?php 
            }
            ?>
            <div class="main_content <?php echo $this->Session->read('Auth.User.type')=="admin" ? "admin" : "admin_login" ?>">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
            </div>
        </div>
        <div class="clear"></div>
        <div class="bottom_part">
        </div>
        <div id="footer">
        </div>
    </div>
    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
