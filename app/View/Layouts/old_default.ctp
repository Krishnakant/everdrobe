<?php
$cakeDescription = __d('cake_dev', 'Everdrobe');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
    </title>
    <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array('default','uploadify'));

        echo $this->Html->script(array('jquery','jquery.livequery.min','ajax_submit','jquery.uploadify.min','all','script'));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    <base href="<?php echo Router::url('/',true);?>" />
</head>
<body>
    <div id="container">
        <div id="header">
            <div id="logo">
            </div>
            <div class="main_links">
                <ul>
                <li><?php echo $this->Html->link("Everdrobe",array('controller'=>"drobes","action"=>"index"),array()); ?></li>
                <li><?php echo $this->Html->link("About Us","javascript:void(0)",array()); ?></li>
                <li><?php echo $this->Html->link("Blog","javascript:void(0)",array()); ?></li>
                <li><?php echo $this->Html->link("How-To","javascript:void(0)",array()); ?></li>
                </ul>
            </div>
        </div>
        <div id="content">
            <div class="left_panel">
	            <div class="my_actions">
	                <ul>
	                    <li><?php echo $this->Html->link("Everdrobe",array('controller'=>"drobes","action"=>"index"),array()); ?></li>
	                    <li><?php echo $this->Html->link("Rate Drobes",array('controller'=>"drobes","action"=>"rate"),array()); ?></li>
	                    <li><?php echo $this->Html->link("My Drobes",array('controller'=>"drobes","action"=>"mydrobe"),array()); ?></li>
	                    <li><?php echo $this->Html->link("My Faves",array('controller'=>"drobes","action"=>"faves"),array()); ?></li>
	                    <li><?php echo $this->Html->link("Following",array('controller'=>"users","action"=>"follower"),array()); ?></li>
	                    <li><?php echo $this->Html->link("My Profile",array('controller'=>"users","action"=>"profile"),array()); ?></li>
	                    <li><?php echo $this->Html->link("My Results",array('controller'=>"drobes","action"=>"results"),array()); ?></li>
	                </ul>
	            </div>
            </div>
            <div class="main_content">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
            </div>
            <div class="right_panel">    
	            <div id="user_area">
                    <?php
                    if($this->Session->read('Auth.User.id')>0)
                    {
                    	echo $this->Element('user_profile');
                    }
                    else
                    {
                     echo $this->Element('user_login');
                     echo $this->Element('user_register');
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="bottom_part">
        </div>
        <div id="footer">
        </div>
    </div>
    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
