<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<?php echo $this->Html->charset(); ?>
<title>Everdrobe : <?php echo $title_for_layout; ?></title>
<?php 
echo $this->Html->meta('icon');
echo $this->Html->css(array('bootstrap.min','style.min'));
echo $this->Html->script(array('utilities.min','script.min'));
echo $this->fetch('meta');
echo $this->fetch('css');
echo $this->fetch('script');
?>
<base href="<?php echo Router::url('/',true);?>">
</head>
<body class="demos <?php if($this->Session->read('Auth.User.id')>0){ ?>after-login <?php } else{ ?> before-login<?php  } ?>">
<div class="content_wrapper">
<div id="fix">
<?php echo $this->element('top');?>
<div class="clear"></div>
<h1>
<?php 
echo $page_title;
?></h1>
<div class="clear"></div>
<div id="container" class="content_page container">
<?php  echo $this->fetch('content'); ?> </div> 
<!-- #container -->
</div>
<div class="spacer"></div>
<div id="footer" class="963">
<?php 
echo $this->element('footer');
?>
</div>
</div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
