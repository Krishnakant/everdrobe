<h2>Add New State/Province</h2>
<?php 
echo $this->Html->link('Back',array('controller'=>'provinces','action'=>'index',$country['id'],'admin'=>true));
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
echo $this->Form->create("Province");
?>
<div class="spacer"></div>
<?php

echo $this->Form->input("province_name",array('label'=>'State/Province Name'));
echo $this->Form->input('status', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Category Status",'options' => array("active"=>"Active","inactive"=>"Inactive"),'type' => 'radio','default'=>'active'));
echo $this->Form->hidden('country_id',array('value'=>$country['id']));
?>
<div class="spacer"></div>
<?php

echo $this->Form->submit('Submit');
echo $this->Form->end();
?>