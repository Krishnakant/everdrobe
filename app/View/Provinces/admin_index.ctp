<h2>State/Province List <?php if(isset($country['country_name'])) echo ": ".$country['country_name']; ?></h2>
<div class="actions right">
<?php echo $this->Html->link('Add New Province',array('controller'=>'provinces','action'=>'add',$country['id'],'admin'=>true))?> 
<?php echo $this->Html->link('View Country List',array('controller'=>'countries','action'=>'index','admin'=>true))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th>State/Province</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($data as $province)
		{
			?>
			<tr>
				<td><?php echo $province['Province']['id'];?></td>
				<td><?php echo $province['Province']['province_name'];?></td>
				<td><?php echo $province['Province']['status'];?></td>
				<td>
				<div class="actions">
				<?php 
					echo $this->Html->link('Edit',array('controller'=>'provinces','action'=>'edit',$country['id'],$province['Province']['id'],'admin'=>true)). " "; 
					echo $this->Html->link('Delete',array('controller'=>'provinces','action'=>'delete',$country['id'],$province['Province']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'));
				?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>