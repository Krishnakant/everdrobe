<h1>My Drobes</h1>
<?php 
echo $this->Html->link('Add new Drobe',array('controller'=>"drobes","action"=>"create"),array("class"=>"right"));
?>
<div class="drobe_list">
<?php 
$i=0;
foreach($data as $drobe){
?>
<div class="drobe">
 <?php echo  $this->Html->link($this->Html->image('/drobe_images/'.$drobe['Drobe']['file_name'],array('alt'=>$drobe['Drobe']['file_name'],'height'=>Configure::read('drobe.thumb.height'), 'width'=>Configure::read('drobe.thumb.width'))),array("controller"=>"drobes","action"=>"index",$drobe['Drobe']['id']),array("border"=>0,'escape'=>false));?> 
</div>
<?php
}    
?>
</div>
<div class="pagignator">
    <!-- Shows the page numbers -->
    <?php echo $this->Paginator->numbers(); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->prev('� Previous', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->next('Next �', null, null, array('class' => 'disabled')); ?>
    <!-- prints X of Y, where X is current page and Y is number of pages -->
    <?php echo $this->Paginator->counter(); ?>

</div>
