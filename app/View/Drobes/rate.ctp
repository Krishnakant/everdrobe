<?php
if(!$is_ajax)
{
	?>
	<div id="fb-root"></div>
	<div id="rate-drobe-container" class="container">
	<?php
	if($this->Session->read('Auth.User.id')>0)
	{
		//echo $this->element('filter_category');
	} 
	else
	{
		//echo '<h2 class="text-center">Login</h2>';
		echo $this->element('user_login_info');
	}
	
	?>
<div id="flag_block" class="flag_form text-center">
<h2>Flag drobe</h2>
<div class="spacer"></div>
<?php 
echo $this->Form->input('flag_category',array('options'=>Cache::read('flag_category'),'id'=>'flag_category','class'=>'form-control','label'=>false,'div'=>false,'empty'=>"Select Reason"));
?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_flag",'class'=>"btn btn-primary"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_flag",'class'=>"btn btn-danger"));
?>
<div class="spacer"></div>
</div>
	
	<div id="rate_drobe_block" class="col-sm-5 col-sm-offset-3_5">
	<?php 
}
if($drobe)
{
	//fixing issue for enter and return character in javascript
	$drobe['Drobe']['comment']=trim(str_replace(array("\r","\n"), " ", $drobe['Drobe']['comment']));
	
	?>
	<h2 class="text-center"><?php  echo $drobe['Category']['category_name']; ?></h2>
	<div class="big-img drobe_big_img"  >
	<div class="rate_response" id="rate_response" style="display:none">
	</div>
 	<?php 
 	echo $this->Html->image('/drobe_images/'.$drobe['Drobe']['file_name'],array('border'=>0, 'id'=>"profile_image", 'class'=>"img-rounded" ,"alt"=>"drobe prview Image"));
 	?>
    <div class="drobe_comment">
	  <div >
     
	  	<?php  
		  	echo $drobe['Drobe']['comment'];
		  	
		  	$header='<div  class="pro_user_info"><div class="imuser">';
		  		if(isset($drobe['User']['photo']) && $drobe['User']['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$drobe['User']['photo']))
		  	{
		  		$header.=$this->Html->image('/profile_images/thumb/'.$drobe['User']['photo'],array('width'=>20,'border'=>0, 'id'=>"profile_image","class"=>"img-thumbnail"));
		  	}
		  	else
		  	{
		  		$header.=$this->Html->image('/images/default.jpg',array('width'=>20,'height'=>20,'border'=>0, 'id'=>"profile_image" ,"class"=>""));
		  	}
		  	$header.='</div>';
		  	
		  	if(isset($drobe['User']['username']) && $drobe['User']['username']!="")
		  	{
		  		$username=$drobe['User']['username'];
		  	}
		  	else
		  	{
		  		$username=$drobe['User']['first_name'].' '.$drobe['User']['last_name'];
		  	}
		  	
		  	$header.='<label class="linkuser" >'.$username.'</label></div>';
		  	echo $this->Html->link($header,array('controller'=>"users",'action'=>"profile",$drobe['User']['unique_id']),array("id"=>"user_link_rate",'escape'=>false));
		  	
	  	?>
	 <div class="clear"></div>
	  <div class="pull-left">
	  <?php echo date("dS M, Y",strtotime($drobe["Drobe"]["uploaded_on"]));?>
	  </div>
       <div class="pull-right" >
		<?php 
	//pr($drobe['Drobe']['file_name']);exit;
	if($this->Session->read('Auth.User.id')>0)
	{
		?>
	 	<a href="javascript:void(0)" id="flag_button"><?php echo $this->Html->image('/images/flag.png',array("height"=>14,"style"=>""));?></a>
	 	<?php 
	}
	?>
	</div>
	  <div class="clear"></div>
	
	 </div>
		<div class="size_price_detail">
		<?php 
		if(($drobe['Drobe']['featured']==1 || $drobe['Drobe']['post_type']=='sell') && $drobe['Drobe']['buy_url']!="" && Configure::read('setting.featured_drobe')=="on")
		{
			echo $this->Html->link('Buy Now',$drobe['Drobe']['buy_url'],array('target'=>'_blank','escape'=>false,"class"=>"big_buy"));
			if(isset($drobe['SellDrobe']['sell_price']) && $drobe['SellDrobe']['sell_price']>0)
			{
				echo '<div class="drobe_size">Size:'.$drobe['SellDrobe']['size_name'].'</div>';
				if($drobe['SellDrobe']['original_sell_price']>$drobe['SellDrobe']['sell_price'])
				{
					echo '&nbsp; <div class="original_price"><strike>$'.$drobe['SellDrobe']['original_sell_price'].'</strike></div>';
				}
				echo '<div class="listing_price">$'.$drobe['SellDrobe']['sell_price'].'</div>';
			}
			
		}
		?>
		</div>
	</div>
 	<div class="flag_drobe">
 	<div class="summary">
    	<div class="imp">Views : <?php echo $drobe['Drobe']['views']?></div>
        <div class="imp">Cute : <?php echo $drobe['Drobe']['total_in'];?></div>
        <div class="imp">Hot : <?php echo $drobe['Drobe']['total_out'];?></div>
    </div>
  	<?php 
 	if($this->Session->read('Auth.User.id')>0)
 	{
 	?>
 	<a href="javascript:void(0)" id="flag_button"><?php echo $this->Html->image('/images/flag.png',array("height"=>14));?> Flag</a>
 	<?php 
	}
 	?>
 	</div>
 	<?php  
	if($drobe['Drobe']['is_highest_rated']>0)
	{
		?>
	 	<div class="highest-rated-drobe-main"></div>
	 	<?php 
	}
	?>
 	</div>
    
    <div class="over text-center">
    
    
	
    <?php
    if($my_drobe)
    {
    	?>
            <a href="javascript:void(0)" class="btn red_btn btn-danger" >
            <div class="white_plus"></div>
            <span class="btn_txt">Follow</span></a>
        <?php  
    } 
    else if(!$is_followed)
    {
    ?>
        <a href="javascript:void(0)" class="btn red_btn" id="follow_button" >
        <div class="white_plus"></div>
        <span class="btn_txt">Follow</span></a>
     <?php 
    }
    else
    {
     ?>
     <a href="javascript:void(0)" class="btn  green_btn" id="follow_button" >
     <div class="white_minus"></div><span class="btn_txt">Following</span></a>
     <?php 
    }
     ?>   
        <a href="javascript:void(0)" class="heart" id="fave_button" <?php if(isset($is_my_favourite) && $is_my_favourite>0) echo 'disabled="disabled"'; ?>>
        <span class="btn_txt"><?php  echo $this->Html->convertToReadeable(intval($drobe['Drobe']['favs'])); ?></span></a>
       <div class="share">
       		<a href="javascript:void(0)" rel="shareit"><img src="images/share.png" border="0"></a>
           
            <div id="shareit-icon">
  <ul>
   <li>
   		<a class="facebook-share-button" href="#" 
  			onclick="window.open(
      			'https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(Router::url("/rate/".$drobe['Drobe']['unique_id'],true))?>', 
      			'facebook-share-dialog', 
      			'width=626,height=436'); 
    			return false;">
		</a>
   </li>
   <li style="display: none;">
	   <div class="fb_like">
	   <fb:like href="<?php echo Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true);?>" send="false" width="50" show_faces="false"></fb:like>
	   </div>
   </li>
   <li>
   		<a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true));?>&media=<?php echo urlencode(Router::url('/drobe_images/'.$drobe['Drobe']['file_name'],true))?>&description=<?php echo urlencode($drobe['Drobe']['comment']." on Everdrobe - Always know what to wear")?>" class="pin-it-button" count-layout="none" target="_blank"><img src="images/pinit.png" alt="Pin it" border="0" /></a>
   </li>
   <li>
   		<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $drobe['Drobe']['short_url'];?>" data-text="<?php echo $drobe['Drobe']['comment']." on Everdrobe - Always know what to wear";?>" data-count="none"><img src="https://apmcommunity.compuware.com/community/download/attachments/25789254/tweetbutton.png" border="0" /></a>
	</li>
	<li>
	<div class="gplus_share">
	<g:plus action="share" href="<?php echo Router::url("/rate/".$drobe['Drobe']['unique_id'],true)?>" annotation="none" ></g:plus>
	</div>
	</li>
	<li>
	<a class="tumblr-button" href="#" 
  			onclick="window.open(
      			'http://www.tumblr.com/share/link?url=<?php echo urlencode(Router::url("/rate/".$drobe['Drobe']['unique_id'],true))?>&name=<?php echo urlencode($drobe['Drobe']['comment']." on Everdrobe - Always know what to wear")?>&description=<?php echo urlencode("<img src=\"".Router::url('/drobe_images/'.$drobe['Drobe']['file_name'],true)."\">")?>', 
      			'tumblr-share-dialog', 
      			'width=626,height=436'); 
    			return false;">Tumblr</a>
	</li>
  </ul>
 </div>
       </div>
       <?php 
       
       if(($drobe['Drobe']['featured']==1 || $drobe['Drobe']['post_type']=='sell') && $drobe['Drobe']['buy_url']!="" && Configure::read('setting.featured_drobe')=="on")
       {
	       	if(isset($drobe['SellDrobe']['sell_price']) && $drobe['SellDrobe']['sell_price']>0)
	       	{
	       		//echo '<div class="price">$'.$drobe['SellDrobe']['sell_price'].'</div>';
	       		if($drobe['SellDrobe']['original_sell_price']>$drobe['SellDrobe']['sell_price'])
	       		{
	       			//echo '<div class="price"><strike>$'.$drobe['SellDrobe']['original_sell_price'].'</strike></div>';
	       		}
	       	   //echo '<div class="drobe_size">Size:'.$drobe['SellDrobe']['size_name'].'</div>';
	       	}
	       	
       		//echo '<div class="buy_it">'.$this->Html->link($this->Html->image('/images/btn_buy_it.png',array("border"=>0)),$drobe['Drobe']['buy_url'],array("target"=>"_blank","escape"=>false)).'</div>';
	       	echo '<div class="">'.$this->Html->link('<span class="btn_txt">Buy Now</span>',$drobe['Drobe']['buy_url'],array("target"=>"_blank","escape"=>false,'class'=>'blue_buy','style'=>'margin-left:10px;')).'</div>';
       }
       ?>
     </div>
     <?php 
     if(!$is_rated)
     {
     ?>
     <div class="clearfix"></div>
     <div class="mt15 text-center btn-inline">
        <div class="form-group"><textarea cols="28" rows="3" id="drobe_comment" class="form-control" placeholder="Leave a comment"></textarea></div>
        <div class="more_margin submit_comment" > <a href="javascript:void(0)" class="only_comment ratebutton btn btn-primary" id="drobe_rate_in" rel="2">Submit</a></div>
        <div class="more_margin rate_button" > <a href="javascript:void(0)" class="in ratebutton btn btn-green cute-button" id="drobe_rate_in" rel="1">Cute</a></div>
        <div class="more_margin rate_button" ><a href="javasrcipt:void(0)" class="out ratebutton btn btn-danger" id="drobe_rate_in" rel="-1">Hot</a></div>
        <div class="more_margin rate_button" ><a href="javascript:void(0)" class="whtever ratebutton btn btn-default" id="drobe_rate_in" rel="0">Skip</a></div>
     </div>
     <?php 
     }
     else
     {
     	$rate_given=intval($rate_info['rate']);
     	?>
     	<div class="clear"></div>
        
       
    
     	<div class="more_margin your_rate full_width mt15" >
     		
             <div class="input-group">
                  <div class="input-group-addon" id="prev_drobe"> <i class="fa fa-angle-left font24" aria-hidden="true"></i> </div>
                  <div class="input-group-addon" style="background: none; width:70%;"><?php if($rate_given!=0 || $rate_info['comment']=="") { ?>
                    <a  class="<?php 
                    if($rate_given > 0)
                    { 
                        echo "in"; 
                    } 
                    else
                    { 
                        echo $rate_given < 0 ? "out" : "whtever"; 
                    }?>">
                    </a>
                <?php }?>
                <?php 
                    if($rate_info['comment']=="")
                    {
                        echo "<div class='no_comment'>Not Commented</div>";
                    } 
                    else
                    {   		
                        echo "<div class='comment'>".$rate_info['comment']."</div>";
                    }
                ?></div>
                <div class="input-group-addon" id="next_drobe"> <i class="fa fa-angle-right font24" aria-hidden="true"></i> </div>
            </div>
        
     	</div>
     	
    	<?php 
     }
	
	?>
	<input type="hidden" id="drobe_id" value="<?php echo $drobe['Drobe']['unique_id']; ?>" />
	
	
	
<?php
}
else
{
?>
<div class="drobe_rate_empty text-center mt25">
<img src="images/sad-baby.png" alt="" />
<h3>Drobes not matched  as your criterea for Rate</h3>
</div>
<?php 
}
?>

<?php 
if(!$is_ajax)
{
?>
</div>
</div>
<div class="clear"></div>
<script type="text/javascript">
var order_by="<?php if(isset($this->params->pass[1])) echo $this->params->pass[1];?>";
var action="<?php if(isset($this->params->pass[2])) echo $this->params->pass[2];?>";
var action_id="<?php if(isset($this->params->pass[3])) echo $this->params->pass[3];?>";

</script>
<?php 
}
?>
<script type="text/javascript">
$('document').ready(function(){
	activate_menu("home");
	$("#user_link_rate").bind("click",function(){
		window.location=$(this).attr("href");
	});

	var all_checked= ($(".filter_drobe .categories input[type='checkbox']").not("#check-all").length==$(".filter_drobe .categories input[type='checkbox']:checked").not("#check-all").length);
	$("#check-all").attr('checked',all_checked);
	$('.drobe_big_img > img').load(function(){$(this).fadeIn()});	
	<?php 
	if($is_ajax && isset($drobe['Drobe']['following_drobe']) && $drobe['Drobe']['following_drobe']==true)
	{
		?>
		if($('#following_badge').length>0)
		{
			var badge_counter=parseInt($('#following_badge').text());
			badge_counter=badge_counter-1;
			if(badge_counter>0)
			{
				$('#following_badge').text(badge_counter);
			}
			else $('#following_badge').remove();
		}
		<?php 
	}
	?>
});
var is_colorbox=false;
var is_selected=<?php echo (isset($this->params->params['pass'][0]) && $this->params->params['pass'][0]!="")? "true": "false" ?>;
<?php 
if($this->Session->read('Auth.User.id')>0){
	?>
	var read_only_mode=false;
	<?php
}
else
{
	?>
	var read_only_mode=true;
	<?php 
}
?>
$.getScript("js/rate_drobe.min.js");
<?php 
if($drobe['Drobe']['comment'])
{
?>
	$('title').html("<?php echo addslashes($drobe['Drobe']['comment']) . " - Everdrobe.com"; ?>");
	$('meta[property="og:title"]').attr('content', "<?php echo addslashes($drobe['Drobe']['comment']." on Everdrobe - Always know what to wear"); ?>");
<?php 
}
?>
$('meta[property="og:image"]').attr('content', "<?php echo Router::url('/drobe_images/'.$drobe['Drobe']['file_name'],true);?>");
$('meta[property="og:url"]').attr('content', "<?php echo Router::url("/rate/".$drobe['Drobe']['unique_id'],true);?>");
</script>

