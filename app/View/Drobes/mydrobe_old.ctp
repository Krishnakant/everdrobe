<?php
if(isset($drobe_uploaded) && $drobe_uploaded==true)
{
	?>
	<h2>
	<?php 
	echo $this->data['Drobe']['comment'];
	?>
	</h2>
	<div class="clear_outer_space">
	<?php 
	echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name'],array('width'=>Configure::read('drobe.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));
	?>
	</div>
	<div class="drobe_uploaded">
        <h2>
          That's it!
        </h2>
          <p>
            Check responses in a couple of<br>
            minutes to see lots of opinions.
          </p>
          <p>
            While you're waiting, go get<br>
            some stars.
          </p>
          <div class="actions">
          <p>
          <?php 
          echo $this->Html->link('Rate',array('controller'=>"drobes","action"=>'rate'),array());
          ?>
          </p>
          </div>
      
	</div>
	<?php
}
else
{
echo $this->Form->create('Drobe',array('class'=>"form", 'type'=>'file'));
?>
<div class="drobe_preview clear_outer_space">
<img src="" alt="drobe prview Image"/>
<a href="javascript:void(0)" id="clear_preview" class="right">Change Image</a>
</div>
<?php 
echo $this->Form->file('file',array('label'=>false,'id'=>'file_upload'));
echo $this->Form->hidden('file_name');
?>
<div class="clear"></div>
<?php 
echo $this->Form->input('category_id',array('label'=>false,'options'=>$categoryList,'empty'=>"Select Category"));
echo $this->Form->textarea('comment',array('label'=>false));
?>
<div class="spacer"></div>
<?php 
echo $this->Form->input("DrobeSetting.ask_public",array("class"=>"required","value"=>"1",'type'=>"checkbox","checked"=>true));
?>
<div class="clear"></div>
<?php 
echo $this->Form->input("DrobeSetting.ask_friends",array("value"=>"1",'type'=>"checkbox","checked"=>"checked"));
echo $this->Form->input("DrobeSetting.facebook",array("value"=>"1",'type'=>"checkbox"));
echo $this->Form->input("DrobeSetting.twitter",array("value"=>"1",'type'=>"checkbox"));
?>
<div class="clear"></div>
<?php 
echo $this->Form->input("DrobeSetting.male",array("value"=>"1",'type'=>"checkbox",'label'=>"Males","checked"=>true));
echo $this->Form->input("DrobeSetting.female",array("value"=>"1",'type'=>"checkbox",'label'=>"Females","checked"=>true));
?>
<div class="clear"></div>
<?php 
echo $this->Form->submit('Submit');
echo $this->Form->end();
?>
<script type="text/javascript">
$(function() {
    $('#file_upload').uploadify({
        'swf'      : '<?php echo $this->params->webroot?>uploadify/uploadify.swf',
        'uploader' : 'http://<?php echo $_SERVER['HTTP_HOST'].$this->Html->url(array('controller'=>"uploads","action"=>"file",'drobes'));?>',
        'buttonText' : 'Click here to upload file',
        'formData'      : {'type' : 'drobes'},
        'height':80,
        'width':367, 
        'onUploadSuccess' : function(file, data, response) {
            var obj=jQuery.parseJSON(data);
            if(obj.type=="success")
            {
                $('#DrobeFileName').val(obj.file);
                $('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')).'/')?>" + obj.file);
                $('.drobe_preview').show();
                $('#file_upload').hide();
                $('#DrobeCreateForm input[type="submit"]').removeAttr('disabled');
            }
            else
            {  
            	alert(obj.message);
            	 $('#DrobeFileName').val("");
            	$('.drobe_preview img').attr('src',"");
            	$('.drobe_preview').hide();
                $('#file_upload').show();
                $('#DrobeCreateForm input[type="submit"]').attr('disabled','disabled');
            }
        }
        // Put your options here
    });
    $('#clear_preview').click(function(){
    	$('#DrobeFileName').val("");
     	$('.drobe_preview img').attr('src',"");
     	$('.drobe_preview').hide();
        $('#file_upload').show().click();
    });
    $('#DrobeCreateForm input[type="submit"]').attr('disabled','disabled');
    <?php 
    if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']!="")
    {
    	?>
    	$('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')).'/'.$this->data['Drobe']['file_name'])?>" );
        $('.drobe_preview').show();
        $('#file_upload').hide();
        $('#DrobeCreateForm input[type="submit"]').removeAttr('disabled');
    	<?php 
    }
    ?> 
});
</script>
<?php 
}
?>