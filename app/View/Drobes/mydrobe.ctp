<div id="drobe_type_btn">
	<div class="post_drobe_btn mt25 text-center">
		<?php echo $this->Html->link('<div class="" id="post_for_feedback"><i class="fa fa-camera" aria-hidden="true"></i></div><br>Post for Feedback',array('action'=>'mydrobe_post'),array('id'=>'post_feedback','escape'=>false,"style"=>"")) ?>
		<div class="spacer"></div>
		<?php if(Configure::read("setting.drobe_sale")!="off") {?>
		<div class="spacer"></div>
		<?php echo $this->Html->link('<div class="" id="post_for_sell"></div>Post for Sale',array('action'=>'mydrobe_sell'),array('id'=>'post_sale','escape'=>false,"style"=>"")) ?>
		<?php }?>
	</div>
</div>
<script type="text/javascript">
$('document').ready(function(){
activate_menu("post");	
});
</script>
