<div class="clearfix mt15"></div>

<div class="tp tp_big drobe_result  center-block">
<div class="reward_response" id="reward_response" style="display:none"></div>
	<div class="media">
		<div class="media-left media-middle">
		
		<?php echo $this->Html->image('/drobe_images/'.$drobeData['Drobe']['file_name'],array('alt'=>$drobeData['Drobe']['file_name'],'height'=>"85", 'width'=>"85")); ?>
		</div>
         <div class="user_cont media-body">
         	<div class="cont"><?php echo $drobeData['Drobe']['comment']=="" ? "" : $drobeData['Drobe']['comment']." <br />";?><em><?php echo $categoryList[$drobeData['Drobe']['category_id']]?></em></div>
            <div class="up btn btn-green btn-sm mt5"><?php echo $drobeData['Drobe']['total_in'];?> Cute</div> 
            <div class="down btn btn-danger btn-sm mt5"><?php echo $drobeData['Drobe']['total_out'];?> Hot</div> 
            <div class="whatever btn btn-default btn-sm mt5 "><?php echo $drobeData['Drobe']['total_rate']?> Votes</div>  
         
		  </div></div>
</div>
<div class="clearfix"></div>
<div class="user_rate col-sm-6 col-md-4 mt15">
	<div class="rate btn rate_<?php echo $drobeData['Rate']['rate']>0 ? "Cute" : ( $drobeData['Rate']['rate'] < 0 ? "Hot" : "Skip" ); ?> "></div>
	<div class="profile_photo"><?php 
	if($drobeData['User']['photo']!="") {
		echo $this->Html->image('/'.str_replace(DS,"/",Configure::read('profile.thumb.upload_dir'))."/".$drobeData['User']['photo'],array('height'=>60));
	}
	else
	{
		echo $this->Html->image(Configure::read('profile.default_image'),array('height'=>60));
	}
	if($drobeData['User']['star_user']==1):?>
		<div class="star-user-small"></div>
	<?php endif;?></div>
	<?php 
	if($drobeData['Rate']['conversation_status']=="open")
	{
	?>
	<div class="right" style="padding: 0; margin: 0">
		<a href="javascript:void(0)" id="refresh" class="blue_btn btn btn-primary btn-xs">Refresh</a>
		<a href="javascript:void(0)" id="confirm_close" class="red_btn btn btn-danger btn-xs" >Close Chat</a>
	</div>
	<?php 
	}
	?>
	<div class="user_info"> <?php if(isset($drobeData['User']['username']) && $drobeData['User']['username'] != null) {?>
		<td><?php echo  $this->Html->link($drobeData['User']['username'],array('controller'=>"users","action"=>"profile",$drobeData['User']['unique_id']));?></td>
   <?php
	}
	else
	{?>
		<td><?php echo  $this->Html->link($drobeData['User']['first_name']. " " .$drobeData['User']['last_name'],array('controller'=>"users","action"=>"profile",$drobeData['User']['unique_id']));?></td>
	<?php } ?><br/>
	<small>
	<?php echo $drobeData['User']['gender'].($drobeData['User']['country']!=""? ", ".$drobeData['User']['country']: "").".";?>
	</small>
	</div>
	<div class="clear"></div>
	<?php 
	if($drobeData['Rate']['comment']!="")
	{
		
		?>
		<div class="star">
		<a href="javascript:void(0)" class="<?php echo $drobeData['Rate']['conversation_id']?>">
	<span class="reward_button <?php echo $drobeData['Rate']['rewarded']>0 ? "rewarded" : ""; ?>"></span>
	</a>
	
</div>

		<div class="comment"><?php echo $drobeData['Rate']['comment'];?></div>
		
	<?php 
	}
	?>
	</div>
	<div class="spacer"></div>
	<div class="spacer"></div>
	<div id='conversations_area' class="conversations_area">
		   	<div class="long_content" id="drobe_conversations">
		   	</div>
		</div>
	<div class="spacer"></div>
		
	<?php 
	if($drobeData['Rate']['conversation_status']=="open")
	{
	?>
		<div class="chat_send center-block form-group" id="reply_conversation">
		<div class="input-group">
	        <textarea cols="43" rows="2" class="chat form-control" id="replyarea" placeholder="Post your reply here"></textarea>
	    	<div class="input-group-addon"><a href="javascript:void(0)" class="btn btn-primary" id="replybtn">Send</a></div>
		</div>
	   	</div>
	   	<div id="close_confirm" class="confirm_popup">
		<h2>Confirm</h2>
		<p>Are you sure to close this conversation?</p>
		<div id="close_drobe_id"></div>
		<div class="actions">
		<?php
		echo $this->Html->link('Yes','javascript:void(0)',array('id'=>"close_conversation_yes","class"=>"green_btn"));
		echo $this->Html->link('No','javascript:void(0)',array('id'=>"close_conversation_no","class"=>"red_btn"));
		?>
		</div>
		<div class="clear"></div>
		</div>
		
   	<?php 
	}
	else
	{
	?>
	<h2 class="empty" style="text-align: center">This conversation has been closed</h2>
	<?php 
	}
	?>
	
	<script type="text/javascript">
			var conversation_id='<?php echo $this->params['pass'][1];?>';
			function refresh_conversation(){
				$('#drobe_conversations').load('conversations/view/'+conversation_id,function(){
					set_conversation_scrollbar();
			    });
			};
			$(document).ready(function(){
				fleXenv.fleXcrollMain($('.conversations_area').attr('id'));
				refresh_conversation();
				$('#confirm_close').click(function() {
					Confirm('Are you sure to remove this drobe conversation?', function(yes) {
						if(yes)
						{
							// do something with yes
							$.ajax({
								url: 'rates/close_conversation',
								type: "POST",
								data: {
									conversation_id: conversation_id
								},
								success: function(data) {
									$('#confirm_close').val("Conversation Closed").attr('disabled','disabled');
									$('#reply_conversation').remove();
							}
							});
						}
						return false;
					});
					
				});
				$('#refresh').click(function() {
					refresh_conversation();
				});
			});
		</script>
	<?php if($this->Session->read('Auth.User.id')==$drobeData['Drobe']['user_id']):?>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.reward_button').each(function(){
			$(this).parent('a').click(function(){
				conversation_id=$(this).attr('class');
				if($(this).find('span:first').hasClass('rewarded'))
				{
					return false;
				}
				else
				{
					$('.'+conversation_id).find('span:first').addClass('rewarded');
					$.ajax({
						url: 'rates/reward', 
						type: "POST",
						data: {
							conversation_id : conversation_id
						},
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
							{
						  		$('#reward_response').html(obj.message).fadeIn('fast');
						  		setTimeout('$("#reward_response").html("").fadeOut("fast");',2500);
							}
						  	else
						  	{
						  		$('.'+conversation_id).find('span:first').removeClass('rewarded');
						  		alert(obj.message);
						  	} 
						  	
					  	}
					})
				}
			});
		});
	})
	</script>
	<?php endif;?>
   