<h2 class="my_result-page text-center title_mt2" >My Results</h2>
<div id="TabbedPanels1" class="TabbedPanels">
		  <ul class="TabbedPanelsTabGroup">
		    <li class="TabbedPanelsTab" tabindex="0">Got Feedback 
		    <?php if($new_rate_reponse>0)
		    {
		     echo '<div class="badges">'.$new_rate_reponse.'</div>';
		      }
		    ?>
		    </li>
		    <li class="TabbedPanelsTab" tabindex="0">Gave Feedback 
		    <?php if($gave_feedback_reponse>0)
		    {
		     echo '<div class="badges">'.$gave_feedback_reponse.'</div>';
		    }
		    ?>
		    </li>
	      </ul>
	      
      	  <div class="TabbedPanelsContentGroup">
      	    <div class="TabbedPanelsContent">
      	    <div class="col-sm-6 col-sm-offset-3">
				<div class="input-group">
					<input type="text" class="search_input form-control input-lg" id="got_feedback_search" placeholder="Search by Drobe comment,Rate comment or Keyword" />
					 <div class="input-group-addon search-icon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
				</div>
      	    </div>
			<div class="clearfix"></div>
		    	<div id='got_feedback_area' class='infiniteScroll feedback_area' style="margin-top:10px;">
            		<div class="long_content" id="got_feedback">
            		</div> 
                 </div>
            </div>
		    <div class="TabbedPanelsContent">
			   
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="input-group">
                        <input type="text" class="search_input form-control input-lg" id="gave_feedback_search" placeholder="Search by Drobe comment,Rate comment or Keyword" />
                         <div class="input-group-addon search-icon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            	<div id='gave_feedback_area' class='infiniteScroll feedback_area' style="margin-top:10px;">
            		<div class="long_content container text-center" id="gave_feedback" style="width:auto;">
            		</div> 
                 </div>
            </div>
           
	      </div>
    </div>
<div id="close_drobe_id"></div>
<script type="text/javascript">
$(document).ready(function(){
	activate_menu("result");	
		
	var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
	$('.TabbedPanelsTab').click(function(){
			current_index=TabbedPanels1.getCurrentTabIndex();
			
			var selectedtab = '.TabbedPanelsContentGroup > div:eq('+current_index+') .infiniteScroll';
			
			
			if($(selectedtab)[0].fleXcroll == undefined)
			{
			   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
			   apply_infinite_scrolling($(selectedtab).attr('id'));
			}	 
	});

	load_got_feedbacks("");
	load_gave_feedbacks("");
	
	 $('#got_feedback_search').keypress(function(event) {
 	    if (event.which == 13) {
 	    	load_got_feedbacks($(this).val());
 	    }
 	});
     $('#got_feedback_search').blur(function() {
    	 load_got_feedbacks($(this).val());
 	});

     $('#gave_feedback_search').keypress(function(event) {
  	    if (event.which == 13) {
  	    	load_gave_feedbacks($(this).val());
  	    }
  	});
      $('#gave_feedback_search').blur(function() {
     	 load_gave_feedbacks($(this).val());
  	});
    	
	
    $('#close_yes').click(function(){
		if($('#close_drobe_id').html()!="")
		{
			$.ajax({
				url: 'drobes/close',
				type: "POST",
				data: {
					drobe_id: $('#close_drobe_id').html()
				},
				success: function(data) {
					$('TabbedPanelsTab:first').click();
				  	$('#close_drobe_id').html("");
			  	}
			});
		}
		else
		{
			alert("Error occured in requesting close for rate");
			$("#close_no").click();
		}
	});
	$('#close_no').click(function(){
		$.unblockUI();
		$('#close_drobe_id').html("");
	});
});
function set_scrollbar(index)
{
   	var selectedtab = '.TabbedPanelsContentGroup > div:eq('+index+') .infiniteScroll';
	if($(selectedtab)[0].fleXcroll== undefined)
	{
	   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
	   apply_infinite_scrolling($(selectedtab).attr('id'));
	}
}

function load_got_feedbacks(search)
{
	search = escape(search);
	$('#got_feedback').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"received"))?>/search:'+search).attr('page',0).attr('last',1);
	$('#got_feedback').load('<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"received"))?>/search:'+search,function(){
    	$('.TabbedPanelsTab:eq(0)').click();
        set_scrollbar(0);
    });
    
}

function load_gave_feedbacks(search)
{
	search = escape(search);
	$('#gave_feedback').attr('rel','<?php echo $this->Html->url(array('controller'=>"rates",'action'=>"given"))?>/search:'+search).attr('page',0).attr('last',1);
    $('#gave_feedback').load('<?php echo $this->Html->url(array('controller'=>"rates",'action'=>"given"))?>/search:'+search,function(){set_scrollbar(1);});
}
</script>

