<?php
if(isset($drobe_uploaded) && $drobe_uploaded==true)
{
	$category=Cache::read('drobe_category');
	?>
	<div class="drobe_uploaded">
	<div class="left">
		<span class="big-txt"><?php echo $this->data['Drobe']['comment']; ?></span>
	</div>
	<div class="clear"></div>
	<div class="right"><?php echo $category[$this->data['Drobe']['category_id']]; ?></div>
	
	<div class="clear_outer_space">
	<div class="big-img" style="overflow:hidden;"><?php 
	echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name'],array('width'=>Configure::read('drobe.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));
	?></div>
	</div>
	    <h2>
          That's it!
        </h2>
          <p>
            Check responses in a couple of<br>
            minutes to see lots of opinions.
          </p>
          <p>
            While you're waiting, go get<br>
            some stars.
          </p>
          <div class="actions">
          <p>
          <?php 
          echo $this->Html->link('Give Rate',array('controller'=>"drobes","action"=>'rate'),array("class"=>"blue_btn"));
          ?>
          </p>
          </div>
      
	</div>
	<?php
}
else
{
echo $this->Form->create('Drobe',array('class'=>"form", 'type'=>'file'));
?>
<div class="drobe_preview clear_outer_space">
<div class="big-img" style="overflow:hidden;"><img src="images/big-photo.jpg" /></div>
<div id="clear_preview" class="right">Change</div>
<div class="clear"></div>
</div>
<?php 
echo $this->Form->file('file',array('label'=>false,'id'=>'file_upload'));
echo $this->Form->hidden('file_name');
?>
<div class="spacer"></div>
<div class="details"><?php echo $this->Form->input('category_id',array('label'=>false,'options'=>$categoryList,'empty'=>"Select Category"));?></div>
<div class="more_margin" >
<?php echo $this->Form->textarea('comment',array('label'=>false,"cols"=>32,"rows"=>3)); ?>
</div>

<div class="more_margin" style="margin-left:20px;">
<p>
<?php 
echo $this->Form->input("DrobeSetting.ask_public",array("class"=>"required","value"=>"1",'type'=>"checkbox","checked"=>true));
?>
</p>
<p>
<?php 
echo $this->Form->input("DrobeSetting.ask_friends",array("value"=>"1",'type'=>"checkbox","checked"=>"checked"));
?>
</p>
</div>
<div class="clear"></div>
<div class="more_options"><label>More Options</label>
<div>
<?php 
echo $this->Form->input("DrobeSetting.facebook",array("value"=>"1",'type'=>"checkbox"));
echo $this->Form->input("DrobeSetting.twitter",array("value"=>"1",'type'=>"checkbox"));
?>
<div class="clear"></div>
<?php 
echo $this->Form->input("DrobeSetting.male",array("value"=>"1",'type'=>"checkbox",'label'=>"Males","checked"=>true));
echo $this->Form->input("DrobeSetting.female",array("value"=>"1",'type'=>"checkbox",'label'=>"Females","checked"=>true));
?>


</div>
</div>

<div class="full">
<?php 
echo $this->Form->submit('SUBMIT',array('class'=>"green_btn"));
?>
 
<div class="share" style="float:right" ><a href="#" rel="shareit"><img src="images/share.png" border="0"></a>
<div id="shareit-box">

<div id="shareit-header"></div>
<div id="shareit-body">
<div id="shareit-blank"></div>
<div id="shareit-icon">
<ul>
<li> <iframe src="https://www.facebook.com/plugins/like.php?href=YOUR_URL"
scrolling="no" frameborder="0"
style="border:none; width:50px; height:30px"></iframe></li>
 
<li><a href="http://pinterest.com/USERNAME/"><img src="images/pinit.png" alt="Follow Me on Pinterest" border="0" /></a></li>

<li><a href="https://twitter.com/share" class="twitter-share-button" data-related="jasoncosta" data-lang="en" data-size="large" data-count="none"><img src="https://apmcommunity.compuware.com/community/download/attachments/25789254/tweetbutton.png" border="0" /></a><script>!function(d,s,id){
	var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){
		js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);
	}
}(document,"script","twitter-wjs");</script></li>
</ul>
</div>
</div>
</div></div>
</div>

<?php 
echo $this->Form->end();
?>
<script type="text/javascript">
$(function() {
    $('#file_upload').uploadify({
        'swf'      : '<?php echo $this->params->webroot?>uploadify/uploadify.swf',
        'uploader' : 'http://<?php echo $_SERVER['HTTP_HOST'].$this->Html->url(array('controller'=>"uploads","action"=>"file",'drobes'));?>',
        'buttonText' : '<div class="big-img"><img src="images/big-photo.jpg" /></div>',
        'formData'      : {'type' : 'drobes'},
        'height':230,
        'width':535, 
        'onUploadSuccess' : function(file, data, response) {
            var obj=jQuery.parseJSON(data);
            if(obj.type=="success")
            {
                $('#DrobeFileName').val(obj.file);
                $('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')).'/')?>" + obj.file);
                $('.drobe_preview').show();
                $('#file_upload').hide();
                $('#DrobeCreateForm input[type="submit"]').removeAttr('disabled');
            }
            else
            {  
            	alert(obj.message);
            	 $('#DrobeFileName').val("");
            	$('.drobe_preview img').attr('src',"");
            	$('.drobe_preview').hide();
                $('#file_upload').show();
                $('#DrobeCreateForm input[type="submit"]').attr('disabled','disabled');
            }
        }
        // Put your options here
    });
    //$('.drobe_preview .big-img').mouseenter(function(){$('#clear_preview').show();}).mouseleave(function(){$('#clear_preview').hide();});
    
    $('.more_options > label').toggle(function(){
        $('.more_options > div').show();
        $('.more_options > label:before').css('content','- ');
        },
        function(){
        	$('.more_options > div').hide();
        	$('.more_options > label:before').css('content','+ ');
    });
    $('#clear_preview').click(function(){
    	$('#DrobeFileName').val("");
     	$('.drobe_preview img').attr('src',"");
     	$('.drobe_preview').hide();
        $('#file_upload').show().click();
    });
    $('#DrobeCreateForm input[type="submit"]').attr('disabled','disabled');
    <?php 
    if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']!="")
    {
    	?>
    	$('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')).'/'.$this->data['Drobe']['file_name'])?>" );
        $('.drobe_preview').show();
        $('#file_upload').hide();
        $('#DrobeCreateForm input[type="submit"]').removeAttr('disabled');
    	<?php 
    }
    ?> 
});
</script>
<?php 
}
?>