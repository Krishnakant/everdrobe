<h2>Edit Drobe Question</h2>
<div class="actions right">
<?php 
echo $this->Html->link("Back to Drobe Detail",array("controller"=>"drobes","action"=>"detail",$this->data['Drobe']['id'],"admin"=>true));
?>
</div>
<br>
<div style="width:68%" id="sell_drobe_area">
<div style="position:relative" id="sell_drobe_preview">
<?php  
	echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name'],array('width'=>"450px",'border'=>0, 'id'=>"crop_image" ,"alt"=>"drobe prview Image")); 
?>
</div>
<div class="drobe_upload_msg" id="file-uploader"></div>
<?php 
echo $this->Form->create("Drobe");
echo $this->Form->input("comment",array("style"=>"width:100%",'id'=>'sell_drobe_comment'));
?>
<div class="spacer"></div>
<input type="hidden" id="pos_left" name="pos_left" />
<input type="hidden" id="pos_top" name="pos_top" />
<input type="hidden" id="new_width" name="new_width" />
<input type="hidden" id="new_height" name="new_height" />
<input type="hidden" name="data[Drobe][file_name]" id="DrobeFileName" value="<?php echo $this->data['Drobe']['file_name'];?>" />
<input type="hidden" id="sell_drobe_file_name" name="old_file" value="<?php echo $this->data['Drobe']['file_name'];?>" />
<?php
echo $this->Form->input("category_id",array('options'=>$categories,"style"=>"width:100%",'id'=>'drobe_category_id_sell'));
?>
<div class="spacer"></div>
<?php
if($this->data['SellDrobe']['sold']=="no")
{
	echo $this->Form->input("post_type",array('options'=>array('post'=>"Post",'sell'=>"Sell"),"style"=>"width:100%"));
	?>
	<div class="spacer"></div>
	<?php
	//echo $this->Form->input("SellDrobe.sell_brand_name");
	/* Brand name load from brand table @sadikhasan*/
	$brandName = array_search($this->data['SellDrobe']['sell_brand_name'], $brandList);
	echo $this->Form->input("SellDrobe.brand_id",array('options'=>$brandList,'default'=>$brandName, "style"=>"width:100%",'id'=>'drobe_brand'));
	?>
	<div class="spacer"></div>
	<?php
	$sizeName = array_search($this->data['SellDrobe']['size_name'], $sizeList);  //for default selection of value in combobox @sadikhasan
	echo $this->Form->input("SellDrobe.size_id",array('options'=>$sizeList,'default'=>$sizeName, "style"=>"width:100%",'id'=>'drobe_size'));
	?>
	<div class="spacer"></div>
	<?php
	echo $this->Form->input("SellDrobe.sell_price",array("type"=>"text","label"=>"Listing Sell Price",'id'=>'drobe_price'));
	?>
	
	<div class="spacer"></div>
	<?php
	echo $this->Form->input("SellDrobe.original_sell_price",array("type"=>"text",'id'=>'original_drobe_price'));
	?>
	
	<div class="spacer"></div>
	<?php
	echo $this->Form->input("SellDrobe.sell_description",array("type"=>"textarea"));
	?>
	<div class="spacer"></div>
	<?php
	echo $this->Form->input("SellDrobe.tracking_number",array("label"=>"UPS/USPS/Fedex Tracking Number"));
	?>
	<div class="spacer"></div>
	<?php
	if(isset($this->data['SellDrobe']['id']) && $this->data['SellDrobe']['id']>0)
	{
		echo $this->Form->input("SellDrobe.id",array("type"=>"hidden"));
	}
}
echo "<p>This drobe is sold so you you can not change sell detail.</p>";
echo $this->Form->input("id",array("type"=>"hidden"));
echo $this->Form->submit("Save Changes",array('id'=>'drobe_edit'));
?>
</div>
<script type="text/javascript">
var is_normal_drobe = false;
var last_uploaded_response=null;
var is_google_image = false;   //This variable is used to check image upload from google search if image from google search then its value is true other wise false

/****************Aviary image upload code ***********************************/
	var featherEditor = new Aviary.Feather({
	    apiKey: '<?php echo Configure::read('aviary_key'); ?>',
	    apiVersion: 3,
	    theme: 'dark', // Check out our new 'light' and 'dark' themes!
	    tools: 'all',
	    appendTo: '',
	    onSave: function(imageID, newURL) {
	    	upload_image_from_url(newURL,false);
	        return true;
	    },
	    onClose:function()
	    {
	    	display_image_cropping(last_uploaded_response);
	    },
	    onError: function(errorObj) {
	        alert(errorObj.message);
	    }
	});


var max_width=450;
var max_height=450;
var display_ratio=1;
var drobe_image_dir="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')),true).'/'; ?>";
var uploader = new qq.FileUploader({
    element: $('#file-uploader')[0],
    uploadButtonText: "Click here to upload Image",
    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'original')); ?>',
    params: {type:'drobes'},
    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
    debug: false,
    sizeLimit:4097152,
    onSubmit: function(id, fileName){
        if(typeof jcrop_api != 'undefined')
        {
        	jcrop_api.destroy();
        }  
        //$("#file-uploader").hide();
    	$('#sell_drobe_preview').block({ 
			message: "<h3>Uploading Image</h3>",
	    	css: { 
	            border: 'none', 
	            padding: '10px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px !important', 
	            '-moz-border-radius': '10px !important', 
	            opacity: .5, 
	            color: '#fff' 
        } });
	  },
    onComplete: function(id, fileName, responseJSON){
        upload_response(responseJSON);
       	
	},
	onCancel: function(id, fileName){},
	messages: {
		typeError: "You can upload only JPEG, PNG or GIF Image file.",
        sizeError: "You can upload less then 2MB file size.",
        emptyError: "Uploaded file is empty.",
	 },
	 showMessage: function(message){ 
		 tip_message('#file-uploader',message);
	}
});
function upload_response(obj)
{
	last_uploaded_response = obj;
	if(obj.type=="success")
    {
	    display_image_cropping_new(obj);
    }
    else
    {  
        alert(obj.message);
    	$('#DrobeFileName').val("");
    	$('#drobe_upload_btn').attr('disabled','disabled');
    }
}
function display_image_cropping_new(response)
{
	$('#DrobeFileName').val(response.file);

	$('#crop_image').attr('src',drobe_image_dir + response.file);

	var width= response.size.width;
	var height= response.size.height;

    if(max_width < width)
    {
    	$('.drobe_preview img').attr("width",max_width).removeAttr("height");
    	height=height/(width/max_width);
    	if(max_height < height) $('.drobe_preview img').attr("height",max_height).removeAttr("width");
    }
    if(max_height < height) $('.drobe_preview img').attr("height",max_height).removeAttr("width");
    $('#crop_image').load(function(){
    	try
    	{
	    	// display image cropping area with image after loading a image;
	    	$('#file_uploader').hide();
	    	$('.drobe_preview').show().find('.big-img').css({'background':"#EDEDED"});
	    	$("#sell_drobe_preview").unblock();
	    	img_width=$(this).width();
	    	img_height=$(this).height();

	    	

	    	var src = $("#crop_image").attr("src");
	    	//Call avaiary image function launchEditor for displaying image with its effects
	    	featherEditor.launch({
		        image: "crop_image",
		        url: src
		    });
			$(this).unbind();
	    	/*$('#crop_image').Jcrop({
	        	aspectRatio: 1,
	        	minSize: [ 320, 320],
	        	onSelect: updateCoords,
	        	trueSize: [response.size.width,response.size.height]
	        	},
	        	function(){
	        		bounds = this.getBounds();
	            	boundx = bounds[0];
	            	boundy = bounds[1];
	            	jcrop_api = this;
	            	if(boundx>boundy)
	            	{
	            		left_cords= parseInt((boundx-boundy)/2);
	            		jcrop_api.animateTo([left_cords,0,boundx,boundy]);
	            		updateCoords({x:left_cords,y:0,w:boundy,h:boundy});
	            	}
	            	else
	            	{
	            		top_cords= parseInt((boundy-boundx)/2);
	            		jcrop_api.animateTo([0,top_cords,boundx,boundy]);
	            		updateCoords({x:0,y:top_cords,w:boundx,h:boundx});
	            	}
	        });*/
    	}
    	catch(e)
    	{
    		alert(e.valueOf());
    	}
    });
   // $.colorbox.close();
}

$(document).ready(function()
	{
		/* Check validation for comment if it is blank do not submit the form*/
		$('#drobe_edit').click(function(){
			var comment = $.trim($('#sell_drobe_comment').val());
			if(comment=="")
			{	
				$("#sell_drobe_comment").qtip({
					content: {text: 'Comment is required', title: false},
					show: {event: false,ready: true},
					position: {at: "top center",my: "bottom center"},
					style: {classes: 'ui-tooltip-shadow ui-tooltip-dark big-tip'}
				}).focus();
				return false;
			}
			else
			{
				/* Removing qtip message when form is proper with validation */
				$('.qtip').remove();
				return true;
			}		
		});
	});
/** Submitting form disable from user when click on submit button */
$("#DrobeAdminEditForm").submit(function(){
	$('#sell_drobe_area').block({ 
		message: "<h3>Submitting</h3>",
    	css: { 
            border: 'none', 
            padding: '10px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px !important', 
            '-moz-border-radius': '10px !important', 
            opacity: .5, 
            color: '#fff' 
    	}
    });
}); 
</script>