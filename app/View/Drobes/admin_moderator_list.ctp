<h2>Moderate Images</h2>
<p>Click on thumb for view enlarge image. Right click on thumb for display action menu popup.</p>  
<div class="moderator_box">
	<div class="drobes" id="normal_drobes">
	<div>
	<?php 
		if(count($images)>0)
		{
			foreach($images as $image)
			{
				if($image['Image']['image_type']=="drobe")
				{
					echo  $this->Html->link( $this->Html->image('/drobe_images/thumb/'.$image['Image']['file_name'],array('height'=>"51px", 'width'=>"51px")),
							'/drobe_images/'.$image['Image']['file_name'],
							array("border"=>0,'escape'=>false,"class"=>"example8 drobe_thumb_small drobe_image", "rel"=>$image['Image']['drobe_id']."::".$image['Image']['user_id']."::0"));
				}
				else
				{ 
					echo  $this->Html->link( $this->Html->image('/drobe_images/additional/thumb/'.$image['Image']['file_name'],array('height'=>"51px", 'width'=>"51px")),
						'/drobe_images/additional/'.$image['Image']['file_name'],
						array("border"=>0,'escape'=>false,"class"=>"example8 drobe_thumb_small additional_image", "rel"=>$image['Image']['drobe_id']."::".$image['Image']['user_id']."::".$image['Image']['sell_drobe_id'], "id"=>$image['Image']['image_id']));
				}
			}
		}
		else
		{
			?>
			<?php 
		}
	?>
	<div class="clear"></div>
	</div>
	</div>
</div>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>

<div id="user_options" style="display: none;">
	<ul class="submenu actions">
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"drobes","action"=>"action","delete","admin"=>true));?>/" class="remove_drobe">Remove Drobe</a></li>
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","block","admin"=>true));?>/" class="block_user">Block User</a></li>
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"sell_drobes","action"=>"remove_image","admin"=>true));?>/" class="remove_image">Remove Image</a></li>
	</ul>
	<div class="clear"></div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.drobe_thumb_small').bind('contextmenu',function(e){
	  	e.preventDefault();
	  	var drobe_id=$(this).attr("rel").split("::")[0];
	  	var user_id=$(this).attr("rel").split("::")[1];
	  	var sell_drobe_id=$(this).attr("rel").split("::")[2];
		if($(this).hasClass("additional_image"))
		{
			$('#user_options a.remove_image').attr("href",$('#user_options a.remove_image').attr('rel')+$(this).attr("id")).parent().show();
		}
		else
		{
			$('#user_options a.remove_image').parent().hide();
		}
		$('#user_options a.remove_drobe').attr("href",$('#user_options a.remove_drobe').attr('rel')+drobe_id);
		$('#user_options a.block_user').attr("href",$('#user_options a.block_user').attr('rel')+user_id);

		$(this).qtip({
			position : { my: 'top left', at: 'top left' } ,
			content: {
				text: $('#user_options').html(), // Use the submenu as the qTip content
			},
			show: {
				event: 'click',
				ready: true // Make sure it shows on first mouseover
 			},
			hide: {
				delay: 100,
				event: 'unfocus mouseleave',
				fixed: true // Make sure we can interact with the qTip by setting it as fixed
			},
 			style: {
				classes: 'ui-tooltip-nav ui-tooltip-light', // Basic styles
				tip: false // We don't want a tip... it's a menu duh!
			},
		});
	    return false;
     });
});
	</script>