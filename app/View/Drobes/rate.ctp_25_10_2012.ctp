<?php
if(!$is_ajax)
{
	?>
	<div id="fb-root"></div>
	<div id="rate-drobe-container" class="container">
	<?php
	if($this->Session->read('Auth.User.id')>0)
	{
		//echo $this->element('filter_category');
	} 
	else
	{
		echo $this->element('user_login_info');
	}
	
	?>
	<div id="flag_block" class="flag_form">
<h2>Flag drobe</h2>
<div class="spacer"></div>
<?php 
echo $this->Form->input('flag_category',array('options'=>Cache::read('flag_category'),'id'=>'flag_category','label'=>false,'div'=>false,'empty'=>"Select Reason"));
?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_flag"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_flag"));
?>
<div class="spacer"></div>
</div>
	<div class="rate_response" id="rate_response" style="display:none">
	</div>
	<div id="rate_drobe_block">
	<?php 
}
if($drobe)
{
	//fixing issue for enter and return character in javascript
	$drobe['Drobe']['comment']=trim(str_replace(array("\r","\n"), " ", $drobe['Drobe']['comment']));
	
	?>
	<<h3><?php  echo $drobe['Category']['category_name']; ?></<h3>
	<div class="big-img drobe_big_img"  >
	<div class="drobe_comment"><?php  echo $drobe['Drobe']['comment']; ?></div>
 	<?php 
 	echo $this->Html->image('/drobe_images/'.$drobe['Drobe']['file_name'],array('border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));
 	?>
 	<div class="flag_drobe">
 	<div class="summary">
    	<div class="imp">Views : <?php echo $drobe['Drobe']['views']?></div>
        <div class="imp">Cute : <?php echo $drobe['Drobe']['total_in'];?></div>
        <div class="imp">Hot : <?php echo $drobe['Drobe']['total_out'];?></div>
    </div>
  	<?php 
 	if($this->Session->read('Auth.User.id')>0)
 	{
 	?>
 	<a href="javascript:void(0)" id="flag_button"><?php echo $this->Html->image('/images/flag.png',array("height"=>14));?> Flag</a>
 	<?php 
	}
 	?>
 	</div>
 	
 	</div>
    
    <div class="over">
    
    <script src="https://apis.google.com/js/plusone.js"></script>
	<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
	
    <?php
    if($my_drobe)
    {
    	?>
            <a href="javascript:void(0)" class="red_btn_disabled" style="margin-left:<?php  echo ($drobe['Drobe']['featured']==1 && $drobe['Drobe']['buy_url']!="") ? '50':'100';?>px;"><div class="white_plus"></div><span class="btn_txt">Follow</span></a>
        <?php  
    } 
    else if(!$is_followed)
    {
    ?>
        <a href="javascript:void(0)" class="red_btn" id="follow_button" style="margin-left:<?php  echo ($drobe['Drobe']['featured']==1 && $drobe['Drobe']['buy_url']!="") ? '50':'100';?>px;"><div class="white_plus"></div><span class="btn_txt">Follow</span></a>
     <?php 
    }
    else
    {
     ?>
     <a href="javascript:void(0)" class="green_btn" id="follow_button" style="margin-left:100px;"><div class="white_minus"></div><span class="btn_txt">Following</span></a>
     <?php 
    }
     ?>   
        <a href="javascript:void(0)" class="heart" id="fave_button" <?php if(isset($is_my_favourite) && $is_my_favourite>0) echo 'disabled="disabled"'; ?>><span class="btn_txt"><?php  echo $drobe['Drobe']['favs']; ?></span></a>
       <div class="share">
       		<a href="javascript:void(0)" rel="shareit"><img src="images/share.png" border="0"></a>
           
            <div id="shareit-icon">
  <ul>
   <li>
	   <div class="fb_like">
	   <fb:like href="<?php echo Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true);?>" send="false" width="50" show_faces="false"></fb:like>
	   </div>
   </li>
   <li>
   		<a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true));?>&media=<?php echo urlencode(Router::url('/drobe_images/'.$drobe['Drobe']['file_name'],true))?>&description=<?php echo urlencode($drobe['Drobe']['comment'])?>" class="pin-it-button" count-layout="none" target="_blank"><img src="images/pinit.png" alt="Pin it" border="0" /></a>
   </li>
   <li>
   		<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $drobe['Drobe']['short_url'];?>" data-text="<?php echo $drobe['Drobe']['comment'];?>" data-count="none"><img src="https://apmcommunity.compuware.com/community/download/attachments/25789254/tweetbutton.png" border="0" /></a>
	</li>
	<li>
	<div class="gplus_share">
	<g:plus action="share" href="<?php echo Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true)?>" annotation="none" ></g:plus>
	</div>
	</li>
  </ul>
 </div>
       </div>
       <?php 
       if($drobe['Drobe']['featured']==1 && $drobe['Drobe']['buy_url']!="")
       {
       echo '<div class="buy_it">'.$this->Html->link($this->Html->image('/images/btn_buy_it.png',array("border"=>0)),$drobe['Drobe']['buy_url'],array("target"=>"_blank","escape"=>false)).'</div>';
       }
       ?>
     </div>
     <?php 
     if(!$is_rated)
     {
     ?>
     <div class="more_margin" style="margin-top:0px">
        <div class="more_margin" style="margin-top:4px"><textarea cols="28" rows="3" id="drobe_comment" class="comment" placeholder="Leave a comment"></textarea></div>
        <div class="more_margin" style="margin-top:0px"> <a href="javasript:void(0)" class="in ratebutton" id="drobe_rate_in" rel="1"></a></div>
        <div class="more_margin" style="margin-top:0px"><a href="javasript:void(0)" class="out ratebutton" id="drobe_rate_in" rel="-1"></a></div>
        <div class="more_margin" style="margin-top:0px"><a href="javasript:void(0)" class="whtever ratebutton" id="drobe_rate_in" rel="0"></a></div>
     </div>
     <?php 
     }
     else
     {
     	?>
     	<div class="clear"></div>
     	<div class="more_margin your_rate full_width" style="margin-top:0px"> 
	     	<a  class="<?php echo $rate_info['rate']=="0" ? "whtever" : $rate_info['rate']=="1" ? "in" : "out"; ?>"></a>
	     	<?php echo $rate_info['comment']=="" ? "<div class='no_comment'>Not Commented</div>" : "<div class='comment'>".$rate_info['comment']."</div>";?>
	     	<div id="next_drobe">Next</div>
     	</div>
     	
    	<?php 
     }
	
	?>
	<input type="hidden" id="drobe_id" value="<?php echo $drobe['Drobe']['unique_id']; ?>" />
	
	
	
<?php
}
else
{
?>
<div class="drobe_rate_empty">
<h1>Drobes not matched  as your criterea for Rate</h1>
</div>
<?php 
}
?>

<?php 
if(!$is_ajax)
{
?>
</div>
</div>
<div class="clear"></div>
<?php 
}
?>
<script>
$('document').ready(function(){
	var all_checked= ($(".filter_drobe .categories input[type='checkbox']").not("#check-all").length==$(".filter_drobe .categories input[type='checkbox']:checked").not("#check-all").length);
	$("#check-all").attr('checked',all_checked);
	$('.drobe_big_img > img').load(function(){$(this).center(true)}).fadeIn();	
});
var is_colorbox=false;
var is_selected=<?php echo (isset($this->params->params['pass'][0]) && $this->params->params['pass'][0]!="")? "true": "false" ?>;
<?php if($this->Session->read('Auth.User.id')>0){?>
$.getScript("js/rate_drobe.js");
<?php 
}
else
{
?>
$.getScript("js/read_only.js");
<?php 
}
?>
<?php 
if($drobe['Drobe']['comment'])
{
?>
	$('title').html("<?php echo "Everdrobe - ".addslashes($drobe['Drobe']['comment']); ?>");
	$('meta[property="og:title"]').attr('content', "<?php echo addslashes($drobe['Drobe']['comment']); ?>");
<?php 
}
?>
$('meta[property="og:image"]').attr('content', "<?php echo Router::url('/drobe_images/'.$drobe['Drobe']['file_name'],true);?>");
$('meta[property="og:url"]').attr('content', "<?php echo Router::url("/drobes/rate/".$drobe['Drobe']['unique_id'],true);?>");
</script>

