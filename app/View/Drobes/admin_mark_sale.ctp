<h2>Mark for Sale Drobe [#<?php echo $this->data['Drobe']['id']?>]</h2>
<div class="actions right">
<?php 
echo $this->Html->link("View Sell Drobes",array("controller"=>"drobes","action"=>"sell_drobes",$this->data['Drobe']['id'],"admin"=>true));
?>
</div>
<div style="width:75%">
<?php
echo $this->Form->create("Drobe");
echo $this->Form->input("SellDrobe.sell_brand_name");
?>
<div class="spacer"></div>

<div class="input select">
<?php $sizeList['Other']='Other';
	echo $this->Form->input('SellDrobe.size_id', array("options"=>$sizeList,'class'=>"drob_size_custom",'div'=>false,"style"=>"width:100%"));
	if($sizeName=array_search($this->data['SellDrobe']['size_name'],$sizeList) || $this->data['SellDrobe']['size_name']=='')
	{
		
	}
	else
	{
		$sizeName=$this->data['SellDrobe']['size_name'];
		echo "<div class='spacer'></div><input type='text' id='drobe_size_name' name='data[SellDrobe][size_name]' value='".$sizeName."' />";
	}
?>
</div>
<?php
//echo $this->Form->input("SellDrobe.size_id",array('options'=>$sizeList,"style"=>"width:100%"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("SellDrobe.sell_price",array("type"=>"text"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("SellDrobe.sell_description",array("type"=>"textarea"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("SellDrobe.tracking_number",array("label"=>"UPS/USPS/Fedex Tracking Number"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("Drobe.id",array("type"=>"hidden"));
echo $this->Form->input("SellDrobe.id",array("type"=>"hidden"));
echo $this->Form->input("Drobe.post_type",array("type"=>"hidden","value"=>"sell"));
echo $this->Form->input("SellDrobe.status",array("type"=>"hidden","value"=>"approved"));
echo $this->Form->input("Drobe.featured",array("type"=>"hidden", "value"=>"1"));
echo $this->Form->submit("Mark for Sale");
?>
</div>