<div class="spacer"></div>
<div class="right actions">
<?php 
echo $this->Html->link('Drobe Settings', array("controller"=>"settings","action"=>"index","admin"=>true))." ";
echo $this->Html->link('Active Sell Drobes', array("controller"=>"drobes","action"=>"sell_drobes","admin"=>true));
?>
</div>
<h2 class="left">Close Drobe Request Pending</h2>
<div class="clear"></div>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search Drobe : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search drobe by comment, user name, category or brand name" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($drobes)>0)
{
	$drobe_size=Cache::read('size_list');
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('Drobe.id','#');?></th>
			<th>Image</th>
			<th width="200px"><?php echo $this->Paginator->sort('Drobe.comment','Comment');?></th>
			<th  width="200px"><?php echo $this->Paginator->sort('User.first_name','Uploaded by');?></th>
			<th width="200px"><?php echo $this->Paginator->sort('User.username','Uploaded User Name');?></th>
			<th width="75"><?php echo $this->Paginator->sort('SellDrobe.sell_brand_name','Brand Name'); ?></th>
			<th width="75"><?php echo $this->Paginator->sort('SellDrobe.sell_price','Price'); ?></th>
			<th width="75"><?php echo $this->Paginator->sort('SellDrobe.size_name','Size'); ?></th>
			<th width="75">Summary</th>
		</tr>
		<?php 
		foreach($drobes as $drobe)
		{
			?>
			<tr class="<?php echo $drobe['Drobe']['id']; ?>">
				<td width="30px" rowspan="2"><?php echo $drobe['Drobe']['id'];?></td>
				<td width="90px" rowspan="2"><?php echo $this->Html->link($this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image")),$this->Html->url('/drobe_images/'.$drobe['Drobe']['file_name'],true),array("escape"=>false,"class"=>"example8"));?></td>
				<td width="200px">
				<?php
					if($drobe['Drobe']['comment']!="") echo "<div class='comment'>".$drobe['Drobe']['comment']."</div>";
					if($drobe['Category']['category_name']!="") echo "<em>(".$drobe['Category']['category_name'].")</em>";
				?>
				</td>
				<td><?php echo $this->Html->link($drobe['User']['first_name']." ".$drobe['User']['last_name'],array("controller"=>"users","action"=>"view","admin"=>true,$drobe['User']['id']));?></td>
				<td><?php echo $drobe['User']['username'];?></td>
				<td><?php echo $drobe['SellDrobe']['sell_brand_name'];?></td>
				<td><?php echo "$ ".$drobe['SellDrobe']['sell_price'];?></td>
				<td><?php echo $drobe['SellDrobe']['size_name'];?></td>
				<td width="200px">
					<div class="actions">
						<?php echo $drobe['SellDrobe']['sell_description'];?>
					</div>
				</td>
			</tr>
			<tr>
			<td colspan="100%">
					<div class="actions">
					<?php 
						echo $this->Html->link('View Detail',array("controller"=>"drobes","action"=>"detail","admin"=>true,$drobe['Drobe']['id']))." "; 
						//echo $this->Html->link('Remove',array("controller"=>"drobes","action"=>"action","remove","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to remove this drobe?"))." "; 
						echo $this->Html->link('View Sell Detail',array("controller"=>"sell_drobes","action"=>"edit","admin"=>true,$drobe['Drobe']['id']))." ";
						echo $this->Html->link('Approve for Close',array("controller"=>"drobes","action"=>"action","close","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to close this sell drobe?"))." ";
					?>
					</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="featured_drobe" style="display: none;" class="featured_form">
	<h2>Mark as Featured</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Drobe: </strong><span id="featured_drobe_name"></span></div>
		
		<?php 
			echo $this->Form->input('buy_url',array('id'=>'buy_url','label'=>false,'div'=>false,'placeholder'=>"Enter URL for buy drobe"));
			echo $this->Form->hidden("featured_drobe_id",array("id"=>"featured_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_featured"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_featured"));
?>
<div class="spacer"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	})
	$('.summary_btn').each(function(){
		var self = $(this);
		$(this).click(function(){
			$(this).qtip({
				position : { my: 'right top', at: 'top left' } ,
				content: {
					text: self.next().html(), // Use the submenu as the qTip content
				},
				show: {
					event: 'click',
					ready: true // Make sure it shows on first mouseover
	 			},
				hide: {
					delay: 100,
					event: 'unfocus mouseleave',
					fixed: true // Make sure we can interact with the qTip by setting it as fixed
				},
	 			style: {
					classes: 'ui-tooltip-nav' // Basic styles
					//tip: false // We don't want a tip... it's a menu duh!
				},
			});
		});
	});

	$('.mark_featured').click(function(){
		$('#featured_drobe_name').html($(this).closest('tr').prev().find('td:eq(2)').html().replace("<br/>"," - "));
		$("#buy_url").val($(this).attr("rel"));
		$("#featured_drobe_id").val($(this).attr("id"));
		$.blockUI({ 
	        message: $('#featured_drobe'), 
	        css: { 
	            top:  ($(window).height()) /2 + 'px', 
	            left: ($(window).width()-300) /2 + 'px', 
	            width: '500px' 
	        } 
	    }); 
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI); 
	
	});

	$('#submit_featured').click(function(){
		if($('#buy_url').val()!="")
		{
			$.ajax({
					url: 'e-a-d-m-i-n-x/drobes/action/mark_featured/'+$("#featured_drobe_id").val(),
					type: "POST",
					data: {
						buy_url_actual:$('#buy_url').val()
					},
					success:function(data) {
						$.unblockUI();
						obj=$.parseJSON(data);
						if(obj.type=="error")
						{
							alert(obj.message);
						}	 
						else document.location.href=document.location.href;
					}
				});
		}
		else
		{
			$('#flag_category').focus();
		}
		return false;
	}); 
	$('#close_featured').click(function(){$.unblockUI()}); 
	
});
</script>