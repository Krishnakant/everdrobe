<?php
if(isset($drobe_uploaded) && $drobe_uploaded==true)
{
	$category=Cache::read('drobe_category');
	?>
	<div class="drobe_uploaded">
	<div class="left">
		<span class="big-txt"><?php echo $this->data['Drobe']['comment']; ?></span>
	</div>
	<div class="clear"></div>
	<div class="right"><?php echo $category[$this->data['Drobe']['category_id']]; ?></div>
	
	<div class="drobe_preview clear_outer_space" style="display: block">
	<div class="big-img"><?php 
	echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name'],array('border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));
	?></div>
	</div>
	    <h2>
          That's it!
        </h2>
          <p>
            Check responses in a couple of<br>
            minutes to see lots of opinions.
          </p>
          <p>
            While you're waiting, go get<br>
            some stars.
          </p>
          <div class="actions">
          <p>
          <?php 
          echo $this->Html->link('Give Rate',array('controller'=>"drobes","action"=>'rate'),array("class"=>"blue_btn"));
          ?>
          </p>
          </div>
      
	</div>
	
	<?php
}
else
{
?>
<div class="drobe_preview clear_outer_space">
<div class="big-img" style="overflow:hidden;"><img src="images/big-photo.jpg" /></div>
<div id="clear_preview" class="right">X</div>
<div class="clear"></div>
</div>
<div class="big-img" id="file_uploader" <?php if(isset($drobe_uploaded) && $drobe_uploaded==false) echo "style='display:none'"; ?>>
<div class="drobe_upload_msg" id="file-uploader">Click here to upload File</div>
<img src="images/big-photo.jpg" />
</div>
<?php
echo $this->Form->create('Drobe',array('class'=>"form", 'type'=>'file'));

echo $this->Form->hidden('file_name');
?>
<div class="spacer"></div>
<div class="details"><?php echo $this->Form->input('category_id',array('label'=>false,'options'=>$categoryList,'empty'=>"Select Category"));?></div>
<div class="more_margin" >
<?php echo $this->Form->textarea('comment',array('label'=>false,"cols"=>32,"rows"=>3)); ?>
</div>

<div class="more_margin" style="margin-left:20px;">
<?php 
echo $this->Form->input("DrobeSetting.ask_public",array("class"=>"required","value"=>"1",'type'=>"checkbox","checked"=>true));
?>
<?php 
echo $this->Form->input("DrobeSetting.ask_friends",array("value"=>"1",'type'=>"checkbox","checked"=>"checked"));
?>

</div>
<div class="clear"></div>
<div class="more_options"><label>More Options</label>
<div>
<?php 
echo $this->Form->input("DrobeSetting.male",array("value"=>"1",'type'=>"checkbox",'label'=>"Males","checked"=>true));
echo $this->Form->input("DrobeSetting.female",array("value"=>"1",'type'=>"checkbox",'label'=>"Females","checked"=>true));
echo $this->Form->input("DrobeSetting.facebook",array("value"=>"1",'type'=>"checkbox"));

//echo $this->Form->input("DrobeSetting.twitter",array("value"=>"1",'type'=>"checkbox"));
?>
</div>
</div>

<div class="full">
<?php 
echo $this->Form->submit('SUBMIT',array('class'=>"green_btn","disabled"=>"disabled","id"=>"drobe_upload_btn"));
?>
</div>
<?php 
echo $this->Form->end();
?>
<form action="<?php echo $this->Html->url(array("controller"=>"uploads","action"=>"file","drobes"));?>" id="file_upload_form" enctype="multipart/form-data" method="post" style="display: none">
<input type="file"  name="qqfile" id="file_input"/>
<input type="hidden" name="type" value="drobes" />
<div id="upload_response"></div>
</form>
<script type="text/javascript">
$(function() {
	if(!$.browser.msie && false)
	{

		$('#file-uploader').uploadify({
	        'swf'      : '<?php echo $this->params->webroot?>uploadify/uploadify.swf',
	        'uploader' : 'http://<?php echo $_SERVER['HTTP_HOST'].$this->Html->url(array('controller'=>"uploads","action"=>"file",'drobes'));?>',
	        'buttonText' : 'Click here to upload file',
	        'formData'      : {'type' : 'drobes'},
	        'fileObjName' : 'qqfile',
	        'height':50,
	        'width':405, 
	        'onUploadSuccess' : function(file, responseText, response) {
		       	      upload_response($.parseJSON(responseText));
	        }
	    });
	    
		
		/*$("#file_upload_form").ajaxForm({
			beforeSubmit: function(a,f,o) { },
			success :  function(responseText, statusText, xhr, $form)  {
				upload_response($.parseJSON(responseText));
			},  // post-submit callback 
			target: '#upload_response',
		  	resetForm : true,
		  	clearForm : true,
	    	type: 'post'
		});
		$("#file_input").change(function(){
			if($(this).val()!="")
			{
				try
				{
					$('#file_upload_form').submit();		 
				}
				catch(e)
				{
					alert(e.valueOf()) 
				}
			}
		});
		$('#file-uploader').click(function(){
			$("#file_input").focus().click().end();
		}) */
	}
	else
	{
		var uploader = new qq.FileUploader({
		    element: $('#file-uploader')[0],
		    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'drobes')); ?>',
		    params: {type:'drobes'},
		    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
		    debug: true,
		    onSubmit: function(id, fileName){},
		    onProgress: function(id, fileName, loaded, total){

			    },
		    onComplete: function(id, fileName, responseJSON){
			   upload_response(responseJSON);
			},
			onCancel: function(id, fileName){},
			messages: {
				 //alert("Error Message");
			     // error messages, see qq.FileUploaderBasic for content
			 },
			 showMessage: function(message){ alert(message); }
		});
	}

	
	$("#file_profile").change(function(){
		if($(this).val()!="")
		{
			try
			{
				$('#file_upload_form input[type="submit"]').click();	
				//fileUpload($('#file_upload_form')[0],$('#file_upload_form').attr('action'),'upload_response');	 
			}
			catch(e)
			{
				alert(e.valueOf()) 
			}
		}
	}); 
	$('#drobe_upload_btn').click(function(){
		if($('#DrobeCategoryId').val()=="")
		{
			$('#DrobeCategoryId').qtip({
				content: {
					text: 'Please select category before submit it!',
					title: false,
				},
				show: {
					event: false,
					ready: true
				},
				position: {
					at: "top left",
					my: "bottom left"
				}
			}).focus();
			return false;
		}
	});
	$('#clear_preview').click(function() {
		Confirm('Are you sure to change this image?', function(yes) {
			$('#DrobeFileName').val("");
	     	$('.drobe_preview img').attr('src',"");
	     	$('.drobe_preview').hide();
	        $('#file_uploader').show();
		});
	});
	
    $('#drobe_upload_btn').attr('disabled','disabled');
    <?php 
    if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']!="")
    {
    	?>
    	$('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')).'/'.$this->data['Drobe']['file_name'])?>" );
        $('.drobe_preview').show();
        $('#file_upload').hide();
        $('#drobe_upload_btn').removeAttr('disabled');
    	<?php 
    }
    ?> 
});
function upload_response(obj)
{
	if(obj.type=="success")
    {
        $('#DrobeFileName').val(obj.file);
        $('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir'))).'/'; ?>" + obj.file);
        $('.drobe_preview').show();
        $('#file_uploader').hide();
        $('#drobe_upload_btn').removeAttr('disabled');
        $('.drobe_preview img').load(function(){$(this).center(true)}).fadeIn();
    }
    else
    {  
    	$('#DrobeFileName').val("");
    	$('#drobe_upload_btn').attr('disabled','disabled');
    }
}
</script>
<?php 
}
?>