<?php 
if(isset($drobe_uploaded) && $drobe_uploaded==true)
{
	$category=Cache::read('drobe_category');
	?>
	<div class="drobe_uploaded mt30">
		
		<div >
		<i class="fa fa-thumbs-o-up" aria-hidden="true" style="font-size: 80px;"></i>
		<h2>Thank you</h2>
		<p>
			Check my results in a few minutes to see the results of your question.
		</p>
		<p>
			While you're waiting, please help others by rating their outfits.
		</p>
		</div>
				
		<div class="actions" style="margin-top:50px;">
	          <p>
	          <?php 
	          echo $this->Html->link('Rate Now',array('controller'=>"drobes","action"=>'rate',0,"recent","rate_stream"),array("class"=>"btn btn-success"));
	          ?>
	          </p>
	     </div>
	</div>
	
	<?php
}
else 
{
?>
<div id="my_drobe_area" class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 mt20" >

	<div class="drobe_preview big-img ">
	<div class="big-img" style="overflow:hidden;">
	<img src="images/big-photo.jpg" class="img-responsive" id="crop_image"/>
	</div>
	<div id="clear_preview" class="right">X</div>
	<div class="clear"></div>
	</div>
	<div class="big-img" id="file_uploader" <?php if(isset($drobe_uploaded) && $drobe_uploaded==false) echo "style='display:none'"; ?>>
	<div class="drobe_upload_msg" id="file-uploader">Click here to upload File</div>
	<img src="images/big-photo.jpg" class="img-responsive " />
	</div>
	<?php
	echo $this->Form->create('Drobe',array('class'=>"form", 'type'=>'file'));
	echo $this->Form->hidden('file_name');
	echo $this->Form->hidden('post_type',array('value'=>'post'));
	?>
	<div class="form-group">
		<?php echo $this->Form->input('category_id',array('label'=>false,"class"=>"form-control",'options'=>$categoryList,'default'=>"0"));?>
	</div>
	<div class="form-group" >
	<?php 
	// Check for facebook setting if user connected with facebook then is_fb_connected is 1 other wise 0 @sadikhasan
	if($this->Session->read('UserSetting.fb_connected'))	
	{
		echo $this->Form->hidden("is_fb_connected",array('id'=>'is_fb_connected','value'=>1));  // Flag value for user connected with facebook or not?
	}
	else
	{
		echo $this->Form->hidden('is_fb_connected',array('id'=>'is_fb_connected','value'=>0));
		echo $this->Form->hidden('fb_url',array('id'=>'fb_url','value'=>$facebookUrl));		//if user not connect with facebook then fb_url contains url for open popup window for facebook login
	}
		echo $this->Form->hidden("is_fb_post",array('id'=>'is_fb_post','value'=>0));  //set value if user want to post it's drobe to facebook site

	// Check for twitter setting if user connected with twitter then is_tw_connected is 1 other wise 0 @sadikhasan
	if($this->Session->read('UserSetting.tw_connected'))
	{
		echo $this->Form->hidden("is_tw_connected",array('id'=>'is_tw_connected','value'=>1));  // Flag value for user connected with twitter or not?
	}
	else
	{
		echo $this->Form->hidden('is_tw_connected',array('id'=>'is_tw_connected','value'=>0));
		echo $this->Form->hidden('tw_url',array('id'=>'tw_url','value'=>$twitterLoginUrl));		//if user not connect with twitter then tw_url contains url for open popup window for twitter login
	}
	echo $this->Form->hidden("is_tw_post",array('id'=>'is_tw_post','value'=>0));  //set value if user want to post it's drobe to twitter site
		
	?>
	<?php 
	echo $this->Form->textarea('comment',array('label'=>false,"cols"=>32,"rows"=>3,"class"=>"form-control","placeholder"=>"Type your question ...","maxlength"=>140)); 
	echo $this->Form->submit('SUBMIT',array('class'=>"btn btn-success mt10","id"=>"drobe_upload_btn"));
	?>
	<input type="button" class="btn btn-danger mt10" id="search_online_btn" value="Search Online" />
	
	<div class="share_btn">
	<?php
		echo $this->Html->link('','javascript:void(0);',array('id'=>'fb_connect','class'=>'fb_connect disconnected fa fa-facebook-square'));
		echo $this->Html->link('',"javascript:void(0)",array('id'=>'tw_connect','class'=>'tw_connect disconnected fa fa-twitter-square')); 
	?>
	</div>
	<div class="drobe_sale" >
	</div>
	</div>

<div class="left">
</div>
<input type="hidden" id="pos_left" name="pos_left" />
<input type="hidden" id="pos_top" name="pos_top" />
<input type="hidden" id="new_width" name="new_width" />
<input type="hidden" id="new_height" name="new_height" />
<?php 
echo $this->Form->end();
?>
<div class="clear"></div>
</div>


<div id="drobe_uploaded" style="display: none"></div>
<div id="image_search" class="search_image_popup" style="display: none">
	<div class="modal-header">
		<h3 class="m0">Search Google Images</h3>
	</div>
	<div class="modal-body">
		<div class="google-image-query-block">
			<div class="form-row-block input-group">
				<input type="text" placeholder="Image search..." name="google-image-query" id="google-image-query-block" class="form-control">
				<div class="input-group-addon" style="padding:0 5px; background: none; border: none; "><button id="search-images-block" class="btn btn-primary pull-right-block">Search</button></div>
			</div>
		</div>
		<div id="image-search-results-block" style="display: block;">
			<div class="clear"></div>
		</div>
	</div>
	<div class="modal-footer-block">Powered by google</div>
</div>
<script>
var max_width=450;
var max_height=450;
var display_ratio=1;
var drobe_image_dir="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')),true).'/'; ?>";
var is_google_image = false;
var last_uploaded_response=null;

/****************Aviary image upload code ***********************************/
var featherEditor = new Aviary.Feather({
    apiKey: '<?php echo Configure::read('aviary_key');?>',
    apiVersion: 3,
    theme: 'dark', // Check out our new 'light' and 'dark' themes!
    tools: 'all',
    appendTo: '',
    onSave: function(imageID, newURL) {
    	upload_image_from_url(newURL,false);
        return true;
    },
    onClose:function()
    {	  
        display_image_cropping(last_uploaded_response);
	},
    onError: function(errorObj) {
        alert(errorObj.message);
    }
});

$(document).ready(function(){
	activate_menu("post");
	var uploader = new qq.FileUploader({
	    element: $('#file-uploader')[0],
	    uploadButtonText: "Click here to upload Image",
	    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'original')); ?>',
	    params: {type:'drobes'},
	    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
	    debug: false,
	    sizeLimit:4097152,
	    onSubmit: function(id, fileName){
		    // Destroy qtip when upload drobe image on screen.
	    	$("#file-uploader").qtip('destroy');
	    	$("#file-uploader").hide();
	    	$('#file_uploader').block({ 
				message: "<h3>Uploading Image</h3>",
		    	css: { 
		            border: 'none', 
		            padding: '10px',
		            width:"auto", 
		            backgroundColor: '#000', 
		            '-webkit-border-radius': '10px !important', 
		            '-moz-border-radius': '10px !important', 
		            opacity: .5, 
		            color: '#fff' 
	        } }); 
		    },
	    onComplete: function(id, fileName, responseJSON){
	    	//$("#file-uploader").show();
	    	//$('#file_uploader').unblock();	
	    	upload_response(responseJSON);
		},
		onCancel: function(id, fileName){},
		messages: {
			typeError: "You can upload only JPEG, PNG or GIF Image file.",
	        sizeError: "You can upload less then 2MB file size.",
	        emptyError: "Uploaded file is empty.",
		 },
		 showMessage: function(message){ 
			 tip_message('#file-uploader',message);
		}
	});
	
	
	$('#DrobeMydrobePostForm').submit(function(){
		$('#my_drobe_area').block({ 
			message: "<h3>Submitting</h3>",
	    	css: { 
	            border: 'none', 
	            padding: '10px', 
	            width:"auto",
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px !important', 
	            '-moz-border-radius': '10px !important', 
	            opacity: 0.5, 
	            color: '#fff' 
	    	}
	    });
	});

	$('.qq-upload-button input[type="file"]').height($('.qq-upload-button').height());

});
</script>

<?php 
}
?>