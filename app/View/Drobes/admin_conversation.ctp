<h2>Conversation <?php echo $drobeData['Drobe']['comment']; ?></h2>
<div class="tp tp_big drobe_result">
 <?php echo $this->Html->image('/drobe_images/'.$drobeData['Drobe']['file_name'],array('alt'=>$drobeData['Drobe']['file_name'],'height'=>"85", 'width'=>"85")); ?>
         <div class="user_cont">
         	<div class="cont"><?php echo $drobeData['Drobe']['comment']=="" ? "" : $drobeData['Drobe']['comment']." <br />";?><em><?php echo $categoryList[$drobeData['Drobe']['category_id']]?></em></div>
            <div class="up"><?php echo $drobeData['Drobe']['total_in'];?></div> 
            <div class="down"><?php echo $drobeData['Drobe']['total_out'];?></div> 
            <div class="whatever">Total Votes: <?php echo $drobeData['Drobe']['total_rate']?></div>  
         </div>
</div>
<div class="spacer"></div>
<div class="spacer"></div>
<div class="user_rate">
	<div class="rate rate_<?php echo $drobeData['Rate']['rate']>0 ? "Cute" : $drobeData['Rate']['rate'] < 0 ? "Hot" : "Skip"; ?> "></div>
	<div class="profile_photo"><?php 
	if($drobeData['User']['photo']!="") {
		echo $this->Html->image('/'.str_replace(DS,"/",Configure::read('profile.thumb.upload_dir'))."/".$drobeData['User']['photo'],array('height'=>30));
	}
	else
	{
		echo $this->Html->image(Configure::read('profile.default_image'),array('height'=>30));
	}
	if($drobeData['User']['star_user']==1):?>
		<div class="star-user-small"></div>
	<?php endif;?></div>
	<div class="right" style="padding: 0; margin: 0">
	<?php 
	if($drobeData['Rate']['conversation_status']=="open")
	{
		?>
		<a href="<?php 
		echo $this->Html->url(array("controller"=>"conversations","action"=>"change_status", $drobeData['Rate']['conversation_id'],"close","admin"=>true));
		?>" id="confirm_close" class="blue_btn" style="color: #FFFFFF; font-size: 12px; padding: 2px 5px; margin: 10px 2px;">Close Chat</a>
		<?php 
	}
	else
	{
		?>
			<a href="<?php 
		echo $this->Html->url(array("controller"=>"conversations","action"=>"change_status", $drobeData['Rate']['conversation_id'],"open","admin"=>true));
		?>" id="confirm_close" class="red_btn" style="color: #FFFFFF; font-size: 12px; padding: 2px 5px; margin: 10px 2px;">Open Chat</a>
	
		<?php 
	
	}?>
	</div>
	<div class="user_info"><?php echo $this->Html->link($drobeData['User']['first_name']." ".$drobeData['User']['last_name']." (".$drobeData['User']['username'].")" ,array('controller'=>"users","action"=>"profile",$drobeData['User']['unique_id'])); ?><br/>
	<small>
	<?php echo $drobeData['User']['gender'].($drobeData['User']['country']!=""? ", ".$drobeData['User']['country']: "").".";?>
	</small>
	</div>
	<div class="clear"></div>
	<?php 
	if($drobeData['Rate']['comment']!="")
	{
	 ?>
		<div class="comment"><?php echo $drobeData['Rate']['comment'];?></div>
		
	<?php 
	}
	?>
	</div>
	<div class="spacer"></div>
	<div class="spacer"></div>
	<fieldset>
	<legend>Conversation History</legend>
	<div id='conversations_area' class="conversations_area">
	   	<div class="long_content" id="drobe_conversations">
	   	</div>
   	</div>
   	</fieldset>
	<div class="spacer"></div>
	<div>
	<fieldset>
	<legend>Add Conversation</legend>
	<?php 
		echo $this->Form->create("Conversation",array("url"=>array("action"=>"add","admin"=>true)));
		echo $this->Form->label("Conversation Message");
		echo $this->Form->textarea('conversation',array("placeholder"=>"Enter Conversation message Here"));
    	echo $this->Form->input('user_id',array("label"=>"Conversation done by",'options'=>$conversation_users,"default"=>$drobeData['Drobe']['user_id'],"empty"=>"Select Conversations by"));
    	echo $this->Form->hidden('unique_id',array('value'=>$drobeData['Rate']['conversation_id']));
    	echo $this->Form->submit("Add to Conversation");
    	echo $this->Form->end();
    ?>
    </fieldset>
    </div>
    <script type="text/javascript">
	$(document).ready(function(){
		$('#drobe_conversations').load('e-a-d-m-i-n-x/conversations/view/<?php echo $drobeData['Rate']['conversation_id']."/".$drobeData['Drobe']['user_id']; ?>',function(){
	    });
	});
</script>