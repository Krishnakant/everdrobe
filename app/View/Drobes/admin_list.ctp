<div class="right actions">
<?php 
echo $this->Html->link('Moderate Images', array("controller"=>"drobes","action"=>"moderator_list","admin"=>true))." ";
echo $this->Html->link('Drobe Settings', array("controller"=>"settings","action"=>"index","admin"=>true));
?>
</div>

<h2>Uploaded Drobes</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search Drobe : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search drobe by comment, user name, category" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($drobes)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('Drobe.id','#');?></th>
			<th>Image</th>
			<th width="200px"><?php echo $this->Paginator->sort('Drobe.comment','Comment');?></th>
			<th width="200px"><?php echo $this->Paginator->sort('User.first_name','Uploaded by');?></th>
			<th width="200px"><?php echo $this->Paginator->sort('Drobe.device_type','From');?></th>
			<th width="75">Summary</th>
		</tr>
		<?php 
		foreach($drobes as $drobe)
		{
			?>
			<tr class="<?php echo $drobe['Drobe']['id']; ?>">
				<td width="30px" rowspan="2" style="border-bottom: 1px solid #666"><?php echo $drobe['Drobe']['id'];?></td>
				<td width="90px" rowspan="2" style="border-bottom: 1px solid #666"><?php echo $this->Html->link($this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image")),$this->Html->url('/drobe_images/'.$drobe['Drobe']['file_name'],true),array("escape"=>false,"class"=>"example8"));?></td>
				<td width="200px"><?php 
					if($drobe['Drobe']['comment']!="") echo "<div class='comment'>".$drobe['Drobe']['comment']."</div>";
					if($drobe['Category']['category_name']!="") echo "<em>(".$drobe['Category']['category_name'].")</em>";
				?></td>
				<td><?php echo $this->Html->link($drobe['User']['first_name']." ".$drobe['User']['last_name']." (".$drobe['User']['username'].")" ,array("controller"=>"users","action"=>"view","admin"=>true,$drobe['User']['id']));?></td>
				<td width="50px"><?php 
				if($drobe['Drobe']['device_type']=="iphone")
					echo "iPhone";
				elseif($drobe['Drobe']['device_type']=="android")
					echo "Android";
				else 
					echo "Website";
				?></td>
				<td width="75px">
				<div class="actions">
				<a class="summary_btn">Summary</a>
				<div class="bio_right_table full_width" style="display: none;">
        	<table cellspacing="0" cellpadding="0" border="0">
        	  <tbody><tr>
                <td class="gry_td">Views : </td>
                <td class="gry_td"><?php  echo $drobe['Drobe']['views']; ?></td>
              </tr>
              <tr>
                <td class="white_td"> Cute :</td>
                <td class="white_td"><?php  echo $drobe['Drobe']['total_in']; ?></td>
              </tr>
              <tr>
                <td class="gry_td">Hot :</td>
                <td class="gry_td"><?php  echo $drobe['Drobe']['total_out']; ?></td>
              </tr>
              <tr>
                <td class="white_td">Rate :</td>
                <td class="white_td"><?php  echo $drobe['Drobe']['total_rate']; ?></td>
              </tr>
               <tr>
                <td class="gry_td">Status :</td>
                <td class="gry_td"><?php  echo Inflector::humanize($drobe['Drobe']['rate_status']); ?></td>
              </tr>
              <tr>
                <td class="white_td">Posted on :</td>
                <td class="white_td"><?php echo $this->Time->timeAgoInWords($drobe['Drobe']['uploaded_on']); ?></td>
              </tr>
            </tbody></table>
        </div>
        
        </div>
        </td>
        </tr>
        <tr>
				<td colspan="100%" style="border-bottom: 1px solid #666">
					<div class="actions left">
					<?php 
						echo $this->Html->link('Detail',array("controller"=>"drobes","action"=>"detail","admin"=>true,$drobe['Drobe']['id']))." "; 
						if($drobe['Drobe']['rate_status']!="open") echo $this->Html->link('Open',array("controller"=>"drobes","action"=>"action","open","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to open this drobe for rate?"))." ";
						else echo $this->Html->link('Close',array("controller"=>"drobes","action"=>"action","close","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to close this drobe for rate?"))." ";
						
						if($drobe['Drobe']['featured']==0)echo $this->Html->link('Mark Featured',"javascript:void(0)",array("class"=>"mark_featured",'id'=>$drobe['Drobe']['id'],"rel"=>"".$drobe['Drobe']['buy_url_actual']))." ";
						else echo $this->Html->link('Unmark Featured',array("controller"=>"drobes","action"=>"action","unmark_featured","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to unmark as featured drobe?"))." ";
						
						echo $this->Html->link('Remove',array("controller"=>"drobes","action"=>"action","delete","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to remove this drobe?"))." ";
						
						if($drobe['SellDrobe']['sold']=="yes")
						{
							echo $this->Html->link('Re-List for Sale',array("controller"=>"sell_drobes","action"=>"relist","admin"=>true,$drobe['Drobe']['id']),array("class"=>"mark_sale",'id'=>$drobe['Drobe']['id'],"rel"=>"".$drobe['Drobe']['buy_url_actual'], "confirm"=>"After this operation revenue will be deducted and drobe will be opening for sell again. Are you sure for this operation?"))." ";
							echo " <b>Drobe Sold</b>";
						}
						else
						{
							if($drobe['Drobe']['post_type']=="post") echo $this->Html->link('Mark Sell',array("controller"=>"drobes","action"=>"mark_sale","admin"=>true,$drobe['Drobe']['id']),array("class"=>"mark_sale",'id'=>$drobe['Drobe']['id'],"rel"=>"".$drobe['Drobe']['buy_url_actual']))." ";
							else echo $this->Html->link('Unmark Sell',array("controller"=>"drobes","action"=>"action","unmark_sale","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to Unmark from Sale this drobe?"))." ";
						}
					?>
					</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
	<?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="featured_drobe" style="display: none;" class="featured_form">
	<h2>Mark as Featured</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Drobe: </strong><span id="featured_drobe_name"></span></div>
		
		<?php 
			echo $this->Form->input('buy_url',array('id'=>'buy_url','label'=>false,'div'=>false,'placeholder'=>"Enter URL for buy drobe"));
			echo $this->Form->hidden("featured_drobe_id",array("id"=>"featured_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_featured"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_featured"));
?>
<div class="spacer"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	})
	$('.summary_btn').each(function(){
		var self = $(this);
		$(this).click(function(){
			$(this).qtip({
				position : { my: 'right top', at: 'top left' } ,
				content: {
					text: self.next().html(), // Use the submenu as the qTip content
				},
				show: {
					event: 'click',
					ready: true // Make sure it shows on first mouseover
	 			},
				hide: {
					delay: 100,
					event: 'unfocus mouseleave',
					fixed: true // Make sure we can interact with the qTip by setting it as fixed
				},
	 			style: {
					classes: 'ui-tooltip-nav' // Basic styles
					//tip: false // We don't want a tip... it's a menu duh!
				},
			});
		});
	});

	$('.mark_featured').click(function(){
		$('#featured_drobe_name').html($(this).closest('tr').prev().find('td:eq(2)').html().replace("<br/>"," - "));
		$("#buy_url").val($(this).attr("rel"));
		$("#featured_drobe_id").val($(this).attr("id"));
		$.blockUI({ 
	        message: $('#featured_drobe'), 
	        css: { 
	            top:  ($(window).height()) /2 + 'px', 
	            left: ($(window).width()-300) /2 + 'px', 
	            width: '500px' 
	        } 
	    }); 
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI); 
	
	});

	$('#submit_featured').click(function(){
		if($('#buy_url').val()!="")
		{
			$.ajax({
					url: 'e-a-d-m-i-n-x/drobes/action/mark_featured/'+$("#featured_drobe_id").val(),
					type: "POST",
					data: {
						buy_url_actual:$('#buy_url').val()
					},
					success:function(data) {
						$.unblockUI();
						document.location.href=document.location.href;
						/*obj=$.parseJSON(data);
						if(obj.type=="error")
						{
							alert(obj.message);
						}	 
						else 
						{
							document.location.href=document.location.href;
						}*/	
					},
					error:function(data){
						$.unblockUI();
						document.location.href=document.location.href;
					}
				});
		}
		else
		{
			$('#flag_category').focus();
		}
		return false;
	}); 
	$('#close_featured').click(function(){$.unblockUI()}); 
	
});
</script>