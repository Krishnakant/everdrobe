<div class="right actions">
<?php 
echo $this->Html->link('Drobe Settings', array("controller"=>"settings","action"=>"index","admin"=>true));
?>
</div>
<h2>Featured Drobes</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search Drobe : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search drobe by comment, user name, category" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($drobes)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('Drobe.id','#');?></th>
			<th>Image</th>
			<th><?php echo $this->Paginator->sort('Drobe.comment','Comment');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name','Uploaded by');?></th>
			<th width="50"><?php echo $this->Paginator->sort('Drobe.order','Order');?></th>
			
			<th>Summary</th>
			<th>Action</th>
		</tr>
		<?php 
		foreach($drobes as $drobe)
		{
			?>
			<tr class="<?php echo $drobe['Drobe']['id']; ?>">
				<td width="30px" rowspan="3"><?php echo $drobe['Drobe']['id'];?></td>
				<td width="90px" rowspan="3"><?php echo $this->Html->link($this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image")),$this->Html->url('/drobe_images/'.$drobe['Drobe']['file_name'],true),array("escape"=>false,"class"=>"example8"));?></td>
				<td>
				<?php
					if($drobe['Drobe']['comment']!="") echo "<div class='comment'>".$drobe['Drobe']['comment']."</div>";
					if($drobe['Category']['category_name']!="") echo "<em>(".$drobe['Category']['category_name'].")</em>";
				?>
				</td>
				<td><?php echo $this->Html->link($drobe['User']['first_name']." ".$drobe['User']['last_name']." (".$drobe['User']['username'].")" ,array("controller"=>"users","action"=>"view","admin"=>true,$drobe['User']['id']));?></td>
				<td align="center"><?php echo $drobe['Drobe']['order'];?></td>
				<td width="75px">
				<div class="actions">
					<a class="summary_btn">Summary</a>
					<div class="bio_right_table full_width" style="display: none;">
			        	<table cellspacing="0" cellpadding="0" border="0">
			        	  <tbody><tr>
			                <td class="gry_td">Views : </td>
			                <td class="gry_td"><?php  echo $drobe['Drobe']['views']; ?></td>
			              </tr>
			              <tr>
			                <td class="white_td"> Cute :</td>
			                <td class="white_td"><?php  echo $drobe['Drobe']['total_in']; ?></td>
			              </tr>
			              <tr>
			                <td class="gry_td">Hot :</td>
			                <td class="gry_td"><?php  echo $drobe['Drobe']['total_out']; ?></td>
			              </tr>
			              <tr>
			                <td class="white_td">Rate :</td>
			                <td class="white_td"><?php  echo $drobe['Drobe']['total_rate']; ?></td>
			              </tr>
			               <tr>
			                <td class="gry_td">Status :</td>
			                <td class="gry_td"><?php  echo Inflector::humanize($drobe['Drobe']['rate_status']); ?></td>
			              </tr>
			              <tr>
			                <td class="white_td">Posted on :</td>
			                <td class="white_td"><?php  echo date(Configure::read('datetime.display_format'),strtotime($drobe['Drobe']['uploaded_on'])); ?></td>
			              </tr>
			            </tbody></table>
			        </div>
		        
		        </div>
		        </td>
				<td width="100px" rowspan="3">
					<div class="submenu actions">
					<?php 
						echo $this->Html->link('View Detail',array("controller"=>"drobes","action"=>"detail","admin"=>true,$drobe['Drobe']['id'])); 
						echo $this->Html->link('Unmark',array("controller"=>"drobes","action"=>"action","unmark_featured","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to unmark as featured drobe?"));
						echo $this->Html->link('Change URL',"javascript:void(0)",array("class"=>"change_url",'id'=>$drobe['Drobe']['id'],"rel"=>"".$drobe['Drobe']['buy_url_actual']));
						
					?>
					</div>
				</td>
			</tr>
			<tr>
			<td colspan="4">
			<?php 
			echo $this->Html->link($drobe['Drobe']['buy_url_actual'],$drobe['Drobe']['buy_url_actual'],array("target"=>"_blank","class"=>"link"));
			echo  " (".$drobe['Drobe']['buy_url'].")";
			?>
			</td>
			</tr>
			<tr>
			<td colspan="4">
				<div class="actions"> Change Order: 
					<?php 
						echo $this->Html->link('Up',array("controller"=>"drobes","action"=>"position","up","admin"=>true,$drobe['Drobe']['id']))." ";
						echo $this->Html->link('Down',array("controller"=>"drobes","action"=>"position","down","admin"=>true,$drobe['Drobe']['id']))." ";
						echo $this->Html->link('First',array("controller"=>"drobes","action"=>"position","first","admin"=>true,$drobe['Drobe']['id']))." ";
						echo $this->Html->link('Last',array("controller"=>"drobes","action"=>"position","last","admin"=>true,$drobe['Drobe']['id']));
					?>
					<div class="spacer"></div>
				</div>
			</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
	<?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="featured_drobe" style="display: none;" class="featured_form">
	<h2>Change Buy-it Link</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Drobe: </strong><span id="featured_drobe_name"></span></div>
		
		<?php 
			echo $this->Form->input('buy_url',array('id'=>'buy_url','label'=>false,'div'=>false,'placeholder'=>"Enter URL for buy drobe"));
			echo $this->Form->hidden("featured_drobe_id",array("id"=>"featured_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_featured"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_featured"));
?>
<div class="spacer"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	})
	$('.summary_btn').each(function(){
		var self = $(this);
		$(this).click(function(){
			$(this).qtip({
				position : { my: 'right top', at: 'top left' } ,
				content: {
					text: self.next().html(), // Use the submenu as the qTip content
				},
				show: {
					event: 'click',
					ready: true // Make sure it shows on first mouseover
	 			},
				hide: {
					delay: 100,
					event: 'unfocus mouseleave',
					fixed: true // Make sure we can interact with the qTip by setting it as fixed
				},
	 			style: {
					classes: 'ui-tooltip-nav' // Basic styles
					//tip: false // We don't want a tip... it's a menu duh!
				},
			});
		});
	});

	$('.change_url').click(function(){
		$('#featured_drobe_name').html($(this).closest('tr').find('td:eq(2)').html().replace("<br/>"," - "));
		$("#buy_url").val($(this).attr("rel"));
		$("#featured_drobe_id").val($(this).attr("id"));
		$.blockUI({ 
	        message: $('#featured_drobe'), 
	        css: { 
	            top:  ($(window).height()) /2 + 'px', 
	            left: ($(window).width()-300) /2 + 'px', 
	            width: '500px' 
	        } 
	    }); 
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI); 
	
	});

	$('#submit_featured').click(function(){
		if($('#buy_url').val()!="")
		{
			$.ajax({
					url: 'e-a-d-m-i-n-x/drobes/action/change_link/'+$("#featured_drobe_id").val(),
					type: "POST",
					data: {
						buy_url_actual:$('#buy_url').val()
					},
					success:function(data) {
						$.unblockUI();
						obj=$.parseJSON(data);
						if(obj.type=="error")
						{
							alert(obj.message);
						}	 
						else document.location.href=document.location.href;
					}
				});
		}
		else
		{
			$('#flag_category').focus();
		}
		return false;
	}); 
	$('#close_featured').click(function(){$.unblockUI()}); 
	
});
</script>