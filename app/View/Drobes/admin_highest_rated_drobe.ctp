<h2>Higest Rated Drobe</h2>
<?php
if(count($drobes)>0)
{
	?>
	<table>
		<tr>
			<th width="30px"><?php echo $this->Paginator->sort('Drobe.id','#');?></th>
			<th width="50px">Image</th>
			<th><?php echo $this->Paginator->sort('Category.category_name','Category');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name','User');?></th>
			<th><?php echo $this->Paginator->sort('User.username','User Name');?>
			<th><?php echo $this->Paginator->sort('Drobe.uploaded_on','Posted On');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.total_favs','Total Favs');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.views','Total Views');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.total_rate','Total Votes');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.total_in','Total Cute');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.total_out','Total Hot');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.rate_index','Rate Index');?></th>
		</tr>
		<?php 
		foreach($drobes as $drobe)
		{
			?>
			<tr class="<?php echo $drobe['Drobe']['id']; ?>">
				<td><?php echo $drobe['Drobe']['id'];?></td>
				<td><?php echo $this->Html->link($this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0,'width'=>50,'id'=>"profile_image" ,"alt"=>"drobe prview Image")),$this->Html->url('/drobe_images/'.$drobe['Drobe']['file_name'],true),array("escape"=>false,"class"=>"example8"));?></td>
				<td width="75px"><div class='comment'><?php echo $drobe['Category']['category_name']; ?></div></td>
				<td><?php echo $this->Html->link($drobe['User']['first_name']." ".$drobe['User']['last_name'],array("controller"=>"users","action"=>"view","admin"=>true,$drobe['User']['id']));?></td>
				<td><?php echo $drobe['User']['username']?></td>
				<td><?php echo $this->Time->niceShort($drobe['Drobe']['uploaded_on']);?></td>
				<td align="center"><?php echo $drobe['Drobe']['total_favs'];?></td>
				<td align="center"><?php echo $drobe['Drobe']['views'];?></td>
				<td align="center"><?php echo $drobe['Drobe']['total_rate'];?></td>
				<td align="center"><?php echo $drobe['Drobe']['total_in'];?></td>
				<td align="center"><?php echo $drobe['Drobe']['total_out'];?></td>
				<td align="center"><?php echo $drobe['Drobe']['rate_index'];?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
	<?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
