<div id="drobe_detail left" >
	<div id='inline_example1' style='padding:10px; background:#fff;'>
	<div class="left"><span class="big-txt" style="font-size: 18px;"><?php  echo $drobe['Drobe']['comment']. ($drobe['Drobe']['post_type']=="sell" ? " [Sell Drobe]": ""); ?></span></div>
	<div class="spacer"></div>
	<div class="spacer"></div>
	<div class="right">
	<div class="actions">
     	<?php 
        	echo $this->Html->link('Edit', array("controller"=>"drobes","action"=>"edit",$drobe['Drobe']['id'],"admin"=>true))." ";
        	
        	echo $this->Html->link('View Rate ('.count($drobe['Rate']).')',array("controller"=>"rates","action"=>"view","admin"=>true,$drobe['Drobe']['id']))." ";
        	
        	if($drobe['Drobe']['rate_status']=="open") echo $this->Html->link('Close for Rate', array("controller"=>"drobes","action"=>"action","close",$drobe['Drobe']['id']))." ";
        	else echo $this->Html->link('Open for Rate', array("controller"=>"drobes","action"=>"action","open",$drobe['Drobe']['id']))." ";
        	
        	if($drobe['SellDrobe']['sold']=="no")
        	{
	        	if($drobe['Drobe']['featured']==0) echo $this->Html->link('Mark Featured',"javascript:void(0)",array("class"=>"mark_featured",'id'=>$drobe['Drobe']['id'],"rel"=>"".$drobe['Drobe']['buy_url']))." ";
		       	else echo $this->Html->link('Unmark Featured',array("controller"=>"drobes","action"=>"action","unmark_featured","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to unmark as featured drobe?"))." ";
		       	if($drobe['Drobe']['post_type']=="post")
		       	{
		       		echo $this->Html->link('Mark Sell',array("controller"=>"drobes","action"=>"mark_sale","admin"=>true,$drobe['Drobe']['id']))." ";
		       	} 
		       	else echo $this->Html->link('Unmark Sell',array("controller"=>"drobes","action"=>"action","unmark_sale","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to unmark as featured drobe?"))." ";
        	}
	    ?>
    </div>
	</div>
	<div class="spacer"></div>
	<div class="spacer"></div>
	<div class="left" style="padding-right: 0; margin-right: 0; font-size: 15px">Posted by: <?php echo $this->Html->link($drobe['User']['first_name']." ".$drobe['User']['last_name']." (".$drobe['User']['username'].")" , array("controller"=>"users","action"=>"view","admin"=>true,$drobe['User']['id'])); ?></div>
	<div class="right" style="padding-right: 0; margin-right: 0; font-size: 15px"><?php  echo "Category: ".$drobe['Category']['category_name']; ?></div>
 	<div class="spacer"></div>
 	<div class="big-img" style=" width:200px; height:200px; float: left">
 	<?php  
 		echo $this->Html->image('/drobe_images/'.$drobe['Drobe']['file_name'],array('width'=>"200px", 'height'=>"200px",'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image")); 
 	?>
 	</div>
 	<div class="left" style="margin-left:20px; padding-right:0; width:170px;">
		<div class="bio_right_table full_width">
        	<table cellspacing="0" cellpadding="0" border="0">
              <tbody><tr>
                <td class="gry_td">Views : </td>
                <td class="gry_td"><?php  echo $drobe['Drobe']['views']; ?></td>
              </tr>
              <tr>
                <td class="white_td"> Cute :</td>
                <td class="white_td"><?php  echo $drobe['Drobe']['total_in']; ?></td>
              </tr>
              <tr>
                <td class="gry_td">Hot :</td>
                <td class="gry_td"><?php  echo $drobe['Drobe']['total_out']; ?></td>
              </tr>
              <tr>
                <td class="white_td">Rate :</td>
                <td class="white_td"><?php  echo $drobe['Drobe']['total_rate']; ?></td>
              </tr>
               <tr>
                <td class="gry_td">Status :</td>
                <td class="gry_td"><?php  echo Inflector::humanize($drobe['Drobe']['rate_status']); ?></td>
              </tr>
              <tr>
                <td class="white_td">Posted on :</td>
                <td class="white_td"><?php  echo date(Configure::read('datetime.display_format'),strtotime($drobe['Drobe']['uploaded_on'])); ?></td>
              </tr>
              <tr>
                <td class="gry_td">Target :</td>
                <td class="gry_td"><?php 
                 if($drobe['DrobeSetting']['male'] && $drobe['DrobeSetting']['female']) echo "Both";
                 else if($drobe['DrobeSetting']['female']) echo "Female";
                 else if($drobe['DrobeSetting']['male']) echo "Male";
                 ?></td>
              </tr>
            </tbody></table>
        </div>
        <div class="spacer"></div>
	</div>
<div class="spacer"></div>
<?php 
	if($drobe['Drobe']['post_type']=="sell")
	{
		$drobe_size=Cache::read('size_list');
		?>
		<h3 class="left nomargin">Sell Drobe Detail</h3>
		<div class="right"><div class="actions">
     	<?php 
	     	if($drobe['SellDrobe']['sold']=="no")
	     	{
	     		echo $this->Html->link('View/Edit',array("controller"=>"sell_drobes","action"=>"edit","admin"=>true,$drobe['Drobe']['id']))." ";
	     		if($drobe['Drobe']['post_type']=="post") echo $this->Html->link('Mark for Sale',array("controller"=>"drobes","action"=>"mark_sale","admin"=>true,$drobe['Drobe']['id']),array("class"=>"mark_sale",'id'=>$drobe['Drobe']['id'],"rel"=>"".$drobe['Drobe']['buy_url_actual']));
	     		else echo $this->Html->link('Unmark-Sale',array("controller"=>"drobes","action"=>"action","unmark_sale","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"Are you sure to Unmark from Sale this drobe?"))." ";
	     		echo $this->Html->link('Mark-Sold',array("controller"=>"drobes","action"=>"sell_drobe_action","sold","admin"=>true,$drobe['Drobe']['id']),array("class"=>"mark_sale",'id'=>$drobe['Drobe']['id'],"rel"=>"".$drobe['Drobe']['buy_url_actual'], "confirm"=>"Are you sure to Unmark from Sale this drobe?"))." ";
	     	}
	 	?>
	 	</div></div>
		
		<table>
		<tr><td width="130px"><b>Brand Name</b></td><td><?php echo $drobe['SellDrobe']['sell_brand_name'];?></td></tr>
		<tr><td width="130px"><b>Size</b></td><td><?php echo $drobe['SellDrobe']['size_name'];?></td></tr>
		<tr><td width="130px"><b>Listing Drobe Price</b></td><td><?php echo "$ ".$drobe['SellDrobe']['sell_price'];?></td></tr>
		<tr><td width="130px"><b>Original Drobe Price</b></td><td><?php echo "$ ".$drobe['SellDrobe']['original_sell_price'];?></td></tr>
		<tr><td width="130px"><b>Description</b></td><td><?php echo $drobe['SellDrobe']['sell_description'];?></td></tr>
		<tr><td width="130px"><b>Status</b></td><td><?php echo Inflector::humanize($drobe['SellDrobe']['status']);?></td></tr>
		<tr><td width="130px"><b>Sold</b></td><td><?php echo Inflector::humanize($drobe['SellDrobe']['sold']);?></td></tr>
		
		</table>
		<?php 
		}
	?>
	<div class="spacer"></div>
<h3>Drobe Vote Statistics</h3>
<div class="spacer"></div>
<?php 
$dates=array();
$data=array();

foreach($rate_stats as $stats)
{
	$dates[]=date('M-d',strtotime($stats[0]['date_day']));
	$data[]=$stats[0]['total'];
}

echo $this->GoogleChart->create()
->setType('bar', array('vericle'))
->setSize(675, 200)
->setMargins(5, 5, 5, 5)
->addData($data)
->addMarker('value', array('format' => 'f1', 'placement' => 'c','color' => 'FFFFFF','size' => 10))
->addAxis('x', array('labels' => $dates))
->addAxis('y', array('axis_or_tick' => 'l', 'size' => 12));
?>
</div>
</div>
<div id="featured_drobe" style="display: none;" class="featured_form">
	<h2>Mark as Featured</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Drobe: </strong><span id="featured_drobe_name"></span></div>
		<?php 
			echo $this->Form->input('buy_url_actual',array('id'=>'buy_url','label'=>false,'div'=>false,'placeholder'=>"Enter URL for buy drobe"));
			echo $this->Form->hidden("featured_drobe_id",array("id"=>"featured_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_featured"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_featured"));
?>
<div class="spacer"></div>
</div>
<div class="clear"></div>
<div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.mark_featured').click(function(){
		$('#featured_drobe_name').html($("span.big-txt").html());
		$("#buy_url").val($(this).attr("rel"));
		$("#featured_drobe_id").val($(this).attr("id"));
		$.blockUI({ 
	        message: $('#featured_drobe'), 
	        css: { 
	            top:  ($(window).height()) /2 + 'px', 
	            left: ($(window).width()-300) /2 + 'px', 
	            width: '500px' 
	        } 
	    }); 
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI); 
	
	});

	$('#submit_featured').click(function(){
		if($('#buy_url').val()!="")
		{
			$.ajax({
					url: 'e-a-d-m-i-n-x/drobes/action/mark_featured/'+$("#featured_drobe_id").val(),
					type: "POST",
					data: {
						buy_url:$('#buy_url').val()
					},
					success:function(data) {
						$.unblockUI(); 
						document.location.href=document.location.href;
					}
				});
		}
		else
		{
			$('#flag_category').focus();
		}
		return false;
	}); 
	
	$('#close_featured').click(function(){$.unblockUI()});
});

</script>
