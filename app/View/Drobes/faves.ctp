<h2 class="text-center title_mt1" ><?php echo $title?></h2>
<?php 
if($this->Session->read('Auth.User.id')>0)
{
//echo $this->element('filter_category');
?>
<?php 
}
?>
<div class="details search-box">

<div class="input-group">
      <input type="text" class="search_input form-control input-lg" id="search_input" placeholder="Search by Name, Username, Comment or Keyword" />
      <div class="input-group-addon search-icon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
    </div>
</div>

		<div id="TabbedPanels1" class="TabbedPanels">
		  <ul class="TabbedPanelsTabGroup 12">
		    <li class="TabbedPanelsTab" tabindex="0">Most Recent</li>
		    <!-- <li class="TabbedPanelsTab" tabindex="0">Highest Rated</li> -->
            <li class="TabbedPanelsTab" tabindex="0">The Shop</li>
	      </ul>
		  <div class="TabbedPanelsContentGroup">
		    <div class="TabbedPanelsContent">
            	<div class="container">
				<div id='most_recent_area' class='drobe_list infiniteScroll'>
                	<div class="long_content" id="most_recent">
                	</div>   
                </div></div>
            </div>
		    <!-- <div class="TabbedPanelsContent">
            	<div id='heighest_rated_area' class='drobe_list infiniteScroll'>
                   <div class="long_content" id="heighest_rated">
                   </div>
                </div>
            </div> -->
            <div class="TabbedPanelsContent">
            	<div class="container">
				<div id='featured_area' class='drobe_list infiniteScroll'>
            		<div class="long_content" id="featured">
            		<div class="spacer"></div>
            		<p class="empty_message">Dear User, Wait for the sometime, Shop coming soon...</p>
            		</div> 
                 </div>
				 </div>
            </div>
	      </div>
	      <div class="infiniteScrollLoading">Loading More Drobes</div>
    </div>
	<script type="text/javascript">
		
		$(document).ready(function(){
			activate_menu("fav");	

				var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
				$('#TabbedPanels1 .TabbedPanelsTab').click(function(){
					current_index=TabbedPanels1.getCurrentTabIndex();
					var selectedtab = '#TabbedPanels1 .TabbedPanelsContentGroup > div:eq('+current_index+') .infiniteScroll';
					if($(selectedtab)[0].fleXcroll== undefined)
					{
					   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
					   apply_infinite_scrolling($(selectedtab).attr('id'));
					}	 
				});
				load_favourite("");	

				 $('#search_input').keypress(function(event) {
	        	    if (event.which == 13) {
	        	    	load_favourite($(this).val());
	        	    }
	        	});

		        $('#search_input').blur(function() {
		        	load_favourite($(this).val());
	        	});
	        
			
      });
		function load_favourite(search)
		{
			search = escape(search);
			$('#most_recent').attr('rel','<?php echo $this->Html->url(array('controller'=>"favourites",'action'=>"getlist",$user_unique_id,"recent"))?>/search:'+search).attr('page',0).attr('last',1);
        	//$('#heighest_rated').attr('rel','<?php echo $this->Html->url(array('controller'=>"favourites",'action'=>"getlist_highest_rated",'page_counter',$user_unique_id))?>/search:'+search).attr('page',0).attr('last',1);
            //$('#featured').attr('rel','<?php echo $this->Html->url(array('controller'=>"favourites",'action'=>"getlist",$user_unique_id,"featured"))?>/search:'+search).attr('page',0).attr('last',1);
            
            $('#most_recent').load('<?php echo $this->Html->url(array('controller'=>"favourites",'action'=>"getlist",$user_unique_id,"recent"))?>/search:'+search,function(){
            	$('.TabbedPanelsTab:eq(0)').click();
            	var last_faved_on=$('#most_recent a:first').attr("rel");
            	$('#most_recent').attr('rel','<?php echo $this->Html->url(array('controller'=>"favourites",'action'=>"getlist",$user_unique_id,"recent"))?>/'+last_faved_on+'/search:'+search);
            	set_scrollbar(0);
            });
            //$('#heighest_rated').load('<?php echo $this->Html->url(array('controller'=>"favourites",'action'=>"getlist_highest_rated",'1',$user_unique_id))?>/search:'+search,function(){set_scrollbar(1);});
            //$('#featured').load('<?php echo $this->Html->url(array('controller'=>"favourites",'action'=>"getlist",$user_unique_id,"featured"))?>/search:'+search,function(){set_scrollbar(1);});
		}
		function set_scrollbar(index)
        {
           	var selectedtab = '#TabbedPanels1 .TabbedPanelsContentGroup > div:eq('+index+') .infiniteScroll';
        	if($(selectedtab)[0].fleXcroll== undefined)
			{
			   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
			   apply_infinite_scrolling($(selectedtab).attr('id'));
			}
        }
		function nextrate()
        {
			load_favourite();
        	//document.location.href=document.location.href;
        }
        </script>