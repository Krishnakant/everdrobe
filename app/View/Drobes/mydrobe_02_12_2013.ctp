<?php

if(isset($drobe_uploaded) && $drobe_uploaded==true)
{
	$category=Cache::read('drobe_category');
	?>
	<div class="drobe_uploaded">
<!--<div class="left" style="font-size: smaller"><?php //echo $category[$this->data['Drobe']['category_id']]; ?></div> --> 
	
<!-- <div class="drobe_comment"><?php  //echo $this->data['Drobe']['comment']; ?></div> -->
		
		<?php //echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name']."?rand=".uniqid(),array('border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image")); ?>
			<div style="margin-top:110px;">
			<h1>Thank you</h1>
			<p>
				Check my results in a few minutes to see the results of your question.
			</p>
			<p>
				While you're waiting, please help others by rating their outfits.
			</p>
			</div>
				
	
	<div class="actions" style="margin-top:50px;">
	          <p>
	          <?php 
	          echo $this->Html->link('Rate Now',array('controller'=>"drobes","action"=>'rate',0,"recent","rate_stream"),array("class"=>"red_btn_rate"));
	          ?>
	          </p>
	     </div>
	      
      
	</div>
	
	<?php
}
else
{
?>
<div id="drobe_type_btn">
	<div class="post_drobe_btn">
		<?php echo $this->Html->link('<div class="" id="post_for_feedback"></div>Post for Feedback','javascript:void(0);',array('id'=>'post_feedback','escape'=>false)) ?>
		<div class="spacer"></div>
		<div class="spacer"></div>
		<?php echo $this->Html->link('<div class="" id="post_for_sell"></div>Post for Sale','javascript:void(0);',array('id'=>'post_sale','escape'=>false)) ?>
	</div>
</div>

<div id="my_drobe_area" style="display: none;">

	<div class="drobe_preview big-img">
	<div class="big-img" style="overflow:hidden;">
	<img src="images/big-photo.jpg" id="crop_image"/>
	</div>
	<div id="clear_preview" class="right">X</div>
	<div class="clear"></div>
	</div>
	<div class="big-img" id="file_uploader" <?php if(isset($drobe_uploaded) && $drobe_uploaded==false) echo "style='display:none'"; ?>>
	<div class="drobe_upload_msg" id="file-uploader">Click here to upload File</div>
	<img src="images/big-photo.jpg" />
	</div>
	<?php
	echo $this->Form->create('Drobe',array('class'=>"form", 'type'=>'file'));
	echo $this->Form->hidden('file_name');
	echo $this->Form->hidden('post_type',array('value'=>'post'));
	?>
	<div class="details">
		<?php echo $this->Form->input('category_id',array('label'=>false,'options'=>$categoryList,'default'=>"0"));?>
	</div>
	<div class="more_margin left" >
	<?php 
	// Check for facebook setting if user connected with facebook then is_fb_connected is 1 other wise 0 @sadikhasan
	if($this->Session->read('UserSetting.fb_connected'))	
	{
		echo $this->Form->hidden("is_fb_connected",array('id'=>'is_fb_connected','value'=>1));  // Flag value for user connected with facebook or not?
	}
	else
	{
		echo $this->Form->hidden('is_fb_connected',array('id'=>'is_fb_connected','value'=>0));
		echo $this->Form->hidden('fb_url',array('id'=>'fb_url','value'=>$facebookUrl));		//if user not connect with facebook then fb_url contains url for open popup window for facebook login
	}
		echo $this->Form->hidden("is_fb_post",array('id'=>'is_fb_post','value'=>0));  //set value if user want to post it's drobe to facebook site

	// Check for twitter setting if user connected with twitter then is_tw_connected is 1 other wise 0 @sadikhasan
	if($this->Session->read('UserSetting.tw_connected'))
	{
		echo $this->Form->hidden("is_tw_connected",array('id'=>'is_tw_connected','value'=>1));  // Flag value for user connected with twitter or not?
	}
	else
	{
		echo $this->Form->hidden('is_tw_connected',array('id'=>'is_tw_connected','value'=>0));
		echo $this->Form->hidden('tw_url',array('id'=>'tw_url','value'=>$twitterLoginUrl));		//if user not connect with twitter then tw_url contains url for open popup window for twitter login
	}
	echo $this->Form->hidden("is_tw_post",array('id'=>'is_tw_post','value'=>0));  //set value if user want to post it's drobe to twitter site
		
	?>
	<?php 
	echo $this->Form->textarea('comment',array('label'=>false,"cols"=>32,"rows"=>3,"class"=>"form-control","placeholder"=>"Type your question ...")); 
	echo $this->Form->submit('SUBMIT',array('class'=>"green_btn","id"=>"drobe_upload_btn"));
	?>
	<input type="button" class="red_btn btn_txt" id="search_online_btn" value="Search Online" />
	
	<div class="share_btn">
	<?php
		echo $this->Html->link('','javascript:void(0);',array('id'=>'fb_connect','class'=>'fb_connect disconnected'));
		echo $this->Html->link('',"javascript:void(0)",array('id'=>'tw_connect','class'=>'tw_connect disconnected')); 
	?>
	</div>
	<div class="drobe_sale" >
	</div>
	</div>

<div class="left">
</div>
<input type="hidden" id="pos_left" name="pos_left" />
<input type="hidden" id="pos_top" name="pos_top" />
<input type="hidden" id="new_width" name="new_width" />
<input type="hidden" id="new_height" name="new_height" />
<?php 
echo $this->Form->end();
?>
<div class="clear"></div>
</div>


<!-- For sell drobe upload form fields  -->
<?php
if(Configure::read('setting.drobe_sale')=="on")
{
?>

<div class="drobe_sale" >
<div class="drobe_sale_popup" style="display: none;" id="sell_drobe_area">
	
	<div class="sell_drobe_preview big-img" id="sell_drobe_preview">
	<div class="big-img" style="overflow:hidden;">
	<img src="images/big-photo.jpg" id="sell_drobe_crop_image"/>
	</div>
	<div id="sell_drobe_clear_preview" class="right">X</div>
	<div class="clear"></div>
	</div>
	<div class="big-img" id="sell_drobe_file_uploader" <?php if(isset($drobe_uploaded) && $drobe_uploaded==false) echo "style='display:none'"; ?>>
	<div class="drobe_upload_msg" id="sell_drobe_file-uploader">Click here to upload File</div>
	<img src="images/big-photo.jpg" />
	</div>

	<?php
	echo $this->Form->create('Drobe',array('class'=>"form", 'type'=>'file','id'=>'sell_drobe_my_drobe_form'));
	echo $this->Form->hidden('file_name',array('name'=>'data[Drobe][file_name]','id'=>'sell_drobe_file_name'));
	echo $this->Form->hidden('post_type',array('value'=>'sell'));
	?>
	
	<div class="spacer"></div>
	<h3>Additional Images</h3>
	<div class="sell_drobe_images">
	<div class="image_box" id="sell_drobe_image_1"></div>
	<div class="image_box" id="sell_drobe_image_2"></div>
	<div class="image_box" id="sell_drobe_image_3"></div>
	</div>
	
	<div class="half left">
	<?php 
		//echo $this->Form->input('SellDrobe.sell_brand_name',array("label"=>"<b>Brand Name: </b>","class"=>"comment","id"=>"brand_name"));
	?>
	<!-- <div class="right"><em>Max 140 Characters</em></div> -->
	<div class="spacer"></div>
	<?php echo $this->Form->input('category_id',array('label'=>'<b>Category:</b>','options'=>$categoryList,'default'=>"0",'id'=>'drobe_category_id_sell'));?>
	
	<div class="spacer"></div>
	<?php $sizeList['Other']='Other'; 
		echo $this->Form->input('SellDrobe.size_id', array("label"=>"<b>Size:</b>","options"=>$sizeList,"id"=>"drobe_size",'class'=>'drob_size_custom','empty'=>"Select Drobe Size"));
	?>
	
	<div class="spacer"></div>
	<?php $brandList['Other']='Other'; 
		echo $this->Form->input('SellDrobe.brand_id', array("label"=>"<b>Brand :</b>","options"=>$brandList,"id"=>"drobe_brand",'class'=>'drobe_brand_custom','empty'=>"Select Brand Name"));
	?>
	
	<div class="spacer"></div>
	<?php
	echo $this->Form->label("<b> This is for :</b>"); 
	echo $this->Form->input('SellDrobe.sell_gender', array('before' => '','after' => '', 'between' => '', 'separator' => '','legend'=>false, 'options' => array('male'=>"Male", 'female'=>'Female','everyone'=>"Everyone"),'type' => 'radio','default'=>"everyone"));
	?>
	
	<div class="spacer"></div>
	<?php
	echo $this->Form->label("<b> Brand New/ Used :</b>"); 
	echo $this->Form->input('SellDrobe.is_brand_new', array('legend'=>false, 'options' => array('new'=>"New", 'used'=>'Used'),'type' => 'radio','default'=>"new"));
	
	?>
	<?php 
	// Check for facebook setting if user connected with facebook then is_fb_connected is 1 other wise 0 @sadikhasan
	if($this->Session->read('UserSetting.fb_connected'))
	{
		echo $this->Form->hidden("is_fb_connected",array('id'=>'is_fb_connected','value'=>1));  // Flag value for user connected with facebook or not?
	}
	else
	{
		echo $this->Form->hidden('is_fb_connected',array('id'=>'is_fb_connected','value'=>0));
		echo $this->Form->hidden('fb_url',array('id'=>'fb_url','value'=>$facebookUrl));		//if user not connect with facebook then fb_url contains url for open popup window for facebook login
	}
	echo $this->Form->hidden("is_fb_post",array('id'=>'is_fb_post_sell','value'=>0));  //set value if user want to post it's drobe to facebook site
	
	// Check for twitter setting if user connected with twitter then is_tw_connected is 1 other wise 0 @sadikhasan
	if($this->Session->read('UserSetting.tw_connected'))
	{
		echo $this->Form->hidden("is_tw_connected",array('id'=>'is_tw_connected','value'=>1));  // Flag value for user connected with twitter or not?
	}
	else
	{
		echo $this->Form->hidden('is_tw_connected',array('id'=>'is_tw_connected','value'=>0));
		echo $this->Form->hidden('tw_url',array('id'=>'tw_url','value'=>$twitterLoginUrl));		//if user not connect with twitter then tw_url contains url for open popup window for twitter login
	}
	echo $this->Form->hidden("is_tw_post",array('id'=>'is_tw_post_sell','value'=>0));  //set value if user want to post it's drobe to twitter site
	
	
	?>
	
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SellDrobe.original_sell_price', array("place","label"=>"<b>Original Price in USD:</b>","type"=>"text","id"=>"original_drobe_price"));
	?>
	
	
	<?php 
		echo $this->Form->input('SellDrobe.sell_price', array("place","label"=>"<b>Listing Price in USD:</b>","type"=>"text","id"=>"drobe_price",'autocomplete' => 'off'));
	?>
	
	<?php 
		echo $this->Form->input('your_earning', array('label'=>'<b> Your Earning</b> (when sold)',"type"=>"text",'readonly'=>'readonly','id'=>'your_earning','name'=>'your_earning'));
		
		//This field for calculating earning of the seller for calculating earning percentage this variable set in drobecontroller
		echo $this->Form->input('earning_percent',array('type'=>'hidden','value'=>$earning_percent,'id'=>'earning_percent','name'=>'earning_percent'));
	?>
	
	
	<div class="spacer"></div>
	<div class="spacer"></div>
	<div class="text"></div>
	</div>
	<div class="half right drobe_description">
	<div class="spacer"></div>
	<?php 
		echo $this->Form->label("<b> Comment: </b>");
		echo $this->Form->textarea('comment',array('id'=>'sell_drobe_comment','label'=>false,"cols"=>12,"rows"=>3,"class"=>"form-control","placeholder"=>"Type your question ...","style"=>"width:200px !important; height:60px !important"));
	?>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->label("<b> Description: </b> <em>(Optional)</em>");
		echo $this->Form->textarea('SellDrobe.sell_description',array('lable'=>"false", "id"=>"drobe_description"));
	?>
	<div><em>Max 300 Characters</em></div>
	</div>
	<div class="clear"></div>
	<input type="hidden" id="sell_pos_left" name="pos_left" />
	<input type="hidden" id="sell_pos_top" name="pos_top" />
	<input type="hidden" id="sell_new_width" name="new_width" />
	<input type="hidden" id="sell_new_height" name="new_height" />
	
	
	<?php
		/* echo $this->Html->link('Cancel and set as Post',"javascript:void(0)",array('id'=>'close_sale_popup', 'class'=>"red_btn"));
		echo $this->Html->link('Apply',"javascript:void(0)",array('id'=>'apply_sale_drobe', 'class'=>"blue_btn")); */
			for($i=0;$i<3;$i++)
			{
				echo $this->Form->input("SellDrobeImage.".$i.".id",array("type"=>"hidden"));
				echo $this->Form->input("SellDrobeImage.".$i.".sell_drobe_id",array("type"=>"hidden"));
				echo $this->Form->input("SellDrobeImage.".$i.".file_name",array("type"=>"hidden","rel"=>"sell_drobe_image_".($i+1)));
			}
		echo $this->Form->submit('SUBMIT',array('class'=>"green_btn","id"=>"drobe_upload_btn_sell"));
		
	?>
	<div class="share_btn">
		
		<?php
		echo $this->Html->link('','javascript:void(0);',array('id'=>'fb_connect_sell','class'=>'fb_connect disconnected'));
		echo $this->Html->link('',"javascript:void(0)",array('id'=>'tw_connect_sell','class'=>'tw_connect disconnected'));
		?>
	</div>
	<div class="spacer"></div>
	<div class="spacer"></div>
	</div>
</div>
	
<?php 
	echo $this->Form->end(); 
	}
?>


<div id="drobe_uploaded" style="display: none"></div>
<div id="image_search" class="search_image_popup" style="display: none">
	<div class="modal-header-block">
		<h3 class="popup-header-block">Search Google Images</h3>
	</div>
	<div class="modal-body-block">
		<div class="google-image-query-block">
			<div class="form-row-block">
				<input type="text" placeholder="Image search..." name="google-image-query" id="google-image-query-block">
				<button id="search-images-block" class="green_btn btn_txt btn-block btn-wide-block pull-right-block">Search</button>
			</div>
		</div>
		<div id="image-search-results-block" style="display: block;">
			<div class="clear"></div>
		</div>
	</div>
	<div class="modal-footer-block">Powered by google</div>
</div>
<script type="text/javascript">
var max_width=450;
var max_height=450;
var display_ratio=1;
var drobe_image_dir="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')),true).'/'; ?>";
var fileUploadComplete=new Array();
var is_normal_drobe = true;
var last_uploaded_response=null;
var is_google_image = false;   //This variable is used to check image upload from google search if image from google search then its value is true other wise false


fileUploadComplete[0]=function(id, fileName, responseJSON){fileUploadCompleteAction($('#sell_drobe_image_1'),responseJSON);};
fileUploadComplete[1]=function(id, fileName, responseJSON){fileUploadCompleteAction($('#sell_drobe_image_2'),responseJSON);};
fileUploadComplete[2]=function(id, fileName, responseJSON){fileUploadCompleteAction($('#sell_drobe_image_3'),responseJSON);};

var fileUploadSubmit=new Array();
fileUploadSubmit[0]=function(id,fileName){blockUploader($('#sell_drobe_image_1'));}; 
fileUploadSubmit[1]=function(id,fileName){blockUploader($('#sell_drobe_image_2'));}; 
fileUploadSubmit[2]=function(id,fileName){blockUploader($('#sell_drobe_image_3'));};


/****************Aviary image upload code ***********************************/
var featherEditor = new Aviary.Feather({
    apiKey: '<?php echo Configure::read('aviary_key');?>',
    apiVersion: 3,
    theme: 'dark', // Check out our new 'light' and 'dark' themes!
    tools: 'all',
    appendTo: '',
    onSave: function(imageID, newURL) {
    	upload_image_from_url(newURL,false);
        return true;
    },
    onClose:function()
    {
         if(is_normal_drobe==true)
		  {
    		 display_image_cropping(last_uploaded_response);
		  }	  
		  else
		  {
			  display_image_cropping_sell_drobe(last_uploaded_response);
		  }   
    },
    onError: function(errorObj) {
        alert(errorObj.message);
    }
});


$(document).ready(function()
{
	var uploader= new Array();
	for(var i=0; i<3;i++)
	{
		fileUploadSettings.element = $('#sell_drobe_image_'+(i+1))[0];
		
		fileUploadSettings.onSubmit= fileUploadSubmit[i];
		fileUploadSettings.onComplete= fileUploadComplete[i];
		uploader.push(new qq.FileUploader(fileUploadSettings));


		$('.qq-upload-button input[type="file"]').livequery(function(){
			$(this).height($(this).closest('.image_box').height());
			
		})
			
		<?php 
		/*$i=1;
		foreach($this->data['SellDrobeImage'] as $drobe_image)
		{
			echo "showAdditionalImage($('#sell_drobe_image_".$i."'),'".$drobe_image['file_name']."');";
			$i++;
		}*/
		?>
	}

			$("#file_uploader").click(function(){});

			
			var uploader = new qq.FileUploader({
				    element: $('#file-uploader')[0],
				    uploadButtonText: "Click here to upload Image",
				    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'original')); ?>',
				    params: {type:'drobes'},
				    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
				    debug: false,
				    sizeLimit:4097152,
				    onSubmit: function(id, fileName){
					    // Destroy qtip when upload drobe image on screen.
				    	$("#file-uploader").qtip('destroy');
				    	$("#file-uploader").hide();
				    	$('#file_uploader').block({ 
							message: "<h3>Uploading Image</h3>",
					    	css: { 
					            border: 'none', 
					            padding: '10px', 
					            backgroundColor: '#000', 
					            '-webkit-border-radius': '10px !important', 
					            '-moz-border-radius': '10px !important', 
					            opacity: .5, 
					            color: '#fff' 
				        } }); 
					    },
				    onComplete: function(id, fileName, responseJSON){
				    	//$("#file-uploader").show();
				    	//$('#file_uploader').unblock();		    	
				    	upload_response(responseJSON);
					},
					onCancel: function(id, fileName){},
					messages: {
						typeError: "You can upload only JPEG, PNG or GIF Image file.",
			            sizeError: "You can upload less then 2MB file size.",
			            emptyError: "Uploaded file is empty.",
					 },
					 showMessage: function(message){ 
						 tip_message('#file-uploader',message);
					}
				});

			var uploader = new qq.FileUploader({
			    element: $('#sell_drobe_file-uploader')[0],
			    uploadButtonText: "Click here to upload Image",
			    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'original')); ?>',
			    params: {type:'drobes'},
			    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
			    debug: false,
			    sizeLimit:4097152,
			    onSubmit: function(id, fileName){
			    	//Destroy qtip when upload drobe image on screen.
			    	$("#sell_drobe_file-uploader").qtip('destroy');
				    $("#sell_drobe_file-uploader").hide();
			    	$('#sell_drobe_file_uploader').block({ 
						message: "<h3>Uploading Image</h3>",
				    	css: { 
				            border: 'none', 
				            padding: '10px', 
				            backgroundColor: '#000', 
				            '-webkit-border-radius': '10px !important', 
				            '-moz-border-radius': '10px !important', 
				            opacity: .5, 
				            color: '#fff' 
			        } }); 
				    },
			    onComplete: function(id, fileName, responseJSON){
					//$("#file-uploader").show();
			    	//$('#file_uploader').unblock();
			    	upload_response_sell_drobe(responseJSON);
				},
				onCancel: function(id, fileName){},
				messages: {
					typeError: "You can upload only JPEG, PNG or GIF Image file.",
		            sizeError: "You can upload less then 2MB file size.",
		            emptyError: "Uploaded file is empty.",
				 },
				 showMessage: function(message){ 
					 tip_message('#sell_drobe_file-uploader',message);
				}
			});

			/*
			 * fixing issue in chrome for clickeable area;
			 */
			$('.qq-upload-button input[type="file"]').each(function(){
				$(this).height(50);
			});
				
				
			/*$("#file_profile").change(function(){
				if($(this).val()!="")
				{
					try
					{
						$('#file_upload_form input[type="submit"]').click();	
						//fileUpload($('#file_upload_form')[0],$('#file_upload_form').attr('action'),'upload_response');	 
					}
					catch(e)
					{
						alert(e.valueOf()) 
					}
				}
			}); */
			
		   // $('#drobe_upload_btn').attr('disabled','disabled');
		    <?php 
		    if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']!="")
		    {
		    	?>
		    	$('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')).'/'.$this->data['Drobe']['file_name'])?>" );
		        $('.drobe_preview').show();
		        $('#file_upload').hide();
		        $('#drobe_upload_btn').removeAttr('disabled');
		    	<?php 
		    }
		    /*if($this->Session->read('Auth.User.status')!="active")
		    {
		    	?>
		    	$('#my_drobe_area').block({message:false});
		    	$('#drobe_upload_btn').attr('disabled','disabled').attr("type","button");
		    	<?php 
		    }*/
		    ?> 

		    
			// hide post for feedback screen on load time
			$('#my_drobe_area').hide();
			 
			// getting earning pefcentage from configuration file
			var earning_percent = $('#earning_percent').val();

			//calculate the seller earning after the admin cut his profit from seller listing value
			$(document).on('keyup','#drobe_price',function(){
				var earn_money = $('#drobe_price').val() - (($('#drobe_price').val() * earning_percent ) / 100);
				$('#your_earning').val("$" + earn_money.toFixed(2));
			});
		    
});





/*************************** Start additional image upload code from here ***************************************************/
var fileUploadSettings={
		element: null,
	    uploadButtonText: "Click here to upload Additional Image",
	    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'sell_drobes','admin'=>false)); ?>',
	    params: {type:'sell_drobes'},
	    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
	    debug: false,
	    sizeLimit:4097152,
	   onCancel: function(id, fileName){},
		messages: {
			typeError: "You can upload only JPEG, PNG or GIF Image file.",
		    sizeError: "You can upload less then 2MB file size.",
		    emptyError: "Uploaded file is empty.",
		 },
		showMessage: function(message){ 
			 tip_message('#file-uploader',message);
		}
	};



// additional imag remove from already uploaded
function showAdditionalImage(el, file_name)
{
	$('input[rel="'+el.attr('id')+'"]').val(file_name);
	el.prepend('<div class="image_preview"><div class="remove">X</div><img src="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('sell_drobe.thumb.upload_dir'))); ?>/'+file_name+'" alt="no preview available" /></div>');
	//el.find('.image_preview>img').attr('src','<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('sell_drobe.thumb.upload_dir'))); ?>'+obj.file);
	el.find('.image_preview').show();
	el.find('.qq-uploader').hide();
	el.unblock();
	el.find('.remove').each(function(){
		$(this).unbind('click').bind('click',function(){
			if(confirm('Are you sure to remove this Image from Additional Image?'))
			{
				el.find('.qq-uploader').fadeIn('fast',function(){el.find('.image_preview').remove()});
				$('input[rel="'+el.attr('id')+'"]').val("");
			}
			return false;
		});
	});
}

</script>
<?php 
}
?>