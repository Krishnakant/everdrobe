
<?php 
if($drobes)
{
	foreach($drobes as $drobe)
	{
		$total_new_reply=0;
		if(isset($drobe['Rate']))
		{
			foreach($drobe['Rate'] as $rate)
			{
				$total_new_reply += $rate['new_reply'];
			}
		}
	?>
	<div class="drobe_result received_result col-sm-6 col-md-4 mt10">
		<?php if($drobe['0']['total_response']>0){?>
		<div class="conversation">
		<div class="badges"><?php echo ($drobe['0']['total_response']); ?></div>
		</div>
		<?php } ?>
        <div class="media">
         <div class="media-left"> <?php echo $this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('alt'=>$drobe['Drobe']['file_name'],'height'=>"85", 'width'=>"85")).($drobe['Drobe']['is_highest_rated']>0 ? "<div class='highest-rated-drobe'></div>":""); ?>
         </div>
         <div class="user_cont media-body">
         	<div class="cont">
         	<div class="user_comment"><?php echo $drobe['Drobe']['comment']=="" ? "" : mb_strimwidth($drobe['Drobe']['comment'],0,30,"...")."";?></div>
         	<em style="font-size: smaller;"><?php echo $categoryList[$drobe['Drobe']['category_id']]?></em></div>
            <div class="btn btn-green btn-sm mt5"><?php echo $drobe['Drobe']['total_in'];?> Cute</div> 
             <div class="btn btn-danger btn-sm mt5"><?php echo $drobe['Drobe']['total_out'];?> Hot</div> 
            <div class="btn btn-default btn-sm mt5"><?php echo $drobe['Drobe']['total_rate']?> Votes</div> 
            <div class="delete close_rate" id=<?php echo $drobe['Drobe']['unique_id']; ?> ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></div> 
         </div>
         </div>
   		</div>
    
	<?php
	}
?>
<script type="text/javascript">
$(document).ready(function(){

	$('div.close_rate').unbind('click').click(function() {
		parentObj=$(this).parent().parent();
		$('#close_drobe_id').html($(this).attr('id'));
		Confirm('You will not get back selected drobe after remove it. Are you confirm to remove selected drobe?', function(yes) {
			if(yes)
			{
				if($('#close_drobe_id').html()!="")
				{
					$.ajax({
						url: 'drobes/close',
						type: "POST",
						data: {
							drobe_id: $('#close_drobe_id').html()
						},
						success: function(data) {
							$('.tabs ul li:first a').click();
						  	$('#close_drobe_id').html("");
						  	parentObj.remove();
						  	$("#got_feedback_area")[0].fleXcroll.updateScrollBars();
					  	}
					});
				}
				else
				{
					alert("Error occured in requesting close for rate");
					$("#close_no").click();
				}
			}
		});
		return false;
	});
	$('.received_result ').click(function()
	{
		drobe_id=$(this).find('.close_rate').attr('id');
		document.location.href="drobes/result/"+drobe_id;
		return false;
	});

});
</script>
<?php  	
}
else
{
	if(!isset($this->params->named['page']) || $this->params->named['page']==1)
	{
		?>
		<div class="spacer"></div>
		<p class="empty_message">No feedback found</p>
		<?php
	} 
}
?>
