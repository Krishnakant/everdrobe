<div class="tp tp_big drobe_result center-block" style="margin-top: 10px">
<div class="reward_response" id="reward_response" style="display:none"></div>
		<div class="media">
        	<div class="media-left">
         <?php echo $this->Html->image('/drobe_images/'.$drobeData['file_name'],array('alt'=>$drobeData['file_name'],'height'=>"85", 'width'=>"85")).($drobeData['is_highest_rated']>0 ? "<div class='highest-rated-drobe'></div>":""); ?>
         </div>
         <div class="media-body">
         <div class="user_cont">
         	<div class="cont"><?php echo $drobeData['comment']=="" ? "" : $drobeData['comment']." <br />";?><em><?php echo $categoryList[$drobeData['category_id']]?></em></div>
            <div class="up btn btn-green btn-sm mt5"><?php echo $drobeData['total_in'];?> Cute</div> 
            <div class="down btn btn-danger btn-sm mt5"><?php echo $drobeData['total_out'];?> Hot</div> 
            <div class="whatever btn btn-default btn-sm mt5">Total Votes: <?php echo $drobeData['total_rate']?></div> 
            <?php if($drobeData["user_id"]==$this->Session->read('Auth.User.id')){?> 
            <div class="delete close_rate" style="position:relative;bottom:20px;"></div>
            <?php }?> 
         </div>
         </div>
         </div>
   		</div>
     <div class="clear"></div>
<div id="TabbedPanels1" class="TabbedPanels">
		  <ul class="TabbedPanelsTabGroup">
		    <li class="TabbedPanelsTab" tabindex="0">All Rates</li>
		    <li class="TabbedPanelsTab" tabindex="0">Only Commented</li>
	      </ul>
		  <div class="TabbedPanelsContentGroup">
		    <div class="TabbedPanelsContent">
            	<div id='all_rates' class='drobe_result_area infiniteScroll' >
                </div>
            </div>
		    <div class="TabbedPanelsContent">
            	<div id='only_commented' class='drobe_result_area infiniteScroll'></div>
            </div>
           
	      </div>
    </div>
<div class="spacer"></div>
<script type="text/javascript">
var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
$(document).ready(function(){
	$('.TabbedPanelsTab').click(function(){
		current_index=TabbedPanels1.getCurrentTabIndex();
		var selectedtab = '.TabbedPanelsContentGroup > div:eq('+current_index+') .infiniteScroll';
		if($(selectedtab)[0].fleXcroll == undefined)
		{
		   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
		   apply_infinite_scrolling($(selectedtab).attr('id'));
		}	 
	});
	$('#all_rates').load('<?php echo $this->Html->url(array('controller'=>"rates",'action'=>"result",$drobeData['unique_id'], "all"))?>', function(){
		$('.TabbedPanelsTab:eq(0)').click();
		set_scrollbar(0);
		});
    $('#only_commented').load('<?php echo $this->Html->url(array('controller'=>"rates",'action'=>"result",$drobeData['unique_id'],"comment"))?>', function(){
    	 set_scrollbar(1);
        });
    $('div.close_rate').unbind('click').click(function() {
		Confirm('You will not get back this drobe after remove it. Are you confirm to remove this drobe?', function(yes) {
			if(yes)
			{
				if($('#close_drobe_id').html()!="")
				{
					$.ajax({
						url: 'drobes/close',
						type: "POST",
						data: {
							drobe_id: "<?php echo $drobeData['unique_id']; ?>"
						},
						success: function(data) {
							if(document.referrer)
							{
								document.location.href=document.referrer
							}
							else
							{
								document.location.href="results";
							}
					  	}
					});
				}
				else
				{
					alert("Error occured in requesting close for rate");
					$("#close_no").click();
				}
			}
		});
	});
});
function set_scrollbar(index)
{
	var selectedtab = '.TabbedPanelsContentGroup > div:eq('+index+') .infiniteScroll';
	if($(selectedtab)[0].fleXcroll== undefined)
	{
	   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
	   apply_infinite_scrolling($(selectedtab).attr('id'));
	}
}
</script>
		