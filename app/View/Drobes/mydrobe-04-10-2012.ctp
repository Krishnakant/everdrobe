<?php
if(isset($drobe_uploaded) && $drobe_uploaded==true)
{
	$category=Cache::read('drobe_category');
	?>
	<div class="drobe_uploaded">
	<div class="left" style="font-size: smaller"><?php echo $category[$this->data['Drobe']['category_id']]; ?></div>
	<div class="drobe_preview big-img" style="display: block">
	<div class="drobe_comment"><?php  echo $this->data['Drobe']['comment']; ?></div>
	<div class="big-img" style="margin: 0;"><?php 
	echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name']."?rand=".uniqid(),array('border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));
	?></div>
	</div>
	<div class="clear"></div>
	    <h2>
          That's it!
        </h2>
          <p>
            Check responses in a couple of minutes to see lots of opinions.
          </p>
          <div class="actions">
          <p>
          <?php 
          echo $this->Html->link('Give Rate',array('controller'=>"drobes","action"=>'rate'),array("class"=>"blue_btn"));
          ?>
          </p>
          </div>
      
	</div>
	
	<?php
}
else
{
?>
<div id="my_drobe_area">
<div class="drobe_preview big-img">
<div class="big-img" style="overflow:hidden;">
<img src="images/big-photo.jpg" id="crop_image"/>
</div>
<div id="clear_preview" class="right">X</div>
<div class="clear"></div>
</div>
<div class="big-img" id="file_uploader" <?php if(isset($drobe_uploaded) && $drobe_uploaded==false) echo "style='display:none'"; ?>>
<div class="drobe_upload_msg" id="file-uploader">Click here to upload File</div>
<img src="images/big-photo.jpg" />
</div>
<?php
echo $this->Form->create('Drobe',array('class'=>"form", 'type'=>'file'));
echo $this->Form->hidden('file_name');
?>
<div class="details"><?php echo $this->Form->input('category_id',array('label'=>false,'options'=>$categoryList,'empty'=>"Select Category"));?></div>
<div class="more_margin" >
<?php echo $this->Form->textarea('comment',array('label'=>false,"cols"=>32,"rows"=>3,"class"=>"form-control","placeholder"=>"Write your question here")); 
echo $this->Form->submit('SUBMIT',array('class'=>"btn btn-success mt10","id"=>"drobe_upload_btn")); ?>
</div>
<div class="more_margin more_options right" style="padding-left: 10px;">
<label>Target By Gender</label>
<div>
<?php 
echo $this->Form->input('DrobeSetting.target', array('before' => '','after' => '', 'between' => '', 'separator' => '','legend'=>false, 'options' => array('both'=>"Male/Female<br>",'male'=>"Male<br>", 'female'=>'Female'),'type' => 'radio','default'=>"both"));
?>
</div>
</div>
<div class="left">
<?php 

?>
</div>
<input type="hidden" id="pos_left" name="pos_left" />
<input type="hidden" id="pos_top" name="pos_top" />
<input type="hidden" id="new_width" name="new_width" />
<input type="hidden" id="new_height" name="new_height" />
<?php 
echo $this->Form->end();
?>
<div class="clear"></div>
</div>
<div id="drobe_uploaded" style="display: none"></div>
<script type="text/javascript">
var max_width=450;
var max_height=450;
var display_ratio=1;
var drobe_image_dir="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir'))).'/'; ?>";
$(function() {

	$("#file_uploader").click(function(){
		})
	$('#DrobeMydrobeForm').ajaxForm(
			{
				target: '#drobe_uploaded',
				beforeSubmit:  function(){
					$('#my_drobe_area').block({ 
						message: "<h3>Submitting</h3>",
				    	css: { 
				            border: 'none', 
				            padding: '10px', 
				            backgroundColor: '#000', 
				            '-webkit-border-radius': '10px !important', 
				            '-moz-border-radius': '10px !important', 
				            opacity: .5, 
				            color: '#fff' 
			        } }); 
					},
				success: function(responseText, statusText, xhr, $form){
					$('#my_drobe_area').unblock();
					$('#my_drobe_area').hide();
					$('#drobe_uploaded').fadeIn();
				}  // post-submit callback
			}
	);
	var uploader = new qq.FileUploader({
		    element: $('#file-uploader')[0],
		    uploadButtonText: "<span class='message'>Click here to upload Image</span>",
		    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'original')); ?>',
		    params: {type:'drobes'},
		    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
		    debug: false,
		    sizeLimit:4097152,
		    onSubmit: function(id, fileName){
			    $("#file-uploader").hide();
		    	$('#file_uploader').block({ 
					message: "<h3>Uploading Image</h3>",
			    	css: { 
			            border: 'none', 
			            padding: '10px', 
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '10px !important', 
			            '-moz-border-radius': '10px !important', 
			            opacity: .5, 
			            color: '#fff' 
		        } }); 
			    },
		    onComplete: function(id, fileName, responseJSON){
		    	//$("#file-uploader").show();
		    	//$('#file_uploader').unblock();
			   upload_response(responseJSON);
			},
			onCancel: function(id, fileName){},
			messages: {
				typeError: "You can upload only JPEG, PNG or GIF Image file.",
	            sizeError: "You can upload less then 2MB file size.",
	            emptyError: "Uploaded file is empty.",
			 },
			 showMessage: function(message){ 
				 tip_message('#file-uploader',message);
			}
		});
	
	
	$("#file_profile").change(function(){
		if($(this).val()!="")
		{
			try
			{
				$('#file_upload_form input[type="submit"]').click();	
				//fileUpload($('#file_upload_form')[0],$('#file_upload_form').attr('action'),'upload_response');	 
			}
			catch(e)
			{
				alert(e.valueOf()) 
			}
		}
	}); 
	$('#drobe_upload_btn').click(function(){
		var return_val=true;
		if($('#DrobeCategoryId').val()=="")
		{
			tip_message('#DrobeCategoryId',"Please select category");
			return_val= false;
		}
		if($('#DrobeFileName').val()=="")
		{
			tip_message('#file-uploader',"Please upload your image");
			return_val=false;
		}
		if($('#DrobeComment').val()=="")
		{
			tip_message('#DrobeComment',"Your question is required");
			return_val=false;
		}
		return return_val;
	});
	$('#clear_preview').click(function() {
		Confirm('Are you sure to change this image?', function(yes) {
			if(yes)
			{
				var filename=$('#DrobeFileName').val();
				$.ajax({
	    			  url: 'drobes/delete_image/'+escape(filename.replace(".","__")),
	    			  success: function(data) {
	    			   	document.location.href=document.location.href;    			    
	    			  }
	    		});
			}
		});
		return false;
	});
	
   // $('#drobe_upload_btn').attr('disabled','disabled');
    <?php 
    if(isset($this->data['Drobe']['file_name']) && $this->data['Drobe']['file_name']!="")
    {
    	?>
    	$('.drobe_preview img').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')).'/'.$this->data['Drobe']['file_name'])?>" );
        $('.drobe_preview').show();
        $('#file_upload').hide();
        $('#drobe_upload_btn').removeAttr('disabled');
    	<?php 
    }
    ?> 
});
function upload_response(obj)
{
	if(obj.type=="success")
    {
		display_image_cropping(obj);
    }
    else
    {  
        alert(obj.message);
    	$('#DrobeFileName').val("");
    	$('#drobe_upload_btn').attr('disabled','disabled');
    }
}
</script>
<?php 
}
?>