<?php
if(count($request_for_label)>0)
{
	?>
	
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('RequestForLabel.id','#');?></th>
			<th width="200px"><?php echo $this->Paginator->sort('RequestForLabel.reason_for','Reason For Request');?></th>
			<th width="120px"><?php echo $this->Paginator->sort('RequestForLabel.request_on','Requested Date');?></th>
			<th width="150px"><?php echo $this->Paginator->sort('User.first_name','Requested By');?></th>
			<th width="150px"><?php echo $this->Paginator ->sort('User.username','Requested User Name');?>
			<th width="50px"><?php echo $this->Paginator->sort('RequestForLabel.weight','Weight');?></th>
			<th width="90px"><?php echo $this->Paginator->sort('RequestForLabel.order_id','Order Id');?></th>
			<th width="70px">Actions</th>
		</tr>
		<?php 
		foreach($request_for_label as $request)
		{
			?>
			<tr class="<?php echo $request['RequestForLabel']['id']; ?>">
				<td width="10px" ><?php echo $request['RequestForLabel']['id'];?></td>
				<td width="30px"><?php echo $request['RequestForLabel']['reason_for'];?></td>				
				<td width="30px"><?php echo date('Y-m-d',strtotime($request['RequestForLabel']['request_on']));?></td>
				<td width="30px"><?php echo $request['User']['first_name']." ".$request['User']['last_name'];?></td>
				<td width="30px"><?php echo $request['User']['username'];?></td>
				<td width="30px"><?php echo $request['RequestForLabel']['weight'];?></td>
				<td width="30px"><?php echo $request['RequestForLabel']['order_id'];?></td>
				<td>
				<div class="actions">
				<?php echo $this->Html->link('Approve', array("controller"=>"request_for_labels","action"=>"approve_request",$request['RequestForLabel']['id'],"admin"=>true),null,'Are you sure approve ?'); ?>
				</div>
				</td>				
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>