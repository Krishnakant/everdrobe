<div class="spacer"></div>
<div class="right actions">
<?php 
echo $this->Html->link('Pending Request ('."$pending_request".')', array("controller"=>"request_for_labels","action"=>"pending_label_request","admin"=>true))." ";
//echo $this->Html->link('Remove Requests ('.$remove_request.')', array("controller"=>"drobes","action"=>"remove_drobe_request","admin"=>true));
?>
</div>
<h2 class="left">Request for Label</h2>
<div class="clear"></div>
<div class="search_box" >
<form action="" method="get">
<?php
if(count($request_for_label)>0)
{
	?>
	
	<table>
		<tr>
			<th width="10px"><?php echo $this->Paginator->sort('RequestForLabel.id','#');?></th>
			<th width="80px"><?php echo $this->Paginator->sort('User.first_name','Requested By');?></th>
			<th width="100px"><?php echo $this->Paginator->sort('User.username','Requested User Name') ?>
			<th width="100px"><?php echo $this->Paginator->sort('RequestForLabel.reason_for','Reason for');?></th>
			<th width="60px"><?php echo $this->Paginator->sort('RequestForLabel.request_on','Requested Date');?></th>
			<th width="20px"><?php echo $this->Paginator->sort('RequestForLabel.weight','Weight');?></th>
			
		</tr>
		<?php 
		foreach($request_for_label as $request)
		{
			?>
			<tr class="<?php echo $request['RequestForLabel']['id']; ?>">
				<td><?php echo $request['RequestForLabel']['id'];?></td>
				<td><?php echo $request['User']['first_name']." ".$request['User']['last_name'];?></td>
				<td><?php echo $request['User']['username'];?></td>
				<td><?php echo $request['RequestForLabel']['reason_for'];?></td>				
				<td><?php echo date('Y-m-d',strtotime($request['RequestForLabel']['request_on']));?></td>
				<td><?php echo $request['RequestForLabel']['weight'];?></td>				
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
</form>
</div>