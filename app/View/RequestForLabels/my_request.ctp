<h2>Request For Label</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>

<?php 
	echo $this->Form->create("RequestForLabel");
?>
<div class="spacer"></div>
<div class="drobe_sale_popup" style="display: block;">
<div class="half left">	
	<?php 
		$options = array(
				'Never Received'=>'Never Received',
				'Label Expired'=>'Label Expired',
				'Need Heavier Weight Label'=>'Need Heavier Weight Label'
				);
		echo $this->Form->input("reason_for",array('label'=>'Reason ','options'=>$options,'id'=>'reason_for','onchange'=>'check_val(this.value)'));
		
	?>
	
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input("email",array('value'=>$email));
	?>
	
	<?php 
		echo $this->Form->input("street1",array('value'=>$profile_data['street1'],'label'=>'<b>Street1</b>','name'=>'street1'));
	?>
	<?php 
		echo $this->Form->input("street2",array('value'=>$profile_data['street2'],'label'=>'<b>Street2</b>','name'=>'street2'));
	?>
	</div>
	<div class="half right">
	<?php 
		echo $this->Form->input('weight', array('label'=>'<b>Weight</b>','options' => $weightList));
	?>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input("city",array('value'=>$profile_data['city'],'label'=>'<b>City</b>','name'=>'city'));
	?>
	<?php 
		echo $this->Form->input("state",array('value'=>$profile_data['state'],'label'=>'<b>State</b>','name'=>'state'));
	?>
	<?php 
		echo $this->Form->input("zip",array('value'=>$profile_data['zip'],'label'=>'<b>Zip</b>','name'=>'zip'));
	?>
	</div>
	
	<div class="spacer"></div>
	<!-- Label reason for never_received    -->
	<div id="never_received" class="dashboard_box" style="font-size:14px;text-align: justify;">
		<?php echo "<p>Your shipping label is emailed to you.</p>";?>
	</div>
	<!-- Label reason for label expired  -->
	<div id="label_expired" class="dashboard_box" style="font-size:14px;text-align: justify;">
		<?php echo "<p>As a courtesy we will resend you a new shipping labe. Please ship your items promptly after receiving the table.</p>";?>
	</div>
	<!-- Label reason for need heavier weight -->
	<div id="heavier_weight" class="dashboard_box" style="font-size:14px;text-align: justify;">
		<?php echo "<p>Overweight packages will incur extra fees. These will be deducted from your earnings/credits. </p>";?>
	</div>
</div>

<div class="spacer"></div>
<?php 
	echo $this->Form->submit("Submit Request");
?>



<script>
	$(document).ready(function(){
		$('#label_expired').hide();
		$('#heavier_weight').hide();
	});

	function check_val(val)
	{
		if(val=="Never Received")
		{
			$('#never_received').show();
			$('#label_expired').hide();
			$('#heavier_weight').hide();
		}
		else if(val=="Label Expired")
		{
			$('#never_received').hide();
			$('#label_expired').show();
			$('#heavier_weight').hide();	
		}
		else if(val=="Need Heavier Weight Label")
		{
			$('#never_received').hide();
			$('#label_expired').hide();
			$('#heavier_weight').show();
		}
	}
</script>
