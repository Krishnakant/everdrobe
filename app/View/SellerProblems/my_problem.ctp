<h2>Problems/Inquiries</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<?php 
//Get Buyer name from it's address in SoldDrobe table
$address = (json_decode($drobe_data['SoldDrobe']['shipping_address']));
$buyer = $address->first_name." ".$address->last_name;

?>
<div class="drobe_sale_popup">
	<div class="left half"> 
	<?php 
		echo $this->Form->create("SellerProblem");
	?>
	<?php 
		$options = array(
				'Delayed Shipment'=>'Delayed Shipment',
				'Release Payment'=>'Release Payment',			
				'Cancel Order'=>'Cancel Order'
				);
		echo $this->Form->input("problem_type",array('label'=>'<b>Problem Type</b>','options'=>$options,'id'=>'problem_type','onchange'=>'check_val(this.value)'));
	?>
	
	<div id="delay_reason">
			<div class="spacer"></div>
			<?php 
				echo $this->Form->input("expected_ship_date",array('type'=>'text','label'=>'<b>Expected shipping date </b>(mm/dd/yyyy)'));			
			?>
			<?php 
				echo $this->Form->input("reason_for_delay",array('label'=>'<b>Reason for delayed shipment</b>'));
			?>
			
		<div class="spacer"></div>
		<?php 
			echo $this->Form->submit("Submit");
		?>
	</div>
	</div>
</div>	

<div id="problem_type_div" style="font-size:14px;text-align:justify;">
	<div>
		<table>
		<tr>
		<td width="90px" rowspan="5">
		<?php echo $this->Html->image('/drobe_images/thumb/'.$drobe_data['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));?></td>
		</tr>
		<tr>
			<td colspan="2"><?php echo $drobe_data['Drobe']['comment'];?></td>
			
		</tr>
		<tr>
			<td><b>Size</b> : <?php echo $drobe_data['SellDrobe']['size_name'];?></td>
			<td><b>Buyer</b> : <?php echo $buyer;?></td>			
		</tr>
		<tr>
			<td><b>Order id</b> : <?php echo $drobe_data['SoldDrobe']['order_id'];?></td>
			<td><b>Order Placed</b> : <?php echo date('M d Y h:i a',strtotime($drobe_data['SoldDrobe']['order_date']));?></td>			
		</tr>
		<tr>
			<td colspan="2">
			<?php 
				if($drobe_data['SoldDrobe']['status']=='shipped')
				{
					$hour_diff = round((strtotime(date('Y-m-d'))-strtotime($drobe_data['SoldDrobe']['shipped_on']))/3600,1);
					if($hour_diff>72)
					{
						echo "<b>Status</b> : Delivered";
					}
					else 
					{
						echo "<b>Status</b> : ".$drobe_data['SoldDrobe']['status'];
					}
			?>
			<?php 		
				}
				else
				{	
			?>
				<b>Status</b> : <?php echo $drobe_data['SoldDrobe']['status']?>
			<?php
				} 
			?>
			
			</td>
		</tr>	
		</table>
	</div>
	<div  class="dashboard_box" id="release_payment">
			<?php echo "<p>Buers typically accept items within a few hours of delivery and Everdrobe immediately releases tour earning on acceprance. If the buyer does not accept the item and does not report any problem, Everdrobe will automatically release the earnings 72 hours from delivery of the order.</p>";?>
	</div>
	
	<div  class="dashboard_box" id="cancel_order">
			<?php echo "<p>Order can not be cancelled. All cancellations are subject to approval by Everdrobe.</p>";?>
	</div>
</div>


<script>
function check_val(val=null)
{
	if(val == "Delayed Shipment")
	{
		$('#delay_reason').show();
		$('#problem_type_div').hide();
	}
	else if(val=="Release Payment")
	{
		$('#problem_type_div').show();
		$('#release_payment').show();
		$('#delay_reason').hide();
		$('#cancel_order').hide();
		
	}
	else if(val=="Cancel Order")
	{
		$('#problem_type_div').show();
		$('#cancel_order').show();
		$('#release_payment').hide();
		$('#delay_reason').hide();
	}
	
}


$(document).ready(function(){
	$('#problem_type_div').hide();
});

</script>