<?php
if(count($request_for_problem)>0)
{
	?>
	
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('SellerProblem.id','#');?></th>
			<th width="80px"><?php echo $this->Paginator->sort('SellerProblem.problem_type','Problem Type');?></th>
			<th width="80px"><?php echo $this->Paginator->sort('User.first_name','Enquiry By');?></th>
			<th width="100px><?php echo $this->Paginator->sort('User.username','Enquiry User Name')?></th>
			<th width="100px"><?php echo $this->Paginator->sort('SellerProblem.created_on','Created On');?></th>
			<th width="100px"><?php echo $this->Paginator->sort('SellerProblem.expected_ship_date','Expected Shipping Date');?></th>
			<th width="100px"><?php echo $this->Paginator->sort('SellerProblem.reason_for_delay','Reason For Delay');?></th>
			<th width="70px">Actions</th>
		</tr>
		<?php 
		foreach($request_for_problem as $request)
		{
			?>
			<tr class="<?php echo $request['SellerProblem']['id']; ?>">
				<td width="10px" ><?php echo $request['SellerProblem']['id'];?></td>
				<td width="30px"><?php echo $request['SellerProblem']['problem_type'];?></td>				
				<td width="30px"><?php echo $request['User']['first_name'].' '.$request['User']['last_name'];?></td>
				<td width="30px"><?php echo $request['User']['username'];?></td>
				<td width="30px"><?php echo date('Y-m-d',strtotime($request['SellerProblem']['created_on']));?></td>
				<td width="30px">
				<?php 
					echo ($request['SellerProblem']['expected_ship_date']!="")?date('Y-m-d',strtotime($request['SellerProblem']['expected_ship_date'])) : "";
				?>
				</td>
				<td width="30px"><?php echo $request['SellerProblem']['reason_for_delay'];?></td>
				<td>
					<div class="actions">
					<?php echo $this->Html->link('Approve', array("controller"=>"seller_problems","action"=>"approve_request",$request['SellerProblem']['id'],"admin"=>true),null,'Are you sure approve ?'); ?>
					</div>
				</td>				
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>

<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>