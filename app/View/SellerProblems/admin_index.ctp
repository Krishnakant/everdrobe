<div class="spacer"></div>
<div class="right actions">
<?php 
echo $this->Html->link('Pending Request ('."$pending_problem".')', array("controller"=>"seller_problems","action"=>"pending_problem_request","admin"=>true))." ";
//echo $this->Html->link('Remove Requests ('.$remove_request.')', array("controller"=>"drobes","action"=>"remove_drobe_request","admin"=>true));
?>
</div>
<h2 class="left">Request For Problems</h2>
<div class="clear"></div>
<div class="search_box" >
<form action="" method="get">
<?php
if(count($request_for_problem)>0)
{
	?>
	
	<table>
		<tr>
			<th width="10px"><?php echo $this->Paginator->sort('SellerProblem.id','#');?></th>
			<th width="90px"><?php echo $this->Paginator->sort('SellerProblem.problem_type','Problem Type');?></th>
			<th width="100px"><?php echo $this->Paginator->sort('User.first_name','Enquiry By');?></th>
			<th width="100px><?php echo $this->Paginator->sort('User.username','Enquiry User Name')?></th>
			<th width="70px"><?php echo $this->Paginator->sort('SellerProblem.created_on','Created On');?></th>
			<th width="70px"><?php echo $this->Paginator->sort('SellerProblem.expected_ship_date','Expected Shipping Date');?></th>
			<th width="100px"><?php echo $this->Paginator->sort('SellerProblem.reason_for_delay','Reason For Delay');?></th>
		</tr>
		<?php 
		foreach($request_for_problem as $request)
		{
			?>
			<tr class="<?php echo $request['SellerProblem']['id']; ?>">
				<td><?php echo $request['SellerProblem']['id'];?></td>
				<td><?php echo $request['SellerProblem']['problem_type'];?></td>				
				<td><?php echo $request['User']['first_name'].' '.$request['User']['last_name'];?></td>
				<td><?php echo $request['User']['username'];?></td>
				<td><?php echo date('Y-m-d',strtotime($request['SellerProblem']['created_on']));?></td>
				<td>
				<?php 
					echo ($request['SellerProblem']['expected_ship_date']!="")?date('Y-m-d',strtotime($request['SellerProblem']['expected_ship_date'])) : "";
				?>
				</td>
				<td width="30px"><?php echo $request['SellerProblem']['reason_for_delay'];?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
</form>
</div>

<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>