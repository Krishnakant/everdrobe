<div class="spacer"></div>
<div class="right actions">
<?php 
//echo $this->Html->link('Pending Request ('."$pending_request".')', array("controller"=>"redeem_requests","action"=>"pending_redeem_request","admin"=>true))." ";
//echo $this->Html->link('Remove Requests ('.$remove_request.')', array("controller"=>"drobes","action"=>"remove_drobe_request","admin"=>true));
?>
</div>
<h2 class="left">Redeems Request</h2>
<div class="clear"></div>
<div class="search_box" >
<form action="" method="get">
<?php
if(count($request_for_redeem_balance)>0)
{
	?>
	
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('RedeemRequest.id','#');?></th>
			<th width="60px"><?php echo $this->Paginator->sort('RedeemRequest.requested_amount','Requested Amount');?></th>
			<th width="60px"><?php echo $this->Paginator->sort('RedeemRequest.paid_amount','Paid Amount');?></th>
			<th width="90px"><?php echo $this->Paginator->sort('RedeemRequest.created_on','Requested Date');?></th>
			<th width="170px"><?php echo $this->Paginator->sort('User.first_name','Requested By');?></th>
			<th width="170px"> <?php echo $this->Paginator->sort('User.username','Requested User Name')?></th>
			<th width="80px"><?php echo $this->Paginator->sort('RedeemRequest.payment_type','Payment Type');?></th>
			<th width="70px"><?php echo $this->Paginator->sort('RedeemRequest.status','Status');?></th>
			<th width="290px">Action</th>
		</tr>
		<?php 
		foreach($request_for_redeem_balance as $request)
		{
			?>
			<tr class="<?php echo $request['RedeemRequest']['id']; ?>">
				<td width="10px" ><?php echo $request['RedeemRequest']['id'];?></td>
				<td style="text-align:right">$ <?php echo $request['RedeemRequest']['requested_amount'];?></td>
				<td style="text-align:right">$ <?php echo $request['RedeemRequest']['paid_amount'];?></td>				
				<td><?php echo date('Y-m-d',strtotime($request['RedeemRequest']['created_on']));?></td>
				<td><?php echo $request['User']['first_name']." ".$request['User']['last_name'];?></td>
				<td><?php echo $request['User']['username'];?></td>
				<td><?php echo $request['RedeemRequest']['payment_type'];?></td>
				<td><?php echo Inflector::camelize($request['RedeemRequest']['status']);?></td>
				<td>
					<div class="actions">
					<?php 
					if ($request['RedeemRequest']['status']=='unpaid')
					{
						echo $this->Html->link('Payment', array("controller"=>"seller_payments","action"=>"pay",$request['User']['id'],$request['RedeemRequest']['id'],"admin"=>true)).' ';
						echo $this->Html->link('Cancel', array("controller"=>"redeem_requests","action"=>"cancel_request",$request['User']['id'],$request['RedeemRequest']['id'],"admin"=>true),'','Are you sure to cancel request?');
					}
					else 
					{
						//echo $this->Html->link('Payment History', array("controller"=>"seller_payments","action"=>"history",$request['User']['id'],"admin"=>true),array('class'=>'history'));
						echo $this->Html->link('Payment History', 'javascript:void(0)',array('class'=>'history','rel'=>$request['User']['id']));
					}	
					?>
					</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
</form>
</div>

<div style='display:none;' id="history">
</div>

<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>

<script>
$(document).ready(function(){
	$(".history").click(function(){
		var user_id = $(this).attr('rel');
		
		$.ajax({
			url:"<?php echo Router::url(array('controller'=>'seller_payments','action'=>'history','admin'=>'true'));?>" + "/" +user_id,
			success:function(result)
			{
				$("#history").html(result);
				$.colorbox({html:$("#history").html()});
			},
		});
	});
});

</script>