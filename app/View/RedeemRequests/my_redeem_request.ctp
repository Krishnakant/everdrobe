<h2>Redeem Request</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
	
<div class="drobe_sale_popup">
<?php 
echo $this->Form->create("RedeemRequest");
?>
<div class="spacer"></div>
<b>Select Payment Type </b>
<div class="payment_radio">
	<?php 
		//echo $this->Form->input('payment_type', array('legend'=>false,'options' => array("cheque"=>"By Cheque","paypal"=>"By Paypal","other"=>"Other"),'type' => 'radio','default'=>'cheque'));
		//array("cheque"=>"By Cheque","paypal"=>"By Paypal","deposit"=>"Bank Direct Deposit","other"=>"Other")
		$payment_settings = Configure::read('setting');
		$options = array();
		if($payment_settings['payment_option_cheque']=="on") $options['cheque'] = 'By Cheque';
		if($payment_settings['payment_option_paypal']=="on")$options['paypal'] = 'By Paypal';
		if($payment_settings['payment_option_deposit']=="on")$options['deposit'] = 'Bank Direct Deposit';
		if($payment_settings['payment_option_other']=="on")$options['other'] = 'Other';
		$keys = array_keys($options); 
		
	echo $this->Form->input('SellProfile.payment_type', array('legend'=>false,'options' => $options ,'type' => 'radio','default'=>$keys[0]));
	?>
</div>
<div id="inner_input">
	<?php
	echo $this->Form->input("requested_amount",array("label"=>"Redeemable Amount",'type'=>'text','value'=>$redeem_balance,'readonly'));
	?>
		<?php if($payment_settings['payment_option_cheque']=="on"):?>
		<div id="cheque_profile">
		<div class="spacer"></div>
			<?php 
			
		
				echo $this->Form->input("SellProfile.full_name",array('label'=>'User Name'));
				echo $this->Form->input("SellProfile.street1",array('label'=>'Street1'));
				echo $this->Form->input("SellProfile.street2",array('label'=>'Street2'));
				echo $this->Form->input("SellProfile.city",array('label'=>'City'));
				echo $this->Form->input("SellProfile.state",array('label'=>'State'));
				echo $this->Form->input("SellProfile.zip",array('label'=>'Zip'));			
			?>
		</div>
		<?php endif;?>
		<?php if($payment_settings['payment_option_deposit']=="on"):?>
		<div id="deposit_profile">
		<div class="spacer"></div>
			<?php 
				echo $this->Form->input("SellProfile.deposit_name",array('label'=>'Name'));
				echo $this->Form->input("SellProfile.account_no",array('label'=>'Account No.'));
				echo $this->Form->input("SellProfile.routing_no",array('label'=>'Routing No.'));
				echo $this->Form->input("SellProfile.bank_name",array('label'=>'Bank Name'));
				echo $this->Form->label("SellProfile.account_type",'<span class="required">Account Type</span>');
				echo $this->Form->input("SellProfile.account_type",array('legend'=>false,'label'=>false,'options' =>array("checking"=>"Checking","saving"=>"Saving"),'default'=>'checking'));
			?>
		</div>
		<?php endif;?>
		<?php if($payment_settings['payment_option_paypal']=="on"):?>
		<div id="paypal_profile">
		<div class="spacer"></div>
			<?php 
				echo $this->Form->input("SellProfile.paypal_email",array('label'=>'Paypal Email ID'));
			?>
		</div>
		<?php endif; ?>
		
	<?php 
		if($redeem_balance!=0)
		{	
			echo $this->Form->submit("Submit Request");
		}	
	?>
</div>	
</div>
	<?php if($payment_settings['payment_option_other']=="on"):?>
	<div class="spacer"></div>
	<div id="other_payment_detail" class="dashboard_box" style="font-size:14px;text-align: justify;">
		<?php echo Configure::read("setting.payment_type_other");?>
	</div>
	<?php endif; ?>

<script>
$(document).ready(function(){
	/* Show div on selected radio button */
	var default_radio_id = $(":radio:checked:first").attr('id');
	/* If no any radio button is selected then default first payment method has been select */
	if(default_radio_id == undefined){
		var default_radio = $(":radio:first").attr('id');
		$("#"+default_radio).prop('checked',true);
		default_radio_id = default_radio;
	}
	if(default_radio_id == 'SellProfilePaymentTypeCheque') 
	{
		$('#cheque_profile').show();
		$('#paypal_profile').hide();
		$("#deposit_profile").hide();
		$('#other_payment_detail').hide();		
	}	
	

	if(default_radio_id == 'SellProfilePaymentTypePaypal')
	{
		$('#paypal_profile').show();
		$('#cheque_profile').hide();
		$("#deposit_profile").hide();
		$('#other_payment_detail').hide();		
	}

	if(default_radio_id == 'SellProfilePaymentTypeDeposit')
	{
		$("#deposit_profile").show();
		$('#paypal_profile').hide();
		$('#cheque_profile').hide();
		$('#other_payment_detail').hide();
	}
	
	if(default_radio_id == 'SellProfilePaymentTypeOther')
	{
		$('#other_payment_detail').show();
		$("#deposit_profile").hide();
		$('#paypal_profile').hide();
		$('#cheque_profile').hide();
	}
		
	$('#SellProfilePaymentTypePaypal').click(function(){
		$('#paypal_profile').show();
		$("#deposit_profile").hide();
		$('#cheque_profile').hide();
		$('#inner_input').show();
		$('#other_payment_detail').hide();
	});

	$('#SellProfilePaymentTypeCheque').click(function(){
		$('#paypal_profile').hide();
		$("#deposit_profile").hide();
		$('#cheque_profile').show();
		$('#inner_input').show();		
		$('#other_payment_detail').hide();
	});

	$('#SellProfilePaymentTypeOther').click(function(){
		$('#other_payment_detail').show();
		$('#inner_input').hide();
	});

	$("#SellProfilePaymentTypeDeposit").click(function(){
		$("#deposit_profile").show();
		$('#cheque_profile').hide();
		$('#paypal_profile').hide();
		$('#inner_input').show();		
		$('#other_payment_detail').hide();
	});

		
	$("#RedeemRequestPaypalEmail").parent().addClass('required');
	
	
});
</script>