<?php
if(count($request_for_redeem)>0)
{
	?>
	
	<table>
		<tr>
			<th width="20px"><?php echo $this->Paginator->sort('RedeemRequest.id','#');?></th>
			<th width="50px"><?php echo $this->Paginator->sort('RedeemRequest.requested_amount','Amount');?></th>
			<th width="50px"><?php echo $this->Paginator->sort('RedeemRequest.created_on','Requested Date');?></th>
			<th width="140px"><?php echo $this->Paginator->sort('User.first_name','Requested By');?></th>
			<th width="140px"><?php echo $this->Paginator->sort('User.username','Requested User Name');?>
			<th width="60px"><?php echo $this->Paginator->sort('RedeemRequest.payment_type','Payment Type');?></th>
			<th width="50px">Actions</th>
		</tr>
		<?php 
		foreach($request_for_redeem as $request)
		{
			?>
			<tr class="<?php echo $request['RedeemRequest']['id']; ?>">
				<td><?php echo $request['RedeemRequest']['id'];?></td>
				<td style="text-align:right">$ <?php echo $request['RedeemRequest']['requested_amount'];?></td>				
				<td><?php echo date('Y-m-d',strtotime($request['RedeemRequest']['created_on']));?></td>
				<td><?php echo $request['User']['first_name']." ".$request['User']['last_name'];?></td>
				<td><?php echo $request['User']['username'];?></td>
				<td><?php echo $request['RedeemRequest']['payment_type'];?></td>
				<td>
				<div class="actions">
				<?php echo $this->Html->link('Approve', array("controller"=>"redeem_requests","action"=>"approve_request",$request['RedeemRequest']['id'],"admin"=>true),null,'Are you sure approve ?'); ?>
				
				</div>
				</td>				
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>