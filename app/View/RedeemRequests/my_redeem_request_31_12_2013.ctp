<h2>Redeem Request</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
	
<div class="drobe_sale_popup">
<?php 
	echo $this->Form->create("RedeemRequest");
?>
<div class="spacer"></div>
<b>Select Payment Type </b>
<div class="payment_radio">
	<?php 
		echo $this->Form->input('payment_type', array('legend'=>false,'options' => array("cheque"=>"By Cheque","paypal"=>"By Paypal","other"=>"Other"),'type' => 'radio','default'=>'cheque'));
	?>
</div>
<div id="inner_input">
	<?php
	echo $this->Form->input("requested_amount",array("label"=>"Redeemable Amount",'type'=>'text','value'=>$redeem_balance,'readonly'));
	?>
	
		<div id="cheque_profile">
		<div class="spacer"></div>
			<?php 
				echo $this->Form->input("SellProfile.full_name",array('label'=>'User Name'));
				echo $this->Form->input("SellProfile.street1",array('label'=>'Street1'));
				echo $this->Form->input("SellProfile.street2",array('label'=>'Street2'));
				echo $this->Form->input("SellProfile.city",array('label'=>'City'));
				echo $this->Form->input("SellProfile.state",array('label'=>'State'));
				echo $this->Form->input("SellProfile.zip",array('label'=>'Zip'));			
			?>
		</div>
		<div id="paypal_profile">
		<div class="spacer"></div>
			<?php 
				echo $this->Form->input("SellProfile.paypal_email",array('label'=>'Paypal Email ID'));
			?>
		</div>
	<?php 
		if($redeem_balance!=0)
		{	
			echo $this->Form->submit("Submit Request");
		}	
	?>
</div>	
</div>
<div class="spacer"></div>
	<div id="other_payment_detail" class="dashboard_box" style="font-size:14px;text-align: justify;">
		<?php echo Configure::read("setting.payment_type_other");?>
	</div>

<script>
$(document).ready(function(){
	if($('#RedeemRequestPaymentTypeCheque').is(':checked')){
		$('#paypal_profile').hide();
		$('#other_payment_detail').hide();
		$('#cheque_profile').show();		
	}	
	else
	{
		$('#paypal_profile').show();
		$('#cheque_profile').hide();			
	}

	if($('#RedeemRequestPaymentTypePaypal').is(':checked')){
		$('#paypal_profile').show();
		$('#other_payment_detail').hide();
		$('#cheque_profile').hide();		
	}
	
		
	$('#RedeemRequestPaymentTypePaypal').click(function(){
		$('#inner_input').show();
		$('#paypal_profile').show();
		$('#cheque_profile').hide();
		$('#other_payment_detail').hide();
	});

	$('#RedeemRequestPaymentTypeCheque').click(function(){
		$('#paypal_profile').hide();
		$('#inner_input').show();
		$('#cheque_profile').show();		
		$('#other_payment_detail').hide();
	});

	$('#RedeemRequestPaymentTypeOther').click(function(){
		$('#other_payment_detail').show();
		$('#inner_input').hide();
	});

	$("#RedeemRequestPaypalEmail").parent().addClass('required');
	
});
</script>