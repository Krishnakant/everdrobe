<div class="user_rates">
<div class="spacer"></div>
<?php 
foreach($rates as $rate)
{
	if($rate['Rate']['rate'] == 1) $rate_type="Cute";
	else if($rate['Rate']['rate'] == -1) $rate_type="Hot";
	else $rate_type="";
	?>
	<div class="user_rate">
	<div class="rate btn btn-<?php echo $rate_type; ?> pull-right"><?php echo $rate_type; ?></div>
	<div class="profile_photo"><?php 
	if($rate['User']['photo']!="") {
		echo $this->Html->image('/'.str_replace(DS,"/",Configure::read('profile.thumb.upload_dir'))."/".$rate['User']['photo'],array('height'=>30));
	}
	else
	{
		echo $this->Html->image(Configure::read('profile.default_image'),array('height'=>30));
	}
	if($rate['User']['star_user']==1):?>
		<div class="star-user-small"></div>
	<?php endif;?></div>
	<div class="user_info">
	<?php
		if(isset($rate['User']['username']) && $rate['User']['username'] != null)
		{
			echo $this->Html->link($rate['User']['username'],array('controller'=>"users","action"=>"profile",$rate['User']['unique_id'])); 
		}
		else
		{
			echo $this->Html->link($rate['User']['first_name']." ".$rate['User']['last_name'],array('controller'=>"users","action"=>"profile",$rate['User']['unique_id'])); 
	}?>
	<br/>
	
	<small>
	<?php echo $rate['User']['gender'].($rate['User']['country']!=""? ", ".$rate['User']['country']: "").".";?>
	</small>
	</div>
	<?php 
	if($rate['Rate']['comment']!="")
	{
		?>
		<div class="conversation"><?php 
		if($rate['Rate']['conversation_status']=="open")
		{
			//echo $this->Html->link($this->Html->image('/images/reply_icon.gif',array('border'=>0)),array('controller'=>'drobes','action'=>'conversation',$this->params['pass'][0],$rate['Rate']['conversation_id']),array('escape'=>false));
			echo $this->Html->link("Reply",array('controller'=>'drobes','action'=>'conversation',$this->params['pass'][0],$rate['Rate']['conversation_id']),array('class'=>"btn btn-primary btn-xs reply_btn",'escape'=>false));
		}
		else
		{
			//echo "Closed";
		}
		if($rate["Rate"]["new_reply"]>0)
		{
			?>
			<div class="badges"><?php echo $rate['Rate']['new_reply']; ?></div>
			<?php 
		}
		?></div>
		<div class="clear"></div>
		
		<a href="javascript:void(0)" class="<?php echo $rate['Rate']['conversation_id']?>">
		
		 
		
	<span <?php if($rate['Rate']['rate'] != 1 && $rate['Rate']['rate'] != -1) echo 'style="right:5px"'?> class="reward_button <?php echo $rate['Rate']['rewarded']>0 ? "rewarded" : ""; ?>"></span>
	</a>
	<div class="comment"><?php echo $rate['Rate']['comment'];?></div>
	<?php 
	}
	?>
	<div class="clear"></div>
	</div>
	<div class="spacer"></div>
	<?php 
	
}
?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.reward_button').each(function(){
			$(this).parent('a').click(function(){
				conversation_id=$(this).attr('class');
				if($(this).find('span:first').hasClass('rewarded'))
				{
					return false;
				}
				else
				{
					$('.'+conversation_id).find('span:first').addClass('rewarded');
					$.ajax({
						url: 'rates/reward', 
						type: "POST",
						data: {
							conversation_id : conversation_id
						},
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
							{
						  		$('#reward_response').html(obj.message).fadeIn('fast');
						  		setTimeout('$("#reward_response").html("").fadeOut("fast");',2500);
							}
						  	else
						  	{
						  		$('.'+conversation_id).find('span:first').removeClass('rewarded');
						  		alert(obj.message);
						  	} 
						  	
					  	}
					})
				}
			});
		});
	})
	</script>
