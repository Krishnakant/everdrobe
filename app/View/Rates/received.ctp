<?php 
if($drobes)
{
	foreach($drobes as $drobe)
	{
	?>
	<div class="tp drobe_result">
		<?php if($drobe['Drobe']['new_response']>0){?><div class="badges"><?php echo $drobe['Drobe']['new_response']; ?></div><?php } ?>
         <?php echo $this->Html->image('/drobe_images/'.$drobe['Drobe']['file_name'],array('alt'=>$drobe['Drobe']['file_name'],'height'=>"85", 'width'=>"85")); ?>
         <div class="user_cont">
         	<div class="cont" style="font-size: larger;"><?php echo $drobe['Drobe']['comment']=="" ? "" : $drobe['Drobe']['comment']." <br />";?><em style="font-size: smaller;"><?php echo $categoryList[$drobe['Drobe']['category_id']]?></em></div>
            <div class="up"><?php echo $drobe['Drobe']['total_in'];?></div> 
            <div class="down"><?php echo $drobe['Drobe']['total_out'];?></div> 
            <div class="vote">Total Votes: <?php echo $drobe['Drobe']['total_rate']?></div> 
            <div class="delete close_rate" id=<?php echo $drobe['Drobe']['unique_id']; ?> ></div> 
         </div>
   		</div>
     <div class="clear"></div>
	<?php
	}
?>
<script type="text/javascript">
$(document).ready(function(){
	/*$('div.close_rate').click(function(){
		$('#close_drobe_id').html($(this).attr('id'));
		$.blockUI({
			message: $('#close_confirm'),
			css: {
				top:  ($(window).height()) /2 + 'px',
				left: ($(window).width()-300) /2 + 'px',
				width: '400px'
			}
		});
		return false;
	});*/

	$('div.close_rate').unbind('click').click(function() {
		parentObj=$(this).parent().parent();
		$('#close_drobe_id').html($(this).attr('id'));
		Confirm('Are you confirm to close this drob for rating?', function(yes) {
			if(yes)
			{
				if($('#close_drobe_id').html()!="")
				{
					$.ajax({
						url: 'drobes/close',
						type: "POST",
						data: {
							drobe_id: $('#close_drobe_id').html()
						},
						success: function(data) {
							$('.tabs ul li:first a').click();
						  	$('#close_drobe_id').html("");
						  	parentObj.remove();
						  	$("#got_feedback_area")[0].fleXcroll.updateScrollBars();
					  	}
					});
				}
				else
				{
					alert("Error occured in requesting close for rate");
					$("#close_no").click();
				}
			}
		});
	});
	$('.drobe_result > img').click(function(){
		drobe_id=$(this).parent().find('.close_rate').attr('id');
		document.location.href="drobes/result/"+drobe_id;
		return false;
	});

});
</script>
<?php  	
}
?>