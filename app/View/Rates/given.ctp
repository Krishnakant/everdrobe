<?php 
if($drobes)
{
	foreach($drobes as $drobe)
	{
	?>
	<div class="tp drobe_result given_result mt5" >
		<div class="media">
			<div class="media-left ">
				<?php if($drobe['Drobe']['new_response']>0){?><div class="badges"><?php echo $drobe['Drobe']['new_response']; ?></div><?php } ?>
         		<?php echo $this->Html->image('/drobe_images/'.$drobe['Drobe']['file_name'],array('alt'=>$drobe['Drobe']['file_name'],'height'=>"85", 'width'=>"85")).($drobe['Drobe']['is_highest_rated']>0 ? "<div class='highest-rated-drobe'></div>":""); ?>
         	</div>
         	<div class="media-body user_cont text-left">
         		<div class="">
         
         	 
		
         	<div class="cont">
	        <div class="user_info"><?php 
	        							if(isset($drobe['User']['username']) && $drobe['User']['username'] != null)
	        							{
	        								echo $this->Html->link($drobe['User']['username'],array("controller"=>"users","action"=>"profile",$drobe['User']['unique_id']));
	        							}
	        							else 
	        							{
	        								echo $this->Html->link($drobe['User']['first_name']." ".$drobe['User']['last_name'],array("controller"=>"users","action"=>"profile",$drobe['User']['unique_id']));
	        							}
	        							?>
	        <div class="drobe_title"><?php echo $drobe['Drobe']['comment']=="" ? "" : mb_strimwidth($drobe['Drobe']['comment'],0,30,"...")."";?></div>
	        </div>
         	
         	</div>
         	
			
            <div class="response_text <?php

            if($drobe['Rate']['rate']==0 )
            {
            	echo "very_small_Commented";
            }
            else
            {
            	//if($drobe['Rate']['rate']>0 )            	{
            		//echo "very_small_Cute";
            	//}            	else            	{
            		//echo "very_small_Hot";
            	//}            	
            }
            
//				echo $drobe['Rate']['rate']==0 ? "very_small_Skip" : $drobe['Rate']['rate']>0 ? "very_small_Cute" : "very_small_Hot";
			?>"><?php echo $drobe['Rate']['comment'];?></div>
		
        	<ul class="lst-unstyled list-inline v-top">
        		<li class=""><a href="javascript:void(0)" id="<?php echo $drobe['Rate']['conversation_id']?>">
			<span class="reward_button <?php echo $drobe['Rate']['rewarded']>0 ? "rewarded" : ""; ?>"></span>
			</a></li>
				<?php if($drobe['Rate']['rate']>0 ) { ?>
            		<li><div class="btn btn-green btn-xs">Cute</div></li>
            	<?php } else { ?>
            		<li><div class="btn btn-danger btn-xs">Hot</div></li>
            	<?php } ?>
                <li>
                <div class="conversation">
		<?php 
			if($drobe['Rate']['conversation_status']=="open" && $drobe['Rate']['conversation_id']!="") 
			{
				echo $this->Html->link("Reply",array("controller"=>"drobes","action"=>"conversation",$drobe['Drobe']['unique_id'],$drobe['Rate']['conversation_id']),array('class'=>"blue_btn btn btn-primary btn-xs mr15",'escape'=>false));
			}
			if($drobe["Rate"]["new_response"]>0)
			{
				
				?>
						<div class="badges"><?php echo $drobe['Rate']['new_response']; ?></div>
				<?php 
			}
			//else echo "closed";
		?>
		</div>
                </li>
                <li class="pull-right"><div class=" remove_feedback" id="<?php echo $drobe['Rate']['conversation_id']; ?>" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></div></li>
            </ul>    
			 
			 
            
            </div>
            </div>
         </div>
        	
   		</div>
     <div class="clear"></div>
     <script type="text/javascript">
     $(document).ready(function(){
    	 $('div.remove_feedback').unbind('click').click(function() {
    			parentObj=$(this).parent().parent();
    			$('#close_drobe_id').html($(this).attr('id'));
    			Confirm('You will not get back selected feedback after remove it. Are you confirm to remove selected feedback?', function(yes) {
    				if(yes)
    				{
    					if($('#close_drobe_id').html()!="")
    					{
    						$.ajax({
    							url: 'rates/remove_feedback',
    							type: "POST",
    							data: {
    								conversation_id: $('#close_drobe_id').html()
    							},
    							success: function(data) {
    								$('.tabs ul li:eq(1) a').click();
    							  	$('#close_drobe_id').html("");
    							  	parentObj.remove();
    							  	$("#gave_feedback_area")[0].fleXcroll.updateScrollBars();
    						  	}
    						});
    					}
    					else
    					{
    						alert("Error occured in requesting close for rate");
    						$("#close_no").click();
    					}
    				}
    			});
    			return false;
    		});
         });
     </script>
	
	<?php
	} 	
}
else
{
	if(!isset($this->params->named['page']) || $this->params->named['page']==1)
	{
		?>
		<div class="spacer"></div>
		<p class="empty_message">No feedback found</p>
		<?php
	} 
}
?>