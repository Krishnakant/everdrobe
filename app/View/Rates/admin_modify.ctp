<h2>Feedback: <?php echo $drobeData['Drobe']['comment'];?> </h2>

<div class="tp tp_big drobe_result">
		 <?php echo $this->Html->image('/drobe_images/'.$drobeData['Drobe']['file_name'],array('alt'=>$drobeData['Drobe']['file_name'],'height'=>"85", 'width'=>"85")); ?>
         <div class="user_cont">
         	<div class="cont"><?php echo $drobeData['Drobe']['comment']=="" ? "" : $drobeData['Drobe']['comment']." <br />";?><em><?php echo $categoryList[$drobeData['Drobe']['category_id']]?></em></div>
         	<div class="clear"></div>
            <div class="up"><?php echo $drobeData['Drobe']['total_in'];?></div> 
            <div class="down"><?php echo $drobeData['Drobe']['total_out'];?></div>
            <div class="whatever">Total Votes: <?php echo $drobeData['Drobe']['total_rate']?></div> 
         </div>
   		</div>
     <div class="clear"></div>


<div class="user_rates">
<div class="spacer"></div>
<?php 
$rate_types=array("1"=>"Cute","-1"=>"Hot","0"=>"Skip");
echo $this->Form->create("Rate");
echo $this->Form->input("rate",array("options"=>$rate_types));
echo $this->Form->input("comment",array("class"=>"comment"));
echo $this->Form->submit("Update");
echo $this->Form->hidden("id");
echo $this->Form->hidden("drobe_id");
echo $this->Form->hidden("user_id");
echo $this->Form->end();
?>
</div>