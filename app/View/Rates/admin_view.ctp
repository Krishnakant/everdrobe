<h2>Feedback: <?php echo $drobeData['Drobe']['comment'];?> </h2>
<div class="tp tp_big drobe_result">
		 <?php echo $this->Html->image('/drobe_images/'.$drobeData['Drobe']['file_name'],array('alt'=>$drobeData['Drobe']['file_name'],'height'=>"85", 'width'=>"85")); ?>
         <div class="user_cont">
         	<div class="cont"><?php echo $drobeData['Drobe']['comment']=="" ? "" : $drobeData['Drobe']['comment']." <br />";?><em><?php echo $categoryList[$drobeData['Drobe']['category_id']]?></em></div>
         	<div class="clear"></div>
            <div class="up"><?php echo $drobeData['Drobe']['total_in'];?></div> 
            <div class="down"><?php echo $drobeData['Drobe']['total_out'];?></div>
            <div class="whatever">Total Votes: <?php echo $drobeData['Drobe']['total_rate']?></div>
            <div class="actions right" style="top:-20px; position: relative;">
            <?php 
            	echo $this->Html->link("Update Vote Counter",array("controller"=>"rates","action"=>"update_counter",$drobeData['Drobe']['id'],"admin"=>true));
            ?>
            </div>
         </div>
         
   		</div>
     <div class="clear"></div>
     
<div class="user_rates">
<div class="spacer"></div>
<?php 
foreach($rates as $rate)
{
	if($rate['Rate']['rate'] == 1) $rate_type="Cute";
	else if($rate['Rate']['rate'] == -1) $rate_type="Hot";
	else $rate_type="Skip";
	?>
	<div class="user_rate">
	<div class="left">
	<div class="rate rate_<?php echo $rate_type; ?> "></div>
	<div class="profile_photo 123"><?php 
	if($rate['User']['photo']!="") {
		echo $this->Html->image('/'.str_replace(DS,"/",Configure::read('profile.thumb.upload_dir'))."/".$rate['User']['photo'],array('height'=>30));
	}
	else
	{
		echo $this->Html->image(Configure::read('profile.default_image'),array('height'=>30));
	}
	if($rate['User']['star_user']==1):?>
			<div class="star-user-small"></div>
	<?php endif;?></div>
	<div class="user_info"><?php echo $this->Html->link($rate['User']['first_name']." ".$rate['User']['last_name']." (".$rate['User']['username'].")",array('controller'=>"users","action"=>"view",$rate['User']['id'])); ?><br/>
	<small>
	<?php echo $rate['User']['gender'].($rate['User']['country']!=""? ", ".$rate['User']['country']: "").".";?>
	</small>
	</div>
	<?php 
	if($rate['Rate']['comment']!="")
	{
		?>
		<div class="clear"></div>
		<div class="comment"><?php echo $rate['Rate']['comment'];?></div>
	<?php 
	}
	?>
	</div>
	<div class="actions small_buttons right">
	<ul>
		<?php if($rate['Rate']['conversation_id']!="")
		{
			?>
			<li>
			<?php echo $this->Html->link("View Conversations",array("controller"=>"drobes","action"=>"conversation",$rate['Rate']['drobe_id'],$rate['Rate']['conversation_id'],"admin"=>true));?>
			</li>
			<?php 
		}
		?>
		<li><?php echo $this->Html->link("Modify Feedback",array("action"=>"modify","admin"=>true,$rate['Rate']['drobe_id'],$rate['Rate']['id']));?></li>
		<li><?php echo $this->Html->link("Remove Feedback",array("action"=>"remove","admin"=>true,$rate['Rate']['drobe_id'],$rate['Rate']['id']),array("confirm"=>"Are you sure to remove this Feedback?"));?></li>
	</ul>

	<div class="clear"></div>
	</div>
	<div class="spacer"></div>
	</div>
	<?php 
}
?>

</div>