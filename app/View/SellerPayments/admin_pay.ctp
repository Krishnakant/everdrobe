<div class="tp tp_big drobe_result">
<div class="seller-pay-profile-pic">
 <?php if(false && $sellerData['User']['photo']!="") 
 {
 	echo $this->Html->image('/profile_images/'.$sellerData['User']['photo'],array('alt'=>$sellerData['User']['photo'],'height'=>"85", 'width'=>"85")); 
 }
 else
 {
 	echo $this->Html->image('/images/default.jpg',array('alt'=>"No photo",'height'=>"85", 'width'=>"85"));
 }
 
 if($sellerData['User']['star_user']==1):?>
	<div class="star-user"></div>
 <?php endif;?>
 </div>
    <div class="user_cont">
    	<div class="cont" style="font-size: larger"><?php echo $sellerData['User']['first_name']." ".$sellerData['User']['last_name']. " (".$sellerData['User']['username'].")";?></div>
      	<div class="clear"></div>
      	<b>Total Earnings : </b><?php echo number_format((float)$earningsData['total_earnings'], 2, '.', '').' $';?> &nbsp;&nbsp;&nbsp;&nbsp; 
        <b>Total Paid : </b><?php echo number_format((float)$sellerData['SellProfile']['total_paid'], 2, '.', '').' $';?>
    </div>
</div>
<?php 
//pr($paymentHistory);

if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="drobe_sale_popup">
	<div class="half left">
	<?php 
	if(!$sellerData['SellProfile']['user_id']>0)
	{
		?>
		<h4 style="margin-bottom: 0">Can't do payment</h4>
		<div>You can not make payment to this seller because seller has not updated sell profile.</div>
		<?php 
	}
	else
	{
	?>
	
	<!-- If payment type is "cheque" then cheque information display to admin.  -->
	<?php  if($sellerData['SellProfile']['payment_type']=='cheque'):?>
	<h4 style="margin-bottom: 0">Full Name with Mailing Address</h4>
	<address style="background: #FAFAFA; border: 1px dotted #AAAAAA; padding: 5px;">
      	<b>"<?php echo $sellerData['SellProfile']['full_name'];?>"</b><br>
      	<?php //echo str_replace("\r","<br>",$sellerData['SellProfile']['address']);?>
      	<?php echo $sellerData['SellProfile']['street1'];?><br>
      	<?php echo (($sellerData['SellProfile']['street2']!="")?$sellerData['SellProfile']['street2']."<br>":"")?>
      	<?php echo $sellerData['SellProfile']['city'];?>,
		<?php echo $sellerData['SellProfile']['state'];?>,
		<?php echo $sellerData['SellProfile']['zip'];?><br>
    </address>
    <?php endif;?>
    
    <!-- If payment type is "paypal" then paypal information display to admin.  -->
    <?php  if($sellerData['SellProfile']['payment_type']=='paypal'):?>
    <h4 style="margin-bottom: 0">Paypal Mailing Address</h4>
	<address style="background: #FAFAFA; border: 1px dotted #AAAAAA; padding: 5px;">
      	<b>"<?php echo $sellerData['SellProfile']['paypal_email'];?>"</b><br>
    </address>
    <?php endif; ?>
    
    <!-- If payment type is "Bank Deposit" then Bank Deposit information display to admin.  -->
    <?php  if($sellerData['SellProfile']['payment_type']=='deposit'):?>
    <h4 style="margin-bottom: 0">Bank Deposit Address</h4>
	<address style="background: #FAFAFA; border: 1px dotted #AAAAAA; padding: 5px;">
      	<b>"<?php echo $sellerData['SellProfile']['deposit_name'];?>"</b><br>
      	Account No : <?php echo $sellerData['SellProfile']['account_no'];?><br>
      	Routing No : <?php echo $sellerData['SellProfile']['routing_no'];?><br>
      	Bank Name : <?php echo $sellerData['SellProfile']['bank_name'];?><br>
      	Account Type : <?php echo $sellerData['SellProfile']['account_type'];?><br>
    </address>
    <?php endif; ?>
    
    
	<h3>Pay to <?php  echo $sellerData['User']['first_name']." ".$sellerData['User']['last_name']. " (".$sellerData['User']['username'].")" ; ?></h3>
	<?php 
	echo $this->Form->create("SellerPayment");
	echo $this->Form->label('request_error', '<span id="request_error" style="color:red"></span>');
	echo $this->Form->input("request",array("label"=>"Request Number","type"=>"text",'value'=>$request_id));
	
	echo $this->Form->input("payment_by",array("label"=>"Payment By","type"=>"select",'options'=>array('cheque'=>'Cheque','paypal'=>'Paypal','deposit'=>'Deposit'),'value'=>$sellerData['SellProfile']['payment_type']));
	
	echo $this->Form->input("payment",array("label"=>"Payment Amount","type"=>"text",'value'=>$payment));
	echo $this->Form->input("remark",array("type"=>"textarea"));
	echo $this->Form->input("user_id",array("type"=>"hidden", "value"=>$sellerData['SellProfile']['user_id']));
	echo $this->Form->hidden("request_id",array("type"=>"text",'value'=>$request_id));
	
	?>
	<div class="spacer"></div>
	<p><b>Caution:</b> After once payment done you can not modify or remove record</p>
	<div class="spacer"></div>
	<?php 
	echo $this->Form->submit("Pay to Seller");
	echo $this->Form->end();
	}
	?>
	</div>
	<div class="half right drobe_description">
	<h3>Payment History</h3>
	<?php 
	if(count($paymentHistory))
	{
		?>
		<div class="flexcroll" style="min-height: 400px; border: 1px dotted #AAAAAA; padding: 5px;">
			<table>
				<tr>
					<th>Paid On</th>
					<th>Amount</th>
					<th>Remarks</th>
					<th>Payment By</th>
				</tr>
				<?php 
				foreach($paymentHistory as $history){
					?>
					<tr>
						<td><?php echo $this->Time->niceShort($history['SellerPayment']['created_on']);?></td>
						<td><?php echo $history['SellerPayment']['payment'];?> $</td>
						<td><?php echo $history['SellerPayment']['remark'];?></td>
						<td><?php echo $history['SellerPayment']['payment_by'];?></td>
					</tr>	
					<?php 
				}
				?>
			</table>
		</div>
		<?php 
	}
	else
	{
		?>
		<p>There is not payment done to <?php echo $sellerData['User']['first_name']." ".$sellerData['User']['last_name']. " (".$sellerData['User']['username'].")"?></p>
		<?php 
	}
	?>
	
	</div>
	
</div>
<script>

$(document).ready(function()
{
	$("#SellerPaymentRequest").blur(function()
	{
		var user_id = $("#SellerPaymentUserId").val();
		var request_id = $("#SellerPaymentRequest").val();
		
		$.ajax({
			url: '<?php echo Router::url(array('controller'=>'SellerPayments','action'=>'get_amount'));?>'+"/"+user_id+"/"+request_id,
			success:function(result)
			{
				if(result!="")
				{
					$("#SellerPaymentPayment").val(result);	
					$("#request_error").html("");
				}
				else
				{
					$("#request_error").html("Request number not found.");
					$("#SellerPaymentPayment").val("");
				}
				if(request_id=="")
				{
					$("#request_error").html("");
				}
				return;
			},
			error:function(result)
			{
				$("#request_error").html("Request Number not found.");
			}
			
		});
	});
});

</script>