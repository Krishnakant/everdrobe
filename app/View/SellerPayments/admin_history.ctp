	<?php 
	
	if(count($payment_history))
	{
		?>
		<div>
			<table>
				<tr>
					<th>Paid On</th>
					<th>Amount</th>
					<th>Remarks</th>
					<th>Payment By</th>
				</tr>
				<?php 
				foreach($payment_history as $history){
					?>
					<tr>
						<td><?php echo $this->Time->niceShort($history['created_on']);?></td>
						<td><?php echo $history['payment'];?> $</td>
						<td><?php echo $history['remark'];?></td>
						<td><?php echo $history['payment_by'];?></td>
					</tr>	
					<?php 
				}
				?>
			</table>
		</div>
		<?php 
	}
	else
	{
		?>
		<p>There is not payment history.</p>
		<?php 
	}
	?>
	
	