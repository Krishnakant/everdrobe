<link rel="stylesheet" type="text/css" href="http://craigsworks.com/projects/qtip2/packages/latest/jquery.qtip.min.css" />
<style type="text/css">
/* Add some nice box-shadow-ness to the modal tooltip */
#qtip-modal{
	max-width: 420px;
	-moz-box-shadow: 0 0 10px 1px rgba(0,0,0,.5);
	-webkit-box-shadow: 0 0 10px 1px rgba(0,0,0,.5);
	box-shadow: 0 0 10px 1px rgba(0,0,0,.5);
}

#qtip-modal .qtip-content{
	padding: 10px;
}
</style>
<div class="right actions">
<?php 
echo $this->Html->link('View Seller List', array("controller"=>"sell_drobes","action"=>"sellers","admin"=>true));
?>
</div>
<h2>Seller Payment Tracking</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search Drobe : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search sellers' by first name, last name or full name" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($paymentData)>0)
{
	?>
	<table>
		<tr>
			<th>Paid On</th>
			<th>Amount</th>
			<th>Seller Name</th>
			<th>Seller User Name</th>
			<th>Remarks</th>
		</tr>
		<?php 
		foreach($paymentData as $payment)
		{
			?>
			<tr>
				<td><?php echo $this->Time->niceShort($payment['SellerPayment']['created_on']); ?></td>
				<td><?php echo $payment['SellerPayment']['payment'];?> $</td>
				<td><?php echo $payment['User']['first_name']." ".$payment['User']['last_name']; ?></td>
				<td><?php echo $payment['User']['username'];?></td>
				<td><?php echo $payment['SellerPayment']['remark'];?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="featured_drobe" style="display: none;" class="featured_form">
	<h2>Mark as Featured</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Drobe: </strong><span id="featured_drobe_name"></span></div>
		
		<?php 
			echo $this->Form->input('buy_url',array('id'=>'buy_url','label'=>false,'div'=>false,'placeholder'=>"Enter URL for buy drobe"));
			echo $this->Form->hidden("featured_drobe_id",array("id"=>"featured_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_featured"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_featured"));
?>
<div class="spacer"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	});
});
</script>