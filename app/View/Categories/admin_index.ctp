<h2>Categories List</h2>
<div class="actions right">
<?php 
echo $this->Html->link('Reset order Alphabaticaly',array('controller'=>'categories','action'=>'position','reset','admin'=>true),array("class"=>"button"))."&nbsp;&nbsp;&nbsp;";
echo $this->Html->link('Add New Category',array('controller'=>'categories','action'=>'add','admin'=>true),array("class"=>"button"))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th width="150px"><?php echo $this->Paginator->sort('Category.category_name','Category Name');?></th>
			<th><?php echo $this->Paginator->sort('Category.order','Order');?></th>
			<th>Drobes</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($data as $category)
		{
			/** Other category not display for edit or delete*/
				
			?>
			<tr>
				<td><?php echo $category['Category']['id'];?></td>
				<td><?php echo $category['Category']['category_name'];?></td>
				<td><?php echo $category['Category']['order'];?></td>
				<td><?php echo $category[0]['total_drobes'];?></td>
				<td><?php echo $category['Category']['status'];?></td>
				<td>
				<div class="actions">
					<?php 
							echo $this->Html->link('Edit',array('controller'=>'categories','action'=>'edit',$category['Category']['id'],'admin'=>true)). " "; 
							if($category['Category']['id']==configure::read('other_category_id'))
							{
								echo $this->Html->link('Delete','javascript:void(0);',array('style'=>'opacity:0.5'))."&nbsp;&nbsp;&nbsp;";
							}
							else 
							{
								echo $this->Html->link('Delete',array('controller'=>'categories','action'=>'delete',$category['Category']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'))."&nbsp;&nbsp;&nbsp;";
							}
							echo $this->Html->link('Up',array("controller"=>"categories","action"=>"position","up","admin"=>true,$category['Category']['id']))." ";
							echo $this->Html->link('Down',array("controller"=>"categories","action"=>"position","down","admin"=>true,$category['Category']['id']))." ";
							echo $this->Html->link('First',array("controller"=>"categories","action"=>"position","first","admin"=>true,$category['Category']['id']))." ";
							echo $this->Html->link('Last',array("controller"=>"categories","action"=>"position","last","admin"=>true,$category['Category']['id']));
							
					?>
				</div>
				</td>
			</tr>
			<?php
			
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
