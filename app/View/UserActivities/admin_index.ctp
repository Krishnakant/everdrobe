<h2>User Activity</h2>
<div class="actions right">
<?php 
echo $this->Html->link('Back User To List',array('controller'=>'users','action'=>'index','admin'=>true),array("class"=>"button"))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th>Date</th>
			<th>Activity</th>
			<th>User</th>
			<th>User Name</th>>
			<th>Ip Address</th>
			<th>Device</th>
			
		</tr>
		<?php 
		foreach($data as $activity)
		{
			?>
			<tr>
				<td><?php echo $activity['UserActivity']['id'];?></td>
				<td><?php echo date('d-M-Y H:i A',strtotime($activity['UserActivity']['created_on']));?></td>
				<td><div class="comment" style="width:250px"><?php echo str_replace(array("{",'","',"}"),array("<br/>{</br/>",'","<br/>',"<br/>}<br/>"),$activity['UserActivity']['activity']);?></div></td>
				<td><?php echo $activity['User']['first_name']." ".$activity['User']['last_name'];?></td>
				<td><?php echo $activity['User']['username']?></td>
				<td><?php echo $this->Html->link($activity['UserActivity']['ip_address'],"http://ip-api.com/".$activity['UserActivity']['ip_address'],array("target"=>"_blank"));?></td>
				<td><?php echo $activity['UserActivity']['device_type'];?></td>
				<td>
				
			</tr>
			<?php
			
		}
		?>
	</table>
	<?php 
} 
else
{
	echo "Activities data not found";
}
		
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
