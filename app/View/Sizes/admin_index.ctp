<h2>Size List</h2>
<div class="actions right">
<?php 
echo $this->Html->link('Reset Order',array('controller'=>'sizes','action'=>'position','reset','admin'=>true),array("class"=>"button"))."&nbsp;&nbsp;&nbsp;";
echo $this->Html->link('Add New Size',array('controller'=>'sizes','action'=>'add','admin'=>true),array("class"=>"button"))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th><?php echo $this->Paginator->sort('Size.order','Size Name');?></th>
			<th><?php echo $this->Paginator->sort('Size.order','Order');?></th>
			<th>Status</th>
			<th>Drobes</th>
			<th>Actions</th>
		</tr>
		<?php
		foreach($data as $size)
		{
			?>
			<tr>
				<td><?php echo $size['Size']['id'];?></td>
				<td><?php echo $size['Size']['size_name'];?></td>
				<td><?php echo $size['Size']['order'];?></td>
				<td><?php echo $size['Size']['status'];?></td>
				<td><?php echo $size[0]['drobes'];?></td>
				<td>
				<div class="actions">
					<?php 
						echo $this->Html->link('Edit',array('controller'=>'sizes','action'=>'edit',$size['Size']['id'],'admin'=>true)). " ";
						if($size['Size']['id']==configure::read('other_size_id'))
						{
							echo $this->Html->link('Delete','javascript:void(0);',array('style'=>'opacity:0.5'))."&nbsp;&nbsp;&nbsp;";
						}
						else
						{
							echo $this->Html->link('Delete',array('controller'=>'sizes','action'=>'delete',$size['Size']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'))."&nbsp;&nbsp;&nbsp;";
						} 
						//echo $this->Html->link('Delete',array('controller'=>'sizes','action'=>'delete',$size['Size']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'))."&nbsp;&nbsp;&nbsp;";
						echo $this->Html->link('Up',array("controller"=>"sizes","action"=>"position","up","admin"=>true,$size['Size']['id']))." ";
						echo $this->Html->link('Down',array("controller"=>"sizes","action"=>"position","down","admin"=>true,$size['Size']['id']))." ";
						echo $this->Html->link('First',array("controller"=>"sizes","action"=>"position","first","admin"=>true,$size['Size']['id']))." ";
						echo $this->Html->link('Last',array("controller"=>"sizes","action"=>"position","last","admin"=>true,$size['Size']['id']));
					?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
