<?php 
foreach($conversations as $conv)
{
	
	$is_me= $conv['Conversation']['user_id']==$this->Session->read('Auth.User.id');
	
	$link=array('controller'=>"users",'action'=>"profile");
	if(!$is_me){
		$user=$conv['User'];
		$link[]=$conv['User']['unique_id'];
	}
	else
	{
		$user=$this->Session->read('Auth.User');
	}
	
	if(trim($user['photo'])!="" && is_file(WWW_ROOT.Configure::read('profile.thumb.upload_dir').DS.$user['photo'])) {
		$image= $this->Html->image('/'.str_replace(DS,"/",Configure::read('profile.thumb.upload_dir'))."/".$user['photo'],array('border'=>0,'height'=>30,'width'=>30));
	}
	else
	{
		$image= $this->Html->image('/images/default.jpg',array('border'=>0, 'height'=>30,'width'=>30));
	}
	
	?>
	<div class="chat_main">
	<div class="time <?php if($is_me) echo "chat_right"?>"><?php echo $this->Time->timeAgoInWords($conv['Conversation']['created_on']);?></div>
	<div class="clear"></div>
	<?php 
	if($is_me)
	{
	?>
	<div class="chat_icon"><?php echo $this->Html->link($image,$link,array('escape'=>false));?></div>
	<div class="chat_txt">
	<?php echo $conv['Conversation']['conversation']; ?>
	</div>
	<?php 
	}
	else
	{
	?>
	<div class="chat_txt1">
	<?php echo $conv['Conversation']['conversation']; ?>
	</div>
	<div class="chat_icon"><?php echo $this->Html->link($image,$link,array('escape'=>false));?></div>
	<?php 	
	}
	?>
	</div>
	<div class="spacer"></div>
	<?php 
}
?>