<?php 
foreach($conversations as $conv)
{
	
	$is_me= $conv['Conversation']['user_id']==$uploader_id;
	
	$link=array('controller'=>"users",'action'=>"view","admin"=>true,$conv['Conversation']['user_id']);
	
	if(trim($conv['User']['photo'])!="" && is_file(WWW_ROOT.Configure::read('profile.thumb.upload_dir').DS.$conv['User']['photo'])) {
		$image= $this->Html->image('/'.str_replace(DS,"/",Configure::read('profile.thumb.upload_dir'))."/".$conv['User']['photo'],array('border'=>0,'height'=>30,'width'=>30));
	}
	else
	{
		$image= $this->Html->image('/images/default.jpg',array('border'=>0, 'height'=>30,'width'=>30));
	}
	if($conv['User']['star_user']==1):
		$image.='<div class="star-user-small"></div>';
	endif;?>
	<div class="chat_main">
	<div class="time <?php if($is_me) echo "chat_right"?>"><?php echo $this->Time->timeAgoInWords($conv['Conversation']['created_on']);?> ago by <?php echo $conv['User']['first_name']." ".$conv['User']['last_name']." (".$conv['User']['username'].")";?></div>
	<div class="clear"></div>
	<?php 
	if($is_me)
	{
	?>
	<div class="chat_icon"><?php echo $this->Html->link($image,$link,array('escape'=>false));?></div>
	<div class="chat_txt">
	<?php echo $conv['Conversation']['conversation']; ?>
	</div>
	<?php 
	}
	else
	{
	?>
	<div class="chat_txt1">
	<?php echo $conv['Conversation']['conversation']; ?>
	</div>
	<div class="chat_icon"><?php echo $this->Html->link($image,$link,array('escape'=>false));?></div>
	<?php 	
	}
	?>
	<div class="actions" style="float: right;">
	<?php echo $this->Html->link("Edit",array("controller"=>"conversations","action"=>"edit","admin"=>true,$conv['Conversation']['unique_id'],$conv['Conversation']['id']))?>
	<?php echo $this->Html->link("Remove",array("controller"=>"conversations","action"=>"remove","admin"=>true,$conv['Conversation']['unique_id'],$conv['Conversation']['id']),array("confirm"=>"Are you sure to remove this message?"))?>
	</div>
	
	</div>
	<div class="spacer"></div>
	<?php 
}
?>