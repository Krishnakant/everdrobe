<h2>Conversation Edit</h2>

<?php 
echo $this->Form->create("Conversation");
echo $this->Form->label("Conversation Message");
echo $this->Form->textarea('conversation',array("placeholder"=>"Enter Conversation message Here"));
echo $this->Form->input('user_id',array("label"=>"Conversation done by",'options'=>$conversation_users,"empty"=>"Select Conversations by"));
echo $this->Form->hidden('unique_id');
echo $this->Form->hidden('id');
echo $this->Form->submit("Save Changes");
echo $this->Form->end();
?>