<h2>Everdrobe Users</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Seach User : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Enter Email, First Name or Last Name" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<style>
table tr td.email
{
    word-break: break-all;
}
</style>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('User.id','#');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name','Full Name');?></th>
			<th><?php echo $this->Paginator->sort('User.username','User Name')?></th>
			<th style="width: 250px"><?php echo $this->Paginator->sort('User.email','Email');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.current_ebx_point','EBX');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.earned_ebx_point','Total EBX');?></th>
			<th><?php echo $this->Paginator->sort('User.status','Status');?></th>
			<th><?php echo $this->Paginator->sort('User.created_on','Registered On');?></th>
			<th>Action</th>
		</tr>
		<?php 
		foreach($data as $user)
		{
			?>
			<tr class="<?php echo $user['User']['id']; ?>">
				<td><?php echo $user['User']['id'];?></td>
				<td><?php echo $user['User']['first_name'].' '.$user['User']['last_name'] ;?></td>
				<td><?php echo $user['User']['username']?></td>
				<td class="email"><?php echo $user['User']['email'];?></td>
				<td><?php echo $user['UserTotal']['current_ebx_point'];?></td>
				<td><?php echo $user['UserTotal']['earned_ebx_point'];?></td>
				<td><?php echo $user['User']['status'];?></td>
				<td><?php echo $this->Time->timeAgoInWords($user['User']['created_on']);?></td>
				<td>
				<div class="actions">
					<a class="user_actions">Actions</a>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">

	<?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="user_options" style="display: none;">
	<ul class="submenu actions">
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"view","admin"=>true)); ?>  class="view_detail_link">View Detail</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"edit","admin"=>true)); ?>  class="remove_link">Edit Detail</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","block","admin"=>true)); ?> onclick="return confirm('Are you sure to block this user?');" class="block_link">Block</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","activate","admin"=>true)); ?> onclick="return confirm('Are you sure to activate this user?');" class="activate_link">Activate</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","deactivate","admin"=>true)); ?> onclick="return confirm('Are you sure to deactivate this user?');" class="deactivate_link">Deactivate</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"ebx_transactions","action"=>"user","admin"=>true)); ?> class="ebx_transaction_link">Ebx Transaction</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"user_activities","action"=>"index","admin"=>true)); ?> class="activity_link">View Activity</a></li>
		
	</ul>
	<div class="clear"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	});
	$('.user_actions').each(function(){
		$(this).click(function(){

			user_id=parseInt($(this).parent().parent().parent().find('td:first').html());
			unique_id=$(this).parent().parent().parent().attr('class');
			user_name=$.trim($(this).parent().parent().parent().find('td:eq(1)').html());
			
			$('#user_options a.view_detail_link').attr("href",$('#user_options a.view_detail_link').attr('rel')+"/"+user_id);
			$('#user_options a.remove_link').attr("href",$('#user_options a.remove_link').attr('rel')+"/"+user_id);
			$('#user_options a.block_link').attr("href",$('#user_options a.block_link').attr('rel')+"/"+user_id);
			$('#user_options a.activate_link').attr("href",$('#user_options a.activate_link').attr('rel')+"/"+user_id);
			$('#user_options a.deactivate_link').attr("href",$('#user_options a.deactivate_link').attr('rel')+"/"+user_id);
			$('#user_options a.ebx_transaction_link').attr("href",$('#user_options a.ebx_transaction_link').attr('rel')+"/"+user_id);
			$('#user_options a.activity_link').attr("href",$('#user_options a.activity_link').attr('rel')+"/"+user_id);
			
			$(this).qtip({
				position : { my: 'bottom left', at: 'bottom left' } ,
				content: {
					text: $('#user_options').html(), // Use the submenu as the qTip content
				},
				show: {
					event: 'click',
					ready: true // Make sure it shows on first mouseover
	 			},
				hide: {
					delay: 100,
					event: 'unfocus mouseleave',
					fixed: true // Make sure we can interact with the qTip by setting it as fixed
				},
	 			style: {
					classes: 'ui-tooltip-nav', // Basic styles
					tip: false // We don't want a tip... it's a menu duh!
				},
			});
		});
		
	});
	
});
</script>