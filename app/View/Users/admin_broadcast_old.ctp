<h2>Send Broadcast Message</h2>
<?php 
echo $this->Form->create("User");
echo $this->Form->input("send_to",array("options"=>array("all"=>"All Everdrobe Users","selected"=>"Selected Everdrobe User","active"=>"Active Everdrobe User","inactive"=>"Inactive Everdrobe User","suspended"=>"Suspended/Blocked Everdrobe User"),"default"=>"all","id"=>"send_to_type"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("users",array("label"=>"Select Users","options"=>$userlist,"id"=>"user_list","multiple"=>"multiple",'class'=>"select_field","empty"=>"Select User from List"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("message",array("type"=>"textarea"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("message_type",array("label"=>"Select Message Type","options"=>array("general"=>"Common notification message","drobe"=>"Drobe specific message"),"id"=>"message_type"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("drobe_id",array("label"=>"Select Drobe","options"=>$drobelist,"id"=>"drobe_list","empty"=>"Select Drobe from drobe list"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->input("display_in",array("label"=>"Display Drobe type","options"=>array("search"=>"Open search tab on notification","featured"=>"Open feratured tab on notification"),"id"=>"display_type"));
echo $this->Form->end("Submit");
?>

<script type="text/javascript">
$('document').ready(function(){

	$('#user_list').wrap('<div class="select_wrap"  multiple="multiple"></div>');
	$(".select_field").custSelectBox({
		isscrolling:true,
		selectwidth:"250px"
	});
	
	if($('#send_to_type option:selected').val()!="selected")
	{
		$('.select_wrap').parent().hide();
	}
	else $('.select_wrap').parent().show();
	$('#send_to_type').change(function(){
		if($(this).val()!="selected")
		{
			$('.select_wrap').parent().hide();
		}
		else $('.select_wrap').parent().show();
	});

	if($('#message_type option:selected').val()!="drobe")
	{
		$('#drobe_list').parent().hide();
		$('#display_type').parent().hide();
	}
	else 
	{
		$('#drobe_list').parent().show();
		$('#display_type').parent().show();
	}
	$('#message_type').change(function(){
		if($(this).val()=="drobe")
		{
			$('#drobe_list').parent().show();
			$('#display_type').parent().show();
		}
		else
		{
			$('#drobe_list').parent().hide();
			$('#display_type').parent().hide();
		}
	});
	
});


</script>