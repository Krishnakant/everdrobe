<?php
echo $this->Form->create('User',array("id"=>"edit_profile"));
?>
<div class="">
   	<h3 class="text-center">Edit Profile: <?php echo ((isset($this->data['User']['username']) && $this->data['User']['username'] != null) ? $this->data['User']['username'] : $this->data['User']['first_name']. " " .$this->data['User']['last_name']); ?></h3>
</div>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="">
    	<div class="text-center col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
    	<div id="file_uploader">
<?php 
if($this->data['User']['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$this->data['User']['photo']))
{
	echo $this->Html->image('/profile_images/thumb/'.$this->data['User']['photo'],array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
else
{
	echo $this->Html->image('/images/default_big.jpg',array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
if(isset($this->data['User']['star_user']) && $this->data['User']['star_user']==1):?>
	<div class="star-user"></div>
<?php endif;?>
<div style="position: relative">
<?php 
echo $this->Html->link('Change profile picture','javascript:void(0)',array('id'=>'change_picture','class'=>'btn btn-success'));
?>
</div>
</div>
<?php 
//echo $this->Form->file('file',array('id'=>'file_upload'));
echo $this->Form->hidden('photo');
?>
<div class="clearfix"></div>

<ul class="list-unstyled list-inline">
<?php  //echo $this->Html->link('Change Email',array('controller'=>"users",'action'=>"change_email"),array("class"=>"blue_btn")); ?>
<li><?php  echo $this->Html->link('Change Password',array('controller'=>"users",'action'=>"change_password"),array("class"=>"btn btn-danger")); ?></li>
<li><?php  echo $this->Html->link('Change Settings',array('controller'=>"users",'action'=>"settings"),array("class"=>"btn btn-primary")); ?></li>
</ul>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
<?php
echo $this->Form->input("username",array("placeholder"=>"Your User Name","readonly"=>"readonly","class"=>"form-control "));
echo $this->Form->input("first_name",array("placeholder"=>"Your First Name","class"=>"form-control"));
echo $this->Form->input("last_name",array("placeholder"=>"Your Last Name","class"=>"form-control"));
?>
<div class="form-group">
<?php
echo $this->Form->label("Edit Your Bio");
?>
<div class="clear"></div>
<?php 
echo $this->Form->textarea('bio',array("class"=>"form-control"));
?>
</div>
<div class="form-group">
<?php
echo $this->Form->label("Edit Gender");
?>
<?php 
echo $this->Form->input('gender', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Gender", 'options' => array('male'=>'Male','female'=> 'Female'),'type' => 'radio'));
$year="";
$month="";
$day="";
if(isset($this->data['User']['birth_date']) && is_int(strtotime($this->data['User']['birth_date'])))
{
	list($year,$month,$day)=explode("-",$this->data['User']['birth_date']);
}
else 
{
	$year=isset($this->data['User']['birth_year'])?$this->data['User']['birth_year']:"";
	$month=isset($this->data['User']['birth_month'])?$this->data['User']['birth_month']:"";
	$day=isset($this->data['User']['birth_day'])?$this->data['User']['birth_day']:"";
}
?>
</div>
<?php
echo $this->Form->label("Edit Your Birthdate");
?>
<div class="clear"></div>
	<div class="input-group">
      <div class="input-group-addon" style="padding:0;"><?php echo $this->Form->input('birth_month',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"MM", "value"=>$month,"class"=>"form-control")); ?></div>
      <div class="input-group-addon" style="padding:0;"><?php echo $this->Form->input('birth_day',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"DD", "value"=>$day,"class"=>"form-control")); ?></div>
      <div class="input-group-addon" style="padding:0;"><?php echo $this->Form->input('birth_year',array("label"=>false,"div"=>false, "class"=>"text_box small","placeholder"=>"YYYY", "value"=>$year,"class"=>"form-control"));
?></div>
    </div>
    



<div class="spacer"></div>
<?php
echo $this->Form->label("Edit Country");
?>
<div class="clear"></div>
<?php
echo $this->Form->input('country',array('label'=>false,'options'=>$countryList,'empty'=>'Select Country',"class"=>"form-control"));
?>
<?php
echo $this->Form->label("Edit State/Province");
?>
<div class="clear"></div>

<?php 
echo $this->Form->input('province',array('label'=>false,'options'=>array(),'empty'=>'Select State/Province',"class"=>"form-control"));
?>

<div class="spacer"></div>
<div class="col-sm-12 text-center">
<?php
/*
echo $this->Form->label("Website");
?>
<div class="clear"></div>

<?php 
echo $this->Form->input('website',array('label'=>false,'style'=>"width:200px", "placeholder"=>"Enter your website link"));
?>

<div class="spacer"></div>
<?php
*/ 
echo $this->Form->hidden('id');
echo $this->Form->submit('Update Profile',array("class"=>"btn btn-success"));
?>
</div>
</div>
</div>
<?php 
echo $this->Form->end();
?>
<script type="text/javascript">
$(document).ready(function(){
	activate_menu("profile");
	var uploader = new qq.FileUploader({
			uploadButtonText: "Change your Photo",
		    element: document.getElementById('change_picture'),
		    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'profile')); ?>',
		    params: {type:'drobes'},
		    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
		    debug: true,
		    onSubmit: function(id, fileName){
		    	 $('#edit_profile input[type="submit"]').attr('disabled',true);
		    	$('#file_uploader').block({ 
					message: "<h5>Uploading</h5>",
			    	css: { 
			            border: 'none', 
			            padding: '3px', 
			            fontSize:'10px',
			            backgroundColor: '#000', 
			            '-webkit-border-radius': '5px !important', 
			            '-moz-border-radius': '5px !important', 
			            opacity: .5, 
			            color: '#fff' 
		        } });
		        
			    },
		    onProgress: function(id, fileName, loaded, total){
		    	 	
			},
		    onComplete: function(id, fileName, responseJSON){
			   upload_response(responseJSON);
			},
			onCancel: function(id, fileName){},
			messages: {
				typeError: "You can upload only JPEG, PNG or GIF Image file.",
	            sizeError: "You can upload less then 2MB file size.",
	            emptyError: "Uploaded file is empty.",
			 },
			 showMessage: function(message){ 
				 tip_message('#file_uploader',message);
			}
		});
		
	$('#file_upload_form').submit(function() { 
		$(this).ajaxSubmit({
					success :  function(responseText, statusText, xhr, $form)  { 
					    upload_response($.parseJSON(responseText));
					},  // post-submit callback 
				  	resetForm : true,
				  	clearForm : true,
			    	type: 'post'
				}); 
		return false; 
	}); 
	$('#change_picture').click(function(){
		$("#file_profile").show().click().hide().end();
	});
	$("#file_profile").change(function(){
		if($(this).val()!="")
		{
			$('#edit_profile input[type="submit"]').attr('disabled',true);
			//$("#file_upload_form").submit();
			alert("Submitting form");
			$("#file_upload_form").ajaxSubmit({
				success :  function(responseText, statusText, xhr, $form)  { 
				    var obj=jQuery.parseJSON(responseText);
			        
				},  // post-submit callback 
			  	resetForm : true,
			  	clearForm : true,
		    	type: 'post'
			}); 
		return false;
		}
	});
	$('#UserCountry option').each(function(){
		if($(this).attr('value')!="")
		{
			$(this).attr('id',$(this).attr('value'));
			//$(this).attr('value',$(this).html());
		}
	});
	$('#UserCountry').change(function(){
		$('#UserProvince').load("<?php echo $this->Html->url(array('controller'=>"countries",'action'=>'get_state_list')); ?>/"+$(this).find('option:selected').attr('id'));
	})
	<?php 
	if(isset($this->data))
	{
		$country_name=$this->data['User']['country'];
		$province_name=$this->data['User']['province'];
	}
	else
	{
		$country_name=$userData['User']['country'];
		$province_name=$userData['User']['province'];
	}
	?>
			$('#UserCountry').val('<?php echo $country_name;?>');
			var option_id=$('#UserCountry').find('option:selected').attr('id');
			$('#UserProvince').load("<?php echo $this->Html->url(array('controller'=>"countries",'action'=>'get_state_list')); ?>/"+option_id,function(){
				$('#UserProvince').val('<?php echo $province_name;?>');
			});
});

function upload_response(obj)
{
	$('#file_uploader').unblock();
	if(obj.type=="success")
    {
        $('#UserPhoto').val(obj.file);
        $('#profile_image').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('profile.thumb.upload_dir')).'/')?>" + obj.file);
    }
    else
    {  
    	alert(obj.message);
    } 
    $('#edit_profile input[type="submit"]').attr('disabled',false);
}
</script>
