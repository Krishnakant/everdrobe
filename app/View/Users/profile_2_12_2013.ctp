<h2><?php echo $this->data['User']['username'];?></h2>

    <div class="page_8_middel">
    	<div class="page_8user">
    	
<?php 

if($this->data['User']['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$this->data['User']['photo']))
{
	echo $this->Html->image('/profile_images/thumb/'.$this->data['User']['photo'],array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
else
{
	echo $this->Html->image('/images/default_big.jpg',array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}

if($this->data['User']['star_user']==1 ):?>
	<div class="star-user"></div>
<?php endif;?>

</div>

<div id="flag_block" class="flag_form">
<h2>Flag user</h2>
<div class="spacer"></div>
<?php 

echo $this->Form->input('flag_category',array('options'=>Cache::read('flag_category'),'id'=>'flag_category','class'=>'form-control','label'=>false,'div'=>false,'empty'=>"Select Reason"));
?>
<div class="spacer"></div>
<?php 
echo $this->Form->input('flagged_user_id',array('type'=>'hidden','id'=>'flagged_user_id','label'=>false,'div'=>false,'value'=>$this->data['User']['id']));
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_flag",'class'=>'btn btn-primary'));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_flag",'class'=>'btn btn-danger'));
?>
<div class="spacer"></div>
</div>

<div class="flag_user" >
<?php
/** User cannot flag itself if profile for user itself then it cannot display flag for user profile. **/ 
if(!$is_me)
{
?>

<b>Flag User : </b> 
	<a href="javascript:void(0)" id="flag_button"><?php echo $this->Html->image('/images/flag.png',array("height"=>18));?></a>
	

<?php 
}
?>
	
</div>

<div class="profile_detail"><?php 
echo $this->data['User']['username']."<br/>";
echo "<small>";
if(!empty($userData['gender'])) echo Inflector::humanize($userData['gender']);
//if(!empty($userData['relationship_status'])) echo Inflector::humanize($userData['relationship_status']).", <br/>";
if(!empty($userData['country'])) echo ", ".Inflector::humanize($userData['country']).". ";
echo "</small>";
//if($userData['website']!="") echo "<br><br>".$this->Html->link("Visit My Website",$userData['website'],array("target"=>"_blank","class"=>"page_8_sky_b","style"=>"margin:0;padding:0;"));
?></div>
        <div class="page_user_right">
        	<div class="page_8_white_box" id="fav_btn"><span>Fav'ed</span> : <?php echo $this->Html->convertToReadeable($user_totals['total_favourites']);?></div>
        	<div class="page_8_gry_box" id="drobe_btn"><span>Drobes</span> : <?php echo $this->Html->convertToReadeable($user_totals['total_drobes']); ?></div>
            <div class="page_8_white_box" id="following_btn"><span>Following</span> : <?php echo $this->Html->convertToReadeable($user_totals['followings']); ?></div>
            <div class="page_8_gry_box" id="follower_btn"><span>Follower</span> : <?php echo $this->Html->convertToReadeable($user_totals['followers']); ?></div>
            <?php 
            if($is_me)
            {
            	echo $this->Html->link('Edit Profile',array('controller'=>"users",'action'=>"edit_profile"),array("class"=>"page_8_sky_b"));
            	?>
            	<div class="clear" style="height: 2px;"></div>
            	<?php 
            	echo $this->Html->link('Find Friends',array('controller'=>"users",'action'=>"search"),array("class"=>"page_8_sky_b green_btn"));
           	} 
            else
            {
            	if($is_followed==1)
            	{ 
            		?>
            		<div id="follow_button" class="page_8_gry_box green_btn white_minus" rel="<?php echo $this->data['User']['unique_id'];?>" >Following</div>
            		<?php 
            	}
            	else
            	{
            		?>
            		<div id="follow_button" class=" page_8_gry_box red_btn white_plus" rel="<?php echo $this->data['User']['unique_id'];?>" >Follow</div>
            		<?php 
            	} 
            }?>
        </div>
    </div>
    <div class="page_8_middel">
    	<div class="bio_data_tax flexcroll"><?php echo str_replace(array("\n"),"<br>",$this->data['User']['bio']); ?></div>
        <div class="bio_right_table">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="2" class="white_td">
                <?php
	                if($is_me) echo "My Totals";
	                else echo $this->data['User']['first_name']." ".$this->data['User']['last_name']." Totals";
                ?>
                </td>
              </tr>
              <tr>
                <td class="gry_td"> Cute :</td>
                <td class="gry_td"><?php echo $user_totals['total_in'];?></td>
              </tr>
              <tr>
                <td class="white_td">Hot :</td>
                <td class="white_td"><?php echo $user_totals['total_out']?></td>
              </tr>
              <tr>
                <td class="gry_td">Votes Received : </td>
                <td class="gry_td"><?php echo $this->Html->convertToReadeable($user_totals['vote_received']);?></td>
              </tr>
               <tr>
                <td class="white_td">Votes Given : </td>
                <td class="white_td"><?php echo $this->Html->convertToReadeable($user_totals['vote_given']);?></td>
              </tr>
              <tr>
                <td class="gry_td">Available Everbucks:</td>
                <td class="gry_td"><?php echo $this->Html->convertToReadeable($user_totals['current_ebx_point']);?></td>
              </tr>
              <tr>
                <td class="white_td">Total Everbucks : </td>
                <td class="white_td"><?php echo $this->Html->convertToReadeable($user_totals['earned_ebx_point']);?></td>
              </tr>
              <tr>
                <td class="gry_td">Total Comments : </td>
                <td class="gry_td"><?php echo $this->Html->convertToReadeable($user_totals['feedback_given']);?></td>
              </tr>
              <?php 
              /*
               ?>
              <tr>
                <td class="white_td">Buttons :</td>
                <td class="white_td"><?php echo $this->data['User']['buttons'];?></td>
              </tr>
              <?php
              */
              ?>
            </table>
        </div>
    <div class="page_8_middel">
    <div class="spacer"></div>
    	<div class="page-8detail">
    		<div class="plus_head"><?php if($is_me) echo "Recent Photos"; else echo "My Recent Photos"; ?></div>
        </div>
    </div>
    <div class="clear"></div>
    <div id='my_drobes' class='drobe_list infiniteScroll'>
      	<div class="long_content my_recent_Drobes drobe_list infiniteScroll" id="drobe_list"></div>
       	<div class="infiniteScrollLoading">Loading More Drobes</div>
    </div>
	
</div>   

<script type="text/javascript">
$(document).ready(function(){
	$('#drobe_list').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe",'recent',$this->data['User']['unique_id']))?>').attr('last',1);
	$('#drobe_list').load('<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe",'recent',$this->data['User']['unique_id']))?>',function(){
        set_scrollbar();
    });
    <?php
    	if($user_totals['total_favourites']>0)
   	 	{
    	?>
    	    	$('#fav_btn').css('cursor','pointer').click(function(){
    	        <?php 
    	        	if($is_me)
    	        	{
    	        	?>
    	        	document.location.href="faves";
    	        	<?php 
    				}
    				else
    				{
    	        	?>
    	        	document.location.href="faves/<?php echo $this->data['User']['unique_id']?>";
    	        	<?php 
    				}
    	        ?>
    	        });
    	<?php 
    	}
    	if($user_totals['total_drobes']>0)
    	{
    		?>
    			$('#drobe_btn').css('cursor','pointer').click(function(){
    			    <?php 
    			    	if($is_me)
    			    	{
    			        	?>
    			        	document.location.href="users/drobe_list";
    			        	<?php 
    					}
    					else
    					{
    			        	?>
    			        	document.location.href="users/drobe_list/<?php echo $this->data['User']['unique_id']?>";
    			        	<?php 
    					}
    				?>
    			});
    		<?php 
    	} 
	    if($user_totals['followings']>0)
	    {
	    ?>
	    	$('#following_btn').css('cursor','pointer').click(function(){
	        <?php 
	        	if($is_me)
	        	{
	        	?>
	        	document.location.href="following";
	        	<?php 
				}
				else
				{
	        	?>
	        	document.location.href="users/following/<?php echo $this->data['User']['unique_id']?>";
	        	<?php 
				}
	        ?>
	        });
	    <?php 
		}
		if($user_totals['followers']>0)
		{
			?>
	    	$('#follower_btn').css('cursor','pointer').click(function(){
	        <?php 
	        	if($is_me)
	        	{
	        	?>
	        	document.location.href="users/follower";
	        	<?php 
				}
				else
				{
	        	?>
	        	document.location.href="users/follower/<?php echo $this->data['User']['unique_id']?>";
	        	<?php 
				}
		       ?>
		      });
		   <?php 
		}
    ?>
    $('#follower_btn').attr("count",<?php echo $user_totals['followers'];?>);
	function set_scrollbar()
    {
       	var selectedtab = '.infiniteScroll';
    	if($(selectedtab)[0].fleXcroll== undefined)
		{
		   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
		   apply_infinite_scrolling($(selectedtab).attr('id'));
		}
    }
});

</script>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   