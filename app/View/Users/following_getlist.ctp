<?php 
$i=0;
if(count($data)>0)
{
	
	foreach($data as $drobe){
		
		$original_sell_price = "";
		if($drobe['SellDrobe']['sell_price']<$drobe['SellDrobe']['original_sell_price'])
		{
			$original_sell_price = "$".$drobe['SellDrobe']['original_sell_price']; 
		}
		
	//if featured_drobe option is on then and then it will display price size and buy button.
		if(($drobe['Drobe']['featured']==1 || $drobe['Drobe']['post_type']=='sell') && $drobe['Drobe']['buy_url']!="" && Configure::read('setting.featured_drobe')=="on")
		{
			$div = "<div class='sell_drobe_detail'>";
			if($drobe['Drobe']['post_type']=='sell')
			{	
				//set Buy button, price and size if the drobe is post for sell
				$div.="<span class='price_detail'>$".
								"<b>".$drobe['SellDrobe']['sell_price']."</b>".
								"&nbsp;<strike><b>".$original_sell_price."</b></strike></span>".
								"<span class='size_detail'>size:<b>".$drobe['SellDrobe']['size_name']."</b></span>";
			}
			$div.="<div class='buy_btn'><div class='buy_most_recent'><span value=".$drobe['Drobe']['buy_url']."></span><b>Buy</b></div> </div>".
						"</div>";
		}	
	
		echo  $this->Html->link($div.$this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],
	 		array('alt'=>$drobe['Drobe']['comment'],'height'=>"100%", 'width'=>"100%")).($drobe['Drobe']['is_highest_rated'] > 0 ? "<div class='highest-rated-drobe'></div>":""),
	 		array("controller"=>"drobes","action"=>"rate",$drobe['Drobe']['unique_id'],$this->params->params['pass'][0],"following",$drobe['User']['unique_id']),
				
	 		array("border"=>0,'escape'=>false,"class"=>"drobe_thumb"));
	}
}
else
{
	if(!isset($this->params->named['page']) || $this->params->named['page']==1)
	{
		?>
		<div class="spacer"></div>
		<p class="empty_message">You have not followed anyone yet, click on Explore and Follow drobes to get started</p>
		<?php
	} 
}	  
?>

<script>
$(document).ready(function(){

	//Click on buy button it open new window for displaying that item in shopify.
	$(".buy_most_recent").each(function()
	{		
		$(this).unbind('click');  // This code for duplicate window open for Most recent and The shop tab.
		$(this).click(function(e)
	    {
		    javascript:window.open($(this).find('span').attr('value'),"_blank");
		    return false;    
	    })
	});	
});
</script>