<?php 

echo $this->Form->create('User',array("id"=>"settings"));
echo $this->Form->hidden('UserSetting.id');
?>
<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
   	<h3 class="text-center">Sharing Settings</h3>

<div class="table-responsive">
<table class="table table-bordered">
<tr align="center">
	<td><?php echo $this->Html->image('/images/facebook_icon.png');?></td>
	<td><?php echo $this->Html->image('/images/twitter_icon.png');?></td>
	<td></td>
</tr>
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.fb_post_drobe",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.tw_post_drobe",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Share My Drobes</td>
</tr>
<tr  align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.fb_post_fave",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.tw_post_fave",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Share My Faves</td>
</tr>
</table>
</div>
<div class="spacer"></div>
<div class=''>
<?php 
if(!$this->data['UserSetting']['fb_connected'] || $fb_connected_info==false)
{
?>
<div class="not_connected">Not linked with Facebook</div><br>
<a href="<?php echo $facebookUrl;?>" class="fb_link_btn connect simple_popup">Connect with Facebook</a>
<?php 
}
else
{
echo "<div class='left'>".$this->Html->image("http://graph.facebook.com/".$fb_connected_info['id']."/picture",array("height"=>"30px","width"=>"30px"))."</div>";
echo "<div class='left' style='margin-left:10px; width:160px'> <b>".$fb_connected_info['first_name']." ".$fb_connected_info['last_name']."</b><br/>".$fb_connected_info['email']."</div>";
echo $this->Html->link("Disconnect from Facebook",array("controller"=>"users","action"=>"disconnect_from_facebook"),array("class"=>"fb_link_btn disconnect right"));
}
?>
<div class="clearfix">
</div>

<div class='mt15'>
<?php 
if(!$this->data['UserSetting']['tw_connected'] || $tw_connected_info==false)
{
?>
<div class="not_connected">Not linked with Twitter</div><br>
<a href="<?php echo $twitterLoginUrl;?>" class="tw_link_btn connect simple_popup">Connect with Twitter</a>
<?php 
}
else
{
	if(isset($tw_connected_info->profile_image_url) && $tw_connected_info->name)
	{
	echo "<div class='left'>".$this->Html->image($tw_connected_info->profile_image_url,array("height"=>"30px","width"=>"30px"))."</div>";
	echo "<div class='left' style='margin-left:10px; width:160px'> <b>".$tw_connected_info->name."</div>";
	}
	else
	{
		echo '<div class="not_connected">Linked with Twitter</div>';
	}
	echo $this->Html->link("Disconnect from Twitter",array("controller"=>"users","action"=>"disconnect_from_twitter"),array("class"=>"tw_link_btn disconnect"));
}
?>
</div>

<div class="clearfix"></div>

<h3 class="text-center mt25">Notifications and Photos</h3>
<div class="table-responsive">
<table class="table table-bordered">
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.conversation_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Conversations</td>
</tr>
<tr  align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.got_comment_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Got Comments</td>
</tr>
<tr  align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.follow_post_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">User you follow post drobes</td>
</tr>
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.follower_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">You have a new follower</td>
</tr>
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.broadcast_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Broad cast notification</td>
</tr>
</table>
</div>
<div class="spacer"></div>
<div class="col-sm-12 text-center">
<?php 
echo $this->Form->submit("Save Changes",array("class"=>"btn btn-success"));
echo $this->Form->end();
?>
</div></div>

