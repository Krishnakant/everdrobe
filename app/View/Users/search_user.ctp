<?php
if(count($searched_users)>0)
{
	?>
	<h4>Seached Users from Everdrobe</h4>
	<?php
	foreach($searched_users as $id=>$friend)
	{
		?>
		<div class="friend">
		<div class="photo">
		<?php 
		echo  $this->Html->link($this->Html->image($friend['photo']=="" ? Router::url('/images/default.jpg',true): '/profile_images/thumb/'.$friend['photo'],array('alt'=>"",'height'=>"36px", 'width'=>"36px")),
 				array("controller"=>"users","action"=>"profile",$id),array("border"=>0,'escape'=>false));
		if($friend['star_user']==1):?>
			<div class="star-user-small"></div>
		<?php endif;?>
		</div>
		<div class="name"><?php echo $friend['name'];?></div>
		<div class="action_btn">
		<?php if($friend['following'])
		{
			?>
				<a href="javascript:void(0)" class="everdrobe btn btn-primary" rel="<?php echo $id;?>">Following</a>
			<?php
		}
		else
		{
			?>
			<a href="javascript:void(0)" class="everdrobe btn  btn-default" rel="<?php echo $id;?>">Follow</a>
			<?php 
		}
		?>
		</div>
		<div class="clearfix"></div>
		</div>
		<div class="spacer"></div>
		<?php 
	} 
}
else
{
	?>
	<p class="empty_message">Result not matched with your keyword</p>
	<?php 
}
