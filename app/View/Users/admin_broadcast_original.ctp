<h2>Send Broadcast Message</h2>
<?php 
echo $this->Form->create("User");
echo $this->Form->input("send_to",array("options"=>array("all"=>"All Everdrobe Users","selected"=>"Selected Everdrobe User","active_users"=>"Active Everdrobe User","inactive"=>"Inactive Everdrobe User","suspended"=>"Suspended/Blocked Everdrobe User"),"default"=>"all","id"=>"send_to_type"));
echo $this->Form->input("users",array("label"=>"Select Users","options"=>$userlist,"id"=>"user_list","multiple"=>"multiple",'class'=>"select_field","empty"=>"Select User from List"));
echo $this->Form->input("message",array("type"=>"textarea"));
echo $this->Form->end();
?>

<script type="text/javascript">
$('document').ready(function(){

	$('#user_list').wrap('<div class="select_wrap"  multiple="multiple"></div>');
	$(".select_field").custSelectBox({isscrolling:true,selectwidth:"250px"});
	if($('#send_to_type option:selected').val()!="selected")
	{
		$('.select_wrap').parent().hide();
	}
	else $('.select_wrap').parent().show();
	$('#send_to_type').change(function(){
		if($(this).val()!="selected")
		{
			$('.select_wrap').parent().hide();
		}
		else $('.select_wrap').parent().show();
	});

	
});


</script>