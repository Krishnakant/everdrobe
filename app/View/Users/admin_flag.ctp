<h2>Flagged Users</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search User : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search user by user name, email" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($users)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('User.id','#');?></th>
			<th>Image</th>
			<th><?php echo $this->Paginator->sort('User.email','Email');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name'." "."User.last_name",'Name');?></th>
			<th>Action</th>
		</tr>
		<?php 
		foreach($users as $user)
		{
			?>
			<tr class="<?php echo $user['User']['id']; ?>">
				<td width="30px" rowspan="2"><?php echo $user['User']['id'];?></td>
				<td width="90px"><?php echo $this->Html->link($this->Html->image('/profile_images/thumb/'.$user['User']['photo'],array('width'=>Configure::read('profile.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"user prview Image")),$this->Html->url('/profile_images/'.$user['User']['photo'],true),array("escape"=>false,"class"=>"example8"));?></td>
				<td><?php echo $user['User']['email']?></td>
				<td width="100px">
					<?php echo $user['User']['first_name']." ".$user['User']['last_name']." (".$user['User']['username'].")";?>
				</td>
				<td width="100px" rowspan="2">
					<div class="submenu actions">
								<?php 
					if($user['User']['status']=="suspended") echo $this->Html->link('Activate User',array("controller"=>"users","action"=>"action","activate","admin"=>true,$user['User']['id']),array("confirm"=>"Are you sure to block this user?"));
					else echo $this->Html->link('Block User',array("controller"=>"users","action"=>"action","block","admin"=>true,$user['User']['id']),array("confirm"=>"Are you sure to block this user?"));
					?>
					<?php 
						echo $this->Html->link('View Detail',array("controller"=>"users","action"=>"view","admin"=>true,$user['User']['id'])); 
						echo $this->Html->link('Unflag It',array("controller"=>"users","action"=>"unflag","admin"=>true,$user['User']['id']),array("confirm"=>"Are you sure to make unflagged this user?"));
					?>
					</div>
				</td>
			</tr>
			<tr>
			<td colspan="3">
			<h3 style="margin: 0; padding: 0">Flagged this User by:</h3>
			<table>
			<tr>
			<th>Full Name</th>
			<th>User Name</th>
			<th>Reason for Flag</th>
			</tr>
			<?php 
			foreach($user['UserFlagged'] as $flag)
			{
				?>
				<tr>
				<td><?php echo $this->Html->link($flag['User']['first_name']." ".$flag['User']['last_name'],array("controller"=>"users","action"=>"view","admin"=>true,$flag['User']['id']))?></td>
				<td><?php echo $this->Html->link($flag['User']['username'],array("controller"=>"users","action"=>"view","admin"=>true,$flag['User']['id']))?></td>
				<td><?php echo $flag['FlagCategory']['category_name'];?></td>
				</tr>
				<?php 
			}
			?>
			</table>
			
			</td>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>