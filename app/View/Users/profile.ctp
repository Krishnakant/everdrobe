

    <div class="col-sm-6 text-center mt30">
    <h2 class="text-center"><?php echo ((isset($this->data['User']['username']) && $this->data['User']['username'] != null) ? $this->data['User']['username'] : $this->data['User']['first_name']. " " .$this->data['User']['last_name']); ?></h2>
    
    	<div class="page_8user">
    	
<?php 

if($this->data['User']['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$this->data['User']['photo']))
{
	echo $this->Html->image('/profile_images/thumb/'.$this->data['User']['photo'],array('border'=>0, 'id'=>"profile_image"));
}
else
{
	echo $this->Html->image('/images/default_big.jpg',array('border'=>0, 'id'=>"profile_image"));
}

if($this->data['User']['star_user']==1 ):?>
	<div class="star-user"></div>
<?php endif;?>

</div>

<div id="flag_block" class="flag_form">
<h2>Flag user</h2>
<div class="spacer"></div>
<?php 

echo $this->Form->input('flag_category',array('options'=>Cache::read('flag_category'),'id'=>'flag_category','class'=>'form-control','label'=>false,'div'=>false,'empty'=>"Select Reason"));
?>
<div class="spacer"></div>
<?php 
echo $this->Form->input('flagged_user_id',array('type'=>'hidden','id'=>'flagged_user_id','label'=>false,'div'=>false,'value'=>$this->data['User']['id']));
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_flag",'class'=>'btn btn-green'));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_flag",'class'=>'btn btn-danger'));
?>
<div class="spacer"></div>
</div>

<ul class="list-inline list-unstyled mt15">
<li>
<div class="flag_user" >
<?php
/** User cannot flag itself if profile for user itself then it cannot display flag for user profile. **/ 
if(!$is_me)
{
?>

<b>Flag User : </b> 
	<a href="javascript:void(0)" id="flag_button"><i class="fa fa-flag" aria-hidden="true"></i><?php //echo $this->Html->image('/images/flag.png',array("height"=>18));?></a>
	

<?php 
}
?>
	
</div>
</li>
<li>

            <?php 
            if($is_me)
            {
            	echo $this->Html->link('Edit Profile',array('controller'=>"users",'action'=>"edit_profile"),array("class"=>"btn btn-success"));
            	?>
            	
            	<?php 
            	echo $this->Html->link('Find Friends',array('controller'=>"users",'action'=>"search"),array("class"=>" btn btn-primary"));
           	} 
            else
            {
            	if($is_followed==1)
            	{ 
            		?>
            		<div id="follow_button" class="btn btn-green white_minus" rel="<?php echo $this->data['User']['unique_id'];?>" >Following</div>
            		<?php 
            	}
            	else
            	{
            		?>
            		<div id="follow_button" class="btn btn-danger white_plus" rel="<?php echo $this->data['User']['unique_id'];?>" >Follow</div>
            		<?php 
            	} 
            }?>
</li>
</ul>
			
			
<div class="clear" style="height: 2px;"></div>
<div class="profile_detail"><?php 
//echo ((isset($this->data['User']['username']) && $this->data['User']['username'] != null) ? $this->data['User']['username'] : $this->data['User']['first_name']. " " .$this->data['User']['last_name'])."<br/>";
echo "<small>";
if(!empty($userData['gender'])) echo Inflector::humanize($userData['gender']);
//if(!empty($userData['relationship_status'])) echo Inflector::humanize($userData['relationship_status']).", <br/>";
if(!empty($userData['country'])) echo ", ".Inflector::humanize($userData['country']).". ";
echo "</small>";
//if($userData['website']!="") echo "<br><br>".$this->Html->link("Visit My Website",$userData['website'],array("target"=>"_blank","class"=>"page_8_sky_b","style"=>"margin:0;padding:0;"));
?></div>


			
<div class="clear" style="height: 2px;"></div>
        
    </div>
    <div class="col-sm-6 mt30">
    	<div class="bio_right_table1 table-responsive">
        	<table class="table table-bordered table-striped" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th colspan="2" class="white_td text-center">
                <h4 class="m0"><?php
	                if($is_me) echo "My Totals";
	                else echo ((isset($this->data['User']['username']) && $this->data['User']['username'] != null ) ? $this->data['User']['username'] :$this->data['User']['first_name']." ".$this->data['User']['last_name'])." Totals";
                ?></h4>
                </th>
              </tr>
              <tr>
                <td class=""> Cute :</td>
                <td class=" text-center"><strong><?php echo $user_totals['total_in']?></strong></td>
              </tr>
              <tr>
                <td class="">Hot :</td>
                <td class=" text-center"><strong><?php echo $user_totals['total_out']?></strong></td>
              </tr>
             <tr>
                <td class="">Vote Received :</td>
                <td class=" text-center"><strong><?php echo $user_totals['vote_received']?></strong></td>
              </tr>
               <tr>
                <td class="">Votes Given : </td>
                <td class=" text-center"><strong><?php echo $this->Html->convertToReadeable($user_totals['vote_given']);?></strong></td>
              </tr>
              <tr>
                <td class="">Available Everbucks:</td>
                <td class=" text-center"><strong><?php echo $this->Html->convertToReadeable($user_totals['current_ebx_point']);?></strong></td>
              </tr>
              <tr>
                <td class="">Total Everbucks : </td>
                <td class=" text-center"><strong><?php echo $this->Html->convertToReadeable($user_totals['earned_ebx_point']);?></strong></td>
              </tr>
              <tr>
                <td class="">Comments Given : </td>
                <td class=" text-center"><strong><?php echo $this->Html->convertToReadeable($user_totals['feedback_given']);?></strong></td>
              </tr>
              <tr>
                <td class="">Comments Received : </td>
                <td class=" text-center"><strong><?php echo $this->Html->convertToReadeable($user_totals['feedback_received']);?></strong></td>
              </tr>
              <?php 
              /*
               ?>
              <tr>
                <td class="white_td">Buttons :</td>
                <td class="white_td"><?php echo $this->data['User']['buttons'];?></td>
              </tr>
              <?php
              */
              ?>
            </table>
        </div>
    </div> 
    <div class="clearfix"></div>
    <div class="info_box_group text-center">
        	<div class="info_box" id="fav_btn"><strong><?php echo $this->Html->convertToReadeable($user_totals['total_favourites']);?></strong><br><span>Fav'ed</span></div>
        	<div class="info_box" id="drobe_btn"><strong><?php echo $this->Html->convertToReadeable($user_totals['total_drobes']); ?></strong><br><span>Drobes</span></div>
            <div class="info_box" id="following_btn"><strong><?php echo $this->Html->convertToReadeable($user_totals['followings']); ?></strong><br><span>Following</span></div>
            <div class="info_box" id="follower_btn"><strong><?php echo $this->Html->convertToReadeable($user_totals['followers']); ?></strong><br><span>Follower</span></div>
		
			<div class="clearfix"></div>
			
        </div>
           
	<div class="clear" style="height: 2px;"></div>
    <div class="page_8_middel">
    	<div class="bio_data_tax flexcroll 12"><?php echo str_replace(array("\n"),"<br>",$this->data['User']['bio']); ?></div>
        
    <div class="page_8_middel">
    <div class="spacer"></div>
    	<h3 class="text-center">
    		<?php if($is_me) echo "Recent Photos"; else echo "My Recent Photos"; ?>
        </h3>
    </div>
    <div class="clear"></div>
    <div id='my_drobes' class='drobe_list infiniteScroll'>
      	<div class="long_content my_recent_Drobes drobe_list" id="most_recent"></div>
       	<div class="infiniteScrollLoading">Loading More Drobes</div>
    </div>
	
</div>

<script type="text/javascript">
$(document).ready(function(){
	activate_menu("profile");	
		
	$('#most_recent').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe",'recent',$this->data['User']['unique_id']))?>').attr('last',1);
	$('#most_recent').load('<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe",'recent',$this->data['User']['unique_id']))?>',function(){
        set_scrollbar();
    });
    <?php
    	if($user_totals['total_favourites']>0)
   	 	{
    	?>
    	    	$('#fav_btn').css('cursor','pointer').click(function(){
    	        <?php 
    	        	if($is_me)
    	        	{
    	        ?>
    	        	document.location.href="faves";
    	        	<?php 
    				}
    				else
    				{
    	        	?>
    	        	document.location.href="faves/<?php echo $this->data['User']['unique_id']?>";
    	        	<?php 
    				}
    	        ?>
    	        });
    	<?php 
    	}
    	if($user_totals['total_drobes']>0)
    	{
    		?>
    			$('#drobe_btn').css('cursor','pointer').click(function(){
    			    <?php 
    			    	if($is_me)
    			    	{
    			        	?>
    			        	document.location.href="users/drobe_list";
    			        	<?php 
    					}
    					else
    					{
    			        	?>
    			        	document.location.href="users/drobe_list/<?php echo $this->data['User']['unique_id']?>";
    			        	<?php 
    					}
    				?>
    			});
    		<?php 
    	} 
	    if($user_totals['followings']>0)
	    {
	    ?>
	    	$('#following_btn').css('cursor','pointer').click(function(){
	        <?php 
	        	if($is_me)
	        	{
	        	?>
	        	document.location.href="following";
	        	<?php 
				}
				else
				{
	        	?>
	        	document.location.href="users/following/<?php echo $this->data['User']['unique_id']?>";
	        	<?php 
				}
	        ?>
	        });
	    <?php 
		}
		if($user_totals['followers']>0)
		{
			?>
	    	$('#follower_btn').css('cursor','pointer').click(function(){
	        <?php 
	        	if($is_me)
	        	{
	        	?>
	        	document.location.href="users/follower";
	        	<?php 
				}
				else
				{
	        	?>
	        	document.location.href="users/follower/<?php echo $this->data['User']['unique_id']?>";
	        	<?php 
				}
		       ?>
		      });
		   <?php 
		}
    ?>
    $('#follower_btn').attr("count",<?php echo $user_totals['followers'];?>);
    
	function set_scrollbar()
    {
       	var selectedtab = '.infiniteScroll';
       	
       	if($(selectedtab)[0].fleXcroll== undefined)
		{
		   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
		   apply_infinite_scrolling($(selectedtab).attr('id'));
		}
    }
});

</script>