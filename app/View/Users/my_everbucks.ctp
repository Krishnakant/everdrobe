<h2 class="text-center">My Everbucks</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>

	<div class="spacer"></div>
	<div class="table-responsive">
		<table class="table table-bordered">
		
			<?php 
			if(isset($user_data['User']['Photo']) && $user_data['User']['photo']!="")
			{
				//echo $this->Html->image('/profile_images/thumb/'.$user_data['User']['photo'],array('width'=>Configure::read('drobe.thumb.width'),'height'=>Configure::read('drobe.thumb.height'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));
			}
			else
			{
				//echo $this->Html->image('/images/default.jpg',array('width'=>Configure::read('drobe.thumb.width'),'height'=>Configure::read('drobe.thumb.height'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));
			}	
			//echo " <b>".$user_data['User']['first_name']." ".$user_data['User']['last_name']."</b> ";
			if($user_data['User']['star_user']>0)
			{ ?>
				<tr>
		<td><?php echo $this->Html->image('/images/star.png'); ?></td>		
		</tr>
			<?php }	?>
		
		</table>
        </div>
				<p class="text-center">
					Everbucks is your everdrobe currency earned by getting rewarded for meaningful comments, your posts and by getting your outfits voted the highest of the month. They will get used towards any purchase you make at 
					<?php echo $this->Html->link('The Everdrobe shop','http://shop.everdrobe.com/',array('target'=>'_blank')); ?>
				</p>
			
		
	
	<div  class="text-center mt20">
		
        <div class=" form-inline">
            <?php 
            echo $this->Form->create('RedeemEbx',array('url'=>array('controller'=>'ebx_transactions','action'=>'redeem_ebx_point')));
            echo $this->Form->input('ebx_redeem',array('label'=>'Redeem EBX : &nbsp;','div'=>false,'name'=>'ebx_redeem','class'=>'form-control input-lg'))." &nbsp;";
            echo $this->Form->submit("Redeem",array('class'=>'btn btn-success btn-lg','div'=>false));
            ?>
        </div>
				
		
		<div class="info_box_group mt20">
		
            <div class="info_box"><strong><?php echo $user_totals['earned_ebx_point'];?></strong><br />Total Everbucks</div>
            <div class="info_box"><strong><?php echo $user_totals['current_ebx_point'];?></strong><br />Available Everbucks</div>
            <div class="info_box"><strong><?php echo $user_totals['feedback_given'];?></strong><br />Total Comments</div>
            <div class="info_box"><strong><?php echo $user_totals['vote_given']?></strong><br />Total Votes</div>		
		
        </div>
		
		<div  class="mt15">
            <p><?php echo Configure::read('setting.month_promo_rate_ebx');?>
        </div>			
	</div>
    
<script type="text/javascript">
$('document').ready(function(){
activate_menu("everbucks");	
});
</script>
	