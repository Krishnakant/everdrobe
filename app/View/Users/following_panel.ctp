<h2 class="text-center title_mt1">Drobes from my Followings</h2>
<?php 
if($this->Session->read('Auth.User.id')>0)
{
//echo $this->element('filter_category');
?>
<?php 
}
?>
<div class="details search-box">
<?php 
//echo $this->Form->input('drobe_category',array("type"=>"select","options"=>$categoryList,"label"=>false,'empty'=>"All Catgeories"));
//echo $this->Form->input('search_drobe',array("type"=>"input","class"=>"search","label"=>false,"placeholder"=>"Enter any keywords like drobe comment, uploader name, etc.."));
?>

<div class="input-group">
      <input type="text" class="search_input form-control input-lg" id="search_input" placeholder="Search by Name, Username or Keyword" />
      <div class="input-group-addon search-icon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
    </div>
</div>

<!-- <div class="details">
<input type="text" class="search_input" id="search_input" placeholder="Search by Name, Username or Keyword" />
</div> -->

		<div id="TabbedPanelsFollowing" class="TabbedPanels">
		  <ul class="TabbedPanelsTabGroup">
		    <li class="TabbedPanelsTab" tabindex="0">Most Recent</li>
		    <!-- <li class="TabbedPanelsTab" tabindex="0">Highest Rated</li> -->
            <li class="TabbedPanelsTab" tabindex="0">The Shop</li>
	      </ul>
		  <div class="TabbedPanelsContentGroup">
		    <div class="TabbedPanelsContent">
				<div class="container">
            	<div id='most_recent_area' class='drobe_list infiniteScroll'>
                	<div class="long_content" id="most_recent" rel="<?php echo $this->Html->url(array('action'=>"following_getlist","recent"))?>" last="1">
                	</div>   
                </div></div>
            </div>
		    <!-- <div class="TabbedPanelsContent">
            	<div id='heighest_rated_area' class='drobe_list infiniteScroll'>
                   <div class="long_content" id="heighest_rated" rel="<?php echo $this->Html->url(array('action'=>"following_getlist_highest_rated","1"))?>" last="1">
                   </div>
                </div>
            </div> -->
            
            <div class="TabbedPanelsContent">
			<div class="container">
            	<div id='featured_area' class='drobe_list infiniteScroll'>
            		<div class="long_content" id="featured" rel="<?php echo $this->Html->url(array('action'=>"following_getlist","featured"))?>" last="1">
            		<div class="spacer"></div>
            		<p class="empty_message">Dear User, Wait for the sometime, Shop coming soon...</p>
            		</div> 
                 </div></div>
            </div>
	      </div>
	      <div class="infiniteScrollLoading">Loading More Drobes</div>
    </div>
	<script type="text/javascript">
		activate_menu("following");
		$(document).ready(function(){

			<?php 
			if($this->Session->read('Auth.User.id')>0)
			{
				?>
				$('#most_recent_area,  #most_viewed_area, #featured_area').each(function(){
					$(this).css('height','450px');
				});
				<?php 
			}
			?>
			var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanelsFollowing");
			$('#TabbedPanelsFollowing .TabbedPanelsTab').click(function(){
				current_index=TabbedPanels1.getCurrentTabIndex();
				var selectedtab = '#TabbedPanelsFollowing .TabbedPanelsContentGroup > div:eq('+current_index+') .infiniteScroll';
				
				if($(selectedtab)[0].fleXcroll== undefined)
				{
				   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
				   apply_infinite_scrolling($(selectedtab).attr('id'));
				}	 
			});
		    //fleXenv.initByClass("drobe_list");
            load_everdrobes("");
            $('#drobe_category').change(function(){
            	load_everdrobes($(this).val());
            })
          
            $('#search_input').keypress(function(event) {
        	    if (event.which == 13) {
        	    	load_everdrobes($(this).val());
        	    }
        	});
            $('#search_input').blur(function() {
        	    load_everdrobes($(this).val());
        	});
              
        });
		function load_everdrobes(search)
        {
	        search=escape(search);
        	$('#most_recent').attr('rel','<?php echo $this->Html->url(array('action'=>"following_getlist","recent"))?>/search:'+search).attr('page',0);
        	//$('#heighest_rated').attr('rel','<?php echo $this->Html->url(array('action'=>"following_getlist_highest_rated","page_counter"))?>/search:'+search).attr('page',0);
            //$('#featured').attr('rel','<?php echo $this->Html->url(array('action'=>"following_getlist","featured"))?>/search:'+search).attr('page',0);
            
            $('#most_recent').load('<?php echo $this->Html->url(array('action'=>"following_getlist","recent"))?>/search:'+search,function(){
            	$('.TabbedPanelsTab:eq(0)').click();
				/**  When no any drobe in list then jquery error occured for this solution this condition written @sadikhasan **/
            	if(typeof $('#most_recent a:first').attr("href") !== "undefined")
            	{	
            		var latest_drobe=$('#most_recent a:first').attr("href").split('/');
            		latest_drobe=latest_drobe[(latest_drobe.length-2)];
            	}	
            	
				$('#most_recent').attr('rel','<?php echo $this->Html->url(array('action'=>"following_getlist","recent"))?>/'+latest_drobe+'/search:'+search);

            	set_scrollbar(0);
            });
            //$('#heighest_rated').load('<?php echo $this->Html->url(array('action'=>"following_getlist_highest_rated","1"))?>/search:'+search,function(){set_scrollbar(1);});
            //$('#featured').load('<?php echo $this->Html->url(array('action'=>"following_getlist","featured"))?>/search:'+search,function(){set_scrollbar(1);});
        }
		
        function set_scrollbar(index)
        {
           	var selectedtab = '#TabbedPanelsFollowing .TabbedPanelsContentGroup > div:eq('+index+') .infiniteScroll';
        	if($(selectedtab)[0].fleXcroll== undefined)
			{
			   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
			   apply_infinite_scrolling($(selectedtab).attr('id'));
			}
        }

        function nextrate()
        {
        	load_everdrobes("");
        	//document.location.href=document.location.href;
        }
        </script>