<div class="page-8detail">
<div class="plus_head">Change Password</div>
</div>
<?php 
echo $this->Html->link("Back to Edit Profile",array("controller"=>"users","action"=>"edit_profile"),array("class"=>"right_link"));
?>
<div class="clear"></div>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
if(isset($message) && $message!="")
{
	echo $message;
}
else if(isset($error_message) && $error_message!="")
{
	echo $error_message;
}
?>
<div class="spacer"></div>
<?php
echo $this->Form->create("User",array('action'=>"change_password"));
echo $this->Form->input("current_password",array("type"=>"password","label"=>"Current Password","class"=>"text_box"));
echo $this->Form->input("new_password",array("type"=>"password","label"=>"New Password","class"=>"text_box"));
echo $this->Form->input("confirm_password",array("type"=>"password","label"=>"Confirm Password","class"=>"text_box"));
?>
<div class="spacer"></div>
<?php 
echo $this->Form->submit('Change Password',array("class"=>"green_btn"));
echo $this->Form->end();
?>