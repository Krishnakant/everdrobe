<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
if(isset($message) && $message!="")
{
	echo $message;
}
else if(isset($error_message) && $error_message!="")
{
	echo $error_message;
}
?>
<div class="page-8detail">
<div class="plus_head">Change Email address</div>
</div>
<?php 
echo $this->Html->link("Back to Edit Profile",array("controller"=>"users","action"=>"edit_profile"),array("class"=>"right_link"));
?>

<?php 
echo $this->Form->create("User");
?>

<div class="spacer"></div>
<div>Your Current Email is: <b><?php echo $this->Session->read('Auth.User.email'); ?></b></div>
<div class="spacer"></div>
<?php 
echo $this->Form->input("email",array("label"=>"New Email","class"=>"text_box"));
echo $this->Form->input("confirm_email",array("class"=>"text_box"));
echo $this->Form->submit('Change Email Address', array("class"=>"green_btn"));
echo $this->Form->end();
?>