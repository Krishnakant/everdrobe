<h2 class="text-center"><?php if($is_me) echo  "My Drobes"; 
else echo "Drobes of ".((isset($user['username']) && $user['username'] != null) ? $user['username'] : $user['first_name']." ".$user['last_name']);
?></h2>
<?php 
if($this->Session->read('Auth.User.id')>0)
{
//echo $this->element('filter_category');
?>
<?php 
}
?>
<div class="details search-box">

<div class="input-group">
      <input type="text" class="search_input form-control input-lg" id="search_input" placeholder="Search by Comment or Keyword" />
      <div class="input-group-addon search-icon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
    </div>
<div class="clearfix"></div>
</div>
		<div id="my_drobe_list" class="TabbedPanels">
		  <ul class="TabbedPanelsTabGroup">
		    <li class="TabbedPanelsTab" tabindex="0">Most Recent</li>
		    <!-- <li class="TabbedPanelsTab" tabindex="0">Highest Rated</li> -->
            <li class="TabbedPanelsTab" tabindex="0">The Shop</li>
	      </ul>
		  <div class="TabbedPanelsContentGroup">
		    <div class="TabbedPanelsContent">
			<div class="container">
            	<div id='most_recent_area' class='drobe_list infiniteScroll'>
                	<div class="long_content" id="most_recent">
                	</div>   
                </div></div>
            </div>
		    <!-- <div class="TabbedPanelsContent">
            	<div id='heighest_rated_area' class='drobe_list infiniteScroll'>
                   <div class="long_content" id="heighest_rated">
                   </div>
                </div>
            </div> -->
            <div class="TabbedPanelsContent">
			<div class="container">
            	<div id='featured_area' class='drobe_list infiniteScroll'>
            		<div class="long_content" id="featured">
            		<div class="spacer"></div>
            		<p class="empty_message">Dear User, Wait for the sometime, Shop coming soon...</p>
            		</div> 
                 </div></div>
            </div>
	      </div>
	      <div class="infiniteScrollLoading">Loading More Drobes</div>
    </div>
	<script type="text/javascript">
		
	$(document).ready(function(){
		activate_menu("profile");
				var TabbedPanels1 = new Spry.Widget.TabbedPanels("my_drobe_list");
				$('#my_drobe_list .TabbedPanelsTab').click(function(){
					current_index=TabbedPanels1.getCurrentTabIndex();
					var selectedtab = '#my_drobe_list .TabbedPanelsContentGroup > div:eq('+current_index+') .infiniteScroll';
					if($(selectedtab)[0].fleXcroll== undefined)
					{
					   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
					   apply_infinite_scrolling($(selectedtab).attr('id'));
					}	 
				});
				load_user_drobe("");
				    /*$('#search_input').change(function(){   
		                load_everdrobes($(this).val());
		            }); */
		            $('#search_input').keypress(function(event) {
		        	    if (event.which == 13) {
		        	    	load_user_drobe($(this).val());
		        	    }
		        	});
		            $('#search_input').blur(function() {
		            	load_user_drobe($(this).val());
		        	});
		});
		function load_user_drobe(search)
		{
			search = escape(search);
			
			$('#most_recent').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe","recent",$user['unique_id']))?>/search:'+search).attr('page',0).attr('last',1);
        	//$('#heighest_rated').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe_highest_rated","page_counter",$user['unique_id']))?>/search:'+search).attr('page',0).attr('last',1);
            //$('#featured').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe","featured",$user['unique_id']))?>/search:'+search).attr('page',0).attr('last',1);
            
            $('#most_recent').load('<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe","recent",$user['unique_id']))?>/search:'+search,function(){
                $('#my_drobe_list .TabbedPanelsTab:eq(0)').click();
                
                var most_recent_href = $('#most_recent a:first').attr("href");
                if(most_recent_href != undefined)
                {    
	            	var latest_drobe=$('#most_recent a:first').attr("href").split('/');
	            	latest_drobe=latest_drobe[(latest_drobe.length-4)];
                }	
            	$('#most_recent').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe","recent",$user['unique_id']))?>/'+latest_drobe+'/search:'+search);
            	
                set_scrollbar(0);
            });
            //$('#heighest_rated').load('<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe_highest_rated","1",$user['unique_id']))?>/search:'+search,function(){set_scrollbar(1);});
            //$('#featured').load('<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe","featured",$user['unique_id']))?>/search:'+search,function(){set_scrollbar(2);});
		}
		function set_scrollbar(index)
        {
           	var selectedtab = '#my_drobe_list .TabbedPanelsContentGroup > div:eq('+index+') .infiniteScroll';
        	if($(selectedtab)[0].fleXcroll== undefined)
			{
			   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
			   apply_infinite_scrolling($(selectedtab).attr('id'));
			}
        }
		function nextrate()
        {
			load_user_drobe();
        }
</script>