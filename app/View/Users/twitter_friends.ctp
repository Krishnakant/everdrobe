<?php 
if(!$is_ajax)
{
	?>
	<html>
	<head>
	<script type="text/javascript">
		window.opener.loadTwFriendsByAjax();
		window.close()
	</script>
	</head>
	<body>
	</body>
	</html>
	<?php 
	exit();
}
if(count($tw_friends['registered'])>0)
{
?>
<h2>Follow Twitter Friends from Everdrobe</h2>
<?php
	foreach($tw_friends['registered'] as $id=>$friend)
	{
		?>
		<div class="friend">
		<div class="photo">
		<?php 
		echo  $this->Html->link($this->Html->image('/profile_images/thumb/'.$friend['photo'],array('alt'=>"",'height'=>"30px", 'width'=>"30px")),
 				array("controller"=>"users","action"=>"profile",$id),array("border"=>0,'escape'=>false));
		if($friend['star_user']==1):?>
			<div class="star-user-small"></div>
		<?php endif;?>
		<a href="">
		<img src="<?php echo $friend['photo'];?>" width="30px"  height="30px"  border="0" />
		</a>
		</div>
		<div class="name"><?php echo $friend['name'];?></div>
		<div class="action_btn">
		<?php if($friend['following'])
		{
			?>
				<a href="javascript:void(0)" class="everdrobe" rel="<?php echo $id;?>">Following</a>
			<?php
		}
		else
		{
			?>
			<a href="javascript:void(0)" class="everdrobe" rel="<?php echo $id;?>">Follow</a>
			<?php 
		}
		?>
		</div>
		</div>
		<div class="spacer"></div>
		<?php 
	} 
}
if(count($tw_friends['not_registered'])>0)
{
?>
<div class="clear"></div>
<h2>Invite Twitter Friends to Everdrobe</h2>
<?php 
	foreach($tw_friends['not_registered'] as $id=>$friend)
	{
		?>
		<div class="friend">
		<div class="photo">
		<img src="<?php echo $friend['photo'];?>" width="30px"  height="30px" />
		</div>
		<div class="name"><?php echo $friend['name'];?></div>
		<div class="action_btn">
		<a href="javascript:void(0)" class="twitter" rel="<?php echo $id;?>">Invite</a>
		</div>
		</div>
		<div class="spacer"></div>
		<?php 
	} 
}
?>
<div class="spcaer"></div>
