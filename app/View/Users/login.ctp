
<?php 
if(!empty($errors))
{
	?>
	<h2 class="text-center">Login</h2>
	<div class="field_error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}

else if(isset($need_activation) && $need_activation==true)
{
	?>
	<h2 class="text-center">Login</h2>
	<div class="field_error">
		<ul><li>Your email id is not confirm, please comfirm your email id first from received wecome mail during registration.
		If you did not get activation info? click <?php echo $this->Html->link('here',array("controller"=>'users','action'=>'reactivate',$unique_id)); ?></li></ul>
	</div> 
		<?php
}

else if(isset($login_error) &&  $login_error==true)
{
	?>
	<h2 class="text-center">Login</h2>
	<div class="field_error">
		<ul><li>Invalid email/username or password, If you forgot password click on <?php echo $this->Html->link('here',array("controller"=>'users','action'=>'forgot_password')); ?></li></ul>
	</div>  
	<?php 
}
else if(isset($user_inactive) && $user_inactive==true)
{
	?>
	<h2 class="text-center">Login</h2>
	<div class="field_error">
		<ul><li>Account is not active or suspended,  please contact us by sending an email to: <a href="mailto:support@everdrobe.com:">support@everdrobe.com</a></li></ul>
	</div>  
	<?php 
}
else
{
	echo $this->element('user_login_info');
	?>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#user_login_block').show();
		//$('#container').height($('#user_login_block').outerHeight()+'px');
	})
	</script>
	<?php 
	
	
}
?>

<?php if($this->Session->read('Auth.User.id')>0){ ?>
	  <?php } else{ ?>
      <ul class="list-inline text-center mt20 user-action-menu">
        <li><a href="#" class="btn btn-primary register_btn">Signup</a></li>
        <li class=""><a class="btn btn-danger login_btn">Login</a></li>
      </ul>
    <?php  } ?>
	
	<script>
		$(".login_form").show();
	
		$(".login_btn").click(function(){
			$(".login_form").show();
			$(".user_register").hide();
		});
		$(".register_btn").click(function(){
			$(".user_register").show();
			$(".login_form").hide();
		});
		
		 $('.login_btn').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});
		$('.register_btn').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});
	</script>