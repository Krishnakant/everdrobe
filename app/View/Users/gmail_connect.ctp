<?php 
if(isset($not_active) && $not_active==true)
{
	?>
	<h1>Oops,</h1>
	<p>Account is not active,  please contact us by sending an email to: <a href="mailto:support@everdrobe.com:">support@everdrobe.com</a></p>
	<?php 
}
else
{
?>
	<html>
	<head>
	<!-- If new user is registered with gmail then it will redirect to follow screen for everdrobe users in user controller . -->
	<script type="text/javascript">
	window.opener.location.href="<?php 
		if($this->Session->read('new_registration'))
		{
			echo $this->Html->url(array("controller"=>"users","action"=>"suggest_to_user"));
		}	
		else
		{
			echo $this->Html->url(array("controller"=>"drobes","action"=>"index"));
		}	
	?>";
	window.close();
	</script>
	</head>
	<body>
	</body>
	</html>
<?php 
}
?>