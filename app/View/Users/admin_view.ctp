
<div class="spacer"></div>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
<?php 
echo $this->Html->link("Change User Detail", array("controller"=>"users","action"=>"edit","admin"=>true,$this->data['User']['id']))." ";
//echo $this->Html->link("Delete User", array("controller"=>"users","action"=>"action","remove","admin"=>true,$this->data['User']['id']),array("confirm"=>"Are you sure to remove this user?"))." ";
echo $this->Html->link("Block User", array("controller"=>"users","action"=>"action","block","admin"=>true,$this->data['User']['id']),array("confirm"=>"Are you sure to block this user?"))." ";
echo $this->Html->link("Activate", array("controller"=>"users","action"=>"action","activate","admin"=>true,$this->data['User']['id']),array("confirm"=>"Are you sure to activate this user?"))." ";
echo $this->Html->link("Deactivate", array("controller"=>"users","action"=>"action","deactivate","admin"=>true,$this->data['User']['id']),array("confirm"=>"Are you sure to deactivate this user?"));
?>
</div>

<h2>
<?php echo $this->data['User']['first_name']." ".$this->data['User']['last_name']." (".$this->data['User']['username'].")";?> (<?php echo Inflector::humanize($this->data['User']['status'])?>)</h2>


    <div class="page_8_middel">
    	<div class="page_8user">
<?php 
if($this->data['User']['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$this->data['User']['photo']))
{
	echo $this->Html->image('/profile_images/'.$this->data['User']['photo'],array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
else
{
	echo $this->Html->image('/images/default.jpg',array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}if($this->data['User']['star_user']==1):?>
		<div class="star-user"></div>
	<?php endif;?>
</div>
<div class="profile_detail"><?php 
echo $this->data['User']['username']."<br/>";
echo "<small>";
if(!empty($userData['gender'])) echo Inflector::humanize($userData['gender']).", <br/>";
if(!empty($userData['relationship_status'])) echo Inflector::humanize($userData['relationship_status']).", <br/>";
if(!empty($userData['country'])) echo Inflector::humanize($userData['country']).". ";
echo "</small>";
?></div>
        <div class="page_user_right">
        	<div class="page_8_white_box" id="fav_btn"><span>Fav'ed</span> : <?php echo $this->Html->convertToReadeable($user_totals['total_favourites']);?></div>
        	<div class="page_8_gry_box" id="drobe_btn"><span>Drobes</span> : <?php echo $this->Html->convertToReadeable($user_totals['total_drobes']); ?></div>
            <div class="page_8_white_box" id="following_btn"><span>Following</span> : <?php echo $this->Html->convertToReadeable($user_totals['followings']); ?></div>
            <div class="page_8_gry_box" id="follower_btn"><span>Follower</span> : <?php echo $this->Html->convertToReadeable($user_totals['followers']); ?></div>
        </div>
        
    </div>
    <div class="page_8_middel">
    	<div class="bio_data_tax flexcroll"><pre><?php echo $this->data['User']['bio']; ?></pre></div>
        <div class="bio_right_table">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="2" class="white_td">
                <?php
	                echo $this->data['User']['first_name']." ".$this->data['User']['last_name']." Totals";
                ?>
                </td>
              </tr>
              <tr>
                <td class="gry_td"> Cute :</td>
                <td class="gry_td"><?php echo $user_totals['total_in']?></td>
              </tr>
              <tr>
                <td class="white_td">Hot :</td>
                <td class="white_td"><?php echo $user_totals['total_out']?></td>
              </tr>
              <tr>
                <td class="gry_td">Votes Received : </td>
                <td class="gry_td"><?php echo $this->Html->convertToReadeable($user_totals['vote_received']);?></td>
              </tr>
               <tr>
                <td class="white_td">Votes Given : </td>
                <td class="white_td"><?php echo $this->Html->convertToReadeable($user_totals['vote_given']);?></td>
              </tr>
              
            </table>
        </div>
    <div class="page_8_middel">
    	<div class="page-8detail">
    		<div class="plus_head">Uploaded Drobe</div>
        </div>
    </div>
    <div class="spacer"></div>
    <div id='my_drobes' class='drobe_list infiniteScroll' style="height: 300px !important;">
      	<div class="long_content my_recent_Drobes drobe_list" id="most_recent" ></div>
       	<div class="infiniteScrollLoading">Loading More Drobes</div>
    </div>
    
    <div class="page_8_middel">
    	<div class="page-8detail">
    		<div class="plus_head">EBX Transactions</div>
        </div>
    </div>
    <div class="spacer"></div>
    <div>
      	<table>
      	<tr>
      		<td>Id</td>
      		<td>Points</td>
      		<td>Transactions type</td>
      		<td>Descriptions</td>
      	</tr>
      	</table>
    </div>
	
</div>                                                    
<script type="text/javascript">
$(document).ready(function(){

	$('#most_recent').attr('rel','<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe",'recent',$this->data['User']['id'],"admin"=>true))?>').attr('last',0);
	$('#most_recent').load('<?php echo $this->Html->url(array('controller'=>"drobes",'action'=>"userdrobe",'recent',$this->data['User']['id'],"admin"=>true))?>',function(){
		set_scrollbar();
    });
   	function set_scrollbar()
    {
       	var selectedtab = '.infiniteScroll';
       	if($(selectedtab)[0].fleXcroll== undefined)
		{
		   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
		   apply_infinite_scrolling($(selectedtab).attr('id'));
		}
    }
});
</script>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   