<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 text-center">
<div class="text-left mt15">
<strong class="pull-left mt5">Search or Invite User</strong>
<?php 
	echo $this->Html->link("Back to Profile",array("controller"=>"users","action"=>"profile"),array("class"=>"btn btn-default pull-right "));
?>

</div>

<div class="spacer"></div>
<div class="friends" >
<div class="">

<div class="input-group">
      <input type="text" class="search_input form-control input-lg" id="search_input" placeholder="Search Everdrobe Users by name, username or email" />
      <div class="input-group-addon search-icon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
    </div>
<div class="spacer"></div>
<div class='connection_info left friend_btn'>

<a href="<?php echo $facebookUrl;?>" class="fb_link_btn connect <?php echo $facebook_logged_in ? "" : "simple_popup"; ?>" >Facebook Friends</a>
</div>
<div class='connection_info right friend_btn'>
<a href="<?php echo $twitterLoginUrl;?>" class="tw_link_btn connect <?php echo $twitter_logged_in ? "" : "simple_popup"; ?>">Twitter Friends</a>
</div>
<div class="clear"></div>
</div>
<div class="spacer"></div>
<div class="friends_detail" id="friends_detail" style="height: 420px !important;">
<div id="friend_list" class="friend_list">
<p class="empty_message" style="margin: 50px 10px 0px 10px;">Search friend users Everdrobe or invite your friend from Facebook or Twitter for register with Everdrobe.</p>
</div>
<div class="clear"></div>
<div id="suggestion_list"></div>
<div class="clear"></div>
</div>
</div>
<?php 
if($this->Session->read('inviteFriend'))
{
	echo $this->Html->link('Next', array('controller'=>'users','action'=>'suggest_to_user',$user_id),array('class'=>'btn btn-primary mt10 btn_txt'));
	SessionComponent::write('inviteFriend',false);
}
?>
<script type="text/javascript">
var facebook_logged_in = <?php echo $facebook_logged_in? "true": "false"; ?>;
var twitter_logged_in = <?php echo $twitter_logged_in? "true": "false"; ?>;

$(document).ready(function(){
	activate_menu("profile");	
	fleXenv.fleXcrollMain('friends_detail');
	if(facebook_logged_in)
	{
		$('.fb_link_btn').unbind('click').click(function(){loadFbFriendsByAjax(); return false;});
	}
	if(twitter_logged_in)
	{
		$('.tw_link_btn').unbind('click').click(function(){loadTwFriendsByAjax(); return false;});
	}
	$('#search_input').change(function(){
		loadFriendList($(this).val());
    });
    $('#search_input').keypress(function(event) {
	    if (event.which == 13) {
	    	loadFriendList($(this).val());
	    }
	});
    $('#search_input').blur(function() {
    	loadFriendList($(this).val());
	});
	
});
</script>
</div>