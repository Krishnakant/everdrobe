<div class="col-sm-6 col-sm-offset-3"><div class="pageuser">
 <a class="logo-in" href="http://localhost/everdrobe/home/"></a>	
</div>
<h2>Choose a username</h2>
<div class="clear"></div>
<h4>Create your new username to get started!</h4>
<p>Only letters, numbers & underscores</p>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
if(isset($message) && $message!="")
{
	echo $message;
}
else if(isset($error_message) && $error_message!="")
{
	echo $error_message;
}
?>

<div class="spacer"></div>
<?php
echo $this->Form->create("User");
echo $this->Form->input("username",array("label"=>"","placeholder"=>"Enter a username","class"=>"form-control input-lg","div"=>"username_txt"));

?>
<div class="spacer"></div>
<?php 
echo $this->Form->submit('Submit',array("class"=>"btn btn-primary btn-lg","style"=>""));
echo $this->Form->end();
?>
</div>
<script>
	$(document).ready(function(){
		$(".text_box:first").focus();
			});
    
</script>