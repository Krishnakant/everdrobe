<h2>Account Activation</h2>
<?php 
if($is_activated)
{
	?>
	<p>Your account is successfully activated.</p>
	<div class="spacer"></div>
	
	<?php 
	if($this->Session->read('Auth.User.id')>0)
	{
	?>
	<h2>Why you wait?</h2>
        <p>Upload your drobe and check responses in a couple of, minutes to see lots of opinions.</p>
        <div class="actions">
        	<p><?php echo $this->Html->link('Upload your drobe',array('controller'=>"drobes","action"=>'mydrobe'),array("class"=>"blue_btn")); ?></p>
		</div>
		<div class="spacer"></div>
		<div class="actions">
		  	 <p>While you're waiting, do your opinions to people of world.</p>
          	 <p><?php echo $this->Html->link('Rate drobe',array('controller'=>"drobes","action"=>'rate'),array("class"=>"blue_btn")); ?></p>
		</div>
	<?php
	}
	else
	{
		?>
		<h2>Why you wait?</h2>
		<p>
		Login with Everdrobe and upload your drobe and check responses in a couple of, minutes to see lots of opinions.
		</p>
		<?php 
	}
}
else
{
	?>
	<p>Your account is not activated, please go to activation link provided on mail</p>
	<?php
}
?>