<?php 
if(count($data)>0)
{
	foreach ($data as $user)
	{
	?>
	<div class="details1">
	<?php
	if(isset($user['User']['username']) && $user['User']['username']!="")
	{
		$username = $user['User']['username'];
	}
	else
	{
		$username = $user['User']['first_name'].' '.$user['User']['last_name'];
	}
	
	
	echo $this->Html->link($username,array('controller'=>"users",'action'=>"profile",$user['User']['unique_id']),array('class'=>"follower_name"));
	if($user['NewDrobe']['total']>0)
	{
		?>
		<div class="badges"><a title="New drobes" class="tip"><?php echo $user['NewDrobe']['total'];?></a></div>
		<?php 
	}
	
	//echo $this->Html->link("Message",array("controller"=>"chattings","action"=>"index",$user['User']['unique_id']),array('class'=>"message_link"))?>
	<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div id='drobe_list' class='my_recent_Drobes'>
	<?php
	if(count($user['Drobe']))
	{
		foreach($user['Drobe'] as $drobe)
		{
			echo  $this->Html->link($this->Html->image('/drobe_images/thumb/'.$drobe['file_name'],
			array('alt'=>$drobe['file_name'],'height'=>85, 'width'=>Configure::read('drobe.thumb.width'))).($drobe['is_highest_rated']>0 ? "<div class='highest-rated-drobe'></div>":""),
			array("controller"=>"users","action"=>"drobe_list",$user['User']['unique_id']),
			array("class"=>"","rel"=>"example4","border"=>0,'escape'=>false,"class"=>"drobe_thumb"));
		}
	}
	else{
		echo "No Drobes.";
	}
	?>
	</div>
	<div class="spacer"></div>
	<?php 
	}
}
else
{
	if(!isset($this->params->named['page']) || $this->params->named['page']==1)
	{
	?>
	<div class="spacer"></div>
	<p class="empty_message">You have not followed anyone yet, click on rate drobes (make a button) to get started</p>
	<?php
	} 
}
?>
