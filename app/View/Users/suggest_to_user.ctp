<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
<h4>Follow Everdrobe Friends</h4>
Try following some users now.
<div class="spacer"></div>
<div class="friends_detail" id="friends_detail" style="height: 420px !important;">
<?php 
if(count($users)>0)
{
	foreach($users as $friend)
	{
		?>
		<div class="friend">
		<div class="photo">
		<?php 
		echo  $this->Html->link($this->Html->image($friend['thumb_photo'],array('alt'=>"",'height'=>"36px", 'width'=>"36px")),
 				array("controller"=>"users","action"=>"profile",$friend['unique_id']),array("border"=>0,'escape'=>false));
		if($friend['star_user']==1):?>
			<div class="star-user-small"></div>
		<?php endif;?>
		</div>
		<div class="name"><?php echo ((isset($friend['username']) && $friend['username'] != null) ? $friend['username'] : $friend['first_name']. " " .$friend['last_name']);?></div>
		<div class="action_btn">
		<?php if($friend['following'])
		{
			?>
				<a href="javascript:void(0)" class="follower_list btn btn-primary" rel="<?php echo $friend['unique_id'];?>">Following</a>
			<?php
		}
		else
		{
			?>
			<a href="javascript:void(0)" class="follower_list btn  btn-default" rel="<?php echo $friend['unique_id'];?>">Follow</a>
			<?php 
		}
		?>
		</div>
		<div class="clearfix"></div>
		</div>
		<div class="spacer"></div>
		<?php 
	} 
}
?>
</div>

<div class="spacer"></div>
<div class="spacer"></div>
<div class="spacer"></div>
<div class="text-center">
<?php 
//if($this->Session->read('followUser'))
//{
	echo $this->Html->link('Start Voting',array('controller'=>"drobes","action"=>"rate",0,"recent","rate_stream"),array('class'=>'btn btn-primary btn_txt'));
	SessionComponent::write('followUser',false);
//}
?>
</div>
</div>
<div class="spcaer"></div>
<script>
$('document').ready(function(){
	fleXenv.fleXcrollMain('friends_detail');
	attechInviteEvent();
	
});	
</script>	




