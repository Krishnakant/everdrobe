<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<h2>Edit Profile: <?php echo $this->data['User']['first_name']." ".$this->data['User']['last_name']." (".$this->data['User']['username'].")";?></h2>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
<?php 
echo $this->Html->link("Back to User Detail", array("controller"=>"users","action"=>"view","admin"=>true,$this->data['User']['id']))." ";
echo $this->Html->link("Change User Settings", array("controller"=>"users","action"=>"edit_settings","admin"=>true,$this->data['User']['id']))." ";
?>
</div>

<div class="page_8_middel">
    	<div class="page_8user left">
<?php 
if($this->data['User']['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$this->data['User']['photo']))
{
	echo $this->Html->image('/profile_images/thumb/'.$this->data['User']['photo'],array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
else
{
	echo $this->Html->image('/images/default_big.jpg',array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
if($this->data['User']['star_user']==1):?>
		<div class="star-user"></div>
<?php endif;?>
<div style="position: relative">
<?php 
echo $this->Html->link('Change profile picture','javascript:void(0)',array('id'=>'change_picture'));
?>
</div>
</div>
<div class="left" style="clear: none; width:500px">
<fieldset><legend>Edit User Profile</legend>
<?php
echo $this->Form->create('User');
echo $this->Form->hidden('photo');
echo $this->Form->input("username",array("placeholder"=>"Your User Name","readonly"=>"readonly"));
echo $this->Form->input("first_name",array("placeholder"=>"Your First Name"));
echo $this->Form->input("last_name",array("placeholder"=>"Your Last Name"));
echo $this->Form->input("email",array("placeholder"=>"Enter User login email id"));
echo $this->Form->label("Edit Your Bio");
echo $this->Form->textarea('bio');
?>
<div class="spacer"></div>

<div class="half left">
<?php 
echo $this->Form->input('UserTotal.current_ebx_point', array('before' => '<div>','after' => '</div>', 'between' => '', 'separator' => '</div><div> ','legend'=>false,"label"=>"EBX point",'type' => 'text'));
?>
</div>
<div class="spacer"></div>
<div class="half left">
<?php 
echo $this->Form->label("Edit Gender");
echo $this->Form->input('gender', array('before' => '<div>','after' => '</div>', 'between' => '', 'separator' => '</div><div> ','legend'=>false,"label"=>"Gender", 'options' => array('male'=>'Male','female'=> 'Female'),'type' => 'radio'));
?>
</div>
<div class="half right">
<?php
$year="";
$month="";
$day="";
if($this->data['User']['birth_date']!="" && $this->data['User']['birth_date']!="0000-00-00")
{
	list($year,$month,$day)=explode("-",$this->data['User']['birth_date']);
}

echo $this->Form->label("Edit Your Birthdate");
echo $this->Form->input('birth_month',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"MM", "value"=>$month,"style"=>"width:40px !important; clear: none !important;"));
echo $this->Form->input('birth_day',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"DD", "value"=>$day,"style"=>"width:40px !important; clear: none !important;"));
echo $this->Form->input('birth_year',array("label"=>false,"div"=>false, "class"=>"text_box small","placeholder"=>"YYYY", "value"=>$year,"style"=>"width:80px !important; clear: none !important;"));
?>
</div>
<div class="spacer"></div>
<div class="half left">
<?php
echo $this->Form->label("Edit Country");
echo $this->Form->input('country',array('label'=>false,'options'=>$countryList,'empty'=>'Select Country','style'=>"width:200px"));
?>
</div>
<div class="half right">
<?php 
echo $this->Form->label("Edit State/Province");
echo $this->Form->input('province',array('label'=>false,'options'=>array(),'empty'=>'Select State/Province','style'=>"width:200px"));

?>
</div>
<?php 
/*
?>
<div class="spacer"></div>
<?php
echo $this->Form->label("Website");
echo $this->Form->input('website',array('label'=>false,'style'=>"width:200px", "placeholder"=>"Enter user's website link"));
*/
?>

<div class="spacer"></div>
<?php 
echo $this->Form->input('status',array("options"=>array('new'=>"New Registered",'active'=>"Active User",'inactive'=>"Inactive User",'suspended'=>"Suspended User")));
echo $this->Form->hidden('id');
echo $this->Form->submit('Update Profile',array("class"=>"green_btn"));
echo $this->Form->end();
?>
</fieldset>
<div class="spacer"></div>
<div class="spacer"></div>

<fieldset><legend>Change Password for this User</legend>
<?php
echo $this->Form->create('User',array("url"=>array("action"=>"change_password","admin"=>true)));
echo $this->Form->input("new_password",array("placeholder"=>"Eneter New password","type"=>"password"));
echo $this->Form->input("confirm_password",array("placeholder"=>"Retype new password","type"=>"password"));
echo $this->Form->hidden('id');
echo $this->Form->submit('Change Password',array("class"=>"green_btn"));
echo $this->Form->end();
?>
</fieldset>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	var uploader = new qq.FileUploader({
			uploadButtonText: "Change User Photo",
		    element: document.getElementById('change_picture'),
		    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'profile',"admin"=>false)); ?>',
		    params: {type:'drobes'},
		    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
		    debug: true,
		    onSubmit: function(id, fileName){},
		    onProgress: function(id, fileName, loaded, total){},
		    onComplete: function(id, fileName, responseJSON){
			   upload_response(responseJSON);
			},
			onCancel: function(id, fileName){},
			messages: {},
			 showMessage: function(message){ alert(message); }
		});
		
	$('#file_upload_form').submit(function() { 
		$(this).ajaxSubmit({
					success :  function(responseText, statusText, xhr, $form)  { 
					    upload_response($.parseJSON(responseText));
					},  // post-submit callback 
				  	resetForm : true,
				  	clearForm : true,
			    	type: 'post'
				}); 
		return false; 
	}); 
	$('#change_picture').click(function(){
		$("#file_profile").show().click().hide().end();
	});
	$("#file_profile").change(function(){
		if($(this).val()!="")
		{
			$('#edit_profile input[type="submit"]').attr('disabled',true);
			//$("#file_upload_form").submit();
			alert("Submitting form");
			$("#file_upload_form").ajaxSubmit({
				success :  function(responseText, statusText, xhr, $form)  { 
				    var obj=jQuery.parseJSON(responseText);
			        
				},  // post-submit callback 
			  	resetForm : true,
			  	clearForm : true,
		    	type: 'post'
			}); 
		return false;
		}
	});
	$('#UserCountry option').each(function(){
		if($(this).attr('value')!="")
		{
			$(this).attr('id',$(this).attr('value'));
			$(this).attr('value',$(this).html());
		}
	});
	$('#UserCountry').change(function(){
		$('#UserProvince').load("<?php echo $this->Html->url(array('controller'=>"countries",'action'=>'get_state_list',"admin"=>false)); ?>/"+$(this).find('option:selected').attr('id'));
	})
	<?php 
		if(isset($this->data))
		{
			$country_name=$this->data['User']['country'];
			$province_name=$this->data['User']['province'];
		}
		else
		{
			$country_name=$userData['User']['country'];
			$province_name=$userData['User']['province'];
		}
	?>
	$('#UserCountry').val('<?php echo $country_name;?>');
	var option_id=$('#UserCountry').find('option:selected').attr('id');
	$('#UserProvince').load("<?php echo $this->Html->url(array('controller'=>"countries",'action'=>'get_state_list',"admin"=>false)); ?>/"+option_id,function(){
		$('#UserProvince').val('<?php echo $province_name;?>');
	});
});

function upload_response(obj)
{
	if(obj.type=="success")
    {
        $('#UserPhoto').val(obj.file);
        $('#profile_image').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('profile.thumb.upload_dir')).'/')?>" + obj.file);
    }
    else
    {  
    	alert(obj.message);
    } 
    $('#edit_profile input[type="submit"]').attr('disabled',false);
}
</script>
