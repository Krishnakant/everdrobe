<h2 class="text-center">Forgot Password</h2>
<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 text-center">
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
if(isset($message) && $message!="")
{
	echo "<div class='success'>".$message."</div>";
}
else if(isset($error_message) && $error_message!="")
{
	echo "<div class='error'>".$error_message."</div>";
}
echo $this->Form->create("User",array('action'=>"forgot_password"));
echo $this->Form->input("email",array("label"=>"Enter Email Id","class"=>"form-control"));
echo $this->Form->submit('Submit',array("class"=>"btn btn-success"));
echo $this->Form->end();
?>