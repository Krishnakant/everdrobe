<h2 class="text-center">Register</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="field_error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
else if(isset($email_used) &&  $email_used==true)
{
	?>
	<p>Entered email id <?php echo $email_id; ?> is already registred with Everdrobe. 
	If you forgot password click on <?php echo $this->Html->link('here',array("controller"=>'users','action'=>'forgot_password')); ?> 
	<?php 
}
?>
<?php //echo $this->element('register');?>
<script>
$(document).ready(function(){
	$(".user_register").show();
});
</script>
