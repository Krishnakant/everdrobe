<h2 class="text-center" ><?php if($is_me) echo  "My Followings"; 
else echo "Following of ".((isset($user['username']) && $user['username']) ? $user['username'] : $user['first_name']." ".$user['last_name']);
?></h2>
<div class="details search-box" style="background-color: #fff;">

<div class="input-group">
      <input type="text" class="search_input form-control input-lg" id="search_input" placeholder="Search by Name, Username or Keyword" />
      <div class="input-group-addon search-icon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
    </div>
</div>
<div class="spacer"></div>
<div class="container">
<div id='my_followers' class='follower_list infiniteScroll'>
   	<div class="long_content" id="following_list" last="1">
   	</div>
   	<div class="infiniteScrollLoading">Loading More Followings</div>   
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	activate_menu("profile");
	$('#following_list').attr('rel','<?php echo $this->Html->url(array('controller'=>"users",'action'=>"following_list",$user['unique_id']))?>').attr('page',0);
	$('#following_list').load('<?php echo $this->Html->url(array('controller'=>"users",'action'=>"following_list",$user['unique_id']))?>',function(){
        set_scrollbar();
    });
	function set_scrollbar()
    {
       	var selectedtab = '.infiniteScroll';
    	if($(selectedtab)[0].fleXcroll== undefined)
		{
		   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
		   apply_infinite_scrolling($(selectedtab).attr('id'));
		}
    }


	$('#search_input').keypress(function(event) {
	    if (event.which == 13) {
	        load_everdrobes($(this).val());
	    }
	});


	function load_everdrobes(search)
	{
		$('#following_list').attr('rel','<?php echo $this->Html->url(array('controller'=>"users",'action'=>"following_list",$user['unique_id']))?>/search:'+search).attr('page',0);
		$('#following_list').load('<?php echo $this->Html->url(array('controller'=>"users",'action'=>"following_list",$user['unique_id']))?>/search:'+search,function(){
	        set_scrollbar();
	    });

		
		  
	}
    
});
</script>