<h2>Send Broadcast Message</h2>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
<?php 
echo $this->Html->link("Back to Report", array("controller"=>"notification_queues","action"=>"report","admin"=>true));
?>
</div>
<div class="spacer"></div>

<?php 
echo $this->Form->create("User");
echo $this->Form->input("message",array("type"=>"textarea","maxlength"=>"120"));
?>
<div><small>Maximium length of message is 120 character</small></div>
<?php 
echo $this->Form->end("Submit");
?>

<script type="text/javascript">
$('document').ready(function(){

	$('#user_list').wrap('<div class="select_wrap"  multiple="multiple"></div>');
	$(".select_field").custSelectBox({
		isscrolling:true,
		selectwidth:"250px"
	});
	
	if($('#send_to_type option:selected').val()!="selected")
	{
		$('.select_wrap').parent().hide();
	}
	else $('.select_wrap').parent().show();
	$('#send_to_type').change(function(){
		if($(this).val()!="selected")
		{
			$('.select_wrap').parent().hide();
		}
		else $('.select_wrap').parent().show();
	});

	if($('#message_type option:selected').val()!="drobe")
	{
		$('#drobe_list').parent().hide();
		$('#display_type').parent().hide();
	}
	else 
	{
		$('#drobe_list').parent().show();
		$('#display_type').parent().show();
	}
	$('#message_type').change(function(){
		if($(this).val()=="drobe")
		{
			$('#drobe_list').parent().show();
			$('#display_type').parent().show();
		}
		else
		{
			$('#drobe_list').parent().hide();
			$('#display_type').parent().hide();
		}
	});
	
});


</script>