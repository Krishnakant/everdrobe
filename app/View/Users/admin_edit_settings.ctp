<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<h2>Edit Notification Settings: <?php echo $this->data['User']['first_name']." ".$this->data['User']['last_name']." (".$this->data['User']['username'].")" ;?></h2>
<div class="actions right" style="margin-right: 0; margin-right: 0;">
<?php 
echo $this->Html->link("Back to User Detail", array("controller"=>"users","action"=>"view","admin"=>true,$this->data['User']['id']))." ";
echo $this->Html->link("Back to Edit User Detail", array("controller"=>"users","action"=>"edit","admin"=>true,$this->data['User']['id']))." ";
?>
</div>
<div class="spacer"></div>
<div class="spacer"></div>
<div class="page_8_middel">
<?php 
echo $this->Form->create('User',array("id"=>"settings"));
echo $this->Form->hidden('UserSetting.id');
?>
<div class="page-8detail">
   	<div class="plus_head">Sharing Settings</div>
</div>
<table>
<tr align="center">
	<td><?php echo $this->Html->image('/images/facebook_icon.png');?></td>
	<td><?php echo $this->Html->image('/images/twitter_icon.png');?></td>
	<td></td>
</tr>
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.fb_post_drobe",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.tw_post_drobe",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Share User's Drobes</td>
</tr>
<tr  align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.fb_post_fave",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.tw_post_fave",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Share User's Faves</td>
</tr>
</table>
<div class="spacer"></div>
<div class='connection_info'>
<?php 
if(!$this->data['UserSetting']['fb_connected'] || $fb_connected_info==false)
{
?>
<div class="not_connected">Not linked with Facebook</div>
<a href="<?php echo $facebookUrl;?>" class="fb_link_btn connect simple_popup">Connect with Facebook</a>
<?php 
}
else
{
echo "<div class='left'>".$this->Html->image("http://graph.facebook.com/".$fb_connected_info['id']."/picture",array("height"=>"30px","width"=>"30px"))."</div>";
echo "<div class='left' style='margin-left:10px; width:160px'> <b>".$fb_connected_info['first_name']." ".$fb_connected_info['last_name']."</b><br/>".$fb_connected_info['email']."</div>";
echo $this->Html->link("Disconnect from Facebook",array("controller"=>"users","action"=>"disconnect_from_facebook",$this->data['User']['id'],1,"admin"=>false),array("class"=>"fb_link_btn disconnect right"));
}
?>
</div>
<div class='connection_info'>
<?php 
if(!$this->data['UserSetting']['tw_connected'] || $tw_connected_info==false)
{
?>
<div class="not_connected">Not linked with Twitter</div>
<a href="<?php echo $twitterLoginUrl;?>" class="tw_link_btn connect simple_popup">Connect with Twitter</a>
<?php 
}
else
{

	echo "<div class='left'>".$this->Html->image($tw_connected_info->profile_image_url,array("height"=>"30px","width"=>"30px"))."</div>";
	echo "<div class='left' style='margin-left:10px; width:160px'> <b>".$tw_connected_info->name."</div>";
	echo $this->Html->link("Disconnect from Twitter",array("controller"=>"users","action"=>"disconnect_from_twitter",$this->data['User']['id'],1,"admin"=>false),array("class"=>"tw_link_btn disconnect"));
}
?>
</div>
<div class="spacer"></div>
<div class="spacer"></div>

<div class="page-8detail">
   	<div class="plus_head">Notifications and Photos</div>
</div>
<table>
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.conversation_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Conversations</td>
</tr>
<tr  align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.got_comment_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">Got Comments</td>
</tr>
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.follow_post_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">User you follow post drobes</td>
</tr>
<tr align="center">
	<td width="20px"><?php 
	echo $this->Form->input("UserSetting.follower_notification",array("div"=>false,"label"=>false,"class"=>"required",'type'=>"checkbox"));
	?></td>
	<td align="left">User has a new follower</td>
</tr>

</table>
<div class="spacer"></div>
<div class="spacer"></div>
<?php 
echo $this->Form->submit("Save Changes",array("class"=>"green_btn"));
echo $this->Form->end();
?>

</div>
