<?php 
echo $this->Form->create('User',array("id"=>"edit_profile"));
?>
<div class="page-8detail">
   	<div class="plus_head">Edit Profile: <?php echo $this->data['User']['first_name']." ".$this->data['User']['last_name'];?></div>
</div>

<div class="page_8_middel">
    	<div class="page_8user">
<?php 
if($this->data['User']['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$this->data['User']['photo']))
{
	echo $this->Html->image('/profile_images/thumb/'.$this->data['User']['photo'],array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
else
{
	echo $this->Html->image('/images/default.jpg',array('width'=>150,'height'=>147,'border'=>0, 'id'=>"profile_image"));
}
?>
<div style="position: relative">
<?php 
echo $this->Html->link('Change profile picture','javascript:void(0)',array('id'=>'change_picture'));
?>
</div>
<?php 
//echo $this->Form->file('file',array('id'=>'file_upload'));
echo $this->Form->hidden('photo');
?>
<div class="spacer"></div>
<div class="spacer"></div>
<?php 
echo $this->Html->link('Change Email',array('controller'=>"users",'action'=>"change_email"),array("class"=>"blue_btn"));
echo $this->Html->link('Change Password',array('controller'=>"users",'action'=>"change_password"),array("class"=>"blue_btn"));
?>
<div class="spacer"></div>

</div>
<div class="profile_detail">
<?php
echo $this->Form->label("Edit Your Bio");
?>
<div class="clear"></div>
<?php 
echo $this->Form->textarea('bio');
?>

<div class="spacer"></div>
<?php
echo $this->Form->label("Edit Gender");
?>
<div class="clear"></div>
<?php 
//echo $userData['User']['first_name']." ".$userData['User']['last_name']."<br/><hr>";
echo $this->Form->input('gender', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Gender", 'options' => array('male'=>'Male','female'=> 'Female'),'type' => 'radio'));
$year="";
$month="";
$day="";
if($userData['User']['birth_date']!="" && $userData['User']['birth_date']!="0000-00-00")
{
	list($year,$month,$day)=explode("-",$userData['User']['birth_date']);
}
?>
<div class="spacer"></div>
<?php
echo $this->Form->label("Edit Your Birthdate");
?>
<div class="clear"></div>
<?php 
echo $this->Form->input('birth_month',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"MM", "value"=>$month,"style"=>"width:40px"));
echo $this->Form->input('birth_day',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"DD", "value"=>$day,"style"=>"width:40px"));
echo $this->Form->input('birth_year',array("label"=>false,"div"=>false, "class"=>"text_box small","placeholder"=>"YYYY", "value"=>$year,"style"=>"width:80px"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->label("Edit Country");
?>
<div class="clear"></div>
<?php 
echo $this->Form->input('country',array('label'=>false,'options'=>$countryList,'empty'=>'Select Country','style'=>"width:200px"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->label("Edit State/Province");
?>
<div class="clear"></div>

<?php 
echo $this->Form->input('province',array('label'=>false,'options'=>array(),'empty'=>'Select State/Province','style'=>"width:200px"));
?>
<div class="spacer"></div>
<?php
echo $this->Form->label("Edit Marital Status");
?>
<div class="clear"></div>

<?php 
echo $this->Form->input('relationship_status', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Gender", 'options' => array('none'=>"None", 'single'=>"Single", 'married'=>"Married", 'in_relationship'=>"In a Relationship"),'type' => 'radio'));
echo $this->Form->hidden('id');
echo $this->Form->submit('Update Profile',array("class"=>"green_btn"));
?>
</div>
</div>
<?php 
echo $this->Form->end();
?>
<script type="text/javascript">
$(document).ready(function(){
	var uploader = new qq.FileUploader({
			uploadButtonText: "Change your Photo",
		    element: document.getElementById('change_picture'),
		    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'profile')); ?>',
		    params: {type:'drobes'},
		    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
		    debug: true,
		    onSubmit: function(id, fileName){},
		    onProgress: function(id, fileName, loaded, total){},
		    onComplete: function(id, fileName, responseJSON){
			   upload_response(responseJSON);
			},
			onCancel: function(id, fileName){},
			messages: {},
			 showMessage: function(message){ alert(message); }
		});
		
	$('#file_upload_form').submit(function() { 
		$(this).ajaxSubmit({
					success :  function(responseText, statusText, xhr, $form)  { 
					    upload_response($.parseJSON(responseText));
					},  // post-submit callback 
				  	resetForm : true,
				  	clearForm : true,
			    	type: 'post'
				}); 
		return false; 
	}); 
	$('#change_picture').click(function(){
		$("#file_profile").show().click().hide().end();
	});
	$("#file_profile").change(function(){
		if($(this).val()!="")
		{
			$('#edit_profile input[type="submit"]').attr('disabled',true);
			//$("#file_upload_form").submit();
			alert("Submitting form");
			$("#file_upload_form").ajaxSubmit({
				success :  function(responseText, statusText, xhr, $form)  { 
				    var obj=jQuery.parseJSON(responseText);
			        
				},  // post-submit callback 
			  	resetForm : true,
			  	clearForm : true,
		    	type: 'post'
			}); 
		return false;
		}
	});
	$('#UserCountry option').each(function(){
		if($(this).attr('value')!="")
		{
			$(this).attr('id',$(this).attr('value'));
			$(this).attr('value',$(this).html());
		}
	});
	$('#UserCountry').change(function(){
		$('#UserProvince').load("<?php echo $this->Html->url(array('controller'=>"countries",'action'=>'get_state_list')); ?>/"+$(this).find('option[value='+$(this).val()+']').attr('id'));
	})
	<?php 
	if(isset($this->data))
	{
		$country_name=$this->data['User']['country'];
		$province_name=$this->data['User']['province'];
	}
	else
	{
		$country_name=$userData['User']['country'];
		$province_name=$userData['User']['province'];
	}
	?>
			$('#UserCountry').val('<?php echo $country_name;?>');
			var option_id=$('#UserCountry').find('option[value='+$('#UserCountry').val()+']').attr('id');
			$('#UserProvince').load("<?php echo $this->Html->url(array('controller'=>"countries",'action'=>'get_state_list')); ?>/"+option_id,function(){
				$('#UserProvince').val('<?php echo $province_name;?>');
			});
});

function upload_response(obj)
{
	if(obj.type=="success")
    {
        $('#UserPhoto').val(obj.file);
        $('#profile_image').attr('src',"<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('profile.thumb.upload_dir')).'/')?>" + obj.file);
    }
    else
    {  
    	alert(obj.message);
    } 
    $('#edit_profile input[type="submit"]').attr('disabled',false);
}
</script>
