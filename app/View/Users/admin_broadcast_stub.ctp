<h2>Send Broadcast Message</h2>
<?php 
echo $this->Form->create("User");
echo $this->Form->input("user_type",array("options"=>array("featured_all"=>"Display Featured Drobe, Send to All User","featured_one"=>"Display Featured Drobe, Send to One User","search_all"=>"Display Search User, Send to All User","search_one"=>"Display Search User, Send to One User")));
?>
<div class="spacer"></div>
<?php 
echo $this->Form->input("message",array('label'=>"Message <small>(Max length: 160 chars)</small>","type"=>"textarea"));
echo $this->Form->End("Submit");
?>