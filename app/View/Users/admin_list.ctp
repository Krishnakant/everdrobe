<h2>Everdrobe User Statistics</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Seach User : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Enter Email, First Name or Last Name" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('User.id','#');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name','Full Name');?></th>
			<th><?php echo $this->Paginator->sort('User.username','User Name');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.total_drobes','Total Drobes');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.vote_given','Rate Given');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.vote_received','Rate Received');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.total_favourites','Total Favs');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.current_ebx_point','Current EBX');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.earned_ebx_point','Earned EBX');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.followings','Followings');?></th>
			<th><?php echo $this->Paginator->sort('UserTotal.followers','Followers');?></th>
			<th>Cute</th>
			<th>Hot</th>
		</tr>
		<?php 
		foreach($data as $user)
		{
			?>
			<tr>
				<td><?php echo $user['User']['id'];?></td>
				<td><?php echo $this->Html->link($user['User']['first_name'].' '.$user['User']['last_name'],array("controller"=>"users","action"=>"view",$user['User']['id'],"admin"=>true)); ?></td>
				<td><?php echo $user['User']['username'];?></td>
				<td><?php echo $user['UserTotal']['total_drobes'];?></td>
				<td><?php echo $user['UserTotal']['vote_given'];?></td>
				<td><?php echo $user['UserTotal']['vote_received'];?></td>
				<td><?php echo $user['UserTotal']['total_favourites'];?></td>
				<td><?php echo $user['UserTotal']['current_ebx_point'];?></td>
				<td><?php echo $user['UserTotal']['earned_ebx_point'];?></td>
				<td><?php echo $user['UserTotal']['followings'];?></td>
				<td><?php echo $user['UserTotal']['followers'];?></td>
				<td><?php echo intval($user['UserTotal']['total_in']);?></td>
				<td><?php echo intval($user['UserTotal']['total_out']);?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
	<?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="user_options" style="display: none;">
	<ul class="submenu actions">
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"view","admin"=>true)); ?>  class="view_detail_link">View Detail</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"edit","admin"=>true)); ?>  class="remove_link">Edit Detail</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","block","admin"=>true)); ?> onclick="return confirm('Are you sure to block this user?');" class="block_link">Block</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","activate","admin"=>true)); ?> onclick="return confirm('Are you sure to activate this user?');" class="activate_link">Activate</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","deactivate","admin"=>true)); ?> onclick="return confirm('Are you sure to deactivate this user?');" class="deactivate_link">Deactivate</a></li>
	</ul>
	<div class="clear"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	});
});
</script>