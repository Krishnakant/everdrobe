<h2>Forgot Password</h2>
<?php 
echo $this->Html->link('Back to Login',array('controller'=>'admins','action'=>'login','admin'=>true),array('class'=>'right'));
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
if(isset($message) && $message!="")
{
	echo $message;
}
else if(isset($error_message) && $error_message!="")
{
	echo $error_message;
}
echo $this->Form->create("Admin",array('action'=>"forgot_password"));
echo $this->Form->input("email",array("label"=>"Enter Email Id","class"=>"text_box"));
echo $this->Form->submit('Submit');
echo $this->Form->end();
?>