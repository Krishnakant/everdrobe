<h2>Dashboard</h2>
<div class="dashboard">
<h3>Earning Statistics</h3>
<div>
<table>
<tr>
<th style="text-align:center; font-size: medium;">Total Revenue</th>
<th style="text-align:center; font-size: medium;">Seller Revenue</th>
<th style="text-align:center; font-size: medium;">Paid to Seller</th>
<th style="text-align:center; font-size: medium;">Profit</th>
<th style="text-align:center; font-size: medium;">Balance</th>
</tr>
<tr>
<td style="text-align:center; font-size: medium;">$<?php echo round(floatval($earnings['total_earnings']),2); ?></td>
<td style="text-align:center; font-size: medium;">$<?php echo round(floatval($earnings['seller_earnings']),2); ?></td>
<td style="text-align:center; font-size: medium;">$<?php echo round(floatval($payments),2); ?></td>
<td style="text-align:center; font-size: medium;">$<?php echo round(floatval($earnings['total_earnings']-$earnings['seller_earnings']),2); ?></td>
<td style="text-align:center; font-size: medium;">$<?php echo round(floatval($earnings['seller_earnings']-$payments),2); ?></td>
</tr>
</table>
</div>
<div class="spacer"></div>
<h3>Drobe Statistics</h3>
<div>
<table>
<tr>
<th style="text-align:center; font-size: medium;">Total Drobes</th>
<th style="text-align:center; font-size: medium;">Drobe for Sale</th>
<th style="text-align:center; font-size: medium;">Featured Drobes</th>
<th style="text-align:center; font-size: medium;">Sold Drobes</th>
<th style="text-align:center; font-size: medium;">Closed Drobes</th>
<th style="text-align:center; font-size: medium;">Flagged Drobes</th>
</tr>
<tr>
<td style="text-align:center; font-size: medium;"><?php echo $total_drobes; ?></td>
<td style="text-align:center; font-size: medium;"><?php echo $sell_drobes; ?></td>
<td style="text-align:center; font-size: medium;"><?php echo $featured_drobes;?></td>
<td style="text-align:center; font-size: medium;"><?php echo $sold_drobes;?></td>
<td style="text-align:center; font-size: medium;"><?php echo $closed_drobes;?></td>
<td style="text-align:center; font-size: medium;"><?php echo $flagged_drobes;?></td>
</tr>
</table>
</div>

<div class="spacer"></div>
<h3>User Statistics</h3>
<div>
<table>
<tr>
<th style="text-align:center; font-size: medium;">Total Users</th>
<th style="text-align:center; font-size: medium;">Active Users</th>
<th style="text-align:center; font-size: medium;">New Users</th>
<th style="text-align:center; font-size: medium;">Seller Users</th>
<th style="text-align:center; font-size: medium;">Flagged Users</th>
</tr>
<tr>
<td style="text-align:center; font-size: medium;"><?php echo $total_users; ?></td>
<td style="text-align:center; font-size: medium;"><?php echo $active_user; ?></td>
<td style="text-align:center; font-size: medium;"><?php echo $new_user;?></td>
<td style="text-align:center; font-size: medium;"><?php echo $seller_users;?></td>
<td style="text-align:center; font-size: medium;"><?php echo $flagged_users;?></td>
</tr>
</table>
</div>

<div class="spacer"></div>
<h3>EBX Statistics</h3>
<div>
<table>
<tr>
<th style="text-align:center; font-size: medium;">Total EBX</th>
<th style="text-align:center; font-size: medium;">Redeemed EBX</th>
<th style="text-align:center; font-size: medium;">Balance EBX</th>
</tr>
<tr>
<td style="text-align:center; font-size: medium;"><?php echo $total_ebx; ?></td>
<td style="text-align:center; font-size: medium;"><?php echo ($total_ebx-$balance_ebx); ?></td>
<td style="text-align:center; font-size: medium;"><?php echo $balance_ebx;?></td>
</tr>
</table>
</div>

<div class="spacer"></div>

<div id="new_drobe" style="height: 300px;"></div>
<div class="spacer"></div>

<div id="new_user" style="height: 300px;"></div>
<?php 
$dates=array();
$new_drobe_data=array();
foreach($drobe_stats as $stats)
{
	$new_drobe_data[date('M-d',strtotime($stats[0]['date_day']))]=intval($stats[0]['total']);
}
?>
<div class="spacer"></div>
<div class="spacer"></div>

<?php 
$dates=array();
$new_user_data = array();
foreach($user_stats as $stats)
{
	$new_user_data[date('M-d',strtotime($stats[0]['date_day']))] = intval($stats[0]['total']); 
}
?>

<div class="spacer"></div>
<div class="spacer"></div>
<div id="vote_statastic" style="height: 300px;"></div>
<div class="spacer"></div>
<?php 
$dates=array();
$vote_statastic = array();
foreach($rate_stats as $stats)
{
	$vote_statastic[date('M-d',strtotime($stats[0]['date_day']))] = intval($stats[0]['total']);
}
?>
<div class="spacer"></div>
<div class="new_user box left">
<h3>Last 10 New Registrations</h3>
<div class="flexcroll" style="height: 200px; overflow: auto;">
<?php 
if(count($new_users)>0)
{
	?>
	<table>
	<tr>
			<th>Full Name</th>
			<th>Email</th>
		</tr>
		<?php 
		foreach($new_users as $user)
		{
			?>
			<tr class="<?php echo $user['User']['id']; ?>">
				<td><?php echo $user['User']['first_name'].' '.$user['User']['last_name']." (".$user['User']['username'].")" ;?></td>
				<td><?php echo $user['User']['email'];?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
}
?>
</div>
</div>
<div class="new_drobes box right">
<h3>Last 10 New Drobes</h3>
<div class="flexcroll" style="height: 200px; overflow: auto;">
<?php 
if(count($new_drobes)>0)
{
	?>
	<table>
		<thead>
			<tr>
				<th>Post</th>
				<th>Posted_by</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach($new_drobes as $drobe)
			{
				?>
				<tr>
					<td><?php echo $drobe['Drobe']['comment'];?></td>
					<td><?php echo $drobe['User']['first_name'].' '.$drobe['User']['last_name']." (".$drobe['User']['username'].")" ;?></td>
				</tr>
				<?php 
			}
			?>
		</tbody>
	</table>
	<?php 
}
?>
</div>
</div>
<div class="spacer"></div>
<div class="new_drobes box" style="width:auto;">
<h3>Top Active Users from last 24 hours</h3>
<div class="flexcroll" style="height: 200px; overflow: auto;">
<?php 
if(count($active_users)>0)
{
	?>
	<table>
		<thead>
			<tr>
				<th>User Name</th>
				<th>User Email</th>
				<th>Rate given</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach($active_users as $user)
			{
				?>
				<tr>
					<td><?php echo $this->Html->link($user['User']['first_name'].' '.$user['User']['last_name']." (".$user['User']['username'].")",array("controller"=>"users","action"=>"view",$user['User']['id'],"admin"=>true)); ?></td>
					<td><?php echo $user['User']['email'];?></td>
					<td><?php echo $user[0]['total'];?></td>
				</tr>
				<?php 
			}
			?>
		</tbody>
	</table>
	<?php 
}
?>
</div>
</div>
</div>

<script>
$(document).ready(function()
{
	$('#new_drobe').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'New Drobe Statastics'
        },
        xAxis: {
            categories: <?php echo json_encode(array_keys($new_drobe_data));?>
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
       legend:false,
        plotOptions: {
            column: {
                pointPadding: 0.1,
                borderWidth: 0
            }
        },
        series: [{
            name: 'New Drobe',
            data: <?php echo json_encode(array_values($new_drobe_data)); ?>

        }]
    });



	$('#new_user').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'New User Statastics'
        },
        xAxis: {
            categories: <?php echo json_encode(array_keys($new_user_data));?>
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
       legend:false,
        plotOptions: {
            column: {
                pointPadding: 0.1,
                borderWidth: 0
            }
        },
        series: [{
            name: 'New User',
            data: <?php echo json_encode(array_values($new_user_data)); ?>

        }]
    });



	$('#vote_statastic').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'All Drobe Vote Statastics'
        },
        xAxis: {
            categories: <?php echo json_encode(array_keys($vote_statastic));?>
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
       legend:false,
        plotOptions: {
            column: {
                pointPadding: 0.1,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Votes',
            data: <?php echo json_encode(array_values($vote_statastic)); ?>

        }]
    });
    

	$(".highcharts-button").remove();
	$("#new_drobe tspan:last").remove();
	$("#new_user tspan:last").remove();
	$("#vote_statastic tspan:last").remove();
});
</script>