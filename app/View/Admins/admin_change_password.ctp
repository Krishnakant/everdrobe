<h2>Change Password</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
if(isset($message) && $message!="")
{
	echo $message;
}
else if(isset($error_message) && $error_message!="")
{
	echo $error_message;
}
echo $this->Form->create("Admin",array('action'=>"change_password"));
echo $this->Form->input("current_password",array("type"=>"password","label"=>"Current Password","class"=>"text_box"));
echo $this->Form->input("new_password",array("type"=>"password","label"=>"New Password","class"=>"text_box"));
echo $this->Form->input("confirm_password",array("type"=>"password","label"=>"Confirm Password","class"=>"text_box"));
echo $this->Form->submit('Submit');
echo $this->Form->end();
?>