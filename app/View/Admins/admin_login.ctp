<h2>Admin Login</h2>
<?php 
echo $this->Form->create('Admin');
echo $this->Form->input('username');
echo $this->Form->input('password');
echo $this->Html->link('Forgot password?',array('controller'=>'admins','action'=>'forgot_password','admin'=>true));
echo $this->Form->submit('Login');
echo $this->Form->end();
?>