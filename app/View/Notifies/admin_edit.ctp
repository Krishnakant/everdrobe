<h2>Edit : <?php echo Inflector::humanize($this->data['Notify']['action']);?></h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="spacer"></div>
<?php

echo $this->Form->create("Notify");
echo $this->Form->hidden("action");
echo $this->Form->input("subject");
echo $this->Form->input("message");
//echo $this->Form->label("Message Type");
//echo $this->Form->input('text_type', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Message Type",'options' => array("html"=>'Html Format', 'text'=>"Plain Text"),'type' => 'radio','default'=>'html'));
//echo $this->Form->input('type', array('legend'=>false, 'options' => array('email'=>"Email", 'facebook'=>"Facebook  Message", 'twitter'=>"Twitter Message"),'type' => 'select'));
echo $this->Form->hidden('id');
?>
<div class="spacer"></div>
<?php

echo $this->Form->submit('Update');
echo $this->Form->end();
if($this->data['Notify']['text_type']=="html")
{
?>
<script type="text/javascript">
		//<![CDATA[
			// Replace the <textarea id="editor1"> with an CKEditor instance.
			var editor = CKEDITOR.replace( 'NotifyMessage' , {

		        filebrowserBrowseUrl : '<?php echo Router::url("/ckeditor/ckfinder/ckfinder.html",true);?>',
		        filebrowserImageBrowseUrl : '<?php echo Router::url("/ckeditor/ckfinder/ckfinder.html?Type=Images",true);?>',
		        filebrowserFlashBrowseUrl : '<?php echo Router::url("/ckeditor/ckfinder/ckfinder.html?Type=Flash",true);?>',
		        filebrowserUploadUrl : '<?php echo Router::url("/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files",true);?>',
		        filebrowserImageUploadUrl : '<?php echo Router::url("/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images",true);?>',
		        filebrowserFlashUploadUrl : '<?php echo Router::url("/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash",true);?>',
			    filebrowserWindowWidth  : 800,
			    filebrowserWindowHeight : 500,
			    toolbar : 'Mail'
			});
		//]]>
</script>
<?php 
}
?>