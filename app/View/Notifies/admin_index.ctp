<h2>Notify List</h2>
<div class="actions right">
<?php echo $this->Html->link('Add New Notifications',array('controller'=>'notifies','action'=>'add','admin'=>true))?>
</div>
<div class="spacer"></div>
<?php
if(count($notifyList)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th width="20%">Action</th>
			<th>Subject</th>
			<th width="12%">Send by</th>
			<th  width="20%">Actions</th>
		</tr>
		<?php 
		foreach($notifyList as $notify)
		{
			?>
			<tr>
				<td><?php echo $notify['Notify']['id'];?></td>
				<td><?php echo Inflector::humanize($notify['Notify']['action']);?></td>
				<td><?php echo $notify['Notify']['subject'];?></td>
				<td><?php echo $notify['Notify']['type'];?></td>
				<td>
				<div class="actions">
				<?php 
					echo $this->Html->link('Edit',array('controller'=>'notifies','action'=>'edit',$notify['Notify']['id'],'admin'=>true)). " "; 
					echo $this->Html->link('Delete',array('controller'=>'notifies','action'=>'delete',$notify['Notify']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'));
				?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>