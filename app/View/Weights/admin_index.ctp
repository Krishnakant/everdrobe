<h2>Weight List</h2>
<div class="actions right">
<?php 
echo $this->Html->link('Add New Weight',array('controller'=>'weights','action'=>'add','admin'=>true),array("class"=>"button"))?>
</div>
<div class="spacer"></div>
<?php
if(count($data)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th width="150px"><?php echo $this->Paginator->sort('Weight.weight','Weight of Label');?></th>
			<th><?php echo $this->Paginator->sort('Weight.price','Label Price');?></th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($data as $weight)
		{
			?>
			<tr>
				<td><?php echo $weight['Weight']['id'];?></td>
				<td><?php echo $weight['Weight']['weight'];?></td>
				<td><?php echo $weight['Weight']['price'];?></td>
				<td><?php echo $weight['Weight']['status'];?></td>
				<td>
				<div class="actions">
					<?php 
						echo $this->Html->link('Edit',array('controller'=>'weights','action'=>'edit',$weight['Weight']['id'],'admin'=>true)). " "; 
						echo $this->Html->link('Delete',array('controller'=>'weights','action'=>'delete',$weight['Weight']['id'],'admin'=>true),array('confirm'=>'Are you sure to delete this?'))."&nbsp;&nbsp;&nbsp;";						
					?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
