<?php 
if(!isset($invalid_friend))
{   
	?>
	<div class="right" style="margin: 0; padding: 0">
	<a class="red_btn" id="confirm" href="javacript:void(0)"  style="color: #FFFFFF; font-size: 12px; padding: 2px 5px; margin: 10px" >Clear Chat</a>
	</div>
	<h2>Chat with <?php echo $friend['username'];?></h2>
	<div class="line"></div>
	<div class="spacer"></div>
	<div id='conversations_area' class="conversations_area chatting_area">
	   	<div class="long_content" id="friend_conversations">
	   	</div>
   	</div>
	<div class="spacer"></div>
	<div class="chat_send">
        <textarea cols="43" rows="1" class="chat" id="replychatarea" placeholder="Post your reply here"></textarea>
    	<a href="javascript:void(0)" class="send_btn" id="replychatbtn">SEND</a>
    </div>
    
	<script type="text/javascript">
	var friend_id='<?php echo $friend['unique_id'];?>';
	$(document).ready(function(){
		fleXenv.fleXcrollMain($('.conversations_area').attr('id'));
		$('#friend_conversations').load('chattings/view/'+friend_id,function(){
			set_conversation_scrollbar();
	    });

		$('#confirm').click(function() {
			Confirm('Are you sure to remove this chat?', function(yes) {
				if(yes)
				{
					$.ajax({
						url: 'chattings/close',
						type: "POST",
						data: {
							friend_id: friend_id
						},
						success: function(data) {
							data=$.parseJSON(data);
							$.unblockUI();
							if(data.type=="success")
						  	{
							  	$("#friend_conversations").html("");
							  	set_conversation_scrollbar();
						  		//document.location.href='users/following';
						  	}
							else
							{
								alert(data.message);
							} 
					  	}
					});
				}
			});
			return false;
		});
	});
	
	</script>
		
	<?php 
}
else
{
	
}
?>