<div class="right-panel <?php /*if($this->params->params['action']=="register") echo 'hide';*/ ?>" id="user_login">
        <h5 class="text-center"> Login Using</h5>
       <div class="text-center mt10">
        <?php echo $this->Html->link("",$facebookUrl,array("class"=>"fb_login simple_popup")); ?>
        <?php echo $this->Html->link("",$gmailAuthUrl,array("class"=>"gmail_login simple_popup")); ?>
        </div>
        <div class="clearfix"></div>
        <div class="or"><span>OR</span></div>
        <h5 class="text-center">Use your Everdrobe Account</h5>
        <?php 
        	echo $this->Form->create("User",array("id"=>"login_form",'url'=>array('action'=>"login",'?'=>$this->params->query))); ?>
       	  <div class="random form-group">
       	  <?php 
       	   echo $this->Form->input("email",array("label"=>false,"div"=>false,"placeholder"=>"Email/Username","class"=>"form-control"));
       	   ?>
       	   </div>
	       <div class="random form-group">
           <?php 
       	   echo $this->Form->input("password",array("label"=>false,"div"=>false,"placeholder"=>"Password","class"=>"form-control"));
       	   ?>
       	   </div>
         	<div class="random">
            	<label><input type="checkbox" name="checkbox" id="checkbox" /> Remember</label>
                <div class="clearfix"></div>
            	<?php echo $this->Html->link("Forgot Password?",array("controller"=>"users","action"=>"forgot_password"),array("class"=>"green"))?>
            </div>
            <div class="random">
            <?php echo $this->Form->button('Login',array('class'=>"login"))?>
            <!-- <a href="#" class="login"></a> -->
            </div>
            <!--<div class="line" ></div>
            <div class="para">Not yet a Everdrobe member? </div>
            <div class="random"><a href="javascript:void(0)" class="signup sign_up"></a></div>-->
       <?php 
       	echo $this->Form->end(); 
       ?>
    </div>
    <script>
    	$(".login_action > a").click(function() {
            $(".login_form").toggle();
			$(".user_register").hide();
        });
    </script>
