<?php 

$profileDetail=$this->Session->read('Auth.User');
$drobeInfo=$this->requestAction(array('controller'=>"users","action"=>"my_totals"));
?>
<div id="user_profile">
	<div class="header-user-box ">
	
    <div class="user-photo">
	<?php 
if(isset($profileDetail['photo']) && $profileDetail['photo']!=null && is_file(WWW_ROOT."profile_images".DS.$profileDetail['photo']))
{
	echo $this->Html->image('/profile_images/thumb/'.$profileDetail['photo'],array('width'=>70,'border'=>0, 'id'=>"profile_image"));
}
else
{
	echo $this->Html->image('/images/default.jpg',array('width'=>70,'height'=>68,'border'=>0, 'id'=>"profile_image"));
}
if($profileDetail['star_user']==1):?>
	<div class="star-user"></div>
<?php endif;?>
	</div>
	<div class="hidden-sm hidden-xs ">
	<div class="user_name"><?php echo ((isset($profileDetail['username']) && $profileDetail['username'] != null) ? $profileDetail['username'] : $profileDetail['first_name']. " " .$profileDetail['last_name']); ?></div>
	<div class="detail">
	<?php 
	//if(!empty($profileDetail['relationship_status'])) echo Inflector::humanize($profileDetail['relationship_status']);
	if(!empty($profileDetail['gender'])) echo Inflector::humanize($profileDetail['gender']);
	if(!empty($profileDetail['country'])) echo ", ".Inflector::humanize($profileDetail['country']).". ";
	?>
	</div>
	
	</div>
    
    <div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="bio_right_table full_width text-left">
        	<table class="user-menu" width="100%" cellspacing="0" cellpadding="0" border="0">
            	<tr>
                	<td class="text-center"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
                    <td><?php echo $this->Html->link("Profile",array("controller"=>"users","action"=>"edit_profile"));?></td>
                </tr>
              <tr>
                
                <td class="text-center"><?php echo $drobeInfo['total_in']?></td>
                <td class=""> Cute</td>
              </tr>
              <tr>
                
                <td class="text-center"><?php echo $drobeInfo['total_out']?></td>
                <td class="">Hot</td>
              </tr>
              <tr>
                
                <td class="text-center"><?php echo $drobeInfo['vote_received']?></td>
                <td class="">Vote Received</td>
              </tr>
               <tr>
                
                <td class="text-center"><?php echo $this->Html->convertToReadeable($drobeInfo['vote_given']);?></td>
                <td class="">Votes Given </td>
              </tr>
              <tr>
                
                <td class="text-center"><?php echo $this->Html->convertToReadeable($drobeInfo['current_ebx']);?></td>
                <td class="">EBX Balance </td>
              </tr>
              <tr>
                
                <td class="text-center"><?php echo $this->Html->convertToReadeable($drobeInfo['total_earned_ebx']);?></td>
                <td class="">Earned EBX </td>
              </tr>
              <!--  <tr>
                <td class="gry_td">Total Everbucks : </td>
                <td class="gry_td"><?php //echo $this->Html->convertToReadeable($drobeInfo['total_earned_ebx']);?></td>
              </tr>
              <tr>
                <td class="white_td">Available Everbucks : </td>
                <td class="white_td"><?php //echo $this->Html->convertToReadeable($drobeInfo['current_ebx']);?></td>
              </tr>
              <tr>
                <td class="gry_td">Total Votes : </td>
                <td class="gry_td"><?php //echo $this->Html->convertToReadeable($drobeInfo['vote_given']);?></td>
              </tr>  -->
            </table>
        </div>
	<div class="clear"></div>
	<div class="spacer"></div>
	<div>
	<?php 
	//$content_pages=Cache::read('content_pages');
//echo $content_pages['google_adsense']['page_content']; 
?>
	</div>
</div>
