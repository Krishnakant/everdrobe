<div id="fix">
    	<div class="left-link">
        	<div class="flink">	<?php echo $this->Html->link("Everdrobe &reg;",array("controller"=>"drobes","action"=>"index","admin"=>false),array("class"=>"grey",'escape' => false));?></div>
        	<div class="flink"><a href="#" class="grey1">|</a></div>
        	<div class="flink"><?php echo $this->Html->link("Unique Gifts","http://www.oddgifts.com",array("class"=>"grey"));?></div>
        	<div class="flink"><a href="#" class="grey1">|</a></div>
            <div class="flink"><?php echo $this->Html->link("Contact",array("controller"=>"contacts","action"=>"index","admin"=>false),array("class"=>"grey"));?></div>
            <div class="flink"><a href="#" class="grey1">|</a></div>
            <div class="flink"><?php echo $this->Html->link("About Us",array("controller"=>"pages","action"=>"display","about-us","admin"=>false),array("class"=>"grey"));?></div>
            <div class="flink"><a href="#" class="grey1">|</a></div>
            <div class="flink"><?php echo $this->Html->link("Help","http://help.everdrobe.com",array("class"=>"grey"));?></div>
            <div class="flink"><a href="#" class="grey1">|</a></div>
            <div class="flink"><?php echo $this->Html->link("Blog","http://www.everdrobe.com/blog/",array("class"=>"grey"));?></div>
            <div class="flink"><a href="#" class="grey1">|</a></div>
            <div class="flink"><?php echo $this->Html->link("Tumblr","http://tumblr.everdrobe.com/",array("class"=>"grey"));?></div>
            <div class="flink"><a href="#" class="grey1">|</a></div>
            <div class="flink"><?php echo $this->Html->link("Terms",array("controller"=>"pages","action"=>"display","terms","admin"=>false),array("class"=>"grey"));?></div>
            <div class="flink"><a href="#" class="grey1">|</a></div>
            <div class="flink"><?php echo $this->Html->link("Privacy",array("controller"=>"pages","action"=>"display","privacy","admin"=>false),array("class"=>"grey"));?></div>
            
        </div>
        <div class="copyright"><span class="grey1">Everdrobe, Inc &copy; 2014-2015 Patent Pending</span></div>
    </div>
