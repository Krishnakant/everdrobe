<?php

$gender_filter=$userFilter['gender'];
$only_new_brand = $userFilter['only_new_brand'];
$sort_by_highest_rated = $userFilter['highest_rated'];
if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="")
{
	$badge_counter= count(explode(",",$userFilter['category_ids']));
}
else
{
	if(in_array($gender_filter, array("male","female")))
	{
		$badge_counter=1;
	}
	else
	{
		$badge_counter=0;
	}
}

?><div class="filter_drobe">

	<div class="blue_btn" id="stream_button">
	<?php 
	
	if($badge_counter>0 && $userFilter['category_ids']!="ALL" )
	{
	?>
	<a class="badges"><?php echo $badge_counter; ?></a>
	<?php 
	}
	?>
	Filter by Style
	</div>

<div class="filter_input">


	<div id="TabbedPanels2" class="TabbedPanels">
		  <ul class="TabbedPanelsTabGroup">
		    <li class="TabbedPanelsTab" tabindex="0">Category</li>
		    <li class="TabbedPanelsTab" tabindex="0">Size</li>
		    <li class="TabbedPanelsTab" tabindex="0">Brand</li>
		    <li class="TabbedPanelsTab" tabindex="0">Other</li>
	      </ul>
		  <div class="TabbedPanelsContentGroup">
		    <div class="TabbedPanelsContent">
            		<div class="categories flexcroll" id="categorylist" style="height: 200px; overflow: auto;">
            		<div>
            		<div class="spacer"></div>
	<div class="left">
		<label><input type="checkbox" id="check-all" <?php echo ($userFilter['category_ids']=="ALL") ? "checked": "" ;?> ><b>Select All Categories</b></label>
	</div>
		<div class="clear"></div>
	<?php
	$categories=Cache::read('drobe_category');
	$i=1;
	
	foreach ($categories as $key=>$category)
	{
		if(trim($category)=="") continue;
		?>
			<div class="list">
			<label><input type="checkbox" value="<?php echo $key;?>" <?php echo ($userFilter['category_ids']=="ALL" || in_array($key, explode(",",$userFilter['category_ids']))) ? "checked" : "" ; ?> /><?php echo $category; ?></label>
			</div>
			<?php
			if($i==0)
			{
				?>
				<div class="clear"></div>
				<?php 
			}
			$i=($i+1)%3; 
	}
		?>
            		</div> 
            		</div>
           	</div>
		    <div class="TabbedPanelsContentFilter">
            		<div class="sizes flexcroll" id="sizelist">
            		<div class="spacer"></div>
			<div class="left">
			<label><input type="checkbox" id="check_all_sizes" <?php echo ($userFilter['size_name']=="{ALL}") ? "checked": "" ;?> ><b>Select All Sizes</b></label>
			
			</div>
		<div class="clear"></div>
		<ul class="size_list">
			<?php 
				$size_names = Cache::read('size_list');
				
				foreach ($size_names as $key=>$size)
				{
					if(trim($size)=="") continue;
					?>
						<li class="list">
						<label><input type="checkbox" value="<?php echo $size;?>" <?php echo ($userFilter['size_name']=="{ALL}" || in_array($size, explode("||",$userFilter['size_name']))) ? "checked" : "" ; ?> /><?php echo $size; ?></label>
						</li>
					<?php 
				}
			?>
		</ul>
		<div class="clear"></div>
            		</div> 
            </div>
            <div class="TabbedPanelsContent">
            		<div class="brands flexcroll" id="brandlist">
            		<div class="spacer"></div>
			<div class="left">
			<label><input type="checkbox" id="check_all_brands" <?php echo ($userFilter['brand_name']=="{ALL}") ? "checked": "" ;?> ><b>Select All Brands</b></label>
			</div>
		<div class="clear"></div>
		<ul class="brand_list">
			<?php 
				$brand_names = Cache::read('brand_list');
				foreach ($brand_names as $key=>$brand)
				{
					if(trim($brand)=="") continue;
					?>
						<li class="list">
						<label><input type="checkbox" value="<?php echo $brand;?>" <?php echo ($userFilter['brand_name']=="{ALL}" || in_array($brand, explode("||",$userFilter['brand_name']))) ? "checked" : "" ; ?> /><?php echo $brand; ?></label>
						</li>
					<?php 
				}
			?>
		</ul>
		<div class="clear"></div>
            		</div> 
            </div>
           <div class="TabbedPanelsContent">
            	<div class="other flexcroll" id="otherlist">
            	<div class="spacer"></div>
		<div class="left">
		<b>Filter By Gender</b>
		<div class="clear"></div>
		<label><input type="radio" value="both" name="filter_gender" <?php if(!in_array($gender_filter, array("male","female"))) echo "checked='checked'"; ?>/>Male or Female</label>
		<label><input type="radio" value="male" name="filter_gender" <?php if($gender_filter=="male") echo "checked='checked'"; ?>/>Male</label>
		<label><input type="radio" value="female" name="filter_gender" <?php if($gender_filter=="female") echo "checked='checked'"; ?> />Female</label>
		</div>
		
		<div class="spacer"></div>
		<div class="left">
		<b>Is Brand New</b>
		<div class="clear"></div>
		<label><input type="radio" value="new" name="only_new_brand" <?php if($only_new_brand=="new") echo "checked='checked'"; ?>/>New</label>
		<label><input type="radio" value="used" name="only_new_brand" <?php if($only_new_brand=="used") echo "checked='checked'"; ?>/>Used</label>
		<label><input type="radio" value="both" name="only_new_brand" <?php if(!in_array($only_new_brand,array("new","used"))) echo "checked='checked'"; ?> />Both</label>
		</div>
		
		<div class="spacer"></div>
		<div class="left">
		<b>Sort By Highest Rated</b>
		<div class="clear"></div>
		<label><input type="radio" value="1" name="highest_rated" <?php if($sort_by_highest_rated=="1") echo "checked='checked'"; ?>/>On</label>
		<label><input type="radio" value="0" name="highest_rated" <?php if($sort_by_highest_rated=="0") echo "checked='checked'"; ?>/>Off</label>
		</div>
		
		<div class="prices" >
		<div class="spacer"></div>
		<div class="left">
		<label><input type="checkbox" id="check_all_prices" <?php echo ($userFilter['price_range']=="{ALL}") ? "checked": "" ;?> ><b>Select All Prices</b></label>
		</div>
		<div class="clear"></div>
		<ul class="price_list">
		<?php 
		$price_range=array();
		$all_price="";
		if($userFilter['price_range']=="{ALL}")
		{
			$all_price=$userFilter['price_range'];
		}	
		else
		{
			$price_range = explode("||", $userFilter['price_range']);
		}
		?>
			<li class="list"><input type="checkbox" value="0,25" <?php if(in_array("0,25", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>>Under $25 </li>
			<li class="list"><input type="checkbox" value="25,50" <?php if(in_array("25,50", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>>$25-$50 </li>
			<li class="list"><input type="checkbox" value="50,100" <?php if(in_array("50,100", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>>$50-$100 </li>
			<li class="list"><input type="checkbox" value="100,250" <?php if(in_array("100,250", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>>$100-$250 </li>
			<li class="list"><input type="checkbox" value="250,0" <?php if(in_array("250,0", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>>Over $250 </li>
		</ul>	
		</div>
		
		
		<div class="clear"></div>
            	</div> 
            </div>
	      </div>
    </div>

<div class="filter_btn" id="filter_btn" style="height:325px; !important">
	<div class="categories">
	
		</div>
		<div class="clear"></div>
		
		<!-- Code by @sadikhasan for more filter options -->
		<div class="sizes">
			
		</div>
		
		<!-- This code done for brand name with check box -->
		<div class="brands">
			
		</div>
		
		<div class="other">
		
		</div>
</div>		
		
		<?php echo $this->Form->button('Apply',array('type'=>'button','id'=>'apply_rate_stream', 'class'=>"blue_btn"));?>
		
</div>		
		
		</div>
		
<script>
$(document).ready(function(){
	fleXenv.fleXcrollMain("categorylist");

	//attechInviteEvent();
	
	var TabbedPanels2 = new Spry.Widget.TabbedPanels("TabbedPanels2");

	/** When page load at that time hide all the tab initially except category tab. **/
	
});
</script>		