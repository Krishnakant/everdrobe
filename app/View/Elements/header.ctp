<nav class="navbar navbar-default">
  <div class="container">

    <div class="navbar-header">
     
      <a class="navbar-brand" href="#"><img src="images/everdrobe_logo.png" alt="" /></a>
    </div>
	<?php if($this->Session->read('Auth.User.id')>0){ ?>
	<div class="logout web-text-right vtop">Welcome User | <?php echo $this->Html->link('Logout',array('controller'=>'users','action'=>'logout'),array("class"=>"right_link"));?></div>
    
	<?php } ?>   
	
	<ul class="list-inline hidden-xs hidden-sm pull-right store_url <?php if($this->Session->read('Auth.User.id')>0){ ?>mt10 <?php } else{ ?> mt20<?php  } ?>">
		<?php if($this->Session->read('Auth.User.id')>0){ ?>
			<li><?php  echo $this->element('profile'); ?></li>
		<?php } else{ ?> 
			<li><a href="https://play.google.com/store/apps/details?id=com.bizhill1.everdrobe" target="_blank"><img src="images/android.png" alt="Android" ></a></li>
			<li><a href="https://itunes.apple.com/in/app/id576116687" target="_blank"><img src="images/apple.png" alt="iOS" ></a></li>
		<?php  } ?>
	</ul>
	
	<ul class="list-inline hidden-md hidden-lg text-center web-text-right store_url <?php if($this->Session->read('Auth.User.id')>0){ ?> mt10 <?php } else{ ?> mt20<?php  } ?>">
		<?php if($this->Session->read('Auth.User.id')>0){ ?>
			<li><?php  echo $this->element('profile'); ?></li>
		<?php } else{ ?> 
			<li><a href="https://itunes.apple.com/in/app/id576116687" target="_blank"><img src="images/android.png" alt="Android" ></a></li>
			<li><a href="https://play.google.com/store/apps/details?id=com.bizhill1.everdrobe" target="_blank"><img src="images/apple.png" alt="iOS" ></a></li>
		<?php  } ?>
	</ul>
	
     
   </div>
   <div class="clearfix"></div>
   <div class="main-menu">
   <div class="container">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	  
	  
	<?php if($this->Session->read('Auth.User.id')>0){ ?>
		
		<?php if(isset($userFilter)) echo $this->element('filter_category');?>

	  <?php } else{ ?>
      <ul class="list-inline navbar-right mt10 user-action-menu">
        <li class="register_action"><a class="btn btn-green btn-sm">Signup</a>
			<div class="user_register">
			<?php echo $this->element('register');?>
			</div>
		</li>
        <li class="login_action"><a class="btn btn-danger btn-sm">Login</a>
			<div class="login_form">
			<?php echo $this->element('login'); ?>
			
			</div>
		</li>
		
      </ul>
    <?php  } ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav main-navigation">
     
			    <li id="home_menu"><?php echo $this->Html->link('Home',array('controller'=>"drobes","action"=>"rate",0,"recent","rate_stream"),array("escape"=>false)); ?></li>
			    <li id="explorer_menu" ><?php echo $this->Html->link('Explore',array('controller'=>"drobes","action"=>"index"),array("escape"=>false)); ?></li>
			    <li id="post_menu" ><?php echo $this->Html->link('Post',array('controller'=>"drobes","action"=>"mydrobe"),array("escape"=>false)); ?></li>
    
    			<?php if(isset($new_response_notifications) && $new_response_notifications>0){ ?>
    				<li id="result_menu"><?php	echo $this->Html->link('My Results <div class="badges">'.$new_response_notifications.'</div>',array('controller'=>"drobes","action"=>"results"),array("escape"=>false)); ?></li>
    			<?php } else { ?>
					<li id="result_menu"><?php echo $this->Html->link('My Results',array('controller'=>"drobes","action"=>"results"),array("escape"=>false)); ?></li>
    			<?php } ?>

    			<li id="fav_menu"><?php echo $this->Html->link('My Faves',array('controller'=>"drobes","action"=>"faves"),array("escape"=>false)); ?></li>
    			
    			<?php if(isset($new_drobe_notifications) && $new_drobe_notifications>0) { ?>
    				<li id="following_menu"><?php	echo $this->Html->link('Following <div class="badges" id="following_badge">'.$new_drobe_notifications.'</div>',array('controller'=>"users","action"=>"following_panel"),array("escape"=>false)); ?></li>
    			<?php } else { ?>
    				<li id="following_menu"><?php echo $this->Html->link('Following',array('controller'=>"users","action"=>"following_panel"),array("escape"=>false)); ?></li>
    			<?php } ?>

    			<li id="profile_menu"><?php echo $this->Html->link('My Profile',array('controller'=>"users","action"=>"profile"),array("escape"=>false)); ?></li>
    			<li id="everbucks_menu"><?php echo $this->Html->link('My Everbucks',array('controller'=>"users","action"=>"my_everbucks"),array("escape"=>false)); ?></li>
    			
				<?php if(Configure::read('setting.drobe_sale')=="on") { ?>
    				<li id="closest_menu"><?php	echo $this->Html->link('My Closet',array('controller'=>"sell_drobes","action"=>"index"),array("escape"=>false)); ?></li>
    			<?php } ?>
     
      </ul>
     </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  </div>
</nav>
<script>
function activate_menu(title)
{
	//$("ul.main-navigation li").removeClass("active");
	$("#"+title+"_menu").addClass("active");
}
$(window).load(function(){
if ($(".search-box").length > 0) {
	//$("#container").removeClass("container");
}
if ($(".my_result-page").length > 0) {
	//$("#container").removeClass("container");
}
});
</script>