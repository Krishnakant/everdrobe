<div id="user_login" <?php if($this->params->params['action']=="register"){ echo 'class="hide"'; } ?>>
	            	<h2>Login</h2>
	            	<hr/>
	            	<div class="spacer line"></div>
	                <div class="social_connect">
	                <?php echo $this->Html->link("Login with Facebook",$facebookUrl,array("class"=>"facebook simple_popup")); ?>
	                <?php //echo $this->Html->link("Login with Twitter",$twitterLoginUrl,array("class"=>"twitter simple_popup")); ?>
	                </div>
	                <div class="login_form">
                    <?php 
	                echo $this->Form->create("User",array("id"=>"login_form",'action' => 'login'));
                    ?>
                    <p>Use your Everdrobe Account</p>
                    <?php
                    echo $this->Form->input("email",array("label"=>false,"class"=>"text_box","placeholder"=>"Email Id"));
	                echo $this->Form->input("password",array("label"=>false,"class"=>"text_box","placeholder"=>"Password"));
	                echo $this->Form->submit("Login",array("class"=>"button"));
	                ?>
	                <div class="spacer line"></div>
	                <div class="sign_up">
	                <p>Not yet an Everdrobe Member</p>
	                <?php
	                echo $this->Html->link("Sign Up","javascript:void(0)",array("class"=>"sign_up")); 
                    ?>
	                </div>
                    <?php
                    echo $this->Form->end();
                    ?>
                    </div>
                    </div>
<div id="user_register" <?php if($this->params->params['action']=="register"){ echo 'class="hide"'; } ?>>
	            	<h2>Login</h2>
	            	<hr/>
	            	<div class="spacer line"></div>
	                <div class="social_connect">
	                <?php echo $this->Html->link("Login with Facebook",$facebookUrl,array("class"=>"facebook simple_popup")); ?>
	                <?php //echo $this->Html->link("Login with Twitter",$twitterLoginUrl,array("class"=>"twitter simple_popup")); ?>
	                </div>
	                <div class="login_form">
                    <?php 
	                echo $this->Form->create("User",array("id"=>"login_form",'action' => 'login'));
                    ?>
                    <p>Use your Everdrobe Account</p>
                    <?php
                    echo $this->Form->input("email",array("label"=>false,"class"=>"text_box","placeholder"=>"Email Id"));
	                echo $this->Form->input("password",array("label"=>false,"class"=>"text_box","placeholder"=>"Password"));
	                echo $this->Form->submit("Login",array("class"=>"button"));
	                ?>
	                <div class="spacer line"></div>
	                <div class="sign_up">
	                <p>Not yet an Everdrobe Member</p>
	                <?php
	                echo $this->Html->link("Sign Up","javascript:void(0)",array("class"=>"sign_up")); 
                    ?>
	                </div>
                    <?php
                    echo $this->Form->end();
                    ?>
                    </div>
                    </div>