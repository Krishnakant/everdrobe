<div class="left-panel 1">
    <div class="left-navi">
    	<?php 
    		echo $this->Html->link('<div class="icon"></div>Manage Users',array('controller'=>"users","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>User Statistics',array('controller'=>"users","action"=>"list",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>Everdrobe Winners',array('controller'=>"highest_rated_drobes","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		//echo $this->Html->link('<div class="icon"></div>Highest Rated Drobes',array('controller'=>"drobes","action"=>"highest_rated_drobe",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>EBX Redeem Requests',array('controller'=>"ebx_transactions","action"=>"user_redeem_request",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>Manage Drobes',array('controller'=>"drobes","action"=>"list",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>Drobe Statistics',array('controller'=>"drobes","action"=>"show_list",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		
    		if(isset($pending_drobes) && $pending_drobes>0)
    		{
    			echo $this->Html->link('<div class="icon"></div>Drobes for Sell <div class="badges">'.$pending_drobes.'</div>',array('controller'=>"drobes","action"=>"sell_drobes",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		else
    		{
    			echo $this->Html->link('<div class="icon"></div>Drobes for Sell',array('controller'=>"drobes","action"=>"sell_drobes",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		echo $this->Html->link('<div class="icon"></div>View Sold Drobes',array('controller'=>"sold_drobes","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>View Closed Drobes',array('controller'=>"drobes","action"=>"closed_drobe",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		if(isset($unpaid_redeem) && $unpaid_redeem>0)
    		{
    			echo $this->Html->link('<div class="icon"></div>Redeem Request<div class="badges">'.$unpaid_redeem.'</div>',array('controller'=>"redeem_requests","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		else
    		{
    			echo $this->Html->link('<div class="icon"></div>Redeem Request',array('controller'=>"redeem_requests","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		
    		
    		if(isset($pending_request) && $pending_request>0)
    		{
    			echo $this->Html->link('<div class="icon"></div>Request For Label<div class="badges">'.$pending_request.'</div>',array('controller'=>"request_for_labels","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		else
    		{
    			echo $this->Html->link('<div class="icon"></div>Request For Label',array('controller'=>"request_for_labels","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		echo $this->Html->link('<div class="icon"></div>View Label Weight',array('controller'=>"weights","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		
    		if(isset($pending_problem) && $pending_problem>0)
    		{
    			echo $this->Html->link('<div class="icon"></div>Seller Problem<div class="badges">'.$pending_problem.'</div>',array('controller'=>"seller_problems","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		else
    		{
    			echo $this->Html->link('<div class="icon"></div>Seller Problem',array('controller'=>"seller_problems","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		}
    		
    		
    		echo $this->Html->link('<div class="icon"></div>View Sellers',array('controller'=>"sell_drobes","action"=>"sellers",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>Feature Drobes',array('controller'=>"drobes","action"=>"featured_drobe",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>Flagged Drobes',array('controller'=>"drobes","action"=>"flag",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
/*
    		echo $this->Html->link('<div class="icon"></div>Flagged Users',array('controller'=>"users","action"=>"flag",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
*/
    		echo $this->Html->link('<div class="icon"></div>View Category List',array('controller'=>"categories","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in")); 
    		echo $this->Html->link('<div class="icon"></div>Manage Flag Category',array('controller'=>"flag_categories","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in")); 
    		echo $this->Html->link('<div class="icon"></div>Country/State List',array('controller'=>"countries","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in")); 
    		echo $this->Html->link('<div class="icon"></div>Manage Block-list',array('controller'=>"blocks","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in")); 
    		echo $this->Html->link('<div class="icon"></div>Manage Notification',array('controller'=>"notifies","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in")); 
    		echo $this->Html->link('<div class="icon"></div>Manage Content Pages',array('controller'=>"pages","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in")); 
    		echo $this->Html->link('<div class="icon"></div>Manage Drobe Size',array('controller'=>"sizes","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>Manage Drobe Brands',array('controller'=>"brands","action"=>"index",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		echo $this->Html->link('<div class="icon"></div>Broadcast Message',array('controller'=>"notification_queues","action"=>"report",'admin'=>true),array("escape"=>false,"class"=>"left-navi-in"));
    		
    	 ?>
    </div>
</div>
