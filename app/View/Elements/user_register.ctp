<div id="user_register" <?php if($this->params->params['action']!="register"){ echo 'class="hide"'; }?>>
<div class="social_connect">
<?php echo $this->Html->link("Connect with Facebook",$facebookUrl,array("class"=>"facebook simple_popup")); ?>
<?php echo $this->Html->link("Connect with Twitter","javascript:void(0)",array("class"=>"twitter")); ?>
</div>
<?php
echo $this->Form->create("User",array("id"=>"register_form",'action' => 'register'));
?>
<div id='response'></div>
<p>Create an Everdrobe account</p>
<?php
echo $this->Form->input("first_name",array("label"=>false,"class"=>"text_box","placeholder"=>"First Name"));
echo $this->Form->input("last_name",array("label"=>false,"class"=>"text_box","placeholder"=>"Last Name"));
echo $this->Form->input("email",array("label"=>"text","label"=>false,"class"=>"text_box","placeholder"=>"Email Id"));
echo $this->Form->input("password",array("label"=>false,"class"=>"text_box","placeholder"=>"Password"));
echo $this->Form->input('field', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false, 'options' => array('Male', 'Female'),'type' => 'radio'));
?>
<div class="spacer line"></div>
<p>Use your real birthdate so your questions reach the right people. </p>
<?php
echo $this->Form->label("Enter Your Birthdate");
echo $this->Form->input('birth_month',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"MM"));
echo $this->Form->input('birth_day',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"DD"));
echo $this->Form->input('birth_year',array("label"=>false,"div"=>false, "class"=>"text_box small","placeholder"=>"YYYY"));
?>
<div class="spacer line"></div>
<?php
echo $this->Form->checkbox("agree",array("class"=>"required"))." I agree to the ".$this->Html->link("Terms of Use and Privacy Policy","javascript:void(0)").".";
echo $this->Form->submit("Sign Up Now",array("class"=>"btn btn-success"));    
echo $this->Form->end();
?>
</div>
 <script>
    	$(".register_action > a").click(function() {
            $(".user_register").toggle();
			$(".login_form").hide();
        });
    </script>