<div id="user_register" class="right-panel <?php //if($this->params->params['action']!="register"){ echo 'hide'; }?>">
        <div class="text-center mt10">
		<?php echo $this->Html->link("",$facebookUrl,array("class"=>"fb_register simple_popup")); ?>
        <?php echo $this->Html->link("",$gmailAuthUrl,array("class"=>"gmail_register simple_popup")); ?>
        </div>
		<div class="clearfix"></div>
        <div class="or"><span>OR</span></div>
        <div  class="para">Create an Everdrobe Account</div>
        <?php
echo $this->Form->create("User",array("id"=>"register_form",'action' => 'register'));
?>
<div class="random username_txt"><?php 
echo $this->Form->input("username",array("label"=>false,"div"=>false,"placeholder"=>"Choose a User name","class"=>"form-control"));
?>
</div>
<!-- <div class="random"><?php 
echo $this->Form->input("first_name",array("label"=>false,"div"=>false,"placeholder"=>"First Name","class"=>"form-control"));
?>
</div>
<div class="random"><?php 
echo $this->Form->input("last_name",array("label"=>false,"div"=>false,"placeholder"=>"Last Name","class"=>"form-control"));
?>
</div> -->
<div class="random"><?php 
echo $this->Form->input("email",array("label"=>false,"div"=>false,"placeholder"=>"Email Id","class"=>"form-control"));
?>
</div>
<div class="random"><?php 
echo $this->Form->input("password",array("label"=>false,"div"=>false,"placeholder"=>"Password","class"=>"form-control"));
?>
</div>
<div class="random"><?php 
echo $this->Form->input("confirm_password",array("type"=>"password","label"=>false,"div"=>false,"placeholder"=>"Confirm Password","class"=>"form-control"));
?>
</div>
<!-- <div class="random">
<label style='color:#696969;font-weight:bold;'>( First and last name will not be visible on your profile )</label>
</div> -->

<div class="clear"></div>
<?php
//echo $this->Form->input('gender', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false, 'options' => array('male'=>"Male", 'female'=>"Female"),'default'=>"Male",'type' => 'radio'));
?>
<div class="spacer"></div>
<div class="line"></div>
<!-- <p>Use your real birthdate so your questions reach the right people. </p> -->
<?php
//echo $this->Form->label("Enter Your Birthdate");
?>
<div class="clear"></div>

<?php 
//echo $this->Form->input('birth_month',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"MM"));
//echo $this->Form->input('birth_day',array("label"=>false,"div"=>false, "class"=>"text_box very_small","placeholder"=>"DD"));
//echo $this->Form->input('birth_year',array("label"=>false,"div"=>false, "class"=>"text_box small","placeholder"=>"YYYY"));
?>
<div class="clear"></div>
<div class="spacer"></div>
<?php
echo $this->Form->checkbox("invite")."<label for='UserInvite' class='ml15'> I want to invite friends</label>";
?>
<div class="spacer"></div>
<div class="line"></div>
<div class="spacer"></div>
<?php
echo $this->Form->checkbox("agree",array("class"=>"required pull-left"))."<label for='UserAgree' class='ml15'> I agree to the ".$this->Html->link("Terms of Use",array("controller"=>"pages","action"=>"display","terms","admin"=>false),array("target"=>"_blank"))."  and ".$this->Html->link("Privacy Policy",array("controller"=>"pages","action"=>"display","privacy","admin"=>false),array("target"=>"_blank")).".</label>";
?>
<div class="spacer"></div>
<?php
echo $this->Form->button('Sign Up',array('class'=>" btn btn-success"));
//echo $this->Form->submit("Sign Up Now",array("class"=>"btn btn-success"));    
echo $this->Form->end();
?>
</div>

 <script>
    	$(".register_action > a").click(function() {
            $(".user_register").toggle();
			$(".login_form").hide();
			
        });
    </script>