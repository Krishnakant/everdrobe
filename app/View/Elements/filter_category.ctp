<?php
$badge_flag = false;
$badge_counter=0;
$gender_filter=$userFilter['gender'];
$brand_filter=$userFilter['only_new_brand'];
$only_new_brand = $userFilter['only_new_brand'];
$sort_by_highest_rated = $userFilter['highest_rated'];
if($userFilter['category_ids']!="ALL" && $userFilter['category_ids']!="" && $badge_flag==false)
{
	$badge_counter= count(explode(",",$userFilter['category_ids']));
	$badge_flag = true;
}
if($userFilter['brand_name']!="{ALL}" && $userFilter['brand_name']!="" && $badge_flag==false)
{
	$badge_counter= count(explode("||",$userFilter['brand_name']));
	$badge_flag = true;
}
if($userFilter['size_name']!="{ALL}" && $userFilter['size_name']!="" && $badge_flag==false)
{
	$badge_counter= count(explode("||",$userFilter['size_name']));
	$badge_flag = true;
}
if($userFilter['price_range']!="{ALL}" && $userFilter['price_range']!="" && $badge_flag==false)
{
	$badge_counter= count(explode("||",$userFilter['price_range']));
	$badge_flag = true;
}
if(in_array($brand_filter, array("new","used")) && $badge_flag==false)
{
	$badge_counter = 1;
	$badge_flag = true;
}
if(($only_new_brand=='new' || $only_new_brand=='yes') && $badge_flag==false)
{
	$badge_counter = 1;
	$badge_flag = true;
}
if(in_array($gender_filter, array("male","female")) && $badge_flag==false)
{
	$badge_counter=1;
	$badge_flag = true;
}
	

?><div class="filter_drobe">

	<div class="blue_btn" id="stream_button">
	<?php 
	
	if($badge_counter>0)
	{
	?>
	<a class="badges"><?php echo $badge_counter; ?></a>
	<?php 
	}
	?>
	
	</div>

<div class="filter_input">


	<div id="TabbedPanelsFilter" class="TabbedPanels">
		  <ul class="TabbedPanelsTabGroup">
		    <li class="TabbedPanelsTab" tabindex="0">Category</li>
		    <li class="TabbedPanelsTab" tabindex="0">Size</li>
		    <li class="TabbedPanelsTab" tabindex="0">Brand</li>
		    <li class="TabbedPanelsTab" tabindex="0">Other</li>
	      </ul>
		  <div class="TabbedPanelsContentGroup">
		    <div class="TabbedPanelsContent">
			<div class="">
            		<div class="long_content flexcroll" id="categorylist">
            		<div class="categories">
					<div class="spacer"></div>
					<div class="col-sm-12">
						<label><input type="checkbox" id="check-all" <?php echo (count($userFilter)==0 || $userFilter['category_ids']=="ALL") ? "checked": "" ;?> > <b>Select All Categories</b></label>
					</div>
						<div class="clear"></div>
					<?php
					$categories=Cache::read('drobe_category');
					$i=1;
					
					foreach ($categories as $key=>$category)
					{
						if(trim($category)=="") continue;
						?>
							<div class="col-sm-6">
							<label><input type="checkbox" value="<?php echo $key;?>" <?php echo (count($userFilter)==0 || $userFilter['category_ids']=="ALL" || in_array($key, explode(",",$userFilter['category_ids']))) ? "checked" : "" ; ?> /> <?php echo $category; ?></label>
							</div>
							<?php
							if($i==0)
							{
								?>
								<div class="clear"></div>
								<?php 
							}
							$i=($i+1)%2; 
					}
						?>
						</div>
						<div class="clear"></div>
            		
            		</div> </div> 
           	</div>
		    <div class="TabbedPanelsContent">
			<div class="">
            		<div class="long_content flexcroll" id="sizelist">
            			<!-- Code by @sadikhasan for more filter options -->
					<div class="sizes">
						<div class="spacer"></div>
						<div class="col-sm-12">
						<label><input type="checkbox" id="check_all_sizes" <?php echo (count($userFilter)==0 || $userFilter['size_name']=="{ALL}") ? "checked": "" ;?> > <b>Select All Sizes</b></label>
						</div>
					<div class="clear"></div>
					<div class="size_list ">
						<?php 
							$size_names = Cache::read('size_list');
							
							foreach ($size_names as $key=>$size)
							{
								if(trim($size)=="") continue;
								?>
									<div class="col-sm-3 col-xs-4">
									<label><input type="checkbox" value="<?php echo $size;?>" <?php echo (count($userFilter)==0 || $userFilter['size_name']=="{ALL}" || in_array($size, explode("||",$userFilter['size_name']))) ? "checked" : "" ; ?> /> <?php echo $size; ?></label>
									</div>
								<?php 
							}
						?>
					</div>
					<div class="clear"></div>
					</div>
            		</div> </div> 
            </div>
            <div class="TabbedPanelsContent">
			<div class="">
            		<div class="long_content flexcroll" id="brandlist">
			            			<!-- This code done for brand name with check box -->
					<div class="brands">
						<div class="spacer"></div>
						<div class="col-sm-12">
						<label><input type="checkbox" id="check_all_brands" <?php echo (count($userFilter)==0 || $userFilter['brand_name']=="{ALL}") ? "checked": "" ;?> > <b>Select All Brands</b></label>
						</div>
					<div class="clear"></div>
					<div class="brand_list">
						<?php 
							$brand_names = Cache::read('brand_list');
							foreach ($brand_names as $key=>$brand)
							{
								if(trim($brand)=="") continue;
								?>
									<div class="col-sm-6">
									<label><input type="checkbox" value="<?php echo $brand;?>" <?php echo (count($userFilter)==0 || $userFilter['brand_name']=="{ALL}" || in_array($brand, explode("||",$userFilter['brand_name']))) ? "checked" : "" ; ?> /> <?php echo $brand; ?></label>
									</div>
								<?php 
							}
						?>
					</div>
					<div class="clear"></div>
					</div>
            		</div> </div> 
            </div>
           <div class="TabbedPanelsContent">
            <div class="long_content flexcroll" id="otherlist">
            <div class="other">
		<div class="spacer"></div>

		<div class="col-sm-12">
		<b>Filter By Gender</b>
		<div class="clear"></div>
		
			<label><input type="radio" value="both" name="filter_gender" <?php if(!in_array($gender_filter, array("male","female"))) echo "checked='checked'"; ?>/> Male or Female</label>
			<label><input type="radio" value="male" name="filter_gender" <?php if($gender_filter=="male") echo "checked='checked'"; ?>/> Male</label>
			<label><input type="radio" value="female" name="filter_gender" <?php if($gender_filter=="female") echo "checked='checked'"; ?> /> Female</label>
		</div>
	
		
		<div class="spacer"></div>
		<div class="col-sm-12">
		<b>Is Brand New</b>
		<div class="clear"></div>
		<label><input type="radio" value="new" name="only_new_brand" <?php if($only_new_brand=="new") echo "checked='checked'"; ?>/> New</label>
		<label><input type="radio" value="used" name="only_new_brand" <?php if($only_new_brand=="used") echo "checked='checked'"; ?>/> Used</label>
		<label><input type="radio" value="both" name="only_new_brand" <?php if(!in_array($only_new_brand,array("new","used"))) echo "checked='checked'"; ?> /> Both</label>
		</div>
		
		<!-- <div class="spacer"></div>
		<div class="col-sm-12">
		<b>Sort By Highest Rated</b>
		<div class="clear"></div>
		<label><input type="radio" value="1" name="highest_rated" <?php if($sort_by_highest_rated=="1") echo "checked='checked'"; ?>/> On</label>
		<label><input type="radio" value="0" name="highest_rated" <?php if($sort_by_highest_rated=="0") echo "checked='checked'"; ?>/> Off</label>
		</div> -->
		
		<div class="prices" >
		<div class="spacer"></div>
		<div class="col-sm-12">
		<label><input type="checkbox" id="check_all_prices" <?php echo (count($userFilter)==0 || $userFilter['price_range']=="{ALL}") ? "checked": "" ;?> > <b>Select All Prices</b></label>
		</div>
		<div class="clear"></div>
		<div class="price_list">
		<?php 
		$price_range=array();
		$all_price="";
		if($userFilter['price_range']=="{ALL}")
		{
			$all_price=$userFilter['price_range'];
		}	
		else
		{
			$price_range = explode("||", $userFilter['price_range']);
		}
		?>
			<div class="col-sm-6"><input type="checkbox" value="0,25" <?php if(count($userFilter)==0 || in_array("0,25", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>> Under $25 </div>
			<div class="col-sm-6"><input type="checkbox" value="25,50" <?php if(count($userFilter)==0 || in_array("25,50", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>> $25-$50 </div>
			<div class="col-sm-6"><input type="checkbox" value="50,100" <?php if(count($userFilter)==0 || in_array("50,100", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>> $50-$100 </div>
			<div class="col-sm-6"><input type="checkbox" value="100,250" <?php if(count($userFilter)==0 || in_array("100,250", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>> $100-$250 </div>
			<div class="col-sm-6"><input type="checkbox" value="250,0" <?php if(count($userFilter)==0 || in_array("250,0", $price_range) || $all_price=="{ALL}") echo "checked='checked'"; ?>> Over $250 </div>
		</div>	
		</div>
		
		
		<div class="clear"></div>
		</div>
            		</div> 
            </div>
	      </div>
    </div>
<div class="text-center p10">
		<?php echo $this->Form->button('Resets Filters',array('type'=>'button','id'=>'reset_filter', 'class'=>"btn btn-blue"));?>
		<?php echo $this->Form->button('Apply',array('type'=>'button','id'=>'apply_rate_stream', 'class'=>"btn btn-blue"));?>
</div>
</div>		
		
		</div>
				