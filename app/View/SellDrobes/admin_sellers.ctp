<link rel="stylesheet" type="text/css" href="http://craigsworks.com/projects/qtip2/packages/latest/jquery.qtip.min.css" />
<style type="text/css">
/* Add some nice box-shadow-ness to the modal tooltip */
#qtip-modal{
	max-width: 420px;
	-moz-box-shadow: 0 0 10px 1px rgba(0,0,0,.5);
	-webkit-box-shadow: 0 0 10px 1px rgba(0,0,0,.5);
	box-shadow: 0 0 10px 1px rgba(0,0,0,.5);
}
#qtip-modal .qtip-content{
	padding: 10px;
}
</style>
<div class="right actions">
<?php 
//echo $this->Html->link('View Payment Tracking', array("controller"=>"seller_payments","action"=>"payment_tracking","admin"=>true));
?>
</div>
<h2>Sellers List</h2>
<div class="search_box">
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search Drobe : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search sellers' by first name, last name or full name" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($sellers)>0)
{
	?>
	<table>
		<tr>
			<th>Image</th>
			<th><?php echo $this->Paginator->sort('User.first_name','Full Name');?></th>
			<th><?php echo $this->Paginator->sort('User.username','User Name')?>
			<th><?php echo $this->Paginator->sort('User.email','Email');?></th>
			<th><?php echo $this->Paginator->sort('User.status','Status');?></th>
			<th>Post for Sell</th>
			<th>Total Sold</th>
			<th>Total Revenue</th>
			<th>Seller Revenue</th>
			<th>Paid to Seller</th>
		</tr>
		<?php 
		foreach($sellers as $seller)
		{
			?>
			<tr>
				<td width="50px" rowspan="2">
				<div style="position: relative;">
				<?php 
					if($seller['User']['photo']!="") echo $this->Html->image('/profile_images/thumb/'.$seller['User']['photo'],array('border'=>0,"width"=>"50px",'id'=>"profile_image" ,"alt"=>"User photo"));
					else echo $this->Html->image('/images/default.jpg',array('border'=>0,"width"=>"50px",'id'=>"profile_image" ,"alt"=>"User photo"));
					if($seller['User']['star_user']==1):?>
						<div class="star-user-small"></div>
					<?php endif;?></div></td>
				<td><?php echo $seller['User']['first_name']." ".$seller['User']['last_name']; ?></td>
				<td><?php echo $seller['User']['username'];?></td>
				<td align="center"><?php echo $seller['User']['email'];?></td>
				<td align="center"><?php echo Inflector::humanize($seller['User']['status']);?></td>
				<td align="center"><?php echo ($seller[0]['sell_drobes']-$seller[0]['sold_items']);?></td>
				<td align="center"><?php echo $seller[0]['sold_items'];?></td>
				<td align="center"><?php echo "$ ".round($seller[0]['total_revenue'],2)." $";?></td>
				<td align="right"><?php echo "$ ".round($seller[0]['seller_revenue'],2);?></td>
				<td align="right"><?php echo (isset($seller['SellProfile']['total_paid']) && $seller['SellProfile']['total_paid']>0)?"$ ".$seller['SellProfile']['total_paid']:"$ 0";?></td>
			</tr>
			<tr>
				<td colspan="100%">
				<div class="actions" style="text-align: right">
				<?php 
				
				if($seller[0]['sold_items']>0) 
				{
					echo $this->Html->link('View Sold Drobe',array("controller"=>"sold_drobes","action"=>"index",$seller['User']['id']))." ";
					//echo $this->Html->link('Payment',array("controller"=>"seller_payments","action"=>"pay","admin"=>true,$seller['User']['id']))." ";
					//echo $this->Html->link('Payment History',array("controller"=>"seller_payments","action"=>"history","admin"=>true,$seller['User']['id']))." ";
				}
				echo $this->Html->link('Seller Profile',"javascript:void(0)",array("class"=>"view_seller_profile"))." ";
				?>
				<div class="seller_profile" style="display: none" rel="<?php echo $seller['User']['first_name']." ".$seller['User']['last_name']?>">
					<table>
						<tr>
						</tr>
						<tr>
							<td> User Name: </td>
							<td><?php echo $seller['User']['username']!="" ? $seller['User']['username']: "Not updated"; ?></td>
						</tr>
						<tr>
						</tr>
						
						<tr>
							<td> Mailing Address: </td>
							<?php 
							if($seller['SellProfile']['street1']!="")
							{
								$street2 = ($seller['SellProfile']['street2'] !="")?$seller['SellProfile']['street2'].",":"";
								$address = $seller['SellProfile']['street1'].",".$street2."\r".$seller['SellProfile']['city'].",".$seller['SellProfile']['state'].",\r".$seller['SellProfile']['zip'];
							}
							else
							{
								$address = $seller['SellProfile']['address'];
							}
							?>
							<td><?php echo $address!="" ? '<pre style="margin:0; padding:0; font-size: larger;">'.$address.'<pre/>' : "Not updated"; ?></td>
						</tr>
						
						<tr>
							<td> Paypal Email: </td>
							<td><?php echo $seller['SellProfile']['paypal_email']!="" ?$seller['SellProfile']['paypal_email'] : "Not updated"; ?></td>
						</tr>
						
						<tr>
							<td> Bank Information </td>
							<?php 
								$bank_info = "Deposit Name:".$seller['SellProfile']['deposit_name'];
								$bank_info .="\rAccount No:".$seller['SellProfile']['account_no'];
								$bank_info .="\rRouting No:".$seller['SellProfile']['routing_no'];
								$bank_info .="\rBank Name:".$seller['SellProfile']['bank_name'];
								$bank_info .="\rAccount Type:".$seller['SellProfile']['account_type'];
							?>
							<td><?php echo $seller['SellProfile']['account_no']!="" ?'<pre style="margin:0; padding:0; font-size: larger;">'.$bank_info.'</pre>': "Not updated"; ?></td>
						</tr>
						
						
						
					</table>
				</div>
				<div class="spacer"></div>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="featured_drobe" style="display: none;" class="featured_form">
	<h2>Mark as Featured</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Drobe: </strong><span id="featured_drobe_name"></span></div>
		
		<?php 
			echo $this->Form->input('buy_url',array('id'=>'buy_url','label'=>false,'div'=>false,'placeholder'=>"Enter URL for buy drobe"));
			echo $this->Form->hidden("featured_drobe_id",array("id"=>"featured_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_featured"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_featured"));
?>
<div class="spacer"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	});
	$('.view_seller_profile').each(function(){
		var self = $(this);
			$(this).qtip({
				id: 'modal', // Since we're only creating one modal, give it an ID so we can style it
				content: {
					text: self.parent().find('.seller_profile').html(),
					title: {
						text: 'Seller Profile : ' + self.parent().find('.seller_profile').attr('rel'),						button: true
					}
				},
				position: {
					my: 'center', // ...at the center of the viewport
					at: 'center',
					target: $(window)
				},
				show: {
					event: 'click', // Show it on click...
					solo: true, // ...and hide all other tooltips...
					modal: true // ...and make it modal
				},
				hide: false,
				style: 'qtip-light qtip-rounded'
		});
	});
});
</script>