<div class="actions right">
<?php 
if($this->Session->read('Auth.User.type')!="admin")
{
	echo $this->Html->link("Back to Manage Drobe List",array("action"=>"drobes"));
}
else echo $this->Html->link("Back to Manage Drobe List",array("controller"=>"drobes","action"=>"sell_drobes"));
?>
</div>
<h2> Edit Drobe for Sale</h2>
<div class="drobe_sale_popup">
<?php 
echo $this->Form->create("Drobe");
echo $this->Form->input("category_id",array('options'=>$categories,"style"=>"width:360px"));
echo $this->Form->input("comment",array("style"=>"width:350px;",'rows'=>4,'id'=>'edit_comment'));
echo $this->Form->submit("Save Changes");
?>

</div>