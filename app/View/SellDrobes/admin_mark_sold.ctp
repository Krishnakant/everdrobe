<div class="actions right">
<?php 
if($this->Session->read('Auth.User.type')!="admin")
{
	echo $this->Html->link("Back to Sell Drobe List",array("action"=>"sell_drobes"));
}
else
{
	echo $this->Html->link('Drobe Settings', array("controller"=>"settings","action"=>"index","admin"=>true))." ";
	echo $this->Html->link("Back to Sell Drobe List",array("controller"=>"drobes","action"=>"sell_drobes"));
}
?>
</div>
<h2> Manage Drobe for Sale</h2>
<div class="tp tp_big drobe_result">
 <?php echo $this->Html->image('/drobe_images/'.$drobe['file_name'],array('alt'=>$drobe['file_name'],'height'=>"85", 'width'=>"85")); ?>
         <div class="user_cont">
         	<div class="cont"><?php echo $drobe['comment']=="" ? "" : $drobe['comment']." <br />";?><em><?php echo $categories[$this->data['Drobe']['category_id']]?></em></div>
         	<div class="clear"></div>
            <div class="up"><?php echo $drobe['total_in'];?></div> 
            <div class="down"><?php echo $drobe['total_out'];?></div> 
            <div class="whatever">Total Votes: <?php echo $drobe['total_rate']?></div>  
         </div>
</div>
<div class="spacer"></div>
<h3>Sold Drobe Payment Information</h3>
<div class="spacer"></div>
<?php
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
echo $this->Form->create("SellDrobe");
?>
<div class="drobe_sale_popup">
	<div class="half left">
	<label style="font-size: large;">
	<?php 
		echo "<b> Price in USD:</b> ".$this->data['SellDrobe']['sell_price']." $";
	?>
	</label>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SoldDrobe.order_id', array("place","label"=>"<b>Order Id:</b>","type"=>"text"));
	?>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SoldDrobe.earning_percent', array("place","label"=>"<b>Everdrobe Earning (in %):</b>","type"=>"text", "id"=>"earning_precent","value"=>Configure::read('setting.everdrobe_earning_percent')));
	?>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SoldDrobe.paid_amount', array("place","label"=>"<b>Seller Earning:</b>","type"=>"text","value"=>($this->data['SellDrobe']['sell_price']-round($this->data['SellDrobe']['sell_price']*Configure::read('setting.everdrobe_earning_percent')/100,2)),"readonly"=>"readonly","id"=>"seller_earning_amount"));
	?>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SoldDrobe.tracking_number', array("place","label"=>"<b>Tracking Number:</b>","type"=>"text","value"=>$this->data['SellDrobe']['tracking_number']));
	?>
	<div class="spacer"></div>
	</div>
	<div class="half right drobe_description">
	<?php 
		echo $this->Form->label("<b> Description: </b> <em>(Optional)</em>");
		echo $this->Form->textarea('SoldDrobe.comment',array('lable'=>"false", "id"=>"drobe_description", "style"=>"height:200px;"));
	?>
	<div><em>Max 300 Characters</em></div>
	</div>
	<div class="spacer"></div>
	</div>
<?php
echo $this->Form->submit("Submit Sold");
echo $this->Form->end();
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#earning_precent').change(function(){
		var earning_percent=$(this).val();
		var sell_drobe_id=<?php echo $drobe['id']?>;
		$.ajax({
			  url: '<?php echo $this->Html->url(array("controller"=>"sell_drobes","action"=>'calculate_seller_earning',"admin"=>true))?>',
			  data: {earning_percent: earning_percent, sell_drobe_id: sell_drobe_id},
			  type: "POST",
			  success: function(data) {
				  obj=$.parseJSON(data);
				  if(obj.type=="success") $('#seller_earning_amount').val(obj.amount);
				  else alert(obj.error);
			  }
		});
	});
});
</script>
