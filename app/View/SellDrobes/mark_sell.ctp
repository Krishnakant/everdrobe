<div class="actions right">
<?php 
echo $this->Html->link("Back to Drobe List",array("action"=>"drobes"));
?>
</div>
<h2>Drobe for Sale</h2>
<div class="tp tp_big drobe_result">
 <?php echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name'],array('alt'=>$this->data['Drobe']['file_name'],'height'=>"85", 'width'=>"85")); ?>
         <div class="user_cont">
         	<div class="cont"><?php echo $this->data['Drobe']['comment']=="" ? "" : $this->data['Drobe']['comment']." <br />";?><em><?php echo $categories[$this->data['Drobe']['category_id']]?></em></div>
         	<div class="clear"></div>
            <div class="up"><?php echo $this->data['Drobe']['total_in'];?></div> 
            <div class="down"><?php echo $this->data['Drobe']['total_out'];?></div> 
            <div class="whatever">Total Votes: <?php echo $this->data['Drobe']['total_rate']?></div>  
         </div>
</div>
<div class="spacer"></div>
<?php
echo $this->Form->create("SellDrobe");
?>
<div class="drobe_sale_popup">
	<div class="half left">
	<?php 
		echo $this->Form->input('SellDrobe.sell_brand_name',array("label"=>"<b>Brand Name: </b>","class"=>"comment","id"=>"brand_name"));
	?>
	<div class="right"><em>Max 140 Characters</em></div>
	<div class="spacer"></div>
	<div class="input select">
	<?php $sizeList['Other']='Other';
		echo $this->Form->input('SellDrobe.size_id', array("label"=>"<b>Size:</b>","options"=>$sizeList,"id"=>"drobe_size",'class'=>"drob_size_custom",'empty'=>"Select Drobe Size",'div'=>false));
		if($sizeName=array_search($this->data['SellDrobe']['size_name'],$sizeList) || $this->data['SellDrobe']['size_name']=='')
		{
			
		}
		else
		{
			$sizeName=$this->data['SellDrobe']['size_name'];
			echo "<div class='spacer'></div><input type='text' id='drobe_size_name' name='data[SellDrobe][size_name]' value='".$sizeName."' />";
		}
	?>
	</div>
	<?php 
		//echo $this->Form->input('SellDrobe.size_id', array("label"=>"<b>Size:</b>","options"=>$sizeList,"id"=>"drobe_size",'empty'=>"Select Drobe Size"));
	?>
	<div class="spacer"></div>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SellDrobe.sell_price', array("place","label"=>"<b>Price in USD:</b>","type"=>"text","id"=>"drobe_price"));
		
		/*
	?>
	<div class="spacer"></div>
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SellDrobe.tracking_number', array("place","label"=>"Tracking Number:","type"=>"text","id"=>"drobe_price"));
		*/
	?>
	<div class="spacer"></div>
	<div class="spacer"></div>
	<div class="text"><?php echo Configure::read('setting.drobe_sale_text');?></div>
	</div>
	<div class="half right drobe_description">
	<?php 
		echo $this->Form->label("<b> Description: </b> <em>(Optional)</em>");
		echo $this->Form->textarea('SellDrobe.sell_description',array('lable'=>"false", "id"=>"drobe_description"));
	?>
	<div><em>Max 300 Characters</em></div>
	</div>
	<div class="spacer"></div>
	</div>
<?php
echo $this->Form->input("Drobe.unique_id",array("type"=>"hidden"));
echo $this->Form->input("SellDrobe.id",array("type"=>"hidden"));
echo $this->Form->submit("Mark for Sale");
?>
