<h2>Moderate Images for Sell Drobe</h2>
<p>Here display all sell drobe images with additional images which drobes are active.</p>
<p>Click on thumb for view enlarge image. Right click on thumb for display action menu popup.</p>  
<div class="spacer"></div>
<div class="moderator_box">
	<div class="drobes flexcroll" id="normal_drobes">
	<div>
	<?php 
	if(count($images)>0)
	{
			foreach($images as $image)
			{
				if($image['type']=="drobe")
				{
					echo  $this->Html->link( $this->Html->image('/drobe_images/thumb/'.$image['file_name'],array('height'=>"51px", 'width'=>"51px")),
							'/drobe_images/'.$image['file_name'],
							array("border"=>0,'escape'=>false,"class"=>"example8 drobe_thumb_small", "rel"=>$image['drobe_id']."::".$image['user_id']."::0"));
				}
				else
				{ 
					echo  $this->Html->link( $this->Html->image('/drobe_images/additional/thumb/'.$image['file_name'],array('height'=>"51px", 'width'=>"51px")),
						'/drobe_images/additional/'.$image['file_name'],
						array("border"=>0,'escape'=>false,"class"=>"example8 drobe_thumb_small", "rel"=>$image['drobe_id']."::".$image['user_id']."::".$image['sell_drobe_id']));
				}
			}
		
	}
	else
	{
		?>
		<?php 
	}
	?>
	<div class="clear"></div>
	</div>
	</div>
</div>
<div id="user_options" style="display: none;">
	<ul class="submenu actions">
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"sell_drobes","action"=>"edit","admin"=>true));?>/" class="edit_sell_detail">Edit ShellDetail</a></li>
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"drobes","action"=>"edit","admin"=>true));?>/" class="edit_drobe_detail">Edit  Drobe Detail</a></li>
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"drobes","action"=>"action","unmark_sale","admin"=>true));?>/" class="unmark_sell">Unmark from Sell</a></li>
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"drobes","action"=>"action","close","admin"=>true));?>/" class="close_rate">Close for Rate</a></li>
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"drobes","action"=>"action","delete","admin"=>true));?>/" class="remove_drobe">Remove Drobe</a></li>
		<li><a href="#" rel="<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","block","admin"=>true));?>/" class="block_user">Block User</a></li>
	</ul>
	<div class="clear"></div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.example8').bind('contextmenu',function(e){
	  	e.preventDefault();
	  	var drobe_id=$(this).attr("rel").split("::")[0];
	  	var user_id=$(this).attr("rel").split("::")[1];
	  	var sell_drobe_id=$(this).attr("rel").split("::")[2];
	  	
		$('#user_options a.edit_sell_detail').attr("href",$('#user_options a.edit_sell_detail').attr('rel')+drobe_id);
		$('#user_options a.edit_drobe_detail').attr("href",$('#user_options a.edit_drobe_detail').attr('rel')+drobe_id);
		$('#user_options a.unmark_sell').attr("href",$('#user_options a.unmark_sell').attr('rel')+drobe_id);
		$('#user_options a.close_rate').attr("href",$('#user_options a.close_rate').attr('rel')+drobe_id);
		$('#user_options a.remove_drobe').attr("href",$('#user_options a.remove_drobe').attr('rel')+drobe_id);
		$('#user_options a.block_user').attr("href",$('#user_options a.block_user').attr('rel')+user_id);

		$(this).qtip({
			//position : { my: 'top right', at: 'bottom right' } ,
			content: {
				text: $('#user_options').html(), // Use the submenu as the qTip content
			},
			show: {
				event: 'click',
				ready: true // Make sure it shows on first mouseover
 			},
			hide: {
				delay: 100,
				event: 'unfocus mouseleave',
				fixed: true // Make sure we can interact with the qTip by setting it as fixed
			},
 			style: {
				classes: 'ui-tooltip-nav ui-tooltip-light', // Basic styles
				tip: true // We don't want a tip... it's a menu duh!
			},
		});
	    return false;
     });
});
	</script>