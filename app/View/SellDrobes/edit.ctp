<?php
echo $this->Form->create("SellDrobe");
?>
<div class="actions right">
<?php 
if($this->Session->read('Auth.User.type')!="admin")
{
	echo $this->Html->link("Back to Sell Drobe List",array("action"=>"sell_drobes"));
}
else echo $this->Html->link("Back to Sell Drobe List",array("controller"=>"drobes","action"=>"sell_drobes"));
?>
</div>
<h2> Manage Drobe for Sale</h2>
<div class="spacer"></div>
<div id="my_drobe_area">
<div class="half left editSellDrobeLeft" >
	<div style="position:relative" id="sell_drobe_preview">
		
	<?php  
		echo $this->Html->image('/drobe_images/'.$this->data['Drobe']['file_name'],array('width'=>"350px",'border'=>0, 'id'=>"crop_image" ,"alt"=>"drobe prview Image")); 
	?>
	</div>
	<div class="drobe_upload_msg" id="file-uploader"></div>
</div>
<div class="half right drobe_description drobe_sale_popup">
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input("Drobe.category_id",array('id'=>'drobe_category_id_sell','options'=>$categories,"style"=>"width:280px",'label'=>'<b>Category :</b>')); 
	?>
	
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input("Drobe.comment",array("style"=>"width:270px; height:75px;",'rows'=>4,'id'=>'sell_drobe_comment','label'=>'<b>Comment :</b>')); 
	?>
</div>
<div class="spacer"></div>
<h3>Additional Images</h3>
<div class="sell_drobe_images">
<div class="image_box" id="sell_drobe_image_1"></div>
<div class="image_box" id="sell_drobe_image_2"></div>
<div class="image_box" id="sell_drobe_image_3"></div>
<div class="clear"></div>
</div>
<div class="spacer"></div>


<div class="spacer"></div>
<input type="hidden" id="pos_left" name="pos_left" />
<input type="hidden" id="pos_top" name="pos_top" />
<input type="hidden" id="new_width" name="new_width" />
<input type="hidden" id="new_height" name="new_height" />
<input type="hidden" name="data[Drobe][file_name]" id="DrobeFileName" value="<?php echo $this->data['Drobe']['file_name'];?>" />
<input type="hidden" name="old_file" id="sell_drobe_file_name" value="<?php echo $this->data['Drobe']['file_name'];?>" />

<div class="drobe_sale_popup">
	<div class="half left">
	
	
	<?php 		
	//	echo $this->Form->input('SellDrobe.sell_brand_name',array("label"=>"<b>Brand Name: </b>","class"=>"comment","id"=>"brand_name"));
	?>
	
	
	<div class="input select">
	<?php $sizeList['Other']='Other';
		
		$sizeName=array_search($this->data['SellDrobe']['size_name'],$sizeList);
		if($sizeName != '')
		{
			echo $this->Form->input('SellDrobe.size_id', array('type'=>'select','default'=>$sizeName,"label"=>"<b>Size:</b>","options"=>$sizeList,"id"=>"drobe_size",'class'=>"drob_size_custom",'empty'=>"Select Drobe Size",'div'=>false));
		}
		else
		{
			echo $this->Form->input('SellDrobe.size_id', array('type'=>'select','default'=>'Other',"label"=>"<b>Size:</b>","options"=>$sizeList,"id"=>"drobe_size",'class'=>"drob_size_custom",'empty'=>"Select Drobe Size",'div'=>false));
			$sizeName=$this->data['SellDrobe']['size_name'];
			echo "<div class='spacer'></div><input type='text' id='drobe_size_name' name='data[SellDrobe][size_name]' value='".$sizeName."' />";
		}
		
	?>
	</div>
	
	<div class="input select">
	<?php $brandList['Other']='Other';
	
		$brandName=array_search($this->data['SellDrobe']['sell_brand_name'],$brandList);
		if($brandName!="")
		{
			echo $this->Form->input('SellDrobe.brand_id', array("label"=>"<b>Brand:</b>",'default'=>$brandName,"options"=>$brandList,"id"=>"drobe_brand",'class'=>'drobe_brand_custom','empty'=>"Select Brand Name",'div'=>false));
		}
		else
		{
			echo $this->Form->input('SellDrobe.brand_id', array("label"=>"<b>Brand Name :</b>",'default'=>'Other',"options"=>$brandList,"id"=>"drobe_brand",'class'=>'drobe_brand_custom','empty'=>"Select Brand Name",'div'=>false));
			$brandName=$this->data['SellDrobe']['sell_brand_name'];
			echo "<div class='spacer'></div><input type='text' id='drobe_brand_name' name='data[SellDrobe][sell_brand_name]' value='".$brandName."' />";
		}	
		
	?>
	</div>
	
	<div class="spacer"></div>
	<div style="width: 160px; float: left !important;">
		<?php
		echo $this->Form->label("<b> This is for :</b>"); 
		echo $this->Form->input('SellDrobe.sell_gender', array('before' => '','after' => '', 'between' => '', 'separator' => '','legend'=>false, 'options' => array('male'=>"Male", 'female'=>'Female','everyone'=>"Everyone"),'type' => 'radio','default'=>"everyone"));
		?>
	</div>
	<div style="float: left !important; clear: none !important; ">
		<?php
		echo $this->Form->label("<b>Brand New/ Used :</b>"); 
		echo $this->Form->input('SellDrobe.is_brand_new', array('before' => '','after' => '', 'between' => '', 'separator' => '','legend'=>false, 'options' => array('new'=>"New", 'used'=>'Used'),'type' => 'radio','default'=>"new"));
		?>
	</div>
	
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SellDrobe.original_sell_price', array("place","label"=>"<b>Original Price in USD:</b>","type"=>"text","id"=>"original_drobe_price"));
	?>
	
	<div class="spacer"></div>
	<?php 
		echo $this->Form->input('SellDrobe.sell_price', array("place","label"=>"<b>Listing Price in USD:</b>","type"=>"text","id"=>"drobe_price","autocomplete"=>"off"));
	?>
	
	<div class="spacer"></div>
	<?php 
		//echo $this->Form->input('SellDrobe.tracking_number', array("place","label"=>"<b>Tracking Number:</b>","type"=>"text","id"=>"drobe_price"));
	echo $this->Form->input('your_earning', array('label'=>'<b> Your Earning</b> (when sold)',"type"=>"text",'readonly'=>'readonly','id'=>'your_earning','name'=>'your_earning'));
	echo $this->Form->input('earning_percent',array('type'=>'hidden','value'=>$earning_percent,'id'=>'earning_percent','name'=>'earning_percent'));
	?>
	
	<div class="spacer"></div>
	
	<div class="spacer"></div>
	<div class="spacer"></div>
	<div class="text"><?php echo Configure::read('setting.drobe_sale_text');?></div>
	</div>
	<div class="half right drobe_description">
	<?php 
		echo $this->Form->label("<b> Description: </b> <em>(Optional)</em>");
		echo $this->Form->textarea('SellDrobe.sell_description',array('lable'=>"false", "id"=>"drobe_description"));
	?>
	<div><em>Max 300 Characters</em></div>
	</div>
	<div class="spacer"></div>
	</div>
	
	
<?php
echo $this->Form->input("Drobe.unique_id",array("type"=>"hidden"));
echo $this->Form->input("SellDrobe.id",array("type"=>"hidden"));
for($i=0;$i<3;$i++)
{
	echo $this->Form->input("SellDrobeImage.".$i.".id",array("type"=>"hidden"));
	echo $this->Form->input("SellDrobeImage.".$i.".sell_drobe_id",array("type"=>"hidden"));
	echo $this->Form->input("SellDrobeImage.".$i.".file_name",array("type"=>"hidden","rel"=>"sell_drobe_image_".($i+1)));
}
echo $this->Form->submit("SaveChanges",array('id'=>'drobe_edit_btn_sell'));
echo $this->Form->end();
?>
</div>
<script type="text/javascript">
var is_normal_drobe = false;
var last_uploaded_response=null;
var is_google_image = false;   //This variable is used to check image upload from google search if image from google search then its value is true other wise false

/****************Aviary image upload code ***********************************/
	var featherEditor = new Aviary.Feather({
	    apiKey: '<?php echo Configure::read('aviary_key'); ?>',
	    apiVersion: 3,
	    theme: 'dark', // Check out our new 'light' and 'dark' themes!
	    tools: 'all',
	    appendTo: '',
	    onSave: function(imageID, newURL) {
	    	upload_image_from_url(newURL,false);
	        return true;
	    },
	    onClose:function()
	    {
		    display_image_cropping(last_uploaded_response);
	    	  
	    },
	    onError: function(errorObj) {
	        alert(errorObj.message);
	    }
	});



var fileUploadSettings={
	element: null,
    uploadButtonText: "Click here to upload Additional Image",
    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'sell_drobes','admin'=>false)); ?>',
    params: {type:'sell_drobes'},
    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
    debug: false,
    sizeLimit:4097152,
   onCancel: function(id, fileName){},
	messages: {
		typeError: "You can upload only JPEG, PNG or GIF Image file.",
	    sizeError: "You can upload less then 2MB file size.",
	    emptyError: "Uploaded file is empty.",
	 },
	showMessage: function(message){ 
		 tip_message('#file-uploader',message);
	}
};
function blockUploader(el)
{
	el.block({ 
		message: "<b>Uploading Image</b>",
    	css: {
        	border: 'none', 
            padding: '10px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px !important', 
            '-moz-border-radius': '10px !important', 
            opacity: .5, 
            color: '#fff'
        }
   	}); 
}
function fileUploadCompleteAction(el, obj)
{
	if(obj.type=="success") showAdditionalImage(el, obj.file);
    else $('input[rel="'+el.attr('id')+'"]').val("");
}
function showAdditionalImage(el, file_name)
{
	$('input[rel="'+el.attr('id')+'"]').val(file_name);
	el.prepend('<div class="image_preview"><div class="remove">X</div><img src="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('sell_drobe.thumb.upload_dir'))); ?>/'+file_name+'" alt="no preview available" /></div>');
	//el.find('.image_preview>img').attr('src','<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('sell_drobe.thumb.upload_dir'))); ?>'+obj.file);
	el.find('.image_preview').show();
	el.find('.qq-uploader').hide();
	el.unblock();
	el.find('.remove').each(function(){
		$(this).unbind('click').bind('click',function(){
			if(confirm('Are you sure to remove this Image from Additional Image?'))
			{
				el.find('.qq-uploader').fadeIn('fast',function(){el.find('.image_preview').remove()});
				$('input[rel="'+el.attr('id')+'"]').val("");
			}
			return false;
		});
	});
}

var fileUploadComplete=new Array();
fileUploadComplete[0]=function(id, fileName, responseJSON){fileUploadCompleteAction($('#sell_drobe_image_1'),responseJSON);};
fileUploadComplete[1]=function(id, fileName, responseJSON){fileUploadCompleteAction($('#sell_drobe_image_2'),responseJSON);};
fileUploadComplete[2]=function(id, fileName, responseJSON){fileUploadCompleteAction($('#sell_drobe_image_3'),responseJSON);};

var fileUploadSubmit=new Array();
fileUploadSubmit[0]=function(id,fileName){blockUploader($('#sell_drobe_image_1'));}; 
fileUploadSubmit[1]=function(id,fileName){blockUploader($('#sell_drobe_image_2'));}; 
fileUploadSubmit[2]=function(id,fileName){blockUploader($('#sell_drobe_image_3'));}; 

$(document).ready(function(){
	var uploader= new Array();
	for(var i=0; i<3;i++)
	{
		fileUploadSettings.element = $('#sell_drobe_image_'+(i+1))[0];
		fileUploadSettings.onSubmit= fileUploadSubmit[i];
		fileUploadSettings.onComplete= fileUploadComplete[i];
		uploader.push(new qq.FileUploader(fileUploadSettings));


		$('.qq-upload-button input[type="file"]').livequery(function(){
			$(this).height($(this).closest('.image_box').height());
		})
			
		<?php 
		$i=1;
		foreach($this->data['SellDrobeImage'] as $drobe_image)
		{
			echo "showAdditionalImage($('#sell_drobe_image_".$i."'),'".$drobe_image['file_name']."');";
			$i++;
		}
		?>
	}

	
		// getting earning pefcentage from configuration file
		var earning_percent = $('#earning_percent').val();
		var earn_money = $('#drobe_price').val() - (($('#drobe_price').val() * earning_percent ) / 100);
		$('#your_earning').val("$" + earn_money.toFixed(2));
		
		//calculate the seller earning after the admin cut his profit from seller listing value
		$(document).on('keyup','#drobe_price',function(){			
		var earn_money = $('#drobe_price').val() - (($('#drobe_price').val() * earning_percent ) / 100);
		$('#your_earning').val("$" + earn_money.toFixed(2));
		});
	
});





// This code for edit sell drobe main image @sadikhasan
var max_width=450;
var max_height=450;
var display_ratio=1;
var drobe_image_dir="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.upload_dir')),true).'/'; ?>";
var drobe_thumb_image_dir="<?php echo $this->Html->url('/'.str_replace(DS,'/',Configure::read('drobe.thumb.upload_dir'))).'/'; ?>";

var uploader = new qq.FileUploader({
    element: $('#file-uploader')[0],
    uploadButtonText: "Click here to upload Image",
    action: '<?php echo $this->Html->url(array('controller'=>"uploads","action"=>"file",'original')); ?>',
    params: {type:'drobes'},
    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
    debug: false,
    sizeLimit:4097152,
    onSubmit: function(id, fileName){
        // $("#file-uploader").hide();
        $('#sell_drobe_preview').block({ 
			message: "<h3>Uploading Image</h3>",
	    	css: { 
	            border: 'none', 
	            padding: '10px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px !important', 
	            '-moz-border-radius': '10px !important', 
	            opacity: .5, 
	            color: '#fff' 
        } });

        /*	Checking if image already uploaded then remove croping from image for other image uploading */
    	if(typeof jcrop_api != 'undefined')
        {
            jcrop_api.destroy();
        }
	  },
    onComplete: function(id, fileName, responseJSON){
        upload_response(responseJSON);
},
	onCancel: function(id, fileName){},
	messages: {
		typeError: "You can upload only JPEG, PNG or GIF Image file.",
        sizeError: "You can upload less then 2MB file size.",
        emptyError: "Uploaded file is empty.",
	 },
	 showMessage: function(message){ 
		 tip_message('#file-uploader',message);
	}
});

function upload_response(obj)
{
	last_uploaded_response = obj;
	if(obj.type=="success")
    {
		display_image_cropping_new(obj);
    }
    else
    {  
        alert(obj.message);
    	$('#DrobeFileName').val("");
    	$('#drobe_upload_btn').attr('disabled','disabled');
    }
}
function display_image_cropping_new(response)
{
	$('#DrobeFileName').val(response.file);
    $('#crop_image').attr('src',drobe_image_dir + response.file);
    
    
	var width= response.size.width;
	var height= response.size.height;

    if(max_width < width)
    {
    	$('.drobe_preview img').attr("width",max_width).removeAttr("height");
    	height=height/(width/max_width);
    	if(max_height < height) $('.drobe_preview img').attr("height",max_height).removeAttr("width");
    }
    if(max_height < height)
    {
         $('.drobe_preview img').attr("height",max_height).removeAttr("width");
    }
         
    $('#crop_image').load(function(){
    	try
    	{
        	
	    	// display image cropping area with image after loading a image;
	    	$('#file_uploader').hide();
	    	$('.drobe_preview').show().find('.big-img').css({'background':"#EDEDED"});

	    	img_width=$(this).width();
	    	img_height=$(this).height();
	    	
	    	
	    	var src = $("#crop_image").attr("src");
	    	//Call avaiary image function launchEditor for displaying image with its effects
	    	featherEditor.launch({
		        image: "crop_image",
		        url: src
		    });
			$(this).unbind();
	    	
	    	/* $('#sell_drobe_crop_image').Jcrop({
		    	aspectRatio: 1,
	        	minSize: [ 320, 320],
	        	onSelect: updateCoords,
	        	trueSize: [response.size.width,response.size.height]
	        	},
	        	function(){
		        	bounds = this.getBounds();
	            	boundx = bounds[0];
	            	boundy = bounds[1];
	            	jcrop_api = this;
	            	if(boundx>boundy)
	            	{
	            		left_cords= parseInt((boundx-boundy)/2);
	            		jcrop_api.animateTo([left_cords,0,boundx,boundy]);
	            		updateCoords({x:left_cords,y:0,w:boundy,h:boundy});
	            	}
	            	else
	            	{
	            		top_cords= parseInt((boundy-boundx)/2);
	            		jcrop_api.animateTo([0,top_cords,boundx,boundy]);
	            		updateCoords({x:0,y:top_cords,w:boundx,h:boundx});
	            	}
	        }); */
    	}
    	catch(e)
    	{
    		alert(e.valueOf());
    	}
    	
    });
   // $.colorbox.close();
}

/** Submitting form disable from user when click on submit button */
$("#SellDrobeEditForm").submit(function(){
	$('#sell_drobe_area').block({ 
		message: "<h3>Submitting</h3>",
    	css: { 
            border: 'none', 
            padding: '10px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px !important', 
            '-moz-border-radius': '10px !important', 
            opacity: .5, 
            color: '#fff' 
    	}
    });
}); 


</script>
