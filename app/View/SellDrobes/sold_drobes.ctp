<h2>Sold Drobes</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search Drobe : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search drobe by comment, user name, category" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($drobes)>0)
{
	?>
	<table>
		<tr>
			<th>Image</th>
			<th width="200px"><?php echo $this->Paginator->sort('Drobe.comment','Comment');?></th>
			<th><?php echo $this->Paginator->sort('Drobe.uploaded_on','Posted On');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.sell_brand_name','Brand Name');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.sell_price','Sell Price');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.sold_items','Sold Items');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.total_paid','Total Revenue');?></th>
		</tr>
		<?php 
		foreach($drobes as $drobe)
		{
			?>
			<tr class="<?php echo $drobe['Drobe']['id']; ?>">
				<td width="90px"><?php echo $this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));?></td>
				<td width="200px"><?php
					if($drobe['Drobe']['comment']!="") echo "<div class='comment'>".$drobe['Drobe']['comment']."</div>";
					if($drobe['Category']['category_name']!="") echo "<em>(".$drobe['Category']['category_name'].")</em>";
				?></td>
				<td><?php echo $this->Time->niceShort($drobe['Drobe']['uploaded_on']);?></td>
				<td align="center"><?php echo $drobe['SellDrobe']['sell_brand_name'];?></td>
				<td align="center"><?php echo $drobe['SellDrobe']['sell_price']." $";?></td>
				<td align="center"><?php echo $drobe['SellDrobe']['sold_items'];?></td>
				<td align="center"><?php echo $drobe['SellDrobe']['total_paid']." $";?></td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="featured_drobe" style="display: none;" class="featured_form">
	<h2>Mark as Featured</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Drobe: </strong><span id="featured_drobe_name"></span></div>
		
		<?php 
			echo $this->Form->input('buy_url',array('id'=>'buy_url','label'=>false,'div'=>false,'placeholder'=>"Enter URL for buy drobe"));
			echo $this->Form->hidden("featured_drobe_id",array("id"=>"featured_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_featured"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_featured"));
?>
<div class="spacer"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	})
});
</script>