<h2 style="float:left; padding-right: 300px;">
	My Balance 	
</h2>
<div class="actions">
		 <?php //echo $this->Html->link("Redemption History",array(''),array("onclick"=>"window.open('".$this->Html->url(array('action'=>'redemption_history'))."','','width=915,height=400,scrollbars=yes')")); ?>
		 <?php echo $this->Html->link("Redemption History",'javascript:void(0)' ,array('class'=>'redemption_history')); ?>
		 <?php echo $this->Html->link("Redeem Request",array("controller"=>"redeem_requests" ,"action"=>"my_redeem_request")); ?>
	</div>
<?php
if(!$sellerProfile || $sellerProfile['SellProfile']['full_name']=="" || $sellerProfile['SellProfile']['city']=="" || $sellerProfile['SellProfile']['zip']=="")
{
	?>
	<div class="warning">
		<b>WARNING!</b> Your profile not completed, you can not get you earning untill complete your seller profile. <a href="<?php echo $this->Html->url(array("controller"=>"sell_profiles","action"=>"sell_profile"));?>">Click here</a> to complete profile.
	</div>
	<?php 
}
?>
<div class="spacer"></div>
<div class="dashboard_box half left">
	<div class="title">Active  Drobes for Sale (Total : <?php echo count($activeSellDrobes);?>)</div>
	<div class="drobes flexcroll" id="sell_drobes">
	<div>
	<?php
	$active_drobe_amount=0;
	if(count($activeSellDrobes)>0)
	{
		foreach($activeSellDrobes as $drobe)
		{
			$active_drobe_amount += $drobe['SellDrobe']['sell_price'];
			echo  $this->Html->link(
					$this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],
							array('alt'=>$drobe['Drobe']['comment'],'height'=>"51px", 'width'=>"51px")),
					"javascript:void(0)",
					array("border"=>0,'escape'=>false,"class"=>"drobe_thumb_small"));
		}
	}
	else
	{
		?>
		<p class="empty_message">Active drobe for sell not found.</p>
		<?php 
	}
	?>
	<div class="clear"></div>
	</div>
	</div>
	<div class="amount">Amount of Active Drobe</div>
	<div class="doller"><?php echo "$ ".sprintf('%0.2f',floatval($active_drobe_amount));?></div>
</div>
<div class="dashboard_box half right">
	<div class="title">Sold Drobes (Total : <?php echo count($soldDrobes);?>)</div>
	<div class="drobes flexcroll" id="sold_drobes">
	<div>
	<?php 
	$total_revenue=0;
	if(count($soldDrobes)>0)
	{
		foreach($soldDrobes as $drobe)
		{
			$total_revenue += $drobe['SellDrobe']['sell_price'];
			echo  $this->Html->link(
					$this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],
							array('alt'=>$drobe['Drobe']['comment'],'height'=>"51px", 'width'=>"51px")),
					"javascript:void(0)",
					array("border"=>0,'escape'=>false,"class"=>"drobe_thumb_small"));
		}
	}
	else
	{
		?>
			<p class="empty_message">Sold drobes not found.</p>
			<?php 
		}
		?>
		<div class="clear"></div>
		</div>
	</div>
	<div class="amount">Total Revenue</div>
	<div class="doller"><?php echo "$ ".sprintf('%0.2f',floatval($totalRevenue));?></div>
</div>
<div class="clear"></div>
<!--  
<div class="dashboard_box">
	<p align="center"><?php echo Configure::read('setting.seller_profile_text');?></p>
	<div class="doller"><span style="opacity:0.7">Total :</span> <?php //echo sprintf('%0.2f',floatval($totalBalance))." $";?></div>
</div>
-->

<div class="dashboard_box">	
	<div class="doller"><span style="opacity:0.7">For Sale :</span> <?php echo "$ ".sprintf('%0.2f',floatval($active_drobe_amount));?></div>
</div>

<!-- 
<div class="dashboard_box">	
	<div class="doller"><span style="opacity:0.7">Credit :</span> <?php //echo "$ ".sprintf('%0.2f',floatval($creditBalance));?></div>
</div>
-->


<div class="dashboard_box">	
	<div class="doller"><span style="opacity:0.7">Pending :</span> <?php echo "$ ".sprintf('%0.2f',floatval($pendingBalance));?></div>
</div>

<div class="dashboard_box">	
	<div class="doller"><span style="opacity:0.7">Redeemable :</span> <?php echo "$ ".sprintf('%0.2f',floatval($redeemableBalance));?></div>
</div>





<div class="clear"></div>
<div class="dashboard_box">
	<div class="title">Active Normal Drobes (Total : <?php echo count($totalDrobes);?>)</div>
	<div class="drobes flexcroll" id="normal_drobes">
	<div>
	<?php 
	if(count($totalDrobes)>0)
	{
		foreach($totalDrobes as $drobe)
		{
			echo  $this->Html->link(
					$this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],
							array('alt'=>$drobe['Drobe']['comment'],'height'=>"51px", 'width'=>"51px")),
					"javascript:void(0)",
					array("border"=>0,'escape'=>false,"class"=>"drobe_thumb_small"));
		}
	}
	else
	{
		?>
		<p class="empty_message">Active drobe not found which is posted by you.</p>
		<?php 
	}
	?>
	<div class="clear"></div>
	</div>
	</div>
</div>


<div class="view_redemption_history" style="display: none"; rel="<?php echo $full_name;?>">
<table>
<?php 
	if(!empty($redemption_history))
	{
?>
	<tr>
	<th>Payment Date</th>
	<th>Payment to Seller</th>
	<th>Remark</th>
	</tr>
	<?php
	$total_payment = 0;
	foreach ($redemption_history as $history)
	{
		$total_payment += $history['SellerPayment']['payment'];
	?>
	  <tr>
	    <td><?php echo date('Y-m-d', strtotime($history['SellerPayment']['created_on']))?></td>
	    <td><?php echo "$ ".$history['SellerPayment']['payment']?></td>
	    <td><?php echo $history['SellerPayment']['remark']?></td>
	  </tr>
	<?php 
	}
	?>
		<tr>
			<th>Total Payment </th>
			<th><?php echo "$ ".$total_payment;?></th>
			<th></th>
		</tr>
<?php 
	}
	else 
	{
?>
	<tr>
		<td colspan="3" style="text-align:center;color:red"><b>Payment history not avalilable !!!</b></td>
	</tr>	
<?php 		
	}
?>		
</table>
</div>


<script>
	$(document).ready(function(){

		$(".redemption_history").click(function()
		{
			$.colorbox({html:$(".view_redemption_history").html()});
			
		});
	});
</script>