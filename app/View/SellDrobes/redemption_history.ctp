<h2>
Redemption History of <?php echo $full_name; ?>
</h2>
<table>
<?php 
	if(!empty($redemption_history))
	{
?>
	<tr>
	<th>Payment Date</th>
	<th>Payment to Seller</th>
	<th>Remark</th>
	</tr>
	<?php
	$total_payment = 0;
	foreach ($redemption_history as $history)
	{
		$total_payment += $history['SellerPayment']['payment'];
	?>
	  <tr>
	    <td><?php echo date('Y-m-d', strtotime($history['SellerPayment']['created_on']))?></td>
	    <td><?php echo "$ ".$history['SellerPayment']['payment']?></td>
	    <td><?php echo $history['SellerPayment']['remark']?></td>
	  </tr>
	<?php 
	}
	?>
		<tr>
			<th>Total Payment </th>
			<th><?php echo "$ ".$total_payment;?></th>
			<th></th>
		</tr>
<?php 
	}
	else 
	{
?>
	<tr>
		<td colspan="3" style="text-align:center;color:red"><b>Payment history not avalilable !!!</b></td>
	</tr>	
<?php 		
	}
?>		
</table>