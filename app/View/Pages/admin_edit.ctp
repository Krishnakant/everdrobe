<h2>Edit : <?php echo $this->data['Page']['page_title']?></h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="spacer"></div>
<?php

echo $this->Form->create("Page");
echo $this->Form->input("page_name",array('disabled'=>'disabled'));
echo $this->Form->input("page_title");
echo $this->Form->input("page_content");
echo $this->Form->hidden('id');
?>
<div class="spacer"></div>
<?php

echo $this->Form->submit('Update');
echo $this->Form->end();
?>
<script type="text/javascript">
		//<![CDATA[
			// Replace the <textarea id="editor1"> with an CKEditor instance.
			<?php if(!in_array($this->data['Page']['page_name'],array("google_analytics","meta_keywords","meta_description","google_adsense","google_conversion_tracking")))
			{
				?>
				var editor = CKEDITOR.replace( 'PagePageContent' , {

			        filebrowserBrowseUrl : '<?php echo Router::url("/ckeditor/ckfinder/ckfinder.html",true);?>',
			        filebrowserImageBrowseUrl : '<?php echo Router::url("/ckeditor/ckfinder/ckfinder.html?Type=Images",true);?>',
			        filebrowserFlashBrowseUrl : '<?php echo Router::url("/ckeditor/ckfinder/ckfinder.html?Type=Flash",true);?>',
			        filebrowserUploadUrl : '<?php echo Router::url("/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files",true);?>',
			        filebrowserImageUploadUrl : '<?php echo Router::url("/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images",true);?>',
			        filebrowserFlashUploadUrl : '<?php echo Router::url("/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash",true);?>',
				    filebrowserWindowWidth  : 800,
				    filebrowserWindowHeight : 500,
				    toolbar : 'Custom'
				});
					
				<?php
			}		
			?>
					//]]>
</script>