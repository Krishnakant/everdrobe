<h2>Content Pages</h2>
<div class="spacer"></div>
<?php
if(count($pageList)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th>Page Name</th>
			<th>Page Title</th>
			<th>Actions</th>
		</tr>
		<?php 
		foreach($pageList as $notify)
		{
			?>
			<tr>
				<td><?php echo $notify['Page']['id'];?></td>
				<td><?php echo $notify['Page']['page_name'];?></td>
				<td><?php echo $notify['Page']['page_title'];?></td>
				<td>
				<div class="actions">
				<?php 
					echo $this->Html->link('Edit',array('controller'=>'pages','action'=>'edit',$notify['Page']['id'],'admin'=>true)). " "; 
				?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>