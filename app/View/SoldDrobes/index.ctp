<h2>My Sales</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Search Drobe : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search drobe by comment or category name" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($drobes)>0)
{
	?>
	<table>
		<tr>
			<th>Image</th>
			<th><?php echo $this->Paginator->sort('Drobe.comment','Comment');?></th>
			<th><?php echo $this->Paginator->sort('SoldDrobe.sold_on','Sold On');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.sell_brand_name','Brand Name');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.order_id','Order ID');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.sell_price','Listing Price');?></th>
			<th><?php echo $this->Paginator->sort('SellDrobe.total_paid','Total Revenue');?></th>
		</tr>
		<?php 
		foreach($drobes as $drobe)
		{
			?>
			<tr class="<?php echo $drobe['SoldDrobe']['id']; ?>">
				<td width="90px" rowspan="2"><?php echo $this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image"));?></td>
				<td><?php
					if($drobe['Drobe']['comment']!="") echo "<div class='comment'>".$drobe['Drobe']['comment']."</div>";
					echo "<em>(".$categoryList[$drobe['Drobe']['category_id']].")</em>";
				?></td>
				<td><?php echo $this->Time->niceShort($drobe['SoldDrobe']['sold_on']);?></td>
				<td><?php echo $drobe['SellDrobe']['sell_brand_name'];?></td>
				<td align="right"><?php echo $drobe['SoldDrobe']['order_id'];?></td>
				<td align="right"><?php echo $drobe['SoldDrobe']['sell_price']." $";?></td>
				<td align="right"><?php echo $drobe['SoldDrobe']['paid_amount']." $";?></td>
			</tr>
			<tr>
				<td colspan="100%">
				<div class="actions" style="text-align: right">
				UPS/USPS/Fedex Tracking Number: <?php 
				if($drobe['SoldDrobe']['tracking_number']!="")
				{
					echo "<b>".$drobe['SoldDrobe']['tracking_number']."</b> <div class='clear'></div><br/> ";
					echo $this->Html->link('Change Tracking Number',"javascript:void(0)",array("class"=>"submit_tracking_number",'id'=>$drobe['SoldDrobe']['id'],"rel"=>$drobe['SoldDrobe']['tracking_number']))." ";
				} 
				else 
				{
					?>
					<span style="color:red">Not submitted</span> <div class='clear'></div><br/> 
					<?php
					echo $this->Html->link('Add Tracking Number',"javascript:void(0)",array("class"=>"submit_tracking_number",'id'=>$drobe['SoldDrobe']['id'],"rel"=>$drobe['SoldDrobe']['tracking_number']))." ";
				}
				echo $this->Html->link('View Buyer Address',"javascript:void(0)",array("class"=>"view_buyer_address"))." ";
				echo $this->Html->link('Shipping Label',array("controller"=>"request_for_labels","action"=>"my_request",$drobe['SoldDrobe']['id']),array("class"=>"request_for_labels"))." ";
				echo $this->Html->link('Inquiry',array("controller"=>"seller_problems","action"=>"my_problem",$drobe['SoldDrobe']['id']),array("class"=>"seller_probelms"))." ";
				//request_for_label
				?>
				</div>
				<div class="actions" style="text-align: right">
				<br>
				<?php 
					/*if($drobe['SoldDrobe']['status']=="sold")
					{
						echo $this->Html->link('Sold',array("action"=>"change_status",$drobe['SoldDrobe']['id']),array("class"=>"change_status"),"Are you sure you want to change status sold to shipped?");
					}
					else
					{
						echo "<b>Shipped</b>";
					}*/
				echo $this->Html->link('Detail',array("action"=>"sold_drobe_detail",$drobe['SoldDrobe']['id']),array("class"=>"sold_drobe_detail"));
				?>
				</div>
				
				<div class="buyer_address" style="display: none">
				<?php 
				$shipping_address=json_decode($drobe['SoldDrobe']['shipping_address'],true);
				$address="<address>".Configure::read('sell_drobe.shipping_address_format')."<address>";
				foreach ($shipping_address as $key=>$val)
				{
					$address=str_replace("<".$key.">",$val,$address);
				}
				echo $address;
				?>
				</div>
				<div class="spacer"></div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<div class="pagignator">
	   <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
	</div>
	
	<?php 
} 
else
{
	?>
	<p class="empty">There is no drobe sold from your sell drobes</p> 
	<?php 
}
?>
<div id="tracking_from" style="display: none;" class="featured_form">
	<h2>Sumbit Tracking Number</h2>
	<div class="spacer"></div>
	<div class="name"><strong>Sold Drobe Name: </strong><span id="drobe_name"></span></div>
		<?php 
			echo $this->Form->input('tracking_number',array('id'=>'tracking_number','label'=>false,'div'=>false,'placeholder'=>"Enter Tracking number"));
			echo $this->Form->hidden("sold_drobe_id",array("id"=>"sold_drobe_id"));
		?>
<div class="spacer"></div>
<?php 
echo $this->Form->button('Submit',array( 'type'=>'button', 'id'=>"submit_tracking_number_btn"));
echo $this->Form->button('Cancel',array( 'type'=>'button', 'id'=>"close_tracking_popup"));
?>
<div class="spacer"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	});


	$('.view_buyer_address').each(function(){
		var self = $(this);
			$(this).qtip({
				id: 'modal', // Since we're only creating one modal, give it an ID so we can style it
				content: {
					text: self.closest('td').find('.buyer_address').html(),
					title: {
						text: 'Buyer Address : '+self.closest('td').find('.buyer_address b').text(),
						button: true
					}
				},
				position: {
					my: 'center', // ...at the center of the viewport
					at: 'center',
					target: $(window)
				},
				show: {
					event: 'click', // Show it on click...
					solo: true, // ...and hide all other tooltips...
					modal: true // ...and make it modal
				},
				hide: false,
				style: 'qtip-light qtip-rounded'
		});
	});
	
	$('.submit_tracking_number').click(function(){
		$('#drobe_name').html($(this).closest('tr').prev().find('td:eq(1)').text());
		$("#tracking_number").val($(this).attr("rel"));
		$("#sold_drobe_id").val($(this).attr("id"));
		$.blockUI({ 
	        message: $('#tracking_from'), 
	        css: { 
	            top:  ($(window).height()) /2 + 'px', 
	            left: ($(window).width()-300) /2 + 'px', 
	            width: '500px' 
	        } 
	    }); 
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI); 
	});

	$('#submit_tracking_number_btn').click(function(){
		if($.trim($('#tracking_number').val())!="")
		{
			$.ajax({
					url: '<?php echo $this->Html->url(array("controller"=>"sold_drobes","action"=>"submit_tracking_number"))?>',
					type: "POST",
					data: {
						tracking_number:$.trim($('#tracking_number').val()),
						sold_drobe_id: $.trim($('#sold_drobe_id').val())
					},
					success:function(data) {
						$.unblockUI();
						obj=$.parseJSON(data);
						if(obj.type=="error")
						{
							alert(obj.message);
						}	 
						else document.location.href=document.location.href;
					}
				});
		}
		else
		{
			$('#flag_category').focus();
		}
		return false;
	}); 
	$('#close_tracking_popup').click(function(){$.unblockUI()}); 
});
</script>