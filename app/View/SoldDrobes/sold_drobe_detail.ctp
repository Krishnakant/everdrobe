<?php 
if($drobes)
{
?>
<div class="actions" style="margin-bottom: 5px;float: right">
<?php 
	echo $this->Html->link('Shipping Label',array("controller"=>"request_for_labels","action"=>"my_request",$drobes['SoldDrobe']['id']),array("class"=>"request_for_labels"))." ";
	echo $this->Html->link('Enquiry',array("controller"=>"seller_problems","action"=>"my_problem",$drobes['SoldDrobe']['id']),array("class"=>"seller_probelms"))." ";
?>
</div>
<div class="">
	<table cellspacing="0" cellpadding="0" border="0">
              <tbody>
              <tr>
                <td rowspan="7" width="110px"><?php echo $this->Html->image('/drobe_images/thumb/'.$drobes['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'))); ?></td>
                <td colspan="2"><strong><?php echo $drobes['Drobe']['comment']; ?> </strong></td>
                
              </tr>
              <tr>
                <td width="15%"> 
                	Listing Price : 
                </td>
                <td>
                	<?php echo "$".number_format((float)$drobes['SellDrobe']['sell_price'], 2, '.', '');  ?>
                </td>
              </tr>
              <tr>
                <td>
                	Your Earnings : 
                </td>
                <td>
                	<?php echo "$".number_format((float)$drobes['SellDrobe']['total_paid'], 2, '.', '');  ?>
                </td>
              </tr>
              <tr>
                <td>
                	Size :  
                </td>
                <td>
                	<?php echo $drobes['SellDrobe']['size_name'];  ?>
                </td>
              </tr>
               <tr>
                <td>
               
                </td>
                <td>
                
                </td>
             </tr>
             
            </tbody>
	</table>
</div>

<div class="dashboard_box half left">
<?php 
               $buyer_data = json_decode($drobes['SoldDrobe']['shipping_address']);
               $full_name = $buyer_data->first_name.' '.$buyer_data->last_name;
               $address = $buyer_data->address1.", ".
               (($buyer_data->address2!="")?($buyer_data->address2.", "):"").
               $buyer_data->city.", ".
               $buyer_data->province.", ".
               $buyer_data->zip.", ".
               $buyer_data->country;
?>
<h3>Order Detail</h3>
Order No : <?php echo $drobes['SoldDrobe']['order_id']; ?> <br>
Order Date : <?php echo date("d-M-Y", strtotime($drobes['SoldDrobe']['order_date'])); ?><br>
Buyer Name : <?php echo $full_name; ?><br>
Buyer Address : <?php echo $address; ?>	<br>
</div>
<div class="dashboard_box half right actions">
	<h3>Status : <?php echo Inflector::camelize($drobes['SoldDrobe']['status']) ?></h3>
	<?php
		if($drobes['SoldDrobe']['status']=='sold')
		{ 
			echo  $this->Html->tag('span', '3 easy steps to this order:', array('style' => 'font-size:16px;font-weight:bold'));
			echo $this->Form->input('',array('type'=>'checkbox','label'=>'Pack your item','class'=>'check_status'));
			echo $this->Form->input('',array('type'=>'checkbox','label'=>'Print pre-paid shipping label','class'=>'check_status'));
			echo $this->Form->input('',array('type'=>'checkbox','label'=>'Drop in mailbox','class'=>'check_status'))."<br>";
			//echo $this->Html->link('<b>Yay! shipped it</b>',array("action"=>"change_status",$drobes['SoldDrobe']['id']),array("class"=>"change_status",'style'=>'display:none;','escape'=>false,'id'=>'change_status'),"Are you sure you want to change status sold to shipped?");
			echo $this->Html->link('<b>Yay! shipped it</b>',array("action"=>"change_status",$drobes['SoldDrobe']['id']),array("class"=>"change_status",'escape'=>false,'id'=>'change_status',"onclick"=>"return mark_checkbox();"));
				
		}
	?>
</div>
<?php 
}
else
{	
?>
<div class="dashboard_box">
	Order details not found.
</div>
<?php 
}
?>

<script>
function mark_checkbox()
{
	length = $(".check_status:not(:checked)").length;
	if(length==0)
	{
		if(confirm("Are you sure you want to change status sold to shipped?"))
			return true;
		else
			return false;
	}
	else
	{
		alert("All selection is required");
		return false;
	}
}
</script>