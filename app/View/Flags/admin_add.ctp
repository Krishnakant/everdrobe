<h2>Add new Notification Message</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="spacer"></div>
<?php
echo $this->Form->create("Notify");
echo $this->Form->input("action");
echo $this->Form->input("subject");
echo $this->Form->input("message");
echo $this->Form->label("Message Type");
echo $this->Form->input('text_type', array('before' => '','after' => '', 'between' => '', 'separator' => ' ','legend'=>false,"label"=>"Message Type",'options' => array("html"=>'Html Format', 'text'=>"Plain Text"),'type' => 'radio','default'=>'html'));
echo $this->Form->input('type', array('legend'=>false, 'options' => array('email'=>"Email", 'facebook'=>"Facebook  Message", 'twitter'=>"Twitter Message"),'type' => 'select'));
?>
<div class="spacer"></div>
<?php
echo $this->Form->submit('Add');
echo $this->Form->end();
?>