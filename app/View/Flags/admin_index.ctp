<h2>Flagged Drobes</h2>
<div class="search_box" >
<form action="" method="get">
<?php 
if(isset($this->params->query['search']))
{
	$searched=$this->params->query['search'];
}
else $searched="";
?>
Seach User : <input type="text" name="search" value="<?php echo $searched; ?>" placeholder="Search drobe by comment, user name, category" style="display: inline; width: 400px !important; " /> <input type="submit" value="Search"  style=""/>  <input type="submit" value="Clear" name="reset"/>
</form>
</div>
<?php
if(count($drobes)>0)
{
	?>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('Flag.id','#');?></th>
			<th>Image</th>
			<th><?php echo $this->Paginator->sort('Drobe.comment','Comment');?></th>
			<th><?php echo $this->Paginator->sort('User.first_name','Uploaded by');?></th>
			<th>Summary</th>
			<th>Action</th>
		</tr>
		<?php 
		foreach($drobes as $drobe)
		{
			?>
			<tr class="<?php echo $drobe['Drobe']['id']; ?>">
				<td width="30px"><?php echo $drobe['Flag']['id'];?></td>
				<td width="90px"><?php echo $this->Html->link($this->Html->image('/drobe_images/thumb/'.$drobe['Drobe']['file_name'],array('width'=>Configure::read('drobe.thumb.width'),'border'=>0, 'id'=>"profile_image" ,"alt"=>"drobe prview Image")),$this->Html->url('/drobe_images/'.$drobe['Drobe']['file_name'],true),array("escape"=>false,"class"=>"example8"));?></td>
				<td><?php
				if($drobe['Drobe']['comment']!="") echo $drobe['Drobe']['comment']."<br/>";
				if($drobe['FlagCategory']['category_name']!="") echo "<em>(".$drobe['FlagCategory']['category_name'].")</em>";
				?></td>
				<td><?php echo $this->Html->link($drobe['User']['first_name']." ".$drobe['User']['last_name'],array("controller"=>"users","action"=>"view","admin"=>true,$drobe['User']['id']));?></td>
				<td width="75px">
				<div class="actions">
				<a class="summary_btn">Summary</a>
				<div class="bio_right_table full_width" style="display: none;">
        	<table cellspacing="0" cellpadding="0" border="0">
        	  <tbody><tr>
                <td class="gry_td">Views : </td>
                <td class="gry_td"><?php  echo $drobe['Drobe']['views']; ?></td>
              </tr>
              <tr>
                <td class="white_td"> Cute :</td>
                <td class="white_td"><?php  echo $drobe['Drobe']['total_in']; ?></td>
              </tr>
              <tr>
                <td class="gry_td">Hot :</td>
                <td class="gry_td"><?php  echo $drobe['Drobe']['total_out']; ?></td>
              </tr>
              <tr>
                <td class="white_td">Rate :</td>
                <td class="white_td"><?php  echo $drobe['Drobe']['total_rate']; ?></td>
              </tr>
               <tr>
                <td class="gry_td">Status :</td>
                <td class="gry_td"><?php  echo Inflector::humanize($drobe['Drobe']['rate_status']); ?></td>
              </tr>
              <tr>
                <td class="white_td">Posted on :</td>
                <td class="white_td"><?php  echo date(Configure::read('datetime.display_format'),strtotime($drobe['Drobe']['uploaded_on'])); ?></td>
              </tr>
            </tbody></table>
        </div>
        
        </div>
        </td>
				<td width="100px">
					<div class="submenu actions">
					<?php 
					echo $this->Html->link('View Detail',array("controller"=>"drobes","action"=>"detail","admin"=>true,$drobe['Drobe']['id'])); 
					echo $this->Html->link('Remove',array("controller"=>"drobes","action"=>"action","remove","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"are you sure to remove this drobe?")); 
					if($drobe['Drobe']['rate_status']!="open") echo $this->Html->link('Open for Rate',array("controller"=>"drobes","action"=>"action","open","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"are you sure to open this drobe for rate?"));
					else echo $this->Html->link('Close for Rate',array("controller"=>"drobes","action"=>"action","close","admin"=>true,$drobe['Drobe']['id']),array("confirm"=>"are you sure to close this drobe for rate?")); ?>
					</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>
<div class="pagignator">
    <?php echo $this->Paginator->first('<< first');?>
    <?php echo $this->Paginator->prev('Previous', null, null, array('class' => 'disabled')); ?>
    <!-- Shows the page numbers -->
    <?php echo str_replace("|","",$this->Paginator->numbers()); ?>
    <!-- Shows the next and previous links -->
    <?php echo $this->Paginator->next('Next', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->last('last >>');?>
</div>
<div id="drobe_options" style="display: none;">
	<ul class="submenu actions">
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"detail","admin"=>true)); ?>  class="view_detail_link">View Detail</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","remove","admin"=>true)); ?> onclick="return confirm('Are you sure to remove this user?');" class="remove_link">Remove</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","block","admin"=>true)); ?> onclick="return confirm('Are you sure to block this user?');" class="close_rate_link">Close Rate</a></li>
		<li><a href="#" rel=<?php echo $this->Html->url(array("controller"=>"users","action"=>"action","activate","admin"=>true)); ?> onclick="return confirm('Are you sure to activate this user?');" class="open_rate_link">Open Rate</a></li>
	</ul>
	<div class="clear"></div>
</div>
<script type="text/javascript">
var user_id=null;
var user_name=null;
$(document).ready(function(){
	$('input[name="reset"]').click(function(){
		$(this).closest('form').find('input').each(function(){$(this).attr('disabled','disabled')});
		$('input[name="search"]').val("");
		$(this).closest('form')[0].submit();
	})
	$('.summary_btn').each(function(){
		var self = $(this);
		$(this).click(function(){
			$(this).qtip({
				position : { my: 'right top', at: 'top left' } ,
				content: {
					text: self.next().html(), // Use the submenu as the qTip content
				},
				show: {
					event: 'click',
					ready: true // Make sure it shows on first mouseover
	 			},
				hide: {
					delay: 100,
					event: 'unfocus mouseleave',
					fixed: true // Make sure we can interact with the qTip by setting it as fixed
				},
	 			style: {
					classes: 'ui-tooltip-nav' // Basic styles
					//tip: false // We don't want a tip... it's a menu duh!
				},
			});
		});
		
	});
	
});
</script>