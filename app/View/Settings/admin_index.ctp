<h2>Drobe Settings</h2>
<div class="spacer"></div>
<?php
if(count($settingList)>0)
{
	?>
	<table>
		<tr>
			<th>#</th>
			<th width="150px">Setting Name</th>
			<th>Setting Value</th>
			<th width="150px">Actions</th>
		</tr>
		<?php 
		foreach($settingList as $setting)
		{
			?>
			<tr>
				<td><?php echo $setting['Setting']['id'];?></td>
				<td><?php echo Inflector::humanize($setting['Setting']['setting_name']);?></td>
				<td><?php 
					if($setting['Setting']['type']=="switch") echo Inflector::humanize($setting['Setting']['value']);
					else echo $setting['Setting']['value']; 
				?></td>
				<td>
				<div class="actions">
				<?php 
					echo $this->Html->link('Change this Setting',array('controller'=>'settings','action'=>'edit',$setting['Setting']['id'],'admin'=>true)). " "; 
				?>
				</div>
				</td>
			</tr>
			<?php 
		}
		?>
	</table>
	<?php 
} 
?>