<h2>Edit Setting: <?php echo Inflector::humanize($this->data['Setting']['setting_name']);?></h2>
<div class="right actions">
<?php 
echo $this->Html->link('Back to Sell Drobe Settings', array("controller"=>"settings","action"=>"index","admin"=>true));
?>
</div>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="spacer"></div>
<?php

echo $this->Form->create("Setting");
echo $this->Form->hidden("setting_name");
if($this->data['Setting']['type']=="text") echo $this->Form->input("value");
else echo $this->Form->input("value",array("label"=>"Value of ".Inflector::humanize($this->data['Setting']['setting_name']),"options"=>array('on'=>"On",'off'=>"Off")));
echo $this->Form->hidden('id');
echo $this->Form->hidden('type');
?>
<div class="spacer"></div>
<?php

echo $this->Form->submit('Update');
echo $this->Form->end();
?>