<h2>Add new Setting</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
<div class="spacer"></div>
<?php
echo $this->Form->create("Setting");
echo $this->Form->input("setting_name");
echo $this->Form->input("type",array("options"=>array('text'=>"Text",'switch'=>"Radio (On/Off)")));
echo $this->Form->input("value");
?>
<div class="spacer"></div>
<?php
echo $this->Form->submit('Add');
echo $this->Form->end();
?>