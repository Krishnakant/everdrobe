<h2>Seller Profile</h2>
<?php 
if(!empty($errors))
{
	?>
	<div class="error">
	<ul>
	<?php 
	foreach($errors as $error)
	{
		echo "<li>".$error[0]."</li>";
	}
	?>
	</ul>
	</div>
	<?php 
}
?>
	

<?php 
	echo $this->Form->create("SellProfile");
?>

<div class="spacer"></div>
<b>Select Payment Type </b>
<div class="payment_radio">
	<?php 
		echo $this->Form->input('payment_type', array('before'=>'','after'=>'','between'=>'','separator'=>'','legend'=>false, 'options' => array("cheque"=>"By Cheque","paypal"=>"By Paypal"),'type' => 'radio','default'=>'cheque','style'=>array('clear:none;')));		
	?>
</div>

<div class="drobe_sale_popup" >
	<div id="cheque_profile" class="left half">
	<?php
	echo $this->Form->input("full_name",array("label"=>"Full Name-checks will be made payable to this name"));
	?>
	
	<?php
	//echo $this->Form->input("address",array("type"=>"textarea","style"=>"width:98%","label"=>"Address to Mail your Check"));
	echo $this->Form->input("street1",array("type"=>"text"));
	echo $this->Form->input("street2",array("type"=>"text"));
	
	echo $this->Form->input("id",array("type"=>"hidden"));

	?>
			<div id="paypal_profile">
			<?php 
				echo $this->Form->input("paypal_email",array('label'=>'Email'));
			?>
			</div>
	</div>
	<div class="half right">		
		<?php
			echo $this->Form->input("city",array("type"=>"text"));
			echo $this->Form->input("state",array("type"=>"text"));
			echo $this->Form->input("zip",array("type"=>"text"));
		?>
	</div>
</div>	

<?php 
	echo $this->Form->submit("Update Profile");
?>




<script>
$(document).ready(function(){
	if($('#SellProfilePaymentTypeCheque').is(':checked')){
		$('#paypal_profile').hide();		
	}	
	else
	{
		$('#paypal_profile').show();			
	}
		
	$('#SellProfilePaymentTypeCheque').click(function(){
		$('#paypal_profile').hide();
		
	});

	$('#SellProfilePaymentTypePaypal').click(function(){
		$('#SellProfilePaypalEmail').parent().addClass('required');
		$('#paypal_profile').show();		
	});
	
});
</script>