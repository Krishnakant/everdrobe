<?php 
function resizeActualImage($src)
{
	$thisNewWidth=430;
	$thisNewHeight=240;
	/* read the source image */
	$ext=array_pop(explode(".",$src));
	
	if($ext=="jpg" || $ext=="jpeg") $source_image = imagecreatefromjpeg($src);
	else if($ext=="gif") $source_image = imagecreatefromgif($src);
	else if($ext=="png") $source_image = imagecreatefrompng($src);
	
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	// keep width and height ratio in ascpect ratio

	if($width>$thisNewWidth)
	{
		$newWidth=$thisNewWidth;
		$newHeight = floor($height*($newWidth/$width));
	}
	else
	{
		$newWidth=$width;
		$newHeight=$height;
	}
	if($newHeight>$thisNewHeight)
	{
		$newHeight=$thisNewHeight;
		$newWidth=floor($width*($newHeight/$height));
	}
	$thisNewWidth=$newWidth;
	$thisNewHeight=$newHeight;
	 

	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($thisNewWidth,$thisNewHeight);

	/* copy source image at a resized size */
	imagecopyresized($virtual_image,$source_image,0,0,0,0,$thisNewWidth,$thisNewHeight,$width,$height);

	if($ext=="jpg" || $ext=="jpeg") imagejpeg($virtual_image,$src);
	else if($ext=="gif") imagegif($virtual_image,$src);
	else if($ext=="png") imagepng($virtual_image,$src);
}
$dir="./drobe_images";
$files = scandir($dir);
foreach ($files as $file);
{
	if($file=="." || $file=="..") continue;
	$path=$dir."/".$file;
	if(is_dir($path)){}
	else if(is_file($path))
	{
		echo $path."<br>";
		resizeActualImage($path);
	}
}
?>