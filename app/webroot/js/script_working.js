var scroll_data_loading=false;
$(document).ready(function(){
	//Examples of how to assign the ColorBox event to elements
	$("a[rel='example4']").colorbox({slideshow:true});
	$(".example8").livequery(function(){
		$(this).colorbox(
				{
					width:"510px", 
				//	height:"550px",
					onClosed:function(){
						//document.location.href=document.location.href;
					}
				});
	});
	
	//Example of preserving a JavaScript event for inline calls.
	$("#click").click(function(){ 
		$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
		return false;
	});
		
	$('.share').livequery(function(){
		var self = $(this);
		$(this).click(function(){
			$(this).qtip({
				position : { my: 'bottom center', at: 'top center' } ,
				content: {
					text: self.find('#shareit-icon'), // Use the submenu as the qTip content
				},
				show: {
					event: 'click',
					ready: true // Make sure it shows on first mouseover
	 			},
				hide: {
					delay: 100,
					event: 'click',
					fixed: true // Make sure we can interact with the qTip by setting it as fixed
				},
	 			style: {
					classes: 'share-popup ui-tooltip-shadow ui-tooltip-light' // Basic styles
					//tip: false // We don't want a tip... it's a menu duh!
				},
			});
		});
	});
	
	
});

/****************
 * Added by haresh Vidja
 */

$(document).ready(function(){
	
	$('a[title]').livequery(function(){
		$(this).qtip({
			position: {
				my: "bottom center", // Use the corner...
				at: "top center" // ...and opposite corner
			},
			style: {
				classes: 'ui-tooltip-shadow ui-tooltip-dark'
			}
		});
	});
	
	// fixing issue of recaptch style
	$('#recaptcha_widget_div').livequery(function(){
		$("#recaptcha_widget_div, #recaptcha_widget_div *").each(function(){
			$(this).css('margin','0px').css('padding','0px');
		})
			
	});
	$("#recaptcha_response_field").livequery(function(){
		$(this).css("background","none repeat #FFFFFF").css("color","#000000").css("height","18px");
	//	$(this).attr('name',"data[Contact][recaptcha_response_field]");
	//	$('input[name="recaptcha_challenge_field"]').attr('name',"data[Contact][recaptcha_challenge_field]");
	});
	
	
	$('.swfupload').livequery(function(){
		$(this).css('left',0);
	})
	$('a.login').click(function(){
		$('#login_form').submit();
		return false;
		
	})
	$('.simple_popup').popupWindow({height:400,width:700,top:50,left:50});
	$('.simple_big_popup').popupWindow({height:550,width:700,top:50,left:50});
	$('a.sign_up').click(function(){
		$('#user_login').hide();
		$('#user_register').show();
	});
	
	
	$('.back_to_login').click(function(){
		$('#user_register').hide();
		$('#user_login').show();
	});
	$('input#file_upload').change(function(){
		var filename=$(this).val().split("\\"); 
		$("div.selected_file").html(filename.pop());
	});
	
	$("#stream_button").click(function () {
		$(".filter_drobe .categories").toggle();
	});
	$("#apply_rate_stream").click(function(){
		
		if($('.categories input[type="checkbox"]:checked').length == $('.categories input[type="checkbox"]').length)
		{
			var categories = "ALL";
		}
		else
		{
			var categories= new Array();
				
			$('.categories input:checked').each(function(){
				categories.push($(this).val());
			});
			if(categories.length==0)
			{
				alert("Select atleast one category for stream drobes");
				return;
			}
		}
		var gender=$('input[name="filter_gender"]:checked').val();
		$(".filter_drobe .categories").hide();
  		$.ajax({
			url: 'users/stream_category',
			type: "POST",
			data: {
				categories: categories,
				gender: gender
			},
			success: function(data) {
			  	if(data=="success")
			  	{
			  		var counter=0;
			  		if($('.categories input[type="checkbox"]:checked').length == $('.categories input[type="checkbox"]').length)
					{
						if($('input[name="filter_gender"]:checked').val()!="both")
						{
							counter=1;
						}
					}
			  		else
			  		{
			  			counter=$('.categories input[type="checkbox"]:checked').length;
			  		}
			  		$("#stream_button a.badges").remove();
			  		if(counter>0 || categories!="ALL") $("#stream_button").prepend("<a class='badges'>"+counter+"</a>");
			  		nextrate();
			  	}
			  	else
			  	{
			  		alert("error occured in filtering rate stream, please try again");
			  	}
		  	}
		});
	});
	
	$('#replybtn').click(function(){
		reply_drobe_conversation();
	});
	// reply on chat after press Enter key
	$("#replyarea").keypress(function(event) {
	    if (event.which == 13) {
	        event.preventDefault();
	        reply_drobe_conversation();
	    }
	});
	/*
	 * Select All on filter
	 */
	$(".filter_drobe .categories input[type='checkbox']").not("#check-all").click(function(){
		var all_checked= ($(".filter_drobe .categories input[type='checkbox']").not("#check-all").length==$(".filter_drobe .categories input[type='checkbox']:checked").not("#check-all").length);
		$("#check-all").attr('checked',all_checked);
	})
	$("#check-all").click(function(){
		var is_checked=$(this).is(':checked');
		$(".filter_drobe .categories input[type='checkbox']").not("#check-all").each(function(){$(this).attr('checked',is_checked)});
	});
	
	
	$('#replychatbtn').click(function(){
		reply_chat_conversation();
	})
	// reply on chat after press Enter key
	$("#replychatarea").keypress(function(event) {
	    if (event.which == 13) {
	        event.preventDefault();
	        reply_chat_conversation();
	    }
	});
	
	$('#btn_close_conversation').click(function(){
		$.blockUI({
			message: $('#close_confirm'),
			css: {
				top:  ($(window).height()) /2 + 'px',
				left: ($(window).width()-300) /2 + 'px',
				width: '400px'
			}
		});
		return false;
	});
	
	
	$('#close_conversation_yes').click(function(){
		$.ajax({
			url: 'rates/close_conversation',
			type: "POST",
			data: {
				conversation_id: conversation_id
			},
			success: function(data) {
				$('#btn_close_conversation').val("Conversation Closed").attr('disabled','disabled');
				$('#reply_conversation').remove();
				$.unblockUI();
		  	}
		});
	});
	$('#close_conversation_no, #close_chatting_no').click(function(){
		$.unblockUI();
	});
	
	//btn_close_chatting
	$('#btn_close_chatting').click(function(){
		$.blockUI({
			message: $('#close_confirm'),
			css: {
				top:  ($(window).height()) /2 + 'px',
				left: ($(window).width()-300) /2 + 'px',
				width: '400px'
			}
		});
		return false;
	});
	
	$('#close_chatting_yes').click(function(){
		$.ajax({
			url: 'chattings/close',
			type: "POST",
			data: {
				friend_id: friend_id
			},
			success: function(data) {
				data=$.parseJSON(data);
				$.unblockUI();
				if(data.type=="success")
			  	{
			  		document.location.href='users/follower';
			  	}
				else
				{
					alert(data.message);
				}
		  	}
		});
	});
	
	
	
	$('.follower_list .pagignator').livequery(function(){
		$(this).find('a').click(function(){
			$('.follower_list').load($(this).attr('href'),function(){});
			return false;
		});
	});

	
	
	/*
	 * Implementing Tabs 
	 */
	
	$('.tabs ul li a').click(function(){
		$('.tabs .tab_content').load($(this).attr('href'),function(){});
		$(this).parent().parent().find('li').removeClass("active");
		$(this).parent('li').addClass("active");
		return false;
	});
	$('.tabs .tab_content .pagignator').livequery(function(){
		$(this).find('a').click(function(){
			$('.tabs .tab_content').load($(this).attr('href'),function(){});
			return false;
		});
	});
	$('.tabs ul li:first a').click();
	
	/*
	 * End of Coding of Tabse
	 */

	
	/*
	 * Drobe upload common script
	 * 
	 */
	/*$("#file_uploader").hover(
			function(){$(this).find('.drobe_upload_msg').fadeOut('fast');}, 
			function(){$(this).find('.drobe_upload_msg').fadeIn('fast');}
	);*/
	
	$('.drobe_preview').hover(
			function(){$('#clear_preview').stop().fadeIn('fast');}, 
			function(){$('#clear_preview').stop().fadeOut('fast');}
	);
	$('.drobe_big_img, .drobe_uploaded .drobe_preview').livequery(function(){
		$(this).toggle(
				function(){
					$('.drobe_comment').stop().fadeOut('fast');
					//$('.flag_drobe').stop().fadeOut('fast');
				},function(){
					$('.drobe_comment').stop().fadeIn('fast');
					//$('.flag_drobe').stop().fadeIn('fast');
				}
		);
		$(this).css("cursor","pointer");
		$('.drobe_comment').click(function(){return false;});
	});
	
	/*
	 * Follow user and unfollow user
	 * 
	 */
	
	$('#follow_button').click(function(){
		if($(this).hasClass('red_btn'))
		{
			$.ajax({
				url: 'users/follow',
				type: "POST",
				data: {
					user_id: $(this).attr('rel'),
				},
				success: function(data) {
				  	obj=$.parseJSON(data);
				  	if(obj.type=="success") 
				  	{
				  		$('#follow_button').find('.white_plus').removeClass('white_plus').addClass('white_minus').next().html('Following');
				  		$('#follow_button').html('Following').removeClass('btn-danger').addClass('btn-green');
				  	}
			  	}
			});
		}
		else
		{
			$.ajax({
				url: 'users/unfollow',
				type: "POST",
				data: {
					user_id: $(this).attr('rel'),
				},
				success: function(data) {
				  	obj=$.parseJSON(data);
				  	if(obj.type=="success") 
				  	{
				  		$('#follow_button').find('.white_minus').removeClass('white_minus').addClass('white_plus').next().html('Follow');
				  		$('#follow_button').html('Follow').removeClass('btn-green').addClass('btn-danger');
				  	}
			  	}
			});
		}
		return false;
	});
	
	$('input.comment, textarea.comment').livequery(function(){
		$(this).keyup(function(){
			 if($(this).val().length > 140){
			        $(this).val($(this).val().substr(0, 140)).focus();
			 }
		});
	})
	
});

function apply_infinite_scrolling(objId)
{
	document.getElementById(objId).onfleXcroll=function(){
		var thisObj=$("#"+objId);
		scrollBaseObj=thisObj.find('.vscrollerbase');
		scrollBarObj=scrollBaseObj.find('.vscrollerbar');
		// getting offset of bottom part from top of screen
		scrollbaseOffset=scrollBaseObj.offset().top + scrollBaseObj.height();
		scrollbarOffset=scrollBarObj.offset().top + scrollBarObj.outerHeight()
		// if offset of both divs is equal then need to go for ajax process
		if(!scroll_data_loading && (scrollbaseOffset-scrollbarOffset<0.1))
		{
			load_next_data(thisObj);
		}
	};
}
function load_next_data(scrollObj)
{
	
	contentObj=scrollObj.find(".long_content");
	// determine next data loading
	var lastPage=parseInt(contentObj.attr('last'));
	if(lastPage>=0)
	{
		// if last page is defined then neet to set limit
		if(contentObj.attr('max')!=null)
		{
			if(lastPage < parseInt(contentObj.attr('max'))) lastPage++;
			else lastPage=null;
		}
		else lastPage++;
	}
	else  lastPage=null;
	if(lastPage>0)
	{
		url=contentObj.attr('rel')+"/page:"+lastPage;
		scroll_data_loading=true;
		$('.infiniteScrollLoading').stop().fadeIn();
		$.post(url,function(data){
			if ($.trim(data) != ""){
				contentObj.append(data);
				scrollObj[0].fleXcroll.updateScrollBars();
			}
			else
			{
				$('.infiniteScrollLoading').html("No more data");
				lastPage=-1;
			}
			$('.infiniteScrollLoading').stop().fadeOut();
			contentObj.attr('last',lastPage);
			scroll_data_loading=false;
		});
	}
	else
	{
		$('.infiniteScrollLoading').stop().fadeIn('fast',function(){$(this).stop().fadeOut('slow')});
		
	}
};

function set_conversation_scrollbar()
{
   	var selectedtab = '.conversations_area';
	$(selectedtab)[0].fleXcroll.updateScrollBars();
	$('.conversations_area')[0].fleXcroll.scrollContent(false,$('.long_content').height());
	$('.vscrollerbase').click();
}
function dialogue(content, title) {
	/* 
	 * Since the dialogue isn't really a tooltip as such, we'll use a dummy
	 * out-of-DOM element as our target instead of an actual element like document.body
	 */
	$('<div />').qtip(
	{
		content: {
			text: content,
			title: title
		},
		position: {
			my: 'center', at: 'center', // Center it...
			target: $(window) // ... in the window
		},
		show: {
			ready: true, // Show it straight away
			modal: {
				on: true, // Make it modal (darken the rest of the page)...
				blur: false // ... but don't close the tooltip when clicked
			}
		},
		hide: false, // We'll hide it maunally so disable hide events
		style: 'ui-tooltip-light ui-tooltip-rounded ui-tooltip-dialogue', // Add a few styles
		events: {
			// Hide the tooltip when any buttons in the dialogue are clicked
			render: function(event, api) {
				$('button', api.elements.content).click(api.hide);
			},
			// Destroy the tooltip once it's hidden as we no longer need it!
			hide: function(event, api) { api.destroy(); }
		}
	});
}
//Our Confirm method
function Confirm(question, callback)
{
	// Content will consist of the question and ok/cancel buttons
	var message = $('<p />', { text: question }),
		ok = $('<button />', { 
			text: 'Yes',
			click: function() {callback(true);}
		}),
		cancel = $('<button />', { 
			text: 'No',
			click: function() { callback(false); }
		});

	dialogue( message.add(ok).add(cancel), 'Confirmation' );
}





function fileUpload(form, action_url, div_id) {
    // Create the iframe...
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "upload_iframe");
    iframe.setAttribute("name", "upload_iframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0; height: 0; border: none;");
 
    // Add to document...
    form.parentNode.appendChild(iframe);
    window.frames['upload_iframe'].name = "upload_iframe";
 
    iframeId = document.getElementById("upload_iframe");
 
    // Add event...
    var eventHandler = function () {
            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("load", eventHandler, false);
 
            // Message from server...
            if (iframeId.contentDocument) {
                content = iframeId.contentDocument.body.innerHTML;
            } else if (iframeId.contentWindow) {
                content = iframeId.contentWindow.document.body.innerHTML;
            } else if (iframeId.document) {
                content = iframeId.document.body.innerHTML;
            }
            document.getElementById(div_id).innerHTML = content;
 
            // Del the iframe...
            setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
        }
 
    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);
 
    // Set properties of form...
    form.setAttribute("target", "upload_iframe");
    //form.setAttribute("action", action_url);
    form.setAttribute("method", "post");
    form.setAttribute("enctype", "multipart/form-data");
    //form.setAttribute("encoding", "multipart/form-data");
 
    alert("submitting");
    // Submit the form...
    form.submit();
 
    document.getElementById(div_id).innerHTML = "Uploading...";
    
}
function reply_drobe_conversation()
{
	var reply=$('#replyarea').val();
	if($.trim(reply)=="")
	{
		$('#replyarea').html("").focus();
	}
	else
	{
		$.ajax({
			url: 'conversations/add/'+conversation_id,
			type: "POST",
			data: {
				reply: reply
			},
			success: function(data) {
				data=$.parseJSON(data);
			  	if(data.type=="success")
			  	{
			  		$('#replyarea').val("");
			  		$('#drobe_conversations').load('conversations/view/'+conversation_id,function(){
			  			set_conversation_scrollbar();
			  		});
			  	}
			  	else
			  	{
			  		$.unblockUI();
			  		alert(data.message);
			  	}
		  	}
		});
	}
}
function reply_chat_conversation()
{
	var message=$('#replychatarea').val();
	if($.trim(message)=="")
	{
		$('#replychatarea').html("").focus();
	}
	else
	{
		$.ajax({
			url: 'chattings/add/'+friend_id,
			type: "POST",
			data: {
				message: message
			},
			success: function(data) {
				data=$.parseJSON(data);
			  	if(data.type=="success")
			  	{
			  		$('#replychatarea').val("");
			  		$('#friend_conversations').load('chattings/view/'+friend_id,function(){
			  			set_conversation_scrollbar();
			  		});
			  	}
			  	else
			  	{
			  		$.unblockUI();
			  		alert(data.message);
			  	}
		  	}
		});
	}
}

function tip_message(obj_id,msg)
{
	$(obj_id).qtip({
		content: {text: msg, title: false},
		show: {event: false,ready: true},
		position: {at: "top center",my: "bottom center"},
		style: {classes: 'ui-tooltip-shadow ui-tooltip-dark big-tip'}
	}).focus();
	return_val= false;
}

/**************************
 * code for cropping image
**************************/
var jcrop_api; // Holder for the API
function display_image_cropping(response)
{
	
	$('#DrobeFileName').val(response.file);
    $('#crop_image').attr('src',drobe_image_dir + response.file);

	var width= response.size.width;
	var height= response.size.height;

    if(max_width < width)
    {
    	$('.drobe_preview img').attr("width",max_width).removeAttr("height");
    	height=height/(width/max_width);
    	if(max_height < height)
        {
        	$('.drobe_preview img').attr("height",max_height).removeAttr("width");
        }	
    }
    if(max_height < height)
    {
    	$('.drobe_preview img').attr("height",max_height).removeAttr("width");
    }
    $('#crop_image').load(function(){
    	try
    	{
    	// display image cropping area with image after loading a image;
    	$('#file_uploader').hide();
    	$('.drobe_preview').show().find('.big-img').css({'background':"#EDEDED"});
    	$("#file-uploader").show();
    	$('#file_uploader').unblock();
    	
    	img_width=$(this).width();
    	img_height=$(this).height();
    	
    	if($(this).parent().width() > img_width)
    	{
    		$(this).parent().css('padding-left',parseInt(($(this).parent().width()-img_width)/2)+"px");
    	}
    	if($(this).parent().height() > img_height)
    	{
    		$(this).parent().css('padding-top',parseInt(($(this).parent().height()-img_height)/2)+"px");
    	}
    	$('#crop_image').Jcrop({
        	aspectRatio: 1,
        	minSize: [ 320, 320],
        	onSelect: updateCoords,
        	trueSize:[response.size.width,response.size.height]
        	},function(){
        		bounds = this.getBounds();
            	boundx = bounds[0];
            	boundy = bounds[1];
            	jcrop_api = this;
            	if(boundx>boundy)
            	{
            		left_cords= parseInt((boundx-boundy)/2);
            		jcrop_api.animateTo([left_cords,0,boundx,boundy]);
            		updateCoords({x:left_cords,y:0,w:boundy,h:boundy});
            	}
            	else
            	{
            		top_cords= parseInt((boundy-boundx)/2);
            		jcrop_api.animateTo([0,top_cords,boundx,boundy]);
            		updateCoords({x:0,y:top_cords,w:boundx,h:boundx});
            	}
            	//$('.jcrop-holder').css({"border": "1px solid #FFFFFF", "box-shadow": "0px 0px 3px #888888"});
        });
    	}
    	catch(e)
    	{
    		alert(e.valueOf());
    	}
    });
    $.colorbox.close();
}
function updateCoords(c)
{
    $('#pos_left').val(c.x);
	$('#pos_top').val(c.y);
	$('#new_width').val(c.w);
	$('#new_height').val(c.h);
};
function checkCoords()
{
	if (parseInt($('#pos_left').val())) return true;
	alert('Please select a crop region then press submit.');
	return false;
};

function check_comment_length(str){
	var data = str.split(/[^\w\d]/);
         for (var i = 0; i < data.length; i++) {
             if (data[i].length >= 25) {
                 return false;
             }
         }
         return true
 };