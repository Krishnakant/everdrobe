if(read_only_mode==false)
{
	$(document).ready(function(){
		$('#rate_drobe_block .ratebutton').click(function(){
			if($(this).hasClass('processing'))
			{
				return false;
			}
			if($.trim($('#drobe_comment').val())=="" && $(this).attr('rel')==2)
			{
				tip_message('#drobe_comment',"Comment is required");
				return false; 
			}
			if($.trim($('#drobe_comment').val())!="" && !check_comment_length($('#drobe_comment').val()))
			{
				tip_message('#drobe_comment',"Some words looking too much long");
				return false; 
			}
			$('#rate_drobe_block .ratebutton').each(function(){
				$(this).addClass('processing');
			});
			$.ajax({
				  url: 'drobes/rate_response',
				  type: "POST",
				  data: {
					drobe_id: $("#drobe_id").val(),
				  	rate: $(this).attr('rel'),
				  	comment: $("#drobe_comment").val()
					},
				  success: function(data) {
					  try
					  {
					  	//var pos_top= $('.drobe_big_img').position().top + $('.drobe_big_img').height()/2 - 50;
					  	//var pos_left=$('.drobe_big_img').position().left + 100;
					  	$('#rate_response').html(data).fadeIn('fast');
					  	setTimeout('nextrate()',1000);
					  }
					  catch(e)
					  {
						  alert(e.valueOf());
					  }
				  }
				});
			return false;
		}); 
		
		$('#rate_drobe_block #flag_button').click(function(){
			
			$('#container').block({ 
		        message: $('#flag_block'), 
		        css: { 
		            top:  ($(window).height()) /2 + 'px', 
		            left: ($(window).width()-300) /2 + 'px', 
		            width: '300px' 
		        } 
		    }); 
			$("#flag_category").focus();
			$('#close_flag').click(function(){
				$('#container').unblock();
			}); 
		
		});
		
		// preventing submit twice in rate stream (multiple binding of click events previously) 
		$('#submit_flag').unbind('click');

		// binding new click event
		$('#submit_flag').click(function(){
			if($('#flag_category').val()!="")
			{
				Confirm('Are you sure you want to flag this content?', function(yes) 
				{
					if(yes)
					{
						$.ajax({
							url: 'drobes/flag',
							type: "POST",
							data: {
								drobe_id: $("#drobe_id").val(),
							  	flag_category_id: $('#flag_category').val(),
							  	comment: $("#drobe_comment").val()
							},
							  success: function(data) {
								  obj=$.parseJSON(data);
								  
								  if(obj.type=="success")
								  {
									setTimeout('nextrate()',1000);
									$('#container').unblock();
								  }
								  else
								  {
									  alert(obj.message);
									  $('#container').unblock();
								  }
							  }
						});
					}
					else
					{
						$('#container').unblock();
					}
				});
			}
			else
			{
				$('#flag_category').focus();
				$('#flag_category').qtip({
					content: {
						text: 'Select category before submit it',
						title: false,
					},
					show: {
						event: false,
						ready: true
					},
					position: {
						at: "top left",
						my: "bottom left"
					}
				}).focus();
				return false;
			}
		}); 
			
		$('#rate_drobe_block #fave_button').click(function(){
			if(!($(this).hasClass("processing")))
			{
				$(this).addClass("processing");
				if(!$(this).attr('disabled'))
				{	$('#fave_button').attr('disabled',true).find('span').html((parseInt($('#fave_button span').html())+1));
					$.ajax({
						url: 'drobes/addtofaves',
						type: "POST",
						data: {
							drobe_id: $("#drobe_id").val(),
						},
						success: function(data) {
							$('#fave_button').removeClass("processing");
							obj=$.parseJSON(data);
							if(obj.type=="success"){
						  		$('#fave_button').attr('disabled',true).find('span').html(obj.count);
						  	}
						  	else
						  	{
						  		$('#fave_button').attr('disabled',false).find('span').html((parseInt($('#fave_button span').html())-1));
						  	}
						  	//$.unblockUI();
					  	}
					});
				}
				else
				{
					$('#fave_button').attr('disabled',false).find('span').html((parseInt($('#fave_button span').html())-1));
					$.ajax({
						url: 'drobes/removefromfaves',
						type: "POST",
						data: {
							drobe_id: $("#drobe_id").val(),
						},
						success: function(data) {
							$('#fave_button').removeClass("processing");
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success"){
						  		$('#fave_button').attr('disabled',false).find('span').html(obj.count);
						  	}
						  	else
						  	{
						  		$('#fave_button').attr('disabled',true).find('span').html((parseInt($('#fave_button span').html())+1));
						  	}
						  	//$.unblockUI();
					  	}
					});
				}
			}
			
		});
		$('#rate_drobe_block #follow_button').unbind('click').click(function(){
			if($(this).hasClass('red_btn'))
			{
				$.ajax({
					url: 'users/follow',
					type: "POST",
					data: {
						drobe_id: $("#drobe_id").val(),
					},
					success: function(data) {
					  	obj=$.parseJSON(data);
					  	if(obj.type=="success") 
					  	{
					  		$('#follow_button').find('.white_plus').removeClass('white_plus').addClass('white_minus').next().html('Following');
					  		$('#follow_button').removeClass('red_btn').addClass('green_btn');
					  	}
				  	}
				});
			}
			else
			{
				$.ajax({
					url: 'users/unfollow',
					type: "POST",
					data: {
						drobe_id: $("#drobe_id").val(),
					},
					success: function(data) {
					  	obj=$.parseJSON(data);
					  	if(obj.type=="success") 
					  	{
					  		$('#follow_button').find('.white_minus').removeClass('white_minus').addClass('white_plus').next().html('Follow');
					  		$('#follow_button').removeClass('green_btn').addClass('red_btn');
					  	}
				  	}
				});
			}
		});
		$('#rate_drobe_block #next_drobe').click(function(){
			nextrate('next');
		});
		
		$('#rate_drobe_block #prev_drobe').click(function(){
			nextrate('prev');
		});
	});
}
else
{
	$(document).ready(function(){
		//$('#user_login_block').height($('#container').innerHeight());
		$('#rate_drobe_block .ratebutton').click(function(){
			display_login_block();
			return false;
		}); 
		$('#rate_drobe_block #flag_button').click(function(){
			display_login_block();
			return false;
		});
		$('#rate_drobe_block  #submit_flag').click(function(){
			display_login_block();
			return false;
		}); 
		
		$('#rate_drobe_block #fave_button').click(function(){
			display_login_block();
			return false;
		});
		$('#rate_drobe_block #follow_button').click(function(){
			display_login_block();
			return false;
		});
	});
}
function display_login_block()
{
	$('.share-popup').hide();
	$('#rate_drobe_block').hide();
	$('#user_login_block').fadeIn('fast',function(){
		/*var height=$('#rate_drobe_block').parent().height();
		$('#rate_drobe_block').hide().parent().height(height-20);
		var height=$(this).parent().height();
		$(this).height(height-20).css('border','none');*/
		$(this).css('border','none');
	});
	$('a.logo-in').focus();
}
function nextrate(direction)
{
	
	if(is_colorbox)
	{
		$.colorbox.next();
	}
	else
	{
		if(false && is_selected) 
		{
			document.location.href="drobes/rate";
			return;
		}
		else
		{
			var rate_url="drobes/rate";
			$.ajax({
				url: rate_url,
				type: "POST",
				data: {
					last_drobe: $("#drobe_id").val(),
					action: action,
					action_id: action_id,
					order: order_by,
					direction : direction
				},
				success: function(data) {
					$('#rate_response').html("").hide();
					$('#rate_drobe_block').html(data);
					var contentImages = $("#rate_drobe_block img");
				    var totalImages = contentImages.length;
				    var loadedImages = 0;
				    contentImages.each(function(){
				        $(this).on('load', function(){
				            loadedImages++;
				            if(loadedImages == totalImages)
				            {
				            	$("html, body").animate({ scrollTop: $(document).height() }, 0);
				            }
				        });
				    });
					// loading facebook and twitter button after ajax update
					FB.XFBML.parse(); 
					twttr.widgets.load();
					//$('.drobe_big_img > img').load(function(){$(this).center(true)}).fadeIn();	
			  	}
			})
		}		
	}
}