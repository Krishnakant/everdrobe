var scroll_data_loading = false;
var featherEditor="";
$(document).ready(function(){
	
	$(".star-user,.star-user-small").livequery(function(){
		$(this).qtip({
			position : { my: 'top center', at: 'bottom center' },
			content: {text: star_user_text},
			show: {event: 'hover',ready: false},
			style: {classes: 'share-popup ui-tooltip-shadow ui-tooltip-light'}
		});
	});
	$(".example8").livequery(function(){
		$(this).colorbox({width:"510px"});
	});
	$("#click").click(function(){ 
		$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
		return false;
	});
	
	//code for shopify autologin iframe delete
	if($('#shopify-auto-login').length)
	{
		$('#shopify-auto-login').load(function(){
			$(this).remove();
		});
	}
	
	$('.share').livequery(function(){
		var self = $(this);
		$(this).click(function(){
			$(this).qtip({
				position : { my: 'bottom center', at: 'top center' },
				content: {text: self.find('#shareit-icon')},
				show: {event: 'click',ready: true},
				hide: {delay: 100,event: 'click',fixed: true},
	 			style: {classes: 'share-popup ui-tooltip-shadow ui-tooltip-light'}
			});
		});
	});
	
	//create textbox for custom size
	$('.drob_size_custom').change(function(){
		if($(this).val()=='Other')
		{
			if(!$(this).parent().find(".spacer").length)
			{
				$(this).parent().append("<div class='spacer'></div>");
			}
			
			if(!$(this).parent().find("#drobe_size_name").length)
			{
				$(this).parent().append("<input type='text' id='drobe_size_name' name='data[SellDrobe][size_name]' />");
			}	
		}
		else
		{
			if($(this).parent().find("#drobe_size_name").length)
			{
				$(this).parent().find("#drobe_size_name").remove();
			}
			
			if($(this).parent().find(".spacer").length)
			{
				$(this).parent().find(".spacer").remove();		
			}	
				
		}
	});
	
	
	//create textbox for custom brand
	$('.drobe_brand_custom').change(function(){
		
		if($(this).val()=='Other')
		{
			if(!$(this).parent().find(".spacer").length)
			{
				$(this).parent().append("<div class='spacer'></div>");
			}	
			if(!$(this).parent().find("#drobe_brand_name").length)
			{
				$(this).parent().append("<input type='text' id='drobe_brand_name' name='data[SellDrobe][sell_brand_name]' />");
			}	
		}
		else
		{
			if($(this).parent().find("#drobe_brand_name").length)
			{
				$(this).parent().find("#drobe_brand_name").remove();
			}	
			if($(this).parent().find(".spacer").length)
			{
				$(this).parent().find(".spacer").remove();
			}					
		}
	});
	
	$('a[title]').livequery(function(){
		$(this).qtip();
	});
	
	// fixing issue of recaptch style
	$('#recaptcha_widget_div').livequery(function(){
		$("#recaptcha_widget_div, #recaptcha_widget_div *").each(function(){
			$(this).css('margin','0px').css('padding','0px');
		});
			
	});
	$("#recaptcha_response_field").livequery(function(){
		$(this).css("background","none repeat #FFFFFF").css("color","#000000").css("height","18px");
	});
	
	$('a.login').click(function(){
		$('#login_form').submit();
		return false;
	});
	$('.simple_popup').popupWindow({height:500,width:700,top:50,left:50});
	$('.simple_big_popup').popupWindow({height:550,width:700,top:50,left:50});
	$('a.sign_up').click(function(){
		$('#user_login').hide();
		$('#user_register').show();
	});
	$('.back_to_login').click(function(){
		$('#user_register').hide();
		$('#user_login').show();
	});
	$('input#file_upload').change(function(){
		var filename=$(this).val().split("\\"); 
		$("div.selected_file").html(filename.pop());
	});
	$("#stream_button").toggle(function()
		{
			//$(".filter_drobe .filter_input").show();
			setScrollBarFilter(0);
		},function(){
			$(".filter_drobe .filter_input").hide();
		});

	
	
	//Check condition for if below id has been created then decalre TabbedPanels2 variable
	if($("#TabbedPanelsFilter").length>0)
	{
		var TabbedPanels2 = new Spry.Widget.TabbedPanels("TabbedPanelsFilter");
	}	
	/* Apply infinite scrollbar to Filter category */
	$('#TabbedPanelsFilter .TabbedPanelsTab').click(function(){
		current_index=TabbedPanels2.getCurrentTabIndex();
		setScrollBarFilter(current_index);
	});

	
	$("#reset_filter").click(function(){
		
		//for all categories selection when user click on reset filter
		$(".filter_drobe .categories input[type='checkbox']").each(function(){
			$(this).attr('checked','checked');
		});
		
		//for all size selection when user click on reset filter
		$(".filter_drobe .sizes input[type='checkbox']").each(function(){
			$(this).attr('checked','checked');
		});
		
		//for all brand selection when user click on reset filter
		$(".filter_drobe .brands input[type='checkbox']").each(function(){
			$(this).attr('checked','checked');
		});
		
		//for all price range selection when user click on reset filter
		$(".filter_drobe .prices input[type='checkbox']").each(function(){
			$(this).attr('checked','checked');
		});
		
		//When user click on reset filter then gender button select both
		$("input[name=filter_gender][value=both]").attr('checked', 'checked');
		//When user click on reset filter then only new brand button select both
		$("input[name=only_new_brand][value=both]").attr('checked', 'checked');
		//When user click on reset filter then highest rated button select off
		$("input[name=highest_rated][value=0]").attr('checked', 'checked');
		
		$(".filter_drobe .filter_input").hide();
		$.ajax({
			url: 'users/reset_filter',
			type: "POST",
			success: function(data) {
			  	if(data=="success"){
			  		//remove badge when user click on reset filter button
			  		$("#stream_button a.badges").remove();
			  		nextrate();
			  	}
			  	else
			  	{
			  		alert("error occured in filtering rate stream, please try again");
			  	}
		  	}
		});
	});
	
	/***  This code is done for filter drobe when user click on apply button all filters are saved. **/
	$("#apply_rate_stream").click(function(){
		var categories="";
		/** This code for category selection if select all then all subcategory will selected and category_ids in database contain ALL when click on apply button **/
		if($('.categories input[type="checkbox"]:checked').length == $('.categories input[type="checkbox"]').length) 
		{
			categories = "ALL";
		}	
		else
		{
			categories= new Array();
			$('.categories input[type="checkbox"]:checked').each(function(){
				categories.push($(this).val());
			});
			if(categories.length==0)
			{
				alert("Select atleast one category for stream drobes");
				return;
			}
		}
		
		var sizes="";
		/** This code for Sizes selection if select all sizes then all sub size will selected and in database table store empty string for all selection. when click on apply button **/
		if($('.sizes input[type="checkbox"]:checked').length == $('.sizes input[type="checkbox"]').length)
		{
			sizes = "{ALL}";
		}	
		else
		{
			sizes= new Array();
			$('.sizes input[type="checkbox"]:checked').each(function(){
				sizes.push($(this).val());
			});
			if(sizes.length==0)
			{
				alert("Select atleast one size for stream drobes");
				return;
			}
		}
		
		var brands ="";
		/** This code for Brands selection if select all brands then all sub brand will selected and in database table store empty string for all selection. when click on apply button **/
		if($('.brands input[type="checkbox"]:checked').length == $('.brands input[type="checkbox"]').length) 
		{
			brands = "{ALL}";
		}	
		else
		{
			brands= new Array();
			$('.brands input[type="checkbox"]:checked').each(function(){
				brands.push($(this).val());
			});
			if(brands.length==0)
			{
				alert("Select atleast one brand for stream drobes");
				return;
			}
		}
		
		
		var prices ="";
		/** This code for Prices selection if select all prices then all sub price will selected and in database table store empty string for all selection. when click on apply button **/
		if($('.prices input[type="checkbox"]:checked').length == $('.prices input[type="checkbox"]').length)
		{
			prices = "{ALL}";
		}	
		else
		{
			prices= new Array();
			$('.prices input[type="checkbox"]:checked').each(function(){
				prices.push($(this).val());
			});
			if(prices.length==0)
			{
				alert("Select atleast one price range for stream drobes");
				return;
			}
		}
		
		
		var only_new_brand=$('input[name="only_new_brand"]:checked').val();
		var highest_rated=$('input[name="highest_rated"]:checked').val();
		var gender=$('input[name="filter_gender"]:checked').val();
		
		$(".filter_drobe .filter_input").hide();
  		$.ajax({
			url: 'users/stream_category',
			type: "POST",
			data: {category_ids: categories,size_name:sizes,brand_name:brands,price_range:prices,gender: gender,only_new_brand:only_new_brand,highest_rated:highest_rated},
			success: function(data) {
			  	if(data=="success"){
			  		var counter=0;
			  		if($('.categories input[type="checkbox"]:checked').length == $('.categories input[type="checkbox"]').length)
					{
			  			if($('.sizes input[type="checkbox"]:checked').length == $('.sizes input[type="checkbox"]').length)
						{
				  			counter=0;
				  			if($('.brands input[type="checkbox"]:checked').length == $('.brands input[type="checkbox"]').length)
							{
					  			counter=0;
					  			if($('.price_list input[type="checkbox"]:checked').length == $('.price_list input[type="checkbox"]').length)
								{
						  			counter=0;
								}
						  		else
					  			{
						  			counter=$('.price_list input[type="checkbox"]:checked').length;
					  			}
							}
					  		else
				  			{
					  			counter=$('.brands input[type="checkbox"]:checked').length;
				  			}
						}
				  		else
			  			{
				  			counter=$('.sizes input[type="checkbox"]:checked').length;
			  			}
			  		}
			  		else 
			  		{
			  			counter=$('.categories input[type="checkbox"]:checked').length;
			  		}
			  		
			  		if($('input[name="filter_gender"]:checked').val()!="both" && counter==0) 
					{
						counter=1;
					}
			  		
			  		
			  		$("#stream_button a.badges").remove();
			  		if(counter>0) 
			  		{
			  			$("#stream_button").prepend("<a class='badges'>"+counter+"</a>");
			  		}
			  		nextrate();
			  	}
			  	else
			  	{
			  		alert("error occured in filtering rate stream, please try again");
			  	}
		  	}
		});
	});
	$('#replybtn').click(function(){
		reply_drobe_conversation();
	});
	$("#replyarea").keypress(function(event) {
	    if (event.which == 13) {
	        event.preventDefault();
	        reply_drobe_conversation();
	    }
	});
	
	/***  This code done for selection of all check box if click on select all checkbox for category ***/
	$(".filter_drobe .categories input[type='checkbox']").not("#check-all").click(function(){
		var all_checked= ($(".filter_drobe .categories input[type='checkbox']").not("#check-all").length==$(".filter_drobe .categories input[type='checkbox']:checked").not("#check-all").length);
		$("#check-all").attr('checked',all_checked);
	});
	$("#check-all").click(function(){
		var is_checked=$(this).is(':checked');
		$(".filter_drobe .categories input[type='checkbox']").not("#check-all").each(function(){
			$(this).attr('checked',is_checked);
		});
	});
	
	
	/***  This code done for selection of all check box if click on select all checkbox for Price ***/
	$(".filter_drobe .prices input[type='checkbox']").not("#check_all_prices").click(function(){
		var all_checked= ($(".filter_drobe .prices input[type='checkbox']").not("#check_all_prices").length==$(".filter_drobe .prices input[type='checkbox']:checked").not("#check_all_prices").length);
		$("#check_all_prices").attr('checked',all_checked);
	});
	$("#check_all_prices").click(function(){
		var is_checked=$(this).is(':checked');
		$(".filter_drobe .prices input[type='checkbox']").not("#check_all_prices").each(function(){
			$(this).attr('checked',is_checked);
		});
	});
	
	
	/***  This code done for selection of all check box if click on select all checkbox for size name ***/
	$(".filter_drobe .sizes input[type='checkbox']").not("#check_all_sizes").click(function(){
		var all_checked = ($(".filter_drobe .sizes input[type='checkbox']").not("#check_all_sizes").length==$(".filter_drobe .sizes input[type='checkbox']:checked").not("#check_all_sizes").length);
		$("#check_all_sizes").attr('checked',all_checked);
	});
	$("#check_all_sizes").click(function(){
		var is_checked=$(this).is(':checked');
		$(".filter_drobe .sizes input[type='checkbox']").not("#check_all_sizes").each(function(){
			$(this).attr('checked',is_checked);
		});
	});
	
	
	/***  This code done for selection of all check box if click on select all checkbox for brand name ***/
	$(".filter_drobe .brands input[type='checkbox']").not("#check_all_brands").click(function(){
		var all_checked= $(".filter_drobe .brands input[type='checkbox']").not("#check_all_brands").length == $(".filter_drobe .brands input[type='checkbox']:checked").not("#check_all_brands").length;
		$("#check_all_brands").attr('checked',all_checked);
	});
	$("#check_all_brands").click(function(){
		var is_checked = $(this).is(":checked");
		$(".filter_drobe .brands input[type='checkbox']").not("#check_all_brands").each(function(){
			$(this).attr('checked',is_checked);
		});
	});
	
	$('#replychatbtn').click(function(){
		reply_chat_conversation();
	});
	$("#replychatarea").keypress(function(event) {
	    if (event.which == 13) {
	        event.preventDefault();
	        reply_chat_conversation();
	    }
	});
	
	
	$('#btn_close_conversation').click(function(){
		$.blockUI({
			message: $('#close_confirm'),
			css: {top: ($(window).height()) /2 + 'px', left: ($(window).width()-300) /2 + 'px', width: '400px'}
		});
		return false;
	});
	$('#close_conversation_yes').click(function(){
		$.ajax({
			url: 'rates/close_conversation',
			type: "POST",
			data: {
				conversation_id: conversation_id
			},
			success: function(data) {
				$('#btn_close_conversation').val("Conversation Closed").attr('disabled','disabled');
				$('#reply_conversation').remove();
				$.unblockUI();
		  	}
		});
	});
	$('#close_conversation_no, #close_chatting_no').click(function(){
		$.unblockUI();
	});
	
	//btn_close_chatting
	$('#btn_close_chatting').click(function(){
		$.blockUI({
			message: $('#close_confirm'),
			css: {
				top:  ($(window).height()) /2 + 'px',
				left: ($(window).width()-300) /2 + 'px',
				width: '400px'
			}
		});
		return false;
	});
	
	$('#close_chatting_yes').click(function(){
		$.ajax({
			url: 'chattings/close',
			type: "POST",
			data: {
				friend_id: friend_id
			},
			success: function(data) {
				data=$.parseJSON(data);
				$.unblockUI();
				if(data.type=="success") 
				{
					document.location.href='users/follower';
				}	
				else 
				{
					alert(data.message);
				}
		  	}
		});
	});
	
	$('.follower_list .pagignator').livequery(function(){
		$(this).find('a').click(function(){
			$('.follower_list').load($(this).attr('href'),function(){});
			return false;
		});
	});

	$('.tabs ul li a').click(function(){
		$('.tabs .tab_content').load($(this).attr('href'),function(){});
		$(this).parent().parent().find('li').removeClass("active");
		$(this).parent('li').addClass("active");
		return false;
	});
	$('.tabs .tab_content .pagignator').livequery(function(){
		$(this).find('a').click(function(){
			$('.tabs .tab_content').load($(this).attr('href'),function(){});
			return false;
		});
	});
	$('.tabs ul li:first a').click();

	
	$('.drobe_preview').hover(function(){$('#clear_preview').stop().fadeIn('fast');}, function(){$('#clear_preview').stop().fadeOut('fast');});
	
	$('.drobe_big_img, .drobe_uploaded .drobe_preview').livequery(function(){
		$(this).toggle(function(){
			$('.drobe_comment').stop().fadeOut('fast');}, function(){$('.drobe_comment').stop().fadeIn('fast');});
		//$(this).css("cursor","pointer");
		$('.drobe_comment').click(function(){return false;});
		$('.drobe_comment .big_buy').click(function(){
			window.open($(this).attr('href'),'_blank');
			});
	});
	
	
	/**  This code for flagged user popup when user click on user profile flag button */
	$('#flag_button').click(function(){
		
		$('#container').block({ 
	        message: $('#flag_block'), 
	        css: { 
	            top:  ($(window).height()) /2 + 'px', 
	            left: ($(window).width()-300) /2 + 'px', 
	            width: '300px' 
	        } 
	    }); 
		$("#flag_category").focus();
		$('#close_flag').click(function(){
			$('#container').unblock();
		}); 
	
	});
	
	// preventing submit twice in rate stream (multiple binding of click events previously) 
	$('#submit_flag').unbind('click');
	
	// binding new click event
	$('#submit_flag').click(function(){
		if($('#flag_category').val()!="")
		{
			Confirm('Are you sure you want to flag this content?', function(yes) 
			{
				if(yes)
				{
					$.ajax({
						url: 'users/add_flag',
						type: "POST",
						data: {
							flagged_user_id: $("#flagged_user_id").val(),
						  	flag_category_id: $('#flag_category').val(),
						  	comment: $("#drobe_comment").val()
						},
						  success: function(data) {
							  obj=$.parseJSON(data);
							  if(obj.type=="success")
							  {
								  $( "#flag_button" ).replaceWith( "<b>Flagged</b>" );
								  $('#container').unblock();
							  }
							  if(obj.type=="error")
							  {
								  $( "#flag_button" ).replaceWith( "<b>Flagged</b>" );
								  $('#container').unblock();
							  }
						  }
					});
				}
				else
				{
					$('#container').unblock();
				}
			});
		}
		else
		{
			$('#flag_category').focus();
			$('#flag_category').qtip({
				content: {
					text: 'Select category before submit it',
					title: false
				},
				show: {
					event: false,
					ready: true
				},
				position: {
					at: "top left",
					my: "bottom left"
				}
			}).focus();
			return false;
		}
	}); 
	
	
	$('#follow_button').click(function(){
		if($(this).hasClass('btn-danger')){
			$.ajax({
				url: 'users/follow',
				type: "POST",
				data: {user_id: $(this).attr('rel')},
				success: function(data) {
				  	obj=$.parseJSON(data);
				  	if(obj.type=="success") 
				  	{
				  		$('#follow_button').find('.white_plus').removeClass('white_plus').addClass('white_minus').next().html('Following');
				  		$('#follow_button').html('Following').removeClass('btn-danger').addClass('btn-green');
				  	}
			  	}
			});
		}
		else
		{
			$.ajax({
				url: 'users/unfollow',
				type: "POST",
				data: {user_id: $(this).attr('rel')},
				success: function(data) {
				  	obj=$.parseJSON(data);
				  	if(obj.type=="success") 
				  	{
				  		$('#follow_button').find('.white_minus').removeClass('white_minus').addClass('white_plus').next().html('Follow');
				  		$('#follow_button').html('Follow').removeClass('btn-green').addClass('btn-danger');
				  	}
			  	}
			});
		}
		return false;
	});
	
	$('input.comment, textarea.comment').livequery(function(){
		$(this).keyup(function(){
			 if($(this).val().length > 140){
			        $(this).val($(this).val().substr(0, 140)).focus();
			 }
		});
	});
	$('textarea#drobe_description').livequery(function(){
		$(this).keyup(function(){
			 if($(this).val().length > 300){
			        $(this).val($(this).val().substr(0, 300)).focus();
			 }
		});
	});
	
	/*
	 * popup error message script
	 */
	if($('#flashMessage.popup_error').length>0)
	{
		popupFlashMessage();
	}
	
	
/********************** Start here code for mydrobe.ctp file **********************************************/
	
	
	/****  When user wants particular drobe upload on facebook then set facebook button on near submit button **/
	$(".fb_connect").click(function(){
		var is_fb_connect = $("#is_fb_connected").val();
		var fb_url = $("#fb_url").val();
		//Check user setting for facebook is connected or not if connected then is_connect=1 else is_connect=0
		if(is_fb_connect == 0)
		{		
			$.ajax({
				  url: 'users/link_with_facebook/',
				  success: function(data) {
					}
			});
			
			window.open(fb_url,'_blank','width=700,height=500,top=50,left=50,scrollbars=yes');
			
			if($(".fb_connect").hasClass("disconnected"))
			{
				$(".fb_connect").removeClass("disconnected");
				$(".fb_connect").addClass("connected");
				$("#is_fb_post,#is_fb_post_sell").val(1);
			}
		}
		
		if($(".fb_connect").hasClass("connected"))
		{
			$(".fb_connect").removeClass("connected");
			$(".fb_connect").addClass("disconnected");
			$("#is_fb_post,#is_fb_post_sell").val(0);
		}
		else
		{
			$(".fb_connect").removeClass("disconnected");
			$(".fb_connect").addClass("connected");
			$("#is_fb_post,#is_fb_post_sell").val(1);
		}
				
	});


	/****  When user wants particular drobe upload on twitter then set twitter button on near submit button **/
	$(".tw_connect").click(function(){
		var is_tw_connect = $("#is_tw_connected").val();
		var tw_url = $("#tw_url").val();
		//Check user setting for twitter is connected or not if connected then is_connect=1 else is_connect=0
		if(is_tw_connect == 0)
		{
			$.ajax({
				  url: 'users/twitterLoginUrl/',
				  success: function(data) {
					}
			});
				
			window.open(tw_url,'_blank','width=700,height=500,top=50,left=50,scrollbars=yes');
			if($(".tw_connect").hasClass("disconnected"))
			{
				$(".tw_connect").removeClass("disconnected");
				$(".tw_connect").addClass("connected");
				$("#is_tw_post,#is_tw_post_sell").val(1);
			}
		}
		
		if($(".tw_connect").hasClass("connected"))
		{
			$(".tw_connect").removeClass("connected");
			$(".tw_connect").addClass("disconnected");
			$("#is_tw_post,#is_tw_post_sell").val(0);
		}
		else
		{
			$(".tw_connect").removeClass("disconnected");
			$(".tw_connect").addClass("connected");
			$("#is_tw_post,#is_tw_post_sell").val(1);
		}
	});
	/************************************** End here code for mydrobe.ctp file  ************************************/

	/**  Validation for sell drobe when user not pass any data and click on submit button then tooltip message will display **/
	$('#drobe_upload_btn_sell,#drobe_edit_btn_sell').click(function(){
		/** Call validate_drobe_sale function for validate sale drobe upload. */
		
		var return_val= validate_drobe_sale();
		return return_val;
		
		/*
		if($('#drobe_size').val()=="")
		{
			tip_message('#drobe_size',"Please select size");
			return_val= false;
		}
		
		if($('#sell_drobe_file_name').val()=="")
		{
			tip_message('#sell_drobe_file-uploader',"Please upload your image");
			return_val=false;
		}
		
		if($.trim($('#sell_drobe_comment').val())=="")
		{
			tip_message('#sell_drobe_comment',"Your question is required");
			return_val=false;
		}
		if(!check_comment_length($('#sell_drobe_comment').val()))
		{
			tip_message('#sell_drobe_comment',"Some comment word length is invalid");
			return_val=false;
		}
		if($('#drobe_brand').val()=="")
		{
			tip_message('#drobe_brand',"Please select brand name");
			return_val= false;
		}
		if($('#original_drobe_price').val()=="")
		{
			tip_message('#original_drobe_price',"Please enter original price");
			return_val= false;
		}
		if($('#drobe_description').val()=="")
		{
			tip_message('#drobe_description',"Please enter description");
			return_val= false;
		}
		
		if($('#drobe_price').val()=="")
		{
			tip_message('#drobe_price',"Please enter listing price");
			return_val= false;
		}
		
		*/
		
	});
	
	
	
	/**  Validation for post drobe when user not pass any data and click on submit button then tooltip message will display **/
	$('#drobe_upload_btn').click(function(){
		var return_val=true;
		if($('#DrobeCategoryId').val()=="")
		{
			tip_message('#DrobeCategoryId',"Please select category");
			return_val= false;
		}
		if($('#DrobeFileName').val()=="")
		{
			tip_message('#file-uploader',"Please upload your image");
			return_val=false;
		}
		if($.trim($('#DrobeComment').val())=="")
		{
			tip_message('#DrobeComment',"Your question is required");
			return_val=false;
		}
		if(!check_comment_length($('#DrobeComment').val()))
		{
			tip_message('#DrobeComment',"Some comment word length is invalid");
			return_val=false;
		}
		/*	If all validation is perfect then remove all qtip message from form.*/
		if(return_val==true)
		{
			$('.qtip').each(function(){
				$(this).hide();
			});
		}
		return return_val;
	});
	
	/*** This code for clear preview from post drobe in mydrobe.ctp file. ***/
	$('#clear_preview').click(function() {
		Confirm('Are you sure to change this image?', function(yes) {
			if(yes)
			{
				var filename=$('#DrobeFileName').val();
				$.ajax({
	    			  url: 'drobes/delete_image/'+escape(filename.replace(".","__")),
	    			  success: function(data) {
	    			   	document.location.href=document.location.href;    			    
	    			  }
	    		});
			}
		});
		return false;
	});
	
	
	/** is_fb_connect variable is checke facebook connected or not at load time if connected then is_fb_connect is 1 otherwise 0 */
    var is_fb_connect = $("#is_fb_connected").val();
	//Check user setting for facebook is connected or not if connected then is_connect=1 else is_connect=0
	if(is_fb_connect == 1)
	{
		if($(".fb_connect").hasClass("disconnected"))
		{
			$(".fb_connect").removeClass("disconnected");
			$(".fb_connect").addClass("connected");
			$("#is_fb_post,#is_fb_post_sell").val(1);
		}
	}
	

	var is_tw_connect = $("#is_tw_connected").val();
	//Check user setting for twitter is connected or not if connected then is_connect=1 else is_connect=0
	if(is_tw_connect == 1)
	{
		if($(".tw_connect").hasClass("disconnected"))
		{
			$(".tw_connect").removeClass("disconnected");
			$(".tw_connect").addClass("connected");
			$("#is_tw_post,#is_tw_post_sell").val(1);
		}
	}
	
	
	
});/********************* document.ready(function) completed here *********/

/*** 	This function is used for post drobe main image upload **/
function upload_response(obj)
{
	
	last_uploaded_response = obj;
	if(obj.type=="success")
    {
		$('#DrobeFileName').val(obj.file);
	    $('#crop_image').attr('src',drobe_image_dir + obj.file);

	    var width= obj.size.width;
		var height= obj.size.height;
		
		if(max_width < width)
	    {
	    	$('.drobe_preview img').attr("width",max_width).removeAttr("height");
	    	height=height/(width/max_width);
	    	if(max_height < height) 
	    		{
	    			$('.drobe_preview img').attr("height",max_height).removeAttr("width");
	    		}
	    }
	    if(max_height < height)
	    	{
	    		$('.drobe_preview img').attr("height",max_height).removeAttr("width");
	    		
	    	}
	    $('#crop_image').load(function(){
		    console.log("onLoad ::" + last_uploaded_response);
		    $('#file_uploader').hide();
	    	$('.drobe_preview').show().find('.big-img').css({'background':"#EDEDED"});
	    	$("#file-uploader").show();
	    	$('#file_uploader').unblock();
	    	

		    img_width=$(this).width();
	    	img_height=$(this).height();
	    	if($(this).parent().width() > img_width) 
	    	{
	    		$(this).parent().css('padding-left',parseInt(($(this).parent().width()-img_width)/2)+"px");
	    	}
	    	if($(this).parent().height() > img_height) 
	    	{
	    		$(this).parent().css('padding-top',parseInt(($(this).parent().height()-img_height)/2)+"px");
	    	}
	    	 // display image aviary editor with image after loading a image;
		    var src = $("#crop_image").attr("src");
	    	//Call avaiary image function launchEditor for displaying image with its effects
			featherEditor.launch({
		        image: "crop_image",
		        url: src
		    });
			$(this).unbind("load");
	    });
	  //  display_image_cropping(obj);
    }
    else
    {  
    	alert(obj.message);
    	$('#DrobeFileName').val("");
    	$('#drobe_upload_btn').attr('disabled','disabled');
    }
}


/*****	This function is used for initializing additional images on mydrobe.ctp file	***/
function fileUploadCompleteAction(el, obj)
{
	if(obj.type=="success")
	{
		showAdditionalImage(el, obj.file);
	}
    else 
	{
    	$('input[rel="'+el.attr('id')+'"]').val("");
	}
}

/**		This code display message uploading image when upload each additional image.  **/
function blockUploader(el)
{
	el.block({ 
		message: "<b>Uploading Image</b>",
    	css: {
        	border: 'none', 
            padding: '22px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px !important', 
            '-moz-border-radius': '10px !important', 
            opacity: 0.5, 
            color: '#fff'
        }
   	}); 
}


/********************** Start here code for mydrobe.ctp file **********************************************/
/* This function is called from gmail_connect.ctp and facebook.ctp file */
function togglesharebutton(share_type)
{
	if(share_type=="link_with_facebook")
	{		
		if($(".fb_connect").hasClass("disconnected"))
			{	
				$(".fb_connect").removeClass("disconnected");
				$(".fb_connect").addClass("connected");
				//if user want to post drobe at facebook then is_fb_post value is 1
				$("#is_fb_post,#is_fb_post_sell").val(1);
				$("#is_fb_connected").val(1);
				
			}
	 }
	if(share_type=="link_with_twitter")
	{
		if($(".tw_connect").hasClass("disconnected"))
		{
			$(".tw_connect").removeClass("disconnected");
			$(".tw_connect").addClass("connected");
			//if user want to post drobe at facebook then is_fb_post value is 1
			$("#is_tw_post,is_tw_post_sell").val(1);
			$("#is_tw_connected").val(1);
		}
	}
}

/************************************** End here code for mydrobe.ctip file  ************************************/





function popupFlashMessage()
{
	$('#flashMessage.popup_error').fadeIn('slow',function(){
		setTimeout(function(){
			$('#flashMessage.popup_error').fadeOut('slow',function(){
				$('#flashMessage.popup_error').remove();
			});
		},3000);
	});
}
function apply_infinite_scrolling(objId)
{
	document.getElementById(objId).onfleXcroll=function(){
		var thisObj=$("#"+objId);
		scrollBaseObj=thisObj.find('.vscrollerbase');
		scrollBarObj=scrollBaseObj.find('.vscrollerbar');
		// getting offset of bottom part from top of screen
		scrollbaseOffset=scrollBaseObj.offset().top + scrollBaseObj.height();
		scrollbarOffset=scrollBarObj.offset().top + scrollBarObj.outerHeight();
		// if offset of both divs is equal then need to go for ajax process
		if(!scroll_data_loading && (scrollbaseOffset-scrollbarOffset<0.1))
		{
			load_next_data(thisObj);
		}
	};
}
function load_next_data(scrollObj)
{
	contentObj=scrollObj.find(".long_content");
	var tab_name=contentObj.attr('id');
	
	// determine next data loading
	var lastPage=parseInt(contentObj.attr('last'));
	if(lastPage>=0)
	{
		// if last page is defined then neet to set limit
		if(contentObj.attr('max')!=null)
		{
			if(lastPage < parseInt(contentObj.attr('max'))) 
			{
				lastPage++;
			}	
			else 
			{
				lastPage=null;
			}
		}
		else 
		{
			lastPage++;
		}
	}
	else  
	{
		lastPage=null;
	}
	
	if(lastPage>0)
	{
		if(tab_name=="heighest_rated")
		{
			url=contentObj.attr('rel').replace('page_counter',lastPage);
		}
		else 
		{
			url=contentObj.attr('rel')+"/page:"+lastPage;
		}
		scroll_data_loading=true;
		$('.infiniteScrollLoading').stop().fadeIn();
		$.post(url,function(data){
			var without_script_tag=$.trim($('<div>'+data+"</div>").remove('script').html());
			if (without_script_tag != ""){
				if(tab_name=="heighest_rated") 
				{
					if(contentObj.find('a').length==$('<div>'+data+'</div>').find('a').length)
					{
						$('.infiniteScrollLoading').html("No more data");
						lastPage=-1;
					}
					else 
					{
						contentObj.html(data);
					}
				}
				else 
				{
					contentObj.append(data);
				}
				scrollObj[0].fleXcroll.updateScrollBars();
			}
			else
			{
				$('.infiniteScrollLoading').html("No more data");
				lastPage=-1;
			}
			$('.infiniteScrollLoading').stop().fadeOut();
			contentObj.attr('last',lastPage);
			scroll_data_loading=false;
		});
	}
	else
	{
		$('.infiniteScrollLoading').stop().fadeIn('fast',function(){$(this).stop().fadeOut('slow');});
	}
}

function set_conversation_scrollbar()
{
   	var selectedtab = '.conversations_area';
	$(selectedtab)[0].fleXcroll.updateScrollBars();
	$('.conversations_area')[0].fleXcroll.scrollContent(false,$('.long_content').height());
	$('.vscrollerbase').click();
}
function dialogue(content, title) {
	$('<div />').qtip(
	{
		content: {text: content, title: title},
		position: { my: 'center', at: 'center', target: $(window)},
		show: {ready: true, modal: { on: true, blur: false}},
		hide: false,
		style: 'ui-tooltip-light ui-tooltip-rounded ui-tooltip-dialogue',
		events: {
			render: function(event, api) {$('button', api.elements.content).click(api.hide);},
			hide: function(event, api) { api.destroy(); }
		}
	});
}
function Confirm(question, callback)
{
	var message = $('<p />', { text: question });
	var	ok = $('<button />', { text: 'Yes', click: function() {callback(true);}});
	var cancel = $('<button />', {text: 'No',click: function() {callback(false);}});
	dialogue( message.add(ok).add(cancel), 'Confirmation');
}

function reply_drobe_conversation()
{
	var reply=$('#replyarea').val();
	if($.trim(reply)=="") 
	{
		$('#replyarea').html("").focus();
	}	
	else
	{
		$.ajax({
			url: 'conversations/add/'+conversation_id,
			type: "POST",
			data: {reply: reply},
			success: function(data) {
				data=$.parseJSON(data);
			  	if(data.type=="success")
			  	{
			  		$('#replyarea').val("");
			  		$('#drobe_conversations').load('conversations/view/'+conversation_id,function(){
			  			set_conversation_scrollbar();
			  		});
			  	}
			  	else
			  	{
			  		$.unblockUI();
			  		alert(data.message);
			  	}
		  	}
		});
	}
}
function reply_chat_conversation()
{
	var message=$('#replychatarea').val();
	if($.trim(message)=="")
	{
		$('#replychatarea').html("").focus();
	}	
	else
	{
		$.ajax({
			url: 'chattings/add/'+friend_id,
			type: "POST",
			data: {message: message},
			success: function(data) {
				data=$.parseJSON(data);
			  	if(data.type=="success")
			  	{
			  		$('#replychatarea').val("");
			  		$('#friend_conversations').load('chattings/view/'+friend_id,function(){
			  			set_conversation_scrollbar();
			  		});
			  	}
			  	else
			  	{
			  		$.unblockUI();
			  		alert(data.message);
			  	}
		  	}
		});
	}
}
/*************************
 * My Drobe Used functions
 *************************/
function tip_message(obj_id,msg)
{
	$(obj_id).qtip({
		content: {text: msg, title: false},
		show: {event: false,ready: true},
		position: {at: "top center",my: "bottom center"},
		style: {classes: 'ui-tooltip-shadow ui-tooltip-dark big-tip'}
	}).focus();
	return_val= false;
}
function validate_drobe_sale()
{
	try
	{
		var return_val = true;
	//	var brand_name=$.trim($('#brand_name').val());
		var drobe_size=$.trim($('#drobe_size').val());
		var drobe_brand=$.trim($('#drobe_brand').val()); //@sadikhasan
		var drobe_price=$.trim($('#drobe_price').val());
		var drobe_description=$.trim($('#drobe_description').val());
		var drobe_category = $.trim($('#drobe_category_id_sell').val()); //@sadikhasan
		var sell_drobe_comment = $.trim($('#sell_drobe_comment').val()); // for sell drobe comment @sadikhasan
		var original_drobe_price = $.trim($('#original_drobe_price').val()); // for sell drobe original price @sadikhasan
		var sell_drobe_main_image = $.trim($('#DrobeFileName').val()); // for sell drobe main image upload validation
		/* If edit sell drobe then file name is DrobeFileName thats why here check if sell_drobe_main_image is blank then get value from DrobeFileName */
		if(sell_drobe_main_image=="")
		{
			sell_drobe_main_image = $.trim($('#DrobeFileName').val());
		}
		/*
		 * This text box name is removed commented by @sadikhasan
		if(brand_name=="")
		{
			tip_message('#brand_name',"Brand Name is required");
			$('#brand_name').focus();
			return_val=false;
		}
		
			
		if(brand_name.length>140)
		{
			tip_message('#brand_name',"Brand name must be less then 140 character");
			if(return_val) $('#brand_name').focus();
			return_val=false;
		} */
		
		if(sell_drobe_main_image=="")
		{
			tip_message('#file-uploader','Please upload your image');
			if(return_val)
			{
				$('#file-uploader').focus();
			}
			return_val = false;	
		}
		
		
		
		if(original_drobe_price=="")
		{
			tip_message('#original_drobe_price','Original price is required');
			if(return_val)
			{
				$('#original_drobe_price').focus();
			}
			return_val = false;
		}
		else if(original_drobe_price.length>9)
		{
			tip_message('#original_drobe_price','Price limit exceed then its limit');
			if(return_val)
			{
				$('#original_drobe_price').focus();
			}
			return_val = false;
		}
		else if(new String(parseFloat(original_drobe_price)) != original_drobe_price)
		{
			tip_message('#original_drobe_price','Original price accept only numerical value');
			if(return_val)
			{
				$('#original_drobe_price').focus();
			}	
			return_val = false;
		}
		
		
		if(sell_drobe_comment=="")
		{
			tip_message('#sell_drobe_comment','Comment is required');
			if(return_val) 
			{
				$('#sell_drobe_comment').focus();
			}
			return_val=false; 
		}
		
		
		if(drobe_category=="")
		{
			tip_message('#drobe_category_id_sell',"Drobe category is required");
			if(return_val) 
			{
				$('#drobe_category_id_sell').focus();
			}
			return_val=false; 
		}
		
		
		if($('#drobe_size_name').length)
		{
			var drobe_size_name=$.trim($('#drobe_size_name').val());
			if(drobe_size_name=="")
			{
				tip_message('#drobe_size_name',"Drobe size is required");
				if(return_val)
				{
					$('#drobe_size_name').focus();
				}
				return_val=false;
			}
			if(drobe_size_name.length>10)
			{
				tip_message('#drobe_size_name',"Drobe size must less then 10 character");
				if(return_val) 
				{
					$('#drobe_size_name').focus();
				}
				return_val=false;
			}
		}
		
		/*
		 * For brand name validation it is not blank and not more than 20 characters
		 * @sadikhasan
		 * */
		if($('#drobe_brand_name').length)
		{
			var drobe_brand_name=$.trim($('#drobe_brand_name').val());
			if(drobe_brand_name=="")
			{
				tip_message('#drobe_brand_name',"Brand name is required");
				if(return_val) 
				{
					$('#drobe_brand_name').focus();
				}
				return_val=false;
			}
			if(drobe_brand_name.length>20)
			{
				tip_message('#drobe_brand_name',"Brand name must less then 20 character");
				if(return_val) 
				{
					$('#drobe_brand_name').focus();
				}
				return_val=false;
			}
		}
		
		if(drobe_size=="")
		{
			tip_message('#drobe_size',"Drobe size is required");
			if(return_val) 
			{
				$('#drobe_size').focus();
			}
			return_val=false;
			
		}
		
		
		
		/*
		 * Check for Brand name in combobox is selected or not.
		 * 
		 * @sadikhasan
		 * */
		if(drobe_brand=="")
		{
			tip_message('#drobe_brand',"Brand name is required");
			if(return_val) 
			{
				$('#drobe_brand').focus();
			}
			return_val=false;
			
		}
		
		if(drobe_price=="")
		{
			tip_message('#drobe_price',"Listing price is required");
			if(return_val) 
			{
				$('#drobe_price').focus();
			}
			return_val=false;
		}
		else if(drobe_price.length>9)
		{
			tip_message('#drobe_price',"Price limit exceed then its limit");
			if(return_val) 
			{
				$('#drobe_price').focus();
			}
			return_val=false;
		}
		else if(new String(parseFloat(drobe_price)) != drobe_price)
		{
			tip_message('#drobe_price',"Listing price accept only numerical value");
			if(return_val) 
			{
				$('#drobe_price').focus();
			}
			return_val=false;
		}
		
		
		if(parseFloat(drobe_price)<5)
		{
			tip_message('#drobe_price',"Drobe price must be minimum $5");
			if(return_val) 
			{
				$('#drobe_price').focus();
			}
			return_val=false;
		}
		if(drobe_description!="" && drobe_description.length>300)
		{
			tip_message('#drobe_description',"Drobe description must be less then 300 character");
			if(return_val) 
			{
				$('#drobe_description').focus();
			}
			return_val=false;
		}
		
		if(return_val==true)
		{
			$('.qtip').remove();
		}
		return return_val;
	}
	catch(e)
	{
		alert(e.valueOf());
	}
}
/****************************
 * Friend Invitation
 ****************************/

/*
 * Code for friend Invitation
 */


function loadFriendList(keyword)
{
	if($.trim(keyword)=="")
	{
		$('#friend_list').html("");
	}
	else
	{
		/*$('#friends_detail').block({ 
			message: "<h3>Fetching Friend List</h3>",
	    	css: { 
	            border: 'none', 
	            padding: '10px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px !important', 
	            '-moz-border-radius': '10px !important', 
	            opacity: 0.5, 
	            color: '#fff' 
	    	}
	    });*/
		$.ajax({
			url: 'users/search_user',
			type: "POST",
			data: {keyword: keyword},
			success: function(data) {
				$('#friend_list').html(data);
				attechInviteEvent();
		  	}
		});
	}
}

function loadFbFriendsByAjax()
{
	$('#friends_detail').block({ 
		message: "<h3>Fetching Friend List</h3>",
    	css: { 
            border: 'none', 
            padding: '10px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px !important', 
            '-moz-border-radius': '10px !important', 
            opacity: 0.5, 
            color: '#fff' 
    	}
    });
    $('#friend_list').load('users/facebook_friends',function(){
        if(!facebook_logged_in)
        {
            facebook_logged_in=true;
            $('.fb_link_btn').unbind('click').click(function(){loadFbFriendsByAjax(); return false;});
        }
        attechInviteEvent();
    });
}


function loadTwFriendsByAjax()
{
	$('#friends_detail').block({ 
		message: "<h3>Fetching Friend List</h3>",
    	css: { 
            border: 'none', 
            padding: '10px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px !important', 
            '-moz-border-radius': '10px !important', 
            opacity: .5, 
            color: '#fff' 
    	}
    });
    $('#friend_list').load('users/twitter_friends',function(){
    	if(!twitter_logged_in)
		{
			 twitter_logged_in=true;
			 $('.tw_link_btn').unbind('click').click(function(){loadTwFriendsByAjax(); return false;});
		}
    	attechInviteEvent();
		
	});
}
function attechInviteEvent()
{
	
	$('#friends_detail a').each(function(){
		if($(this).hasClass('everdrobe'))
		{
			$(this).click(function(){
				var thisBtn=$(this);
				if(thisBtn.hasClass("processing")) 
				{
					return false;
				}
				thisBtn.addClass("processing");
				if($(this).text()=="Follow")
				{
					thisBtn.text("Following");
					$.ajax({
						url: 'users/follow',
						type: "POST",
						data: {user_id: thisBtn.attr('rel')},
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
						  	{
						  		thisBtn.text("Following");
						  	}	
						  	else 
						  	{
						  		thisBtn.text("Follow");
						  	}
						  	thisBtn.removeClass("processing");
					  	}
					});
				}
				else
				{
					thisBtn.text("Follow");
					$.ajax({
						url: 'users/unfollow',
						type: "POST",
						data: {user_id: thisBtn.attr('rel')},
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
						  	{
						  		thisBtn.text("Follow");
						  	}	
						  	else 
						  	{
						  		thisBtn.text("Following");
						  	}
						  	thisBtn.removeClass("processing");
					  	}
					});
				}
				return false;
			});
		}
		else if($(this).hasClass('facebook'))
		{
			$(this).click(function(){
				var thisBtn=$(this);
				if($(this).text()=="Invite"){
					if(thisBtn.hasClass("processing")) 
					{
						return false;
					}
					thisBtn.addClass("processing").text("Invite Sent");
					$.ajax({
						url: 'users/invite_friend/facebook/'+ thisBtn.attr('rel'),
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
						  	{
						  		thisBtn.text("Invite Sent").bind('click', function(){return false;});
						  	}	
						  	else 
						  	{
						  		thisBtn.text("Invite");
						  	}
						  	thisBtn.removeClass("processing");
					  	}
					});
				}
				return false;
			});
		}
		else if($(this).hasClass('twitter'))
		{
			$(this).click(function(){
				var thisBtn=$(this);
				if($(this).text()=="Invite"){
					if(thisBtn.hasClass("processing")) 
					{
						return false;
					}
					thisBtn.addClass("processing").text("Invite Sent");
					$.ajax({
						url: 'users/invite_friend/twitter/'+ thisBtn.attr('rel'),
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
						  	{
						  		thisBtn.text("Invite Sent").bind('click', function(){return false;});
						  	}	
						  	else 
						  	{
						  		thisBtn.text("Invite");
						  	}
						  	thisBtn.removeClass("processing");
					  	}
					});
				}
				return false;
			});
		}
		else if($(this).hasClass('follower_list'))
		{
			$(this).click(function(){
				var thisBtn=$(this);
				if(thisBtn.hasClass("processing")) 
				{
					return false;
				}
				thisBtn.addClass("processing");
				if($(this).text()=="Follow")
				{
					thisBtn.text("Following");
					$.ajax({
						url: 'users/follow',
						type: "POST",
						data: {user_id: thisBtn.attr('rel')},
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
						  	{
						  		thisBtn.text("Following");
						  	}	
						  	else 
						  	{
						  		thisBtn.text("Follow");
						  	}
						  	thisBtn.removeClass("processing");
					  	}
					});
				}
				else
				{
					thisBtn.text("Follow");
					$.ajax({
						url: 'users/unfollow',
						type: "POST",
						data: {user_id: thisBtn.attr('rel')},
						success: function(data) {
						  	obj=$.parseJSON(data);
						  	if(obj.type=="success") 
						  	{
						  		thisBtn.text("Follow");
						  	}	
						  	else 
						  	{
						  		thisBtn.text("Following");
						  	}
						  	thisBtn.removeClass("processing");
					  	}
					});
				}
				return false;
			});
		}
		
	});
	fleXenv.fleXcrollMain('friends_detail');
	$('#friends_detail').unblock();
}





/**************************
 * code for cropping image
**************************/
var jcrop_api; // Holder for the API
function display_image_cropping(response)
{
	console.log("display_image_cropping :: ");
	console.log(response);
	$('#DrobeFileName').val(response.file);
    $('#crop_image').attr('src',drobe_image_dir + response.file);
    
	var width= response.size.width;
	var height= response.size.height;
	
	if(max_width < width)
    {
    	$('.drobe_preview img').attr("width",max_width).removeAttr("height");
    	height=height/(width/max_width);
    	if(max_height < height) 
    		{
    			$('.drobe_preview img').attr("height",max_height).removeAttr("width");
    		}
    }
    if(max_height < height)
    	{
    		$('.drobe_preview img').attr("height",max_height).removeAttr("width");
    		
    	}
    
    
    $('#crop_image').load(function(){
    	try
    	{
    		
    		// display image cropping area with image after loading a image;
	    	$('#file_uploader').hide();
	    	$('.drobe_preview').show().find('.big-img').css({'background':"#EDEDED"});
	    	$("#file-uploader").show();
	    	$('#file_uploader').unblock();
	    	$('#sell_drobe_preview').unblock();
	    	
	    	img_width=$(this).width();
	    	img_height=$(this).height();
	    	
	    	if($(this).parent().width() > img_width) 
	    	{
	    		$(this).parent().css('padding-left',parseInt(($(this).parent().width()-img_width)/2)+"px");
	    	}
	    	if($(this).parent().height() > img_height) 
	    	{
	    		$(this).parent().css('padding-top',parseInt(($(this).parent().height()-img_height)/2)+"px");
	    	}
	    	
	    	
	    	//Check for if image not coming from google search then apply cropping to that image. this variable declare publicly in mydrobe.ctp
	    	if(is_google_image==false)
	    	{	
		    	$('#crop_image').Jcrop({
		        	aspectRatio: 1,
		        	minSize: [ 320, 320],
		        	onSelect: updateCoords,
		        	trueSize: [response.size.width,response.size.height]
		        	},
		        	function(){
		        		bounds = this.getBounds(); 
		            	boundx = bounds[0];
		            	boundy = bounds[1];
		            	jcrop_api = this;
		            	if(boundx>boundy)
		            	{
		            		left_cords= parseInt((boundx-boundy)/2);
		            		jcrop_api.animateTo([left_cords,0,boundx,boundy]);
		            		updateCoords({x:left_cords,y:0,w:boundy,h:boundy});
		            	}
		            	else
		            	{
		            		top_cords= parseInt((boundy-boundx)/2);
		            		jcrop_api.animateTo([0,top_cords,boundx,boundy]);
		            		updateCoords({x:0,y:top_cords,w:boundx,h:boundx});
		            	}
		            	
		            	
		        });
	    	}
	    	
	    	// Set this variable initially to false this variable is used for checking image come from google if come from google then it contain true value.
	    	is_google_image = false;
	    	
    	}
    	catch(e)
    	{
    		alert(e.valueOf());
    	}
    	
    	$(this).unbind("load");
    });
    
    
   // $.colorbox.close();
}


function updateCoords(c)
{
	$('#pos_left').val(c.x);
	$('#pos_top').val(c.y);
	$('#new_width').val(c.w);
	$('#new_height').val(c.h);
}

function checkCoords()
{
	if (parseInt($('#pos_left').val())) 
	{
		return true;
	}
	alert('Please select a crop region then press submit.');
	return false;
}
function check_comment_length(str){
	var data = str.split(/[^\w\d]/);
         for (var i = 0; i < data.length; i++) {
             if (data[i].length >= 25) {
                 return false;
             }
         }
         return true;
 }
 
 /****code for google images load******/
 
 var lastPage=3;
 var googleImagesUrls=new Array();
 var googleUrl="https://ajax.googleapis.com/ajax/services/search/images?v=1.0&callback=getImagesJson&rsz=8";
 var searchText="";
 function getImagesFromUrl(textSearch,start){
 	tempGoogleUrl=googleUrl+"&q="+textSearch+"&start="+start;
 	$.ajax({
 		  url: tempGoogleUrl,
 		  dataType : "jsonp",
 		  success: function(data) {
 				eval(data);
 		  }
 	});
 	
 }

 var getImagesJson=function (data){
 	for(i in data.responseData.results)
 	{
 		googleImagesUrls.push({"thumbUrl":data.responseData.results[i].tbUrl,"url":data.responseData.results[i].url});
 	}

 	if(data.responseData.cursor.currentPageIndex<(lastPage-1))
 	{
 		getImagesFromUrl(searchText,((data.responseData.cursor.currentPageIndex)+1)*8);
 	}
 	else
 	{
 		setImagesToView(googleImagesUrls);
 	}	
 };
 
 function setScrollBarFilter(index)
	{
	 	var selectedtab = '#TabbedPanelsFilter .TabbedPanelsContentGroup > div:eq('+index+') .flexcroll';
		if($(selectedtab)[0].fleXcroll== undefined)
		{
		   fleXenv.fleXcrollMain($(selectedtab).attr('id'));
		
		}	 
	}	

 function setImagesToView(urls)
 {
 	var html="";
 	for(i in urls)
 	{
 		html+="<img src='"+urls[i].thumbUrl+"' rel='"+urls[i].url+"' />";
 	}
 	html+="<div class='clear'></div>";
 	$('#image-search-results-block').html(html);

 	$('#image-search-results-block img').bind('click',function(){
 		if(jcrop_api!=undefined)
		  {
			  jcrop_api.destroy();
			  $('.drobe_preview').hide();
			  $('#file_uploader').show();
		  }
 		var currentUrl=$(this).attr('rel');
 		googleImagesUrls=new Array();
 		$('.blockOverlay').trigger('click');
 		$('#file_uploader').block({ 
			message: "<h3>Uploading Image</h3>",
	    	css: { 
	            border: 'none', 
	            padding: '10px', 
	            backgroundColor: '#000', 
	            '-webkit-border-radius': '10px !important', 
	            '-moz-border-radius': '10px !important', 
	            opacity: 0.5, 
	            color: '#fff' 
        } });
 		
 		is_google_image = true;
 		/* This function is used for upload image from URL when search either from google or from aviary  */
 		upload_image_from_url(currentUrl,true);
	});
 }
 
 //is_open_editor is used for check we want to open aviary image editor or not if true then open else not open?
 function upload_image_from_url(currentUrl,is_open_editor)
 {
	 $.ajax({
 		  url: 'uploads/image_from_url/original.json',
 		  data: {url:currentUrl},
 		  type:'POST',
 		  success: function(data) {
 			  console.log("upload_image_from_url ::");
 			  console.log(data.response);
 			  if(data.response.type=='success')
			  {
 				  //assign last uploaded response to this variable which is public declare in mydrobe.ctp that contain last response from aviary.
 				  last_uploaded_response = data.response;
 				 
 				   display_image_cropping(data.response);  
 				 
 				  $('#image-search-results-block').html('');
 				  $('#google-image-query-block').val('');
 				  $('.blockOverlay').trigger('click');
 				  searchText="";
 				  
 				  if(is_open_editor==true)
				  {
 					 // display image aviary editor with image after loading a image;
 					    var src = $("#crop_image").attr("src");
 					//Call avaiary image function launchEditor for displaying image with its effects
 						featherEditor.launch({
 					        image: "crop_image",
 					        url: src
 					    });
				  }
 			
 			 // After image upload bind load event to crop_image id for further load image if user wants to upload another image.	  
 			  $("#crop_image").bind("load");
 			  
			  }
 			  else
			  {
			  	alert(data.response.message);
			  	$('#image-search-results-block').html('');
				$('#google-image-query-block').val('').focus();
				searchText="";
			  }
 		  }
 	});
 }
 
 $(document).ready(function(){
	$('#search-images-block').click(function(){
		if($('#google-image-query-block').val()!="" && searchText!=$('#google-image-query-block').val())
		{
			googleImagesUrls=new Array();
			$('#image-search-results-block').html('<div class="loader-bar"><img src="images/287.gif" /></div>');
			searchText=$('#google-image-query-block').val();
			getImagesFromUrl(searchText,0);
		}
	});
		
	$('#search_online_btn').click(function(){
		$.blockUI({
	        message: $('#image_search'), 
	        css: { 
	            top:  ($(window).height()) /2 + 'px', 
	            left: ($(window).width()-300) /2 + 'px', 
	            width: '500px' 
	        } 
	    }); 
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
	});
});