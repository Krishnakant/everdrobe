<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eblog');

/** MySQL database username */
define('DB_USER', 'eblog');

/** MySQL database password */
define('DB_PASSWORD', 'edo11jan');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8#c6ZtH~Uu>fm!|{+WwLuGQ|}_x&+F4<!-PB,-|d+XnPby(CB]ZxDY6q|`cmHc#n');
define('SECURE_AUTH_KEY',  'xhI!c zGn |CkSUEqi}v H<&.^Wj~Mq9xqY7?Tcg3H-h8b&FT#2N|.A]ol7ihACz');
define('LOGGED_IN_KEY',    'qn5w_VJ}9=_]|2Ex-@6FJKVk5?J}#XA.7|{FI&me`.9++%&+GU(1;*@9X,pUe&@ ');
define('NONCE_KEY',        'n8SJm8^h}(.iJ7L JaHHC:,|+pbG*^WE-,A3h^{p}s<&jeje,04QF.+!xbr*vk0L');
define('AUTH_SALT',        'ek+]FW+Be#Hv-]~{v|o0=uJ{;Q;[vaRwYgtZK+S6[Qh.Px{|[qp+MSO_H~E-ql?%');
define('SECURE_AUTH_SALT', '(eD;t-E%*1UU6j6dj;|Xf{3v#)gjFXdu-s8+se[4|H+@W+Ef@LPFbWP@FxA}/Rh+');
define('LOGGED_IN_SALT',   '`[/_#r;K$6!G^|$<tlkny%B1`yB;4#w>Vo8%_|>h$*JlrTwehcG,,!!j_*SeYi~*');
define('NONCE_SALT',       '}pXSmdHs-mSB^XnT9L>;m]z&o]s$jKN/@d^Gn7tv6C*#)| QGNS<8^@Z*:O(*`9!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
