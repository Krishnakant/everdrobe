<?php 
function resizeActualImage($source,$target)
{
	echo $source.":: ";
	$thisNewWidth=160;
	$thisNewHeight=160;
	
	/* read the source image */
	$ext=strtolower(array_pop(explode(".",$source)));
	
	if($ext=="jpg" || $ext=="jpeg") $source_image = imagecreatefromjpeg($source);
	else if($ext=="gif") $source_image = imagecreatefromgif($source);
	else if($ext=="png") $source_image = imagecreatefrompng($source);
	if($source_image==null) return;
	
	$width = imagesx($source_image);
	$height = imagesy($source_image);

	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($thisNewWidth,$thisNewHeight);

	/* copy source image at a resized size */
	imagecopyresampled($virtual_image,$source_image,0,0,0,0,$thisNewWidth,$thisNewHeight,$width,$height);

	if($ext=="jpg" || $ext=="jpeg") imagejpeg($virtual_image,$target);
	else if($ext=="gif") imagegif($virtual_image,$target);
	else if($ext=="png") imagepng($virtual_image,$target);
	imagedestroy($virtual_image);
	imagedestroy($source_image);
}
$dir="drobe_images/";
$targetpath="drobe_images/thumb/";
$files = scandir($dir);

foreach($files as $file)
{
	echo $file."::";
	if($file=="." || $file=="..") continue;
	$path=$dir."/".$file;
	if(is_file($path))
	{
		resizeActualImage($path,$targetpath.$file);
		echo "resized: ".$path."\n";
	}
}
?>