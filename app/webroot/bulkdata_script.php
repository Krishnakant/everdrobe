<?php
$google_image_url = "https://ajax.googleapis.com/ajax/services/search/images?" .
       "v=1.0&q=".urlencode("cards")."&imgsz=xlarge&rsz=8&userip=209.239.117.48&start=";
$start_counter=0;

$webservice_url="http://www.everdrobe.com/test/";

$searched_images=array();

$category=null;

//$google_image_url="http://www.google.com/images?oe=utf8&ie=utf8&source=uds&start=0&imgsz=xlarge&hl=en&q=amezing+places";
function get_images_from_google()
{
	global $google_image_url,$start_counter;
    //echo $google_image_url.$start_counter;
    //exit();
	// now, process the JSON string
	$json = json_decode(file_get_contents($google_image_url.$start_counter));
	// now have some fun with the results...
	echo "<pre>";
	$images=array();
	foreach ($json->responseData->results as $data)
	{
		$images[]=array('title'=>$data->title,'url'=>$data->url);
	}
	$start_counter=$start_counter+8;
	return $images;
}
function add_new_drobe($user_id,$drobe_data)
{
	global $webservice_url,$category;
	$url=$webservice_url."drobes/add_drobe/".$user_id.".json";
	$data=array(
			'comment='.urlencode(strip_tags($drobe_data['title'])),
			'category_id='.$category[rand(0,(count($category)-1))],
			'ask_public=1',
			'ask_friend=1',
			'facebook=0',
			'twitter=0',
			'Male=0',
			'Female=0',
			'image_url='.$drobe_data['url']
	);
	
	$post_string=implode("&", $data);
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST      ,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS    ,$post_string);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
	curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
	curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
	$response = curl_exec($ch);
	print_r($response);
	echo "<br>";
}
function add_new_user($counter=1)
{
	global $webservice_url,$searched_images;
	$url=$webservice_url."users/add_user.json";
	$gender=array("male","female");
	shuffle($gender);
	$data=array(
		'first_name='.urlencode("Ed".sprintf("%03d",$counter)),
		'last_name='.urlencode("Test"),
		'email='.urlencode("ed".sprintf("%03d",$counter).".test@everdrobe.com"),
		'password='.urlencode("everdrobe123"),
		'birth_date='.urlencode(date('m/d/Y',strtotime('-'.rand(7000,10000)." days"))),
		'gender='.urlencode(array_pop($gender))
	);
	$post_string=implode("&", $data);
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST      ,1);
	curl_setopt($ch, CURLOPT_POSTFIELDS    ,$post_string);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
	curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
	curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
	$response = curl_exec($ch);
	$response_array=json_decode($response,true);
	if(true || $response_array['response']['type']=="success")
	{
		$user_id=$response_array['response']['user_id'];
		$drobe_counter=rand(15,20);
		for($i=0;$i<$drobe_counter;$i++)
		{
			if(count($searched_images)==0)
			{
				$searched_images=get_images_from_google();
			}
			$image_data=array_pop($searched_images);
			add_new_drobe($user_id,$image_data);
		}
	}
}
function get_drobe_categories()
{
	global $webservice_url,$category;
	$url=$webservice_url."categories/getlist.json";
	$response=json_decode(file_get_contents($url),true);
	foreach($response['response']['categories'] as $cat)
	{
		$category[]=$cat['id'];
	}
}
get_drobe_categories();

for($user=65;$user<=80;$user++) add_new_user($user);
?>