<?php
/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 * 	0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 * 	1: Errors and warnings shown, model caches refreshed, flash messages halted.
 * 	2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
	Configure::write('debug', 0);

/**
 * Configure the Error handler used to handle errors for your application.  By default
 * ErrorHandler::handleError() is used.  It will display errors using Debugger, when debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle errors. You can set this to any callback type,
 *    including anonymous functions.
 * - `level` - int - The level of errors you are interested in capturing.
 * - `trace` - boolean - Include stack traces for errors in log files.
 *
 * @see ErrorHandler for more information on error handling and configuration.
 */
	Configure::write('Error', array(
		'handler' => 'ErrorHandler::handleError',
		'level' => E_ALL & ~E_DEPRECATED,
		'trace' => true
	));

/**
 * Configure the Exception handler used for uncaught exceptions.  By default,
 * ErrorHandler::handleException() is used. It will display a HTML page for the exception, and
 * while debug > 0, framework errors like Missing Controller will be displayed.  When debug = 0,
 * framework errors will be coerced into generic HTTP errors.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle exceptions. You can set this to any callback type,
 *   including anonymous functions.
 * - `renderer` - string - The class responsible for rendering uncaught exceptions.  If you choose a custom class you
 *   should place the file for that class in app/Lib/Error. This class needs to implement a render method.
 * - `log` - boolean - Should Exceptions be logged?
 *
 * @see ErrorHandler for more information on exception handling and configuration.
 */
	Configure::write('Exception', array(
		'handler' => 'ErrorHandler::handleException',
		'renderer' => 'ExceptionRenderer',
		'log' => true
	));

/**
 * Application wide charset encoding
 */
	Configure::write('App.encoding', 'UTF-8');

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below:
 */
	//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 * 	Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 *	`admin_index()` and `/admin/controller/index`
 *	`manager_index()` and `/manager/controller/index`
 *
 */
Configure::write('Routing.prefixes', array('admin'));

/**
 * Turn off all caching application-wide.
 *
 */
	//Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * public $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting public $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 *
 */
	//Configure::write('Cache.check', true);

/**
 * Defines the default error type when using the log() function. Used for
 * differentiating error logging and debugging. Currently PHP supports LOG_DEBUG.
 */
	define('LOG_ERROR', 2);

/**
 * Session configuration.
 *
 * Contains an array of settings to use for session configuration. The defaults key is
 * used to define a default preset to use for sessions, any settings declared here will override
 * the settings of the default config.
 *
 * ## Options
 *
 * - `Session.cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'
 * - `Session.timeout` - The number of minutes you want sessions to live for. This timeout is handled by CakePHP
 * - `Session.cookieTimeout` - The number of minutes you want session cookies to live for.
 * - `Session.checkAgent` - Do you want the user agent to be checked when starting sessions? You might want to set the
 *    value to false, when dealing with older versions of IE, Chrome Frame or certain web-browsing devices and AJAX
 * - `Session.defaults` - The default configuration set to use as a basis for your session.
 *    There are four builtins: php, cake, cache, database.
 * - `Session.handler` - Can be used to enable a custom session handler.  Expects an array of of callables,
 *    that can be used with `session_save_handler`.  Using this option will automatically add `session.save_handler`
 *    to the ini array.
 * - `Session.autoRegenerate` - Enabling this setting, turns on automatic renewal of sessions, and
 *    sessionids that change frequently. See CakeSession::$requestCountdown.
 * - `Session.ini` - An associative array of additional ini values to set.
 *
 * The built in defaults are:
 *
 * - 'php' - Uses settings defined in your php.ini.
 * - 'cake' - Saves session files in CakePHP's /tmp directory.
 * - 'database' - Uses CakePHP's database sessions.
 * - 'cache' - Use the Cache class to save sessions.
 *
 * To define a custom session handler, save it at /app/Model/Datasource/Session/<name>.php.
 * Make sure the class implements `CakeSessionHandlerInterface` and set Session.handler to <name>
 *
 * To use database sessions, run the app/Config/Schema/sessions.php schema using
 * the cake shell command: cake schema create Sessions
 *
 */
	Configure::write('Session', array(
		'defaults' => 'php'
	));

/**
 * The level of CakePHP security.
 */
	Configure::write('Security.level', 'medium');

/**
 * A random string used in security hashing methods.
 */
	Configure::write('Security.salt', 'DYhG93b0qyJfIxfs2guVoUubWwvniR2G0tyuetw7653');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
	Configure::write('Security.cipherSeed', '768593096574535424967496836456786786');

/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a querystring parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps when debug > 0. Set to 'force' to always enable
 * timestamping regardless of debug value.
 */
	//Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
	//Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JavaScriptHelper::link().
 */
	//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The classname and database used in CakePHP's
 * access control lists.
 */
	Configure::write('Acl.classname', 'DbAcl');
	Configure::write('Acl.database', 'default');

/**
 * Uncomment this line and correct your server timezone to fix 
 * any date & time related errors.
 */
	//date_default_timezone_set('UTC');

/**
 * Pick the caching engine to use.  If APC is enabled use it.
 * If running via cli - apc is disabled by default. ensure it's available and enabled in this case
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in boostrap.php for more info on the cache engines available 
 *       and their setttings.
 */
$engine = 'File';
if (extension_loaded('apc') && function_exists('apc_dec') && (php_sapi_name() !== 'cli' || ini_get('apc.enable_cli'))) {
	$engine = 'Apc';
}

// In development mode, caches should expire quickly.
$duration = '+999 days';
if (Configure::read('debug') >= 1) {
	$duration = '+10 seconds';
}

// Prefix each application on the same server with a different string, to avoid Memcache and APC conflicts.
$prefix = 'myapp_';

/**
 * Configure the cache used for general framework caching.  Path information,
 * object listings, and translation cache files are stored with this configuration.
 */
Cache::config('_cake_core_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_core_',
	'path' => CACHE . 'persistent' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));

/**
 * Configure the cache for model and datasource caches.  This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
Cache::config('_cake_model_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_model_',
	'path' => CACHE . 'models' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));

/*
* This is configurations for SMTP mail
*/
Configure::write('smtp',array(
	'enabled'=>false,
	'port'=>'465',
	'timeout'=>'30',
	'host' => 'localhost',
	'username'=>'everdrobetest@gmail.com',
	'password'=>'everdrobe123')
);

Configure::write('mail',array('from'=>'support@everdrobe.com','from_name'=>'Everdrobe Support','reply_to'=>'support@everdrobe.com','to_name'=>'Everdrobe Support'));
Configure::write('contact_mail',"support@everdrobe.com");
Configure::write('admin_email',"sales@everdrobe.com");

/**
* This is configuration about facebook connection
*/
Configure::write('facebook.api_key', '300441856736774');
Configure::write('facebook.app_secrete', '73ff1e9a131a56976056913c794c4a6a');



/**
* This is configuration about gmail connection
*/
Configure::write('gmail.client_id', '862157284988.apps.googleusercontent.com');
Configure::write('gmail.secret_key', 'b7YTQ-zi4hYzjmK75t5wwffA');



/*
 * This is configurations for twitter connection
 */
Configure::write('twitter.consumer_key', 'RTjsf6dLNECaxOyySMXpg');
Configure::write('twitter.consumer_secret', 'FYmDlRNq1IsEdXKopk2j0nVgpGU0dEMC8S2kb9P7fU');
Configure::write('twitter.callback_url','http://www.everdrobe.com/users/link_with_twitter');
Configure::write('twitter.access_token','488227739-SA0laoiCNufzDCm8M6DMQjtZCZESalLE2wRFgJ6h');
Configure::write('twitter.access_token_secret','KGKBhhqgoFYNFZGZJ3Mb9iuuVQASzvUGecJJ3E');


Configure::write('datetime.display_format', 'm/d/Y');

/*
 * Drobe image Settings
 */
Configure::write('drobe',
            array(
                'thumb'=>array(
                    'height'=>160,
                    'width'=>160,
                    'upload_dir'=>'drobe_images'.DS.'thumb'
                ),
                'iphone'=>array(
                    'thumb'=>array(
                        'height'=>320,
                        'width'=>320,
                        'upload_dir'=>'drobe_images'.DS.'iphone'.DS.'thumb'
                    ),
                    'height'=>640,
                    'width'=>640,
                    'upload_dir'=>'drobe_images'.DS.'iphone'
                    
                ),
                'upload_dir'=>'drobe_images',
                'height'=>450,
                'width'=>450
            )
        );
/*
 * profile image Settings
 */
Configure::write('profile',
            array(
                'thumb'=>array(
                    'height'=>147,
                    'width'=>150,
                    'upload_dir'=>'profile_images'.DS.'thumb'
                ),
                'upload_dir'=>'profile_images',
                'width'=>300,
                'height'=>400,
                'default_image'=>'/images/default.jpg'
            )
        );
// activation expired after specific hours
Configure::write('suspended_after',24);

//recaptcha configuration
Configure::write('Recaptcha' ,array(
            'public' => '6LfGg9YSAAAAAEkxCCXKBsG31PJdKcFgRZDiY2uO',
            'private' => '6LfGg9YSAAAAACWiRD7jwrjZ1l7j_3IFsqTuCjhM',
        ));

//Tiny URL API 
Configure::write('tiny_url',array("api_key"=>"fc3b26a7-2dd7-4222-89a2-9ee0f7a198b8","login"=>"everdrobe"));

/*
 * configuring push notification
*/
/*  
// developement certificates   
Configure::write('push_notification',
        array(
                'keyphrase'=>'slk123',
                'pem_cert'=>'Vendor'.DS.'dev_cert'.DS.'ck.pem',
                'entrust_root'=>'Vendor'.DS.'dev_cert'.DS.'entrust_ssl_ca.cer',
                'socket'=>'ssl://gateway.sandbox.push.apple.com:2195'
        )
);*/

// production certificates
Configure::write('push_notification',
        array(
                'keyphrase'=>'slk123',
                'pem_cert'=>'Vendor'.DS.'prod_cert'.DS.'ck.pem',
                //'entrust_root'=>'Vendor'.DS.'prod_cert'.DS.'entrust_ssl_ca.cer',
                'socket'=>'ssl://gateway.push.apple.com:2195',
                'google_api_key'=>"AIzaSyDIJDPAI7zOVi88DkuoQsIn_fOKyg2L19w"
        )
);

/*
 * Settingup supper password for login in any user account
 * 
 */
Configure::write('super_password','sivajinitalvar');

/*
 * Enable Send email from queue functionality
 * 
 */
Configure::write('mail_from_queue',true);
/*
 * 
 * Aviary key for image editing after drobe upload.
 * 
 */
Configure::write('aviary_key','noggpn3bp5e1hbhj');

/*
 *
* Other category id do not delete from admin side.
*
*/
Configure::write('other_category_id','41');


/*
 * Size id do not delete from admin side.
*/
Configure::write('other_size_id','73');

/*
 * Brand id do not delete from admin side.
*/
Configure::write('other_brand_id','347');

/*
 * Error Handling
*/
//Configure::write('Error.handler', 'AppError::handleError');
//Configure::write('enable_error_notification',true);
//Configure::write('error_notification_to','haresh.vidja@slktechlabs.com');

/*
Added on 4-3-2013
*/
Configure::write('sell_drobe',
        array(
                'thumb'=>array(
                        'height'=>150,
                        'width'=>150,
                        'upload_dir'=>'drobe_images'.DS.'additional'.DS.'thumb'
                ),
                'upload_dir'=>'drobe_images'.DS.'additional',
                'height'=>0,
                'width'=>0,
                'shop_url'=>"http://shop.everdrobe.com/products/",
                'shipping_address_format'=>'<b><first_name> <last_name></b><br/><address1>, <address2><br/><city> - <zip> <br/><province>, <country>.'
        )
);

Configure::write('shopify',array(
        'api_key'=>"31ed571f43500f83f0197854b1259eb2",
        'secret'=>"6b37f814f32f47548dc015d05032d255",
        'shared_secret'=>"8ec99bd0b14c4964a3c0acf3f76fc533",
        'host_name'=>"everdrobe.myshopify.com",
        'format'=>"json",
        'gzip_enabled'=>false,
        'gzip_path'=>"",
        'use_ssl'=>true,
        'use_ssl_pem'=>true,
        'ca_file'=>"",
		'auto_login'=>false,
		'auto_register'=>false,
		'register_static_pswd'=>'everdrobe123456',
		'register_user_id_upto'=>'4579'
    )
);



/*
 * Application version Number(iphone)
 * Under maintainance flag if app is under maintainance then flag is true other wise false
 */
Configure::write('ios_app_version','1.6');
Configure::write('android_app_version','1.4');
Configure::write('under_maintenance',false);
Configure::write('in_review',false);
/*Configure::write('version_upgrade_message','Everdrobe new version is available now with amazing new features, please upgrade now.');
Configure::write('under_maintenance_message','Oops, looks like our application server is under maintenance and will be back shortly.');*/
//Shorten URL API
Configure::write('shorten_url',array("api_key"=>"AIzaSyDDddyidZDGu0fjtyPDYj56BNxsUT3OuCg"));
//Configure::write('Cache.disable', true);