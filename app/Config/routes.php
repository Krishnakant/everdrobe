<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 * 
 */

	Router::parseExtensions();

	Router::connect('/', array('controller' => 'drobes', 'action' => 'index','admin'=>false));
	
	Router::connect('/sell', array('controller' => 'sell_drobes','action'=>"index", 'admin'=>false));
	Router::connect('/sell/sold_drobes', array('controller' => 'sold_drobes','action'=>'index','admin'=>false));
	Router::connect('/sell/profile', array('controller' => 'sell_profiles','action'=>'sell_profile','admin'=>false));
	Router::connect('/sell/:action/*', array('controller' => 'sell_drobes','admin'=>false));
	
	
	
	Router::connect('/facebook/description', array('controller' => 'pages','action'=>'facebook_description'));
	
	Router::connect('/e-a-d-m-i-n-x',array('controller'=>'admins','action'=>'index','admin'=>true));
	Router::connect('/e-a-d-m-i-n-x/login',array('controller'=>'admins','action'=>'login','admin'=>true));
	Router::connect('/e-a-d-m-i-n-x/logout',array('controller'=>'admins','action'=>'logout','admin'=>true));
	Router::connect('/e-a-d-m-i-n-x/forgotpassword', array('controller' => 'admins', 'action' => 'forgot_password','admin'=>true));
	Router::connect('/e-a-d-m-i-n-x/changepassword', array('controller' => 'admins', 'action' => 'change_password','admin'=>true));
	Router::connect('/e-a-d-m-i-n-x/:controller/:action/*', array('admin'=>true));
	Router::connect('/e-a-d-m-i-n-x/:controller/*', array('admin'=>true));
	
	Router::connect('/home/*', array('controller' => 'drobes', 'action' => 'index','admin'=>false));
	/*
	 * Addedd new routing rules
	 */
	Router::connect('/about-us', array('controller' => 'pages', 'action' => 'display', 'about-us'));
	
	/*
	 * for iphone only
	 */
	Router::connect('/terms/iphone', array('controller' => 'pages', 'action' => 'display', 'terms','iphone'));
	Router::connect('/privacy/iphone', array('controller' => 'pages', 'action' => 'display', 'privacy','iphone'));
	Router::connect('/faq/iphone', array('controller' => 'pages', 'action' => 'display', 'faq','iphone'));
	
	Router::connect('/terms', array('controller' => 'pages', 'action' => 'display', 'terms'));
	Router::connect('/privacy', array('controller' => 'pages', 'action' => 'display', 'privacy'));
	Router::connect('/faq', array('controller' => 'pages', 'action' => 'display', 'faq'));
	
	Router::connect('/stream/*', array('controller' => 'drobes', 'action' => 'rate',0,'recent','rate_stream','admin'=>false));
	Router::connect('/rate/*', array('controller' => 'drobes', 'action' => 'rate','admin'=>false));
	Router::connect('/mydrobe', array('controller' => 'drobes', 'action' => 'mydrobe','admin'=>false));
	Router::connect('/faves', array('controller' => 'drobes', 'action' => 'faves','admin'=>false));
	Router::connect('/faves/*', array('controller' => 'drobes', 'action' => 'faves','admin'=>false));
	Router::connect('/results', array('controller' => 'drobes', 'action' => 'results','admin'=>false));
	Router::connect('/result/*', array('controller' => 'drobes', 'action' => 'result','admin'=>false));
	Router::connect('/following/*', array('controller' => 'users', 'action' => 'following','admin'=>false));
	Router::connect('/follower/*', array('controller' => 'users', 'action' => 'follower','admin'=>false));
	Router::connect('/user/*', array('controller' => 'users', 'action' => 'profile','admin'=>false));
	
	/*
	 * Start @sadikhasan code
	 * Sparate file for post drobe and sell drobe route
	 * */
	Router::connect('/mydrobe/post', array('controller' => 'drobes', 'action' => 'mydrobe_post'));
	Router::connect('/mydrobe/sell', array('controller' => 'drobes', 'action' => 'mydrobe_sell'));
	/*
	 * End of @sadikhasan code 
	 */
	 
	Router::connect('/contact', array('controller' => 'contacts', 'action' => 'index'));
	
	/*
	 * End of added new
	 */
	Router::connect('/profile/edit', array('controller' => 'users', 'action' => 'edit_profile','admin'=>false));
	Router::connect('/profile', array('controller' => 'users', 'action' => 'profile','admin'=>false));
	Router::connect('/forgotpassword', array('controller' => 'users', 'action' => 'forgot_password','admin'=>false));
	Router::connect('/changepassword', array('controller' => 'users', 'action' => 'change_password','admin'=>false));
	Router::connect('/change_email', array('controller' => 'users', 'action' => 'change_email','admin'=>false));
	
	/*
	 *  user block page
	 */
	Router::connect('/access_denied',array('controller'=>'users','action'=>'block','admin'=>false));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on 
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
