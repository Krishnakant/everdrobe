LIB=${0/%cake/}
APP=`pwd`
while getopts ":q" opt; do
  case $opt in
    q)
      quiet=1;
      shift $((OPTIND-1));
      OPTIND=1;
  esac
done
if [ $quiet != 1 ]; then
  clear
  exec php -q ${LIB}cake.php -working "${APP}" "$@"
else
  exec php -q ${LIB}cake.php -working "${APP}" "$@" >/dev/null 2>&1
fi
exit;
